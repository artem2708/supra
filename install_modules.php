<?php
require_once('common.php');
if($_GET['opr']!='install'){
	$dirs = Main::getDirList('mod');
	$db->query("select * from modules");
	if($db->nf()){
		while($db->next_record()){
			$Inst[] = $db->f('name');
		}
	}
	$new_mod = array_diff($dirs, $Inst);
	if(sizeof($new_mod)==0){die('Нет неустановленных модулей');}
	echo print_form_mod($new_mod);exit;
}
  elseif($_GET['opr']=='install' && sizeof($_GET['mod'])>0){
  	$dirs = Main::getDirList('mod');
  	foreach($_GET['mod'] as $v){
  		if(!in_array($v, $dirs)){die('Error');}
  		$ret[] = install_mod($v);
  	}
  	echo print_form_mod_inst($ret);exit;
}
function print_form_mod($arr){
$TEXT = <<<EOD
<html>
<head>
<title>Установка модулей</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
</head>
<body bgcolor="#FFFFFF" text="#000000">
<h1>Установить новые модули </h1>
<form name="form1" method="get" action="">
EOD;
foreach($arr as $v){
$TEXT .= <<<EOD
	<input type="checkbox" name="mod[]" value="$v"> $v<br>
EOD;
}
  
$TEXT .= <<<EOD
<hr>  
  <input type="checkbox" name="fill" value="1">Наполнить содержанием по умолчанию<br>
  <input type="submit" value="Выполнить">
  <input type="hidden" name="opr" value="install">
  <br>
</form>
</body>
</html>
EOD;
return $TEXT;
}
function print_form_mod_inst($arr){
$TEXT = <<<EOD
<html>
<head>
<title>Установка модулей</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
</head>
<body bgcolor="#FFFFFF" text="#000000">
<h1>Установить новые модули </h1>
EOD;
foreach($arr as $v){
$TEXT .= <<<EOD
	$v
EOD;
}
  
$TEXT .= <<<EOD
<hr>  
</body>
</html>
EOD;
return $TEXT;
}
function install_mod($mod, $fill=null){
  global $CONFIG, $db;
  $db->query("SET NAMES utf8");
  $file[] = sprintf(RP.'%s%s/sql/%s_sys_content.sql', $CONFIG['class_path'],$mod, $mod);
  $tbl = sprintf(RP.'%s%s/sql/%s_tables.php', $CONFIG['class_path'],$mod, $mod);
  if($fill){
    $file[] = sprintf(RP.'%s%s/.default/sql/%s_demo_content.sql', $CONFIG['class_path'],$mod, $mod);
  }
  foreach($file as $v){
  	$QUERYS = array_merge((is_array($QUERYS)?$QUERYS:array()), get_file_sql($v));
  }
  if(is_array($GLOBALS['core']->aSitesLangs)){
  	foreach($GLOBALS['core']->aSitesLangs as $v){
  		if(is_array($v['site_lang'])){
  			foreach($v['site_lang'] as $_k => $_v){
  				$prefix = sprintf('%s_%s', $v['site_alias'], $_k);
  				if(file_exists($tbl)){include($tbl);}
  				if(is_array($MOD_TABLES)){
  					foreach($MOD_TABLES as $query){
  						$query = str_replace('CREATE TABLE' , 'CREATE TABLE IF NOT EXISTS', $query);
  						$db->query($query);
  					}
  				}
  				if(is_dir(RP.'tpl/'.$prefix) && is_dir(RP."{$CONFIG['class_path']}{$mod}/.default/tpl")){
  					copy_files(RP."{$CONFIG['class_path']}{$mod}/.default/tpl",RP.'tpl/'.$prefix);
  				}
  				$demo_fill_pref[] = $prefix;
  			}
  		}
  	}
  }
  if(is_array($QUERYS)){
  	$QUERYN = array();
  	foreach($QUERYS as $v){
  		if(false !== strpos($v, '{site_prefix}')){
  			$QUERYN = array_merge($QUERYN, replace_sql($demo_fill_pref, $v));
  		}
  		 else {
  		   	array_push($QUERYN,$v);
  		}
  	}
  }
  if(is_array($QUERYS)){
  	foreach($QUERYS as $query){
  		$db->query($query);
  	}
  }
  return "модуль <b>$mod</b> установлен <br>";
}
function get_file_sql($file){
  if(file_exists($file)){
  	$text = file_get_contents($file);
  	$QUERYS = preg_split("/;[\r\n]+/",$text);
  }
  return $QUERYS;
}
function copy_files($from, $to){
  if (is_dir($from) && is_dir($to)) {
    if ($dh = opendir($from)) {
        while (($file = readdir($dh)) !== false) {
        	if(is_file($from.'/'.$file)){copy($from.'/'.$file, $to.'/'.$file);}
        }
        closedir($dh);
        return true;
    }
  }
  return false;
}
function replace_sql($arr, $str){
  if(sizeof($arr)==0){return array();}
  foreach($arr as $v){$ret[] = str_replace('{site_prefix}', $v, $str);}
  return $ret;
}
?>