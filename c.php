<?php
function unicode_urldecode($url){
   preg_match_all('/%u([[:alnum:]]{4})/', $url, $a);

   foreach ($a[1] as $uniord) {
       $dec = hexdec($uniord);
       $utf = '';

       if ($dec < 128) {
           $utf = chr($dec);
       } else if ($dec < 2048) {
           $utf = chr(192 + (($dec - ($dec % 64)) / 64));
           $utf .= chr(128 + ($dec % 64));
       } else {
           $utf = chr(224 + (($dec - ($dec % 4096)) / 4096));
           $utf .= chr(128 + ((($dec % 4096) - ($dec % 64)) / 64));
           $utf .= chr(128 + ($dec % 64));
       }
       $url = str_replace('%u'.$uniord, $utf, $url);
   }
   #return $url;
   return urldecode($url);
}
function code(&$str){
	if(!preg_match('~^(?:
              [\x09\x0A\x0D\x20-\x7E]            # ASCII
        	| [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
        	|  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
        	| [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
        	|  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
        	|  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
        	| [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
        	|  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
    )*$~xs', $str)) $str = iconv('WINDOWS-1251','UTF-8', $str);
}

define('RP', realpath(getcwd()).'/');

require_once(RP.'config.php');

$CONFIG = array_merge($CONFIG, $NEW);

require_once(RP.'inc/class.Main.php');

$db = new My_Sql();
$db->Database 	= $CONFIG['db_database'];
$db->Host 		= $CONFIG['db_host'];
$db->User 		= $CONFIG['db_user'];
$db->Password 	= $CONFIG['db_password'];

$db->query("SET NAMES utf8");

$site_config = Main::getSiteConfig();
$mult_lang   = @$site_config['multilanguage'] ? $site_config['multilanguage'] : $NEW['multilanguage'];
$def_lang    = @$site_config['language']      ? $site_config['language'] : $NEW['default_lang'];
$server      = @$site_config['pref']          ? $site_config['pref']."_" : Main::getServerName();

// Определение языковой версии сайта
if ($mult_lang && @$_REQUEST['lang']) {
	$lang	= My_Sql::escape($_REQUEST['lang']);
} else {
	$lang	= $def_lang;
}
define('SITE_PREFIX', $server.$lang);

$main = new Main();

$lang_exist = false;
foreach ($CONFIG['site_langs'] as $key => $val) {
	if ($lang == $val['value']) {
		$lang_exist = true;
		break;
	}
}
if (!$lang_exist) {
	exit;
}

require_once(RP.'mod/counter/create_base.php');

if (@$_SERVER['HTTP_X_FORWARDED_FOR']){
	$ip_addr = preg_split('/[^\d.]/', $_SERVER['HTTP_X_FORWARDED_FOR']);
} elseif (@$_SERVER['HTTP_FORWARDED_FOR']) {
	$ip_addr = preg_split('/[^\d.]/', $_SERVER['HTTP_FORWARDED_FOR']);
} else {
	$ip_addr = preg_split('/[^\d.]/', $_SERVER['REMOTE_ADDR']);
}
$ip_addr = sprintf("%u", ip2long($ip_addr[0]));

$t_stamp = time();
$sid = @$_COOKIE['abosid'] ? (int) $_COOKIE['abosid'] : crc32($ip_addr.$t_stamp.rand(999, 999999));
@setcookie('abosid', $sid, $t_stamp+60*60*30, "/");

$logotype	= imagecreate(1, 1);
$color		= ImageColorAllocate($logotype, 0, 0, 0);

header('Content-Type: '.$CONFIG['counter_banner_type']);
ImagePng($logotype);
ImageDestroy($logotype);

if (@isset($_SERVER['HTTP_REFERER'])) {
	$page_visit = $_SERVER['HTTP_REFERER'];
} else {
    $page_visit = "n\a";//$_SERVER['SCRIPT_NAME'];
}

$page_visit=str_replace('http://www.', '', $page_visit);
$page_visit=str_replace('http://', '', $page_visit);
if ($page_visit=='www.'.$_SERVER['HTTP_HOST'].'/') {
    $page_visit=$_SERVER['HTTP_HOST'];
}

$user_lang = My_Sql::escape($_SERVER['HTTP_ACCEPT_LANGUAGE']);

$day		= date('j');
$mounth		= date('m');
$year		= date('y');
$the_time	= date('G');
$minuts		= date('i');

if (@$_GET['history']) {
	$visit_from = unicode_urldecode($_GET['history']);
	$site_visitor = unicode_urldecode(Main::parseSiteName($_GET['history']));
} else {
	$visit_from	= $site_visitor = unicode_urldecode(Main::parseSiteName($_SERVER['HTTP_HOST']));
}
$arr = explode("/", $site_visitor);
$site_visitor = $arr[0];

if (@$_GET['resolution']) {
	$user_resolution=My_Sql::escape($_GET['resolution']);
} else {
	$user_resolution='Another';
}

$page_title=$db->escape(trim(@$_GET['title']));

if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Opera 8')) !== False) $user_agent='Opera 8.xx';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Opera 7')) !== False) $user_agent='Opera 7.xx';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Opera 6')) !== False) $user_agent='Opera 6.xx';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Opera 5')) !== False) $user_agent='Opera 5.xx';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Netscape 7')) !== False) $user_agent='Netscape 7.x';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Netscape 6')) !== False) $user_agent='Netscape 6.x';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Netscape 4')) !== False) $user_agent='Netscape 4.x';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Konqueror')) !== False) $user_agent='Konqueror';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Lynx')) !== False ) $user_agent='Lynx';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Galeon')) !== False ) $user_agent='Galeon';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Epiphany')) !== False ) $user_agent='Epiphany';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.0')) !== False) $user_agent='IE 6.0';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 5.5')) !== False) $user_agent='IE 5.5';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 5.0')) !== False) $user_agent='IE 5.0';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 4.0')) !== False) $user_agent='IE 4.0';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox')) !== False) $user_agent='Firefox';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Mozilla')) !== False) $user_agent='Mozilla';
else $user_agent='Another';

if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Linux')) !== False) $os_user='Linux';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Unix')) !== False) $os_user='Unix';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'FreeBSD')) !== False) $os_user='FreeBSD';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'IRIX')) !== False) $os_user='IRIX';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'BeOS')) !== False) $os_user='BeOS';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Mac/PPC')) !== False) $os_user='Mac';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'OS/2')) !== False) $os_user='OS/2';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'AIX')) !== False) $os_user='AIX';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'SunOS')) !== False) $os_user='SunOS';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Windows NT 5.2')) !== False) $os_user='Windows 2003';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Windows NT 5.1')) !== False) $os_user='Windows XP';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Windows NT 5')) !== False) $os_user='Windows 2000';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Windows NT')) !== False) $os_user='Windows NT';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Windows Me')) !== False) $os_user='Windows Me';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Windows 98')) !== False) $os_user='Windows 98';
else if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Windows 95')) !== False) $os_user='Windows 95';
else $os_user='Another';

$engine = '';
foreach ($CONFIG['search_engine_url'] as $key=>$val) {
	if (false!== strpos($site_visitor, $val)) {
		$engine = My_Sql::escape($key);
		break;
	}
}
$engine_str = '';
if ($engine) {
	$engine_str = preg_replace("/.+".$CONFIG['search_engine_var'][$engine]."=([^&]*).*/s","\\1", $visit_from);
	$engine_str = $engine_str == $visit_from ? '' : $engine_str;
	if ('windows-1251' != $CONFIG['search_engine_encoding'][$engine]) {
		$engine_str = iconv($CONFIG['search_engine_encoding'][$engine], 'WINDOWS-1251', $engine_str);
		$visit_from = iconv($CONFIG['search_engine_encoding'][$engine], 'WINDOWS-1251', $visit_from);
	}

	//set charset utf8
	code($engine_str);
	code($visit_from);

	#$engine_str = strtolower($engine_str);
}
$visit_from = My_Sql::escape($visit_from);
$site_visitor = My_Sql::escape($site_visitor);
$engine_str = My_Sql::escape($engine_str);

/* Определаяем главный модуль */
$url_info = parse_url($_SERVER['HTTP_REFERER']);
if ($CONFIG['rewrite_mod']) {
	$link = @$url_info['path'] ? preg_replace("/^\/(rus|ukr|blr|de|frn|eng)\/?/i", "", $url_info['path']) : "";
	/*
	$amp_pos = strpos($link, "&");
	$link = FALSE !== $amp_pos ? substr($link, 0, $amp_pos) : $link;

	*/
	if($link=='/') $link = null;
    $by = 'address';
} else {
    $link = array();
    parse_str(@$url_info['query'], $link);
    $link = (@$link['link'] && $link['link']!='/') ? My_Sql::escape($link['link']) : '';
    $by = 'link';
}
$link = My_Sql::escape($link);
$page_id = 0;
$module_id = 0;

$query = "SELECT P.id, PB.module_id, CT.zone, CT.name FROM ".$server.$lang."_pages AS P ";
$query .= "LEFT JOIN ".$server.$lang."_pages_blocks AS PB ON P.id = page_id AND 0=field_number ";
$query .= "LEFT JOIN counter_cities AS CT ON ".$ip_addr." BETWEEN CT.first_long_ip AND CT.last_long_ip ";
$query .= " WHERE ". ($link ? $by." = '".$link."'" : "is_default");
$db->query($query);
if ($db->nf()) {
	$db->next_record();
	$page_id = (int) $db->f('id');
	$module_id = (int) $db->f('module_id');
	$zone = $db->f('zone');
	$city = $db->f('name');
}

$city = $city ?  '"'.$city.'"' : "NULL";
$zone = $zone ? '"'.$zone.'"' : "NULL";


$db->query('INSERT DELAYED INTO '.$server.$lang.'_counter_'.$date.'
				VALUES ("'.$user_agent.'",
						"'.$page_visit.'",
						"'.$ip_addr.'",
						"'.$day.'",
						"'.$mounth.'",
						"'.$year.'",
						"'.$the_time.'",
						"'.$minuts.'",
						"'.$os_user.'",
						"'.$user_lang.'",
						"'.$visit_from.'",
						"'.$site_visitor.'",
						"'.$user_resolution.'",
						"'.$engine.'",
						"'.$engine_str.'",
						"'.$module_id.'",
						"'.$sid.'",
						"'.$t_stamp.'",
						'.$zone.',
						'.$city.')');

$db->query('SELECT count(*) as cnt FROM '.$server.$lang.'_counter_page_titles WHERE page_url="'.$page_visit.'"');
$db->next_record();
if ($db->f("cnt") == 0){
	$db->query('INSERT DELAYED INTO '.$server.$lang.'_counter_page_titles
				VALUES ("'.$page_visit.'",
						"'.$page_title.'")');
}
?>