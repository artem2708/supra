<?php

class Cache {

	var $last_modified = 0;											// дата последнего модифицированного кэш-файла страницы 

	// формирование имени cache-файла с html-кодом 
	function get_cache_filename($page_id, $field, $block_id, $mod_id, $link = "") {
		global $CONFIG, $baseurl;
		$params	= '';
		if ($_SERVER['QUERY_STRING']) {
			$params	= $_SERVER['QUERY_STRING'];
			$params	= preg_replace("/[&?]PHPSESSID=[^&]*(.*)/", "\\1", $params);
			if ($params) {
				$params	= preg_replace("/[&?](onlymain|blockborder|debug|mark|printable)[^&]*/i", "", $params);
				$params	= preg_replace("/link=[^&]*/i", "", $params);
				$params	= '-'.md5($params);
			}
		}

		$cache_filename	= $CONFIG['cache_path'].SITE_PREFIX.'/'.$link.$params.'-'.'('.$page_id.'-'.$field.'-'.$block_id.'-'.$mod_id.').html';
		$cache_filename	= str_replace('--','-',$cache_filename);
		return $cache_filename;
	}

	// получение html-кода из cache-файла 
	function get_cache_content($cache_filename) {
		GLOBAL $CONFIG, $PAGE, $main;
		$content	= '';
		if ($CONFIG['use_cache'] && $cache_filename && file_exists($cache_filename)) {
			$flm	= filemtime($cache_filename);
			if ($flm > $this->last_modified) {
				$this->last_modified = $flm;
			}
			$content	= file_get_contents($cache_filename);
		}
		return $content;
	}


	// создание cache-файла 
	function create_cache_file($cache_filename, $content = '') {
		global $CONFIG;
		if ($handle = fopen($cache_filename, 'w')) {
			if (fwrite($handle, $content)) {
				fclose($handle);
				@chmod($cache_filename, 0757);
				$this->last_modified	= time();
				return TRUE;
			}
		}
		return FALSE;
	}

	// удаление cache-файлов 
	function delete_cache_files($page_id = '') {
		global $module_id, $name, $action, $CONFIG;
		if (in_array($name, $CONFIG['no_cache_modules']) || in_array($action, $CONFIG['no_cache_actions'])) return false;
		if (is_dir($CONFIG['cache_path'].SITE_PREFIX.'/')) {
			$cache_files	= array();
			if ($dir = opendir($CONFIG['cache_path'].SITE_PREFIX.'/')) {
				while (FALSE !== ($f = readdir($dir))) {
					if ($f != '.' && $f != '..') {
						preg_match("/-\(([0-9]+)-([0-9]+)-([0-9]+)-([0-9]+)\)\.html$/", $f, $a);
						if ($a[4] == $module_id) {
							unlink($CONFIG['cache_path'].SITE_PREFIX.'/'.$f);
						}
						if ($name == 'pages' && $page_id) {
							if ($a[1] == $page_id) {
								unlink($CONFIG['cache_path'].SITE_PREFIX.'/'.$f);
							}
						}
					} else {
						next;
					}
				}
				closedir($dir);
				return TRUE;
			}
		}
		return FALSE;
	}


	// удаление всех cache-файлов 
	function delete_all_cache_files() {
		global $module_id, $name, $action, $CONFIG;
		if (!is_dir($CONFIG['cache_path'].SITE_PREFIX.'/')) return FALSE;
		if ($dir = opendir($CONFIG['cache_path'].SITE_PREFIX.'/')) {
			while (FALSE !== ($f = readdir($dir))) {
				if ($f != '.' && $f != '..') {
					@unlink($CONFIG['cache_path'].SITE_PREFIX.'/'.$f);
				} else {
					next;
				}
			}
			closedir($dir);
			return TRUE;
		}
		return FALSE;
	}
}

?>