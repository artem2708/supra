<?php
class BbCode {
	private static $REPLACEMENTS = array(
		'QUOTE' => array(
					'open'  		=> '\[QUOTE\]',
					'close' 		=> '\[/QUOTE\]',
					'open_replace'  => '<div class="quote">',
					'close_replace' => '</div>',
				   ),
		'B'    => array(
					'open'  		=> '\[B\]',
					'close' 		=> '\[/B\]',
					'open_replace'  => '<b>',
					'close_replace' => '</b>',
				   ),
		'I'    => array(
					'open'  		=> '\[I\]',
					'close' 		=> '\[/I\]',
					'open_replace'  => '<i>',
					'close_replace' => '</i>',
				   ),			   			   
		'U'    => array(
					'open'  		=> '\[U\]',
					'close' 		=> '\[/U\]',
					'open_replace'  => '<u>',
					'close_replace' => '</u>',
				   ),			   
		'CODE' => array(
					'open'  		=> '\[CODE\]',
					'close' 		=> '\[/CODE\]',
					'open_replace'  => '<code>',
					'close_replace' => '</code>',
				   ),
		'URL'  => array(
					'open'  		=> '\[URL=[ ]*([^ ]*).*\](.*)\[/URL\]', // '\[URL=(.*)\](.*)\[/URL\]'
					'close' 		=> '',
					'open_replace'  => '<a href="\\1" target="_blank">\\2</a>',
					'close_replace' => '',
				   ),			   
		'URL_EMPTY' => array(
					'open'  		=> '\[URL\]',
					'close' 		=> '\[/URL\]',
					'open_replace'  => '<a>',
					'close_replace' => '</a>',
				   ),				   			   
		'COLOR' => array(
					'open'  		=> '\[COLOR=([a-z]*)\]',
					'close' 		=> '',
					'open_replace'  => '<font color="\\1">',
					'close_replace' => '',
				   ),
		'FONT' => array(
					'open'  		=> '\[FONT=([a-z]*)\]',
					'close' 		=> '',
					'open_replace'  => '<font face="\\1">',
					'close_replace' => '',
				   ),			   
		'FONT_EMPTY' => array(
					'open'  		=> '\[FONT\]',
					'close' 		=> '\[/FONT\]',
					'open_replace'  => '<font>',
					'close_replace' => '</font>',
				   ),	
		'COLOR_EMPTY' => array(
					'open'  		=> '\[COLOR\]',
					'close' 		=> '\[/COLOR\]',
					'open_replace'  => '<font>',
					'close_replace' => '</font>',
				   ),
		'IMG'	=> array(
					'open'			=> '\[IMG=([^]]+)\]',
					'close'			=> '',
					'open_replace'  => '<img src="\\1">',
					'close_replace' => ''			
		)	
	);
	
	public static function parse($text)
	{
		foreach (self::$REPLACEMENTS as $key => $misc)
		{
			if ($misc['open']){
				$text = preg_replace("~".$misc['open']."~i", $misc['open_replace'], $text);
			}
			if ($misc['close']){
				$text = preg_replace('~'.$misc['close'].'~i', $misc['close_replace'], $text);
			}
		}
		return nl2br($text);
	}
}
?>