<?php

/**
 *
 * Абстракный класс доступа к СУРБД 
 * реализует доступ к MySQL
 *
 */
class db_mysql {
	/**
	 *
	 * Параметры подключения к БД:
	 * $host - хост
	 * $dbname - имя БД
	 * $user - логин
	 * $passwd - пароль
	 * $port - порт БД
	 * $persist - тип подключения, FALSE - неустойчивое подключение, TRUE - устойчивое подключение
	 *
	 */
	var $host;
	var $dbname;
	var $user;
	var $passwd;
	var $port;
	var $persist;

	/**
	 *
	 * Указатель на открытое соединение с MySQL
	 *
	 */
	var $resource;

	/**
	 *
	 * Указатель на результат
	 *
	 */
	var $result;	
	
	/**
	 *
	 * Метод подлючения к БД
	 *
	 */
	function db_mysql($BASE) {
		$this->host    = $BASE['db_host'];
		$this->dbname  = $BASE['db_name'];
		$this->user    = $BASE['db_user'];
		$this->passwd  = $BASE['db_password'];
		$this->port    = $BASE['db_port'];
		$this->persist = $BASE['db_persistent'];
		
		function_exists('mysql_connect')
			or  die('FATAL ERROR: MySQL support not avaiable. Please check your configuration.');
		if ($this->persist) {
			$this->resource = mysql_pconnect($this->host.':'.$this->port, $this->user, $this->passwd)
								or die('FATAL ERROR: Persistent connection to database server failed');
		} else {
			$this->resource = mysql_connect($this->host.':'.$this->port, $this->user, $this->passwd)
								or die('FATAL ERROR: Connection to database server failed');
		}
		if ($this->dbname) {
			mysql_select_db($this->dbname, $this->resource)
				or die('FATAL ERROR: Database not found '.$this->dbname);
		} else {
			die('FATAL ERROR: Database name not supplied<br>(connection to database server succesful)');
		}
	}
	
	/**
	 *
	 * Возвращает строку последней ошибки MySQL
	 *
	 */
	function db_error() {
		return mysql_error($this->resource);
	}

	/**
	 *
	 * Возвращает числовой код последней ошибки MySQL
	 *
	 */	
	function db_errno() {
		return mysql_errno($this->resource);
	}

	/**
	 *
	 * Возвращает ID, сгенерированный при последнем INSERT-запросе
	 *
	 */
	function db_insert_id() {
		return mysql_insert_id($this->resource);
	}

	/**
	 *
	 * Возвращает число затронутых последней операцией рядов
	 *
	 */
	function db_affected_rows() {
		return mysql_affected_rows($this->resource);
	}

	/**
	 *
	 * Отсылает sql-запрос $sql серверу MySQL
	 *
	 */
	function db_exec($sql) {
		$this->result = mysql_query($sql, $this->resource);
	}

	/**
	 *
	 * Освобождает память от результата запроса
	 *
	 */
	function db_free_result() {
		mysql_free_result($this->result);
	}

	/**
	 *
	 * Возвращает количество строк результата запроса
	 *
	 */
	function db_num_rows() {
		return mysql_num_rows($this->result);
	}

	/**
	 *
	 * Возвращает количество полей результата запроса
	 *
	 */
	function db_num_fields() {
		return mysql_num_fields($this->result);
	}

	/**
	 *
	 * Обрабатывает ряд результата запроса и возвращает неассоциативный массив
	 *
	 */
	function db_fetch_row() {
		return mysql_fetch_row($this->result);
	}

   /**
	 *
	 * Возвращает двумерный массив с элементами результата выполнения метода db_fetch_row()
	 *
	 */
	function db_fetch_all_row() {
		while ($result[] = $this->db_fetch_row())
		return $result;
	}
	
	/**
	 *
	 * Обрабатывает ряд результата запроса и возвращает ассоциативный массив
	 *
	 */
	function db_fetch_assoc() {
		return mysql_fetch_assoc($this->result);
	}

   /**
	 *
	 * Возвращает двумерный массив с элементами результата выполнения метода db_fetch_assoc()
	 *
	 */
	function db_fetch_all_assoc() {
		while ($result[] = $this->db_fetch_assoc())
		return $result;
	}

	/**
	 *
	 * Обрабатывает ряд результата запроса, возвращая ассоциативный и численный массивы
	 *
	 */
	function db_fetch_array() {
		return mysql_fetch_array($this->result);
	}

   /**
	 *
	 * Возвращает двумерный массив с элементами результата выполнения метода db_fetch_array()
	 *
	 */
	function db_fetch_all_array() {
		while ($result[] = $this->db_fetch_array())
		return $result;
	}
	
	/**
	 *
	 * Обрабатывает ряд результата запроса и возвращает объект
	 *
	 */
	function db_fetch_object() {
		return mysql_fetch_object($this->result);
	}

	/**
	 *
	 * Экранирует SQL спец-символы для db_exec
	 *
	 */
	function db_escape($str) {
		return (!get_magic_quotes_gpc())?mysql_real_escape_string($str):$str;
	}

	/**
	 *
	 * Экранирует специальные символы в строке, используемой в SQL-запросе, 
	 * принимимая во внимание кодировку соединения
	 * не экранирует символы % и _
	 *
	 */
	function db_real_escape($str) {
		return mysql_real_escape_string($str);
	}

	/**
	 *
	 * Возвращает версию MySQL
	 *
	 */
	function db_version() {
		$this->db_exec('SELECT VERSION()');
		if($this->resource) {
			$row = $this->db_fetch_row();
			$this->db_free_result();
			return $row[0];
		} else {
			return FALSE;
		}
	}


	/**
	 *
	 * Возвращает дату и время преобразованные из типа timestamp
	 *
	 */
	function db_unix2dateTime($time) {
		return $time > 0 ? date("Y-m-d H:i:s", $time) : null;
	}

	/**
	 *
	 * Возвращает дату и время приведённые к unix формату, типу timestamp
	 *
	 */
	function db_dateTime2unix($time) {
		if ($time == '0000-00-00 00:00:00') {
			return -1;
		}
		if(!preg_match("/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})(.?)$/", $time, $a)) {
			return -1;
		} else {
			return mktime($a[4], $a[5], $a[6], $a[2], $a[3], $a[1]);
		}
	}

	/**
	 *
	 * Возвращает список таблиц в текущей БД
	 *
	 */
	function db_tableNames() {
		$i   = 0;
		$sql = "show tables";
		$this->db_exec($sql);
		while ($info = $this->db_fetch_row()) {
			$return[$i]["table_name"]      = $info[0];
			$return[$i]["tablespace_name"] = $this->dbname;
			$return[$i]["database"]        = $this->dbname;
			$i++;
		}
		$this->db_free_result();
		return $return;
	}


//-------------------- Второй файл ----------------------------------------------//

/*
	function db_loadObject($sql, $object, $bindAll=false , $strip = true) {
		if ($object != null) {
  			$hash = array();
  			if(!$this->db_loadHash($sql)) {
   				return FALSE;
  			}
			$this->bindHashToObject($hash, $object, null, $strip, $bindAll);
			return TRUE;
 		} else {
			$cur = $this->db_exec($sql);
			$cur or exit($this->db_error());
			if ($object = $this->db_fetch_object()) {
				$this->db_free_result();
   				return TRUE;
			} else {
				$object = null;
				return FALSE;
			}
 		}
	}
*/
	
	/**
	 *
	 * Выполняет запрос $sql и возвращает либо ассоциированный массив результата 
	 * или FALSE
	 *
	 */
/*
	function db_loadHash($sql) {
		$this->db_exec($sql);
		$this->result or exit($this->db_error());
		$hash = $this->db_fetch_assoc();
		$this->db_free_result();
		if ($hash) {
			return $hash;
		} else {
			return FALSE;
		}
	}
*/

/*
	function bindHashToObject( $hash, &$obj, $prefix=NULL, $checkSlashes=true, $bindAll=false ) {
 		is_array( $hash ) or die( "bindHashToObject : hash expected" );
		is_object( $obj ) or die( "bindHashToObject : object expected" );

		if ($bindAll) {
			foreach ($hash as $k => $v) {
				$obj->$k = ($checkSlashes && get_magic_quotes_gpc()) ? stripslashes( $hash[$k] ) : $hash[$k];
			}
		} elseif ($prefix) {
			foreach (get_object_vars($obj) as $k => $v) {
				if (isset($hash[$prefix . $k ])) {
					$obj->$k = ($checkSlashes && get_magic_quotes_gpc()) ? stripslashes( $hash[$k] ) : $hash[$k];
				}
			}
		} else {
			foreach (get_object_vars($obj) as $k => $v) {
				if (isset($hash[$k])) {
					$obj->$k = ($checkSlashes && get_magic_quotes_gpc()) ? stripslashes( $hash[$k] ) : $hash[$k];
				}
			}
		}
		//echo "obj="; print_r($obj); exit;
	}
*/
	
	/**
	 *
	 * Выполняет запрос $sql
	 *
	 */
	function db_loadResult($sql) {
		$this->db_exec($sql);
		$this->result or exit($this->db_error());
		$ret = null;
		if ($row = $this->db_fetch_row()) {
			$ret = $row[0];
		}
		$this->db_free_result();
		return $ret;
	}

	/**
	 *
	 * Выполняет запрос sql и возвращает ассоциативный массив с результатом запроса
	 *
	 */
	function db_loadResult2($sql) {
		$this->db_exec($sql);
		$this->result or exit($this->db_error());
		$ret = $this->db_fetch_array();
		$this->db_free_result();
		return $ret;
	}

	/**
	 *
	 * Выполняет sql запрос и ...
	 *
	 */
/*
	function db_loadHashList($sql, $index='') {
		$this->db_exec($sql);
		$this->result or exit($this->db_error());
		$hashlist = array();
		while ($hash = $this->db_fetch_array()) {
			$hashlist[$hash[$index ? $index : 0]] = $index ? $hash : $hash[1];
		}
		$this->db_free_result();
		return $hashlist;
	}
*/
	
	/**
	 *
	 * Выполняет sql запрос и возвращает двумерный массив с количеством строк равным $maxrows
	 * строки представляют собой ассоциированный массив
	 * В случае ошибки возвращает FALSE и выводит сообщение о случившейся ошибке
	 *
	 */
	function db_loadList($sql, $maxrows=NULL) {
		$this->db_exec($sql);
		if (!$this->result) {
			echo $this->db_error();
			return FALSE;
		}
		$list = array();
		$cnt = 0;
		while ($hash = $this->db_fetch_assoc()) {
			$list[] = $hash;
			if($maxrows && $maxrows == $cnt++) {
				break;
			}
		}
		$this->db_free_result();
		return $list;
	}

	/**
	 *
	 * Выполняет sql запрос и возвращает двумерный массив с количеством строк равным $maxrows
	 * строки представляют собой массив
	 * В случае ошибки возвращает FALSE и выводит сообщение о случившейся ошибке
	 *
	 */
	function db_loadList2($sql, $maxrows=NULL) {
		$this->db_exec($sql);
		if (!$this->result) {
			echo $this->db_error();
			return FALSE;
		}
		$list = array();
		$cnt = 0;
		while ($hash = $this->db_fetch_row()) {
			$list[] = $hash[0];
			if($maxrows && $maxrows == $cnt++) {
				break;
			}
		}
		$this->db_free_result();
		return $list;
	}

	/**
	 *
	 * Тоже самое, что и db_loadList2
	 *
	 */
/*
	function db_loadColumn($sql, $maxrows=NULL) {
		$this->db_exec($sql);
		if (!$this->result) {
			echo $this->db_error();
			return FALSE;
		}
		$list = array();
		$cnt = 0;
		while ($row = $this->db_fetch_row()) {
			$list[] = $row[0];
			if($maxrows && $maxrows == $cnt++) {
				break;
			}
		}
		$this->db_free_result();
		return $list;
	}
*/
	
/*
	function db_loadObjectList($sql, $object, $maxrows = NULL) {
		$this->db_exec($sql);
		if (!$this->result) {
			die("db_loadObjectList : ".$this->db_error());
		}
		$list = array();
		$cnt = 0;
		while ($row = $this->db_fetch_array()) {
			$object->load($row[0]);
			$list[] = $object;
			if($maxrows && $maxrows == $cnt++) {
				break;
			}
		}
		$this->db_free_result();
		return $list;
	}
*/
	
/*
	function db_insertArray($table, $hash, $verbose=false) {
		$fmtsql = 'insert into '.$table.' ( %s ) values( %s )';
		foreach ($hash as $k => $v) {
			if (is_array($v) or is_object($v) or $v == NULL) {
				continue;
			}
			$fields[] = $k;
			$values[] = "'".$this->db_escape($v)."'";
		}
		$sql = sprintf($fmtsql, implode( ",", $fields ) ,implode(",", $values));
		($verbose) && print $sql."<br>\n";
		$this->db_exec($sql);
		if (!$this->result) {
			return FALSE;
		}
//		$id = $this->db_insert_id();
//		return TRUE;
		return $this->db_insert_id();
	}
*/
	
/*
	function db_updateArray($table, &$hash, $keyName, $verbose=false) {
		$fmtsql = 'UPDATE '.$table.' SET %s WHERE %s';
		foreach ($hash as $k => $v) {
			if (is_array($v) or is_object($v) or $k[0] == '_') continue;
			if($k == $keyName) {
				$where = '"'.$keyName.'="'.$this->db_escape($v)."'";
				continue;
			}
			if ($v == '') {
				$val = 'NULL';
			} else {
				$val = "'".$this->db_escape($v)."'";
			}
			$tmp[] = '"'.$k.'='.$val.'"';
		}
		$sql = sprintf($fmtsql, implode(",", $tmp), $where);
		($verbose) && print "$sql<br>\n";
		$ret = $this->db_exec($sql);
		return $ret;
	}
*/
	
	/**
	 *
	 * Удаляет из таблицы $table записи с именами $keyName имеющие значения $keyValue
	 * и возвращает $this->result
	 *
	 */
/*
	function db_delete($table, $keyName, $keyValue) {
		$keyName = $this->db_escape($keyName);
		$keyValue = $this->db_escape($keyValue);
		$this->db_exec('DELETE FROM '.$table.' WHERE '.$keyName.'="'.$keyValue.'"');
		return $this->result;
	}
*/
	
/*
	function db_insertObject($table, &$object, $keyName = NULL, $verbose=false) {
		$fmtsql = 'INSERT INTO '.$table.' ( %s ) VALUES ( %s )';
		foreach (get_object_vars($object) as $k => $v) {
			if (is_array($v) or is_object($v) or $v == NULL) {
				continue;
			}
			if ($k[0] == '_') { // internal field
				continue;
			}
			$fields[] = $k;
			$values[] = "'".$this->db_escape($v)."'";
		}
		$sql = sprintf($fmtsql, implode(",", $fields), implode(",", $values));
		($verbose) && print "$sql<br>\n";
		$this->db_exec($sql);
		if (!$this->result) {
			return FALSE;
		}
		$id = $this->db_insert_id();
		($verbose) && print "id=[".$id."]<br>\n";
		if ($keyName && $id) $object->$keyName = $id;
		return TRUE;
	}
*/
	
/*
	function db_updateObject($table, $object, $keyName, $updateNulls=true) {
		$fmtsql = 'UPDATE '.$table.' SET %s WHERE %s';
		foreach (get_object_vars($object) as $k => $v) {
			if(is_array($v) or is_object($v) or $k[0] == '_' ) {
				continue;
			}
			if($k == $keyName) {
				$where = '"'.$keyName.'="'.$this->db_escape($v)."'";
				continue;
			}
			if ($v === NULL && !$updateNulls) {
				continue;
			}
			if($v == '') {
				$val = "''";
			} else {
				$val = "'".$this->db_escape($v)."'";
			}
			$tmp[] = '"'.$k.'='.$val.'"';
		}
		$sql = sprintf( $fmtsql, implode(",", $tmp ) , $where);
		return $this->db_exec($sql);
	}
*/

	/**
	 *
	 * Преобразует текстовое представление даты $src на английском языке в метку времени Unix
	 *
	 */
	function db_dateConvert($src) {
		if ($result = strtotime($src)) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/**
	 *
	 * Форматирует текущую дату/время с учетом текущей локали
	 *
	 */
	function db_datetime($timestamp = NULL) {
		if (!$timestamp) {
			return FALSE;
		}
		if (is_object($timestamp)) {
			return $timestamp->toString('%Y-%m-%d %H:%M:%S');
		} else {
			return strftime('%Y-%m-%d %H:%M:%S', $timestamp);
		}
	}
}
?>