<?php
define("ABOTPL_ROOT", '_ROOT');

class AboTemplate {
    
    /**
     * Название шаблона
     *
     * @var string
     */
    var $tplName = '';
    
    /**
     * Содержимое шаблона.
     *  
     * @var array
     */
    var $tpl = array();
    
    /**
     * Стек вызовов
     * [ blockName: [
     *         [
     *             vars: [ '{varName}': varVal, ....]
     *             blocks: [blockName1, blockName2, ...]
     *         ]
     *     ] 
     * ]
     * @var array
     */
    var $callStack = array();
    
    /**
     * Блоки шаблона.
     *  
     * blockArray - [blockName, string | [blockArray], string | blockArray, ...  ]
     * string - текст который не содержит внутри себя блоков
     * @var array
     */
    var $blocks = array();
    
    /**
     * Массим ссылок на блоки шаблона
     *
     * @var array [blockName: [parent: 'parentBlockName', block: &blocks['blockName'] ], ... ]
     */
    var $blocksIndx = array();
    
    var $currBlock = '';
    var $inclusion = array();
    var $blockVars = array();
    var $globalVars = array();
    var $prepared = false;
    var $varPattern = '/\{[\w_\-\.]+\}/';
    
    function AboTemplate($tpl, $is_file = true)
    {
    	$this->tpl = $this->_getContent($tpl, $is_file);
    	$this->tplName = $is_file ? $tpl : '';
    }
    
    function assignInclude($block_name, $tpl, $is_file = true)
    {
    	if ($content = $this->_getContent($tpl, $is_file)) {
    	    $this->inclusion[$block_name] = $content;
    	}
    }
    
    function prepare()
    {
    	if (empty($this->tpl)) {
    	    $this->triggerError('template is empty');
    	    return;
    	}
    	$this->_clear();
    	$block_stack = array(ABOTPL_ROOT);
    	$content_size = count($this->tpl);    	
    	$this->blocks = array(ABOTPL_ROOT, '');
    	$curr_block =& $this->blocks;
    	$curr_block_val =& $curr_block[1];
    	$this->blockVars[ABOTPL_ROOT] = array();
    	$this->blocksIndx[ABOTPL_ROOT] = array('parent' => false, 'block' => &$curr_block);
    	for ($line = 0; $line < $content_size; $line++) {
    	    $str = $this->tpl[$line];
    		if (preg_match("/<\!--\s*(START|END|INCLUDE)\s+BLOCK\s*:\s*([\w-]+)\s*-->/", $str, $match)) {
    		    $block_name = $match[2];
    		    switch ($match[1]) {
    		        case 'INCLUDE':
    		            if ( isset($this->inclusion[$block_name]) && ! empty($this->inclusion[$block_name]) ) {
    		                $s = count($this->inclusion[$block_name]);
    		                $this->tpl = array_pad($this->tpl, $content_size+$s-1, '');
    		                $shift = $content_size - $line - 1;
    		                for ($i = $s, $j = 1; $j <= $shift; $i--, $j++) {
    		                    $this->tpl[$content_size + $i - 2] = $this->tpl[$content_size-$j];
    		                }
    		                for ($i = 0; $i < $s; $i++) {
    		                    $this->tpl[$line + $i] = $this->inclusion[$block_name][$i];
    		                }
    		                $content_size = count($this->tpl);
    		                $line--;
    		            }
    		            break;
    		        case 'START':
    		            if (isset($this->blocksIndx[$block_name])) {
    		                $this->triggerError("Block with the name '{$block_name}' is already used");
    		            } else {
    		                $this->blockVars[$block_name] = array();
    		                $this->blocksIndx[$block_name] = array('parent' => $curr_block[0], 'block' => false);
    		                $this->_parseBlockVars($curr_block[0], $curr_block_val);
    		                unset($curr_block_val);
    		                $curr_block[] = array($block_name, '');
    		                $curr_block =& $curr_block[count($curr_block)-1];
    		                $curr_block_val =& $curr_block[1];
    		                array_unshift($block_stack, $block_name);
    		                $this->blocksIndx[$block_name]['block'] =& $curr_block;
    		                
    		                break;
    		            }
    		            
    		        case 'END':
    		            if (!isset($this->blocksIndx[$block_name])) {
    		                $this->triggerError("Not found start of block '{$block_name}'");    		                
    		            } else if ($block_name != array_shift($block_stack) ) {
    		                $this->triggerError("Expected end of the block '{$block_name}'");
    		            } else {
    		                $this->_parseBlockVars($block_name, $curr_block_val);
    		                unset($curr_block_val);
    		                $curr_block =& $this->blocksIndx[$this->blocksIndx[$block_name]['parent']]['block'];
    		                $curr_block_val =& $curr_block[count($curr_block)];
    		            }
    		            break;
    		        default:
    		    }
    		} else {
    		    $curr_block_val .= $str."\n";
    		}
    	}
    	
    	$this->_parseBlockVars(ABOTPL_ROOT, $curr_block_val);
    	
    	if (! $this->prepared) {
    	    $this->newBlock(ABOTPL_ROOT);
    	    $this->prepared = true;
    	}
    }
    
    function newBlock($block_name)
    {
    	if ($block_name && isset($this->blocksIndx[$block_name])) {
    	    if (! isset($this->callStack[$block_name])) {
    	        $this->callStack[$block_name] = array();
    	    }
    	    $this->callStack[$block_name][] = array(
    	        'vars' => array(),
    	        'blocks' => array()
    	    );
    	    if (ABOTPL_ROOT != $block_name) {
    	        $parent_block = $this->blocksIndx[$block_name]['parent'];
    	        $this->callStack[$parent_block][ count($this->callStack[$parent_block]) - 1 ]['blocks'][] = $block_name;
    	    }
    	    $this->currBlock = $block_name;
    	} else {
    	    $this->currBlock = false;
    	}
    }
    
    function gotoBlock($block_name)
    {
    	if ($block_name) {
    	    if (! isset($this->blocksIndx[$block_name])) {
    	        $this->newBlock($block_name);
    	    } else {
    	        $this->currBlock = $block_name;
    	    }
    	}
    }
    
    function assign($var, $value = false)
    {
    	if (is_array($var)) {
    	    foreach ($var as $k => $v) {
    	    	$this->_assignVar($k, $v);
    	    }
    	} else {
    	    $this->_assignVar($var, $value);
    	}
    }
    
    function assignGlobal($var, $value = false)
    {
    	$this->globalVars['{'.$var.'}'] = $value;
    }
    
    function assign_array($block_name, $array)
    {
        if (is_array($array)) {
        	foreach ($array as $val) {
        		$this->newBlock($block_name);
        		foreach ($val as $k => $v) {
        			$this->_assignVar($k, $v);
        		}
        	}
        }
    }
	
function assignList($blok_pref = '', $list, $param = array(), $callback = false, $group = 0)
	{
		$group = (int)$group;
		$param = (array) $param;
		if (empty($list)) {
		    $this->newBlock($blok_pref.'list_empty');
		    $this->assign($param);
		} else {
		    $i = 0;
		    $group_cnt = $group > 0 ? ceil(count($list)/$group) : 0;
		    $this->newBlock($blok_pref.'list');
		    $this->assign($param);
		    $item_cnt = count($list);
		    $param['divider'] = $param['divider'] ? $param['divider'] : ',';
		    foreach ($list as $val) {
		    	if (0 == $i || ($group_cnt > 0 && 0 == $i%$group_cnt) ) {
		    		$this->newBlock($blok_pref.'list_group');
		    	}
		        $i++;
		    	$this->newBlock($blok_pref.'list_item');
		    	if ($param['divider'] && $item_cnt == $i) {
		    		$param['divider'] = '';
		    	}
		    	$assign = array_merge($val, $param);
		    	if ($callback) {
		    	    $assign = call_user_func_array($callback, array($assign));		    	    
		    	}
		    	$assign['i'] = $assign['i'] ? $assign['i'] : $i;
		    	$this->assign($assign);
		    	if ($item_cnt != $i) {
		    		$this->newBlock($blok_pref.'list_divider');
		    	}
		    }
		}
	}
    
    function printToScreen()
    {        
        $this->_parseBlock(ABOTPL_ROOT, $output);        
        //$output = preg_replace($this->varPattern, '', $output);
        echo $output;
    }
    
    function getOutputContent()
    {        
        $this->_parseBlock(ABOTPL_ROOT, $output);
        //$output = preg_replace($this->varPattern, '', $output);
        return $output;
    }
    
    function _getContent($tpl, $is_file = true)
    {
        $ret = false;
    	if ($is_file) {
    	    if (file_exists($tpl)) {
    	        $ret = file($tpl);
    	        if ($ret) {
    	            $ret = array_map('trim', $ret);
    	        }
    	    } else {
    	        $this->triggerError('templates file not found - '.$tpl);
    	    }
    	} else {
    	    $ret = explode('\n', $tpl);
    	}
    	
    	return $ret;
    }
    
    function _parseBlock($block_name, &$output)
    {
        if (isset($this->blocksIndx[$block_name]) && $this->callStack[$block_name]) {
        	if ($call = array_shift($this->callStack[$block_name]) ) {
        	    for ($i = 1, $c = count($this->blocksIndx[$block_name]['block']); $i < $c; $i++) {
        			if (is_array($this->blocksIndx[$block_name]['block'][$i])) {
        			    $child_block_name = $this->blocksIndx[$block_name]['block'][$i][0];
        			    $find = array_search($child_block_name, (array)$call['blocks']);
        			    while (false !== $find) {
        			        $this->_parseBlock($child_block_name, $output);
        			        unset($call['blocks'][$find]);
        			        $find = array_search($child_block_name, $call['blocks']);
        			    }        			    
        			} else {
        			    $a = (array) $call['vars'];
        			    if (! empty($this->globalVars)) {
        			        $a = array_merge($a, $this->globalVars);
        			    }
        			    if (! empty($this->blockVars[$block_name])) {
        			        foreach ($this->blockVars[$block_name] as $var) {
        			        	if (! isset($a[$var])) {
        			        	    $a[$var] = '';
        			        	}
        			        }
        			    }
        			    
        			    if (! empty($a)) {
        			        $output .= str_replace(
            			        array_keys($a),
            			        $a,
            			        $this->blocksIndx[$block_name]['block'][$i]
            			    );
        			    } else {
        			        $output .= $this->blocksIndx[$block_name]['block'][$i];
        			    }
        			}
        	    }
        	}
        }
    }
    
    function _assignVar($var_name, $value = false)
    {
        $p = explode('.', $var_name);
        if (count($p) > 1) {
            $block_name =  $p[0];
            $var_name = $p[1];
        } else {
            $block_name = $this->currBlock;
        }
        if ($block_name && isset($this->blocksIndx[$block_name])) {
            $this->callStack[$block_name][count($this->callStack[$block_name])-1]['vars']['{'.$var_name.'}'] = $value;
        }
    }
    
    function triggerError($msg) {
        $err = "AboTemplator error: $msg".($this->tplName ? ", in '{$this->tplName}'" : '');
        trigger_error ($err, E_USER_ERROR);
        //die($err);
    }
    
    function _clear()
    {
    	$this->blocks = array();
    	$this->blocksIndx = array();
    	$this->currBlock = '';
    }
    
    function _parseBlockVars($block_name, $contetn)
    {
    	if (preg_match_all($this->varPattern, $contetn, $match)) {
    	    foreach ($match[0] as $var) {
    	        if (!in_array($var, $this->blockVars[$block_name])) {
    	            $this->blockVars[$block_name][] = $var;
    	        }
    	    }
    	}
    }
}

class TemplatePower extends AboTemplate
{
    function TemplatePower($tpl, $is_file = true)
    {
        parent::AboTemplate($tpl, $is_file = true);
    }
}
?>