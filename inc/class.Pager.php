<?php
class Pager {
    /**
     * "Пейджер" - навигация по постраничной разбивке
     *
     * @param int $item_cnt Общее кол-во элементов
     * @param int $item_on_page Кол-во элементов на странице
     * @param int $curr_page Текущая страница
     * @param string $url 'Базовый' URL
     * @param string $block Корень в названии блоков шаблона.
     *        Если указан, наример news, то будут обработаны блоки
     *        block_news_pager, block_news_first_page и тд.
     * @param string $pager_flag Есди указан, то в ссылках будет добавлен параметр '&pager=$pager_flag'
     * @param int $link_on_page Кол-во ссылок на страницы в одном пейджере.
     *
     * @example
     *
     *
        <!-- START BLOCK : block_pager -->
        <p>
        Страница {page} из {page_count}, элементы {on_curr_page} шт. с {start_item} по {last_item}, всего - {item_count}.<br>

        <!-- START BLOCK : block_first_page -->
        <a href="{link}" title="Первая страница {page}, элементов - {on_page}, c {start_item} по {last_item}">&laquo;</a> &nbsp;
        <!-- END BLOCK : block_first_page -->

        <!-- START BLOCK : block_prev_pager -->
        <a href="{link}" title="Страница {page}, элементов - {on_page}, c {start_item} по {last_item}">-{link_on_pager}</a> &nbsp;
        <!-- END BLOCK : block_prev_pager -->

        <!-- START BLOCK : block_prev_page -->
        <a href="{link}" title="Предыдущая страница {prev_page}, элементов - {on_page}, c {start_item} по {last_item}">&larr;</a> &nbsp;
        <!-- END BLOCK : block_prev_page -->

        <!-- START BLOCK : block_pager_list -->
            <!-- START BLOCK : block_curr_page -->
            <b title="Текущая страница {page}, элементов - {on_page}, c {start_item} по {last_item}">{page}</b>|
            <!-- END BLOCK : block_curr_page -->
            <!-- START BLOCK : block_link_page -->
            <a href="{link}" title="Страница {page}, элементов - {on_page}, c {start_item} по {last_item}">{page}</a>|
            <!-- END BLOCK : block_link_page -->
        <!-- END BLOCK : block_pager_list -->

        <!-- START BLOCK : block_next_page -->
        <a href="{link}" title="Следующая страница {next_page}, элементов - {on_page}, c {start_item} по {last_item}">&rarr;</a> &nbsp;
        <!-- END BLOCK : block_next_page -->

        <!-- START BLOCK : block_next_pager -->
        <a href="{link}" title="Страница {page}, элементов - {on_page}, c {start_item} по {last_item}">+{link_on_pager}</a> &nbsp;
        <!-- END BLOCK : block_next_pager -->

        <!-- START BLOCK : block_last_page -->
        <a href="{link}" title="Последняя страница {page}, элементов - {on_page}, c {start_item} по {last_item}">&raquo;</a> &nbsp;
        <!-- END BLOCK : block_last_page -->
        </p>
        <!-- END BLOCK : block_pager -->
     */
	function Build($item_cnt, $item_on_page, $curr_page, $url, $block = '', $pager_flag = '', $link_on_page = 0)
	{   global $tpl;
    	if ($item_cnt && $item_on_page) {
    	     /* Общее кол-во страниц */
    		$page_cnt = ceil($item_cnt/$item_on_page);
    		 /* Текущая страница */
    		$curr_page = $curr_page < 1 || $curr_page > $page_cnt ? 1 : $curr_page;
    		/* Кол-во ссылок на страницы в одном пейджере */
    		$link_on_page = $link_on_page > 1 ? $link_on_page : 10; /* Кол-во ссылок на страницы */
    		/* Первая видимая страница */
    		$start = $link_on_page*floor(($curr_page-1)/$link_on_page) + 1;
    		/* Последняя видимая страница */
    		$last = min(($start + $link_on_page-1), $page_cnt);
    		/* Кол-во элементов на последней странице */
    		$on_last_page = $item_cnt - (($page_cnt-1) * $item_on_page);  /* Кол-во элементов на последней странице*/
    		/* Кол-во элементов на текущей странице */
    		$on_curr_page = $curr_page < $page_cnt ? $item_on_page : $on_last_page; /* Кол-во элементов на текущей странице*/
    		/* Кол-во пейджеров */
    		$pager_cnt = ceil($page_cnt/$link_on_page);
    		/* Текущий пейджер */
    		$curr_pager = ceil($start/$link_on_page);
    		/* Номер первого элемента на странице */
    		$start_item = ($curr_page-1)*$item_on_page;

    		$block_name = $block ? $block."_" : '';
    		$url = preg_replace("/[&\?]start=\d+/i", '', $url);
    		$pos = strrpos($url, '#');
    		if (false !== $pos) {
    		    $hash = abo_substr($url, $pos);
    		    $url = abo_substr($url, 0, $pos);
    		} else {
    		    $hash = '';
    		}
    		$i = substr($url, -1);
    		if ($i != '&' && $i != '?') {
    			$url = $url.(false === strpos($url, '?') ? '?' : '&');    			
    		}    		
    		if ($page_cnt > 1) {
    		    $assign = array(
    		        'page_count' => $page_cnt, /* Общее кол-во страниц */
    		        'item_count' => $item_cnt,  /* Общее кол-во элементов */
    		        'on_curr_page' => $on_curr_page,  /* Элементов на текущей странице */
    		        'start_page' => $start, /* Первая видимая страница */
    		        'last_page' => $last, /* Последняя видимая страница */
    		    );
    		    $tpl->newBlock('block_'.$block_name.'pager');
    		    $tpl->assign($assign + array(
    		        'page' => $curr_page, /* Текущая страница */
    		        'on_page' => $item_on_page,  /* Элементов на страницу */
    		        'start_item' => $start_item+1, /* Номер первого элемента на странице */
    		        'last_item' => $start_item + $on_curr_page, /* Номер последнего элемента на странице */
    		    ));
    			for ($i = $start; $i <= $last; $i++) {
    			    $tpl->newBlock('block_'.$block_name.'pager_list');
    			    $on_page = $i == $page_cnt ? $on_last_page : $item_on_page;
    			    $start_item = ($i-1)*$item_on_page;
    			    $arr = $assign + array(
    			        'page' => $i,
    			        'on_page' => $on_page,
    			        'start_item' => $start_item+1,
    			        'last_item' => $start_item + $on_page
    			    );
    			    if ($i == $curr_page) {
    			    	$tpl->newBlock('block_'.$block_name.'curr_page');
    			    } else {
    			        $tpl->newBlock('block_'.$block_name.'link_page');
    			        $arr['link'] = 1 == $i
        				    ? substr($url, 0, -1).$hash
        				    : $url."start=".$i.($pager_flag ? "&pager=".$pager_flag : '').$hash;
    			    }
    			    $tpl->assign($arr);
    			}

    			if ($curr_page > 1) {
    			    if ($curr_page > 2) {
        			    $start_item = ($curr_page-2)*$item_on_page;
        				$tpl->newBlock('block_'.$block_name.'prev_page');
        				$tpl->assign(array(
        				    'prev_page' => $curr_page - 1,
        				    'page' => $curr_page - 1,
        				    'on_page' => $item_on_page,
        				    'start_item' => $start_item+1,
        			        'last_item' => $start_item + $item_on_page,
            				'link' => 2 == $curr_page
            				    ? substr($url, 0, -1).$hash
            				    : $url."start=".($curr_page - 1).($pager_flag ? "&pager=".$pager_flag : '').$hash,
            			));
    			    }
        			$tpl->newBlock('block_'.$block_name.'first_page');
    				$tpl->assign(array(
    				    'page' => 1,
    				    'on_page' => $item_on_page,
    				    'start_item' => 1,
    			        'last_item' => $item_on_page,
        				'link' => substr($url, 0, -1).$hash,
        			));
    			}
    			if ($curr_page < $page_cnt) {
    			    if ($curr_page < $page_cnt - 1) {
        			    $start_item = $curr_page*$item_on_page;
        			    $on_page = $curr_page + 1 < $page_cnt ? $item_on_page : $on_last_page;
        				$tpl->newBlock('block_'.$block_name.'next_page');
        				$tpl->assign(array(
            				'next_page' => $curr_page + 1,
            				'page' => $curr_page + 1,
            				'on_page' => $on_page,
        				    'start_item' => $start_item+1,
        			        'last_item' => $start_item + $on_page,
            				'link' => $url."start=".($curr_page + 1).($pager_flag ? "&pager=".$pager_flag : '').$hash,
            			));
    			    }
        			$start_item = ($page_cnt-1)*$item_on_page;
    			    $on_page = $curr_page + 1 < $page_cnt ? $item_on_page : $on_last_page;
    				$tpl->newBlock('block_'.$block_name.'last_page');
    				$tpl->assign(array(
        				'page' => $page_cnt,
        				'on_page' => $on_last_page,
    				    'start_item' => $start_item+1,
    			        'last_item' => $start_item + $on_last_page,
        				'link' => $url."start=".$page_cnt.($pager_flag ? "&pager=".$pager_flag : '').$hash,
        			));
    			}
    			if ($curr_pager > 1) {
    			    $page = ($curr_pager-2)*$link_on_page + 1;
    				$start_item = ($page-1)*$item_on_page;
    				$tpl->newBlock('block_'.$block_name.'prev_pager');
    				$tpl->assign(array(
    				    'page' => $page,
    				    'on_page' => $item_on_page,
    				    'start_item' => $start_item+1,
    			        'last_item' => $start_item + $item_on_page,
    			        'link_on_pager' => $link_on_page,
        				'link' => 1 == $page
        				    ? substr($url, 0, -1).$hash
        				    : $url."start=".$page.($pager_flag ? "&pager=".$pager_flag : '').$hash,
        			));
    			}
    			if ($curr_pager < $pager_cnt) {
    			    $page = $curr_pager*$link_on_page + 1;
    				$start_item = ($page-1)*$item_on_page;
    			    $on_page = $page < $page_cnt ? $item_on_page : $on_last_page;
    				$tpl->newBlock('block_'.$block_name.'next_pager');
    				$tpl->assign(array(
    				    'page' => $page,
    				    'on_page' => $on_page,
    				    'start_item' => $start_item+1,
    			        'last_item' => $start_item + $on_page,
    			        'link_on_pager' => $link_on_page,
        				'link' => $url."start=".$page.($pager_flag ? "&pager=".$pager_flag : '').$hash,
        			));
    			}
    		}
    	}
	}
}
?>