<?php
function repl_entities($text)
{
	return str_replace(
	   array(
	       "€", "¢", "£", "¤", "¥", "¦",
	       "§", "©", "«", "»", "¬", "®",
	       "°", "±", "²", "³", "µ", "¶",
	       "·", "¹", "¼", "½", "×", "÷"
	   ),
	   array(
	       "&euro;", "&cent;", "&pound;", "&curren;", "&yen;", "&brvbar;",
	       "&sect;", "&copy;", "&laquo;", "&raquo;", "&not;", "&reg;",
	       "&deg;", "&plusm;", "&sup2;", "&sup3;", "&micro;", "&para;",
	       "&middot;", "&sup1;", "&frac14;", "&frac12;", "&times;", "&divide;"
	   ),
	   $text
	);
}

function arr_to_csv_line($arr, $delimiter = ';', $enclosure = '"')
{
    $line = array();
    foreach ($arr as $v) {
        if (false !== strpos($v, $delimiter) || false !== strpos($v, $enclosure) || false !== strpos($v, "\n")) {
            $v = $enclosure.str_replace($enclosure, $enclosure.$enclosure, $v).$enclosure;
        }
        $line[] = $v;
    }
    return implode($delimiter, $line);
}

function abo_strlen($str)
{
	return function_exists('mb_strlen') ? mb_strlen($str) : strlen($str);
}

function abo_strtolower($str)
{
	return function_exists('mb_strtolower') ? mb_strtolower($str) : strtolower($str);
}

function abo_strtoupper($str)
{
	return function_exists('mb_strtoupper') ? mb_strtoupper($str) : strtoupper($str);
}

function abo_substr($str, $start)
{
    $param = func_get_args();
	return call_user_func_array(function_exists('mb_substr') ? 'mb_substr' :  'substr', $param);
}

function abo_chunk_split ($str, $chunklen = 76, $end = "\r\n")
{
    $ret = "";
	for ($i = 0, $l = abo_strlen($str); $i < $l; $i++) {
	    if ($i > 0 && 0 == $i % $chunklen && $i != $l-1) {
	        $ret .= $end;
	    }
	    $ret .= abo_substr($str, $i, 1);
	}
	return $ret;
}

function abo_str_crop($str, $len = 0, $suff = null)
{   global $CONFIG;

    $len = $len > 0 ? $len : $CONFIG['str_default_crop_len'];
    $suff = is_null($suff) ? $CONFIG['str_default_crop_suff'] : $suff;
	return abo_strlen($str) > $len ? abo_substr($str, 0, $len).$suff : $str;
}

function abo_str_array_crop(&$array, $len = 0, $suff = null)
{
    if (is_array($array)) {
        foreach ($array as $key => $val) {        
        	if (is_array($val)) {
        	    abo_str_array_crop($array[$key], $len, $suff);
        	} else {
        	    $array[$key] = abo_str_crop($val, $len, $suff);
        	}
        }
    }
}

function abo_float($val)
{
	return str_replace(',', '.', floatval($val));
}

function abo_quote_encode($str)
{
	return str_replace(array('"', "'"), array('&quot;', '&#039;'), $str);
}

if (function_exists('mb_internal_encoding')) {
    mb_internal_encoding("UTF-8");
}
?>