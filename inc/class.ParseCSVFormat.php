<?php

class ParseCSVFormat {

	var $aParsingFile		= NULL;
	var $outFields			= NULL;
	var $aFileDescriptor	= NULL;
	var $aStruct			= NULL;
	var $aDefReadStrings	= 1000;
	var $aMaxLenString		= 100000;
	var $aBufferStrings		= array();
	var $aParceFields		= array();
	var $aSeparator			= ';';
	var $aCurrentTable		= NULL;
	var $aCurrentLocal		= '';

	function ParseCSVFormat($struct = NULL, $file = NULL) {
		$this->aStruct			= &$struct;
		$this->aParsingFile		= $file;
		$this->aFileDescriptor	= fopen($this->aParsingFile['tmp_name'], 'r');
		$this->aCurrentLocal = setlocale(LC_ALL, NULL);
		setlocale(LC_ALL, 'ru_RU.CP1251');
	}

	function doExportParse($table = NULL, $arr = NULL) {
		if ($table && $arr) {
			$aCurTable	= NULL;
			$struct		= &$this->aStruct;
			$marker		= $struct->getLineMarker($table);
			$separator	= $this->getSeparator();
			for ($i = 0, $j = sizeof($arr); $i < $j; $i++) {
				if ($aCurTable != $table) {
					$aTableStruct	= $struct->getTable($table);
					$aCurTable		= $table;
				}

				if ($aTableStruct['cdbtree'] && $arr[$i][$aTableStruct['key_of_table'][1]] != 0 && $arr[$i][$aTableStruct['key_of_table'][1]] != '') {
					$string	.= $marker.$separator.str_replace("\r\n", '', implode($separator, $arr[$i]))."\r\n";
				} else if (!$aTableStruct['cdbtree']) {
					$string	.= $marker.$separator.str_replace("\r\n", '', implode($separator, $arr[$i]))."\r\n";
				}
			}
			return $string;
		}
		return FALSE;
	}

	function doInputParse() {
		if ($this->aStruct) {

			$struct					= &$this->aStruct;

			if (!$this->aCurrentTable) {
				$this->aCurrentTable	= $struct->getNextTable();
				$this->aBufferStrings	= NULL;
				$this->aParceFields		= NULL;
				$this->outFields		= NULL;
				$this->readCSVFile(TRUE);
				$this->parseFields();
			} else if ($this->aCurrentTable && !feof($this->aFileDescriptor)) {
				$this->aBufferStrings	= NULL;
				$this->aParceFields		= NULL;
				$this->outFields		= NULL;
				$this->readCSVFile();
				$this->parseFields();
			} else if ($this->aCurrentTable && feof($this->aFileDescriptor)) {
				$this->aCurrentTable	= $struct->getNextTable();
				$this->aBufferStrings	= NULL;
				$this->aParceFields		= NULL;
				$this->outFields		= NULL;
				$this->readCSVFile(TRUE);
				$this->parseFields();
			}
			
			if (! $this->aCurrentTable) {
			    fclose($this->aFileDescriptor);
			    setlocale(LC_ALL, $this->aCurrentLocal);
			}
			return	$this->aCurrentTable;
		}
	}


	function parseCSVString($buffer = NULL)
	{
		$buffer = str_replace('""', '"', $buffer);
		
		$n = strlen($buffer);
		$i = 0;
		$del = false;
		$data = array();
		$str = '';
		while($i < $n) {
			$part = substr($buffer, $i);
			if ((substr($part, 0, 1) == $this->aSeparator && !$del)
			    || (substr($part, 0, 2) == '"'.$this->aSeparator && $del)
			) {
				$i++;
				if ($del) {
					$str = substr($str, 1, strlen($str) - 1);
					$i++;
				}
				$data[] = $str;
				$del = false;
				$str = '';
			} else {
				if ($part[0] == '"') {
					$del = true;
				}
				$str .= $part[0];
				$i++;
			}
		}
		if (trim($str)!='') $data[] = $str;

		return $data;
	}

	function readCSVFile($aEndOfFile = FALSE) {
		if ($this->aCurrentTable) {
			if ($aEndOfFile) {
				fseek($this->aFileDescriptor, 0);
			}
			/*
			for ($i = 0; $i < $this->aDefReadStrings && !feof($this->aFileDescriptor); $i++) {
				$buffer	= fgets($this->aFileDescriptor, $this->aMaxLenString);
				$fields = $this->parseCSVString($buffer);
				$this->aBufferStrings[]	= $fields;
				//print_r($fields); echo "<hr>";
			}
			*/
			$line = 0;
			while ($line < $this->aDefReadStrings
			       && $fields = fgetcsv($this->aFileDescriptor, $this->aMaxLenString, $this->aSeparator)
			) {
				$this->aBufferStrings[]	= $fields;
				$line++;
			}


			return TRUE;
		}
		return FALSE;
	}

	function getStruct() {
		return $this->aStruct;
	}

	function parseFields() {
		if (sizeof($this->aBufferStrings) > 0) {
			for ($i = 0, $j = sizeof($this->aBufferStrings); $i < $j; $i++) {
				if ($this->aBufferStrings[$i][0] == $this->aCurrentTable[0]['marker']) {
					$this->aParceFields[] = $this->aBufferStrings[$i];
				}
			}
			for ($i = 0, $j = sizeof($this->aParceFields); $i < $j; $i++) {
				$buffer					= NULL;
				$buffer['marker']		= $this->aCurrentTable[0]['marker'];
				$buffer['updateable']	= $this->aCurrentTable[0]['updateable'];
				$buffer['key_of_table']	= $this->aCurrentTable[0]['key_of_table'];
				$buffer['cdbtree']		= $this->aCurrentTable[0]['cdbtree'];
				$buffer['table']		= $this->aCurrentTable[1];
				for ($ii = 1, $jj = sizeof($this->aParceFields[$i]); $ii < $jj; $ii++) {
					if ($buffer['cdbtree']) {
						$buffer['fields'][$this->aCurrentTable[0]['fields'][($ii - 1)]]	= str_replace("\r\n", '', str_replace('""', '"', $this->aParceFields[$i][$ii]));
					} else {
						$buffer['fields'][$this->aCurrentTable[0]['fields'][($ii - 1)]]	= str_replace("\r\n", '', $this->aParceFields[$i][$ii]);
					}
				}
				$this->outFields[] = $buffer;
			}
			return TRUE;
		}
		return FALSE;
	}

	function getMaxLenString() {
		return $this->aMaxLenString;
	}

	function setMaxLenString($bytes = NULL) {
		if ($bytes) {
			$this->aMaxLenString = $bytes;
			return TRUE;
		}
		return FALSE;
	}

	function getDefReadStrings() {
		return $this->aDefReadStrings;
	}

	function setDefReadStrings($strings = NULL) {
		if ($strings) {
			$this->aDefReadStrings = $strings;
			return TRUE;
		}
		return FALSE;
	}

	function getSeparator() {
		return $this->aSeparator;
	}

	function setSeparator($separator = NULL) {
		if ($separator) {
			$this->aSeparator = $separator;
			return TRUE;
		}
		return FALSE;
	}
}

?>