<?php
/*##################################
#        test template class       #
##################################*/
class template {
  var $debug=false;
  var $_prg;
  var $ALL_DATA;
  var $path;
  var $ARRAY_END = array ();
  function template($PATH_TO_TEMPLATE, $type = 'file'){
    if($type == 'file'){
      if(!file_exists($PATH_TO_TEMPLATE)){
      	$this->__errorAlert('AboTemplate Error: Can\'t open [ '. $PATH_TO_TEMPLATE .' ]!');
      }
      $TEMPLATE = $this->cleanBOM($PATH_TO_TEMPLATE);
    }
     elseif ($type == 'var'){
      $TEMPLATE = $PATH_TO_TEMPLATE;
    }
     else {
      return false; 
    }
    $this->TEMPLATE = &$TEMPLATE;
    $this->ALL_DATA = null;
	$this->prepared = true;
  }
  function parse($VARS){
	$this->output = $this->parsing(is_array($VARS)?$VARS:array());
  }
  function getOutputContent(){
		return $this->output;
  }
  function parsing($VARS,$TEMPLATE=null){
    $_RETURN = $_RET = null;
    if(is_null($VARS)){return null;}
    if($this->ALL_DATA==null){$this->ALL_DATA= &$VARS;}
    if($TEMPLATE==null){$TEMPLATE= &$this->TEMPLATE;}
    $this->_prg++;

    preg_match_all (
		"#\<\!\-\-\s*START BLOCK\s*:\s*([a-zA-Z0-9_\.\-]+) \s*\-\-\>(.*)\<\!\-\-\s*END BLOCK\s*:\s*\\1 \s*\-\-\>#ism",
		$TEMPLATE,$arr, PREG_SET_ORDER
	);

    if(is_array($arr) && count($arr)>0){
      foreach($arr as $val){
        $_pref=explode(".",$val[1]);
        while(substr($_pref[0],0,5)=='_DUMP'){unset($_pref[0]);$_pref=array_values($_pref);}
        if($_pref[(count($_pref)-1)]=="REPEAT"){
          
          unset($_pref[(count($_pref)-1)]);
          if(substr($_pref[0],0,4)=='_ALL'){;unset($_pref[0]);
            $_vr="\$this->ALL_DATA['".implode("']['",$_pref)."']";
          }
           elseif($_pref[0]=='_THIS'){unset($_pref[0]);
            $_vr="\$VARS";
          }
           elseif($_pref[0]=='_PARENT'){unset($_pref[0]);
		   	$e = debug_backtrace(); array_shift($e); $t = array_shift($e);
			$NOW = $t['args'][0];unset($e,$t);
			$_vr="\$NOW['".implode("']['",$_pref)."']";
          }
           else {
            $_vr="\$VARS['".implode("']['",$_pref)."']";         
          }
          if(!$_NewVars || $_vr) @eval("\$_NewVars = $_vr;");
          if($_vr=='$VARS[\'VAL\']' || $_vr=='$VARS[\'\']'){$_NewVars=$VARS;unset($_NewVars['KEY']);}
          if(is_array($_NewVars) && count($_NewVars)>0){
           $_end = end($_NewVars); @reset($_NewVars); $this->ARRAY_END[] = true; 
           foreach($_NewVars as $K=>$V){
             if ($_end == $V){@array_pop($this->ARRAY_END); $this->ARRAY_END[] = false;}
             if(!is_array($V)){$vv=$V;unset($V);$V['VAL']=$vv;unset($vv);}
             $V['KEY']=$K;
             $_RET.=$this->parsing($V,$val[2]);
           }
           @array_pop($this->ARRAY_END);
           $TEMPLATE=str_replace($val[0],$_RET,$TEMPLATE);unset($_NewVars); $_RET = '';
          }
           else {
            $TEMPLATE=str_replace($val[0],'',$TEMPLATE);
          }
		  unset($_NewVars);
        }// end if REPEAT
         elseif($_pref[(count($_pref)-1)]=="NULL"){
          unset($_pref[(count($_pref)-1)]);
          if(substr($_pref[0],0,4)=='_ALL'){;unset($_pref[0]);
            $_vr="\$this->ALL_DATA['".implode("']['",$_pref)."']";
          }
           elseif($_pref[0]=='_PARENT'){unset($_pref[0]);
		   	$e = debug_backtrace(); array_shift($e); $t = array_shift($e);
			$NOW = $t['args'][0];unset($e,$t);
			$_vr="\$NOW['".implode("']['",$_pref)."']";
          }
           else {
            $_vr="\$VARS['".implode("']['",$_pref)."']";
          }
          if(!$_NewVars || $_vr) @eval("\$_NewVars = $_vr;");
          if(!$_NewVars){
            $TEMPLATE=str_replace($val[0],$val[2],$TEMPLATE);
          }
           else {
            $TEMPLATE=str_replace($val[0],'',$TEMPLATE); 
          }
		  unset($_NewVars);
        }
         else { // if TEMPLATE
          if(substr($_pref[0],0,4)=='_ALL'){;unset($_pref[0]);
            $_vr="\$this->ALL_DATA['".implode("']['",$_pref)."']";
          }
           elseif($_pref[0]=='_PARENT'){unset($_pref[0]);
		   	$e = debug_backtrace(); array_shift($e); $t = array_shift($e);
			$NOW = $t['args'][0];unset($e,$t);
			$_vr="\$NOW['".implode("']['",$_pref)."']";
          }
           elseif($_pref[0]=='IF_NOT_END'){
            $_vr = (end($this->ARRAY_END)==true) ? "' '" : "false";
          }
           else {
            $_vr="\$VARS['".implode("']['",$_pref)."']";         
          }
		  if(!$_NewVars || $_vr) @eval("\$_NewVars = $_vr;");
          if(is_string($_NewVars) && $_NewVars!=''){
              $temp=$_NewVars;unset($_NewVars);
              $_NewVars['VAL']=$temp;unset($temp);
          }
          
          if(is_array($_NewVars) && count($_NewVars)>0){
            $_RET .= $this->parsing($_NewVars,$val[2]);
            $TEMPLATE=str_replace($val[0], $_RET, $TEMPLATE); $_RET = '';
          }
           else {
            $TEMPLATE=str_replace($val[0], '', $TEMPLATE);
          }
		  unset($_NewVars);
        }
      }
    }//end if parsing ok
    unset($arr);
    preg_match_all ("#\{([a-zA-Z0-9_\.\-]+)\}#is",$TEMPLATE,$arr, PREG_SET_ORDER);
      if(is_array($arr) && count($arr)>0){
       foreach($arr as $val){
         $_pref=explode(".",$val[1]);
         while($_pref[0]=='_DUMP'){unset($_pref[0]);$_pref=array_values($_pref);}
         if($_pref[(count($_pref)-1)]=="INCLUDE"){
           unset($_pref[(count($_pref)-1)]);
           if(substr($_pref[0],0,4)=='_ALL'){;unset($_pref[0]);
             $_vr="\$this->ALL_DATA['".implode("']['",$_pref)."']";
           }
            elseif($_pref[0]=='_PARENT'){unset($_pref[0]);
		   	 $e = debug_backtrace(); array_shift($e); $t = array_shift($e);
			 $NOW = $t['args'][0];unset($e,$t);
			 $_vr="\$NOW['".implode("']['",$_pref)."']";
           }
            else {
             $_vr="\$VARS['".implode("']['",$_pref)."']";         
           }
           @eval("\$_inc = $_vr;");
           $_inc=str_replace('../','_',$_inc);
           $_NewVars=@file_get_contents($_inc);
         }
          elseif($_pref[(count($_pref)-1)]=="COUNT"){
            unset($_pref[(count($_pref)-1)]);
            if(substr($_pref[0],0,4)=='_ALL'){;unset($_pref[0]);
              $_vr="\$this->ALL_DATA['".implode("']['",$_pref)."']";
            }
            elseif($_pref[0]=='_PARENT'){unset($_pref[0]);
		   	  $e = debug_backtrace(); array_shift($e); $t = array_shift($e);
			  $NOW = $t['args'][0];unset($e,$t);
			  $_vr="\$NOW['".implode("']['",$_pref)."']";
            }
             else {
              $_vr="\$VARS['".implode("']['",$_pref)."']";         
            }
            @eval("\$_NewVars = $_vr;");
            if(is_array($_NewVars)){
              $_Nv=sizeof($_NewVars);$_NewVars=$_Nv;unset($_Nv);
            }
             else {
              $_NewVars=null;
            }
         }
          else {
            if(substr($_pref[0],0,4)=='_ALL'){;unset($_pref[0]);
              $_vr="\$this->ALL_DATA['".implode("']['",$_pref)."']";
            }
            elseif($_pref[0]=='_PARENT'){unset($_pref[0]);
		   	  $e = debug_backtrace(); array_shift($e); $t = array_shift($e);
			  $NOW = $t['args'][0];unset($e,$t);
			  $_vr="\$NOW['".implode("']['",$_pref)."']";
            }
             else {
              $_vr="\$VARS['".implode("']['",$_pref)."']";         
            }
            @eval("\$_NewVars = $_vr;");
            $_CHANGE["$val[0]"]=$_NewVars;
         }
         $_CHANGE["$val[0]"]=$_NewVars;
       }
       $_RETURN.=strtr($TEMPLATE,$_CHANGE);
      }
       else {
       $_RETURN=$TEMPLATE;
      }
      unset($arr);
    return $_RETURN;
  }
  function __errorAlert( $message ){
        $err = debug_backtrace();
        $i=3;while($i>0 && sizeof($err)>1){@array_shift($err); $i--;}
        list(,$e) = each ($err);
		printf("<br>%s<br>file:<b>%s</b> line: <b>%d</b>\r\n", $message, basename($e['file']), $e['line']);
  }
  function cleanBOM($filename){
		$fp = @fopen($filename, 'r');
		if(!$fp){return false;}
		$p = fread($fp,3);$rew = true;
		if(strlen($p>3)){$str = @unpack("H6", $p);if($str[1] == 'efbbbf'){$rew = false;}}
		if($rew) fseek($fp,0);
		while(!feof($fp)){
			$tmp = fgets($fp, 4096);
			if(!preg_match('~^(?:[\x00-\x7F])*$~sx',$tmp)) {$this->code($tmp);}
			$ret .= $tmp;
		}
		@fclose($fp);
		return $ret;
  }
  function code(&$str){
		if(!preg_match('~^(?:
              [\x09\x0A\x0D\x20-\x7E]            # ASCII
        	| [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
        	|  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
        	| [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
        	|  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
        	|  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
        	| [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
        	|  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
    	)*$~xs', $str)) $str = iconv('WINDOWS-1251','UTF-8', $str);
  }
}
?>