<?php

class multi_mail {
  var $from;
  var $parts;
  var $to;
  var $headers;
  var $body;
  var $bnd;
  var $_c_from;
  var $_c_to = 'iso-8859-1';
  //var $flag_body;
  //var $num;
  //var $num_emails;
  //var $exist_mail_arr;
  //var $counter;

  function multi_mail()
  {
       $this -> from = "";
       $this -> to = "";
       $this -> body = "";
       $this -> headers = Array();
       $this -> subject = "";
	   $this -> bnd = "";
	   //$this -> flag_body = 1;
	   //$this -> num;
	   //$this -> num_emails;
	   //$this -> exist_mail_arr = array();
	   //$this -> counter;
  }
  function change_encoding($from=null, $to=null){

    $from   = $from ? $from : $this->_c_from;
    $to     = $to ? $to : $this->_c_to;
    $this->subject = '=?'.strtoupper($to).
        '?B?'.base64_encode(iconv($from, $to, $this->subject)).'?=';
    $this->body = iconv($from, $to, $this->body);
  }
  function attach_file($file_name = "" ,$file_content,$encoding_type = "application/octet-stream", $charset=null)
  {
    $this -> headers[] = array(
             "name"     => $file_name,
             "content"  => $file_content,
             "encode"   => $encoding_type ,
             "charset"  => $charset,
             );
			 //echo $this->body."<br>";
  }

  function build_letter($header)
  {
    $letter = $header["content"];
    if ($header["encode"] != "text/html"):
    $letter = chunk_split(base64_encode($letter));
    $encoding = "base64";
    else:
	
    $encoding = $header["encode"];
    endif;
	//if($header["encode"] == "text/html") {
	//	echo $letter."<br>";
	//}
    return "Content-Type: ".
            $header["encode"].
           ($header["name"]? ".; name = \"".$header["name"]."\"" : "").
           ($header["charset"]? "; charset = ".$header["charset"] : ""). 
           "\nContent-Transfer-Encoding: $encoding\n\n$letter\n";
  }

  function set_multipart_mail()
  {
    $boundary = 'b'.md5(uniqid(time()));

    $multipart = "Content-Type: multipart/mixed; boundary =$boundary\n\nThis is a MIME encoded letter\n\n--$boundary";
    for($step = sizeof($this->headers)-1; $step >=0; $step--)
    {
      $multipart .= "\n".$this->build_letter($this->headers[$step])."--$boundary";
    }
	//echo $this->bnd =  $boundary;
    return $multipart .= "--\n";
  }

  function get_full_message()
  {
    $mime = "";
    if (!empty($this->from)):
       $mime .= "From: ".$this->from." \n";
    endif;

    if (!empty($this->body)){ 
		$this -> attach_file("",$this->body, "text/html", $this->_c_to);
	}
	//echo $header["encode"];
	$mime .= "MIME-Version: 1.0\n".$this->set_multipart_mail();
	$mime_clean = explode('Content-Type:  text/html', $mime, 'charset=utf-8');
	//print_r($mime_clean)."<br><br><br>";
	$num_fields = count($mime_clean);
	$mime = $mime_clean[0]." Content-Type: text/html".( !empty($header["charset"]) ? '; charset='.$header["charset"] : '')
           .$mime_clean[1].$mime_clean[$num_fields-1];
	//echo $num_fields;
		//if($this->num <= $this->num_emails){
			//echo $this->to."-".$this->body."<br>";			
			//if(in_array($this->to, $this->exist_mail_arr)) {
			
			//if($this->counter == 0) {
				
				//$this->body = "";
				//echo $this->counter;
				//$this->counter = 1;
			//}
				//$key = key($this->exist_mail_arr);
				//echo $key;
				//unset($this->exist_mail_arr[$key]);
			//}
			//$this->exist_mail_arr[] = $this->to;
			//$this->num ++;
			//$this->flag_body;	$this->body = "";
		//}		
		//print_r($this->exist_mail_arr);
    return $mime;
  }


  function send_mail()
  {
  	$this->counter = 0;
  	//$this->num_emails = $num_emails;
	$mime = $this -> get_full_message(false);
    mail($this->to, $this->subject, $this->body, $mime);
  }


}
?>
