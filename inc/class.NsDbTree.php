<?php

class NsDbTree {
	var $table;
	var $id;
	
	var $left  = 'cleft';
	var $right = 'cright';
	var $level = 'clevel';

	var $qryParams = '';
	var $qryFields = '';
	var $qryTables = '';
	var $qryWhere = '';
	var $qryGroupBy = '';
	var $qryHaving = '';
	var $qryOrderBy = '';
	var $qryLimit = '';
	var $sqlNeedReset = true;
	var $sql;
	var $db = false;

	function NsDbTree($tableName, $itemId, $fieldNames=array())
	{   static $_db = false;
	    global $db;
	    
		if (empty($tableName)) {
		    trigger_error("NSTree: Unknown table", E_USER_ERROR);
		}
		if (empty($itemId)) {
		    trigger_error("NSTree: Unknown ID column", E_USER_ERROR);
		}
		$this->table = $tableName;
		$this->id = $itemId;
		if (is_array($fieldNames)) {
		    foreach($fieldNames as $k => $v) {
		        $this->$k = $v;
		    }
		}
		if (!$_db) {
		    $_db = new My_Sql();
		    $_db->Database = $db->Database;
            $_db->Host = $db->Host;
            $_db->User = $db->User;
            $_db->Password = $db->Password;
		}
		$this->db = $_db;
	}

	function getElementInfo($ID)
	{;
		return $this->getNodeInfo($ID);
	}
	
	function getNodeInfo($ID)
	{
	    $this->sql = 'SELECT '.$this->left.','.$this->right.','.$this->level.' FROM '.$this->table.' WHERE '.$this->id.'=\''.$ID.'\'';
		$this->db->query($this->sql);
		if ( $this->db->next_record() ) {
		    return array(
		        (int)$this->db->Record[$this->left],
		        (int)$this->db->Record[$this->right],
		        (int)$this->db->Record[$this->level]
		    );
		} else {
			return false;
		}
	}

	function clear($data=array())
	{;
	
	    $this->db->query('TRUNCATE '.$this->table);

		if (sizeof($data)) {
			$fld_names = implode(',', array_keys($data)).',';
			if (sizeof($data)) {
			    $fld_values = '\''.implode('\',\'', array_values($data)).'\',';
			}
		}
		$fld_names .= $this->left.','.$this->right.','.$this->level;
		$fld_values .= '1,2,0';
		$this->sql = 'REPLACE INTO '.$this->table.'('.$fld_names.') VALUES('.$fld_values.')';
		$this->db->query($this->sql);

		return $this->db->lid();
	}

	function update($ID, $data)
	{
		$sql_set = '';
		foreach($data as $k=>$v){
		    $sql_arr[$k] = (is_string($v)) ? sprintf("`%s`='%s'",$k, $v) 
		        : ( is_null($v) ? "`{$k}`=NULL" : "`{$k}`=$v");
        }
        $this->db->query("select `{$this->id}` from {$this->table}  WHERE `{$this->id}`={$ID}");
		if ($this->db->next_record()){
			$this->db->query("UPDATE {$this->table} SET ".implode(' , ',$sql_arr)." WHERE `{$this->id}`={$ID}");
			return true;
		} else {
		 	$sql_arr[$this->id] = "`{$this->id}`={$ID}";
			$this->db->query("INSERT INTO {$this->table} SET ".implode(' , ',$sql_arr) );
			return $this->db->lid();
		}
	}

	function insert($ID, $data)
	{
	    if (!$inf = $this->getNodeInfo($ID)) {
	        return false;
	    }
		list($leftId, $rightId, $level) = $inf;
        unset($data['']);
		if(sizeof($data)) {
			$fld_names = implode(',', array_keys($data)).',';
			$fld_values = '\''.implode('\',\'', array_values($data)).'\',';
		}
		$fld_names .= $this->left.','.$this->right.','.$this->level;
		$fld_values .= ($rightId).','.($rightId+1).','.($level+1);

		if ($ID) {
			$this->sql = 'UPDATE '.$this->table.' SET '
				. $this->left.'=IF('.$this->left.'>'.$rightId.','.$this->left.'+2,'.$this->left.'),'
				. $this->right.'=IF('.$this->right.'>='.$rightId.','.$this->right.'+2,'.$this->right.')'
				. 'WHERE '.$this->right.'>='.$rightId;
			$this->db->query($this->sql);
		}

		$this->sql = 'REPLACE INTO '.$this->table.'('.$fld_names.') VALUES('.$fld_values.')';
		$this->db->query($this->sql);

		return $this->db->lid();
	}

	function insertNear($ID, $data)
	{
	    if (!$inf = $this->getNodeInfo($ID)) {
	        return false;
	    }
		list($leftId, $rightId, $level) = $inf;
		unset($data['']);
		if(sizeof($data)) {
			$fld_names = implode(',', array_keys($data)).',';
			$fld_values = '\''.implode('\',\'', array_values($data)).'\',';
		}
		$fld_names .= $this->left.','.$this->right.','.$this->level;
		$fld_values .= ($rightId+1).','.($rightId+2).','.($level);

		if($ID) {
			$this->sql = 'UPDATE '.$this->table.' SET '.$this->left.'=IF('.$this->left.'>'.$rightId.','.$this->left.'+2,'.$this->left.'),'
			    .$this->right.'=IF('.$this->right.'>'.$rightId.','.$this->right.'+2,'.$this->right.')'
			    . 'WHERE '.$this->right.'>'.$rightId;
			$this->db->query($this->sql);
		}

		$this->sql = 'REPLACE INTO '.$this->table.'('.$fld_names.') VALUES('.$fld_values.')';
		$this->db->query($this->sql);

		return $this->db->lid();
	}
	
	function moveAll($ID, $newParentId)
	{
	    if (!$inf = $this->getNodeInfo($ID)) {
	       return false;
	    }
		list($leftId, $rightId, $level) = $inf;
		if (!$inf = $this->getNodeInfo($newParentId)) {
	       return false;
	    }
		list($leftIdP, $rightIdP, $levelP) = $inf;
		
		if ($ID == $newParentId || $leftId == $leftIdP || ($leftIdP >= $leftId && $leftIdP <= $rightId)) {
		    return false;
		}
		
		if ($leftIdP < $leftId && $rightIdP > $rightId && $levelP < $level - 1 ) {
		    $this->sql = 'UPDATE '.$this->table.' SET '
                . $this->level.'=IF('.$this->left.' BETWEEN '.$leftId.' AND '.$rightId.', '.$this->level.sprintf('%+d', -($level-1)+$levelP).', '.$this->level.'), '
                . $this->right.'=IF('.$this->right.' BETWEEN '.($rightId+1).' AND '.($rightIdP-1).', '.$this->right.'-'.($rightId-$leftId+1).', '
                .'IF('.$this->left.' BETWEEN '.($leftId).' AND '.($rightId).', '.$this->right.'+'.((($rightIdP-$rightId-$level+$levelP)/2)*2 + $level - $levelP - 1).', '.$this->right.')),  '
                . $this->left.'=IF('.$this->left.' BETWEEN '.($rightId+1).' AND '.($rightIdP-1).', '.$this->left.'-'.($rightId-$leftId+1).', '
                .'IF('.$this->left.' BETWEEN '.$leftId.' AND '.($rightId).', '.$this->left.'+'.((($rightIdP-$rightId-$level+$levelP)/2)*2 + $level - $levelP - 1).', '.$this->left. ')) '
                . 'WHERE '.$this->left.' BETWEEN '.($leftIdP+1).' AND '.($rightIdP-1);
		} elseif($leftIdP < $leftId) {
		    $this->sql = 'UPDATE '.$this->table.' SET '
                . $this->level.'=IF('.$this->left.' BETWEEN '.$leftId.' AND '.$rightId.', '.$this->level.sprintf('%+d', -($level-1)+$levelP).', '.$this->level.'), '
                . $this->left.'=IF('.$this->left.' BETWEEN '.$rightIdP.' AND '.($leftId-1).', '.$this->left.'+'.($rightId-$leftId+1).', '
                . 'IF('.$this->left.' BETWEEN '.$leftId.' AND '.$rightId.', '.$this->left.'-'.($leftId-$rightIdP).', '.$this->left.') '
                . '), '
                . $this->right.'=IF('.$this->right.' BETWEEN '.$rightIdP.' AND '.$leftId.', '.$this->right.'+'.($rightId-$leftId+1).', '
                . 'IF('.$this->right.' BETWEEN '.$leftId.' AND '.$rightId.', '.$this->right.'-'.($leftId-$rightIdP).', '.$this->right.') '
                . ') WHERE '.$this->left.' BETWEEN '.$leftIdP.' AND '.$rightId
                .' OR '.$this->right.' BETWEEN '.$leftIdP.' AND '.$rightId;
		} else {
		    $this->sql = 'UPDATE '.$this->table.' SET '
                . $this->level.'=IF('.$this->left.' BETWEEN '.$leftId.' AND '.$rightId.', '.$this->level.sprintf('%+d', -($level-1)+$levelP).', '.$this->level.'), '
                . $this->left.'=IF('.$this->left.' BETWEEN '.$rightId.' AND '.$rightIdP.', '.$this->left.'-'.($rightId-$leftId+1).', '
                   . 'IF('.$this->left.' BETWEEN '.$leftId.' AND '.$rightId.', '.$this->left.'+'.($rightIdP-1-$rightId).', '.$this->left.')'
                . '), '
                . $this->right.'=IF('.$this->right.' BETWEEN '.($rightId+1).' AND '.($rightIdP-1).', '.$this->right.'-'.($rightId-$leftId+1).', '
                   . 'IF('.$this->right.' BETWEEN '.$leftId.' AND '.$rightId.', '.$this->right.'+'.($rightIdP-1-$rightId).', '.$this->right.') '
                . ') WHERE '.$this->left.' BETWEEN '.$leftId.' AND '.$rightIdP
                . ' OR '.$this->right.' BETWEEN '.$leftId.' AND '.$rightIdP;
		}
		$this->db->query($this->sql);
		return true;
	}

	function delete($ID)
	{
	    if (!$inf = $this->getNodeInfo($ID)) {
	        return false;
	    }
	    list($leftId, $rightId, $level) = $inf;
	    $this->sql = 'DELETE FROM '.$this->table.' WHERE '.$this->id.'=\''.$ID.'\'';
	    $this->db->query($this->sql);
	    
	    $this->sql = 'UPDATE '.$this->table.' SET '
	        . $this->left.'=IF('.$this->left.' BETWEEN '.$leftId.' AND '.$rightId.','.$this->left.'-1,'.$this->left.'),'
            . $this->right.'=IF('.$this->right.' BETWEEN '.$leftId.' AND '.$rightId.','.$this->right.'-1,'.$this->right.'),'
            . $this->level.'=IF('.$this->left.' BETWEEN '.$leftId.' AND '.$rightId.','.$this->level.'-1,'.$this->level.'),'
            . $this->left.'=IF('.$this->left.'>'.$rightId.','.$this->left.'-2,'.$this->left.'),'
            . $this->right.'=IF('.$this->right.'>'.$rightId.','.$this->right.'-2,'.$this->right.') '
            . 'WHERE '.$this->right.'>'.$leftId;
        $this->db->query($this->sql);
        return true;
	}
	
	function deleteAll($ID)
	{
	    if (!$inf = $this->getNodeInfo($ID)) {
	        return false;
	    }
	    list($leftId, $rightId, $level) = $inf;
	    $this->sql = 'DELETE FROM '.$this->table.' WHERE '.$this->left.' BETWEEN '.$leftId.' AND '.$rightId;
	    $this->db->query($this->sql);
	    
	    $deltaId = ($rightId - $leftId)+1;
	    $this->sql = 'UPDATE '.$this->table.' SET '
	        . $this->left.'=IF('.$this->left.'>'.$leftId.','.$this->left.'-'.$deltaId.','.$this->left.'),'
	        . $this->right.'=IF('.$this->right.'>'.$leftId.','.$this->right.'-'.$deltaId.','.$this->right.') '
	        . 'WHERE '.$this->right.'>'.$rightId;
	    $this->db->query($this->sql);
	    return true;
    }
    
    function enumChildrenAll($ID)
    {
        return $this->enumChildren($ID, 1, 0);
	}
	
	function enumChildren($ID, $start_level=1, $end_level=1)
	{
	    if ($start_level < 0) {
	        return false;
	    }

		$whereSql1 = ' AND '.$this->table.'.'.$this->level;
		$whereSql2 = '_'.$this->table.'.'.$this->level.'+';

		if (!$end_level) {
			$whereSql = $whereSql1.'>='.$whereSql2.(int)$start_level;
		} else {
			$whereSql = ($end_level <= $start_level) ? $whereSql1.'='.$whereSql2.(int)$start_level : ' AND '.$this->table.'.'.$this->level.' BETWEEN _'.$this->table.'.'.$this->level.'+'.(int)$start_level.' AND _'.$this->table.'.'.$this->level.'+'.(int)$end_level;
		}

		$this->sql = $this->sqlComposeSelect(array(
			'', // Params
			'', // Fields
			$this->table.' _'.$this->table.', '.$this->table, // Tables
			'_'.$this->table.'.'.$this->id.'=\''.$ID.'\''
				.' AND '.$this->table.'.'.$this->left.' BETWEEN _'.$this->table.'.'.$this->left.' AND _'.$this->table.'.'.$this->right
				.$whereSql
		));
		return $this->db->getFstRow($this->sql);
	}

	function enumPath($ID, $showRoot=false)
	{
		$this->sql = $this->sqlComposeSelect(array(
			'', // Params
			'', // Fields
			$this->table, // Tables
			$this->table.'.'.$this->id.'=\''.$ID.'\''
				.' AND '.$this->table.'.'.$this->left.' BETWEEN '.$this->table.'.'.$this->left.' AND '.$this->table.'.'.$this->right
				.(($showRoot) ? '' : ' AND '.$this->table.'.'.$this->level.'>0'), // Where
			'', // GroupBy
			'', // Having
			$this->table.'.'.$this->left // OrderBy
		));
		$this->db->query($this->sql);
		return $this->db->Record;
	}
	
	function getParent($ID, $level=1)
	{
		if($level < 1) {
		    return false;
		}

		$this->sql = $this->sqlComposeSelect(array(
			'', // Params
			'', // Fields
			$this->table.' _'.$this->table.', '.$this->table, // Tables
			'_'.$this->table.'.'.$this->id.'=\''.$ID.'\''
				.' AND _'.$this->table.'.'.$this->left.' BETWEEN '.$this->table.'.'.$this->left.' AND '.$this->table.'.'.$this->right
				.' AND '.$this->table.'.'.$this->level.'=_'.$this->table.'.'.$this->level.'-'.(int)$level // Where
		));

		$this->db->query($this->sql);
		return $this->db->next_record() ? $this->db->Record : false;
	}

	function sqlReset()
	{
		
		$this->qryParams = ''; $this->qryFields = ''; $this->qryTables = '';
		$this->qryWhere = ''; $this->qryGroupBy = ''; $this->qryHaving = '';
		$this->qryOrderBy = ''; $this->qryLimit = '';
		return true;
	}

	function sqlSetReset($resetMode)
	{
	    $this->sqlNeedReset = ($resetMode) ? true : false;
	}

//************************************************************************
	function sqlParams($param='') { return (empty($param)) ? $this->qryParams : $this->qryParams = $param; }
	function sqlFields($param='') { return (empty($param)) ? $this->qryFields : $this->qryFields = $param; }
	function sqlSelect($param='') { return $this->sqlFields($param); }
	function sqlTables($param='') { return (empty($param)) ? $this->qryTables : $this->qryTables = $param; }
	function sqlFrom($param='') { return $this->sqlTables($param); }
	function sqlWhere($param='') { return (empty($param)) ? $this->qryWhere : $this->qryWhere = $param; }
	function sqlGroupBy($param='') { return (empty($param)) ? $this->qryGroupBy : $this->qryGroupBy = $param; }
	function sqlHaving($param='') { return (empty($param)) ? $this->qryHaving : $this->qryHaving = $param; }
	function sqlOrderBy($param='') { return (empty($param)) ? $this->qryOrderBy : $this->qryOrderBy = $param; }
	function sqlLimit($param='') { return (empty($param)) ? $this->qryLimit : $this->qryLimit = $param; }

//************************************************************************
	function sqlComposeSelect($arSql)
	{
	
		$joinTypes = array('join'=>1, 'cross'=>1, 'inner'=>1, 'straight'=>1, 'left'=>1, 'natural'=>1, 'right'=>1);

		$this->sql = 'SELECT '.$arSql[0].' ';
		if (!empty($this->qryParams)) {
		    $this->sql .= $this->sqlParams.' ';
		}

		if (empty($arSql[1]) && empty($this->qryFields)) {
		    $this->sql .= $this->table.'.'.$this->id;
		} else {
			if (!empty($arSql[1])) {
			    $this->sql .= $arSql[1];
			}
			if (!empty($this->qryFields)) {
			    $this->sql .= ((empty($arSql[1])) ? '' : ',') . $this->qryFields;
			}
		}
		$this->sql .= ' FROM ';
		$isJoin = ($tblAr=explode(' ',trim($this->qryTables))) && ($joinTypes[strtolower($tblAr[0])]);
		if (empty($arSql[2]) && empty($this->qryTables)) {
		    $this->sql .= $this->table;
		} else {
			if (!empty($arSql[2])) {
			    $this->sql .= $arSql[2];
			}
			if (!empty($this->qryTables)) {
				if (!empty($arSql[2])) {
				    $this->sql .= (($isJoin)?' ':',');
				} elseif ($isJoin) {
				    $this->sql .= $this->table.' ';
				}
				$this->sql .= $this->qryTables;
			}
		}
		if ((!empty($arSql[3])) || (!empty($this->qryWhere))) {
			$this->sql .= ' WHERE ' . $arSql[3] . ' ';
			if (!empty($this->qryWhere)) {
			    $this->sql .= (empty($arSql[3])) ? $this->qryWhere : 'AND('.$this->qryWhere.')';
			}
		}
		if ((!empty($arSql[4])) || (!empty($this->qryGroupBy))) {
			$this->sql .= ' GROUP BY ' . $arSql[4] . ' ';
			if (!empty($this->qryGroupBy)) {
			    $this->sql .= (empty($arSql[4])) ? $this->qryGroupBy : ','.$this->qryGroupBy;
			}
		}
		if ((!empty($arSql[5])) || (!empty($this->qryHaving))) {
			$this->sql .= ' HAVING ' . $arSql[5] . ' ';
			if (!empty($this->qryHaving)) {
			    $this->sql .= (empty($arSql[5])) ? $this->qryHaving : 'AND('.$this->qryHaving.')';
			}
		}
		if ((!empty($arSql[6])) || (!empty($this->qryOrderBy))) {
			$this->sql .= ' ORDER BY ' . $arSql[6] . ' ';
			if (!empty($this->qryOrderBy)) {
			    $this->sql .= (empty($arSql[6])) ? $this->qryOrderBy : ','.$this->qryOrderBy;
			}
		}
		if (!empty($arSql[7])) {
		    $this->sql .= ' LIMIT '.$arSql[7];
		} elseif (!empty($this->qryLimit)) {
		    $this->sql .= ' LIMIT '.$this->qryLimit;
		}

		if ($this->sqlNeedReset) {
		    $this->sqlReset();
		}

		return $this->sql;
	}

//	Recalculate lefts, rights, and levels of the tree with ids and parent_ids
//	Denis Shumeev, 29.12.2006
	function getNodeLeft($id = 0)
	{
		$this->db->query("SELECT " . $this->left . " FROM " . $this->table . " WHERE " . $this->id . " = " . $id . " LIMIT 1");
		return $this->db->next_record() ? (int)$this->db->Record[$this->left] : false;
	}

	function getOffsprings($parent_id = 0, $offsprings = Array())
	{
		if (!isset($this->element[$parent_id]) || !is_array($this->element[$parent_id])) {
		    return $offsprings;
		}
		foreach ($this->element[$parent_id] as $id) {
			$offsprings[] = $id;
			$offsprings = $this->getOffsprings($id, $offsprings);
		}
		return $offsprings;
	}

	function fixNode($parent_id = 0, $level = 0)
	{
		if (!isset($this->element[$parent_id]) || !is_array($this->element[$parent_id])) {
		    return 0;
		}
		$right = $this->getNodeLeft($parent_id);
		foreach($this->element[$parent_id] as $id) {
			$left = $right + 1;
			$offsprings = $this->getOffsprings($id);
			$right = $left + sizeof($offsprings) * 2 + 1;
			$sql = "UPDATE " . $this->table . " SET " . $this->left . " = " . $left . ", " . $this->right . " = " . $right . ", " . $this->level . " = " . $level . " WHERE " . $this->id . " = " . $id . " LIMIT 1";
			$this->result_message[] = $id . " : cleft=" . $left . " cright=" . $right . " clevel=" . $level;
			$this->db->query($sql);
			$this->fixNode($id, $level + 1);
		}
	}

	function fixTree()
	{
		$this->result_message = array();
		$sql = "SELECT " . $this->id . ", " . $this->parent_id . " FROM " . $this->table;
		$this->db->query($sql);
		while ($this->db->next_record()) {
			$this->element[$this->db->Record[$this->parent_id]][$this->db->Record[$this->id]] = $this->db->Record[$this->id];
		}
		$this->fixNode(0, 0);
	}
	
	function treeReload()
	{
		$SQL = "SELECT id, clevel, cleft, cright, rank, parent_id FROM {$this->table} ".
				" ORDER BY clevel, rank, parent_id";
		$this->db->query($SQL);
		while($this->db->next_record()){
			$TREE[($this->db->Record['id'])] = !$TREE[($this->db->Record['id'])]
			    ? $this->db->Record
			    : array_merge($TREE[($this->db->Record['id'])], $this->db->Record);
			$TREE[($this->db->Record['parent_id'])]['CHILDREN'][($this->db->Record['id'])] = &$TREE[($this->db->Record['id'])];
		}
		$this->treeSet($TREE[0]['CHILDREN']);
		unset($TREE[0]);
		foreach($TREE as $v){
			$SQL = "UPDATE {$this->table} set cleft={$v['cleft']}, cright={$v['cright']}, clevel={$v['clevel']}, rank={$v['rank']} ".
				   " where id={$v['id']}";
			$this->db->query($SQL);
		}
	}
	
	function treeSet(&$TREE)
	{
		static $s, $u, $r = array();
		
		if (!$s) {
		    $s = 1;
		}
		if (!is_array($TREE) || sizeof($TREE)==0) {
		    return;
		}
		foreach ($TREE as $k => $v) {
			$r[intval($u)]++;

			$TREE[$k]['cleft'] = $s++;
			$TREE[$k]['clevel'] = (int) $u;
			if (is_array($TREE[$k]['CHILDREN'])) {
			    $u++;
			    $this->treeSet($TREE[$k]['CHILDREN']);
			    $u--;
			}
			$TREE[$k]['cright'] = $s++;
			$TREE[$k]['rank'] = (int) $r[$u];
		}
	}
}
?>