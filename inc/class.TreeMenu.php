<?php
class TreeMenu
{
    var $items;

    function TreeMenu()
    {
        // Not much to do here :(
    }

    /**
    * This function adds an item to the the tree.
    *
    * @access public
    * @param  object $node TreeNode object.
    * @return object       Returns a reference to the new node inside the tree.
    */
    function &addItem(&$node)
    {
        $this->items[] = &$node;
        return $this->items[count($this->items) - 1];
    }
} // TreeMenu

class TreeNode
{
    var $text;
    var $link;
    var $cssClass;
    var $linkTarget;
    var $items;
    var $parent;

    function TreeNode($options = array())
    {
        $this->text          = '';
        $this->link          = '';
        $this->cssClass      = '';
        $this->linkTarget    = null;

        $this->parent        = null;
        foreach ($options as $option => $value) {
            $this->$option = $value;
        }
    }

    /**
    * Adds a new subnode to this node.
    *
    * @access public
    * @param  object $node The new node
    */
    function &addItem(&$node)
    {
        $node->parent  = &$this;
        $this->items[] = &$node;

        return $this->items[count($this->items) - 1];
    }

} // TreeNode

class TreeMenu_DHTML
{
    var $menu;
    
    function TreeMenu_DHTML(&$structure)
    {
        $this->menu = &$structure;
    }

    function toHTML()
    {   static $count = 0;
    
        if (0 == $count) {
            $html = '<script language="javascript" type="text/javascript" src="/js/admin/tree.js"></script>'."\n";
        } else {
            $html = '';
        }
        $count++;
        $html .= "\n<div class=\"abo-tree\">\n";
        if (isset($this->menu->items)) {
            for ($i=0; $i<count($this->menu->items); $i++) {
                $html .= $this->_nodeToHTML($this->menu->items[$i]);
            }
        }
        $html .= "</div>\n";
        
        return $html;        
    }

    function _nodeToHTML($node)
    {
        static $iter = 0, $content_tpl = "
<dd class=\"content\">
{content}
</dd>
",
        $node_tpl = "
<dl class=\"{css_class}\"><dt></dt><dd>{menu_item}</dd>
{content}
</dl>
",
        $link_menu_item_tpl = "<a href=\"{link}\">{title}</a>";
        
        $content = '';
        $css_class = $node->cssClass ? array($node->cssClass) : array();
        if (! empty($node->items)) {
            foreach ($node->items as $val) {
                $iter++;
            	$content .= str_replace('{content}', $this->_nodeToHTML($val), $content_tpl);
            	$iter--;
            }
            $css_class[] = 0 == $iter ? 'open' : 'close';
        } else {
            $css_class[] = 'leaf';
        }
        if (!$node->parent || ! empty($node->parent->items)) {
            if (!$node->parent || $node == $node->parent->items[count($node->parent->items)-1]) {
                $css_class[] = 'last';
            }
        }
        return str_replace(
            array('{css_class}', '{menu_item}', '{content}'),
            array(
                empty($css_class) ? '' : implode(' ', $css_class),
                !$node->link ? $node->text : str_replace(array('{link}', '{title}'), array($node->link, $node->text), $link_menu_item_tpl),
                $content),
            $node_tpl
        );    
    }
} // End class TreeMenu_DHTML
?>