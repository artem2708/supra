<?php
class ExportData {

	var	$aFields		= NULL;
	var	$aModule		= NULL;
	var $aCurrentTable	= NULL;
	var $aOutputSteam	= NULL;

	function ExportData($fields = NULL, $module = NULL) {
			$this->aFields		= &$fields;
			$this->aModule		= $module;
	}
	
	function doExportData() {
 		if (is_object($this->aFields) && $this->aDescriptorDB) {
			$fields	= &$this->aFields;
        	$struct = $fields->getStruct();
        	$module	= $this->aModule;
        	while($table = $struct->getNextTable()) {
                $aDataArray	 = $this->getData($table);
                $string		.= $fields->doExportParse($table[1], $aDataArray);
			}
        	$file	= $this->getOutputStream().'-('.date('d-m-Y_H-i-s').').csv';
			header('Content-Type: application/x-csv; charset=win-1251');
			header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
			header('Content-Disposition: inline; filename="'.$file.'"');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			echo iconv('UTF-8', 'WINDOWS-1251//TRANSLIT', $string);
			exit;
		}
		return FALSE;
	}

	function getData($table = NULL) {
		GLOBAL $server, $lang;
		if ($this->aDescriptorDB && $table) {
			$db		= &$this->aDescriptorDB;
			$fields	= &$this->aFields;
			$cols	= implode('`,`', $table[0]['fields']);
			$arr	= $db->getArrayOfResult('SELECT `'.$cols.'` FROM '.$server.$lang.'_'.$table[1].' ORDER BY 1');
			if (is_array($arr)) {
				for ($i = 0, $j = sizeof($arr); $i < $j; $i++) {
					foreach ($arr[$i] as $k => $v) {
						if (FALSE !== strpos($arr[$i][$k], $fields->getSeparator())
						    || FALSE !== strpos($arr[$i][$k], '"')
						) {
							$arr[$i][$k]	= '"'.str_replace('"', '""', $v).'"';
						}
					}
				}
				return $arr;
			}
		}
		return FALSE;
	}

	function getOutputStream() {
		return $this->aOutpuStream;
	}
	
	function setOutputStream($aOutpuStream = NULL) {
		if ($aOutpuStream) {
			$this->aOutpuStream	= $aOutpuStream;
			return TRUE;
		}
		return FALSE;
	}
	
	function setDescriptorDB($db = NULL) {
		if ($db) {
			$this->aDescriptorDB = $db;
			return TRUE;
		}
		return FALSE;
	}
    function utf2ansii($var){
      static $table;
      if(!$table){
        $table = array(
                "\xD0\x81" => "\xA8",
                "\xD1\x91" => "\xB8"
        );
      }
      if(is_array($var)){foreach($var as $_k => $_v){$ret[$_k] = $this->utf2ansii($_v);}return $ret;}
       elseif(is_string($var)){
        $ret = preg_replace(
                '#([\xD0-\xD1])([\x80-\xBF])#se',
                'isset($table["$0"]) ? $table["$0"] : chr(ord("$2")+("$1" == "\xD0" ? 0x30 : 0x70))',
                $var
        );
        return $ret;
      }
      return $var;
    }
}

?>