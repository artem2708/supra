<?php
class JsHttpRequest
{
    var $SCRIPT_ENCODING = "UTF-8";
    var $SCRIPT_DECODE_MODE = '';
    var $LOADER = null;
    var $ID = null;    
    var $RESULT = null;
    
    // Internal; uniq value.
    var $_uniqHash;
    // Magic number for display_error checking.
    var $_magic = 14623;
    // Previous display_errors value.
    var $_prevDisplayErrors = null;    
    // Internal: response content-type depending on loader type.
    var $_contentTypes = array(
        "script" => "text/javascript",
        "xml"    => "text/plain", // In XMLHttpRequest mode we must return text/plain - stupid Opera 8.0. :(
        "form"   => "text/html",
        ""       => "text/plain", // for unknown loader
    );
    // Emergency memory buffer to be freed on memory_limit error.
    var $_emergBuffer = null;

    
    /**
     * Constructor.
     * 
     * Create new JsHttpRequest backend object and attach it
     * to script output buffer. As a result - script will always return
     * correct JavaScript code, even in case of fatal errors.
     *
     * QUERY_STRING is in form of: PHPSESSID=<sid>&a=aaa&b=bbb&JsHttpRequest=<id>-<loader>
     * where <id> is a request ID, <loader> is a loader name, <sid> - a session ID (if present), 
     * PHPSESSID - session parameter name (by default = "PHPSESSID").
     * 
     * If an object is created WITHOUT an active AJAX query, it is simply marked as
     * non-active. Use statuc method isActive() to check.
     */
    function JsHttpRequest()
    {
        global $JsHttpRequest_Active;
        
        // To be on a safe side - do not allow to drop reference counter on ob processing.
        $GLOBALS['_RESULT'] =& $this->RESULT; 
        
        // Parse QUERY_STRING.
        if (preg_match('/^(.*)(?:&|^)JsHttpRequest=(?:(\d+)-)?([^&]+)((?:&|$).*)$/s', @$_SERVER['QUERY_STRING'], $m)) {
            $this->ID = $m[2];
            $this->LOADER = strtolower($m[3]);
            $_SERVER['QUERY_STRING'] = preg_replace('/^&+|&+$/s', '', preg_replace('/(^|&)'.session_name().'=[^&]*&?/s', '&', $m[1] . $m[4]));
            unset(
                $_GET['JsHttpRequest'],
                $_REQUEST['JsHttpRequest'],
                $_GET[session_name()],
                $_POST[session_name()],
                $_REQUEST[session_name()]
            );   
            // Fill an emergency buffer. We erase it at the first line of OB processor
            // to free some memory. This memory may be used on memory_limit error.
            $this->_emergBuffer = str_repeat('a', 1024 * 200);

            // Intercept fatal errors via display_errors (seems it is the only way).     
            $this->_uniqHash = md5('JsHttpRequest' . microtime() . getmypid());
            $this->_prevDisplayErrors = ini_get('display_errors');
            ini_set('display_errors', $this->_magic); //
            ini_set('error_prepend_string', $this->_uniqHash . ini_get('error_prepend_string'));
            ini_set('error_append_string',  ini_get('error_append_string') . $this->_uniqHash);

            // Start OB handling early.
            ob_start(array(&$this, "_obHandler"));
            $JsHttpRequest_Active = true;
    
            $this->_correctSuperglobals();
    
            // Check if headers are already sent (see Content-Type library usage).
            // If true - generate a debug message and exit.
            $file = $line = null;
            $headersSent = headers_sent($file, $line);
            if ($headersSent) {
                trigger_error(
                    "HTTP headers are already sent" . ($line !== null? " in $file on line $line" : " somewhere in the script") . ". "
                    . "Possibly you have an extra space (or a newline) before the first line of the script or any library. "
                    . "Please note that JsHttpRequest uses its own Content-Type header and fails if "
                    . "this header cannot be set. See header() function documentation for more details",
                    E_USER_ERROR
                );
                exit();
            }
        } else {
            $this->ID = 0;
            $this->LOADER = 'unknown';
            $JsHttpRequest_Active = false;
        }
    }
    

    /**
     * Static function.
     * Returns true if JsHttpRequest output processor is currently active.
     * 
     * @return boolean    True if the library is active, false otherwise.
     */
    function isActive()
    {
        return !empty($GLOBALS['JsHttpRequest_Active']);
    }
    

    /**
     * string getJsCode()
     * 
     * Return JavaScript part of the library.
     */
    function getJsCode()
    {
        return file_get_contents(RP . '/js/JsHttpRequest.js');
    }


    /**
     * void setEncoding(string $encoding)
     * 
     * Set an active script encoding & correct QUERY_STRING according to it.
     * Examples:
     *   "windows-1251"          - set plain encoding (non-windows characters, 
     *                             e.g. hieroglyphs, are totally ignored)
     *   "windows-1251 entities" - set windows encoding, BUT additionally replace:
     *                             "&"         ->  "&amp;" 
     *                             hieroglyph  ->  &#XXXX; entity
     */
    function setEncoding($enc)
    {
        // Parse an encoding.
        preg_match('/^(\S*)(?:\s+(\S*))$/', $enc, $p);
        $this->SCRIPT_ENCODING    = strtolower(!empty($p[1])? $p[1] : $enc);
        $this->SCRIPT_DECODE_MODE = !empty($p[2])? $p[2] : '';
        // Manually parse QUERY_STRING because of damned Unicode's %uXXXX.
        $this->_correctSuperglobals();
    }

    
    /**
     * string quoteInput(string $input)
     * 
     * Quote a string according to the input decoding mode.
     * If entities are used (see setEncoding()), no '&' character is quoted,
     * only '"', '>' and '<' (we presume that '&' is already quoted by
     * an input reader function).
     *
     * Use this function INSTEAD of htmlspecialchars() for $_GET data 
     * in your scripts.
     */
    function quoteInput($s)
    {
        if ($this->SCRIPT_DECODE_MODE == 'entities')
            return str_replace(array('"', '<', '>'), array('&quot;', '&lt;', '&gt;'), $s);
        else
            return htmlspecialchars($s);
    }
    
        
    /**
     * Internal methods.
     */

    /**
     * Parse & decode QUERY_STRING.
     */
    function _correctSuperglobals()
    {
        // In case of FORM loader we may go to nirvana, everything is already parsed by PHP.
        if ($this->LOADER == 'form') return;
        
        // ATTENTION!!!
        // HTTP_RAW_POST_DATA is only accessible when Content-Type of POST request
        // is NOT default "application/x-www-form-urlencoded"!!!
        // Library frontend sets "application/octet-stream" for that purpose,
        // see JavaScript code. In PHP 5.2.2.HTTP_RAW_POST_DATA is not set sometimes; 
        // in such cases - read the POST data manually from the STDIN stream.
        $rawPost = strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0? (isset($GLOBALS['HTTP_RAW_POST_DATA'])? $GLOBALS['HTTP_RAW_POST_DATA'] : @file_get_contents("php://input")) : null;
        $source = array(
            '_GET' => !empty($_SERVER['QUERY_STRING'])? $_SERVER['QUERY_STRING'] : null, 
            '_POST'=> $rawPost,
        );
        foreach ($source as $dst=>$src) {
            // First correct all 2-byte entities.
            $s = preg_replace('/%(?!5B)(?!5D)([0-9a-f]{2})/si', '%u00\\1', $src);
            // Now we can use standard parse_str() with no worry!
            $data = null;
            parse_str($s, $data);
            $GLOBALS[$dst] = $this->_ucs2EntitiesDecode($data);
        }
        $GLOBALS['HTTP_GET_VARS'] = $_GET; // deprecated vars
        $GLOBALS['HTTP_POST_VARS'] = $_POST;
        $_REQUEST = 
            (isset($_COOKIE)? $_COOKIE : array()) + 
            (isset($_POST)? $_POST : array()) + 
            (isset($_GET)? $_GET : array());
        if (ini_get('register_globals')) {
            // TODO?
        }
    }


    /**
     * Called in case of error too!
     */
    function _obHandler($text)
    {
        unset($this->_emergBuffer); // free a piece of memory for memory_limit error
        unset($GLOBALS['JsHttpRequest_Active']);
        
        // Check for error & fetch a resulting data.
        if (preg_match("/{$this->_uniqHash}(.*?){$this->_uniqHash}/sx", $text, $m)) {
            if (!ini_get('display_errors') || (!$this->_prevDisplayErrors && ini_get('display_errors') == $this->_magic)) {
                // Display_errors:
                // 1. disabled manually after the library initialization, or
                // 2. was initially disabled and is not changed
                $text = str_replace($m[0], '', $text); // strip whole error message
            } else {
                $text = str_replace($this->_uniqHash, '', $text);
            }
        }
        if ($m && preg_match('/\bFatal error(<.*?>)?:/i', $m[1])) {
            // On fatal errors - force null result (generate 500 error). 
            $this->RESULT = null;
        } else {
            // Make a resulting hash.
            if (!isset($this->RESULT)) {
                global $_RESULT;
                $this->RESULT = $_RESULT;
            }
        }
        
        $encoding = $this->SCRIPT_ENCODING;
        $result = array(
            'id'   => $this->ID,
            'js'   => $this->RESULT,
            'text' => $text,
        );

        $text = json_encode($result);

        // Content-type header.
        // In XMLHttpRequest mode we must return text/plain - damned stupid Opera 8.0. :(
        $ctype = !empty($this->_contentTypes[$this->LOADER])? $this->_contentTypes[$this->LOADER] : $this->_contentTypes[''];
        header("Content-type: $ctype; charset=$encoding");
        
        if ($this->LOADER != "xml") {
            // In non-XML mode we cannot use plain JSON. So - wrap with JS function call.
            // If top.JsHttpRequestGlobal is not defined, loading is aborted and 
            // iframe is removed, so - do not call dataReady().
            $text = "" 
                . ($this->LOADER == "form"? 'top && top.JsHttpRequestGlobal && top.JsHttpRequestGlobal' : 'JsHttpRequest') 
                . ".dataReady(" . $text . ")\n"
                . "";
            if ($this->LOADER == "form") {
                $text = '<script type="text/javascript" language="JavaScript"><!--' . "\n$text" . '//--></script>';
            }
        }

        return $text;
    }
    

    /**
     * Decode all %uXXXX entities in string or array (recurrent).
     * String must not contain %XX entities - they are ignored!
     */
    function _ucs2EntitiesDecode($data)
    {
        if (is_array($data)) {
            $d = array();
            foreach ($data as $k=>$v) {
                $d[$this->_ucs2EntitiesDecode($k)] = $this->_ucs2EntitiesDecode($v);
            }
            return $d;
        } else {
            if (strpos($data, '%u') !== false) { // improve speed
                $data = preg_replace_callback('/%u([0-9A-F]{1,4})/si', array(&$this, '_ucs2EntitiesDecodeCallback'), $data);
            }
            return $data;
        }
    }


    /**
     * Decode one %uXXXX entity (RE callback).
     */
    function _ucs2EntitiesDecodeCallback($p)
    {
        $hex = $p[1];
        $dec = hexdec($hex);
        if ($dec === "38" && $this->SCRIPT_DECODE_MODE == 'entities') {
            // Process "&" separately in "entities" decode mode.
            $c = "&amp;";
        } else {
            $c = iconv('UCS-2BE', $this->SCRIPT_ENCODING, pack('n', $dec));
            if (!strlen($c)) {
                if ($this->SCRIPT_DECODE_MODE == 'entities') {
                    $c = '&#' . $dec . ';';
                } else {
                    $c = '?';
                }
            }
        }
        return $c;
    }
}
