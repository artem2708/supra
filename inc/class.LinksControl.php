<?php

/*
Класс LinksControl
Система контроля внутренних ссылок:
Если в одном из текстовых полей в базе данных есть ссылка на страницу сайта,
то она отлавливается системой контроля и заносится в так называемую "таблицу взаимосвязей" pages_internal_links.
При изменении адреса страницы системой контроля меняются все ссылки, ссылающиеся на эту страницу.
Прим. - ссылки отслеживаются только в тех полях, которых редактируются через визуальный редактор, то есть в которых предусмотрен интерфейс вставки ссылок.
*/

class LinksControl {

	var $ilinks_file = 'admin/files/{SITE_PREFIX}_internal_links.txt';

	// В конструкторе прописываем возможные варианты названия файла internal_links.txt
	function LinksControl() {
		global $CONFIG, $lang;

		if ($CONFIG['multilanguage']) {
			$this->ilinks_file = str_replace('{SITE_PREFIX}', SITE_PREFIX, $this->ilinks_file);
		} else {
			$this->ilinks_file = str_replace('{SITE_PREFIX}', SITE_PREFIX, $this->ilinks_file);
		}
	}


/**
 * Получение внутренних ссылок в виде as_page_address,
 * ссылки доступны address страницы (без query_string и начального слэша)
 */
	function get_internal_links($text = '', $as_page_address = 0) {
		global $CONFIG, $lang;
		$link_array = array();
		if ('' != $text) {
			// выкинуть из ссылок полный URL нашего сайта
			$patt = array("/http(s)?:\/\/".preg_quote($CONFIG['domen'])."/i");
			foreach ($CONFIG['site_aliases'] as $val) {
				$patt[] = "/http(s)?:\/\/".preg_quote($val)."/i";
			}
			$text = preg_replace($patt, "", $text);
			/* /<a[^>]+href\s*=\s*(\"|')([^>'\"]+)\\1[^>]*>(.*)<\/a>/i */
			if (preg_match_all("/<a[^>]+href\s*=\s*(\"|')([^>'\"]+)(\"|')[^>]*>/i", $text, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $val) {
					if (!strstr($val[2], "mailto") && !strstr($val[2], "javascript") && !strstr($val[2], "http://") && !strstr($val[2], "https://")) {
					    if ($str = strstr($val[2], "#")) {
					        $val[2] = str_replace($str, "", $val[2]);
					    }
					    // удалить данные сессии
					    $val[2] = preg_replace("/[&\?]".session_name()."=w+[&$]/", "", $val[2]);
					    $val[2] = str_replace("&amp;", "&", $val[2]);
					    // запишем в as_address адрес страницы, без query string и начального слэша
					    $as_address = str_replace(strstr($val[2], "?"), "", $val[2]);
					    if ("/" == $as_address{0}) {
					        $as_address = substr($as_address, 1);
					    }
					    $link = ($as_page_address) ? $as_address : $val[2];
					    if ('index.php' == $as_address || !file_exists($as_address)) {
					        $link_array[] = $link;
					    }
					}
				}
			}
		}
		return $link_array;
	}


	// Функция для занесения ссылок в таблицу взаимосвязей pages_internal_links.
	// Принимает параметры $module_name - имя модуля, $table_name - имя таблицы,
	// где находится запись содержащая ссылки, $field_name - имя поля в таблице,
	// $entry_id - идентификатор записи, $text - собственно текст с ссылками.
	function add_internal_links($module_name, $table_name, $field_name, $entry_id, $description = "", $text = "") {
		global $CONFIG, $lang, $server, $db;
		if ("" == $module_name || "" == $table_name || "" == $field_name || "" == $text) return 0;
		$entry_id = intval($entry_id);
		if (!$entry_id) return 0;
		$link_array = array();
		// удалим все старые внутренние ссылки, связанные с этой записью
		$this->delete_internal_links($module_name, $table_name, $entry_id);
		// получим массив внутренних ссылок на странице
		$link_array = $this->get_internal_links($text, 1);
		if (count($link_array) > 0) {
			$module_name	= $db->escape($module_name);
			$table_name		= $db->escape($table_name);
			$field_name		= $db->escape($field_name);
			$description	= $db->escape($description);
			foreach ($link_array as $link) {
				// удалить из ссылки языковую версию сайта, если включена многоязыковая поддержка
				if ($CONFIG["multilanguage"]) $link = preg_replace("/^$lang\//i", "", $link);
				$link = $db->escape($link);
				// проверим, существует ли такая страница
				$query = "SELECT * FROM ".$server.$lang."_pages WHERE address = '$link'";
				$ret = db_loadResult2($query);
				if (count($ret) > 0) {
					$page_id = $ret["id"];
					$status = "200";
				} else {
					$page_id = "NULL";
					$status = "404";
				}
				$query = "INSERT INTO ".$server.$lang."_pages_internal_links (module_name, table_name, field_name, entry_id, page_id, link, description, status) VALUES ('$module_name', '$table_name', '$field_name', $entry_id, $page_id, '$link', '$description', $status)";
				db_exec($query);
			}
			return TRUE;
		}
		return FALSE;
	}


	// функция для замены всех внутренних ссылок в тексте на глобальные, содержащие домен сайта
	// (функция написана, предполагая, что будет использоваться rewrite_mod)
	function replace_internal_links_to_global($text = "") {
		global $CONFIG, $lang;
		// получим массив внутренних ссылок на странице
		$link_array = $this->get_internal_links($text, 1);
		if (count($link_array) > 0) {
			foreach ($link_array as $link) {
				// удалить из ссылки языковую версию сайта, если включена многоязыковая поддержка
				if ($CONFIG["multilanguage"]) $link = preg_replace("/^$lang\//i", "", $link);
				$text = str_replace("/" . $link, "{$CONFIG["web_address"]}$link", $text);
			}
		}

		return $text;
	}


	// функция для обновления ссылок по $page_id в таблице взаимосвязей pages_internal_links
	// используется в модуле pages при привязке и отвязке страницы, так как именно при этих
	// действиях меняется address страницы, и ссылки, на неё ведущие, тоже должны измениться
	function update_internal_links($page_id) {
		global $CONFIG, $lang, $server, $db;
		if ($ret = $this->get_page_by_id($page_id)) {
			$page_address = $ret["address"];
			$this->update_entries_related_with_page($page_id, $page_address);
			// помимо внутренних ссылок в БД есть еще файл с внутренними ссылками, который тоже надоть обновить
			$this->update_internal_links_in_file($page_id, $page_address);
			$page_address = $db->escape($page_address);
			$query = "UPDATE ".$server.$lang."_pages_internal_links SET link = '$page_address' WHERE page_id = $page_id";
			db_exec($query);
			$query = "UPDATE ".$server.$lang."_pages_internal_links SET status = 200 WHERE link = '$page_address' AND status = 404";
			db_exec($query);
			return TRUE;
		}
		return FALSE;
	}


	// Функция для изменения статуса и page_id ссылок при удалении страницы из БД.
	// Эта функция также используется при отключении публикации страницы:
	// при отключении публикации ссылки, ведущие на страницу, получают статус 404
	function change_internal_link_status_to_404($page_id, $action = "") {
		global $CONFIG, $lang, $server;
		$page_id = intval($page_id);
		if (!$page_id) return 0;
		if ($action == "delete") $page_id_str = ", page_id = NULL";
		$query = "UPDATE ".$server.$lang."_pages_internal_links SET status = '404' $page_id_str WHERE page_id = $page_id";
		if (db_exec($query)) {
			return 1;
		} else {
			return 0;
		}
	}


	// Функция для изменения статуса и page_id битых ссылок,
	// которые были сгенерированы раньше, чем добавлена сама страница.
	// Таким образом ссылки в документах можно добавлять раньше,
	// чем будет создана страница, куда ведут эти ссылки.
	// Эта функция также используется при включении публикации страницы:
	// при включении публикации ссылки, ведущие на страницу, получают статус 200
	function change_internal_link_status_to_200($page_id, $action = "", $page_link = "") {
		global $CONFIG, $lang, $server, $db;
		$page_id = intval($page_id);
		if (!$page_id) return 0;
		if ($action == "add") {
			$page_link = $db->escape($page_link) . "/";
			$where_str = "link = '$page_link'";
		} else {
			$where_str = "page_id = '$page_id'";
		}
		$query = "UPDATE ".$server.$lang."_pages_internal_links SET status = '200', page_id = $page_id WHERE $where_str AND status = 404";
		if (db_exec($query)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


	// функция для обновления ссылок на страницу $page_id
	// в записях БД, содержащих эти ссылки
	function update_entries_related_with_page($page_id, $page_address) {
		global $CONFIG, $lang, $server, $db;

		$query = "SELECT * FROM ".$server.$lang."_pages_internal_links WHERE page_id = $page_id";
		$temp = db_loadList($query);

		if (count($temp) > 0) {
			foreach ($temp as $idx => $v) {
				$module_name	= $v["module_name"];
				$table_name		= $v["table_name"];
				$field_name		= $v["field_name"];
				$entry_id		= $v["entry_id"];
				$link			= $v["link"];
				$status			= $v["status"];

				if ($table_name && $field_name && $entry_id) {
					$query = "SELECT $field_name FROM $table_name WHERE id = $entry_id";
					$temp2 = db_loadResult2($query);
					if (count($temp2) > 0) {
						$edit_text = $temp2["$field_name"];
						$edit_text = str_replace($link, $page_address, $edit_text);
						$edit_text = $db->escape($edit_text);
						$query = "UPDATE $table_name SET $field_name = '$edit_text' WHERE id = $entry_id";
						db_exec($query);
					}
				}
			}
			return 1;
		}
		return 0;
	}


	// функция для получения страницы по id
	function get_page_by_id($id) {
		global $CONFIG, $lang, $server;
		$id = intval($id);
		if (!$id) return 0;
		$query = "SELECT * FROM ".$server.$lang."_pages WHERE id = $id";
		$ret = db_loadResult2($query);
		return $ret;
	}


	// функция для удаления внутренних ссылок по entry_id
	function delete_internal_links($module_name, $table_name, $entry_id = "") {
		global $CONFIG, $lang, $server, $db;
		if ("" == $module_name || "" == $table_name) return 0;
		$entry_id = intval($entry_id);
		if (!$entry_id) return 0;
		$module_name	= $db->escape($module_name);
		$table_name		= $db->escape($table_name);
		$query = "DELETE FROM ".$server.$lang."_pages_internal_links WHERE module_name = '$module_name' AND table_name = '$table_name' AND entry_id = $entry_id";
		if (!db_exec($query)) return 0;
		else return 1;
	}


/*
	Функция для получения в спец. файле внутренних (ведущих на этот же сайт) ссылок, которые не содержатся в полях БД, а вводятся в html прямо в модулях. Файл считывается, адреса и линки (линки вытаскиваются функцией из адреса) страниц записываются в массив конфигурации $CONFIG, а потом используются в модулях.
	Формат строчки файла такой:
	[id страницы]=[название переменной в CONFIG]=[адрес страницы], например: 2=map_page_address=map/
	После парсинга этой строки получим элементы массива:
	$CONFIG["map_page_address"] = "map/";
	$CONFIG["map_page_link"] = "map";
	map и map/ - линк и адрес страницы с id=2.
	Наполняется файл вручную программистом при создании сайта.
*/

	function get_internal_links_from_file() {
		global $CONFIG, $main, $lang;
		$lines = array();

		if (file_exists($this->ilinks_file)) {
			$lines = file($this->ilinks_file);
		} else return 0;

		foreach ($lines as $str) {
			list($page_id, $key_address, $address) = explode("=", $str);

			$address = preg_replace("/\\r\\n/", "", $address);
			$address = preg_replace("/\\n/", "", $address);

			if ($address) {
				$CONFIG[$key_address] = $address;
				preg_match("/([^\/]*)\/([[:space:]]*)$/i", $address, $arr);
				if ($arr[1]) {
					$key_link = str_replace("address", "link", $key_address);
					$CONFIG[$key_link] = $arr[1];
				}
			}
		}

		return;
	}

	/* функция для обновления файла для отслеживания ссылок, которые не содержатся в полях БД,
	а вводятся в html прямо в php-модулях...
	этот файл считывается через get_internal_links_from_file()
	*/
	function update_internal_links_in_file($id, $new_address) {
		global $CONFIG, $main, $lang;
		$lines = array();
		$update = 0;

		if (file_exists($this->ilinks_file)) {
			$lines = file($this->ilinks_file);
		} else return 0;

		foreach ($lines as $idx => $str) {
			$lines[$idx] = preg_replace("/[[:space:]]/i", "", $lines[$idx]);
			list($page_id, $key_address, $address) = explode("=", $lines[$idx]);

			if ($address && $page_id == $id) {
				$lines[$idx] = str_replace($address, $new_address, $lines[$idx]);
				$update = 1;
			}
		}

		if ($update) {
			$text = implode("\n", $lines);
			if ($handle = fopen($this->ilinks_file, "w")) {
				fwrite($handle, $text);
				fclose($handle);
			}
		}

		return $update;
	}
}

?>