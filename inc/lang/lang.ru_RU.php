<?php

//------------------- Языковые настройки класса class.mailer.php ----------------//
$CORE_LANG['provide_address']		= 'You must provide at least one recipient email address.';
$CORE_LANG['mailer_not_supported']	= ' mailer is not supported.';
$CORE_LANG['execute']				= 'Could not execute: ';
$CORE_LANG['instantiate']			= 'Could not instantiate mail function.';
$CORE_LANG['authenticate']			= 'SMTP Error: Could not authenticate.';
$CORE_LANG['from_failed']			= 'The following From address failed: ';
$CORE_LANG['recipients_failed']		= 'SMTP Error: The following recipients failed: ';
$CORE_LANG['data_not_accepted']		= 'SMTP Error: Data not accepted.';
$CORE_LANG['connect_host']			= 'SMTP Error: Could not connect to SMTP host.';
$CORE_LANG['file_access']			= 'Could not access file: ';
$CORE_LANG['file_open']				= 'File Error: Could not open file: ';
$CORE_LANG['encoding']				= 'Unknown encoding: ';

?>