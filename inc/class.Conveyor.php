<?php
/**
 * Имя переменной сесии в которой сохраняются данные конвейера
 */
define("CNVR_SESS_NAME", "conveyor");
/**
 * Максимально время работы конвейера в % по умолчанию
 */
define("CNVR_DEFAULT_MAX_TIME", 40);

/**
 * Conveyor class.
 *
 * Description.
 */
class Conveyor
{
    /**
     * Регистрируемая в конвейере ф-ция.
     * Ф-ция должа возвращать - TRUE в случае успешного выполнения и сообщение об ошибке при ошибке.
     * Возможные значения: строка - имя ф-ии, массив с 2-мя элементами - название класса или объект и имя метода.
     * @var string
     */
    var $func = '';

    /**
     * Ф-ция вызываемая при запуске конвейера.
     * Если эта ф-ция установлена, то её значение передаётся зарегистрированной ф-ции и
     * ф-ции вызываемой перед завершением работы конвейера в качестве первого параметра.
     * Возможные значения: строка - имя ф-ии, массив с 2-мя элементами - название класса или объект и имя метода.
     * @var string
     */
    var $beginFunc = '';

    /**
     * Ф-ция вызываемая перед завершением работы конвейера.
     * Возможные значения: строка - имя ф-ии, массив с 2-мя элементами - название класса или объект и имя метода.
     * @var string
     */
    var $endFunc = '';

    /**
     * Двухмерный массив(очередь) аргументов передаваемых в зарегистрированную ф-цию
     * @var array
     */
    var $args = array();

    /**
     * Список сообщений об ошибках
     * @var array
     */
    var $errors = array();

    /**
     * Итерация конвейера
     * @var int
     */
    var $iteration = 0;

    /**
     * Кол-во вызовов ф-ции(сколько раз была вызвана зарегистрированная ф-ция)
     * @var int
     */
    var $callCnt = 0;

    /**
     * Время запуска конвейера
     * @var int
     */
    var $startTime = 0;

    /**
     * Максимально время работы конвейера
     * Значение от 1 до 99 - процент от max_execution_time
     * @var int
     */
    var $maxWorkTime = CNVR_DEFAULT_MAX_TIME;

    /**
     * Конструктор.
     *
     * Если в переменных сессии сохранены значения зарегистрированной ф-ии, очереди аргументов,
     * итерации конвейера - то они записываются в соответствующие поля.
     *
     * @param int $maxWorkTime - Максимально время работы конвейера.
     * @return mixed description
     */
    function Conveyor($maxWorkTime = CNVR_DEFAULT_MAX_TIME)
    {
        $maxWorkTime = $maxWorkTime < 1 || $maxWorkTime > 99 ? CNVR_DEFAULT_MAX_TIME : $maxWorkTime;

        if (isset($_SESSION[CNVR_SESS_NAME]['func'])) {
            $this->func = $_SESSION[CNVR_SESS_NAME]['func'];
        }

        if (isset($_SESSION[CNVR_SESS_NAME]['begin_func'])) {
            $this->beginFunc = $_SESSION[CNVR_SESS_NAME]['begin_func'];
        }

        if (isset($_SESSION[CNVR_SESS_NAME]['end_func'])) {
            $this->endFunc = $_SESSION[CNVR_SESS_NAME]['end_func'];
        }

        if (isset($_SESSION[CNVR_SESS_NAME]['args'])) {
            $this->args = $_SESSION[CNVR_SESS_NAME]['args'];
        }

        if (isset($_SESSION[CNVR_SESS_NAME]['iteration'])) {
            $this->iteration = $_SESSION[CNVR_SESS_NAME]['iteration'];
        }

        if (isset($_SESSION[CNVR_SESS_NAME]['call_cnt'])) {
            $this->callCnt = $_SESSION[CNVR_SESS_NAME]['call_cnt'];
        }

        if (isset($_SESSION[CNVR_SESS_NAME])) {
            unset($_SESSION[CNVR_SESS_NAME]);
        }

        return;
    }

    /**
     * Зарегистрироват ф-цию в конвейере
     * @param  string $func  Регистрируемая ф-ия.
     *                       Возможные значения: строка - имя ф-ии,
     *                       массив с 2-мя элементами - название класса или объект и имя метода.
     * @return boolean Если ф-ии не существует или уже зарегистрирована - FALSE, иначе - TRUE
     */
    function RegigistrFunc($func)
    {
        if (!is_callable($func, true) || '' !== $this->func) {
            return FALSE;
        }
        $this->func = $func;

        return TRUE;
    }

    /**
     * Зарегистрироват ф-цию вызываемую при запуске конвейера.
     * @param  string $func  Регистрируемая ф-ия.
     *                       Возможные значения: строка - имя ф-ии,
     *                       массив с 2-мя элементами - название класса или объект и имя метода.
     * @return boolean Если ф-ии не существует или уже зарегистрирована - FALSE, иначе - TRUE
     */
    function RegigistrBeginFunc($func)
    {
        if (!is_callable($func, true) || '' !== $this->beginFunc) {
            return FALSE;
        }
        $this->beginFunc = $func;

        return TRUE;
    }


    /**
     * Зарегистрироват ф-цию вызываемую пред завершении работы конвейера
     * @param  string $func  Регистрируемая ф-ия.
     *                       Возможные значения: строка - имя ф-ии,
     *                       массив с 2-мя элементами - название класса или объект и имя метода.
     * @return boolean Если ф-ии не существует или уже зарегистрирована - FALSE, иначе - TRUE
     */
    function RegigistrEndFunc($func)
    {
        if (!is_callable($func, true) || '' !== $this->endFunc) {
            return FALSE;
        }
        $this->endFunc = $func;

        return TRUE;
    }

    /**
     * Проверяет зарегистрирована ли функция в конвейере
     * @param  void
     * @return boolean
     */
    function IsRegFunc()
    {
        return '' == $this->func ? FALSE : TRUE;
    }

    /**
     * Проверяет зарегистрирована ли функция вызываемая при запуске конвейера
     * @param  void
     * @return boolean
     */
    function IsRegBeginFunc()
    {
        return '' == $this->beginFunc ? FALSE : TRUE;
    }

    /**
     * Проверяет зарегистрирована ли функция вызываемая перед завершением работы конвейера
     * @param  void
     * @return boolean
     */
    function IsRegEndFunc()
    {
        return '' == $this->endFunc ? FALSE : TRUE;
    }

    /**
     * Добавить аргументы зарегистрированной ф-ии в очередь аргументов
     * @param  array $args  Массив аргументов зарегистрированной ф-ии
     * @return void
     */
    function AddArgs($args)
    {
        $this->args[] = $args;
    }

    /**
     * Получить список ошибок
     * @param  void
     * @return mixed FALSE - нет ошибок, иначе список ошибок
     */
    function GetErrors()
    {
        return count($this->errors) ? $this->errors : FALSE;
    }

    /**
    * Сохранить свойства оъекта в сиссии
    * @param  void
    * @return void
    */
    function Save()
    {
        $_SESSION[CNVR_SESS_NAME]['func'] = $this->func;
        $_SESSION[CNVR_SESS_NAME]['begin_func'] = $this->beginFunc;
        $_SESSION[CNVR_SESS_NAME]['end_func'] = $this->endFunc;
        $_SESSION[CNVR_SESS_NAME]['args'] = $this->args;
        $_SESSION[CNVR_SESS_NAME]['iteration'] = $this->iteration;
        $_SESSION[CNVR_SESS_NAME]['call_cnt'] = $this->callCnt;
    }

    /**
    * Запустить конвейер.
    * Проверяется зарегистриронанна ли функция и установлена ли очередь аргументов.
    * Фиксируется время запуска. Увеличивается итерация. Проверяется максимально время работы.
    * До тех пор пока не превышено максимальное время работы, из очереди выталкивается список аргументов
    * и передаётся зарегистрированной ф-ции. Если превышается максимальное время работы - свойства
    * оъекта сохраняются в сессии
    * @param  void
    * @return int  < 0 - Ошибка - ф-ия не зарегистрированна или очередь пуста
    *              0   - Превышено максимальное время работы
    *              > 0 - Обработана вся очередь
    */
    function Start()
    {
        if (!$this->IsRegFunc()) {
            return -1;
        }

        if (!count($this->args)) {
            return -1;
        }
        $this->startTime = time();

        $stop_time = $this->startTime +
            ($this->maxWorkTime * (ini_get('max_execution_time') > 0 ? ini_get('max_execution_time') : 30) / 100);
        $this->iteration++;

        if ('' !== $this->beginFunc) {
            $begin_val = call_user_func($this->beginFunc);
        }

        while ( TRUE ) {
            if ($stop_time < time()) {
                $this->Save();
                if ('' !== $this->endFunc) {
                    if (isset($begin_val)) {
                        call_user_func($this->endFunc, $begin_val);
                    } else {
                        call_user_func($this->endFunc);
                    }
                }
                return 0;
            }

            $args = array_shift($this->args);

            if (NULL === $args) {
                if ('' !== $this->endFunc) {
                    if (isset($begin_val)) {
                        call_user_func($this->endFunc, $begin_val);
                    } else {
                        call_user_func($this->endFunc);
                    }
                }
                return 1;
            }

            if (isset($begin_val)) {
                if (is_array($args)) {
                    array_unshift($args, $begin_val);
                } else {
                    $args = array($begin_val, $args);
                }
            }

            $this->callCnt++;
            $ret = call_user_func_array($this->func, $args);
            if (TRUE != $ret) {
                $this->errors[] = $ret;
            }
        }

    }
}
/**
 * END Class Conveyor
 */
?>