<?php
s_tart();
define('ABOCMS_RELEASE', '5.8');

header('Content-Type: text/html; charset=utf-8');
header('A-Powered-By: ABO.CMS '.ABOCMS_RELEASE.' ('.$checkKey().')');

$RESERVE_MODULES_NAME = array (
	'_kernel',
	'article',
	'backup',
	'banners',
    'blogs',
    'catalog',
    'comments',
    'configuration',
    'counter',
    'documents',
    'faq',
    'forms',
    'forum',
    'friends',
    'gallery',
	'glossary',
	'job',
	'maps',
	'news',
	'pages',
	'poll',
	'pricelists',
	'scripts',
	'search',
	'shop',
	'sitelinks',
	'siteusers',
	'subscription',
	'support',
	'tags');

function s_tart(){
	global $checkKey;
	$checkKey = create_function('$type=null',
	  '
		static $KEY;$UNIQ = "*&4a$";
		global $db, $CONFIG, $NEW;
		if(!$KEY){$KEY = (isset($CONFIG["L_KEY"])) ? $CONFIG["L_KEY"] : (isset($NEW["L_KEY"]) ? $NEW["L_KEY"] : NULL );}
		if($type!=$UNIQ){return md5($KEY);} else {return $KEY;}
	  '
	);
}

class Main {
	var $version = '1.0.0';
	var $updates = '';
/**
 * Сканирование директорий с модулями, подключение конфигов модулей.
 * Так же формирование массива с данными для подключения к серверу БД.
 * @version	3.0
 * @access	public
 * @param	string	$rp			абсолютный путь к текущей директории (обычно используется константа RP)
 * @param	string	$lang		языковой префикс, значение по умолчанию "rus"
 * @param	int		$base_num	номер БД, нумерация идёт с 1, значение по умолчанию 1.
 * @return	bool	возвращает TRUE
 */
  function Main() {
    GLOBAL	$CONFIG, $NEW, $OPEN_NEW, $TYPES, $lang, $Uploads, $UploadImgPath,
		$ImgPath, $Uploads, $AdminImgPath, $UploadImgPath;

		if (isset($_REQUEST['edit_content'])) {
		    $_COOKIE['edit_content'] = $_REQUEST['edit_content'] ? 1 : 0;
		}
		$site_key = substr($GLOBALS['server'], 0, -1); /* Удалянм '_' на конце */
		$path_to_scan	= RP.$CONFIG['class_path'];
		$dirs			= $this->getDirList($path_to_scan);
		if ('configuration' != $_REQUEST['name'] || ($_REQUEST['action'] && 'prp' != $_REQUEST['action']) ) {
			$this->checkKey();
		}
		$this->getTypeBox($dirs);
		$count_of_dirs	= count($dirs);
		$CONFIG			= array_merge((array)$NEW, (array)$CONFIG);
		$CONFIG			= array_merge((array)$OPEN_NEW,	$CONFIG);
		for ($i = 0; $i <= ($count_of_dirs - 1); $i++) {
			if (file_exists(RP.$CONFIG['class_path'].$dirs[$i].'/config.php')) {
				#if(CHECK_BOM_INCLUDES) check_BOM(RP.$CONFIG['class_path'].$dirs[$i].'/config.php');
				require_once(RP.$CONFIG['class_path'].$dirs[$i].'/config.php');
				$CONFIG = array_merge((array)$OPEN_NEW, $CONFIG);
				$CONFIG = array_merge((array)$NEW, $CONFIG);
				$TYPES	= array_merge((array)$TYPES, (array)$TYPES_NEW);
                #Main::ConfSetDefault(&$CONFIG, $TYPES);
			} else {
				echo '<b style="color: red">ОШИБКА!!!</b>: Один из системных файлов <b style="color: red">НЕ НАЙДЕН!</b><br>';
				echo RP.$CONFIG["class_path"].$dirs[$i].'/config.php';
			}
		}
		foreach ($TYPES as $key => $val) {
			if ($val['multisite']) {
				$CONFIG[$key]  = isset($CONFIG[$key][$site_key]) ? $CONFIG[$key][$site_key] : '';
			}
		}
		#if(CHECK_BOM_INCLUDES) check_BOM(RP.'inc/lang/lang_'.$CONFIG["admin_lang"].'.php');
		require RP.'inc/lang/lang_'.$CONFIG["admin_lang"].'.php';

		$this->_msg = $_MSG;
		return TRUE;
	}
	function WrongLicense(){
		global $CONFIG;
		header('Location: http://www.abocms.ru/wronglicense/');
		die();
	}
  function getTypeBox($dirs){
    static $vers;
    if($vers){return $vers;}
    global $RESERVE_MODULES_NAME, $checkKey;
    $vers = 'Unknown';
    $all = array_intersect($RESERVE_MODULES_NAME, $dirs);
    $size = sizeof($all);
    if($size>=8) $vers = 'Free';
    if($size>=9) $vers = 'Start';
    if($size>=13) $vers = 'Promo';
    if($size>=16 && in_array('shop', $all)) $vers = 'Shop';
    if($size>=19) $vers = 'Community';
    if($size>=23) $vers = 'Corporative';
    if($size>=26) $vers = 'Business';
    define('ABOCMS_RELEASE_NAME', $vers);
    define('ABOCMS_VERSION', $vers. ' '.ABOCMS_RELEASE.' ('.$checkKey().')');
    return $vers;
  }

    function getDateUpdate($type, $dop = array())
    {   global $checkKey, $db, $CONFIG, $NEW;

        $host = 'update51.abocms.ru';
        $url = '/';
        $fp = @fsockopen($host, 80, $errno, $errstr, 5);
        if ($fp) {
            $out = "HEAD $url HTTP/1.1\r\n";
            $out .= "Host: $host\r\n";
            $out .= "Key: ".$checkKey()."\r\n";
            $out .= "Domain: {$dop['host']}\r\n";
            $out .= "Connection: Close\r\n\r\n";
            fwrite($fp, $out);
            while (!feof($fp)) {
                $str = fgets($fp, 128);
                if(strpos($str, 'DATE_REG:')>-1) {
                    $date = trim(substr($str,9));
                    break;
                }
            }
            fclose($fp);
        } else {
            @header('Licence: error connect');
            return false;
        }
        return $date;
    }

	function getKey($type, $dop = array())
	{
		global $checkKey, $db, $CONFIG, $NEW;

		if (false === ($key = Main::checkLocalKey(
		    $checkKey("*&4a$"), $type, ($type=='domain')? $dop['host'] : $dop['name']))
		) {
		    $host = (!empty($NEW['server_updates'])) ? str_replace('http://','',$NEW['server_updates']) : 'update51.abocms.ru';
		    $url = '/';
		    $fp = @fsockopen($host, 80, $errno, $errstr, 5);
		    if ($fp) {
		        $out = "HEAD $url HTTP/1.1\r\n";
		        $out .= "Host: $host\r\n";
		        $out .= "Key: ".$checkKey()."\r\n";
              	if($type == 'mod' && !empty($dop['name'])){
              		$out .= "Modules: {$dop['name']}\r\n";
              	}
              	if (isset($dop['SendInfo']) && is_array($dop['SendInfo'])) {
              	    foreach($dop['SendInfo'] as $_k => $_v) {
              	        $out .= "$_k: {$_v}\r\n";
              	    }
            	}
            	$out .= "Domain: {$dop['host']}\r\n";
            	$out .= "Connection: Close\r\n\r\n";
            	fwrite($fp, $out);
            	while (!feof($fp)) {
            	    $str = fgets($fp, 128);
            	    if(strpos($str, 'VERSION:')>-1){$version = trim(substr($str,8));}
            	    if(strpos($str, 'DOMAIN_TYPE:')>-1){$domain_type = trim(substr($str,12));}
            	    if(strpos($str, 'KEY:')>-1){$key = trim(substr($str,4)); break;}
            	}
            	fclose($fp);
		    } else {
		        @header('Licence: error connect');
		        return false;
		    }
		}
        $path	= RP.$CONFIG['class_path'];
        $dirs_box	= Main::getDirList($path);
        Main::getTypeBox($dirs_box);
        $ver = explode(' ', ABOCMS_VERSION);
        if (!empty($key) && $key!=$dop['l_key'] && $type == 'domain') {
            $num = @mysql_query("select * from sites");
            if (@mysql_num_rows($num) == 1 && $domain_type == 1) {
                return false;
            } elseif ($ver[0] != $version) {
                return false;
            } else {
                $res = @mysql_query("update sites set LKEY='{$key}' where id='{$dop['id']}'");
                if($res && mysql_affected_rows()>0){header("location: ".$_SERVER['REQUEST_URI']);exit;}
                return true;
            }
        } elseif(!empty($key) && $key!=$dop['l_key'] && $type == 'mod' ) {
            $res = @mysql_query("update modules set LKEY='{$key}' where name='{$dop['name']}'");
            if ($res) {
                header("location: ".$_SERVER['REQUEST_URI']);
                exit;
            }
			return true;
		} elseif($type == 'SupportPass') {
			 return $key;
		}
		return false;
	}
	function checkKey(){
		global $checkKey,$db, $ErrKEY;
		if($_SERVER['HTTP_HOST'] == 'localhost' || true) {return true;}
		$host = Main::parseSiteName($_SERVER['HTTP_HOST']);
		$db->query("select s.id, s.title, sa.alias, s.LKEY from sites s left join sites_alias sa ON (s.id=sa.site_id) ".
			" where s.title='{$host}' OR sa.alias='{$host}'"
		);
		$db->next_record();
		if(md5($db->Record["title"].'*.*'.$checkKey("*&4a$") )!= $db->Record["LKEY"]){
			$check = $this->getKey('domain',
				array ('host' 	=> $db->Record["title"],
					     'id'		=> $db->Record["id"],
					     'l_key'	=> $db->Record["LKEY"],
				)
			);
			if(!$check){
				$ErrKEY = true;
				if($_SERVER['PHP_SELF']!='/admin.php' && $_SERVER['PHP_SELF']!='/login.php'){Main::WrongLicense();exit;}
			}
		}
		 else {
		 	$GLOBALS['site_config']['host'] = $db->Record["title"];
		}
	}

	/**
	* функция проверки валидности данного модуля (проверка подписи)
	*
	*/
	function checkCrc($mdl, $act){
		static $mds;
		global $checkKey, $tpl;
		$name_mdl = strtolower( get_class($mdl) );
		if(!$mds){
			global $db;
			$query = "SELECT name, LKEY FROM modules where ".
				     " name IN ('".implode("','", $GLOBALS['RESERVE_MODULES_NAME'])."')";
			$db->query($query);
			while($db->next_record()){
				$mds[($db->f('name'))] = $db->f('LKEY');
			}
			//unset ($GLOBALS['RESERVE_MODULES_NAME']);
			$mds = array_change_key_case($mds, CASE_LOWER);
		}
		if(!array_key_exists($name_mdl, $mds)){return true;}
		if($act=='prp' || ($name_mdl=='configuration' && $act == 'show_info')){return true;}

		if(md5($name_mdl.'*.*'.$checkKey("*&4a$")) != $mds[$name_mdl] ){
			$check = Main::getKey('mod',
				array ('host' 	=> $GLOBALS['site_config']['host'],
					   'name'	=> $name_mdl,
					   'l_key'	=> $mds[$name_mdl],
				)
			);
			if(!$check && !Main::is_local()){Main::message_noright($mdl);}
		}
	}
	
	function is_local()
	{
        return true;
        return false;
	}
	
	function CleanKey(){
		global $db; if(!$db){return false;}
		$db->query("update sites set LKEY=NULL");
		$db->query("update modules set LKEY=NULL");
	}
  function checkLocalKey($key, $type=null, $var = null){
    if(!file_exists(RP.'inc/liz.key')) return false;
    $a = create_function('$key','
        $k = unpack ("C*", $key);array_unshift ($k, array_shift ($k));$n = sizeof ($k);$i = $n;
        for ($i = $n; $i < 0x100; $i++) $k[$i] = $k[$i % $n];
        for ($i--; $i >= 0x100; $i--) $k[$i & 0xff] ^= $k[$i];$s = array();for ($i = 0; $i < 0x100; $i++) $s[$i] = $i;
        $j = 0; for ($i = 0; $i < 0x100; $i++) {$j = ($j + $s[$i] + $k[$i]) & 0xff;$tmp = $s[$i]; $s[$i] = $s[$j];
        $s[$j] = $tmp; } return $s;
    ');
    $b = create_function('$text1, $key, $crc','
        $s = $crc($key); $n = strlen($text1); $text2 = ""; $i = $j = 0;
        for ($k = 0; $k < $n; $k++) {  $i = ($i + 1) & 0xff; $j = ($j + $s[$i]) & 0xff;
        $tmp = $s[$i]; $s[$i] = $s[$j]; $s[$j] = $tmp; $text2 .= $text1{$k} ^ chr($s[$i] + $s[$j]);
        }  return $text2;
    ');

    $decrypt = create_function('$enc_text, $password, $crc','
      return $crc[0] (base64_decode ($enc_text), $password,$crc[1]);
    ');
    $str = @file_get_contents(RP.'inc/liz.key');

    $dec = $decrypt($str, $key, array($b,$a));
    if(!$dec){return true;}
    $str1 = unserialize($dec);
    if(!$str1[$type][$var]){return true;}
    return $str1[$type][$var];
  }
	function parseSiteNameAndPort($title) {
		$pos	= NULL;
		if ('' != $title) {
			$title = preg_replace(array("/^http:\/\//i", "/^www\./i", "/\/$/"), '', $title);
			$pos	= strpos($title, ':');
       		if ($pos === FALSE) {
       			$port = '';
       		} else {
				$port	= substr($title, $pos);
				$title	= substr($title, 0, $pos);
			}
			return array('name'=>$title, 'port'=>$port);
		}
		return FALSE;
	}
	
	function addUpline($fl = false){
    return false;
    global $CONFIG, $tpl, $lang, $tpl_path;
    if(!$fl) {
       //$tpl->assignInclude('front_edit', $tpl_path.'../admin/_upline.html');
       return true;
    }
    $menu =  Main::show_admin_menu(false, true);

    if(is_array($menu)){
      $tpl->newBlock('front_edit');
      foreach($menu as $v){
             $tpl->newBlock('block_rzd');
             $tpl->assign($v);
      }
    }
  }
	function parseSiteName($title) {
		if (is_array($arr = Main::parseSiteNameAndPort($title))) {
			return $arr['name'];
		}
		return FALSE;
	}

	function parseSitePort($title) {
		if (is_array($arr = Main::parseSiteNameAndPort($title))) {
			return $arr['port'];
		}
		return FALSE;
	}

	function getServerName()
	{   GLOBAL $db;

		$h = array();
		if (is_callable("getallheaders")) $h = @getallheaders();
		$server	= Main::parseSiteName(empty($h["Host"]) ? $_SERVER['HTTP_HOST'] : $h["Host"]);
		$domain_alias = '';

		$query = "SELECT DISTINCT S.title, S.alias AS site_alias, A.alias";
		$query .= " FROM sites_languages AS L, sites AS S LEFT JOIN sites_alias AS A ON S.id = site_id";
		$query .= " WHERE category_id = S.id";
		$db->query($query);
		while ($db->next_record()) {
			$alias = $db->f('alias');
			$title = $db->f('title');
			if ($server == $title) {
				return str_replace(array('.', '-'), array('_', '_'), $db->f('site_alias'))."_";
			}
			if ($server == $alias) {
				$domain_alias = $db->f('site_alias');
			}
		}

		return str_replace(array('.', '-'), array('_', '_'), $domain_alias ? $domain_alias : $server)."_";
	}

	function getAnotherServerName($server = NULL)
	{   GLOBAL $db;

		$server	= Main::parseSiteName($server);
		$domain_alias = '';

		$query = "SELECT DISTINCT S.title, S.alias AS site_alias, A.alias";
		$query .= " FROM sites_languages AS L, sites AS S LEFT JOIN sites_alias AS A ON S.id = site_id";
		$query .= " WHERE category_id = S.id";
		$db->query($query);
		while ($db->next_record()) {
			$alias = $db->f('alias');
			$title = $db->f('title');
			if ($server == $title) {
				return str_replace(array('.', '-'), array('_', '_'), $db->f('site_alias'))."_";
			}
			if ($server == $alias) {
				$domain_alias = $db->f('site_alias');
			}
		}

		return str_replace(array('.', '-'), array('_', '_'), $domain_alias ? $domain_alias : $server)."_";
	}

	function getSiteConfig()
	{   GLOBAL $db;

		$host = Main::parseSiteName($_SERVER['HTTP_HOST']);
		$ret = false;
		$db->query("SELECT S.*, A.alias AS site_alias FROM sites AS S LEFT JOIN sites_alias AS A ON S.id=A.site_id"
		    ." WHERE S.title='{$host}' OR A.alias='{$host}'");
		if ($db->nf()) {
		    $db->next_record();
		    $ret = array();
			$ret['site_id'] = $db->f('id');
    		$ret['host'] = $db->f('title');
    		$ret['pref'] = str_replace(array('.', '-'), array('_', '_'), $db->f('alias'));
    		$ret['descr'] = $db->f('descr');
    		$ret['sitename'] = $db->f('sitename');
    		$ret['sitename_rus'] = $db->f('sitename_rus');
    		$ret['multilanguage'] = $db->f('multilanguage');
    		$ret['contact_email'] = $db->f('contact_email');
    		$ret['return_email'] = $db->f('return_email');
    		$ret['lang_id'] = 0;
    		$ret['language'] = '';
    		$ret['default_lang'] = '';
    		$ret['site_aliases'] = array();
    		if ($db->f('site_alias')) {
    			$ret['site_aliases'][] = $db->f('site_alias');
    			while ($db->next_record()) {
    			    $ret['site_aliases'][] = $db->f('site_alias');
    			}
    		}
    		$l = $_REQUEST['lang'] ? $_REQUEST['lang'] : "";
    		$db->query("SELECT id, language FROM sites_languages"
    		    ." WHERE category_id={$ret['site_id']} ORDER BY is_default DESC");;
    		if ($db->nf()) {
    		    $db->next_record();
    			$ret['lang_id'] = $db->f('id');
    			$ret['language'] = $db->f('language');
    			$ret['default_lang'] = $db->f('language');
    			while ($db->next_record()) {
    				if ($db->f('language') == $l) {
    					$ret['language'] = $db->f('language');
    					$ret['lang_id'] = $db->f('id');
    					$find = true;
    				}
    			}
    		}
		}

		return $ret;
	}

/**
 * Получение URL по ID сайта и ID языка
 */
	function getURLByBlock_id($block_id) {
		GLOBAL $db, $server, $lang;
		$block_id = (int)$block_id;
		if ($block_id) {
			$db->query('SELECT a.site_id site_id, a.lang_id lang_id, b.title site, c.language lang
							FROM '.$server.$lang.'_pages_blocks a,
								 sites b,
								 sites_languages c
							WHERE a.id = '.$block_id.' AND
								  a.site_id = b.id AND
								  a.lang_id = c.id');
			if ($db->nf() > 0) {
				$db->next_record();
				$site_id	= $db->f('site_id');
				$lang_id	= $db->f('lang_id');
				$site		= $db->f('site');
				$lang		= $db->f('lang');
				$www = (0 === strpos($_SERVER['HTTP_HOST'], "www.") ? "www." : "");
				return '/admin.php?site_target='.$www.$site.'&amp;lang='.$lang;
			}
		}
		return FALSE;
	}
//	function checkSupport($pass){
//		//  в данном случае есть дополнительная учетная запись support c
//		//	динамическим паролем (запрос с сайта обновлений)
//		$check = Main::getKey('SupportPass',
//				array (
//					'host' 		=> $GLOBALS['site_config']['host'],
//					'l_key'		=> $mds[$name_mdl],
//					'SendInfo' 	=> array('option'=> 'getPass'),
//				)
//		);
//		if($check == md5($pass)){
//			global $db;
//			//get all sites
//			$db->query("select s.id SITE_ID, sl.id LANG_ID, s.alias,sl.language from sites s inner join sites_languages sl on s.id=sl.category_id");
//			if($db->nf()){while($db->next_record()){$sites[$db->f('alias').'_'.$db->f('language')] = array('site'=>$db->f('SITE_ID'),'lang'=>$db->f('LANG_ID'));}}
//
//			//get all group
//			$db->query("select id from core_groups ");
//			if($db->nf()){
//			     while($db->next_record()){
//			         $groups[] = $db->f('id');
//			    }
//			}
//
//			//set all right
//			$r = array("r"=>'1',"e"=>'1',"p"=>'1',"d"=>'1');
//			$db->query("select name from modules");
//			if($db->nf()){while($db->next_record()){$perm[$db->f('name')] = $r;}}
//
//			session_start();
//
//			$_SESSION['session_is_admin']			= true;
//			$_SESSION["siteuser"]["id"]				= -1;
//			$_SESSION["siteuser"]["group_id"]		= $groups;
//			$_SESSION['permissions']				= array();
//
//			$sp = &$_SESSION['permissions'];
//			foreach($sites as $k => $v){$sp[$k] = $perm;}
//			return true;
//		}
//		return false;
//	}

    function addAdminRecord($u, $p){
        global $db, $server, $lang;
        // проверим есть ли какие нить группы, роли и права (если нет то соответственно создадим)
        // точнее не будем проверять а сразу создадим полный набор прав
        $query = "select ".
                 " sl.id as LANG_ID, sl.title as LangTitle, sl.language as LangAlias, ".
                 " s.ID as SITE_ID, s.title as SiteTitle, s.alias as SiteAlias  ".
                 " from sites_languages sl ".
                 " inner join sites s on (sl.category_id=s.id) ";
        $db->query($query);
        if(!$db->nf()){return false;}
        while($db->next_record()){
            $sites[] = $db->Record;
        }
        if(!is_array($sites)){return false;}
        foreach($sites as $v){
            $query = "insert into core_roles set site_id='{$v['SITE_ID']}', lang_id='{$v['LANG_ID']}', ".
            " descr='full permissions for {$v['SiteTitle']} {$v['LangTitle']}' ";
            $db->query($query);
            $IDs[] = mysql_insert_id();
        }
        if(!is_array($IDs)){return false;}
        $query = "insert into core_roles_permissions (role_id,module_id,r,e,p,d) ".
                 " select ur.id, m.id, 1, 1, 1, 1".
                 " from core_roles ur ".
                 " join modules m ".
                 " where ur.id in (".implode(',',$IDs).")";
        $db->query($query);
        // Создаем пользователя
        $db->query("insert into core_users set username='{$u}', password='{$p}',"
            ." site_id=".$sites[0]['SITE_ID'].", lang_id=".$sites[0]['LANG_ID'].", active=1");
        $USER_ID = mysql_insert_id();
        // Создаём группу админитстратора
        $db->query("insert into core_groups set owner_id=".$USER_ID.","
            ." name='Admin', description='full permissions for', active=1 ");
        $GROUP_ID = mysql_insert_id();
        // Добавляем поьзователя в группу
        $db->query("insert into `core_users_groups` set user_id=".$USER_ID.", group_id=".$GROUP_ID);
        // Назначаем группе все роли
        $db->query("insert into core_roles_groups (group_id, role_id) VALUES "
            ."(".$GROUP_ID."," .implode("), (".$GROUP_ID.",", $IDs) .")");

        return true;
    }

	function fillPermissions($username, $password)
	{
		global $db, $server, $lang;
		$base_limit = 60;
//		if($username=='support' && Main::checkSupport($password)){
//			return TRUE;
//		}

        // возникла проблема при создании сайта без наполнения (в этом случае отсутствуют
        // пользователи и группы пользователей)
        $query = 'select count(*) FROM core_users ';
        $db->query($query);
        if(!$db->nf()){return false;}
            else {$db->next_record();}
        if($db->Record[0]==0){
            if (Main::addAdminRecord($username, $password))
                return Main::fillPermissions($username, $password);
        }

		// проверим зарегистрирован ли пользователь для других сайтов или языков
		$db->query("SELECT s.id, sl.language, sl.id as lang_id"
		    ." FROM sites s JOIN sites_languages sl WHERE s.id = sl.category_id"
		    ." AND alias='".substr($server, 0, -1)."' AND language='".$lang."'");
		$query = array();
		$cur_site_id = 0;
		$cur_lang_id = 0;
		while ($db->next_record()) {
			$cur_site_id = $db->f('id');
			$cur_lang_id = $db->f('lang_id');
		}

		$query = "SELECT u.id, u.username, u.email, ug.group_id FROM core_users u LEFT JOIN core_users_groups ug ON (u.id = ug.user_id)"
		    ." JOIN core_groups g WHERE ug.group_id = g.id AND u.email = '".$username."'"
		    ." AND u.password = MD5('".$password."') AND u.active = 1 AND g.active = 1";
		$db->query($query);

		if ($db->nf()) {
			$db->next_record();

			session_start();

			$_SESSION['session_login']				= $username;
			$_SESSION['session_password']			= $password;
			$_SESSION['session_cur_site_id']		= $cur_site_id;
			$_SESSION['session_cur_lang_id']		= $cur_lang_id;

			$_SESSION["siteuser"]["siteusername"]	= $db->f('username');
			$_SESSION["siteuser"]["id"]				= $db->f('id');
			do {
				$_SESSION["siteuser"]["group_id"][]	= $db->f('group_id');
			} while ($db->next_record());

			// получение прав доступа группы данного пользователя
			$_SESSION['permissions']				= array();
			$query = "SELECT S.id AS site_id, S.alias AS site_alias, S.title as site_domain,"
			    ." L.language AS lang, M.name as mod_name, M.id as mod_id,"
			    ." RP.r r, RP.e e, RP.p p, RP.d d"
			    ." FROM sites S, sites_languages L, modules M, core_roles_permissions RP,"
			    ." core_roles_groups RG, core_roles R"
			    ." WHERE RG.group_id IN (".implode(',', $_SESSION["siteuser"]["group_id"]).")"
			    ." AND RG.role_id=R.id AND R.site_id=S.id AND R.lang_id=L.id AND R.id=RP.role_id"
			    ." AND RP.module_id = M.id";
			$db->query($query);

			if ($db->nf() > 0) {
				while ($db->next_record()) {
					$server_pref	= str_replace('.', '_', Main::parseSiteName($db->f('site_alias')).'_');
					$site	= str_replace('-', '_', $server_pref).$db->f('lang');
					$name	= $db->f('mod_name');

					if ($db->f('r')) {
						$_SESSION['permissions'][$site][$name]['r'] = $db->f('r');
						$_SESSION['session_is_admin']			= true;
					} elseif (!isset($_SESSION['permissions'][$site][$name]['r'])) {
						$_SESSION['permissions'][$site][$name]['r'] = '0';
					}

					if ($db->f('e')) {
						$_SESSION['permissions'][$site][$name]['e'] = $db->f('e');
					} elseif (!isset($_SESSION['permissions'][$site][$name]['e'])) {
						$_SESSION['permissions'][$site][$name]['e'] = '0';
					}

					if ($db->f('p')) {
						$_SESSION['permissions'][$site][$name]['p'] = $db->f('p');
					} elseif (!isset($_SESSION['permissions'][$site][$name]['p'])) {
						$_SESSION['permissions'][$site][$name]['p'] = '0';
					}

					if ($db->f('d')) {
						$_SESSION['permissions'][$site][$name]['d'] = $db->f('d');
					} elseif (!isset($_SESSION['permissions'][$site][$name]['d'])) {
						$_SESSION['permissions'][$site][$name]['d'] = '0';
					}
				}
			}
            if(file_exists(RP.'mod/siteusers/lib/class.Siteusers.php')){
                require_once(RP.'mod/siteusers/lib/class.Siteusers.php');
                $x = new Siteusers('only_create_object');
                $x->login_siteuser($username, $password);
            }
			sleep(1);
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function getSitesLangsSelect($site = NULL) {
		GLOBAL $CONFIG, $db, $lang;
		$db->query('SELECT a.title title, b.language language
						FROM sites AS a,
							 sites_languages AS b
						WHERE b.category_id = a.id');
		if ($db->nf() > 0) {
			$www = (0 === strpos($_SERVER['HTTP_HOST'], "www.") ? "www." : "");
			//$www = "";
			for ($i = 0; $i < $db->nf(); $i++) {
				$db->next_record();
				$arr[$i]['site']	= 'http://'.$www.$db->f('title').'/admin.php?lang='.$db->f('language');
				$arr[$i]['host']	= $db->f('title');
				$arr[$i]['lang']	= $db->f('language');
				for ($j = 0; $j < sizeof($CONFIG['site_langs']); $j++) {
					if ($CONFIG['site_langs'][$j]['value'] == $db->f('language')) {
						$arr[$i]['title']		= abo_strtoupper($db->f('title')).' ('.$CONFIG['site_langs'][$j]['name'].')';
						break;
					}
				}
			}

			if ('' != $site) {
				for ($i = 0; $i < sizeof($arr); $i++) {
					if ($arr[$i]['site'] == $site) {
						$select .= '<option value="'.$arr[$i]['site'].'" selected>'.$arr[$i]['title'].'</option>';
					} else {
						$select .= '<option value="'.$arr[$i]['site'].'&action=changesite">'.$arr[$i]['title'].'</option>';
					}
				}
			} else {
				$host = $this->parseSiteName($_SERVER['HTTP_HOST']);
				for ($i = 0; $i < sizeof($arr); $i++) {
					if ($host == $arr[$i]['host']) {
						if ($lang == $arr[$i]['lang']) {
							$select .= '<option value="'.$arr[$i]['site'].'" selected>'.$arr[$i]['title'].'</option>';
						} else {
							$select .= '<option value="'.$arr[$i]['site'].'&action=changesite">'.$arr[$i]['title'].'</option>';
						}
					} else {
						$select .= '<option value="'.$arr[$i]['site'].'">'.$arr[$i]['title'].'</option>';
					}
				}
			}
        	return $select;
		}
		return FALSE;
	}

	function getMeterialSitesLangsSelect($site = NULL, $mod = NULL) {
		GLOBAL $CONFIG, $db, $lang;
		$db->query('SELECT a.title title, b.language language
						FROM sites AS a,
							 sites_languages AS b
						WHERE b.category_id = a.id');
		if ($db->nf() > 0) {
			$www = (0 === strpos($_SERVER['HTTP_HOST'], "www.") ? "www." : "");
			//$www = "";
			for ($i = 0; $i < $db->nf(); $i++) {
				$db->next_record();
				$arr[$i]['site']	= '/admin.php?site_target='.$www.$db->f('title').'&amp;lang='.$db->f('language');
				$arr[$i]['host']	= $db->f('title');
				$arr[$i]['lang']	= $db->f('language');
				for ($j = 0; $j < sizeof($CONFIG['site_langs']); $j++) {
					if ($CONFIG['site_langs'][$j]['value'] == $db->f('language')) {
						$arr[$i]['title']		= abo_strtoupper($db->f('title')).' ('.$CONFIG['site_langs'][$j]['name'].')';
						break;
					}
				}
			}

			for ($i = 0; $i < sizeof($arr); $i++) {
				if ($arr[$i]['site'] == $site) {
					$select .= '<option value="'.$arr[$i]['site'].'" selected>'.$arr[$i]['title'].'</option>';
				} else {
					$select .= '<option value="'.$arr[$i]['site'].'">'.$arr[$i]['title'].'</option>';
				}
			}
        	return $select;
		}
		return FALSE;
	}

	function makeDemoSite($prefix, $site_id, $lang_id) {
		GLOBAL $db;
		if ($prefix) {
			$modules = Main::getDirList(RP.'mod', 'directories');
			for ($j = 0, $h = sizeof($modules); $j < $h; $j++) {
				if (file_exists(RP.'mod/'.$modules[$j].'/.default/sql/'.$modules[$j].'_demo_content.sql')) {
					$file_name = RP.'mod/'.$modules[$j].'/.default/sql/'.$modules[$j].'_demo_content.sql';
					$f = @fopen($file_name, 'r');
					$file_size = filesize($file_name);
					if ($f && $file_size > 0) {
						$sql	= fread($f, $file_size);
						fclose($f);
						#$sql	= explode('INSERT', $sql);
						$sql	= preg_split('/^(INSERT|REPLACE)/im', $sql);
						for($i = 0; $i < sizeof($sql); $i++) {
							if ($sql != '' && strpos($sql[$i], 'INTO')) {
								$query = str_replace(");\r\n", ")\n", $sql[$i]);
								$query = str_replace(
								    array('{site_prefix}', '{site_id}', '{lang_id}'),
								    array($prefix, $site_id, $lang_id),
								    $query);
								$db->query('REPLACE '.$query, $this->db);
							}
						}
					}
				}
			}
        	return TRUE;
		}
		return FALSE;
	}

	function getArrayOfTables($prefix = '', $prefix_2 = '') {
		$dirs = Main::getDirList(RP.'/mod/');
		if (is_array($dirs)) {
			for($i = 0; $i < sizeof($dirs); $i++) {
			  if (file_exists(RP.'/mod/'.$dirs[$i].'/sql/'.$dirs[$i].$prefix_2.'_tables.php')) {
					include(RP.'/mod/'.$dirs[$i].'/sql/'.$dirs[$i].$prefix_2.'_tables.php');
				}
      }
      return $MOD_TABLES;
		}
		return FALSE;
	}

	function createTables($tables = NULL) {
		GLOBAL $db;
		if (is_array($tables)) {
			for ($i = 0; $i < sizeof($tables); $i++) {
				$db->query($tables[$i]);
			}
			return TRUE;
		}
		return FALSE;
	}

/**
 * Перебирает массив $arr и устанавливает элемент $int в значение selected.
 * Применяется при генерации выпадающего меню.
 * Пример: имеется массив $arr = array(0 => array('option_name' => 'пункт 1'),
 *									   1 => array('option_name' => 'пункт 2'),
 * 									   2 => array('option_name' => 'пункт 3'),
 *									  );
 * при передаче методу массива $arr и параметра $int равного 1, на результатом будем массив:
 * $arr = array(0 => array('option_name' => 'пункт 1', 'option_select' => ''),
 *				1 => array('option_name' => 'пункт 2', 'option_select' => 'selected'),
 * 				2 => array('option_name' => 'пункт 3', 'option_select' => ''),
 *			   );
 * @version	3.0
 * @access	public
 * @param	array	$arr	перебираемый массив
 * @param	int		$int	номер элемента массива который необходимо установить в selected
 * @return	mixed	FALSE в случае неудачи, массив в случае успеха
 */
	function getSelectTag($arr, $int) {
		$int = (int)$int;
		if (is_array($arr) && $int < sizeof($arr) && isset($int)) {
			for ($i = 0; $i < sizeof($arr); $i++) {
				if ($i == $int) {
					$arr[$i]['option_select'] = 'selected';
				} else {
					$arr[$i]['option_select'] = '';
				}
			}
			return $arr;
		}
		return FALSE;
	}

/**
 * Перебирает ассоциативный массив $arr и устанавливает элемент $val в значение selected.
 * @version	3.0
 * @access	public
 * @param	array	$arr	перебираемый массив
 * @param	string	$int	номер элемента массива который необходимо установить в selected
 * @return	mixed	FALSE в случае неудачи, массив в случае успеха
 */
	function getAsocSelectTag($arr, $val) {
		global $CONFIG;
		if (is_array($arr)) {
			$i = 0;
			foreach($arr as $k => $v) {
				$prp[$i]['value']			= $k;
				$prp[$i]['descr']			= $v[$CONFIG['admin_lang']];
				$prp[$i]['option_select']	= $k == $val ? 'selected' : '';
				$i++;
			}
			return $prp;
		}
		return FALSE;
	}


/**
 * Поиск в массиве $arr_1 всех ключей массива $arr_2, если ключ не найден он добавляется из $arr_2 вместе со значением
 * @version	3.0
 * @access	private
 * @param	array	$arr_1
 * @param	array	$arr_2
 * @return	mixed	FALSE в случае неудачи, в случае успеха массив
 */
	function searchInArrKeyAnotherArr($arr_1, $arr_2) {
        if (is_array($arr_1) && is_array($arr_2)) {
			foreach ($arr_2 AS $k => $v) {
				if (!array_key_exists($k, $arr_1) || $arr_1[$k] == '') {
					$arr_1[$k] = $v;
				}
		    }
        	return $arr_1;
		}
    	return FALSE;
	}

/**
 * Получение единого массива путём слияния массивов $arr_1 и $arr_2, дублирование ключей массива $arr_2 в массиве $arr_1 исключается
 * @version	3.0
 * @access	public
 * @param	array	$arr_1		массив <b>в</b> который необходимо добавить массив $arr_2
 * @param	array	$arr_2		массив который необходимо добавить <b>в</b> массив $arr_1
 * @param	string	$array_key
 * @return	array	объединённый массив
 */
	function makeMergeArray($arr_1, $arr_2, $array_key = '') {
        global $CONFIG;
        if ($array_key != '') {
            $arr_1[$array_key] = Main::searchInArrKeyAnotherArr($arr_1[$array_key], $CONFIG[$array_key]);
		}
		if (is_array($arr_1) && is_array($arr_2)) {
			$max_k = 0;
			foreach($arr_1 AS $k => $v) {
                foreach($v AS $k_2 => $v_2) {
					if ($k_2 > $max_k) {
						$max_k = $k_2;
					}
				}
            	$val_k = $k;
			}
			foreach($arr_2 AS $key => $val) {
				while(array_key_exists($key, $arr_1) || ($key <= $max_k)) {
                	$key++;
				}
				$arr_1[$val_k][$key] = $val;
				$max_k = $key;
			}
        	return $arr_1;
		}
		return $arr_1;
	}

/**
 * Получение текстового определения массива в соответствии с правилами PHP
 * @version	3.0
 * @access	public
 * @param	array	массив который необходимо привести к текстовому определению
 * @return	mixed	FALSE в случае неудачи, в случае успеха текстовое представление массива в соответствии с правилами PHP
 */
	function makeDefArrayByArray($arr, $i = 0) {
    	global $CONFIG;
    	$tab = str_repeat("\t", $i);
    	if (is_array($arr)) {
			$i++;
			$str = '';
			foreach($arr AS $key => $val) {
				$str .= $tab.'\''.$key.'\' => ';
				$str_2 = '';
				if (is_array($val)) {
					$str_2 .= 'array(';
					$str_2 .= Main::makeDefArrayByArray($val, $i);
				} elseif ($val !== '') {
                	$str .= '\''.$val.'\',';                	
				} else {
					$str .= '\'\',';
				}
            	$str = $str.$tab.$str_2."\n";
			}
            $ret = "\n".$str.$tab;
            if ($i > 1) {
				$ret .= '),';
			}
            return $ret;
		}
    	return FALSE;
	}

/**
 * Получение настроек модуля $module доступных для изменения в административном разделе модуля из файла $file
 * (по умолчанию config.php) из стандартной директории модуля
 * @version	3.0
 * @access	public
 * @param	string	$module		название модуля
 * @param	string	$file		целевой конфигурационный файл
 * @return	mixed
 */
    function getModuleConf($module = '', $file = 'config.php') {
        global $CONFIG;
        if (isset($module) && $module != '') {
            $filename	= RP.'mod/'.$module.'/'.$file;
		} else {
			$filename	= RP.$file;
		}
		$f			= fopen($filename, 'r');
		$conf		= fread($f, filesize($filename));
		fclose($f);
		$start		= strpos($conf, '/* ABOCMS:START */') + strlen('/* ABOCMS:START */');
		$end		= strpos($conf, '/* ABOCMS:END */');
		$lenght		= $end - $start;
		$conf		= substr($conf, $start, $lenght);
		return $conf;
    }

/**
 * Установка настроек модуля $module доступных для изменения в административном разделе модуля
 * из файла $file (по умолчанию config.php) config.php из стандартной директории модуля
 * @version	3.0
 * @access	public
 * @param	array	$arr		массив 1
 * @param	array	$arr_2		массив 2
 * @param	string	$module		имя модуля
 * @param	string	$file		целевой конфигурационный файл
 * @param	string	$array_key	наименование ключа
 * @return	bool	FALSE в случае неудачи, TRUE в случае успеха
 */
    function setModuleConf($arr = array(), $arr_2 = array(), $module = '', $file = 'config.php', $array_key = '') {
        global $CONFIG;
        if (is_array($arr)) {
			foreach ($arr AS $k => $v) {
				//$v = str_replace("\'", "", $v);
				//$v = str_replace("'", "", $v);
                $v = str_replace(
                    array("'"    ,'"'),
                    array('&#39;','&quot;'),
                    $v
                );

				$arr_3[$k] = $v;
			}
        	$arr = $arr_3;

        	if (is_array($arr_2)) {
            	$arr = Main::makeMergeArray($arr, $arr_2, $array_key);
			}
			$str = Main::makeDefArrayByArray($arr).'/* ABOCMS:END */'."\n);\n?>";
        	if ($module) {
            	$filename	= RP.'mod/'.$module.'/'.$file;
			} else {
				$filename	= RP.$file;
			}
			@chmod($filename, 0777);
			$f			= fopen($filename, 'r+');
            $conf		= fread($f, filesize($filename));
            $start		= strpos($conf, '/* ABOCMS:START */') + strlen('/* ABOCMS:START */');
			ftruncate($f, $start);
            fseek($f, $start);
            fwrite($f, $str);
			fclose($f);
			@chmod($filename, 0755);
        	return TRUE;
		}
		return FALSE;
    }

	function getIntegerProperty($k = NULL, $v = NULL, $types = NULL) {
		GLOBAL $tpl, $CONFIG;
		if ($types && ($types['access'] != 'private')) {
			$tpl->assign('descr', $types['descr'][$CONFIG['admin_lang']]);
			$tpl->assign('name', $k);
			$tpl->newBlock('block_integer');
			if ($v) {
				$tpl->assign('value', $v);
			} else {
				$tpl->assign('value', $types['defval']);
			}
			$tpl->assign('name', $k);
			if ($types['access'] == 'readonly' || $types['access'] == 'final') $tpl->assign('readonly', 'readonly');
			if ($types['access'] == 'disabled') $tpl->assign('readonly', 'disabled');
			return TRUE;
		}
		return FALSE;
	}

	function getDoubleProperty($k = NULL, $v = NULL, $types = NULL) {
		GLOBAL $tpl, $CONFIG;
		if ($types && ($types['access'] != 'private')) {
			$tpl->assign('descr', $types['descr'][$CONFIG['admin_lang']]);
			$tpl->assign('name', $k);
			$tpl->newBlock('block_double');
			if ($v) {
				$tpl->assign('value', $v);
			} else {
				$tpl->assign('value', $types['defval']);
			}
			$tpl->assign('name', $k);
			if ($types['access'] == 'readonly' || $types['access'] == 'final') $tpl->assign('readonly', 'readonly');
			if ($types['access'] == 'disabled') $tpl->assign('readonly', 'disabled');
			return TRUE;
		}
		return FALSE;
	}

	function getStringProperty($k = NULL, $v = NULL, $types = NULL) {
		GLOBAL $tpl, $CONFIG;
		if ($types && ($types['access'] != 'private')) {
			$tpl->assign('descr', $types['descr'][$CONFIG['admin_lang']]);
			$tpl->assign('name', $k);
			$tpl->newBlock('block_string');
			if ($v) {
				$tpl->assign('value', $v);
			} else {
				$tpl->assign('value', $types['defval']);
			}
			$tpl->assign('name', $k);
			if ($types['access'] == 'readonly' || $types['access'] == 'final') $tpl->assign('readonly', 'readonly');
			if ($types['access'] == 'disabled') $tpl->assign('readonly', 'disabled');
			return TRUE;
		}
		return FALSE;
	}

	function getCheckboxProperty($k = NULL, $v = NULL, $types = NULL) {
		GLOBAL $tpl, $CONFIG;
		if ($types && ($types['access'] != 'private')) {
 			$v =  $v ? 'checked' : '';
			$tpl->assign('descr', ''.$types['descr'][$CONFIG['admin_lang']]);
			$tpl->assign('name', $k);
			$tpl->newBlock('block_checkbox');
			$tpl->assign('value', $v);
			$tpl->assign('name', $k);
			if ($types['access'] == 'disabled' || $types['access'] == 'final') $tpl->assign('readonly', 'disabled');
        	return TRUE;
		}
		return FALSE;
	}

	function getSelectProperty($k = NULL, $v = NULL, $types = NULL) {
		GLOBAL $tpl, $CONFIG;
		if ($types && ($types['access'] != 'private')) {
			$prp = $this->getAsocSelectTag($types['defval'], $v);
			$tpl->assign('descr', $types['descr'][$CONFIG['admin_lang']]);
			$tpl->assign('name', $k);
			$tpl->newBlock('block_select');
			$tpl->assign('name', $k);
			if ($types['access'] == 'disabled' || $types['access'] == 'final') $tpl->assign('readonly', 'disabled');
			$tpl->assign_array('block_select_row', $prp);
			return TRUE;
		}
		return FALSE;
	}
	
	function getFileProperty($k = NULL, $v = NULL, $types = NULL) {
		GLOBAL $tpl, $CONFIG;
		if ($types && ($types['access'] != 'private')) {
			$prp = $this->getAsocSelectTag($types['defval'], $v);
			$tpl->assign('descr', $types['descr'][$CONFIG['admin_lang']]);
			$tpl->assign('name', $k);
			$tpl->newBlock('block_file');
			$tpl->assign('name', $k);
			$tpl->assign('value', $v);
			if ($v) {
			  $tpl->newBlock('block_file_view');
			  $tpl->assign('value', $v);
			  $tpl->assign('SITE_PREFIX', SITE_PREFIX);
			}
			if ($types['access'] == 'disabled' || $types['access'] == 'final') $tpl->assign('readonly', 'disabled');
			$tpl->assign_array('block_select_row', $prp);
			return TRUE;
		}
		return FALSE;
	}

	function getTextProperty($k = NULL, $v = NULL, $types = NULL) {
		GLOBAL $tpl, $CONFIG;
		if ($types && ($types['access'] != 'private')) {
			$tpl->assign('descr', $types['descr'][$CONFIG['admin_lang']]);
			$tpl->assign('name', $k);
			$tpl->newBlock('block_text');
			if ($v) {
				$tpl->assign('value', $v);
			} else {
				$tpl->assign('value', $types['defval']);
			}
			$tpl->assign('name', $k);
			if ($types['access'] == 'readonly' || $types['access'] == 'final') $tpl->assign('readonly', 'readonly');
			if ($types['access'] == 'disabled') $tpl->assign('readonly', 'disabled');
			return TRUE;
		}
		return FALSE;
	}

  function getPropertiesStrings($NEW = NULL, $TYPES_NEW = NULL) {
		GLOBAL $tpl;
		$site_key = substr($GLOBALS['server'], 0, -1); /* Удалянм '_' на конце */
		if (is_array($NEW) && is_array($TYPES_NEW)) {
        	foreach($TYPES_NEW as $k => $v) {
				if ($TYPES_NEW[$k]['type']) {
				    $val = $TYPES_NEW[$k]['multisite']
				        ? isset($NEW[$k][$site_key]) ? $NEW[$k][$site_key] : ''
				        : $NEW[$k];
					$tpl->newBlock('block_property_row');
					switch($TYPES_NEW[$k]['type']) {
						case 'integer':
							$this->getIntegerProperty($k, $val, $TYPES_NEW[$k]);
							break;
						case 'double':
							$this->getDoubleProperty($k, $val, $TYPES_NEW[$k]);
							break;
						case 'string':
							$this->getStringProperty($k, $val, $TYPES_NEW[$k]);
							break;
						case 'checkbox':
							$this->getCheckboxProperty($k, $val, $TYPES_NEW[$k]);
							break;
						case 'select':
							$this->getSelectProperty($k, $val, $TYPES_NEW[$k]);
							break;
						case 'file':
							$this->getFileProperty($k, $val, $TYPES_NEW[$k]);
							break;
						case 'text':
							$this->getTextProperty($k, $val, $TYPES_NEW[$k]);
							break;
					}
				}
			}
		}
    	return FALSE;
    }

/**
* Формирование страницы редактирования настроек модуля
* @version	3.0
* @access	public
* @param	string	$module		текстовое представление названия модуля
* @param	object	$tpl		объект шаблонизатора
* @return	bool	FALSE в случае неудачи, TRUE в случае успеха
*/
    function getModuleProperties($module = NULL, $replaceVar = false) {
      GLOBAL $tpl;
    	if ($module) {
			if (file_exists(RP.'mod/'.$module.'/config.php')) {
				include(RP.'mod/'.$module.'/config.php');
				if($replaceVar){
					if($replaceVar['NEW']){$NEW = array_merge_recursive($NEW, (array) $replaceVar['NEW']);}
					if($replaceVar['TYPES_NEW']){$TYPES_NEW = array_merge_recursive($TYPES_NEW, (array) $replaceVar['TYPES_NEW']);}
				}
        $this->getPropertiesStrings($NEW, $TYPES_NEW);
			}
			return TRUE;
    	} else {
			if (file_exists(RP.'config.php')) {
				include(RP.'config.php');
				if($replaceVar){
					if($replaceVar['NEW']){$NEW = array_merge_recursive($NEW, (array) $replaceVar['NEW']);}
					if($replaceVar['TYPES_NEW']){$TYPES_NEW = array_merge_recursive($TYPES_NEW, (array) $replaceVar['TYPES_NEW']);}

				}
       	$this->getPropertiesStrings($NEW, $TYPES_NEW);
			}
			return TRUE;
    	}
    	return FALSE;
    }

    function filterPrp($p = NULL, $types_new = NULL, $module = NULL, $new = NULL) {
        global $core;
        $site_key = substr($GLOBALS['server'], 0, -1); /* Удалянм '_' на конце */
    	if (is_array($p) && is_array($types_new)) {
    		foreach($types_new as $k => $v) {
				if (!array_key_exists($k, $p)) {
					$p[$k] = '';
				}
			}

			foreach($p as $k => $v) {
				if (array_key_exists($k, $types_new)) {
					$arr[$k] = $p[$k];
				}
			}

			foreach($types_new as $k => $v) {
					$arr_2[$k] = $arr[$k];
			}

			$arr = $arr_2;

			foreach ($arr as $k => $v) {
				switch ($types_new[$k]['type']) {
					case 'integer':
						$vv = (int)$v;
						break;

					case 'double':
						$vv = (double)$v;
						break;

					case 'select':
						foreach ($types_new[$k]['defval'] AS $key => $val) {
							if ($key == $v) {
								$y[0] = $v;
								break;
							} else {
								$y[] = $key;
							}
						}
						$vv = $y[0];
						break;
					default:
						$vv = $v;
						break;
				}

				if ( ($module && is_numeric(strpos($k, $module)))
				    || !$module
				) {

    			    if ($types_new[$k]['multisite'] && is_array($core->aSitesLangs)) {
    			    	foreach ($core->aSitesLangs as $site_title => $site) {
    			    		$prp[$k][$site['site_alias']] = isset($new[$k][$site['site_alias']]) ? $new[$k][$site['site_alias']] : '';
    			    	}
    			    	$prp[$k][$site_key] = $vv;
    			    } else {
    			        $prp[$k] = $vv;
    			    }
				}
       		}
       		return $prp;
    	}
    	return FALSE;
    }

    function setModuleProperties($module, $p, $types_new = NULL) {
    	if (is_array($p)) {
    		if ($module) {
        		include(RP.'mod/'.$module.'/config.php');
        		$prp = Main::filterPrp($p, $TYPES_NEW, $module, $NEW);
        		Main::setModuleConf($prp, NULL, $module);
			} else {
    			if ($types_new) $prp = Main::filterPrp($p, $types_new);
    			Main::CleanKey();
    			Main::setModuleConf($prp, NULL);
			}
		}
    	return FALSE;
    }

/**
 * Получение формы (с полями согласно массиву $arr) отправляемой по адресу $url
 * @version	3.0
 * @access	public
 * @param	string	$module		название модуля
 * @param	string	$file		файл
 * @param	string	$url		url
 * @return	mixed	FALSE в случае неудачи, массив в случае успеха
 */
    function getConfToForm($module = '', $file = '', $url = '') {
        $arr = Main::getModuleConf($module, $file);
		if (isset($arr)) {
            $arr = '$OPEN_NEW_CONF = array('.$arr.');';
			eval($arr);
		}
		if (is_array($OPEN_NEW_CONF) && (sizeof($OPEN_NEW_CONF) > 0)) {
            $i = 0;
			foreach ($OPEN_NEW_CONF AS $key => $val) {
				if (is_array($val) && (sizeof($val) > 0)) {
                    foreach ($val AS $key_2 => $val_2) {
                        if ($val_2 !== '') {
							$form[$i]['conf_name'] = 'arr['.$key.']['.$key_2.']';
							$form[$i]['conf_val'] = $val_2;
						}
						$i++;
					}
				} elseif ($val !== '') {
					$form[$i]['conf_name']	= $key;
					$form[$i]['conf_val']	= $val;
                	$i++;
				}
			}
			return @$form;
		}
		return FALSE;
    }

/**
 * Получение листинга директорий
 * @version	3.0
 * @access	public
 * @param
 * @param
 * @param
 * @return	mixed	FALSE в случае неудачи, в случае успеха массив директорий
 */
	function getDirList($pth, $types = 'directories', $recursive = 0) {
		if ($dir = opendir($pth)) {
			$file_list = array();
			while (false !== ($file = readdir($dir))) {
				if ($file != '.' && $file != '..') {
					if ((is_dir($pth.'/'.$file)) && (($types == 'directories') || ($types == 'all'))) {
						$file_list[] = $file;
						if ($recursive) $file_list = array_merge($file_list, getDirList($pth.'/'.$file.'/', $types, $recursive));
					} else if (($types == 'files') || ($types == 'all')) {
						$file_list[] = $file;
					}
				}
			}
			closedir($dir);
			return $file_list;
		} else {
			return FALSE;
		}
	}

/**
 * Список файлов директории
 * @version	3.0
 * @access	public
 * @param	string	$pth		Путь к с целевой директории
 * @param	string	$types		Тип искомой сущности
 * @param	bool	$recursive	TRUE рекурсивный метод работы, FALSE без рекурсии
 * @return	mixed	FALSE в случае неудачи, массив в случае успеха
 */
	function files_list($pth, $types = 'files', $recursive = 0) {
		if (@$dir = opendir($pth)) {
			$file_list = array();
			while (FALSE !== ($file = readdir($dir))) {
				if ($file != '.' && $file != '..') {
					if ((is_dir($pth.'/'.$file)) && (($types == 'directories') || ($types == 'all'))) {
						$file_list[] = $file;
     					if ($recursive) {
     						$file_list = array_merge($file_list,
     												 $this->getDirList($pth.'/'.$file.'/', $types, $recursive));
     					}
					} else if ($types == 'files' && !is_dir($pth.'/'.$file)) {
						$file_list[] = $file;
					} else if ($types == 'all') {
						$file_list[] = $file;
					}
				}
			}
			closedir($dir);
			return $file_list;
		}
		return FALSE;
	}

/**
 * Вывод списка из массива $elm_arr в блок $blockname, идентификатор выбранного пункта списка
 * @version	3.0
 * @access	public
 * @param	array	$elm_arr		массив с ключами являющимися отображаемыми пунктами списка и значениями подставляемыми
 										в атрибут списка value=""
 * @param	string	$blockname		блок в шаблоне из которого формируется список
 * @param	string	$selected_elm	идентификатор ключа массива выбранного пункта списка
 * @return	bool
 */
	function show_list($elm_arr, $blockname, $selected_elm = '') {
		global $CONFIG, $tpl;
		foreach ($elm_arr as $elm_idx => $elm_val) {
			$selected = ($elm_idx == $selected_elm) ? ' selected' : '';
			$tpl->newBlock($blockname);
			$tpl->assign(
				array(
					'elm_idx'	=> $elm_idx,
					'elm_val'	=> $elm_val,
					'selected'	=> $selected
				)
			);
		}
		return TRUE;
	}


/**
 * Расчёт курсов валют и помещение в $_SESSION['catalog_rates']
 * @version 3.0
 * @access	public
 * @param	string	$show	признак необходимости помещения курсов в шаблон
 * @return 	bool
 */
	function get_rates($show = '') {
		global $CONFIG, $main, $tpl;
		if ($CONFIG['catalog_upload_rates_file'] && file_exists(RP.$CONFIG['catalog_upload_rates_file'])) {
			$lines = file(RP.$CONFIG['catalog_upload_rates_file']);
		} else {
			return FALSE;
		}
		foreach ($lines as $idx => $str) {
			$str = preg_replace('/[[:space:]]/i', '', $str);
				if (preg_match('/([[:alpha:]]+)\/([[:alpha:]]+):([[:digit:].]+)/', $str, $arr))	{
				if ($show) $tpl->assign($arr[1].'/'.$arr[2], $arr[3]);
				else {
					$_SESSION['catalog_rates'][$arr[1].'/'.$arr[1]] = 1;
					$_SESSION['catalog_rates'][$arr[2].'/'.$arr[2]] = 1;
					settype($arr[3], 'double');
					$_SESSION['catalog_rates'][$arr[1].'/'.$arr[2]] = $arr[3];
					$_SESSION['catalog_rates'][$arr[2].'/'.$arr[1]] = 1 / $arr[3];
				}
			}
		}
		return TRUE;
	}

/**
 * Вывод сообщения
 * @version	3.0
 * @access	public
 * @param	string	$result_message	сообщение
 * @param	string	$blockname		блок в который выводиться сообшение,
 									по умолчанию в block_result_message файла _result_message.html
 * @return 	bool
 */
	function show_result_message($result_message, $blockname = '') {
		global $CONFIG, $main, $tpl;
 		if (!$blockname) $blockname = 'block_result_message';
		$main->include_main_blocks('_result_message.html', 'main');
		$tpl->prepare();
		$tpl->newBlock($blockname);
		$tpl->assign('result_message', $result_message);
		return TRUE;
	}

/**
 * Вывод меню в админской части
 * @version	3.0
 * @access	public
 * @param	string	$default_action		при отсутствии в $_REQUEST action используется $default_action
 * @return	bool
 */
	function show_admin_menu($default_action = '', $return=false) {
	  GLOBAL $CONFIG, $core, $tpl, $name, $lang, $server, $db, $db1;

		$site_link 		= $core->formPageLink('', '', $lang);
		$lang_query 	= ($CONFIG['multilanguage']) ? 'lang='.$lang.'&' : 'lang=rus&';
		$path_to_scan	= RP.$CONFIG['class_path'];
		$site_pref = substr($server, 0 , -1);
		$site_mods = Main::getSiteModulesNames($site_pref);
		$menu = $ret = array();

		$db->query('SELECT * FROM modules WHERE parent_id="0" ORDER BY rank ASC');
		$i=0;
		if($db->nf()>0) {
		  while ($db->next_record()) {
            $i++;
            $menu_sub = array();            
		    $db1->query('SELECT id, title, name, active, parent_id
		                 FROM modules
		                 LEFT JOIN sites_modules ON id=module_id AND site_id='.$_SESSION['session_cur_site_id'].'
		                 WHERE parent_id='.$db->f('id').' AND active="1"
		                 ORDER BY rank ASC');
		    if ($db1->nf()>0) {
		      $menu['hd'.$i] = array('link' => $db->f('title'));
		      while($db1->next_record()) {
		        if(file_exists($path_to_scan.$db1->f('name').'/lang/'.$db1->f('name').'_'.$CONFIG["admin_lang"].'.php')) {
      				require $path_to_scan.$db1->f('name').'/lang/'.$db1->f('name').'_'.$CONFIG["admin_lang"].'.php';
      			}
		        $menu_sub[$_MSG[$db1->f('name')]] = array('id' => $db1->f('name'), 'link' => '?lang='.$lang.'&name='.$db1->f('name'));
		      }
		    }
            $menu = array_merge($menu, $menu_sub);
            $menu['br'.$i] = array('link'	=> '');
		  }
		}

		//$menu[$this->_msg["View_site"]] = array('id' => 'site', 'link' => $site_link);
		$menu[$this->_msg["Exit"]] = array('id' => 'exit', 'link' => 'logout.php');

		$sub_menu = array();
		$dirs = Main::getDirList($path_to_scan);
	  $count_of_dirs=count($dirs);
		for ($i = 0; $i <= ($count_of_dirs - 1); $i++) {
			if (file_exists($path_to_scan.$dirs[$i].'/admin-sub.php')) {
				require_once $path_to_scan.$dirs[$i].'/admin-sub.php';
				$sub_menu = array_merge($sub_menu, (array)$sub_menu_new);
			}
		}
    if($return){return $ret;}
		foreach ($menu as $key => $val) {
			$main_menu_item = $key;
			$main_menu_item_link = $val['link'];
			$main_menu_id = $val['id'];
			$tpl->newBlock('block_menu');
			if (preg_match('/^hd[0-9]/',$main_menu_item)) {
				$tpl->newBlock('block_heading');
				$tpl->assign(array(
					menu_heading	=>	$main_menu_item_link,
				));
				continue;
			} else if (preg_match('/^br[0-9]/',$main_menu_item)) {
				$tpl->newBlock('block_padding');
				continue;
			} else {
				$tpl->newBlock('block_menu_item');
			}
			$module = "";
			if (strstr($main_menu_item_link, '?'.$lang_query.'name=')) {
				preg_match('/^\?'.$lang_query.'name=([^&]*)/i', $main_menu_item_link, $arr);
				if ($arr[1]) $module = $arr[1];
			}
			if ($module && $module == $name) $img = ' class="curr"';
			else $img = '';
			$tpl->assign(array(
				'main_menu_item'		=>	$main_menu_item,
				'main_menu_item_link'	=>	$main_menu_item_link,
				'img'					=>	$img,
				'main_menu_id'			=>  $main_menu_id
			));
			if ($module && $module == $name && is_array($sub_menu[$module]) && count($sub_menu[$module])>0) {
				if ($_REQUEST['action']) {
					$action = 'action=' . $_REQUEST['action'];
				} else if (!$_REQUEST['action'] && $default_action) {
					$action = 'action='.$default_action;
				}
				$i = 0;
				$tpl->newBlock('block_sub_menu');

				if(file_exists($path_to_scan.$module.'/lang/'.$module.'_'.$CONFIG["admin_lang"].'.php')) {
					require $path_to_scan.$module.'/lang/'.$module.'_'.$CONFIG["admin_lang"].'.php';
				}

				foreach ($sub_menu[$module] as $value) {
					$i++;
					if ($i == 1) $menu_item = $_MSG[$value];
					if ($i == 2) $menu_item_link = $value;
					if ($i == 3) {
						$menu_item_icon = $value;
						$menu_item_class = ($action && (preg_match("/".$action.'$/i',$menu_item_link) || preg_match("/".$action.'&/i',$menu_item_link))) ? 'class=active' : '';
						$tpl->newBlock('block_sub_menu_item');
						$tpl->assign(array(
							'menu_item'			=>	$menu_item,
							'menu_item_link'	=>	$menu_item_link,
							'menu_item_icon'	=>	$menu_item_icon,
							'menu_item_class'	=>	$menu_item_class,
						));
						$i = 0;
					}
				}
			}
		}

		$tpl->assignGlobal('Edit_on_site', $this->_msg['Edit_on_site']);

		return TRUE;
	}

	function getSiteModulesNames($site_pref)
	{   GLOBAL $db;
	    static $ret = array();

		if (! isset($ret[$site_pref])) {
		    $ret[$site_pref] = array();
    		$query = "SELECT M.name FROM sites AS S, modules AS M, sites_modules AS SM";
    		$query .= " WHERE alias='".$site_pref."' AND S.id = site_id AND M.id=module_id";
    		$query .= " AND active";
    		$db->query($query);
    		while ($db->next_record()) {
    			$ret[$site_pref][] = $db->f('name');
    		}
		}

		return $ret[$site_pref];
	}


/**
 * Вывод $len слов из текста $string
 * @version	3.0
 * @access	public
 * @param	string		$string	текст
 * @param	integer		$len	кол-во слов
 * @return	string
 */
	function tokenize($string, $len = 30) {
		$string = strip_tags($string);
		$tok = strtok($string, " \n\t"); $arr[] = $tok;
		while ($tok) {
		    $tok = strtok(" \n\t");
				$arr[] = $tok;
		}
		$newarr		= array_slice($arr, 0, $len);
		$string		= implode(' ', $newarr);
		$string		= (count($arr) > $len+1) ? $string.'...' : $string;
		return $string;
	}

/**
 * Преобразование даты из dd-mm-yyyy в yyyy-mm-dd, для внесения в базу
 * @version	3.0
 * @access	public
 * @param	string	$date	дата
 * @param	string	$hrs	часы
 * @param	string	$min	минуты
 * @return	string
 */
	function transform_date($date, $hrs = "", $min = "") {
		list($day, $month, $year) = preg_split ('/\.|\-|\//', $date);
		$time = ($hrs || $min) ? " $hrs:$min:00" : "";
		$date = "$year-$month-$day$time";
		return $date;
	}

/**
 * Обработки цены для вывода на экран в опр. формате
 * @version	3.0
 * @access	public
 * @param	integer		$price	цена
 * @return	float
 */
	function process_price($price) {
		return $price ? sprintf("%.2f", $price) : "0";
	}

/**
 * Формирование блока страниц
 * �?мена полей и их назначение:
 * $pages = количество страниц,
 * $blockname = название навигационного блока в шаблоне,
 * $action = добавка к урл, $current = текущая страница/начало выборки,
 * $current - номер текущей страницы
 * @return	string
 */
	function _show_nav_block($pages, $blockname = null, $action = null, $current = null, $separator = null) {
		global $tpl, $name, $baseurl, $request_total, $CONFIG, $lang;
		$pages_in_block = $CONFIG["pages_in_block"]; // кол-во ссылок на страницы в одном навигационном блоке
		$nav_string = "";
		$url = $baseurl;
		if ($action) $url .= (strpos($url, "?") === FALSE ? "?" : "&").$action;
		$glue = strpos($url, "?") === FALSE ? "?" : "&";
		// кол-во блоков по $pages_in_block страниц в каждом
		$page_blocks = ceil($pages/$pages_in_block);
		// текущий блок в котором находится страница current
		$current_block = ($current)  ? ceil($current/$pages_in_block) : 1;

		// вывод на экран постраничной навигации
		if ($pages > 1) {
			// есть ли предыдущая страница
			if ($current > 1) {
				$start_str = ($current == 2) ? "" : $glue."start=".($current - 1);
				$nav_string = "<a href=\"$url$start_str\">←</a>" . $nav_string;
			} else {
				$nav_string = "" . $nav_string;
			}
			// есть ли предыдущий блок с $pages_in_block страницами
			if (1 < $current_block) {
				$start_str = $glue."start=" . (($current_block - 1)*$pages_in_block-($pages_in_block-1));
				$nav_string = "<a href=\"$url$start_str\">…</a>" . $nav_string;
			}
			$end = ($current_block*$pages_in_block > $pages) ? $pages : $current_block*$pages_in_block;
			for ($i=(($current_block-1)*$pages_in_block)+1; $i<=$end; $i++) {
				$start_str = ($i == 1) ? "" : $glue."start=$i";
				$link = ($i == $current) ? "<span>$i</span>" : "<a href=\"$url$start_str\">$i</a>";
				$nav_string .= $link;
			}
			// есть ли следующий блок  с $pages_in_block страницами
			if ($current_block < $page_blocks) {
				$start_str = $glue."start=" . (($current_block*$pages_in_block)+1);
				$nav_string = $nav_string . "<a href=\"$url$start_str\">…</a>";
			}
			// есть ли следующая страница
			if ($current < $pages) {
				$start_str = $glue."start=" . ($current + 1);
				$nav_string = $nav_string . "<a href=\"$url$start_str\">→</a>";
			} else {
				$nav_string = $nav_string . "";
			}

			if (!$blockname) $blockname = "nav";
			$tpl->newBlock($blockname);
			$tpl->assign(array(
				"nav_string"	=>	$nav_string ,
			));
		}
		return $nav_string;
	}

/**
 * Разбивка записей по страницам и вывод навигации по ним
 * Имена полей и их назначение:
 * ($table = таблица, $where = условие выборки, $blockname = имя блока,
 * $action = добавка к урл, $current = текущая страница/начало выборки,
 * $rows = кол-во строк, $order_by = порядок выборки, $separator = разделитель)
 * $admin - вызывается ли эта функция из адм. части
 * $only_active - считать в $total только активные/опубликованные записи
 * $admin = выводится навигация в адм.консоли или на основном сайте
 * Записи разбиваются по $rows строк - получаем страницы, страницы в свою
 * очередь разбиваются на блоки по $pages_in_block страниц в каждом
 * @version	3.0
 * @access	public
 * @param
 * @param
 * @param
 * @param
 * @param
 * @param
 * @param
 * @return	string
 */
	function _show_nav_string($table = null, $where = null, $blockname = null, $action = null, $current = null, $rows = 10, $order_by = null, $separator = null, $admin = 1, $only_active = 1, $other_params=null) {
		global $tpl, $name, $baseurl, $request_total, $CONFIG, $lang;
		$current = (int)$current;
		if ($current <= 0) $current = 1;
		if ($order_by) $order_by = "ORDER BY $order_by";
		$having = $group = '';$select=' COUNT(*) as count ';
		if(is_array($other_params)){
			$having = (isset($other_params['having'])) ? 'HAVING '.$other_params['having'] : $having;
			$group = (isset($other_params['group'])) ? 'GROUP BY '.$other_params['group'] : $group;
			$select = (isset($other_params['select'])) ? $other_params['select'] : $select;
		}
		$total = 0;

		if (!$admin && $only_active) $where .= " AND active = 1";

		if(!isset($other_params['result'])){
		  $query = "SELECT $select FROM $table WHERE 1=1 $where $group $having $order_by";
		  $res = db_loadResult($query);
		  if ($res) {
			$total = $res; // кол-во записей в базе
		  }
		}
		//временная заглушка, при невозможности правильно выбрать count
		 else {
		  $total = (int) $other_params['result'];
		}
		if ($total) {
			$GLOBALS["nav_total"]		= $total;
			$GLOBALS["request_total"] = $total;
			//если разбить по $rows записей получится столько страниц
			//округляем в сторону большего положительного числа
            if(!$rows){$rows = 10;}
			$pages = ceil($total/$rows);
			$nav_string = $this->_show_nav_block($pages, $blockname, $action, $current, $separator);
		}
		return $nav_string;
	}

/**
 * Разбивка записей по страницам и вывод навигации по ним
 * Имена полей и их назначение:
 * ($table = таблица, $where = условие выборки, $blockname = имя блока,
 * $action = добавка к урл, $current = текущая страница/начало выборки,
 * $rows = кол-во строк, $order_by = порядок выборки, $separator = разделитель)
 * $admin - вызывается ли эта функция из адм. части
 * $only_active - считать в $total только активные/опубликованные записи
 * $admin = выводится навигация в адм.консоли или на основном сайте
 * Записи разбиваются по $rows строк - получаем страницы, страницы в свою
 * очередь разбиваются на блоки по $pages_in_block страниц в каждом
 * @version	3.0
 * @access	public
 * @param
 * @param
 * @param
 * @param
 * @param
 * @param
 * @param
 * @return	string
 */
	function _show_nav_string_2($col = null, $table = null, $where = null, $blockname = null, $action = null, $current = null, $rows = 10, $order_by = null, $separator = null, $admin = 1, $only_active = 1) {
		global $tpl, $name, $baseurl, $request_total, $CONFIG, $lang;
		$current = (int)$current;
		if ($current <= 0) $current = 1;
		if ($order_by) $order_by = "ORDER BY $order_by";
		$total = 0;

		if (!$admin && $only_active) $where .= " AND active = 1";
           if ($col) {
           	$query = "SELECT COUNT(DISTINCT $col) as count FROM $table WHERE 1=1 $where $order_by";
           } else {
           	$query = "SELECT COUNT(*) as count FROM $table WHERE 1=1 $where $order_by";
		}
		$res = db_loadResult($query);
		if ($res) {
			$total = $res; // кол-во записей в базе
		}

		if ($total) {
			$GLOBALS["nav_total"]		= $total;
			$GLOBALS["request_total"] = $total;
			//если разбить по $rows записей получится столько страниц
			//округляем в сторону большего положительного числа
			$pages = ceil($total/$rows);
			$nav_string = $this->_show_nav_block($pages, $blockname, $action, $current, $separator);
		}
		return $nav_string;
	}


/**
 * Включение в шаблон основных блоков (шапка, низ и т.д.)
 * $template - шаблон представления основной информации на странице
 * если $mode = "main", $template становится главным шаблоном
 * @version	3.0
 * @access	public
 * @param	string	$tamplate
 * @param	string	$mode
 * @return	bool
 */
	function include_main_blocks($template = '_empty.html', $mode = '', $dop = null) {
		global $tpl_path, $tpl, $name, $server, $lang, $PAGE;
		$PAGE['curr_tpl'] = $template;
		if ($mode == 'main') {
			$tpl = new AboTemplate($tpl_path.$template, true);
		} else {
			$tpl->assignInclude('menu',		$tpl_path.'_menu.html');
			$tpl->assignInclude('sub_menu',	$tpl_path.'_sub_menu.html');
			$tpl->assignInclude('main',		$tpl_path.$template);
		}
		if (is_array($dop)) {
		    foreach ($dop as $k=>$v) {
		        if (file_exists($v)) {
		            $tpl->assignInclude($k,$v);
		        }
		    }
		}
		return TRUE;
	}

/**
 * Включение в шаблон основных блоков (шапка, низ и т.д.)
 * $template - шаблон представления основной информации на странице
 * если $mode = "main", $template становится главным шаблоном
 * @version	3.0
 * @access	public
 * @param	string	$tamplate
 * @param	string	$tpl_path_2	путь к шаблону
 * @param	string	$mode
 * @return	bool
 */
	function include_main_blocks_2($template = '_empty.html', $tpl_path_2, $mode = '', $dop = null) {
		global $tpl_path, $tpl, $name, $lang;
		if ($mode == 'main') {
			$tpl = new AboTemplate($tpl_path_2.$template, true);
		} else {
			$tpl->assignInclude('menu',		$tpl_path.'_menu.html');
			$tpl->assignInclude('sub_menu',	$tpl_path.'_sub_menu.html');
			$tpl->assignInclude('main',		$tpl_path_2.$template);
		}
		if(is_array($dop)){foreach($dop as $k=>$v){if(file_exists($v)){$tpl->assignInclude($k,$v);}}}
		return TRUE;
	}

/**
 * Вывод свойств блока данного модуля
 * @version	3.0
 * @access	public
 * @param	string	$field	поле
 * @param	string	$default_action action по умолчанию
 * @param	array	$block_main_module_actions	массив action модуля (главное поле)
 * @param	array	$block_module_actions	массив action модуля
 * @return	bool
 */
	function show_actions($field = "", $default_action = "", $block_main_module_actions = array(), $block_module_actions = array()) {
		global $tpl, $mod, $lang;
		// сначала выясним, имеем мы дело с полем главного модуля или нет
		// и в зависимости от этого используем разные массивы действий
		if ($field == 0 && is_array($block_main_module_actions) && count($block_main_module_actions) > 0) {
			$action_array = $block_main_module_actions;
		} else {
			$action_array = $block_module_actions;
		}
		// список действий, которые пользователь имеет
		// возможность задать для блока с данным модулем
		if (is_array($action_array) && count($action_array) > 0) {
			$tpl->newBlock('block_select_actions');
			foreach($action_array as $act => $descr) {
				$tpl->newBlock('block_available_actions');
				$selected = ($act == $default_action) ? ' selected' : '';
				$tpl->assign(array(
					'action'		=> $act,
					'description'	=> $descr,
					'selected'		=> $selected,
				));
			}
			$tpl->newBlock('block_properties');
		} else {
			$tpl->assign('no_action', 'Для данного блока действия не определены');
			return FALSE;
		}
		return TRUE;
	}

/**
 * Вывод списка страниц, module_id главный модуль, используется для навигационных целей
 * @version	3.0
 * @access	public
 * @param	integer	$mudule_id	id модуля
 * @param	integer	$field	признак текущей страницы
 * @return	bool
 */
	function show_linked_pages($module_id, $field = null) {
		global $tpl, $name, $baseurl, $lang;
		$tpl->newBlock('block_linked_page');
		if (!$field) {
			$tpl->assign("current_page", $this->_msg["Current_page"]);
			return TRUE;
		}
		if ($res = $this->get_linked_pages($module_id)) {
			$tpl->newBlock('block_select_pages');
			foreach ($res as $r) {
				$tpl->newBlock('block_available_pages');
				$tpl->assign(array(
					page_title	=>	$r["title"],
					page_link	=>	$r["link"],
					page_id		=>	$r["page_id"],
				));
			}
			return TRUE;
		}
		$tpl->assign("no_pages", $this->_msg["No_available_redirect_pages"]);
		return FALSE;
	}

/**
 * Получения списка страниц, где module_id идет как главный модуль
 * @version	3.0
 * @access	public
 * @param	integer	$module_id	id модуля
 * @return	array
 */
	function get_linked_pages($module_id) {
		global $lang, $server;
		$module_id = (int)$module_id;
		if (!$module_id) return FALSE;
		$res = db_loadList('SELECT '.$server.$lang.'_pages.title as title,
								   '.$server.$lang.'_pages.link as link,
								   '.$server.$lang.'_pages.address as address,
								   '.$server.$lang.'_pages_blocks.*
							   FROM '.$server.$lang.'_pages_blocks,
							   		'.$server.$lang.'_pages
							   WHERE module_id = '.$module_id.' AND
							   		 field_number = 0 AND
							   		 '.$server.$lang.'_pages.id = page_id
							   GROUP BY '.$server.$lang.'_pages.id
							   ORDER BY page_id');
		return $res;
	}

/**
 * Формирование http адреса данной страницы
 * @version	3.0
 * @access	public
 * @return	string
 */
	function permanent_address() {
		return "Постоянный адрес: <a href=\"" . $_SERVER['REQUEST_URI'] . "\">http://" . $_SERVER['SERVER_NAME'] .  $_SERVER['REQUEST_URI'] . "</a>";
	}

/**
 * Отключение/отмена публикации
 * @version	3.0
 * @access	public
 * @param	string		$table	название таблицы
 * @param	integer		$id		id записи
 * @param	string		$field	столбец
 * @return	bool
 */
	function suspend($table, $id, $field = 'active') {
		$id = (int)$id;
		if (!$id) return FALSE;
		$query = 'UPDATE '.$table.' SET '.$field.' = 0 WHERE id = '.$id;
		db_exec($query);
		return TRUE;
	}

/**
 * Включение публикации
 * @version	3.0
 * @access	public
 * @param	string		$table	название таблицы
 * @param	integer		$id		id записи
 * @param	string		$field	столбец
 * @return	bool
 */
 function activate($table, $id, $field = 'active') {
		global $lang;
 		$id = (int)$id;
		if (!$id) return FALSE;
		$query = 'UPDATE '.$table.' SET '.$field.' = 1 WHERE id = '.$id;
		db_exec($query);
		return TRUE;
	}

	function arr($POST, $NAME = NULL) {
		echo "<table cellspacing=1 cellpadding=2 border=0 class=arr_table><th style='font-size: 10pt; background: gray; color: black;'>".$NAME."</th><tr><td><pre style='font-size: 8pt; background: silver; color: black;'>";
		print_r($POST);
		echo "</pre></td></tr></table>";
	}

/**
 * Проверка возможности загрузить файлы
 * @version	3.0
 * @access	private
 * @param	string	$file	имя файла
 * @return	bool	FASLE в случае невозможности загрузить (по соображениям безопасности), в допустимости загрузки TRUE
 */
    function testFiles($file) {
		if (preg_match("/\.(phtml|php|php3|php4|php5|shtml|cgi|exe|pl|asp|aspx|htaccess|htgroup|htpasswd)$/i", $file)) {
			$this->message_die("<span class=\"title\">Недопустимый формат файла ".$file."!</span>");
			return FALSE;
		}
		return TRUE;
	}

/**
 * Загрузка массива файлов на сервер
 * @version	3.0
 * @access	public
 * @param	string	$formname		имя тега
 * @param	integer	$max_size		максимальный размер загружаемого файла
 * @param	string	$path			путь
 * @param	string	$dir			директория
 * @param	array	$file_formats	допустимые форматы файла
 * @param	string	$set_file_name	имя которое будет присвоенно загруженному файлу
 * @return	bool
 */
	function uploadFiles($formname, $max_size, $path, $dir = null, $file_formats = array(), $lid = "") {
		global $lang, $db;
        foreach($_FILES[$formname]['name'] AS $key => $v) {
        	foreach($v AS $k => $val) {
        		foreach($val AS $k2 => $v2) {
					if (!$this->testFiles($val[$k2])) {
                    	return FALSE;
                	}
            	}
        	}
		}
		foreach($_FILES[$formname]['name'] AS $key => $v) {
			foreach($v AS $k => $val) {
				if (@isset($key) && !@is_numeric($key)) {
					if (isset($val) && $val != '') {
                    	$new_name = $lid.'_other_'.$val;
						unlink($path.$key);
    	            	if (move_uploaded_file($_FILES[$formname]['tmp_name'][$key][$k], $path.$new_name)) {
							@chmod($path.$new_name, 0757);
							$fls_chg[$key] = $new_name;
						}
					}
				} elseif (@isset($key) && @is_numeric($key)) {
        				foreach($val AS $k2 => $v2) {
						$new_name = $lid.'_'.$key.'_'.$k2.'_'.$val[$k2];
				    	if (move_uploaded_file($_FILES[$formname]['tmp_name'][$key][$k][$k2], $path.$new_name)) {
					    	@chmod($path.$new_name, 0757);
                        	$fls_chg[] = $new_name;
				    	}
	                }
				}
			}
		}
        if ($fls_chg) {return $fls_chg;}
    	return TRUE;
	}


/**
 *	Загрузка файла на сервер
 * @version	3.0
 * @access	public
 * @param	string	$formname		имя тега
 * @param	integer	$max_size		максимальный размер загружаемого файла
 * @param	string	$path			путь
 * @param	string	$dir			директория
 * @param	array	$file_formats	допустимые форматы файла
 * @param	string	$set_file_name	имя которое будет присвоенно загруженному файлу
 * @return	bool
 */
	function upload_file($formname, $max_size, $path, $dir = null, $file_formats = array(), $set_file_name = "") {
    if (is_array($_FILES[$formname]['name'])) {
      return $this->uploadFiles($formname, $max_size, $path, $dir, $file_formats, $set_file_name);
		}
    if (preg_match("/\.(phtml|php|php3|php4|php5|shtml|cgi|exe|pl|asp|aspx|htaccess|htgroup|htpasswd)$/i", $_FILES[$formname]['name'])) {
			$this->message_die("<span class=\"title\">Недопустимый формат файла " . $_FILES[$formname]['name'] . "!</span>");
			return false;
		}
		if (!$_FILES[$formname]['name']) {
			$this->message_die("<span class=\"title\">Ошибка закачки файла " . $_FILES[$formname]['name'] . "!</span>");
			return false;
		}
		$new_name = $_FILES[$formname]['name'];
		if ($set_file_name) $new_name = $set_file_name;
		// проверка формата файла
		if (count($file_formats)>0) {
			foreach ($file_formats as $key => $val) {
				if ($_FILES[$formname]['type'] == $key) {
					$format = $val;
					break;
				}
			}
			if (!$format) {
				$this->message_die("<span class=\"title\">Недопустимый формат файла " . $_FILES[$formname]['type'] . "!</span>");
				return false;
			}
		}
		// проверка размера файла
		if ($max_size) {
			if ($_FILES[$formname]['size'] > $max_size) {
				$this->message_die("<span class=\"title\">Слишком большой размер файла!</span>");
				return false;
			}
		}
		// проверка существования папки с файлами по указанному пути
		if ($dir) {
			$path = $path . $dir . "/";
		}
        Main::checkPath($path);
		if (!is_dir($path)) {
			$this->message_die("<span class=\"title\">Неправильный путь закачки $path!</span>");
			return false;
		}
		// если файл с таким именем существует, то присвоить новое имя
    if (!$set_file_name && file_exists($path.$new_name)) {
      $file_name = $new_name;
      $pos = strrpos($file_name, ".");
      if (!$format && $pos) {
           $format = substr($file_name, $pos + 1, strlen($file_name));
      }
      do {
        $new_name =  $pos
               ? substr($file_name, 0, $pos).'-'.mt_rand(10,9999999).'.'.$format
               : $file_name.'-'.mt_rand(10,9999999).'.'.$format;
      } while (file_exists($path.$new_name));
    }
		// переместить закачанный файл из временной папки в указанную папку $path
		if (move_uploaded_file($_FILES[$formname]['tmp_name'], $path.$new_name)) {
			@chmod($path.$new_name, 0757);
			return $new_name;
		}
		$this->message_die('<span class="title">Ошибка закачки файла '.$path.$new_name.'!</span>');
		return false;
	}

/**
 * Редактирования значения rank (для порядка сортировки), cдвигает элемент вверх или вниз в зависимости от
 * параметра operation (up || down)
 * @version	3.0
 * @access	public
 * @param	string	$operation			операция (down - сместить вниз, любое другое установленное значение смещает вверх)
 * @param	string	$table				таблица
 * @param	string	$field				название rank-поля
 * @param	integer	$curren_rank		текущий ранк
 * @param	integer	$shift				на сколько позиций сдвинуть
 * @param	string	$where_additional	дополнительное условие для запроса
 * @return	bool
 */
    function change_order($operation, $table, $field, $current_rank, $shift, $where_additional = null)
    {   global $db;
		$shift			= $shift>1 ? (int)$shift + 1 : 2;
		$current_rank	= (int)$current_rank;
		if ($operation == "down") {
			$query = "SELECT id, {$field} FROM {$table} WHERE {$where_additional} {$field} >= {$current_rank} ORDER BY {$field} ASC LIMIT {$shift}";
		} else {
			$query = "SELECT id, {$field} FROM {$table} WHERE {$where_additional} {$field} <= {$current_rank} ORDER BY {$field} DESC LIMIT {$shift}";
		}		
		$res = $db->getArrayOfResult($query);
		if (count($res) > 1) {
		    $start = $res[0][$field];
		    $rank = $operation == "down"
		        ? $start + $shift - 1
		        : $start - $shift + 1;
			$db->query("UPDATE {$table} SET {$field}={$rank} where id = {$res[0]['id']}");
		    for ($i=1; $i<count($res); $i++) {
		        $rank = $operation == "down"
    		        ? $start + $i - 1
    		        : $start - $i + 1;
    		    $db->query("UPDATE {$table} SET {$field}={$rank} where id = {$res[$i]['id']}");
		    }
		    		    
			return TRUE;
		}
		return FALSE;
	}

/**
 * Получение максимального и минимального rank из указанной таблицы
 * @version	3.0
 * @access	public
 * @param	string	$table	название таблицы
 * @param	string	$field	название поля rank
 * @param	string	$where	дополнительное условие
 * @return	array
 */
	function get_max_min_rank($table, $field = "rank", $where = "") {
		global $lang;
		$query = "SELECT MAX($field) as max_rank, MIN($field) as min_rank FROM $table $where";
		$res = db_loadResult2($query);
		if ($res) {
			$max_rank	= $res["max_rank"];
			$min_rank	= $res["min_rank"];
		}
		if (!$max_rank) $max_rank = 1;
		if (!$min_rank) $min_rank = -1;
		return array($max_rank,$min_rank);
	}

/**
 * Вывод сообщения об ошибке, и прекращение работы скрипта
 * @version	3.0
 * @access	public
 * @param	string	$msg_text	текст сообщение
 * @return	void
 */
	function message_die($msg_text) {
		die($msg_text . "	<form><input type=button value=\" ".$this->_msg["Back"]." \" onClick=\"javascript:history.back();\"></form>");
	}

/**
 * Вывод сообщения об отсутствии прав доступа (в адм.части)
 * @version	3.0
 * @access	public
 * @param	string	$name	имя модуля
 * @param	string	$action	операция (action)
 * @return	void
 */
	function message_access_denied($name, $action) {
		$r = ($_SESSION['permissions'][$name]["r"]) ? $this->_msg["available"] : $this->_msg["no"]; // чтение
		$e = ($_SESSION['permissions'][$name]["e"]) ? $this->_msg["available"] : $this->_msg["no"]; // редактирование
		$p = ($_SESSION['permissions'][$name]["p"]) ? $this->_msg["available"] : $this->_msg["no"]; // публикация
		$d = ($_SESSION['permissions'][$name]["d"]) ? $this->_msg["available"] : $this->_msg["no"]; // удаление
		$this->message_die("
		<pre>
		".$this->_msg['Module'].": $name
		".$this->_msg['Operation'].": $action
		<span style=\"color: red;\">".$this->_msg['You_do_not_have_access']."</span>

		".$this->_msg['Your_permissions'].":
		".$this->_msg['read'].": $r
		".$this->_msg['write/add'].": $e
		".$this->_msg['publication/turn_on'].": $p
		".$this->_msg['delete'].": $d
		</pre>
		");
	}

/**
 * Вывод сообщения об ошибках; по умолчанию (404 page not found)
 * @version	3.0
 * @access	public
 * @return	void
 */
	function no_such_page($err_code='404') {
		global $main, $CONFIG, $server, $lang, $lc, $tpl;
        $_msg = array (
            '404' => array ('header'=> 'HTTP/1.0 404 Not Found',   'tpl'=>'404.html'),
            '500' => array ('header'=> 'HTTP/1.0 500 MySQL Error', 'tpl'=>'510.html'),
        );
        if(isset($_msg[$err_code]['header'])){
            @header($_msg[$err_code]['header']);
        }


		$address = $CONFIG["web_address"] . $_SERVER["REQUEST_URI"];
		$address = str_replace("http://", "http:::", $address);
		$address = str_replace("//", "/", $address);
		$address = str_replace("http:::", "http://", $address);
		$address = preg_replace(("/[(\;)(\|)(\')(\")(\!)(\<)(\>)]/"), "", $address);

		if($lc) $lc->get_internal_links_from_file();
		$map_link = Core::formPageLink($CONFIG["map_page_link"], $CONFIG["map_page_address"], $lang);
		$home_link = Core::formPageLink("", "", $lang);
        if(isset($_msg[$err_code]['tpl']) && file_exists("tpl/{$server}{$lang}/{$_msg[$err_code]['tpl']}")){
    		$main->include_main_blocks_2($_msg[$err_code]['tpl'], 'tpl/'.$server.$lang.'/', 'main');
    		$tpl->prepare();
    		$tpl->assign(
    			array(
    				"_ROOT.address"				=>	$address,
    				"_ROOT.map_link"			=>	$map_link,
    				"_ROOT.home_link"			=>  $home_link,
    				"_ROOT.contact_email"		=>  $CONFIG["contact_email"],
    			)
    		);
    		$tpl->printToScreen();
    		$content = ob_get_contents();
    		ob_end_clean();
    		if (@$_SERVER["HTTP_ACCEPT_ENCODING"] && FALSE !== strpos($_SERVER["HTTP_ACCEPT_ENCODING"], 'gzip')
    		    && $CONFIG['front_office_zip'] > 0 && $CONFIG['front_office_zip'] <= 9) {
    			$content = gzencode($content, $zip);
    			header('Content-Encoding: gzip');
    		}
    		@header('Content-Length: '.strlen($content));
    		echo $content;
        }

		exit;
	}

/**
 * Вывод формы авторизации
 * @version	3.0
 * @access	public
 * @return	void
 */
	function login_form_page() {
		global $main, $CONFIG, $server, $lang, $lc, $tpl, $request_mode;

		$address = $CONFIG["web_address"] . $_SERVER["REQUEST_URI"];
		$address = str_replace("http://", "http:::", $address);
		$address = str_replace("//", "/", $address);
		$address = str_replace("http:::", "http://", $address);
		$address = preg_replace(("/[(\;)(\|)(\')(\")(\!)(\<)(\>)]/"), "", $address);

		$lc->get_internal_links_from_file();
		$map_link = Core::formPageLink($CONFIG["map_page_link"], $CONFIG["map_page_address"], $lang);
		$home_link = Core::formPageLink("", "", $lang);

		$main->include_main_blocks_2('login.html', 'tpl/'.$server.$lang.'/', 'main');
		$tpl->prepare();
		$tpl->assign(
			array(
				"_ROOT.address"				=>	$address,
				"_ROOT.map_link"			=>	$map_link,
				"_ROOT.home_link"			=>  $home_link,
				"_ROOT.contact_email"		=>  $CONFIG["contact_email"],
			)
		);

		if($request_mode == "remind")
		{
			$tpl->newBlock("block_remind_form");
			$tpl->newBlock("block_remind_text");
		}
		else
		{
			$tpl->newBlock("block_authorize_form");
			$tpl->newBlock("block_remind_link");
		}

		if($request_mode == "logged_in")
		{
			$tpl->newBlock("block_logged_in");
		}
		elseif($request_mode == "logged_out")
		{
			$tpl->newBlock("block_logged_out");
		}
		elseif($request_mode == "access_denied")
		{
			$tpl->newBlock("block_access_denied");
		}
		elseif($request_mode == "incorrect")
		{
			$tpl->newBlock("block_incorrect");
		}
		elseif($request_mode == "reminded")
		{
			$tpl->newBlock("block_reminded");
		}
		elseif($request_mode == "mail_error")
		{
			$tpl->newBlock("block_mail_error");
		}
		elseif($request_mode == "not_found")
		{
			$tpl->newBlock("block_not_found");
		}
		elseif(!$request_mode)
		{
			$tpl->newBlock("block_default");
		}

		$tpl->printToScreen();
		$content = ob_get_contents();
		ob_end_clean();
		if (@$_SERVER["HTTP_ACCEPT_ENCODING"] && FALSE !== strpos($_SERVER["HTTP_ACCEPT_ENCODING"], 'gzip')
		    && $CONFIG['front_office_zip'] > 0 && $CONFIG['front_office_zip'] <= 9) {
			$content = gzencode($content, $zip);
			header('Content-Encoding: gzip');
		}
		header('Content-Length: '.strlen($content));
		echo $content;
		exit;
	}

/**
 * Получение времени в микросекундах
 * @version	3.0
 * @access	public
 * @return	float
 */
	function getmicrotime(){
		list($usec, $sec) = explode(" ",microtime());
		return ((float)$usec + (float)$sec);
	}

/**
 * Получение содержимого файла
 * @version	3.0
 * @access	public
 * @param	string	$filename			имя файла
 * @param	integer	$use_include_path	искать в include_path (при равестве TRUE) или нет (FALSE)
 * @return	string
 */
	function file_get_contents($filename, $use_include_path = 0) {
		$data = '';
		$file = @fopen($filename, 'r', $use_include_path);
		if ($file) {
		  while (!feof($file)) $data .= fread($file, 1024);
		  fclose($file);
		}
		return $data;
  }

/**
 * Генерация изображения из исходного
 * @version	3.0
 * @access	public
 * @param	string			$source			источник
 * @param	string			$destination	создаваемый файл
 * @param	integer			$w				высота
 * @param	integer			$h				ширина
 * @param	string			$action_type	тип действия ("resize", "square", "")
 * @param	string			$filetype		тип файла ("image/jpeg", "image/pjpeg", "image/jpg", "image/gif", "image/x-png")
 * @param	string			$string			строка для вывода поверх изображения
 * @param	integer			$jpeg_quality	качество jpeg картинки
 * @return	bool
 */
	function generate_image($source, $destination, $w, $h, $action_type = "", $filetype = "", $string = "", $jpeg_quality = "85")
	{   global $CONFIG;

		$size = getimagesize($source);
		if (!$filetype) {
		    $filetype = $size["mime"];
		}
		if ($filetype == "image/jpeg" || $filetype == "image/pjpeg" || $filetype == "image/jpg") {
		    $src = imagecreatefromjpeg($source);
		} else if ($filetype == "image/gif") {
		    $src = imagecreatefromgif($source);
		} else if ($filetype == "image/x-png") {
		    $src = imagecreatefrompng($source);
		} else {
		    return 0;
		}
		$dest = imagecreatetruecolor($w, $h) or $this->message_die("Cannot Initialize new GD image stream");
		if ($src && $dest) {
		    list($sw, $sh) = $size;
			// поменять размеры картинки
			if ($action_type == "resize") {
			    $x = $y = 0;
			    if ((($w*$sh)/$sw)>$h) {
			        $y = abs(round(($sh - ($h*$sw/$w) )/2));
			        $sh = round(($h*$sw)/$w);
			    } elseif ((($w*$sh)/$sw)<$h) {
			        $x = abs(round(($sw - ($w*$sh/$h) )/2));
			        $sw = round(($w*$sh)/$h);
			    }
        		if ($w>$h || $w<$h) {
        		    imagecopyresized($dest, $src, 0, 0, $x, $y, $w, $h, $sw, $sh);
        		}
        		if ($w==$h) {
        		    imagecopyresized($dest, $src, 0, 0, round((max($sw,$sh)-min($sw,$sh))/2), 0, $w, $w, min($sw,$sh), min($sw,$sh));
        		}
			// сделать картинку квадратной
			} else if ($action_type == "square") {
				if ($sw > $sh) {
					$tmp = imagecreate($sh, $sh) or $this->message_die("Cannot Initialize new GD image stream");
					imagecopyresized($dest, $src, 0, 0, 0, 0, $w, $h, $sh, $sh);
				} else if ($sw < $sh) {
					$tmp = imagecreate($sw, $sw) or $this->message_die("Cannot Initialize new GD image stream");
					imagecopyresized($dest, $src, 0, 0, 0, 0, $w, $h, $sw, $sw);
				} else {
					imagecopyresized($dest, $src, 0, 0, 0, 0, $w, $h, $sw, $sh);
				}
				@imagedestroy($tmp);
			} else {
			// скопировать часть картинки
				imagecopyresized($dest, $src, 0, 0, 0, 0, $w, $h, $w, $h);
			}
			if ($string) {
				$bg = ImageColorAllocateAlpha($dest, 0, 0, 0, 200);
				$text_color = ImageColorAllocate($dest, 255, 255, 255);
				imagefilledrectangle ($dest, 0, 0, 6+strlen($string)*8, 23, $bg); # Make transparent
				imagestring($dest, 3, 5, 5, $string, $text_color);
			}
			if ($filetype == "image/jpeg" || $filetype == "image/pjpeg" || $filetype == "image/jpg") {
				imagejpeg($dest, $destination, $jpeg_quality);
			} else if ($filetype == "image/gif") {
				imagegif($dest, $destination);
			} else if ($filetype == "image/x-png" || $filetype == "image/png") {
				imagepng($dest, $destination);
			}
			@imagedestroy($src);
			@imagedestroy($dest);
			return TRUE;
		}
		return FALSE;
	}

/**
 * Подгонка картинки по ширине
 * @version	3.0
 * @access	public
 * @param	string			$imagename		имя файла изображения
 * @param	string			$path			к изображение путь
 * @param	integer			$max_width		максимальная ширина картинки
 * @param	string			$filetype		тип файла ("image/jpeg", "image/pjpeg", "image/jpg", "image/gif", "image/x-png")
 * @param	string			$string			строка для вывода поверх изображения
 * @param	integer			$jpeg_quality	качество jpeg картинки
 * @return	bool
 */
	function resize_by_width($imagename, $path, $max_width, $filetype = "", $string = NULL, $jpeg_quality = 100) {
		GLOBAL $CONFIG;
		if (file_exists($path.$imagename)) {
			list($w, $h) = getimagesize($path.$imagename);
			if ($w > $max_width) {
				$dh	= (int)$h*($max_width/$w);
				if ($this->generate_image($path.$imagename,
										  $path.$imagename,
										  $max_width,
										  $dh,
										  'resize',
										  $filetype,
										  $string,
										  $jpeg_quality)) {
					return TRUE;
				}
			}
		}
		return TRUE;
	}

	function get_dependent_css($page_addr) {
		global $CONFIG, $lang;
		$is_load_top_lvl = false;
		$ret = '';
		$file_names = explode('/', $page_addr);
		$lang_ = $CONFIG['multilanguage'] == 'on' ? $lang.'/' : '';
		for ($i=0; $i<count($file_names); $i++){
			if ($file_names[$i]) {
			    $is_top_level = in_array($file_names[$i], $CONFIG['top_level_links']);
				if (file_exists(RP.$CONFIG['css_path'].$lang_.$file_names[$i].'.css')){
					$ret .= '<link rel="StyleSheet" type="text/css" href="/'.$CONFIG['css_path'].$lang_.$file_names[$i].'.css">';
					if ($is_top_level) {
					    $_COOKIE['tl'] = $file_names[$i];
						setcookie('tl', $file_names[$i], time() + (3600 * 24 * $CONFIG['cookie_lt']), "/");
						$is_load_top_lvl = true;
					}
				}
			}
		}
		if (! $is_load_top_lvl && $_COOKIE['tl'] && file_exists(RP.$CONFIG['css_path'].$lang_.$_COOKIE['tl'].'.css')) {
			$ret .= '<link rel="StyleSheet" type="text/css" href="/'.$CONFIG['css_path'].$lang_.$_COOKIE['tl'].'.css">';
		}
		return $ret;
	}

	/**
	 * Увеличить счётчик просмотров сущности
	 *
	 * @param string $table
	 * @param string $id
	 * @param string $id_firld_name
	 * @return boolean
	 */
	function addHits($table, $id, $id_firld_name = 'id')
	{   global $db;

		$query = "UPDATE ".My_Sql::escape($table)." SET hits=IF(hits > 0, hits + 1, 1)";
		$query .= " WHERE ".My_Sql::escape($id_firld_name)."='".My_Sql::escape($id)."'";
		return $db->query($query) ? TRUE : FALSE;
	}
	function message_noright($mdl){
		global $tpl;
		$tpl->tpl_include['main'][0] = "tpl/admin/_novalid.html";
		$tpl->__prepare();
	}
    function checkPath($dir){
        $parent = dirname($dir);
        if(!is_dir($parent)){Main::checkPath($parent);}
        if(!is_writable($parent)){return false;}
        if(!is_dir($dir)) @mkdir($dir, 0777);
		 else chmod($dir, 0777);
        return true;
    }
    
    
	
	function getFileExt($filename)
	{
		$ext = strrchr($filename, '.');
		return $ext ? strtolower(substr($ext, 1)) : false;
	}
	
	function getImgExt($filename)
	{
		$ret = false;
		$a = @getimagesize($filename);
		if (! empty($a)) {
		    switch ($a['mime']) {
		    	case 'image/gif':
		    		$ret = 'gif';
		    		break;
		    	case 'image/jpeg':
		    	case 'image/pjpeg':
		    	case 'image/jpg':
		    		$ret = 'jpg';
		    		break;
		    	case 'image/x-png':
		    	case 'image/png':
		    		$ret = 'png';
		    		break;
		    	default:
		    		break;
		    }
		}
		
		return $ret;
	}
    
    function generateImage($source, $destination, $param = array())
	{
	    if (! file_exists($source)) {
	        //throw new Exeption('Source file not found');
	        return false;
	    }
	    if (! $destination) {
	        //throw new Exeption('Not defined destination file');
	        return false;
	    }
	    $param['method'] = $param['method'] ? $param['method'] : 'crop';
		$size = getimagesize($source);
		list($sw, $sh) = $size;
		if ( ($param['source_min_width'] > 0 && $sw < $param['source_min_width'])
		    || (($param['source_min_height'] > 0 && $sh < $param['source_min_height']))
		) {
		    return false;
		}
		$w = $param['width'] > 0 ? (int)$param['width'] : 0;
		$h = $param['height'] > 0 ? (int)$param['height'] : 0;
		$w = $w <= 0 ? $sw : $w;
		$h = $h <= 0 ? $sh : $h;
		$filetype = $size["mime"];
		if ($filetype == "image/jpeg" || $filetype == "image/pjpeg" || $filetype == "image/jpg") {
		    $src = imagecreatefromjpeg($source);
		    $type = 'jpg';
		} else if ($filetype == "image/gif") {
		    $src = imagecreatefromgif($source);
		    $type = 'gif';
		} else if ($filetype == "image/x-png" || $filetype == 'image/png') {
		    $src = imagecreatefrompng($source);
		    $type = 'png';
		} else {
		    //throw new Exeption('Not defined mime type - '.$filetype);
		    return false;
		}
		$kw = $sw/$w;
		if ($kw < 1) {
		    $kw = 1;
		}
    	$kh = $sh/$h;
    	if ($kh < 1) {
		    $kh = 1;
		}
    	if ($kw <= 1 && $kh <= 1) {
    	    copy($source, $destination);
    	    @imagedestroy($src);
    	    
    	    return TRUE;
    	}
    	
		if ('crop' == $param['method']) {
		    $k = min($kw, $kh);
    	    $dw = $w;
    	    $dh = $h;    		
    	} else {
    	    $k = max($kw, $kh);
    		$dw = floor($sw/$k);
    		$dh = floor($sh/$k);
    	}
		if (! ($dest = imagecreatetruecolor($dw, $dh)) ) {
		    //throw new Exeption('Can not create destination file');
		    return false;
		}
		
		if ('png' == $type) {
    		$trans_index = imagecolortransparent($src);
    		$trans_color = array('red' => 255, 'green' => 255, 'blue' => 255);
    		if ($trans_index >= 0) {
    		    $trans_color = imagecolorsforindex($src, $trans_index);   
    		}
    		$trans_index = imagecolorallocate($dest, $trans_color['red'], $trans_color['green'], $trans_color['blue']);
    		imagefill($dest, 0, 0, $trans_index);
    		imagecolortransparent($dest, $trans_index);
    	} else if ('gif' == $type) {
    		$color_count = imagecolorstotal($src);
    		imagetruecolortopalette($dest, true, $color_count);
    		imagepalettecopy($dest, $src);
    		$transparentcolor = imagecolortransparent($src);
    		imagefill($dest, 0, 0, $transparentcolor);
    		imagecolortransparent($dest, $transparentcolor);
    	}
    	
    	if ('crop' == $param['method']) {
			imagecopyresampled($dest, $src, 
			    0, 0,
			    round(($sw - ($w*$k))/2), round(($sh - ($h*$k))/2),
			    $w, $h,
			    round($w*$k), round($h*$k) );
    	} else {
    	    imagecopyresampled($dest, $src, 0, 0, 0, 0, $dw, $dh, $sw, $sh );
    	}
		if ($filetype == "image/jpeg" || $filetype == "image/pjpeg" || $filetype == "image/jpg") {
			imagejpeg($dest, $destination, 100);
		} else if ($filetype == "image/gif") {
			imagegif($dest, $destination);
		} else if ($filetype == "image/x-png" || $filetype == "image/png") {
			imagepng($dest, $destination);
		}
		@imagedestroy($src);
		@imagedestroy($dest);
		
		return TRUE;
	}
}

class My_Sql {
	var $Host     = "";
	var $Database = "";
	var $User     = "";
	var $Password = "";

/**
 * Установка в 1 вызывает автоматическое освобождение памяти от результата (automatic mysql_free_result()), по умолчанию 0
 */
	var $Auto_Free     = 0;
/**
 * Установка в 1 разрешает вывод отладочный сообщений, по умолчанию 0
 */
	var $Debug         = 0;
/**
 * "Yes" завершение с сообщением об ошибке, "no" игнорировать ошибки, "report" игнорировать ошибки по выводить предупреждения. По умолчанию "yes"
 */
	var $Halt_On_Error = "yes";
/**
 * При установке в 1 используется постоянное подключение, по умолчанию 0
 */
	var $PConnect      = 0;
	var $Seq_Table     = "db_sequence";

/**
 * Результирующий массив и номер текущей строки
 */
	var $Record   = array();

	var $Row;

/**
 * Номер текущей ошибки
 */
  var $Errno    = 0;

/**
 * Иписание текущей ошибки
 */
  var $Error    = "";

  var $type     = "mysql";
  var $revision = "1.2";

  var $Link_ID  = 0;
  var $Query_ID = 0;

  var $locked   = false;      ## set to true while we have a lock
  var $log = false;
  var $sql = '';

  /* public: constructor */
  function DB_Sql($query = "") {
      $this->query($query);
  }
  function log($file = "") {
      if(!empty($file)){$this->log_file = $file; $this->log = true;}
  }
/**
 * Экранирование параметров
 * @version 3.0
 * @param	string	$query	параметр, в зависимости от значения magic_quotes_gpc() параметр либо экранируются, либо нет
 * @return	string	экранированный параметр
 */
  function escape($query) {
	if (!get_magic_quotes_gpc()) {
	    if (is_array($query)) {
	        foreach ($query as $key => $val) {
	            $query[$key] = mysql_real_escape_string(trim($val));
	        }
	    } else {
	        $query = mysql_real_escape_string(trim($query));
	    }
	} else {
	    if (is_array($query)) {
	        foreach ($query as $key => $val) {
	            $query[$key] = trim($val);
	        }
	    } else {
	        $query = trim($query);
	    }
	}
  	return $query;
  }

/* public: some trivial reporting */
  function link_id() {
    return $this->Link_ID;
  }

  function query_id() {
    return $this->Query_ID;
  }

/**
 * Подключение к базе
 * @version	3.0
 * @param	string	$database	название БД
 * @param	string	$host		хост
 * @param	string	$user		логин
 * @param	string	$password	пароль
 * @return	mixed	FALSE в случае неудачной попытки подключения, иначе id подключения
 */
	function connect($database = '', $host = '', $user = '', $password = '') {
		if ('' == $database)	$database = $this->Database;
		if ('' == $host) 		$host     = $this->Host;
		if ('' == $user)		$user     = $this->User;
		if ('' == $password)	$password = $this->Password;

		if (0 == $this->Link_ID) {
			if(!$this->PConnect) {
				$this->Link_ID = mysql_connect($host, $user, $password);
			} else {
				$this->Link_ID = mysql_pconnect($host, $user, $password);
			}
			if (!$this->Link_ID) {
				if($this->log) error_log(mysql_error(), 3, $this->log_file);
				$this->halt('connection to database failed!');
				//$this->halt('connect('.$host.', '.$user.', '.$password.') failed.');
				return FALSE;
			}
			if (!@mysql_select_db($database, $this->Link_ID)) {
				$this->halt('cannot use database!');//.$database);
				return FALSE;
			}
		}
		return $this->Link_ID;
	}

/**
 * Уничтожение результата
 */
  function free() {
    if(gettype($this->Query_ID)=='resource' && get_resource_type ($this->Query_ID)=='mysql result'){
        @mysql_free_result($this->Query_ID);
    }
    $this->Query_ID = 0;
  }

/**
 * Выполнение SQL запроса
 * @version	3.0
 * @param	string	$query_string	SQL запрос
 * @return	mixed	FALSE в случае неудачи, иначе id запроса
 */
	function query($query_string) {
		global $CONFIG, $queries;
		if ('' == $query_string )	return FALSE;
		if (!$this->connect())		return FALSE;
		if ($this->Query_ID)		$this->free();
		if ($this->Debug)			printf("Debug: query = %s<br>\n", $query_string);
		if (@$CONFIG['debug'] || isset($_REQUEST['debug'])) $queries[] = $query_string;
		#$query_string = preg_replace('~SELECT(.*)FROM(.*)(LEFT|INNER)\s*JOIN~is', 'SELECT $1 FROM ($2) $3 JOIN', $query_string);

		if($this->log) {
			$t = date('H:i:s '.microtime());
			error_log('begin: '.$t."\t", 3, $this->log_file);
			error_log($query_string, 3, $this->log_file);
		}
		$this->sql = $query_string;
		$this->Query_ID = @mysql_query($query_string,$this->Link_ID);
		if($this->log) {
			$t = date('H:i:s '.microtime());
			error_log("\tend: ".$t."\t", 3, $this->log_file);
			error_log("\n", 3, $this->log_file);
		}

		$this->Row   = 0;
		$this->Errno = mysql_errno();
		$this->Error = mysql_error();
		if($this->Error && $this->log) error_log("Error: ".$this->Error."\n", 3, $this->log_file);
		if (!$this->Query_ID)		$this->halt('Invalid SQL: '.$query_string);
		return $this->Query_ID;
	}

/**
 * Возвращает двумерный массив результата выполнения SQL запроса $query_string
 * @author 	Михалёв Денис <denis_m@armex.ru>
 * @version	3.0
 * @param	string	$query_string	SQL запрос
 * @return	mixed	FALSE в случае неудачи, массив в случае успеха
 */
	function getArrayOfResult($query_string) {
		if ($this->query($query_string)) {
			while ($arr = mysql_fetch_assoc($this->Query_ID)) {
				$val[] = $arr;
			}
			if (is_array($val)) {
				return $val;
			}
		}
		return FALSE;
	}
	
	function getFstRow($query_string) {
	    $ret = false;
		if ($this->query($query_string)) {
			while ($arr = mysql_fetch_row($this->Query_ID)) {
				$ret[] = $arr[0];
			}
		}
		return $ret;
	}

/**
 * Получение следующей строки результирующего набора данных
 * @version	3.0
 * @return	mixed	FALSE в случае неудачи, иначе следующая строка результата
 */
	function next_record() {
		if (!$this->Query_ID) {
			$this->halt('next_record called with no query pending.');
			return FALSE;
		}

		$this->Record = @mysql_fetch_array($this->Query_ID);
		$this->Row   += 1;
		$this->Errno  = mysql_errno();
		$this->Error  = mysql_error();

		$stat = is_array($this->Record);
		if (!$stat && $this->Auto_Free) {
			$this->free();
		}
		return $stat;
	}
	
	function next() {
		if (! is_resource($this->Query_ID)) {
			$this->halt('next_record called with no query pending.');
			return FALSE;
		}

		$this->Record = @mysql_fetch_assoc($this->Query_ID);
		$this->Row   += 1;
		$this->Errno  = mysql_errno();
		$this->Error  = mysql_error();

		$stat = is_array($this->Record);
		if (!$stat && $this->Auto_Free) {
			$this->free();
		}
		return $stat;
	}

/**
 * Установка курсора разультата в новую позицию
 * @version	3.0
 * @param	int		$pos	новая позиция курсора
 * @return
 */
	function seek($pos = 0) {
		$status = @mysql_data_seek($this->Query_ID, $pos);
		if ($status) {
			$this->Row = $pos;
		} else {
			$this->halt('seek('.$pos.') failed: result has '.$this->num_rows().' rows.');
			@mysql_data_seek($this->Query_ID, $this->num_rows());
			$this->Row = $this->num_rows();
			return FALSE;
		}
		return TRUE;
	}

/**
 * Установить блокировку
 * @version	3.0
 * @param	mixed	$table	название таблицы (таблиц), либо массив, либо строка содержащая перечисленные через запятую названия таблиц
 * @param	string	$mode	тип плокировки (write - по умолчанию, read, read local, low priority write)
 * @return	bool	TRUE в случае успеха, FALSE в случае неудачи
 */
	function lock($table, $mode = 'write') {
		$query = 'lock tables ';
		if(is_array($table)) {
			while(list($key,$value) = each($table)) {
				if(is_int($key)) $key = $mode;
				if(strpos($value, ",")) {
					$query .= str_replace(",", " $key, ", $value) . " $key, ";
				} else {
					$query .= "$value $key, ";
				}
			}
			$query = substr($query, 0, -2);
		} else if (strpos($table, ',')) {
			$query .= str_replace(',', ' '.$mode.', ', $table).' '.$mode;
		} else {
			$query .= $table.' '.$mode;
		}
		if(!$this->query($query)) {
			$this->halt('lock() failed.');
			return FALSE;
		}
		$this->locked = TRUE;
		return TRUE;
	}

/**
 * Снять блокировку
 * @version	3.0
 * @return	bool	FALSE в случае неудачи, TRUE  в случае успеха
 */
	function unlock() {
		$this->locked = FALSE;
		if(!$this->query('unlock tables')) {
			$this->halt('unlock() failed.');
			return FALSE;
		}
		return TRUE;
	}

/**
 * Возвращает количество затронутых строк последним SQL запросом
 */
	function affected_rows() {
		return ($this->Link_ID && is_resource($this->Link_ID)) ? @mysql_affected_rows($this->Link_ID) : false;
	}

/**
 * Возвращает количество строк в результирующем наборе данных
 * @version	3.0
 * @return	int		количество строк в результирующем наборе данных
 */
	function num_rows() {

		return $this->Query_ID && is_resource($this->Query_ID) ? @mysql_num_rows($this->Query_ID) : false;
	}

/**
 * Возвращает количество полей в результирующем наборе данных
 * @version	3.0
 * @return	int		количество полей в результирующем наборе данных
 */
	function num_fields() {
		return ($this->Query_ID && is_resource($this->Link_ID)) ? @mysql_num_fields($this->Query_ID): false;
	}

/**
 * Сокращённая запись метода $this->num_rows(). Возвращает количество строк в результирующем наборе данных
 * @version	3.0
 * @return	int		количество строк в результирующем наборе данных
 */
	function nf() {
		return $this->num_rows();
	}

/**
 * Вывод в стандартный выходной поток количества строк в результирующем наборе данных
 * @version	3.0
 * @return	void
 */
	function np() {
		print $this->num_rows();
	}

/**
 * Получение значения поля $name из результирующего набора данных, в текущей позиции
 * @version	3.0
 * @param	string	$name	название поля
 * @return	int		получение значения поля $name из результирующего набора данных, в текущей позиции.
 */
	function f($name) {
		if (isset($this->Record[$name])) {
			return $this->Record[$name];
		}
	}

/**
 * Вывод в стандартный выходной поток значения поля $name из результирующего набора данных, в текущей позиции
 * @version	3.0
 * @param	string	$name	название поля
 * @return	void
 */
	function p($Name) {
		if (isset($this->Record[$Name])) {
			print $this->Record[$Name];
		}
	}

	function nextid($seq_name) {
		/* if no current lock, lock sequence table */
		if(!$this->locked) {
			if($this->lock($this->Seq_Table)) {
				$locked = TRUE;
			} else {
				$this->halt("cannot lock ".$this->Seq_Table." - has it been created?");
    		    return FALSE;
			}
		}

		$q = sprintf("select nextid from %s where seq_name = '%s'",
        			 $this->Seq_Table,
					 $seq_name);
		if(!$this->query($q)) {
			$this->halt('query failed in nextid: '.$q);
			return FALSE;
		}

		if(!$this->next_record()) {
			$currentid = 0;
			$q = sprintf("insert into %s values('%s', %s)",
						 $this->Seq_Table,
						 $seq_name,
						 $currentid);
			if(!$this->query($q)) {
				$this->halt('query failed in nextid: '.$q);
				return FALSE;
			}
		} else {
			$currentid = $this->f("nextid");
		}
		$nextid = $currentid + 1;
		$q = sprintf("update %s set nextid = '%s' where seq_name = '%s'",
					 $this->Seq_Table,
					 $nextid,
					 $seq_name);
		if(!$this->query($q)) {
			$this->halt('query failed in nextid: '.$q);
			return FALSE;
		}

		if($locked) {
			$this->unlock();
		}
		return $nextid;
	}

/**
 * Возвращает метаданные таблицы
 * @version	3.0
 * @param	string	$table	имя таблицы
 * @param	bool	$full	при равенстве TRUE возвращаются полные метаданные, при равенстве FALSE (по умолчанию) возвращаются сокращённые метаданные
 * @return	mixed	FALSE в случае неудачи, массив в случае успеха
 */
	function metadata($table = '', $full = FALSE) {
		$count = 0;
		$id    = 0;
		$res   = array();

		if ($table) {
			$this->connect();
			$id = @mysql_list_fields($this->Database, $table);
			if (!$id) {
				$this->halt("Metadata query failed.");
				return FALSE;
			}
		} else {
			$id = $this->Query_ID;
			if (!$id) {
				$this->halt("No query specified.");
				return FALSE;
			}
		}
		$count = @mysql_num_fields($id);
		if (!$full) {
			for ($i=0; $i<$count; $i++) {
				$res[$i]['table'] = @mysql_field_table ($id, $i);
				$res[$i]['name']  = @mysql_field_name  ($id, $i);
				$res[$i]['type']  = @mysql_field_type  ($id, $i);
				$res[$i]['len']   = @mysql_field_len   ($id, $i);
				$res[$i]['flags'] = @mysql_field_flags ($id, $i);
			}
		} else {
			$res['num_fields']= $count;
			for ($i = 0; $i < $count; $i++) {
				$res[$i]['table'] = @mysql_field_table ($id, $i);
				$res[$i]['name']  = @mysql_field_name  ($id, $i);
				$res[$i]['type']  = @mysql_field_type  ($id, $i);
				$res[$i]['len']   = @mysql_field_len   ($id, $i);
				$res[$i]['flags'] = @mysql_field_flags ($id, $i);
				$res['meta'][$res[$i]['name']] = $i;
			}
		}
		if ($table) {
			@mysql_free_result($id);
		}
		return $res;
	}

/**
 * Возвращает доступные имена таблиц
 * @version	3.0
 * @return	array	массив имён таблиц
 */
	function table_names() {
		$this->connect();
		$h = @mysql_query('show tables', $this->Link_ID);
		$i = 0;
		while ($info = @mysql_fetch_row($h)) {
			$return[$i]['table_name']		= $info[0];
			$return[$i]['tablespace_name']	= $this->Database;
			$return[$i]['database']			= $this->Database;
			$i++;
		}
		@mysql_free_result($h);
		return $return;
	}

/**
 * Обработчик ошибок, если ошибка имеет место устанавливает значения свойств $this->Error, $this->Errno. Так же выводит сообщение $msg
 * @version	3.0
 * @param	string	$msg
 * @return	void
 */
	function halt($msg) {
		$this->Error = @mysql_error($this->Link_ID);
		$this->Errno = @mysql_errno($this->Link_ID);
		if ($this->locked) {
			$this->unlock();
		}
		if ($this->Halt_On_Error == "no")		return NULL;
		$this->haltmsg($msg);
		if ($this->Halt_On_Error != 'report'){
            main::no_such_page('500');exit;
        }
        //	die('Session halted.');
	}

	function haltmsg($msg) {
        $err = debug_backtrace();
        @array_shift($err);@array_shift($err);
        list(,$e) = each ($err);
        $msg = sprintf("<b>MySQL Error</b>: %s (%s)",$msg, $this->Error);
		#var_dump($msg,$e);exit;
        if(function_exists('ABO_ErrorHandler')) ABO_ErrorHandler($this->Errno, $msg, basename($e['file']), $e['line'], $err);
        return false;
	}

	// получить id последней записи, занесенной в базу
	function lid() {
		return @mysql_insert_id($this->Link_ID);
	}

	function get_arr($tabname) {
		$head = $this->metadata($tabname);
		for ($j = 0; $j < $this->num_fields(); $j++) {
			$table[0][$j] = $head[$j]["name"] ;
		}
		for ($i = 1; $i <= $this->num_rows(); $i++ ) {
			$this->next_record();
			for ( $j=0; $j<$this->num_fields(); $j++) {
				$h = $head[$j]["name"];
				$table[$i][$j] = $this->f($h);
				$table[$i][$j] = trim( $table[$i][$j] );
			}
		}
		return $table;
	}
	
	function fetchAll($table, $filter=array(), $key_field = false)
	{
	    $order = '';
	    if ($filter['order']) {
	        $order = ' ORDER BY '.My_Sql::escape($filter['order']);
	    }
	    $limit = '';
	    if ($filter['limit'] >= 1) {
	        $l = (int) $filter['limit'];
	        $s = $filter['start'] > 1 ? (int)$filter['start'] : 1;
	        $limit = ' LIMIT '.($s-1)*$l.','.$l;
	    }
	    $clause = '';
	    if ($filter['clause'] && is_array($filter['clause'])) {
	        $clause = $this->getClause($filter['clause']);
	        $clause = $clause ? " WHERE ".$clause : '';
	    }
	    $ret = false;
	    if ($this->query("SELECT * FROM ".My_Sql::escape($table).$clause.$order.$limit)) {
			while ($data = mysql_fetch_assoc($this->Query_ID)) {
				if (false !== $key_field && isset($data[$key_field])) {
				    $ret[$data[$key_field]] = $data;
				} else {
				    $ret[] = $data;
				}
			}
		}
		return $ret;
	}
	
	function fetchColumn($table, $col_name, $filter = false)
	{
	    $order = '';
	    if ($filter['order']) {
	        $order = ' ORDER BY '.My_Sql::escape($filter['order']);
	    }
	    $limit = '';
	    if ($filter['limit'] >= 1) {
	        $l = (int) $filter['limit'];
	        $s = $filter['start'] > 1 ? (int)$filter['start'] : 1;
	        $limit = ' LIMIT '.($s*$l).','.$l;
	    }
	    $clause = '';
	    if ($filter['clause'] && is_array($filter['clause'])) {
	        $clause = $this->getClause($filter['clause']);
	        $clause = $clause ? " WHERE ".$clause : '';
	    }
	    $ret = false;
	    if ($this->query("SELECT `".$col_name."` FROM ".My_Sql::escape($table).$clause.$order.$limit)) {
			while ($data = mysql_fetch_row($this->Query_ID)) {
				$ret[] = $data[0];
			}
		}
		
		return $ret;
	}
	
	function fetchOne($table, $filter = array())
	{
	    $order = '';
	    if ($filter['order']) {
	        $order = ' ORDER BY '.My_Sql::escape($filter['order']);
	    }
	    $clause = '';
	    if ($filter['clause'] && is_array($filter['clause'])) {
	        $clause = $this->getClause($filter['clause']);
	        $clause = $clause ? " WHERE ".$clause : '';
	    }
	    $ret = $this->getArrayOfResult("SELECT * FROM ".My_Sql::escape($table).$clause.$order.' LIMIT 1');
	    
		return $ret[0];
	}
	
	function fetchCnt($table, $filter)
	{
	    $clause = '';
	    if ($filter['clause'] && is_array($filter['clause'])) {
	        $clause = $this->getClause($filter['clause']);
	        $clause = $clause ? " WHERE ".$clause : '';
	    }
	    $this->query("SELECT COUNT(*) FROM ".My_Sql::escape($table).$clause);
	    
		return $this->next_record() ? $this->Record[0] : 0;
	}
	
	function getClause($array)
	{
		$ret = false;
		foreach ($array as $key => $val) {
		    if (is_object($val)) {
	            $ret[] = $val->get();
		    } else {
		        $ret[] = "`".My_Sql::escape($key)."`='".My_Sql::escape($val)."'";
		    }
	    }
	    
	    return ! empty($ret) ? implode(' AND ', $ret) : false;
	}
	
	function set($table, $data, $id = false)
	{
	    $ret = false;
	    $d = array();
	    foreach ($data as $key => $val) {
	        if (is_object($val)) {
	            $d[] = "`".My_Sql::escape($key)."`=".$val->get();
	        } else {
	            $d[] = "`".My_Sql::escape($key)."`='".My_Sql::escape($val)."'";
	        }
	    }
	    if (! empty($d)) {
	        $d = implode(', ', $d);
	        $clause = '';
    	    if ($id && is_array($id)) {
    	        $clause = $this->getClause($id);
    	        if ($clause) {
    	            $this->query("UPDATE ".My_Sql::escape($table)." SET ".$d
    	                ." WHERE ".$clause);
    	            $ret = true;
    	        }
    	    } else {
    	        $this->query("INSERT INTO ".My_Sql::escape($table)." SET ".$d);
    	        $ret = true;
    	    }
	    }
	    
		return $ret;
	}
	
	function del($table, $id = false)
	{
	    $ret = false;
	    $clause = '';
    	if ($id && is_array($id)) {
    	    $clause = $this->getClause($id);
    	    if ($clause) {
    	        $this->query("DELETE FROM ".My_Sql::escape($table)." WHERE ".$clause);
    	        $ret = $this->affected_rows();
    	    }
    	} else {
    	    $this->query("DELETE FROM ".My_Sql::escape($table));
    	    $ret = $this->affected_rows();
    	}
	    
		return $ret;
	}
}

class DbExp
{
    /**
     * Expression
     * @var string
     * @access protected
     */
    var $exp;

    /**
     * Constructor
     */
    function DbExp($exp)
    {
        $this->exp = $exp;
        return;
    }

    /**
     * Constructor
     */
    function __construct($exp)
    {
        $this->exp = $exp;
        return;
    }

    /**
     * Destructor
     */
    function __destruct() {
        return;
    }
    /**
     * Get expression
     *
     * @return string
     */
    function get()
    {
    	return $this->exp;
    }
}

class Core {

	var $aSitesLangs = NULL;										// Массив сайтов/id сайтов, языков сайтов/id языков сайтов

	function Core() {
		GLOBAL $db;
		$db->query('SELECT a.id AS site_id, a.title AS site_title, a.alias AS site_alias, b.id AS lang_id, b.language AS site_lang
						FROM sites AS a,
							 sites_languages AS b
						WHERE a.id = b.category_id');
		if ($db->nf() > 0) {
			$site = '';
			$lang = '';
			for ($i = 0, $j = $db->nf(); $i < $j; $i++) {
				$db->next_record();
				if ($site != $db->f('site_title')) {
					$arr[$db->f('site_title')]['site_id'] = $db->f('site_id');
					$arr[$db->f('site_title')]['site_alias'] = $db->f('site_alias');
					if ('' != $db->f('site_lang')) {
						$arr[$db->f('site_title')]['site_lang'][$db->f('site_lang')] = $db->f('lang_id');
					}
					$site = $db->f('site_title');
					$lang = $db->f('site_lang');
				} else if ($lang != $db->f('site_lang')) {
					$arr[$db->f('site_title')]['site_lang'][$db->f('site_lang')] = $db->f('lang_id');
					$lang = $db->f('site_lang');
				}
			}
			$this->aSitesLangs = $arr;
		}
	}

/**
 * Получения страницы по link (линк) или по address (адрес), в таблице $lang_pages имеются одноимённые столбцы.
 * Адрес - строка определяющая положение страницы в дереве сайта, пример: "what-is-leasing/types/euro-leasing/".
 * Страница с линком euro-leasing является дочерней по отношению к странице с линком types,
 * а та в свою очередь вложена в what-is-leasing. Если страница не привязана к дереву сайта,
 * то адрес будет просто "euro-leasing/".
 * Если включен rewrite_mod, страница извлекается по address, иначе по link.
 * При отсутствии в БД запрашиваемой страницы, выбирается default (стартовая) страница.
 * При отсутствии default страницы выбирается первая по rank из списка.
 * @version	3.0
 * @param		string $link
 * @return		array
 */
	function getPage($link) {
		global $CONFIG, $lang, $server, $db;

		if ('abocms_version' == $link) {
		    die('ABOCMS '.ABOCMS_VERSION.'<br>Время последнего изменения файла: '.date ("d F Y H:i:s.", filemtime(__FILE__)));
		}

		$by = ($CONFIG['rewrite_mod']) ? 'address' : 'link';
		if ($link) {
			$link = $db->escape(strtolower(substr($link, 0, 255)));
			if ($CONFIG['rewrite_mod']) {
                $p = (false!=($r = strrpos($link,'/'))) ? substr($link, $r) : $link;
                if(!strpos($p,'.')){$link .= '/';}

				$link = str_replace('//', '/', $link);
				$link_parts = explode('/', $link);
				if (empty($link_parts[sizeof($link_parts)-1])) unset($link_parts[sizeof($link_parts)-1]);
				$cnt = sizeof($link_parts);
				for ($i=0; $i < $cnt; $i++) {
					$link_arr[] = implode('/', $link_parts).'/';
					unset($link_parts[sizeof($link_parts)-1]);
				}
				$link = implode('", "', $link_arr);
			}
			$ret = db_loadResult2('SELECT p.*, pt.*, p.id as id, pt.id as tpl_id, IF(ISNULL(p.parent_id),0,1) as pid
										FROM '.$server.$lang.'_pages as p,
							 				 '.$server.$lang.'_pages_templates as pt
										WHERE '.$by.' IN ("'.$link.'") AND
							  				 pt.id = p.tpl_id
										ORDER BY LENGTH('.$by.') DESC
										LIMIT 0,1');
			$ret["params"] = $CONFIG['rewrite_mod'] ? substr($link_arr[0], strlen($ret[$by])) : "";
		} else {
			$ret = db_loadResult2('SELECT p.*, pt.*, p.id as id, pt.id as tpl_id, IF(ISNULL(p.parent_id),0,1) as pid
										FROM '.$server.$lang.'_pages as p,
							 				 '.$server.$lang.'_pages_templates as pt
										WHERE is_default = 1 AND
							  				  pt.id = p.tpl_id
										ORDER BY page_rank DESC');
			if (!count($ret) > 0) {
				$query	= "SELECT p.*, pt.*, p.id as id, pt.id as tpl_id, IF(ISNULL(p.parent_id),0,1) as pid
							FROM ".$server.$lang."_pages as p,
								 ".$server.$lang."_pages_templates as pt
							WHERE parent_id = 0 AND
								  pt.id = p.tpl_id
							ORDER BY page_rank DESC";
				$ret	= db_loadResult2($query);
				$ret["params"] = "";
			}
		}
        $ret["params"] = substr($ret["params"], 0, -1);
        if($ret["params"]){
            $p = (false!=($r = strrpos($ret["params"],'/'))) ? substr($ret["params"], $r) : $ret["params"];
            if(!strpos($p,'.')){$ret["params"] .= '/';}
            @preg_match('~(?:([a-z0-9_/-]+)(?:/))?(?:([a-z0-9_-]+)(?:\.html))?~i',$ret["params"], $test);
            if(sizeof($test)>1){$ret["dop_params"] = $test;}
        }
		return $ret;
	}

/**
 *	Получение HTML-маски для поля с меню для добавления новых блоков
 *	@version	5.0
 *	@param		integer	$group_id		id группы
 *	@return 	array(string, string)	HTML-маски поля: первая для главного поля, вторая для всех остальных
 */
	function get_block_menu_mask($page_id, $group_id) {
		GLOBAL $baseurl, $server, $lang, $db;
		if ($group_id == 1) {
			$db->query("SELECT id, name, only_as_main, title FROM modules");
		} else {
			$db->query("SELECT DISTINCT m.id, m.name, m.only_as_main, m.title
						FROM modules m, core_roles_groups rg, core_roles_permissions rp
						WHERE rg.group_id = ".$group_id." AND rg.role_id = rp.role_id AND m.id = rp.module_id AND rp.r = 1");
		}
   		$tpl_1 = new AboTemplate(RP.'tpl/admin/_menu_field.html', true);
        $tpl_1->prepare();
   		$tpl_2 = new AboTemplate(RP.'tpl/admin/_menu_field.html', true);
        $tpl_2->prepare();
		while ($db->next_record()) {
			$tpl_1->newBlock("block_".$db->f("name")."_module");
			$tpl_1->assign(array(
				"page_id"	=> $page_id,
				"module_id"	=> $db->f("id"),
				"baseurl"	=> $baseurl,
			));
			if ($db->f("only_as_main") == 0) {
				$tpl_2->newBlock("block_".$db->f("name")."_module");
				$tpl_2->assign(array(
					"page_id"	=> $page_id,
					"module_id"	=> $db->f("id"),
					"baseurl"	=> $baseurl,
				));
			}
			$this->blocks[($db->f("name"))] = $db->f("title");
		}
		return array($tpl_1->getOutputContent(), $tpl_2->getOutputContent());
	}

/**
 *	Получение HTML для блока с меню для редактирования параметров блока
 *	@version	5.0
 *	@param		integer	$field_num		номер поля
 *	@param		integer	$block_id		id блока
 *	@param		string	$block_name		название блока
 *	@param		array	$temp			массив с блоками на странице
 *	@param		array	$rights			массив прав
 *	@param	 	string	block_content	HTML блока
 *	@param	 	string	baseurl			базовый URL
 *	@return 	string					HTML блока с меню
 */
	function get_block_menu($field_num, $block_id, $block_name, &$temp, &$rights, &$block_content, $baseurl) {
		global $lang, $request_type;
		$up_comand = $down_comand = $first_is_found = $current_is_found = false;
		$cnt = sizeof($temp);
		$prev_block = $next_block = 0;
		$cur_idx = -1;
		for ($i=0; $i < $cnt; $i++) {
			if ($temp[$i]["field_number"] != $field_num) continue;
			if (!$first_is_found) {
				$first_is_found = true;
				if ($temp[$i]["block_id"] == $block_id) {
					$current_is_found = true;
					$cur_idx = $i;
				} else {
					$up_comand = true;
				}
				continue;
			}
			if (!$current_is_found) {
				if ($temp[$i]["block_id"] == $block_id) {
					$current_is_found = true;
					$cur_idx = $i;
					continue;
				}
			}
			if ($current_is_found) {
				$down_comand = true;
				break;
			}
		}
		#error_log(var_export($this,true),3,'test.txt');
		if ($request_type == "JsHttpRequest") {
			list($prefix, $adminurl) = $this->getPrefixByID($_SESSION['session_cur_site_id'], $_SESSION['session_cur_lang_id']);
		} else {
			$prefix = SITE_PREFIX;
		}
   		$tpl_2 = new AboTemplate(RP.'tpl/admin/_menu_block.html', true);
        $tpl_2->prepare();
        $block_title = $this->blocks[$block_name];
        list($block_abbr) = explode(' ',trim($block_title),2);
		#$e = debug_backtrace();
		#var_export($e);
		$tpl_2->assign(array(
			'baseurl'		=> $baseurl,
			'block_name'	=> $block_name,
			'block_abbr'	=> $block_name, #iconv("WINDOWS-1251", "UTF-8",$block_abbr),
			'block_content'	=> $block_content,
			'this_block'	=> $block_id,
			'display_up'	=> ($rights["e"] && $up_comand) ? "block" : "none",
			'display_down'	=> ($rights["e"] && $down_comand) ? "block" : "none",
		));
		if (empty($block_content) || $block_content == ' ') {
			$tpl_2->newBlock("block_empty_block");
		}
		if ($rights["e"]) {
			$tpl_2->newBlock("block_prop_command");
			$tpl_2->assign(array(
				"baseurl" => $baseurl,
				"module" => $block_name,
				"field" => $field_num,
				"lang" => $lang,
				"this_block" => $block_id,
				"tpl" => str_replace("'", "\'", $temp[$cur_idx]['tpl']),
				"prop"		=> str_replace('"', '\'', "new Array('".$temp[$cur_idx]["action"]."',"
										.(preg_match("/^[{\[].*[}\]]$/", $temp[$cur_idx]["property1"]) ? $temp[$cur_idx]["property1"] : "'{$temp[$cur_idx]["property1"]}'").", "
										.(preg_match("/^[{\[].*[}\]]$/", $temp[$cur_idx]["property2"]) ? $temp[$cur_idx]["property2"] : "'{$temp[$cur_idx]["property2"]}'").","
										.(preg_match("/^[{\[].*[}\]]$/", $temp[$cur_idx]["property3"]) ? $temp[$cur_idx]["property3"] : "'{$temp[$cur_idx]["property3"]}'").", "
										.(preg_match("/^[{\[].*[}\]]$/", $temp[$cur_idx]["property4"]) ? $temp[$cur_idx]["property4"] : "'{$temp[$cur_idx]["property4"]}'").","
										.(preg_match("/^[{\[].*[}\]]$/", $temp[$cur_idx]["property5"]) ? $temp[$cur_idx]["property5"] : "'{$temp[$cur_idx]["property5"]}'").", '"
										.$temp[$cur_idx]["linked_page"]."','"
										.$temp[$cur_idx]["site_id"]."', '".$temp[$cur_idx]["lang_id"]."')"),
			));
		}
		if ($rights["d"]) {
			$tpl_2->newBlock("block_del_command");
			$tpl_2->assign(array(
				'baseurl'		=> $baseurl,
				'this_block'	=> $block_id,
			));
		}

		return $tpl_2->getOutputContent();
	}

/**
 *	Получение контента поля путем перебора всех блоков поля
 *	@version	3.0
 *	@param		integer	$page_id	id страницы
 *	@param		string	$link		адрес/линк страницы, в зависимости от значения $CONFIG['rewrite_mod']
 *	@param		bool	$onlymain	значение по умолчанию FALSE, если onlymain установлен - это означает,
 * 									что надо открыть главный модуль с установленным для него действием,
 *									а остальные блоки не нужны
 *	@return 	array	массив содержащий контент для каждого поля страницы
 */
	function get_field_content($page_id, $link, $onlymain = FALSE, $printable = FALSE) {
		GLOBAL $CONFIG, $tpl, $baseurl, $request_action, $lang, $server, $db;
		$field_output = array();			// массив, в котором будет содержаться контент для каждого поля страницы,
											// сформированного посредством заполнения более мелких шаблонов для блоков
											// если установлен флажок onlymain - это означает, что надо открыть главный модуль
											// с установленным для него действием, а остальные блоки не нужны

		if ($onlymain || $printable) {
		    $onlymain_str = ' AND field_number = 0';
		}
		// сделать выборку блоков для данной страницы по $page_id
		Module::get_list_with_rights("pb.*, pb.id as block_id, m.name, m.title, m.id as mod_id, p.usr_group_id, number_of_fields, pt.main_module ",
                                     array( 'p' => $server.$lang.'_pages',
                                            'pb' => $server.$lang.'_pages_blocks',
                                            'pt' => $server.$lang.'_pages_templates',
						 			 		'm' => 'modules',

                                     ),
									 "p",
									 "p.id = ".$page_id." ".$onlymain_str."
						  			 AND p.tpl_id = pt.id AND p.id = pb.page_id
									 AND m.id = pb.module_id",
									 "",
									 "field_number, block_rank");
		$temp = array();
		while ($db->next_record()) {
		    $temp[] = $db->Record;
		}

		if (count($temp) > 0) {
			$cnt_fields = $temp[0]["number_of_fields"];
			//$field_output = array_pad(array(), $temp[0]["number_of_fields"] ? $cnt_fields+1 :$cnt_fields, '');
			$is_owner	= $temp[0]["is_owner"];
			$rights		= $temp[0]["user_rights"];
			$rights		= array("d" => ($rights & 8) >> 3,
								"p" => ($rights & 4) >> 2,
								"e" => ($rights & 2) >> 1,
								"r" => $rights & 1,
							);
			list($main_field_mask, $field_mask) = ($CONFIG['edit_on_site'] && $_SESSION["session_is_admin"] && $_COOKIE['edit_content'])
						? $rights["e"]  ? $this->get_block_menu_mask($page_id, $temp[0]["usr_group_id"])
										: array("<div id=\"page_field___FIELD_ID__\" class=\"field-tools\">%s</div>", "<div id=\"page_field___FIELD_ID__\" class=\"field-tools\">%s</div>")
						: array("%s", "%s");

			foreach($temp as $idx => $v) {
				unset($tpl);
				$block_id			= $v['block_id'];			// Id блока
				$field				= $v['field_number'];		// Номер поля
				$module_id			= $v['mod_id'];				// Id модуля
				$name				= $v['name'];				// Имя модуля для текущего блока
				$GLOBALS['name']	= $v['name'];

				$site_id			= $v['site_id'];
				$lang_id			= $v['lang_id'];

				$this->blocks[($v['name'])] = $v['title'];
				
				$properties = array(
        			1 => preg_match("/^[{\[].*[}\]]$/", $v['property1']) ? json_decode($v['property1']) : $v['property1'],
        			2 => preg_match("/^[{\[].*[}\]]$/", $v['property2']) ? json_decode($v['property2']) : $v['property2'],
        			3 => preg_match("/^[{\[].*[}\]]$/", $v['property3']) ? json_decode($v['property3']) : $v['property3'],
        			4 => preg_match("/^[{\[].*[}\]]$/", $v['property4']) ? json_decode($v['property4']) : $v['property4'],
        			5 => preg_match("/^[{\[].*[}\]]$/", $v['property5']) ? json_decode($v['property5']) : $v['property5'],
        			'tpl' => $v['tpl']
        		);
        		
				// $action - действие, которое надо выполнить с данным модулем для заполнения блока контентом
				// если это поле главного модуля и передан $request_action, то значит пользователь пришел
				// по ссылке с другой страницы, и мы должны выполнить $request_action, а не action из БД
				if ($field == 0 && $request_action) {
				  $action = strip_tags(trim($request_action));
              	  $action = str_replace(array("<", ">", "\"", "'", ".", '\\', "@"), "", $action);
              	  if ($action != $v['action']) {
              	      $properties['tpl'] = '';
              	  }
				} else {
					$action = $v['action'];
				}
				if ($v['linked_page']) {
					$transurl = $this->formPageLink($v['linked_page'], $v['linked_page_address'], $lang, 1); // страница перехода
				} else {
					$transurl = $baseurl;
				}
				$block_content = $this->get_block_content($page_id, $field, $block_id, $module_id, $name, $action, $link, $transurl, $properties, $site_id, $lang_id);
				if ($CONFIG['edit_on_site'] && $_SESSION["session_is_admin"] && $_COOKIE['edit_content']) {
					$block_content = $this->get_block_menu($field, $block_id, $name, $temp, $rights, $block_content, $baseurl);
				}
				$field_output[$field] .= $block_content;
			}
			if ($CONFIG['edit_on_site'] && $_SESSION["session_is_admin"] && $_COOKIE['edit_content']) {
				for ($i=0; $i <= $cnt_fields; $i++) {
					$template = $i ? $field_mask : $main_field_mask;
					$template = str_replace("__FIELD_ID__", $i, $template);
					$field_output[$i] = sprintf($template,
												empty($field_output[$i])
													? "<div class=\"admin-empty-field\">Пустое поле</div>"
													: $field_output[$i]);
				}
			}
		}
		return $field_output;
	}

	function is_page_availaible($page_id)
	{   global $CONFIG, $PAGE, $db, $db1, $tpl, $tpl_path, $main, $baseurl, $lang, $server;
		static $ret = array();
		
		$page_id = (int) $page_id;
		if (!isset($ret[$page_id])) {
		    $db->query('SELECT permission_type FROM '.$server.$lang.'_pages USE INDEX (PRIMARY) WHERE id = '.$page_id);
    		$db->next_record();
    		$p_type = $db->f("permission_type");
    		// Установка значений флагов по умолчанию
    		$flag_register		= 0;
    		$flag_group_denied	= 0;
    		if (($p_type == 1) || ($p_type == 3)) {
    			$flag_register	= 1;
    			if ($_SESSION['siteuser']['is_authorized']) {
    				$db->query('SELECT A.id, B.page_id, C.group_id, B.show
    								FROM core_users A
                                    LEFT JOIN core_users_groups C
                                        ON  (A.id = C.user_id)
    								LEFT JOIN core_group_permissions B
    								    ON ( (C.group_id = B.group_id) AND (B.page_id = '.$page_id.') )
    								WHERE A.id = '.$_SESSION['siteuser']['id'].' AND A.id=C.user_id');
    				$flag_group_denied	= 1;
    				if ($db->nf()) {
    					while ($db->next_record()) {
    						if ($db->f('show') == 1) {
    							$flag_register		= 0;
    							$flag_group_denied	= 0;
    							break;
    						}
    					}
    				}
    			}
    		}
    		if ($flag_group_denied) {
    			$ret[$page_id] = 2;
    		} else if ($flag_register) {
    			$ret[$page_id] = 1;
    		} else {
    			$ret[$page_id] = 0;
    		}
		}
		
		return $ret[$page_id];
	}

/**
 *	Получение контента определенного блока
 *	@version	3.0
 *	@param		integer	$page_id	id страницы
 *	@param		integer	$field		номер поля
 *	@param		integer	$block_id	id блока
 *	@param		integer	$module_id	id модуля
 *	@param		string	$name		название модуля
 *	@param		string	$action		действие (action) модуля
 *	@param		string	$link		адрес/линк страницы
 *	@param		string	$transurl	url для формирования ссылок
 *	@param		array	$properties	свойства блока
 *	@return 	string
 */
	function get_block_content($page_id, $field, $block_id, $module_id, $name, $action = '', $link = '', $transurl = '',
							   $properties= array(), $site_id = NULL, $lang_id = NULL, $noadmin = false) {
		GLOBAL $CONFIG, $PAGE, $cache, $tpl, $debug_arr, $cache_arr, $class_path, $lang, $server;
		$block_content = '';												// html код блока, получим через $tpl
		$Name = ucwords($name);//.'Prototype';												// Название модуля с заглавной буквы
		if($this) $this->log_debug();
		$site_mods = Main::getSiteModulesNames(substr($server, 0, -1));
		if (!in_array($name, $site_mods)) {
			return FALSE;
		}
		$PAGE['field'] = $field;
		$PAGE['block_id'] = $block_id;
		if (!$CONFIG['use_cache'] || in_array($name, $CONFIG['no_cache_modules']) || in_array($action, $CONFIG['no_cache_actions']) || isset($_REQUEST['nocache']) || $action == '') {
			$no_cachee		= 1;
		} else {
			$no_cachee		= 0;
			$cache_filename	= $cache->get_cache_filename($page_id, $field, $block_id, $module_id, $link.(isset($PAGE['dop_params']) ? "/".$PAGE['dop_params'][0] : ''));
		}
		$PAGE['curr_tpl'] = false;
		if (!$no_cachee && is_object($cache) && file_exists($cache_filename) && (!$CONFIG['cache_expiration_time'] || ($CONFIG['cache_expiration_time'] && ((time()-@filemtime($cache_filename))/3600) < $CONFIG['cache_expiration_time']))) {
			$block_content = $cache->get_cache_content($cache_filename);
			if ($CONFIG['debug'] || isset($_REQUEST['debug'])) {
			    $cache_arr[$block_id] = 'Блок '.$block_id.' модуля '.$name.' с действием '.$action.' <- '.$cache_filename;
			}
			if (0 == $field) {
			    $PAGE['process_params'] = true;
			}
		} else {
			// подключить класс для данного модуля
			if (is_file($class_path.$name.'/lib/class.'.$Name.'.php')) {
				include_once($class_path.$name.'/lib/class.'.$Name.'.php');
				if (class_exists($Name)) {
					// Создадим объект - экземпляр класса-модуля, определенного для текущего блока
					// Передадим ему действие $action, которое необходимо выполнить модулю
					list($prefix, $adminurl) = $this->getPrefixByID($site_id, $lang_id);
					if (is_object($PAGE['modules'][$name])) {
					    $m = method_exists($PAGE['modules'][$name], $Name) ? $Name : $Name.'Prototype';
						$PAGE['modules'][$name]->$m($action, $transurl, $properties, $prefix, $field == 0 ? $PAGE["params"] : "", $adminurl, $noadmin);
						if ($CONFIG['debug'] || isset($_REQUEST['debug'])) {
							$debug_arr[$block_id] = "\$PAGE[\"modules\"][\"$name\"]->$m('$action', '$transurl', ".print_r($properties, true).")".($PAGE['curr_tpl'] ? ", tpl - {$PAGE['curr_tpl']}" : '');
						}
					} else {
						$PAGE['modules'][$name] = new $Name($action, $transurl, $properties, $prefix, $field == 0 ? $PAGE["params"] : "", $adminurl, $noadmin);
						if ($CONFIG["debug"] || isset($_REQUEST["debug"])) {
						    $debug_arr[$block_id] = "\$PAGE[\"modules\"][\"$name\"] = new $Name('$action', '$transurl', " . print_r($properties, true) . ")".($PAGE['curr_tpl'] ? ", tpl - {$PAGE['curr_tpl']}" : '');
						}
					}
					// Контент для блока
					if (is_object($tpl)) {
					    $tpl->assignGlobal('BLOCK_ID', $block_id);
					    $block_content = $tpl->getOutputContent();
					}
					if ($CONFIG['rewrite_mod']) {
					    $block_content = str_replace('/?&','/?',$block_content);
					}
					// выкинуть все переносы строк из блока
//					$block_content = preg_replace('/[\n\t\r]/i','',$block_content);
					// поле 0 - поле главного модуля
					if ($field == 0) {
						$PAGE['main_module_id']		= $module_id;
						$PAGE['main_module_name']	= $name;
						$PAGE['action']				= $action;
						// главный модуль переопределяет title страницы
						if ($PAGE['modules'][$name]->main_title) {
							$PAGE['title_from_main_module'] = $PAGE['modules'][$name]->main_title;
						}
						// главный модуль добавляет к пути страницы (типа: > главная > пресс-центр > новости ... ) строчку addition_to_path
						
						if ($PAGE['modules'][$name]->addition_to_path) { 
							$PAGE['addition_to_path'] = $PAGE['modules'][$name]->addition_to_path;
						}
						// главный модуль переопределяет description страницы
						if ($PAGE['modules'][$name]->main_description) {
							$PAGE['description']	= $PAGE['modules'][$name]->main_description;
						}
						// главный модуль переопределяет keywords страницы
						if ($PAGE['modules'][$name]->main_keywords) {
							$PAGE['keywords']	= $PAGE['modules'][$name]->main_keywords;
						}
					}
					// создать cache-файл с html-кодом для данного поля
					if (!$no_cachee && is_object($cache) && $cache_filename) {
						$ch = $cache->create_cache_file($cache_filename, $block_content);
						if (($CONFIG['debug'] || isset($_REQUEST['debug'])) && $ch) {
							$cache_arr[$block_id] = 'Блок '.$block_id.' модуля '.$name.' с действием '.$action.' -> '.$cache_filename;
						}
					}
				}
			}
		}
		// выделить границы блоков красным цветом
		// флажок blockborder применяется при отладке
		if (isset($_REQUEST['blockborder'])) {
			$block_content	= '<table border="3" bordercolor="#ff0000" width="100%" title="'.$PAGE['curr_tpl'].'"><tr><td>'.$block_content.'</td></tr></table>';
		}
		// флажок mark означает подсветить определенное слово на странице
		// используется для подсветки результатов поиска
		// если mark определено, то из mark формируется массив $PAGE["markwords_array"]
		if (count($PAGE['markwords_array']) > 0) {
			require_once(RP."inc/rumor/class.RuMor.php");
			$rumor = new RuMor(RP."inc/rumor");
			$rumor->prep_dict();
			$pattern = array();
			foreach($PAGE['markwords_array'] as $markword) {
			    $pattern[] = "/(?<!span class=markword>)(".preg_quote($markword).")/iu";
				$markword = iconv("UTF-8", "WINDOWS-1251", strtolower($markword));
				if (strlen($markword) > 2) {
					$res = $rumor->get_base_form($markword);
					if ($res) {
						$res = $rumor->get_paradigm($res[0]);
						foreach ($res as $key=>$word) {
							$words[$key] = $word;
							$words_length[$key] = strlen($word);
						}
						array_multisort($words_length, SORT_DESC, $words);
						unset($words_length);
					} else {
						$words[] = $markword;
					}
					unset($res);
					foreach ($words as $key=>$word) {					    
						$pattern[] = iconv("WINDOWS-1251", "UTF-8", "/(?<!span class=markword>)(".preg_quote($word).")/iu");
					}
					unset($words);
				}
			}
			$block_content = preg_replace($pattern, "<span class=markword>\\1</span>", $block_content);
		}
		return $block_content;
	}
	
	function get_block_content_by_id($block_id)
	{
	    list($content,) = $this->get_block_content_with_menu($block_id, false);
	    return $content;
	}

/**
 *	Получение контента определенного блока с меню
 *	@version	5.0
 *	@param		integer	$block_id	id блока
 *	@return 	string
 */
	function get_block_content_with_menu($block_id, $withMenu = true) {
		GLOBAL $CONFIG, $tpl_path, $db, $lang, $server, $request_type, $request_cur_url;
		$block_content = '';

		if ($request_type == "JsHttpRequest") {
			list($prefix, $adminurl) = $this->getPrefixByID($_SESSION['session_cur_site_id'], $_SESSION['session_cur_lang_id']);
		} else {
			$prefix = $server.$lang;
		}
		$tpl_path	= $lang ? $CONFIG['tpl_path'].$prefix."/" : $CONFIG['tpl_path'];
		$db->query("SELECT page_id, field_number FROM ".$prefix."_pages_blocks WHERE id=".$block_id);
		if ($db->nf() == 0) return array('',0);
		$db->next_record();
		// сделать выборку блоков для данной страницы по $page_id
		Module::get_list_with_rights("pb.*, pb.id as block_id, p.link, p.address, m.name, m.title",
									array("pb" => $prefix."_pages_blocks", "m" => "modules", "p" => $prefix."_pages"),
									"p",
									"pb.page_id=".$db->f('page_id')." AND pb.field_number=".$db->f('field_number')
									." AND pb.page_id = p.id AND pb.module_id = m.id",
									"",
									"field_number, block_rank");
		if ($db->nf() == 0) return array('',0);
		$cur_idx = -1;
		$idx = 0;
		while ($db->next_record()) {
			$temp[$idx] = $db->Record;
			if ($temp[$idx]["id"] == $block_id) $cur_idx = $idx;
			$idx++;
		}
		$next_block_id = intval($temp[$cur_idx+1]["id"]);

		$is_owner	= $temp[$cur_idx]["is_owner"];
		$rights		= $temp[$cur_idx]["user_rights"];
		$rights		= array("d" => ($rights & 8) >> 3,
							"p" => ($rights & 4) >> 2,
							"e" => ($rights & 2) >> 1,
							"r" => $rights & 1,
						);
		$baseurl = ($temp[$cur_idx]['linked_page'])
					? $this->formPageLink($temp[$cur_idx]['linked_page'], $temp[$cur_idx]['linked_page_address'], $lang, 1) // страница перехода
					: $this->formPageLink($temp[$cur_idx]['link'], $temp[$cur_idx]['address'], $lang, 1);

		if ($temp[$cur_idx]['field_number'] == 0) {
			// восстановим параметры запроса
			$cur_url = parse_url($request_cur_url);
			if ($cur_url["query"]) {
				parse_str($cur_url["query"], $params);
				eval('global $request_'.implode(', $request_', array_keys($params)).';');
				foreach ($params as $name => $value) {
					if ($name == 'action') {
					    if ($value != $temp[$cur_idx]['action']) {
					        $temp[$cur_idx]['tpl'] = '';
					    }
						$temp[$cur_idx]['action'] = addslashes($value);
					} else {
						eval('$request_'.$name.' = "'.addslashes($value).'";');
					}
				}
			}
		}
		$prop = array(
			1 => preg_match("/^[{\[].*[}\]]$/", $temp[$cur_idx]['property1']) ? json_decode($temp[$cur_idx]['property1']) : $temp[$cur_idx]['property1'],
			2 => preg_match("/^[{\[].*[}\]]$/", $temp[$cur_idx]['property2']) ? json_decode($temp[$cur_idx]['property2']) : $temp[$cur_idx]['property2'],
			3 => preg_match("/^[{\[].*[}\]]$/", $temp[$cur_idx]['property3']) ? json_decode($temp[$cur_idx]['property3']) : $temp[$cur_idx]['property3'],
			4 => preg_match("/^[{\[].*[}\]]$/", $temp[$cur_idx]['property4']) ? json_decode($temp[$cur_idx]['property4']) : $temp[$cur_idx]['property4'],
			5 => preg_match("/^[{\[].*[}\]]$/", $temp[$cur_idx]['property5']) ? json_decode($temp[$cur_idx]['property5']) : $temp[$cur_idx]['property5'],
			'tpl' => $temp[$cur_idx]['tpl']
		);
		$block_content = $this->get_block_content(
		    $temp[$cur_idx]['page_id'],
			$temp[$cur_idx]['field_number'],
			$block_id,
			$temp[$cur_idx]['module_id'],
			$temp[$cur_idx]['name'],
			$temp[$cur_idx]['action'],
			$temp[$cur_idx]['link'],
			$baseurl,
			$prop,
			$temp[$cur_idx]['site_id'],
			$temp[$cur_idx]['lang_id'],
			true);
		if ($withMenu) {
		    $block_content = $this->get_block_menu($temp[$cur_idx]['field_number'], $block_id, $temp[$cur_idx]['name'], $temp, $rights, $block_content, $baseurl);
		}

		return array($block_content, $next_block_id);
	}
	
/**
 * Префикс для таблиц по ID сайта и ID языка сайта
 */
	function getPrefixByID($site_id, $lang_id) {
		global $server, $lang;
		$site_id	= (int)$site_id;
		$lang_id	= (int)$lang_id;
		if ($site_id && $lang_id) {
			foreach($this->aSitesLangs AS $k => $v) {
				if ($v['site_id'] == $site_id) {
					foreach($v['site_lang'] AS $l => $m) {
						if ($m == $lang_id) {
							$prefix = str_replace('.', '_', Main::parseSiteName($this->aSitesLangs[$k]['site_alias']).'_'.$l);
							$prefix = str_replace('-', '_', $prefix);
							return array($prefix, "http://".$k."/admin.php?lang=".$l);
						}
					}
				}
			}
		} else {
			$site_alias = substr($server, 0, -1);
			foreach($this->aSitesLangs AS $k => $v) {
				if ($v['site_alias'] == $site_alias) {
					return array($server.$lang, "http://".$k."/admin.php?lang=".$lang);
				}
			}
		}
		return FALSE;
	}

/**
 *	Подключение несущего шаблона соответствующего данной странице
 *	@version	3.0
 *	@param		integer $tpl_id		id шаблона
 *	@param		bool	$onlymain	признак шаблона главной страницы
 *	@return 	bool
 */
	function set_main_template($tpl_id, $onlymain = false, $printable = false) {
		global $CONFIG, $main, $tpl, $tpl_path, $lang;
		if ($onlymain) {
			if (!file_exists($tpl_path.'_index0.html')) {
				$main->message_die('Не существует запрашиваемый шаблон _index0.html');
			}
			$tpl = new AboTemplate($tpl_path.'_index0.html');
			$tpl->assignInclude('header', $tpl_path.'_header.html');
      if ($_SESSION['session_is_admin'] && $CONFIG['siteusers_show_panel'])
			  $tpl->assignInclude('front_edit', $tpl_path.'../admin/_upline.html');
      if ($CONFIG['edit_on_site'] && $_SESSION['session_is_admin'] && $_COOKIE['edit_content']){
          $this->addUpline();
      }

		if ($CONFIG['debug'] || isset($_REQUEST['debug']))
			$tpl->assignInclude('debug', $tpl_path.'_debug.html');
		}
          else if ($printable) {
            if (!file_exists($tpl_path.'_printable.html')) {
                $main->message_die('Не существует запрашиваемый шаблон _printable.html');
			}
            $tpl = new AboTemplate($tpl_path.'_printable.html');
            $tpl->assignInclude('header', $tpl_path.'_header.html');

            $tpl->assignInclude('contacts', $tpl_path.'_contacts.html');
            if ($CONFIG['debug'] || isset($_REQUEST['debug'])) {
            	$tpl->assignInclude('debug', $tpl_path.'_debug.html');
			}
        }
          else {
			if (!file_exists($tpl_path.'_index'.$tpl_id.'.html')) {
				$main->message_die('Не существует запрашиваемый шаблон _index'.$tpl_id.'.html');
			}
			$tpl = new AboTemplate($tpl_path.'_index'.$tpl_id.'.html');
			$tpl->assignInclude('header', $tpl_path.'_header.html');
			$tpl->assignInclude('top', $tpl_path.'_top.html');
			$tpl->assignInclude('bottom', $tpl_path.'_bottom.html');
      if ($_SESSION['session_is_admin'] && $CONFIG['siteusers_show_panel'])
			  $tpl->assignInclude('front_edit', $tpl_path.'../admin/_upline.html');
      if ($CONFIG['edit_on_site'] && $_SESSION['session_is_admin'] && $_COOKIE['edit_content']){
          $this->addUpline();
      }
		    if ($CONFIG['debug'] || isset($_REQUEST['debug']))
			    $tpl->assignInclude('debug', $tpl_path.'_debug.html');
		}
		$tpl->prepare();

		$tpl->assignGlobal('Structure',	$main->_msg['Structure']);
        $tpl->assignGlobal('Exit',		$main->_msg['Exit']);
        $tpl->assignGlobal('Add_page',	$main->_msg['Add_page']);
        $tpl->assignGlobal('Edit_on_site',$main->_msg['Edit_on_site']);
        $tpl->assignGlobal('Go_on_rzd',	$main->_msg['Go_on_rzd']);

		return TRUE;
	}
    function addUpline($fl = false){
        global $CONFIG, $tpl, $lang, $tpl_path;
        if(!$fl) {
           //$tpl->assignInclude('front_edit', $tpl_path.'../admin/_upline.html');
           return true;
        }
        $menu =  Main::show_admin_menu(false, true);

        if(is_array($menu)){
            $tpl->newBlock('front_edit');
            foreach($menu as $v){
                $tpl->newBlock('block_rzd');
                $tpl->assign($v);
            }
        }
    }
/**
 *	Сборка сайта
 *	@version 	3.0
 *	@param		array $field_output массив html кода
 *	@param		array $labels		массив значений меток
 *	@return		bool
 */
	function page_assembling($field_output = array(), $labels = array()) {
		global $CONFIG, $tpl, $lang;
		// Произведем окончательную сборку страницы путем заполнения полей шаблона
		// сначала хтмл-кодом (контентом) из массива $field_output
		if (is_array($field_output)) {
			foreach ($field_output as $field => $content) {
				$tpl->assign(array(
					'_ROOT.field_'.$field => $content,
				));
			}
		}
		// затем заполним метки страницы из массива $labels
		foreach ($labels as $label => $content) {
			$tpl->assign('_ROOT.'.$label, $content);
		}
		return TRUE;
	}


// сформируем постоянный адрес страницы для распечатки
    function get_original_link($original_link) {
        global $CONFIG;
        $original_link = str_replace("http://", "http:::", $original_link);
        $original_link = str_replace("//","/",$original_link);
        $original_link = str_replace("?&","?",$original_link);
        $original_link = str_replace("http:::", "http://", $original_link);
        $original_link = preg_replace("/[&?]printable/", "", $original_link);
        $sess = strstr($original_link, 'PHPSESSID');
        if ($sess) {
            $original_link = str_replace('?'.$sess, '', $original_link);
            $original_link = str_replace('&'.$sess, '', $original_link);
        }
        return $original_link;
    }

/**
 *	Формирование внутренней ссылки в зависимости от значения $CONFIG['rewrite_mod'],
 *	@version	3.0
 *	@param		string		$link		линк страницы
 *	@param		string		$address	адрес страницы
 *	@param		string		$lang		языковой идентификатор
 *	@param		integer		$qs			признак необходимости добавления в ссылку знака вопроса
 *	@return		string
 */
	function formPageLink($link = '', $address = '', $lang = '', $qs = 0) {
		GLOBAL $CONFIG, $lang;
		// добавить знак вопроса
		$q = ($qs) ? '?' : '';
		if ($CONFIG['rewrite_mod']) {
			$lang_query	= (@($CONFIG['multilanguage']) && $lang) ? $lang.'/' : '';
			$lang_query = ($lang_query == $CONFIG['basic_lang'].'/') ? '' : $lang_query;
			if (!$address && !$link) {
				$page_link = '/'.$lang_query.$q;
			} else if (!$address && $link) {
				$page_link = '/'.$lang_query.$link.'/'.$q;
			} else {
				$page_link = '/'.$lang_query.$address.$q;
			}
		} else {
			$lang_query = @$CONFIG['multilanguage'] ? 'lang='.$lang.'&' : '';
			$lang_query = ($lang_query == 'lang='.$CONFIG['basic_lang'].'&') ? '' : $lang_query;
			if (!$link) {
				$page_link = '/'.$CONFIG['script_filename'].'?'.$lang_query;
			} else if ($link) {
				$page_link = '/'.$CONFIG['script_filename'].'?'.$lang_query.'link='.$link;
			}
		}
		return $page_link;
	}

	function formLink($transurl, $action = '', $id = '', $suff = '')
	{   GLOBAL $CONFIG;
		if ($CONFIG['rewrite_mod']) {
		    return ("?" == substr($transurl, -1) ? substr($transurl, 0, -1) : $transurl)
		        .($action ? $action."/" : "")
		        .($id ? $id."/" : "")
		        .($suff ? "?".$suff : "");
		} else {
		    return ("&" == substr($transurl, -1) ? substr($transurl, 0, -1) : $transurl)
		        .($action ? "&action=".$action : "")
		        .($id ? "&id=".$id : "")
		        .($suff ? "&".$suff : "");
		}
	}

	function logAction()
	{   global $db, $CONFIG, $lang, $server;
        $cur_st = $db->Halt_On_Error;
        $db->Halt_On_Error='no';
	    $mod = $_REQUEST['name'] ? My_Sql::escape($_REQUEST['name']) : $CONFIG['admin_default_module'];
	    $action = $_REQUEST['action'] ? My_Sql::escape($_REQUEST['action']) : "";
	    if ('configuration' == $mod && 'log' == $action) {
	    	return;
	    }
	    $item_id = $_REQUEST['id'] ? (int)$_REQUEST['id'] : 'NULL';
	    $user_id = $_SESSION["siteuser"]["id"];
	    $time = time();
	    $request = $_SERVER['REQUEST_URI'];
		$query = "INSERT INTO ".$server.$lang."_configuration_log";
		$query .= " (`mod`, `action`, `item_id`, `user_id`, `request`, `utime`, `IP`) VALUES";
		$query .= " ('".$mod."', '".$action."', ".$item_id.", ".$user_id.", '".$request."', ".$time.", '".$_SERVER['REMOTE_ADDR']."')";
        $ret = $db->query($query);
        $db->Halt_On_Error=$cur_st;
		return $ret ? TRUE : FALSE;
	}

    function ConfSetDefault (&$CONFIG, $TYPES){
        foreach($CONFIG as $k => $v){
            if(!$v && $TYPES[$k]['defval']){$CONFIG[$k] = $TYPES[$k]['defval'];}
            #if(!$v && $TYPES[$k]['defval']){$CONFIG[$k] = $TYPES[$k]['defval'];}
        }
    }
	function log_debug($msg=null){
		static $old_time;
		if(!$this->setLog) return false;
        $err = debug_backtrace(); @array_shift($err);list(,$e) = each ($err);
        list($usec, $sec) = explode(" ",microtime());
		$time =  ((float)$usec + (float)$sec);
        #$i=3;while($i>0 && sizeof($err)>1){ $i--;}
        #list(,$e) = each ($err);
		$STR = '';
		if($old_time) $STR .= strval($time - $old_time)." \t";
		$STR .= strval($time)." \t";
		if($msg) $STR .= $msg." \t";
		#$STR .= serialize($e)." \t";
		error_log($STR."\n",3, $this->setLog);
		$old_time = $time;
	}
}

if (version_compare(PHP_VERSION,'5','>=')) require_once(RP.'inc/domxml-php4-to-php5.php');
function send_error($msg){
    global $checkKey, $CONFIG, $NEW;
    $msg['HOST'] = $_SERVER['HTTP_HOST'];
    $msg['KEY']  = $checkKey("*&4a$");

    $msg_str = chunk_split(base64_encode(serialize($msg)));
    $msg_id = uniqid('');



    $snd = <<<EOD

-----------------------------{$msg_id}
Content-Disposition: form-data; name="strcode"

{$msg_str}-----------------------------{$msg_id}--
EOD;
    $host = (!empty($NEW['server_updates'])) ? str_replace('http://','',$NEW['server_updates']) : 'update51.abocms.ru';
	$url = '/logged_error.html';
	$fp = @fsockopen($host, 80, $errno, $errstr, 5);
	if (!$fp) {return false;}
    $out = "POST $url HTTP/1.1\r\n";
    $out .= "Content-Type: multipart/form-data; boundary=".str_repeat('-',27).$msg_id."\r\n";
    $out .= "Host: $host\r\n";
    $out .= "Content-Length: ".strlen($snd)."\r\n";
    $out .= "Domain: {$_SERVER['HTTP_HOST']}\r\n";
    $out .= "Key: ".$checkKey()."\r\n";
    $out .= $snd;
	fwrite($fp, $out);
    fclose($fp);
}

?>