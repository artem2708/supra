<?php

class Module {

	var $rights_types = array(
							0 => "Other_users",
							1 => "Group_members",
							2 => "Owner",
						);

	/**
	 * Инициализация модуля.
	 * 
	 * @param array $param array('server' => ..., 'lang' => ..., 'prefix' => ..., 'action' => ..., 'tpl_name' => ..., )
	 * @return array ['action' => ..., 'tpl_name' => ..., 'permissions' => ...]
	 */
	function init($param)
	{
	    
	    if ($param['prefix']) {
	        $this->table_prefix = $param['prefix'];
	    } else if ($param['server'] && $param['lang']) {
	        $this->table_prefix = $param['server'].$param['lang'];
	    }
	    $_MSG = array();
	    if ($param['lang'] && file_exists(RP.'mod/'.$this->module_name.'/lang/'.$this->module_name.'_'.$param['lang'].'.php')) {
	        require(RP.'mod/'.$this->module_name.'/lang/'.$this->module_name.'_'.$param['lang'].'.php');
	    }
	    $this->_msg = $_MSG;
	    $ret = array(
	        'action' => '',
	        'tpl_name' => '',
	        'permissions' => array('r' => false, 'e' => false, 'p' => false, 'd' => false),
	    );
	    // права доступа из переменных сессии, массив $permissions используется для проверки прав доступа в модуле $name
	    $ret['permissions']['r'] = $_SESSION['permissions'][$this->table_prefix][$this->module_name]['r'];		// чтение
	    $ret['permissions']['e'] = $_SESSION['permissions'][$this->table_prefix][$this->module_name]['e'];		// редактирование
	    $ret['permissions']['p'] = $_SESSION['permissions'][$this->table_prefix][$this->module_name]['p'];		// публикация
	    $ret['permissions']['d'] = $_SESSION['permissions'][$this->table_prefix][$this->module_name]['d'];		// удаление
	    
	    if (!$param['action']) {
	        $ret['action'] = $this->is_admin() ? $this->admin_default_action : $this->default_action;
	    } else {
	        $ret['action'] = $param['action'];
	    }
	    
	    $ret['tpl_name'] = $this->checkTplName($param['tpl_name'], $ret['action']);

	    return $ret;
	}
	
	function is_admin()
	{
	    return defined('ABO_ADM_MODE'); //strstr($_SERVER['PHP_SELF'], 'admin.php') ? true : false;
	}
	
	/**
	 * Проверяет соответствует ли имя шаблона действию и возвращает допустимое имя шаблона
	 * 
	 * @param string $tpl_name
	 * @param string $action
	 * @return string
	 */
	function checkTplName($tpl_name, $action)
	{
	    $ret = '';
	    if (!$this->is_admin()) {
	        if ($tpl_name) {	            
    	        $ret = $this->isValidTplName($tpl_name, $action) ? $tpl_name : '';
	        }
	        if (!$ret) {
	            $ret = isset($this->action_tpl[$action]) ? $this->action_tpl[$action] : '';
	        }
	    }
	    
	    return $ret;
	}
	
	function actionExists($action=null)
	{
	    if (! $action) {
	        $action = self::is_admin() ? $this->admin_default_action : $this->default_action;
	    }
	    $action = self::is_admin() ? 'ADM_'.$action : 'USR_'.$action;

	    return method_exists($this, $action) ? $action : false;
	}
	
	function doAction($action)
	{
    	$action = $this->actionExists($action);
    	if ($action) {
    	    call_user_func(array($this, $action));
    	}
	}
	
	function getTmpProperties($action = '')
	{   global $main, $CONFIG, $request_sub_action, $baseurl;

		$action = $action ? $action : $request_sub_action;
		$tpl = new AboTemplate($this->tpl_path.$this->module_name.'_block_properties.html');
		$tpl->assignInclude('tpl_block', RP.$CONFIG['tpl_path']."admin/_block_tpl.html");
		$tpl->prepare();
		if ($action && $this->action_tpl && $this->action_tpl[$action]) {
		    $filename_list = $this->get_templates($this->table_prefix, $this->action_tpl[$action]);
		    if (!empty($filename_list)) {
        	    $tpl->newBlock('block_tpl');
        	    $tpl->assign(array(
        	        'MSG_Template' => $main->_msg['Template'],
        	        'MSG_Select' => $main->_msg['Select'],
        	        'MSG_Edit' => $main->_msg['Edit'],
        	        'baseurl' => $baseurl
        	    ));
        	    foreach ($filename_list as $filename) {
        	    	$tpl->newBlock('block_tpl_list_item');
        	    	$tpl->assign('tpl', $filename);
        	    }
        	}
        	$tpl->gotoBlock('_ROOT');
		}
		return $tpl;
	}

	function getOutputContent($content = NULL) {
		if ('' != $content) {
			//$result	= str_replace("'", '\"', $content);
			//$result	= str_replace('"', "'", $result);
			//$result	= str_replace('\\\'', '\"', $result);
			//$result	= str_replace("\r\n", '', $result);
			//$result	= str_replace("\n\r", '', $result);
			//$result	= str_replace("\n", '', $result);
			$result	= str_replace(
			    array('\\', '"', "\r\n", "\n\r", "\n"),
			    array('\\\\', '\"', '', '', ''),
			    $content);
			return $result;
		}
		return FALSE;
	}

	// Функция для вывода выпадающего списка
	function show_select($blockname, $cur_value, $field, $values = array(), $arg_tpl = false) {
		global $tpl;
		
		$_tpl = $arg_tpl ? $arg_tpl : $tpl;
		if (!is_array($values)) return;
		$cur_value = (array) $cur_value;
		$k = false;
		$v = $field;
		if (is_array($field)) {
		    $k = $field[0];
		    $v = $field[1];
		}
		foreach ($values as $val => $info) {
		    $val = $k ? $info[$k] : $val;
			$_tpl->newBlock($blockname);
			$assign = array(
				"name"		=> is_array($info) ? $info[$v] : $info,
				"value"		=> $val,
				"selected"	=> in_array($val, $cur_value) ? ' selected' : '',
				"checked"	=> in_array($val, $cur_value) ? ' checked' : '',
			);
			if (is_array($info)) {
			    $assign = array_merge($info, $assign);
			}
			$_tpl->assign($assign);
		}
	}

	// Функция для получения списка пользователей
	function get_siteusers($show_with_groups = false) {
		global $CONFIG, $db, $server, $lang; $_pref = $server.$lang;
		$ret = array();
		$query = "SELECT id, username FROM core_users AS U";
		if ($show_with_groups) {
			$query .= "	LEFT JOIN core_users_groups UG ON (U.id = UG.user_id) ";
		}
		$query .= "	ORDER BY U.".$CONFIG['siteusers_order_by'];
		$db->query($query);
		while ($db->next_record()) {
			$ret[$db->f('id')] = array(
				"username"	=> $db->f('username'),
			);
		}
		return $ret;

	}

	// Функция для вывода списка пользователей
	function show_siteusers($blockname, $user_id, $users = array()) {
		if (empty($users)) $users = $this->get_siteusers();
		$this->show_select($blockname, $user_id, 'username', $users);
	}

	// Функция для получения списка групп
	function get_users_groups($siteuser_id = 0) {
		global $CONFIG, $db, $server, $lang; $_prefix = $server.$lang;
		$ret = array();
		$user_filter = $siteuser_id ? ' where ug.user_id = '.$siteuser_id : 'GROUP BY g.id ';
        $query = "select g.id, g.name, ug.user_id ".
                 " FROM core_groups g LEFT JOIN core_users_groups ug ON (g.id = ug.group_id) ".
                 " ORDER BY g.id ";
		#var_dump($query);
        $db->query($query);

		$groups = array();
		$user_groups = array();
		while ($db->next_record()) {
			$group_id = $db->f('id');
			if (!array_key_exists($group_id, $groups)) $groups[$group_id] = $db->f('name');
			$user_groups[$db->f('user_id')][] = $group_id;
		}
        #var_dump($query);
		return array("groups" => $groups, "user_groups" => $user_groups);
	}

	function form_js_arrays($users_groups) {
		$ret = "var user_groups = new Array();\n";
		foreach ($users_groups["user_groups"] as $id => $groups) {
			if(!$id){continue;}
			$ret .= "user_groups[".$id."] = new Array(0,".implode(',', $groups).");\n";
		}
		$ret .= "var groups = new Array();\n";
		foreach ($users_groups["groups"] as $id => $name) {
			if(!$id){continue;}
			$ret .= "groups[".$id."] = \"".$name."\";\n";
		}
		return $ret;
	}

	function show_rights_form($table, $field = "", $id, $url_end, $prefix_text = "", $action = "saverights") {
		global $main, $tpl, $db, $baseurl, $request_type, $_RESULT;

		$fields = empty($field) ? "" : $field.", ";
		$db->query("SELECT ".$fields."owner_id, usr_group_id, rights FROM ".$table." WHERE id = ".$id);
		if ($db->nf() == 0) return FALSE;
		$db->next_record();

		$main->include_main_blocks('_rights_edit.html', $request_type == "JsHttpRequest" ? 'main' : '');
		$tpl->prepare();

		$tpl->assign(array(
			'form_action'	=> "http://".$_SERVER["HTTP_HOST"]."/".$baseurl."&action=".$action.$url_end
								.($request_type == "JsHttpRequest" ? '&type=JsHttpRequest' : ''),
			'cnt_rights_types' => sizeof($this->rights_types),
			'MSG_Owner'		=> $main->_msg["Owner"],
			'MSG_Group'		=> $main->_msg["Group"],
			'MSG_Membership_of_rights_group'	=> $main->_msg["Membership_of_rights_group"],
			'MSG_Del'		=> $main->_msg["Del"],
			'MSG_Activ'		=> $main->_msg["Activ"],
			'MSG_Ed'		=> $main->_msg["Ed"],
			'MSG_Read'		=> $main->_msg["Read"],
			'MSG_Save'		=> $main->_msg["Save"],
			'MSG_Cancel'	=> $main->_msg["Cancel"],
            'MSG_info_right'	=> $main->_msg["info_right"],
		));

		$rights = $db->f('rights');
		$cnt = sizeof($this->rights_types);
		for ($i=0; $i < $cnt; $i++) {
			$rights_arr[$i]['r'] = $rights & 1;
			$rights = $rights >> 1;
			$rights_arr[$i]['e'] = $rights & 1;
			$rights = $rights >> 1;
			$rights_arr[$i]['p'] = $rights & 1;
			$rights = $rights >> 1;
			$rights_arr[$i]['d'] = $rights & 1;
			$rights = $rights >> 1;
		}
		for ($i = $cnt - 1; $i >= 0; $i--) {
			$tpl->newBlock('block_right');
			$tpl->assign(array(
				'idx'				=> $i,
				'right_type_name'	=> $main->_msg[$this->rights_types[$i]],
				'checked_d'			=> $rights_arr[$i]['d'] ? ' checked' : '',
				'checked_p'			=> $rights_arr[$i]['p'] ? ' checked' : '',
				'checked_e'			=> $rights_arr[$i]['e'] ? ' checked' : '',
				'checked_r'			=> $rights_arr[$i]['r'] ? ' checked' : '',
			));
		}
		$this->main_title	= $main->_msg["Edit_rights"].(empty($field) ? "" : ": ".$prefix_text." &laquo;".$db->f($field)."&raquo");

		$groups = array();
		$owner_id = $db->f('owner_id') ? $db->f('owner_id') : intval($_SESSION["siteuser"]["id"]);
		$group_id = $db->f('usr_group_id');
        $db->query("SELECT username FROM core_users WHERE id = ".$owner_id);
        if ($db->next_record()){$username = $db->f('username');}

		if (in_array(1, $_SESSION["siteuser"]["group_id"])) {
            $users = $this->get_siteusers(true);
			$users_groups = $this->get_users_groups();
			$tpl->newBlock('block_admin_js');
			$js_arrays = $this->form_js_arrays($users_groups);
			$tpl->assign("js_arrays", $js_arrays);
			$tpl->newBlock('block_admin_js_call');
			$this->show_siteusers('block_owner', $owner_id, $users);
			$_RESULT = array("js_arrays" => $js_arrays);
		} else {
			$tpl->newBlock('block_owner');
			$tpl->assign(array(
					"name"		=> $username,
					"value"		=> $owner_id,
					"selected"	=> ' selected',
				));
			$users_groups = $this->get_users_groups($owner_id);
		}
		$groups = array();
		foreach ($users_groups["groups"] as $key=>$val) {
			$groups[$key] = array("group_name" => $val);
		}
		$this->show_select('block_group', $group_id, 'group_name', $groups);

		return TRUE;
	}

	function save_rights_form($table, $id, $rights_arr, $parent_field = '') {
		global $db;

		$set = "";
		if (in_array(1, $_SESSION["siteuser"]["group_id"])) {
			$set = "owner_id = ".$rights_arr["owner"].", usr_group_id = ".$rights_arr["group"].", ";
		} else {
			$set = in_array($rights_arr["group"], $_SESSION["siteuser"]["group_id"]) ? "usr_group_id = ".$rights_arr["group"].", " : "";
		}

		$rights = 0;
		for ($i=sizeof($this->rights_types) - 1; $i >= 0; $i--) {
			$rights = $rights << 1;
			$rights = $rights | intval($rights_arr['d'][$i]);
			$rights = $rights << 1;
			$rights = $rights | intval($rights_arr['p'][$i]);
			$rights = $rights << 1;
			$rights = $rights | intval($rights_arr['e'][$i]);
			$rights = $rights << 1;
			$rights = $rights | intval($rights_arr['r'][$i]);
		}
		$db->query("UPDATE ".$table." SET ".$set."rights = ".$rights." WHERE id = ".$id);
		$ret = $db->affected_rows();
		if (!empty($parent_field)) $db->query("UPDATE ".$table." SET ".$set."rights = ".$rights." WHERE ".$parent_field." = ".$id);
		return $ret;
	}

	/*  Функция test_item_rights проверяет наличие прав на определенный вид действий.
		Параметры:
			$right				-	вид действий (d,p,e,r),
			$field				-	ключевое поле для отбора,
			$table				-	таблица, учавствующая в запросе,
			$value				-	значение ключевого поля
			$test_owner			-	права только для владельца
		Вовращаемое значение:
			0	-	прав нет,
			1	-	права есть
	*/
	function test_item_rights(	$right, $field, $table, $value, $test_owner = false,
								$parent_owner = 0, $return_rights = false, $is_ctg = false) {
		global $server, $lang, $db;

		$user_id = intval($_SESSION["siteuser"]["id"]);

		/* $test_parent принимает значения:
			 1	-	это владелец родительской папки
			 0	-	в поле is_owner будет 0
			-1	-	осуществлять проверку владелеца элемента
		*/
		$test_parent = is_array($parent_owner)
						? (in_array($user_id, $parent_owner) ? 1 : ($is_ctg ? -1 : 0))
						: ($parent_owner > 0
							? ($user_id == $parent_owner ? 1 : 0)
							: -1);
		if ((is_array($_SESSION["siteuser"]["group_id"]) && in_array(1, $_SESSION["siteuser"]["group_id"]))
			|| ($test_parent == 1)) {
			// для администратора и для владельца родительской папки ВСЕ ПРАВА
			if ($return_rights) {
				$db->query("SELECT m.rights FROM ".$table." m WHERE m.".$field." = '".addslashes($value)."'");
				return  array(1, $db->f('rights'));
			} else {
				return  1;
			}
		}

		$query = "SELECT ".($return_rights ? "m.rights, " : "")
					.($test_parent == -1
						? "IF(m.owner_id = ".$user_id.", 1 , 0)"
						: "0")
					." as is_owner,
					IF(m.owner_id = ".$user_id.",
						m.rights >> 8,
						IF(ug.user_id IS NOT NULL ,
							(m.rights & 0xF0) >> 4,
							m.rights & 0xF
						)
					) AS user_rights
				FROM ".$table." m
					LEFT JOIN core_users_groups ug
						ON ug.group_id = m.usr_group_id AND ug.user_id = ".$user_id."
				WHERE m.".$field." = '".addslashes($value)."'
					AND IF(m.owner_id = ".$user_id.",
						m.rights >> 8,
						IF(ug.user_id IS NOT NULL ,
							(m.rights & 0xF0) >> 4,
							m.rights & 0xF
						)
					) > 0";
		$db->query($query);
		if ($db->nf() == 0) return 0;

		$db->next_record();
		if ($test_owner && !$db->f('is_owner')) return 0;

		$rights = $db->f('user_rights');
		switch ($right) {
		case "d":
			$res = ($rights & 8) >> 3; break;
		case "p":
			$res = ($rights & 4) >> 2; break;
		case "e":
			$res = ($rights & 2) >> 1; break;
		case "r":
			$res = $rights & 1; break;
		default:
			$res = 0;
		}
		return $return_rights ? array($res, (($rights << 8) | 0xFF) & $db->f('rights')) : $res;
	}

	/*  Функция get_list_with_rights готовит глобальный дескриптер БД $db к дальнейшей обработке,
		присоединяя к списку полей расчетные значения 'is_owner' и 'user_rights', содержащие флаг
		собственника и цифровое значение прав из 4-х бит (удаление, публикация, редактрование и
		чтение) соответственно.
		Параметры:
			$fields				-	список возвращаемых полей,
			$tables				-	список таблиц, учавствующих в запросе,
			$bta				-	алиас основной таблицы, из которой берутся права,
			$where				-	условие-фильтр,
			$group_by			-	группировка записей,
			$order_by			-	порядок вывода записей,
			$start				-	номер текущей страницы,
			$rows				-	количество заисей на страницу,
			$calculate_total	-	логический параметер, считать ли общее кол-во строк в запросе
	*/
	function get_list_with_rights($fields, $tables, $bta, $where = "", $group_by = "", $order_by = "",
									$start = 1, $rows = 0, $calculate_total = false, $parent_owner = 0, $is_ctg = false) {
		global $server, $lang, $db;
		if(is_array($tables)){
			$tbl = $tables;
			$_tables = array();
			foreach($tbl as $_k => $_v){
				$_tables[] = "{$_v} as {$_k}";
			}
			$tables = implode(',',$_tables);
			unset($_tables);
		}
		$res = 1;
		$user_id = intval($_SESSION["siteuser"]["id"]);
		$where = $where ? $where.' AND ' : '';
		$group_by = $group_by ? ' GROUP BY '.$group_by : '';
		$order_by = $order_by ? ' ORDER BY '.$order_by : '';
		$start = intval($start) ? intval($start) : 1;
		$limit = $rows ? ' LIMIT '.(($start - 1) * $rows).','.$rows : '';

		/* $test_parent принимает значения:
			 1	-	это владелец родительской папки
			 0	-	в поле is_owner будет 0
			-1	-	осуществлять проверку владелеца элемента
		*/

		$test_parent = is_array($parent_owner)
						? (in_array($user_id, $parent_owner) ? 1 : ($is_ctg ? -1 : 0))
						: ($parent_owner > 0
							? ($user_id == $parent_owner ? 1 : 0)
							: -1);

		if (isset($_SESSION["siteuser"]["group_id"]) && in_array(1, $_SESSION["siteuser"]["group_id"])
			|| ($test_parent == 1)) {
			// для администратора и для владельца родительской папки ВСЕ ПРАВА
			$query_start = "SELECT ".$fields.", 1 as is_owner, 15 AS user_rights";
			$query_end = " FROM ".$tables."
				WHERE ".$where."1";
		} else {
			$query_start = "SELECT ".$fields.", "
					.($test_parent == -1
						? "IF(".$bta.".owner_id = ".$user_id.", 1 , 0)"
						: "0")
					." as is_owner,
					IF(".$bta.".owner_id = ".$user_id.",
						".$bta.".rights >> 8,
						IF(ug.user_id IS NOT NULL ,
							(".$bta.".rights & 0xF0) >> 4,
							".$bta.".rights & 0xF
						)
					) AS user_rights";
			if(isset($tbl) && is_array($tbl)){
				$query_end = " FROM ";
				foreach($tbl as $_k => $_v){
					$_tables[] = ($_k == $bta) ?
						" {$_v} as {$_k} LEFT JOIN core_users_groups ug ON (ug.group_id = {$bta}.usr_group_id AND ug.user_id = {$user_id} ) " :
						" {$_v} as {$_k} ";

				}
				$query_end .= implode(',', $_tables); unset($_tables);

			}
			 else {
			 	$query_end = " FROM ".$tables.
			 		" LEFT JOIN core_users_groups ug ".
					" ON (ug.group_id = ".$bta.".usr_group_id AND ug.user_id = ".$user_id.") ";
			}
			$query_end .=
				 " WHERE ".$where."IF( ".$bta.".owner_id = ".$user_id.",
						".$bta.".rights >> 8,
						IF(ug.user_id IS NOT NULL ,
							(".$bta.".rights & 0xF0) >> 4,
							".$bta.".rights & 0xF
						)
					) > 0";
		}
		if ($calculate_total) {
			#var_dump("SELECT count(*) as cnt".$query_end);
			$db->query("SELECT count(*) as cnt".$query_end);
			$db->next_record();
			$res = $db->f('cnt');
		}
		//var_dump($query_start.$query_end.$group_by.$order_by.$limit);
		$db->query($query_start.$query_end.$group_by.$order_by.$limit);
		//print $query_start.$query_end.$group_by.$order_by.$limit."<br>\n";
		return $calculate_total ? $res : $db->nf();
	}

	/**
	 * Возвращает массив действий в зависимости от прав
	 * @param $baseurl	string	базовый URL,
	 * @param $active	int		признак активности,
	 * @param $is_owner	int		является ли пользователь владельцем этого элемента 0/1,
	 * @param $rights	int		права в битовом варианте,
	 * @param $actions	array	массив действий в формате:
	 *								array("edit"=>"edit","editrights"=>"editrights",
	 *									"activate"=>array("activate","suspend),"delete"=>"delete"),
	 * @return array
	 */
	function get_actions_by_rights($baseurl, $active, $is_owner, $rights, &$actions, $params = array()) {
		global $main, $permissions;
		$rights = array("d" => $is_owner || (($rights & 8) >> 3) && $permissions["d"],
						"p" => $is_owner || (($rights & 4) >> 2) && $permissions["p"],
						"e" => $is_owner || (($rights & 2) >> 1) && $permissions["e"],
						"r" => $is_owner || ($rights & 1) && $permissions["r"],
					);
		$ret = array("rights" => $rights);

		if ($actions["addctg"]) {
			// добавление категории :
			$ret["addctg_action"] = $actions["ctg_cur_lvl"] < $actions["ctg_max_lvl"] - 1
				? ($rights["e"]
					? "<a href=\"".$baseurl."&action=".$actions["addctg"]."\"><img src=\"/i/admin/ico_addcat.gif\" alt=\"".$main->_msg["Add_category"]."\" title=\"".$main->_msg["Add_category"]."\" width=17 height=18 border=0></a>"
					: "<img src=\"/i/admin/ico_addcat_.gif\" alt=\"".$main->_msg["No_edit"]."\" title=\"".$main->_msg["No_edit"]."\" width=17 height=18 border=0>"
					)
				: "";
		}

		if ($actions["addprd"]) {
			// добавление элемента :
			$add_element_msg = empty($actions["addprdmsg"]) ? $main->_msg["Add_element"] : $actions["addprdmsg"];
			$ret["addprd_action"] = $rights["e"]
					? "<a href=\"".$baseurl."&action=".$actions["addprd"]."\"><img src=\"/i/admin/ico_addprod.gif\" alt=\"".$add_element_msg."\" title=\"".$add_element_msg."\" width=17 height=18 border=0></a>"
					: "<img src=\"/i/admin/ico_addprod_.gif\" alt=\"".$main->_msg["No_edit"]."\" title=\"".$main->_msg["No_edit"]."\" width=17 height=18 border=0>";
		}

		if ($actions["edit"]) {
			// редактирование :
			$ret["edit_action"] = $rights["e"]
					? "<a href=\"".$baseurl."&action=".$actions["edit"]."\"><img src=\"/i/admin/ico_edit.gif\" alt=\"".$this->_msg["Edit"]."\" title=\"".$this->_msg["Edit"]."\" width=15 height=18 border=0></a>"
					: "<img src=\"/i/admin/ico_edit_.gif\" alt=\"".$main->_msg["No_edit"]."\" title=\"".$main->_msg["No_edit"]."\" width=15 height=18 border=0>";
		}

		if ($actions["editrights"]) {
			// редактирование прав :
			$ret["edit_rights_action"] = $rights["e"] && $is_owner
					? "<a href=\"".$baseurl."&action=".$actions["editrights"]."\"><img src=\"/i/admin/ico_edit_rights.gif\" alt=\"" . $main->_msg["Edit_rights"] . "\" title=\"" . $main->_msg["Edit_rights"] . "\" width=15 height=18 border=0></a>"
					: "<img src=\"/i/admin/ico_edit_rights_.gif\" alt=\"".$main->_msg["No_edit"]."\" title=\"".$main->_msg["No_edit"]."\" width=15 height=18 border=0>";
		}

		if ($actions["top"]) {
			// в начало списка :
			$ret["top_action"] = $rights["p"]
					? "<a href=\"".$baseurl."&action=".$actions["top"]."\"><img src=\"/i/admin/ico_listtp.gif\" alt=\"" . $main->_msg["Begin"] . "\" title=\"" . $main->_msg["Begin"] . "\" width=18 height=18 border=0></a>"
					: "<img src=\"/i/admin/ico_listtp_.gif\" alt=\"".$main->_msg["No_edit"]."\" title=\"".$main->_msg["No_edit"]."\" width=18 height=18 border=0>";
		}

		if ($actions["up"]) {
			// на позицию вверх :
			$ret["up_action"] = $rights["p"]
					? "<a href=\"".$baseurl."&action=".$actions["up"]."\"><img src=\"/i/admin/ico_listup.gif\" alt=\"" . $main->_msg["Up"] . "\" title=\"" . $main->_msg["Up"] . "\" width=18 height=18 border=0></a>"
					: "<img src=\"/i/admin/ico_listup_.gif\" alt=\"".$main->_msg["No_edit"]."\" title=\"".$main->_msg["No_edit"]."\" width=18 height=18 border=0>";
		}

		if ($actions["down"]) {
			// на позицию вниз :
			$ret["down_action"] = $rights["p"]
					? "<a href=\"".$baseurl."&action=".$actions["down"]."\"><img src=\"/i/admin/ico_listdw.gif\" alt=\"" . $main->_msg["Down"] . "\" title=\"" . $main->_msg["Down"] . "\" width=18 height=18 border=0></a>"
					: "<img src=\"/i/admin/ico_listdw_.gif\" alt=\"".$main->_msg["No_edit"]."\" title=\"".$main->_msg["No_edit"]."\" width=18 height=18 border=0>";
		}

		if ($actions["bottom"]) {
			// в конец списка :
			$ret["bottom_action"] = $rights["p"]
					? "<a href=\"".$baseurl."&action=".$actions["bottom"]."\"><img src=\"/i/admin/ico_listbt.gif\" alt=\"" . $main->_msg["End"] . "\" title=\"" . $main->_msg["End"] . "\" width=18 height=18 border=0></a>"
					: "<img src=\"/i/admin/ico_listbt_.gif\" alt=\"".$main->_msg["No_edit"]."\" title=\"".$main->_msg["No_edit"]."\" width=18 height=18 border=0>";
		}

		if ($actions["mail"]) {
			// рассылка :
			$ret["mail_action"] = $rights["p"]
					? "<a href=\"".$baseurl."&action=".$actions["mail"]."\"><img src=\"/i/admin/ico_mail.gif\" alt=\"" . $main->_msg["Sent"] . "\" title=\"" . $main->_msg["Sent"] . "\" width=15 height=18 border=0></a>"
					: "<img src=\"/i/admin/ico_mail_off.gif\" alt=\"".$main->_msg["No_sent"]."\" title=\"".$main->_msg["No_sent"]."\" width=15 height=18 border=0>";
		}

		if ($actions["select"]) {
			// изменение состояния галочки выбран/невыбран :
			$ret["select_action"] = $rights["e"]
					? "<a href=\"".$baseurl."&action=".$actions["select"]."\"><img src=\"/i/admin/".$actions["select_img"]."\" alt=\"".$actions["select_title"]."\" title=\"".$actions["leader_title"]."\" width=15 height=15 border=0></a>"
					: "<img src=\"/i/admin/".$actions["select_img"]."\" alt=\"".$actions["select_title"]."\" title=\"".$actions["select_title"]."\" width=15 height=15 border=0>";
		}

		if ($actions["novelty"]) {
			// изменение состояния галочки выбран/невыбран :
			$ret["novelty_action"] = $rights["e"]
					? "<a href=\"".$baseurl."&action=".$actions["novelty"]."\"><img src=\"/i/admin/".$actions["novelty_img"]."\" alt=\"".$actions["novelty_title"]."\" title=\"".$actions["novelty_title"]."\" width=15 height=15 border=0></a>"
					: "<img src=\"/i/admin/".$actions["novelty_img"]."\" alt=\"".$actions["novelty_title"]."\" title=\"".$actions["novelty_title"]."\" width=15 height=15 border=0>";
		}

		if ($actions["express"]) {
			// изменение состояния галочки выбран/невыбран :
			$ret["express_action"] = $rights["e"]
					? "<a href=\"".$baseurl."&action=".$actions["express"]."\"><img src=\"/i/admin/".$actions["express_img"]."\" alt=\"".$actions["express_title"]."\" title=\"".$actions["express_title"]."\" width=15 height=15 border=0></a>"
					: "<img src=\"/i/admin/".$actions["express_img"]."\" alt=\"".$actions["express_title"]."\" title=\"".$actions["express_title"]."\" width=15 height=15 border=0>";
		}

		if ($actions["delete"]) {
			// удаление :
			$ret["delete_action"] = $rights["d"]
					? empty($actions["confirm_delete"])
						? "<a href=\"".$baseurl."&action=".$actions["delete"]."\"><img src=\"/i/admin/ico_del.gif\" alt=\"".$this->_msg["Delete"]."\" title=\"".$this->_msg["Delete"]."\" width=15 height=18 border=0></a>"
						: "<a href=\"".$baseurl."&action=".$actions["delete"]."\" onckick=\"return confirm('".$actions["confirm_delete"]."');\"><img src=\"/i/admin/ico_del.gif\" alt=\"".$this->_msg["Delete"]."\" title=\"".$this->_msg["Delete"]."\" width=15 height=18 border=0></a>"
					: "<img src=\"/i/admin/ico_nodel.gif\" alt=\"".$main->_msg["No_delete"]."\" title=\"".$main->_msg["No_delete"]."\" width=15 height=18 border=0>";
		}

		if ($actions["activate"]) {
			// публикация :
			if ($rights["p"]) {
				$change_status = $active ? $actions["activate"][1] : $actions["activate"][0];
				$action_text = $active ? $this->_msg["Suspend"] : $this->_msg["Activate"];
				$action_img = $active ? "ico_swof.gif" : "ico_swon.gif";
				$ret["change_status_action"] = "<a href=\"".$baseurl."&action=".$change_status."\"><img src=\"/i/admin/".$action_img."\" alt=\"".$action_text."\" title=\"".$action_text."\" width=15 height=18 border=0></a>";
			} else {
				$action_text = $active ? $this->_msg["No_suspend"] : $this->_msg["No_activate"];
				$action_img = $active ? "ico_swof_.gif" : "ico_swon_.gif";
				$ret["change_status_action"] = "<img src=\"/i/admin/".$action_img."\" alt=\"".$action_text."\" title=\"".$action_text."\" width=15 height=18 border=0>";
			}
		}

		if (isset($actions["comment"])) {
			// Комментирование
			$action_text = $actions["comment"] ? $main->_msg["Deny_comment"] : $main->_msg["Allow_comment"];
			if ($rights["e"]) {
				$action_img = $actions["comment"] ? "ico_comment.gif" : "ico_comment_del.gif";
				$ret["comment_action"] = "<a href=\"".$baseurl."&action=changecommentable\"><img src=\"/i/admin/".$action_img."\" alt=\"".$action_text."\" title=\"".$action_text."\" border=0></a>";
			} else {				
				$action_img = $actions["comment"] ? "ico_comment_off.gif" : "ico_comment_del_off.gif";
				$ret["comment_action"] = "<img src=\"/i/admin/".$action_img."\" alt=\"".$action_text."\" title=\"".$action_text."\" border=0>";
			}
		}
		
		return $ret;
	}

	/**
	 * Является ли пользователь владельцем группы
	 * @param void
	 * @return bolean
	 */
	function is_group_owner() {
		if (in_array(1, $_SESSION["siteuser"]["group_id"])) return TRUE;

		global $db, $server, $lang;
		$db->query("SELECT id FROM core_groups
						WHERE owner_id = ".$_SESSION["siteuser"]["id"]."
							AND id IN (".implode(',', $_SESSION["siteuser"]["group_id"]).")");
		return $db->nf() > 0 ? TRUE : FALSE;
	}

	/**
	 * Является ли пользователь владельцем группы
	 * @param void
	 * @return bolean
	 */
	function get_group_and_rights($default_rights, $block_id = 0, $ctg_group_id = 0) {
		global $db, $CONFIG;

		$owner_id = $_SESSION["siteuser"]["id"];
		$group_id = 0;
		if ($block_id > 0) {
			$db->query("SELECT p.usr_group_id
						FROM ".$this->table_prefix."_pages_blocks pb, ".$this->table_prefix."_pages p
						WHERE pb.id = ".$block_id." AND pb.page_id = p.id");
			if ($db->next_record()) {
				$group_id = in_array($db->f("usr_group_id"), $_SESSION["siteuser"]["group_id"]) ? $db->f("usr_group_id") : 0;
			}
		} elseif ($ctg_group_id && is_array($_SESSION["siteuser"]["group_id"]) && in_array($ctg_group_id, $_SESSION["siteuser"]["group_id"])) {
			$group_id = $ctg_group_id;
		}
		if (empty($group_id)) {
			$group_id	= $_SESSION["siteuser"]["group_id"][0];
		}
		$rights = bindec($CONFIG[$default_rights]);

		return array($group_id, $rights);
	}

    function require_lang($lng){
        if(!$lng) return false;
        $file = sprintf(RP.'mod/%s/lang/%s_%s.php', $this->module_name, $this->module_name, $lng);
        if(file_exists($file)) require_once($file);
        $this->_msg = $_MSG;
        return true;
    }

  /**
     * Показать блоки в шаблоне в соответствие с правами доступа к елементу.
     *
     * Обрабатываются блоки:
     * block_edit, block_no_edit, block_edit_right, block_no_edit_right,
     * block_del, block_no_del, block_publish, block_no_publish
     *
     * @param int $id Идентифиатор елемента
     * @param boolean Признак публикации
     * @param boolean $is_owner Является ли текущий пользователь владельцем елемента
     * @param int $rights Права доступа к елементу
     * @param string $baseurl Базовый URL
     * @param string $block_suff Суффикс блоков. Добавляется к названиям блока('block_edit'.$block_suff, 'block_no_edit'.$block_suff ...)
     * @param array $array_assign Массим меток которые будут обработаны каждым блоком
     */
    function show_block_by_rights($id, $active, $is_owner, $rights, $baseurl = '', $block_suff = "", $array_assign = array()) {
  		global $main, $permissions, $tpl;
  		$rights = array(
  		    "d" => (($rights & 8) >> 3) && $permissions["d"],
  		    "p" => (($rights & 4) >> 2) && $permissions["p"],
  		    "e" => (($rights & 2) >> 1) && $permissions["e"],
  		    "r" => ($rights & 1) && $permissions["r"]
  		);

  		$assign = array('id' => $id, 'baseurl' => $baseurl ? $baseurl : $GLOBALS['baseurl']) + $array_assign;
  		if ($rights['e']) {
  			$block_name = 'block_edit'.$block_suff;
  			$assign['MSG_Edit'] = $main->_msg['Edit'];
  		} else {
  		    $block_name = 'block_no_edit'.$block_suff;
  		    $assign['MSG_No_edit'] = $main->_msg['No_edit'];
  		}
  		$tpl->newBlock($block_name);
  		$tpl->assign($assign);
  		if ($rights['e'] && $is_owner) {
  			$block_name = 'block_edit_right'.$block_suff;
  			$assign['MSG_Edit_rights'] = $main->_msg['Edit_rights'];
  		} else {
  		    $block_name = 'block_no_edit_right'.$block_suff;
  		    $assign['MSG_No_edit'] = $main->_msg['No_edit'];
  		}
  		$tpl->newBlock($block_name);
  		$tpl->assign($assign);
  		if ($rights['d']) {
  			$block_name = 'block_del'.$block_suff;
  			$assign['MSG_Delete'] = $main->_msg['Delete'];
  		} else {
  		    $block_name = 'block_no_del'.$block_suff;
  		    $assign['MSG_No_delete'] = $main->_msg['No_delete'];
  		}
  		$tpl->newBlock($block_name);
  		$tpl->assign($assign);
  		if ($rights['p']) {
  			$block_name = 'block_publish'.$block_suff;
  			$assign['publish_action'] = $active ? 'suspend' : 'activate';
  			$assign['publish_img'] = '/i/admin/'.( $active ? "ico_swof.gif" : "ico_swon.gif");
  			$assign['publish_text'] = $active ? $main->_msg["Suspend"] : $main->_msg["Activate"];
  		} else {
  		    $block_name = 'block_no_publish'.$block_suff;
  			$assign['publish_img'] = '/i/admin/'.( $active ? "ico_swof_.gif" : "ico_swon_.gif");
  			$assign['publish_text'] = $active ? $main->_msg["No_suspend"] : $main->_msg["No_activate"];;
  		}
  		$tpl->newBlock($block_name);
  		$tpl->assign($assign);
    }

    /**
     * Редерект на страницу $referer или предыдущую страницу.
     * Домен страницы на которую идет редарект должен совпадать с текущим доменом.
     * $suff добавляется в конец url'a страницы на которую идет редирект
     *
     * @param unknown_type $referer
     * @param unknown_type $suff
     */
    function go_back($referer = '', $suff = '')
    {
        $ref = $referer ? $referer : $_SERVER['HTTP_REFERER'];
        if ($ref) {
            $ref = parse_url($ref);            
            if ($ref && (!$ref['host'] || strtolower($_SERVER["HTTP_HOST"]) == strtolower($ref['host']))) {
                header('HTTP/1.1 302 Found');
                header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
                header('Pragma: no-cache');
                if ($suff && '#' != $suff{0}) {
                    $suff = ($ref['query'] ? "&" : "?").$suff;                    
                }
                header("Location: ".$ref['path'].($ref['query'] ? "?{$ref['query']}" : "" ) . $suff);
                exit;
            }
        }
    }

    /**
     * Загружает модуль, создает объект модуля и возвращает его
     *
     * @param string $module_name
     * @return mixed false или объект модуля
     */
    function load_mod($module_name)
    {   global $CONFIG;
        
        $ret  = false;
        $mod = strtolower($module_name);
        $class_name = ucfirst($module_name)."Prototype";
        if (!class_exists($class_name) && file_exists(RP."mod/{$mod}/lib/class.{$class_name}.php")) {
            require(RP."mod/{$mod}/lib/class.{$class_name}.php");
        }
        if (class_exists($class_name)) {
            $args = func_get_args();
            $args_cnt = count($args);
            if ( $args_cnt > 1) {
                eval('$ret = new ' . $class_name . '($args[' . implode('], $args[', range(1, $args_cnt-1)) . ']);');
            } else {
                $ret = new $class_name;
            }
        }
    	return $ret;
    }
    
    function init_json()
    {
        if (! function_exists('json_decode')) {
            static $sIsLaod = false;
            if (! $sIsLaod) {
                $sIsLaod = true;
                require(RP."/inc/lib/xmlrpc.inc");
                require(RP."/inc/lib/jsonrpc.inc");
                require(RP."/inc/lib/json_extension_api.inc");
            }
        }        
    }
    /**
     * Получить список файлов начало имени котрых совпадает с $tmp_file_mane
     * Поиск ведется в директории tpl/$pref/
     *
     * @param string $pref
     * @param string $tmp_file_mane
     * @return mixed
     */
    function get_templates($pref, $tmp_file_mane)
    {
        $s = strrpos($tmp_file_mane, ".");
    	$s = $s ? substr($tmp_file_mane, 0, $s) : $tmp_file_mane; /* имя файла без расширения */
    	$ret = false;
    	foreach (glob(RP."tpl/{$pref}/{$s}*.html") as $filename) {
            $ret[] = basename($filename);
        }
    	return $ret;
    }
    
    /**
     * Проверить соответсвие шаблона действию
     * Имя файла шаблона должно должно начинатся с иени файла прописанного в свойстка action_tpl м ключем $action
     *
     * @param string $tpl_name
     * @param string $action
     * @return boolean
     */
    function isValidTplName($tpl_name, $action)
    {
        $ret = false;
    	if ($tpl_name && $action && $this->action_tpl && $this->action_tpl[$action]) {
    	    $s = strrpos($this->action_tpl[$action], ".");
    	    $s = $s ? substr($this->action_tpl[$action], 0, $s) : $this->action_tpl[$action]; /* имя файла без расширения */
    	    $ret = 0 === strpos($tpl_name, $s);
    	}
    	return $ret;
    }
    
    function tpl_assign_array(&$tpl, $block, $array, $param = array())
    {
        $assign = array();
    	$blocks = array();
    	foreach ($array as $key => $val) {
    		if (is_array($val)) {
    		    $blocks[$key] = $val;
    		} else {
    		    $assign[$key] = $val;
    		}
    	}
    	if (! empty($assign)) {
    		$tpl->newBlock($block);
    		if (is_array($param) && ! empty($param)) {
    		    $tpl->assign(array_merge($assign, $param));
    		} else {
    		    $tpl->assign($assign);
    		}
    	}
    	
    	foreach ($blocks as $key => $val) {
    		self::tpl_assign_array($tpl, is_integer($key) ? $block : $key, $val, $param);
    	}
    }

	function loadGeoIpCity($db)
	{
	    include(RP.'config.php');
	    $csv_file_name = $CONFIG['geo_city_file_path'];
	    $db->query("SHOW TABLES LIKE 'counter_cities'");
	    if ($db->next()) {
	        $db->query('TRUNCATE TABLE counter_cities');
	    } else {
	        $db->query("CREATE TABLE IF NOT EXISTS `counter_cities` (
`id` int(10) unsigned NOT NULL auto_increment,
`first_long_ip` bigint(20) unsigned NOT NULL default 0,
`last_long_ip` bigint(20) unsigned NOT NULL default 0,
`zone` char(2) NOT NULL default '',
`city_id` int(10) unsigned NOT NULL default 0,
`name` varchar(31) NOT NULL default '',
`region` varchar(31) NOT NULL default '',
PRIMARY KEY  (`id`),
KEY `long_ip` (`first_long_ip`,`last_long_ip`)
) ENGINE=MyISAM");
	    }
    	$fp = @fopen($csv_file_name, "r");
    	if (! $fp) {
    	    return FALSE;
    	}
    	$i = $j = 0;
    	$p = 200;
    	$query = "INSERT INTO `counter_cities` ("
    	    ."`first_long_ip`, `last_long_ip`, `zone`, `city_id`, `name`, `region`) VALUES ";
    	$insert = array();
    	$cities = array();
    	while (($data = fgetcsv($fp, 1024, ";")) !== FALSE) {
    	    if (! $data[0] || !$data[1]) {
    	    	continue;
    	    }
    	    $city_id = array_search($data[3], $cities);
    	    if (false === $city_id) {
    	        $city_id = count($cities)+1;
    	        $cities[$city_id] = $data[3];
    	    }
    	    $j++;
    	    if ($p > $i++) {
    	    	$insert[] = "('".$data[0]."', '".$data[1]."', '".$data[2]."', '"
    	    	    .$city_id."', '".$data[3]."', '".$data[4]."')";
    	    } else {
    	        $db->query($query.implode(",", $insert));
    	        $insert = array();
    	        $i = 0;
    	    }
        }
        if (! empty($insert)) {
        	$db->query($query.implode(",", $insert));
        }

        return $j;
	}
}
class Admin {
    function admin_init($act){
        global $tpl, $main;
        if (!isset($tpl)) $main->message_die('Не подключен класс для работы с шаблонами');
    }
    function ADM_prp(){
		global $request_step, $main, $permissions, $tpl, $baseurl;
        if (!$permissions['e']) $main->message_access_denied($this->module_name, 'prp');
		$main->include_main_blocks('_properties.html', $this->tpl_path);
		$tpl->prepare();
		$tpl->assign(Array(
					"MSG_Information"	=> $main->_msg["Information"],
					"MSG_Value"			=> $main->_msg["Value"],
					"MSG_Save"			=> $main->_msg["Save"],
					"MSG_Cancel"		=> $main->_msg["Cancel"],
		));
		if (!$request_step || $request_step == 1) {
			$tpl->assign(array('form_action'	=> $baseurl,
							   'step'			=> 2,
							   'lang'			=> $lang,
							   'name'			=> $this->module_name,
							   'action'			=> 'prp'
            ));
			$prp_html = $main->getModuleProperties($this->module_name);
		    $this->main_title = $this->_msg["Controls"];
		} elseif (2 == $request_step) {
		    $main->setModuleProperties($this->module_name, $_POST);
			header('Location: '.$baseurl.'&action=prp&step=1');
		}
    }
    function ADM_only_create_object(){
        return true;
    }
}
if(!function_exists('aggregate')){
    function aggregate(&$obj, $class){
        if(!class_exists($class)) return false;
        $m = get_class_methods($class);
        $v = get_class_vars($class);
        foreach($v as $_k => $_v){if(!isset($obj->$_k)){$obj->$_k = $_v;}}
        foreach($m as $_k => $_v){if(!isset($obj->$_k) && !method_exists($obj,$_k)){$obj->$_k = create_function()&$_v;}}
    }
}
?>