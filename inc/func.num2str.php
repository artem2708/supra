<?php
/**
 * Получить форму существительного для целого числа > 0
 *
 * @param int $num
 * @param array $arg  формы существительного ('год', 'года', 'лет')
 * @return mixed
 */
 function get_noun_form_for_num($num, $arg)
 {

     if ( !is_array($arg) ) {
         return false;
     }

     if ( !preg_match("/^\-?\d/U", $num) ) {
         return false;
     }

     $last = fmod($num, 10);
     $p_last = floor(fmod($num, 100)/10);

     if ( $last == 0 || $p_last == 1 || ( $last > 4 && $last < 10) ) {
         return $arg[2];
     }
     if ($last > 1 && $last < 5) {
         return $arg[1];
     }
     return $arg[0];
 }

 /**
  * Получить сроковое представление целого числа  > 0 и < 1000
  *
  * @param int $int
  * @param boolean $is_thsnd
  * @return mixed
  */
 function smallint2str($int, $is_thsnd = false)
 {
     if ($int < 0 || $int >= 1000) {
         return FASLE;
     }
     $_1[0]  = "00";
     $_1[1]  = "один";
     $_1[2]  = "два";
     $_1[3]  = "три";
     $_1[4]  = "четыре";
     $_1[5]  = "пять";
     $_1[6]  = "шесть";
     $_1[7]  = "семь";
     $_1[8]  = "восемь";
     $_1[9]  = "девять";
     $_1[10] = "десять";
     $_1[11] = "одиннацать";
     $_1[12] = "двенадцать";
     $_1[13] = "тринадцать";
     $_1[14] = "четырнадцать";
     $_1[15] = "пятнадцать";
     $_1[16] = "шестнадцать";
     $_1[17] = "семнадцать";
     $_1[18] = "восемнадцать";
     $_1[19] = "девятнадцать";

     $_10[2] = "двадцать";
     $_10[3] = "тридцать";
     $_10[4] = "сорок";
     $_10[5] = "пятьдесят";
     $_10[6] = "шестьдесят";
     $_10[7] = "семьдесят";
     $_10[8] = "восемдесят";
     $_10[9] = "девяносто";

     $_100[1] = "сто";
     $_100[2] = "двести";
     $_100[3] = "триста";
     $_100[4] = "четыреста";
     $_100[5] = "пятьсот";
     $_100[6] = "шестьсот";
     $_100[7] = "семьсот";
     $_100[8] = "восемьсот ";
     $_100[9] = "девятьсот";

     $_1000[1]="одна";
     $_1000[2]="две";

     if (0 == $int) {
         return $_1[0];
     }

     $ret = '';
     $rsr = (int) $int;
     if ($rsr >= 100) {
         $ret.=$_100[floor($rsr/100)]." ";
         $rsr = fmod($rsr, 100);
     }

     if ($rsr >= 20) {
         $ret.=$_10[floor($rsr/10)]." ";
         $rsr = fmod($rsr, 10);
         $ret.= $is_thsnd && $rsr <3 ? $_1000[$rsr] : (0 != $rsr ? $_1[$rsr] : "");
     } else {
         $ret.= $is_thsnd && $rsr <3 ? $_1000[$rsr] : (0 != $rsr ? $_1[$rsr] : "");
     }

     return $ret;

 }

 /**
  * Получить сроковое представление числа
  *
  * @param unknown_type $num
  * @param unknown_type $int_name  формы существительного целой части
  * @param unknown_type $rest_name формы существительного вещественной части
  * @return unknown
  */
 function num2str($num, $int_name = array("рубль", "рубля", "рублей"), $rest_name = array("копейка", "копейки", "копеек"))
 {


     $name_mllrd = array("миллиард", "миллиарда", "миллиардов");
     $name_mlln  = array("миллион", "миллиона", "миллионов");
     $name_thsnd = array("тысяча", "тысячи", "тысяч");

     $int = (int) $num;
     $rest = round(($num - (int)$num)*100);

     $ret = "";
     if ($int >= 1000000000) {
     	$ret = get_noun_form_for_num(smallint2str( floor($int/1000000000) ), $name_mllrd);
     }

     $rsr = $int;
     $mllrd = $rsr >= 1000000000 ? floor($rsr/1000000000) : 0;
     $rsr = fmod($rsr, 1000000000);
     $mlln = $rsr >= 1000000 ? floor($rsr/1000000) : 0;
     $rsr = fmod($rsr, 1000000);
     $thsnd = $rsr >= 1000 ? floor($rsr/1000) : 0;
     $rsr = fmod($rsr, 1000);

     return ($mllrd ? smallint2str($mllrd) . " " . get_noun_form_for_num($mllrd, $name_mllrd). " " : "")
         . ($mlln ? smallint2str($mlln) . " " . get_noun_form_for_num($mlln, $name_mlln). " " : "")
         . ($thsnd ? smallint2str($thsnd, true) . " " . get_noun_form_for_num($thsnd, $name_thsnd). " " : "")
         . smallint2str($rsr) . " " . get_noun_form_for_num($int, $int_name)
         . " " . smallint2str($rest) . " " . get_noun_form_for_num($rest, $rest_name );
 }
?>