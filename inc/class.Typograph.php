<?php
class Typograph
{
	var $_TAG = "\xAC";
	var $_LAQUO = "\xAB";
	var $_RAQUO = "\xBB";
	var $_LDQUO = "\x84";
	var $_RDQUO = "\x93";
	var $_MDASH = "\x97";
	var $_NDASH = "\x96";
	var $_HELLIP = "\x85";

	function xxxTypo($x, &$Refs)
	{
		$Refs[] = stripslashes($x);
		return $this->_TAG . (count($Refs) - 1) . $this->_TAG;
	}

	function typograph($text = "")
	{
		$Refs = array();
		$text = preg_replace('{<!--.*?-->}es', "\$this->xxxTypo('\\0',\$Refs,'c')", $text);
		$PrivateTags = array("script","style","pre","title");

		foreach($PrivateTags as $tag)
			$text = preg_replace('{<\s*' . $tag . '[\s>].*?<\s*/\s*' . $tag . '\s*>}ies', "\$this->xxxTypo('\\0',\$Refs)", $text);
		$text = preg_replace('{<(?:[^\'"\>]+|".*?"|\'.*?\')+>}es', "\$this->xxxTypo('\\0',\$Refs)", $text);

		$text = preg_replace('/&quot;/', "\"", $text);
		$text = preg_replace('/&nbsp;/', " ", $text);
		$text = preg_replace('/' . chr(160) . '/', " ", $text);
		$text = preg_replace('/�/', "\"", $text);
		$text = preg_replace('/�/', "\"", $text);
		$text = preg_replace('/�/', "\"", $text);
		$text = preg_replace('/�/', "\"", $text);
		$text = preg_replace('/�/', "\"", $text);
		$text = preg_replace('/�/', "...", $text);
		$text = preg_replace('/�/', "-", $text);
		$text = preg_replace('/�/', "-", $text);

	    $text = preg_replace('{(\s|^|\(|\{|\[|")"}s', "\\1" . $this->_LAQUO, $text);
	    $text = preg_replace('{(\s|^|\(|\{|\[|")((?:' . $this->_TAG . '\d+' . $this->_TAG . ')+)?"}s', "\\1\\2" . $this->_LAQUO, $text);
		$text = preg_replace('{(\S)"}s', "\\1" . $this->_RAQUO, $text);
		$text = preg_replace(
			'{(^|[^' . $this->_RAQUO . '])' . $this->_LAQUO . $this->_RAQUO . '}s',
			"\\1" . $this->_LAQUO . $this->_LAQUO, $text);
		$text = preg_replace(
			'{(^|[^' . $this->_LAQUO . '])' . $this->_RAQUO . $this->_LAQUO . '}s',
			"\\1" . $this->_RAQUO . $this->_RAQUO, $text);
		$i = 0;
		while(($i++ < 10) && preg_match("{" . $this->_LAQUO . "([^" . $this->_RAQUO . "]*?)" . $this->_LAQUO . "}s", $text))
			$text = preg_replace(
				"{" . $this->_LAQUO . "([^" . $this->_RAQUO . "]*?)" . $this->_LAQUO . "(.*?)" . $this->_RAQUO . "}s",
				$this->_LAQUO . "\\1" . $this->_LDQUO . "\\2" . $this->_RDQUO, $text);
		$i = 0;
		while(($i++ < 10) && preg_match("{" . $this->_RAQUO . "([^" . $this->_LAQUO . "]*?)" . $this->_RAQUO . "}s", $text))
			$text = preg_replace(
				"{" . $this->_RAQUO . "([^" . $this->_LAQUO . "]*?)" . $this->_RAQUO . "}s",
				$this->_RDQUO . "\\1" . $this->_RAQUO, $text);

		$text = preg_replace('{(^|\s|'.$this->_TAG.')-(\s)}s', "\\1" . $this->_MDASH . "\\2",$text);
		$text = preg_replace('{ '.$this->_MDASH.'}', "&nbsp;" . $this->_MDASH, $text);
		$text = preg_replace('{(\d)-(\d)}s', "\\1" . $this->_NDASH . "\\2", $text);
		$text = preg_replace('{\.\.\.}s', $this->_HELLIP, $text);

		$text = preg_replace('/\"/', "&quot;", $text);
		$text = preg_replace('{\'}s', "&#146;", $text);

		while(preg_match('{' . $this->_TAG . '\d' . $this->_TAG.'}', $text))
			$text = preg_replace('{' . $this->_TAG.'(\d+)' . $this->_TAG.'}e', "\$Refs[\\1]", $text);

		$text = preg_replace('{' . $this->_LAQUO . '}s', "&laquo;", $text);
		$text = preg_replace('{' . $this->_RAQUO . '}s', "&raquo;", $text);
		$text = preg_replace('{' . $this->_LDQUO . '}s', "&bdquo;", $text);
		$text = preg_replace('{' . $this->_RDQUO . '}s', "&ldquo;", $text);
		$text = preg_replace('{' . $this->_MDASH . '}s', "&#151;", $text);
		$text = preg_replace('{' . $this->_NDASH . '}s', "&#150;", $text);
		$text = preg_replace('{' . $this->_HELLIP . '}s', "&hellip;", $text);

		$text = preg_replace('{\(c\)}i', "&copy;", $text);
		$text = preg_replace('{\(r\)}i', "&reg;", $text);
		$text = preg_replace('{\(tm\)}i', "&trade;", $text);
		$text = preg_replace('{�}s', "&#8470;", $text);

		return $text; 
	}
}
?>