<?php

/**
	!!! Данный файл для работы с БД устарел, будет со временем удалён из системы!!!
*/


class TObject {
 var $_tbl 			= '';
 var $_tbl_key 		= '';
 var $_error 		= '';

 function TObject( $table, $key ) {
	$this->_tbl 		= $table;
	$this->_tbl_key 	= $key;
 }

 function getError() {
  return $this->_error;
 }

 function bind( $hash ) {
  if (!is_array( $hash )) {
   $this->_error = get_class( $this )."::bind failed.";
   return false;
  } else {
   bindHashToObject( $hash, $this );
   return true;
  }
 }

 function load( $oid=null , $strip = true, $fl = false) {
  $k = $this->_tbl_key;
  if ($oid) {
   $this->$k = intval( $oid );
  }
  $oid = $this->$k;
  if ($oid === null) {
   return false;
  }
  $sql = "SELECT * FROM $this->_tbl WHERE $this->_tbl_key=$oid";
  return db_loadObject( $sql, $this, $fl, $strip );
 }

 function check() {
  return NULL;
 }

 function _clone() {
  $_key 			= $this->_tbl_key;
  $newObj 			= $this;
  $newObj->$_key 	= '';
  return $newObj;
 }

 function store( $updateNulls = false ) {
  $msg = $this->check();
  if( $msg ) {
   return get_class( $this )."::store-check failed<br />$msg";
  }
  $k = $this->_tbl_key;

  if( $this->$k ) {
   $ret = db_updateObject( $this->_tbl, $this, $this->_tbl_key, $updateNulls );
   return true;
  } else {
   $ret = db_insertObject( $this->_tbl, $this, $this->_tbl_key );
   return true;
  }
  if( !$ret ) {
   //return get_class( $this )."::store failed <br />" . db_error();
   return false;
  } else {
   return true;
  }
 }

 function delete( $oid=null ) {
  $k = $this->_tbl_key;
  if ($oid) {
   $this->$k = intval( $oid );
  }
  $sql = "DELETE FROM $this->_tbl WHERE $this->_tbl_key = '".$this->$k."'";
  db_exec( $sql );
  if (!db_affected_rows() > 0) {
   return false;
  } else {
   return true;
  }
 }
}

?>
