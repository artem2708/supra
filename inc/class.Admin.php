<?php

class Access extends Module {
	var $default_action = 'create_only';
	function Access($action=null){
		$this->cleanVar($_POST);
		$this->Init($action);
	}
	function Init($action = null){
		global $CONFIG, $server, $lang;
		$lang_file = realpath(RP.'mod/'.$this->module_name.'/lang/'.$this->module_name.'_'.$lang.'.php');
		if($lang_file && file_exists($lang_file)) require_once($lang_file);
		$this->_msg = $_MSG; unset ($_MSG);
		$is_admin = preg_match('~admin\.php~i', $_SERVER["SCRIPT_NAME"]);
		$this->admin = $is_admin;
		if (empty($action)) $action = $this->default_action;
		$this->action = $action;

		//получение переменных для функции
		$tmp = debug_backtrace ();
		while(false!=($t = array_shift($tmp)) && @++$i<3){$var = $t['args'];};  unset($tmp,$t);

		$this->table_prefix = (isset($var[3])) ? $var[3] : $server.$lang;

    	//error_log($_SERVER['QUERY_STRING']."\n\n", 3, "test.txt");#
    	
		if($is_admin && !empty($action) && method_exists($this, 'ADM_'.$action))
		{
		 	$this->perm = $GLOBALS['permissions'];

		 	//print_r(array(&$this, 'ADM_'.$action));
			call_user_func_array(array(&$this, 'ADM_'.$action), $var);
			return true;
		}
		 elseif(!$is_admin && !empty($action) && method_exists($this, 'USR_'.$action)){
			call_user_func_array(array(&$this, 'USR_'.$action), $var);
			return true;
		}
		 elseif($is_admin){
		 	global $main,$tpl;
			$main->include_main_blocks('_empty.html');
			$tpl->prepare();
			$tpl->assign('msg',$main->_msg['action_is_absent']." <b>{$action}</b>");
		}
		 else{
		 	//var_dump($action);
		 	#header('HTTP/1.0 404 Not Found');
			#exit;
		 }

		return false;
	}
	function checkRight(&$el, $act, $user = false){
		if(!isset($el['rights'])) return false;
		if(!in_array($act, array('d','p','e','r'))) return false;
		$r = $el['rights'];
		$USER 	= (@$user['id']) ? $user['id'] : $_SESSION['siteuser']['id'];
		$GROUP 	= (@$user['group_id']) ? $user['group_id'] : $_SESSION['siteuser']['group_id'];
		if($USER == $el['owner_id']){$r >>= 8;}
		 elseif(in_array($el['owner_id'], (array) $GROUP)){$r >>= 4; $r &= 15;}
		 else {$r &= 15;}

		switch ($act) {
		case "d":
			$res = ($r & 8) >> 3; break;
		case "p":
			$res = ($r & 4) >> 2; break;
		case "e":
			$res = ($r & 2) >> 1; break;
		case "r":
			$res = $r & 1; break;
		default:
			$res = 0;
		}
		return $res ? true : false;
	}
	function cleanStr($STRING){
		$from = array("'");
		$to = array('"');
		$STRING = trim (stripslashes (str_replace ($from,$to,$STRING)));
		return $STRING;
	}
	function cleanVar(&$var){
		$from = array("'");
		$to = array('"');
		if(is_scalar($var)){$var = trim (stripslashes (str_replace ($from,$to,$var)));}
		 elseif(is_array($var)){
			foreach($var as $k => $v){$this->cleanVar($var[$k]);}
		}
	}
	function ADM_only_create_object(){
		if($GLOBALS['CONFIG'][$this->module_name.'_default_rights']){
			$this->default_rights = bindec($GLOBALS['CONFIG'][$this->module_name.'_default_rights']);
		}
		return true;
	}
	function USR_only_create_object(){
		return $this->ADM_only_create_object();
	}
	function ADM_properties(){
		global $main, $tpl, $db, $baseurl;
		if (!$this->perm['r']) $main->message_access_denied($this->module_name, $this->action);
	    global $module_id, $request_field, $request_block_id, $title;
	    $main->include_main_blocks('_block_properties.html', 'main');
	    $tpl->prepare();
	    if ($main->show_actions($request_field, '', $this->block_main_module_actions, $this->block_module_actions)) {
			$main->show_linked_pages($module_id, $request_field);
	    }
	    $tpl->assign(array( '_ROOT.title'		=> $title,
	   						'_ROOT.username'	=> $_SESSION['session_login'],
	   						'_ROOT.password'	=> $_SESSION['session_password'],
	   						'_ROOT.name'		=> $this->module_name,
	   						'_ROOT.lang'		=> $lang,
	   						'_ROOT.block_id'	=> $request_block_id,
	   	));
	   	$this->main_title	= $this->_msg["Block_properties"];
	}

	function ADM_prp()
	{
		global $main, $tpl, $baseurl;
		
		
		if (!$this->perm['e']) $main->message_access_denied($this->module_name, $this->action);
		global $request_step, $CONFIG;$osg_f = RP.$CONFIG['osg_data_access'];

		$main->include_main_blocks('_properties.html', $this->tpl_path);
		$tpl->prepare();
		$tpl->assign(Array(
					"MSG_Information"	=> $main->_msg["Information"],
					"MSG_Value"			=> $main->_msg["Value"],
					"MSG_Save"			=> $main->_msg["Save"],
					"MSG_Cancel"		=> $main->_msg["Cancel"],
		));
		if (!$request_step || $request_step == 1) {
			$tpl->assign(array('form_action'	=> $baseurl,
									   'step'			=> 2,
									   'lang'			=> $lang,
									   'name'			=> $this->module_name,
									   'action'			=> 'prp'));
			$prp_html = $main->getModuleProperties($this->module_name, $DEF);

		    $this->main_title = $this->_msg["Controls"];
		} elseif (2 == $request_step) {
		    $main->setModuleProperties($this->module_name, $_POST);
			header('Location: '.$baseurl.'&action=prp&step=1');
		}
	}



	function EmptyReturn($vars=array()){
		GLOBAL $tpl, $main;
		$main->include_main_blocks_2('_empty.html', 'tpl/admin/','main');
		$tpl->prepare(); if($vars) {$tpl->assign($vars);}
		return false;
	}
	function translit($STRING, $path = false){
 		$russ=array(
		   "ё"=>"yo", "Ё"=>"yo", "й"=>"y",  "Й"=>"y",  "ц"=>"z",  "Ц"=>"z",  "у"=>"u", "У"=>"u",
 		   "к"=>"k",  "К"=>"k",	 "е"=>"e",  "Е"=>"e",  "н"=>"n",  "Н"=>"n",  "г"=>"g", "Г"=>"g",
		   "ш"=>"sh", "Ш"=>"sh", "щ"=>"sh", "Щ"=>"sh", "з"=>"z",  "З"=>"z",  "х"=>"h", "Х"=>"h",
		   "ъ"=>"`",  "Ъ"=>"`",	 "ф"=>"f",  "Ф"=>"f",  "ы"=>"i",  "Ы"=>"i",  "в"=>"v", "В"=>"v",
		   "а"=>"a",  "А"=>"a",  "п"=>"p",  "П"=>"p",  "р"=>"r",  "Р"=>"r",	 "о"=>"o", "О"=>"o",
		   "л"=>"l",  "Л"=>"l",  "д"=>"d",  "Д"=>"d",  "ж"=>"zh", "Ж"=>"zh", "э"=>"e", "Э"=>"e",
		   "я"=>"ya", "Я"=>"ya", "ч"=>"ch", "Ч"=>"ch", "с"=>"s",  "С"=>"s",	 "м"=>"m", "М"=>"m",
		   "и"=>"i",  "�?"=>"i",  "т"=>"t",  "Т"=>"t",  "ь"=>"",   "Ь"=>"",	 "б"=>"b", "Б"=>"b",
		   "ю"=>"yu", "Ю"=>"yu", " "=>"_"
   		);
		$text = strtolower (strtr ($STRING, $russ));
	    $text = preg_replace("/[^A-Za-z0-9\._\/\-]/", "", $text);
		return ($path == true) ? $text : str_replace('/','',$text);
	}
	function delFile($mask, $path=''){
		if(empty($mask) || empty($path)){return false;}
		if(is_array($mask)){foreach($mask as $v){Access::delFile($v,$path);}}
		 elseif(false==!strpos($path.$mask,'*') && is_file($path.$mask)){@unlink($path.$mask);}
		 else {
			$files = glob($path.$mask);
			if(is_array($files) && sizeof($files)){
				foreach($files as $v){
					if(is_file($v)){@unlink($v);}
					 elseif(is_dir($v)){Access::delFile('/*',$v);}
				}
			}
		 }
	}
	function checkTemplate($path){
		global $tpl_path;
		if(!file_exists(RP.$tpl_path.$path)){return false;}
		return true;
	}
	function arrReplace(&$var,$key,$dop){
		if(is_array($dop)){foreach($dop as $k=>$v){
			$t = $var[$k];
			$var[$k] = sprintf($v[0],$v[1],$t);
		}}
	}
}

class DB_man {
	var $table_prefix;
	var $TableInFo = array();
	var $DB;
#vars for db querys
	var $PAGE = 0;
	var $LIMIT = 10;
#    var $DEFAULT_ORDER='t.id:1'; // param1:1&param2:-1
	var $FILTER_SET = array (
		'active'	=> " and t.`active` = '%d' "
#		'id'		=> " and t.id = %d ",
	);
#end vars
	function DB_man($table_prefix=null){
		global $server, $lang,$db;
		if($table_prefix) $this->table_prefix = $table_prefix;
		 else $this->table_prefix = $server . $lang;
		if($db) $this->DB = $db;
		$this->_init();
	}
	function _init(){
		if(!$this->DB){return false;}
		$db = &$this->DB;
		$db->Halt_On_Error = 'no';
		$q = str_replace('_','\\_',sprintf("show tables like '%s_%%'",$this->table_prefix));
		$res = $db->query($q); if(!$res){$this->error = $db->Error; return false;}
		while($db->next_record()){$this->TableInFo[strtoupper($db->Record[0])] = false;}
	}
	function getTableInfo($tableName=null){
		$db = &$this->DB; $TI = &$this->TableInFo;$tp = &$this->table_prefix;
		if ($tableName != 'core_users') {
		  if(is_string($tableName) && false===strpos($tableName,$tp)){
		    $tableName = $tp.'_'.$tableName;
		  }
		} else {
		  if(is_string($tableName) && false===strpos($tableName,$tp)){
		    $tableName = $tableName;
		  }
		}
		#if(is_string($tableName) && false===strpos($tableName,$tp)){$tableName = $tp.'_'.$tableName;}
		if ($tableName != 'core_users')
		  if(!isset($TI[strtoupper($tableName)])){return false;}
    #error_log("{$tableName} \n",3,'error_query.txt');
		if($TI[strtoupper($tableName)]){return $TI[strtoupper($tableName)];}
		$q = sprintf("SHOW COLUMNS FROM `%s`",$tableName);
		
		$res = $db->query($q); if(!$res){$this->error = $db->Error; return false;}
		while($db->next_record(1)){$ret[($db->Record['Field'])] = $db->Record;}
		if(!$ret) $ret = true;
		$TI[strtoupper($tableName)] = $ret;
		return $ret;
	}
	function cleanData($tableName=null){
		if(!$tableName){return false;}
		$db = &$this->DB; $TI = &$this->TableInFo; $tp = &$this->table_prefix;
		if(is_string($tableName) && false===strpos($tableName,$tp)){$tableName = $tp.'_'.$tableName;}
		if(!isset($TI[strtoupper($tableName)])){$this->error = 'table not exists'; return false;}
		$q = sprintf("truncate table `%s`",$tableName);
		$res = $db->query($q);
		//if(!$res){$this->error = $db->Error; return false;}
		return true;
	}
	function InsertData($tableName=null, $Fields = array(), $KEY = false){
		$arr = $this->CheckData($Fields, &$tableName, TRUE);
		$db = $this->DB;$tp = &$this->table_prefix;
		if(is_string($tableName) && false===strpos($tableName,$tp)){$tableName = $tp.'_'.$tableName;}
		if(!is_array($arr)){return false;}
		$sql = sprintf('INSERT INTO %s SET %s %s',$tableName,implode(' , ', $arr),$w);
		error_log("{$sql}\n",3,'test_query.txt');
		if(!$db->query($sql)){error_log("{$sql}: {$db->Error}\n",3,'error_query.txt');$this->error = $db->Error;}
		return (false!==($ret = $db->lid())) ? $ret : true;
	}
	function UpdateData($tableName=null, $Fields = array(), $KEY = false){
	  $arr = $this->CheckData($Fields, &$tableName, FALSE);
		$db = $this->DB;$tp = &$this->table_prefix;
		if ($tableName != 'core_users') {
		  if(is_string($tableName) && false===strpos($tableName,$tp)){
		    $tableName = $tp.'_'.$tableName;
		  }
		} else {
		  if(is_string($tableName) && false===strpos($tableName,$tp)){
		    $tableName = $tableName;
		  }
		}
		if(!is_array($arr)){return false;}
		if($KEY){
		  foreach((array) $KEY as $v){
		    $_w[$v] = $arr[$v];
		  }
		}
		$filter = implode(' AND ', $_w);
		if ($filter) $filter = ' AND '.$filter; 
		#error_log("{$tableName} \n",3,'error_query.txt');
		$sql = sprintf('UPDATE %s SET %s  WHERE 1 %s',$tableName,implode(' , ', $arr),$filter);
		#error_log("{$sql} \n",3,'error_query.txt');
		if(!$db->query($sql)){error_log("{$sql}: {$db->Error}\n",3,'error_query.txt');$this->error = $db->Error;}
		#if(!$db->affected_rows()){$this->InsertData($tableName, $Fields, $KEY);}
		return true;
	}
	function DeleteData($tableName=null, $Fields = array(), $KEY = false){
		$arr = $this->CheckData($Fields, &$tableName, FALSE);
		$db = $this->DB;$tp = &$this->table_prefix;
		if(is_string($tableName) && false===strpos($tableName,$tp)){$tableName = $tp.'_'.$tableName;}
		if(!is_array($arr)){return false;}

		if(isset($KEY) && is_scalar($KEY) && ($arr[$KEY] || $KEY{0}=='&')){
			if($KEY{0}=='&') $clause = substr($KEY,1);
			 else $clause = $arr[$KEY];
			$w = " WHERE {$clause}";
		}
		 elseif(is_array($KEY) && sizeof($KEY)==sizeof(array_intersect($KEY, array_keys($arr)))){
		 	foreach($KEY as $v){$_w[$v] = $arr[$v];}
			$w = " WHERE ".implode(' AND ', $_w);
		}
		 else {$this->error = 'key for delete not defined';return false;}
		$sql = sprintf('DELETE FROM `%s` %s', $tableName, $w);
		if(!$db->query($sql)){$this->error = $db->error;return false;}
		return true;
	}
	function Query($str, $KEY = false){
		$db = $this->DB;
		$res = $db->query($str); if(!$res){$this->error = $db->Error; return false;}
		if($db->nf()){
			while($db->next_record(1)){
				if($KEY && $db->Record[$KEY]){
					$ret[($db->Record[$KEY])] = $db->Record;
				}
				 else {
					$ret[] = $db->Record;
				}
			}
			return $ret;
		}
		return true;
	}
	function CheckData($arr, $table = null, $set_default= true){
		$ARR = $this->getTableInfo($table);
		if(!$ARR){return false;}
		foreach($ARR as $k => $v){
			if(!array_key_exists($k,$arr) && $set_default){$arr[$k] = ($v['Default']=='CURRENT_TIMESTAMP')? NULL : $v['Default'];}
			if(array_key_exists($k,$arr)){
				$RET[$k] = "`{$k}`=";
				if(!$arr[$k] && strtoupper($v['Null'])=='YES'){
					$RET[$k] .= "NULL ";
				}
				 elseif(false!==strpos($v['Type'],'int')){
					$RET[$k] .= intval($arr[$k]).' ';
				}
				 else {
				 	if(preg_match('~\((\d+)\)~',$v['Type'],$m)){$arr[$k] = substr($arr[$k],0,$m[1]);}
					$RET[$k] .= "'".my_sql::escape($arr[$k])."' ";
				}
			}
		}
		return $RET;
	}

	function getData($TBL=null, $FLTR='', $ARR='*',$KEY = NULL, $JOIN = null){
	    //prepared data
		if(!$TBL){return false;}
		$db = &$this->DB; $tp = &$this->table_prefix;
		if(is_string($TBL) && false===strpos($TBL,$tp)){$TBL = $tp.'_'.$TBL;}
		if(is_array($JOIN)){
			foreach($JOIN as $k => $v){
				if(is_string($v['TABLE']) && false===strpos($v['TABLE'],$tp)){$JOIN[$k]['TABLE'] = $tp.'_'.$v['TABLE'];}
			}
			unset($k,$v);
		}
	    //select set
		if(is_array($ARR)){
			foreach($ARR as $_v){
			  if(false === strpos($_v,'.')){
			    $VAL[] = 't.'.$_v;
			  }else{
			    $VAL[] = $_v; 
			    //error_log("{$_v}\n", 3, "error_query.txt");
			  }
			}
			$STR = implode(' , ', $VAL); unset($VAL, $_v, $ARR);
		}
		  else {$STR = empty($ARR) ? '*' : $ARR; unset($ARR);}
	    //join,group set
		if(is_array($JOIN)){
			$QUERY_JOIN = '';
			$i=0;
			foreach($JOIN as $k => $v){
				if($v['GROUP']){$GROUP[$i] = $v['GROUP'];$i++;}
				if($v['TABLE']){
					$QUERY_JOIN .= " {$v['JOIN_TYPE']} JOIN {$v['TABLE']} {$k}";
					if($v['JOIN_UNION']) $QUERY_JOIN .= " ON {$v['JOIN_UNION']} ";
				}
			}
			if(is_array($GROUP)){$QUERY_GROUP = 'GROUP BY '.implode(' , ', array_unique($GROUP));}
	      	unset($k,$v,$JOIN,$GROUP);
		}
	    //where set
		if(isset($FLTR) && sizeof($FLTR)>0){
	      $QUERY_FILTR = "where 1";
	      foreach($FLTR as $k=>$v){
	      	if($k=='ORDER'){ $this->DEFAULT_ORDER = $v;}
	      	 elseif($k=='LIMIT'){ $this->LIMIT = (int) $v;}
	         elseif(isset($this->FILTER_SET[$k])){$QUERY_FILTR .= sprintf($this->FILTER_SET[$k], $v);}
			 elseif($k{0}==='&'){$QUERY_FILTR .= sprintf(' %s ', $v);}
	         elseif($v===NULL){$QUERY_FILTR .= sprintf(' AND %s is null ', $k);}
	         elseif(strval($v)===strval((int)$v)){$QUERY_FILTR .= sprintf(' AND %s=%d ', $k, $v);}
	         else {$QUERY_FILTR .= sprintf(" AND %s='%s' ", $k, my_sql::escape($v));}
	      }
	      unset($k,$v);
		} elseif (!empty($FLTR) && isset($this->FILTER_SET['FULL_TEXT'])){
	      $QUERY_FILTR = " where ".sprintf($this->FILTER_SET['FULL_TEXT'], $FLTR);
	    }
	     else {
	       $QUERY_FILTR = "";
	    }
	    //order set
	    if(!$QUERY_ORDER && !empty($this->DEFAULT_ORDER)){
	    	$sort = explode('&', $this->DEFAULT_ORDER);
	    	foreach($sort as $v){
	    		list($k,$o) = explode(':', $v); $s = ($o && $o >= 0 ) ? ($o ? 'asc' : '') : 'desc';
	    		$ord[] = $k.' '.$s;
	    	}
	      	$QUERY_ORDER = " ORDER BY ". implode(' , ',$ord);
	      	unset($sort,$ord,$k,$o,$s,$v);
	    }
	    //limit set
	    $this->LIMIT = ((int) $this->LIMIT > 0) ? (int) $this->LIMIT : ((int)PHP_INT_MAX ? PHP_INT_MAX : sprintf('%u',-1));
    	$QUERY_LIMIT = ' LIMIT '.((int) $this->PAGE * $this->LIMIT).' , '.$this->LIMIT;
		$db->query ("select count(*) from {$TBL} as t $QUERY_JOIN $QUERY_FILTR $QUERY_GROUP");
		if($db->nf() && $db->next_record()){$this->ALL = $db->Record[0];}

		$SQL = "select {$STR} FROM {$TBL} as t ".
				" $QUERY_JOIN ".
          		" $QUERY_FILTR $QUERY_GROUP $QUERY_ORDER $QUERY_LIMIT";
          		#error_log("{$SQL}\n",3,'error_query.txt');
          		#print $SQL . '<br><br>';
    	$res = $db->query ($SQL);
#var_dump($SQL);
    	if(!$res){$this->error = sprintf('MySQL error: %s in QUERY (%s)',$db->Error,$SQL); return false;}
		 else {$this->error = null;}

		while($db->next_record(1)){
    	 	if(!empty($KEY)){
			  if(is_array($KEY)){
				foreach($KEY as $k){$K[$k] = $db->f($k);}
				$ITEMS[(implode(':',$K))] = $db->Record;
			  }
			   else {
				$ITEMS[$db->f($KEY)] = $db->Record;
			  }
			}
			 else{ $ITEMS[] = $db->Record;}
		}
    	@$db->free();
    	return (array) $ITEMS;
	}
}
?>