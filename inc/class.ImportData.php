<?php

/**
 * Импорт информации
 */

class ImportData {

	var $aFileds			= NULL;
	var $aModule			= NULL;
	var	$aClearTable		= NULL;
	var $aCurrentTable		= NULL;
	var $aSQLQueres			= array();
	var $aDescriptorDB		= NULL;

	function ImportData($fields = NULL, $module = NULL, $clear = NULL) {
			$this->aFields		= $fields;
			$this->aModule		= $module;
			$this->aClearTable	= $clear;
	}

	function doImportData() {
		if (is_object($this->aFields)) {
			$fields	= $this->aFields;
			$module	= $this->aModule;
			while ($fields->doInputParse()) {
                $fields->outFields = $this->ansii2utf($fields->outFields);                
                
                $this->makeSQLQueries($fields->outFields, $module);

				$this->goSQLQueries($fields->outFields, $module);
			};
		}
		return FALSE;
	}

	function makeSQLQueries($arr = NULL, $module = NULL) {
		GLOBAL $server, $lang;

		if (is_array($arr) && sizeof($arr) > 0) {
			@array_walk($arr, create_function('&$v, $k','if(isset($v["fields"][""])){unset($v["fields"][""]);}'));
			$this->aSQLQueres		= NULL;
			$this->aCurrentTable	= $arr[0]['table'];
			$this->isTableCDBTRee	= $arr[0]['cdbtree'];

			for ($i = 0, $j = sizeof($arr); $i < $j; $i++) {
				$aFieldArr = $this->getColSRows($arr[$i]);
                if (isset($aFieldArr[1]) && is_array($aFieldArr[1])) unset($aFieldArr[1]['']);
                if ($arr[$i]['cdbtree']) {
					switch ($aFieldArr[2]) {
						case 'replace':
						case 'insert':
							if ($arr[$i]['updateable'] && !$this->aClearTable) {
								$this->aSQLQueres[]	= array('update', $aFieldArr[1][$aFieldArr[0][0]], $aFieldArr[1]);
							} else {
								$this->aSQLQueres[]	= array('insert', $aFieldArr[1][$aFieldArr[0][1]], $aFieldArr[1]);
							}
							break;

						case 'delete':
							$this->aSQLQueres[]	= array('delete', $aFieldArr[1][$aFieldArr[0][0]]);
							break;
					}
				} else {
					$this->addQuotes($aFieldArr[0]);
					switch ($aFieldArr[2]) {
						case 'replace':
							if (1 || $arr[$i]['updateable']) {
								$this->aSQLQueres[] = 'REPLACE INTO '.$server.$lang.'_'.$arr[$i]['table'].' ('.$aFieldArr[0].') VALUES ('.$aFieldArr[1].')';
							} else {
								$this->aSQLQueres[] = 'INSERT INTO '.$server.$lang.'_'.$arr[$i]['table'].' ('.$aFieldArr[0].') VALUES ('.$aFieldArr[1].')';
							}
							break;
						case 'delete':
							$this->aSQLQueres[] = 'DELETE FROM '.$server.$lang.'_'.$arr[$i]['table']
									.' WHERE '.$arr[$i]['key_of_table'][0].' = '.$arr[$i]['fields'][$arr[$i]['key_of_table'][0]];
							break;
					}
				}
			}

			return TRUE;
		}
		return FALSE;
	}

	function getColsRows($arr = NULL) {
		if (is_array($arr) && sizeof($arr) > 0) {
			$db	= $this->aDescriptorDB;
			$val = array_filter($arr['fields'], create_function('$a','return !empty($a);'));
			$val = array_map('mysql_real_escape_string',$val);
			list($_k, $_v) = each($arr['fields']);
			if (!in_array($_k, $arr['key_of_table'])) {
			   #var_dump($_k, $_v, $arr['key_of_table']);
			   #$type_query = 'delete';
			   $type_query = 'insert';
			   #$type_query = 'replace';
			} else {
			   $type_query = 'replace';
			}
			if ($arr['cdbtree']) {
			  $kk		= $arr['key_of_table'];
			  $vv		= $val;
			  return array($kk, $vv, $type_query);
			}
			 else {
			  $kk = array_keys($val); $vv = array_values($val);
			}

			return array(0 => implode(', ',$kk), 1 => '"'.implode('", "',$vv).'"', 2 => $type_query);
		}
		return FALSE;
	}

	function setDescriptorDB($db = NULL) {
		if ($db) {
			$this->aDescriptorDB = $db;
			return TRUE;
		}
		return FALSE;
	}

	function goSQLQueries($arr = NULL, $module = NULL) {
		GLOBAL $server, $lang;
		if (is_array($this->aSQLQueres) && sizeof($this->aSQLQueres) > 0) {
			$db			= $this->aDescriptorDB;
			$aCDBTree	= new NsDbTree($server.$lang.'_'.$arr[0]['table'], $arr[0]['key_of_table'][0], $module->CDBT_fields);

			if ($this->aClearTable && $this->aCurrentTable != $this->aLastTrancateTable) {
				if ($this->isTableCDBTRee) {
					$aCDBTree->clear(array('title'	=> 'Корень каталога', 'description' => 'Описание корня каталога'));
				} else {
					$db->query('TRUNCATE TABLE '.$server.$lang.'_'.$this->aCurrentTable);
				}
				$this->aLastTrancateTable	= $this->aCurrentTable;
			}
			for ($i = 0, $j = sizeof($this->aSQLQueres); $i < $j; $i++) {
				if (is_array($this->aSQLQueres[$i])) {
					switch($this->aSQLQueres[$i][0]) {
						case 'insert':
						case 'replace':
							$aCDBTree->insert($this->aSQLQueres[$i][1], $this->aSQLQueres[$i][2]);
							break;

						case 'update':
							$aCDBTree->update($this->aSQLQueres[$i][1], $this->aSQLQueres[$i][2]);
							if(mysql_affected_rows()==0) $aCDBTree->insert($this->aSQLQueres[$i][1], $this->aSQLQueres[$i][2]);
							break;

						case 'delete':
							$aCDBTree->delete($this->aSQLQueres[$i][1]);
							break;
					}
				} else {
					$db->query($this->aSQLQueres[$i]);
				}
			}
			$this->aSQLQueres	= NULL;
		}
	}
    function ansii2utf($var) {
      if (is_array($var)) {
          foreach($var as $_k => $_v) {
              $ret[$_k] = $this->ansii2utf($_v);
          }
          return $ret;
      } elseif(is_string($var)) {
        $ret = iconv('WINDOWS-1251', 'UTF-8',  $var);
        return $ret;
      }
      return $var;
    }
	function addQuotes(&$str){
		$str = preg_replace('~(\w+)~','`\\1`',$str);
	}
}

?>