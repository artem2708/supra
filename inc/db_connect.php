<?php

/**
	!!! ������ ���� ��� ������� � �� �������, ����� �� �������� ����� �� �������!!!
*/

function db_connect($host='localhost', $dbname, $user='root', $passwd='', $port='3306', $persist=false) {
	function_exists( 'mysql_connect' )
		or  die( 'FATAL ERROR: MySQL support not avaiable.  Please check your configuration.' );

	if ($persist) {
        $link = mysql_pconnect($host, $user, $passwd);
        if (!$link) die('FATAL ERROR: Connection to database server failed');
	} else {
        $link = mysql_connect($host, $user, $passwd);
        if (!$link) die( 'FATAL ERROR: Connection to database server failed' );
	}

	if ($dbname) {
		mysql_select_db( $dbname )
			or die( "FATAL ERROR: Database not found ($dbname)" );
	} else {
		die( "FATAL ERROR: Database name not supplied<br />(connection to database server succesful)" );
	}
}

function db_error() {
	return mysql_error();
}

function db_errno() {
	return mysql_errno();
}

function db_insert_id() {
	return mysql_insert_id();
}

function db_affected_rows() {
	return mysql_affected_rows();
}

function db_exec( $sql ) {
	$cur = mysql_query( $sql );
	if( !$cur || db_errno()) {
		return false;
	}
	return $cur;
}

function db_free_result( $cur ) {
	if(is_resource($cur)) {
		mysql_free_result( $cur );
	}
}

function db_num_rows( $qid ) {
	return mysql_num_rows( $qid );
}

function db_num_fields( $qid ) {
	return mysql_num_fields( $qid );
}

function db_fetch_row( $cur ) {
	return mysql_fetch_row( $cur );
}

function db_fetch_assoc( $cur , $type = null) {
	if ($type) return mysql_fetch_assoc( $cur , $type);
	else return mysql_fetch_assoc( $cur );
}

function db_fetch_array( $cur  ) {
	return @mysql_fetch_array( $cur );
}

function db_fetch_object( $cur  ) {
	return mysql_fetch_object( $cur );
}

function db_escape( $str ) {
	return (!get_magic_quotes_gpc()) ? mysql_real_escape_string($str) : $str;
}

function db_version() {

	if( ($cur = mysql_query( "SELECT VERSION()" )) ) {
		$row =  mysql_fetch_row( $cur );
		mysql_free_result( $cur );
		return $row[0];
	} else {
		return 0;
	}
}

function db_unix2dateTime( $time ) {
	return $time > 0 ? date("Y-m-d H:i:s", $time) : null;
}

function db_dateTime2unix( $time ) {
	if ($time == '0000-00-00 00:00:00') {
		return -1;
	}
	if( ! preg_match( "/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})(.?)$/", $time, $a ) ) {
		return -1;
	} else {
		return mktime( $a[4], $a[5], $a[6], $a[2], $a[3], $a[1] );
	}
}

function db_tableNames() {
	global $CONFIG, $db1;
	$connect = db_connect($CONFIG['db_host'], 
		   				  $CONFIG['db_database'], 
		   				  $CONFIG['db_user'], 
		   				  $CONFIG['db_password'], 
		   				  $CONFIG['db_port'], 
		   				  FALSE);
	$sql = "show tables";
	$i = 0;
	$res = @mysql_query($sql);
	while ($info = @mysql_fetch_row($res)) {
		$return[$i]["table_name"]      = $info[0];
		$return[$i]["tablespace_name"] = $this->Database;
		$return[$i]["database"]        = $this->Database;
		$i++;
	}
	@mysql_free_result($h);
	return $return;
}

function db_loadObject( $sql, &$object, $bindAll=false , $strip = true) {
 if ($object != null) {
  $hash = array();
  if( !db_loadHash( $sql, $hash ) ) {
   return false;
  }
  bindHashToObject( $hash, $object, null, $strip, $bindAll );
  return true;
 } else {
  $cur = db_exec( $sql );
  $cur or exit( db_error() );
  if ($object = db_fetch_object( $cur )) {
   db_free_result( $cur );
   return true;
  } else {
   $object = null;
   return false;
  }
 }
}

function db_loadHash( $sql, &$hash ) {
 $cur = db_exec( $sql );
 $cur or exit( db_error() );
 $hash = db_fetch_assoc( $cur );
 db_free_result( $cur );
 if ($hash == false) {
  return false;
 } else {
  return true;
 }
}

function bindHashToObject( $hash, &$obj, $prefix=NULL, $checkSlashes=true, $bindAll=false ) {
 is_array( $hash ) or die( "bindHashToObject : hash expected" );
 is_object( $obj ) or die( "bindHashToObject : object expected" );

 if ($bindAll) {
  foreach ($hash as $k => $v) {
   $obj->$k = ($checkSlashes && get_magic_quotes_gpc()) ? stripslashes( $hash[$k] ) : $hash[$k];
  }
 } else if ($prefix) {
  foreach (get_object_vars($obj) as $k => $v) {
   if (isset($hash[$prefix . $k ])) {
    $obj->$k = ($checkSlashes && get_magic_quotes_gpc()) ? stripslashes( $hash[$k] ) : $hash[$k];
   }
  }
 } else {
  foreach (get_object_vars($obj) as $k => $v) {
   if (isset($hash[$k])) {
    $obj->$k = ($checkSlashes && get_magic_quotes_gpc()) ? stripslashes( $hash[$k] ) : $hash[$k];
   }
  }
 }
 //echo "obj="; print_r($obj); exit;
}

function db_loadResult( $sql ) {
 $cur = db_exec( $sql );
 $cur or exit( db_error() );
 $ret = null;
 if ($row = db_fetch_row( $cur )) {
  $ret = $row[0];
 }
 db_free_result( $cur );
 return $ret;
}

function db_loadResult2($sql) {
	$cur = db_exec($sql);	
	if ($cur) {
	    $ret = db_fetch_array($cur);
	    db_free_result($cur);
	}	
	return $ret;
}

function db_loadHashList( $sql, $index='' ) {
 $cur = db_exec( $sql );
 $cur or exit( db_error() );
 $hashlist = array();
 while ($hash = db_fetch_array( $cur )) {
  $hashlist[$hash[$index ? $index : 0]] = $index ? $hash : $hash[1];
 }
 db_free_result( $cur );
 return $hashlist;
}

function db_loadList( $sql, $maxrows=NULL ) {
 if (!($cur = db_exec( $sql ))) {;
  echo db_error();
  return false;
 }
 $list = array();
 $cnt = 0;
 while ($hash = db_fetch_assoc( $cur )) {
  $list[] = $hash;
  if( $maxrows && $maxrows == $cnt++ ) {
   break;
  }
 }
 db_free_result( $cur );
 return $list;
}

function db_loadList2( $sql, $maxrows=NULL ) {
	if (!($cur = db_exec( $sql ))) {;
		echo db_error();
		return false;
	}
	$list = array();
	$cnt = 0;
	while ($hash = db_fetch_row( $cur )) {
		$list[] = $hash[0];
		if( $maxrows && $maxrows == $cnt++ ) {
			break;
		}
	}
	db_free_result( $cur );
	return $list;
}

function db_loadColumn( $sql, $maxrows=NULL ) {
 if (!($cur = db_exec( $sql ))) {;
  echo db_error();
  return false;
 }
 $list = array();
 $cnt = 0;
 while ($row = db_fetch_row( $cur )) {
  $list[] = $row[0];
  if( $maxrows && $maxrows == $cnt++ ) {
   break;
  }
 }
 db_free_result( $cur );
 return $list;
}

function db_loadObjectList( $sql, $object, $maxrows = NULL ) {
 $cur = db_exec( $sql );
 if (!$cur) {
  die( "db_loadObjectList : " . db_error() );
 }
 $list = array();
 $cnt = 0;
 while ($row = db_fetch_array( $cur )) {
  $object->load( $row[0] );
  $list[] = $object;
  if( $maxrows && $maxrows == $cnt++ ) {
   break;
  }
 }
 db_free_result( $cur );
 return $list;
}

function db_insertArray( $table, &$hash, $verbose=false ) {
 $fmtsql = "insert into $table ( %s ) values( %s ) ";
 foreach ($hash as $k => $v) {
  if (is_array($v) or is_object($v) or $v == NULL) {
   continue;
  }
  $fields[] = $k;
  $values[] = "'" . ( $v ) . "'";
 }
 $sql = sprintf( $fmtsql, implode( ",", $fields ) ,  implode( ",", $values ) );

 ($verbose) && print "$sql<br />\n";

 if (!db_exec( $sql )) {
  return false;
 }
 $id = db_insert_id();
 return true;
}

function db_updateArray( $table, &$hash, $keyName, $verbose=false ) {
 $fmtsql = "UPDATE $table SET %s WHERE %s";
 foreach ($hash as $k => $v) {
  if( is_array($v) or is_object($v) or $k[0] == '_' ) // internal or NA field
   continue;

  if( $k == $keyName ) { // PK not to be updated
   $where = "$keyName='" . ( $v ) . "'";
   continue;
  }
  if ($v == '') {
   $val = 'NULL';
  } else {
   $val = "'" . ( $v ) . "'";
  }
  $tmp[] = "$k=$val";
 }
 $sql = sprintf( $fmtsql, implode( ",", $tmp ) , $where );
 ($verbose) && print "$sql<br />\n";
 $ret = db_exec( $sql );
 return $ret;
}

function db_delete( $table, $keyName, $keyValue ) {
 $keyName = ( $keyName );
 $keyValue = ( $keyValue );
 $ret = db_exec( "DELETE FROM $table WHERE $keyName='$keyValue'" );
 return $ret;
}

function db_insertObject( $table, &$object, $keyName = NULL, $verbose=false ) {
 $fmtsql = "INSERT INTO $table ( %s ) VALUES ( %s ) ";
 foreach (get_object_vars( $object ) as $k => $v) {
  if (is_array($v) or is_object($v) or $v == NULL) {
   continue;
  }
  if ($k[0] == '_') { // internal field
   continue;
  }
  $fields[] = $k;
  $values[] = "'" . ( $v ) . "'";
 }
 $sql = sprintf( $fmtsql, implode( ",", $fields ) ,  implode( ",", $values ) );
 ($verbose) && print "$sql<br />\n";
 if (!db_exec( $sql )) {
  return false;
 }
 $id = db_insert_id();
 ($verbose) && print "id=[$id]<br />\n";
 if ($keyName && $id)
  $object->$keyName = $id;
 return true;
}

function db_updateObject( $table, &$object, $keyName, $updateNulls=true ) {
 $fmtsql = "UPDATE $table SET %s WHERE %s";
 foreach (get_object_vars( $object ) as $k => $v) {
  if( is_array($v) or is_object($v) or $k[0] == '_' ) { // internal or NA field
   continue;
  }
  if( $k == $keyName ) { // PK not to be updated
   $where = "$keyName='" . ( $v ) . "'";
   continue;
  }
  if ($v === NULL && !$updateNulls) {
   continue;
  }
  if( is_string($v) && $v == '' ) {
   $val = "''";
  } else {
   $val = (!is_int($v)) ? "'" . ( $v ) . "'" : $v;
  }
  $tmp[] = "$k=$val";
 }
 $sql = sprintf( $fmtsql, implode( ",", $tmp ) , $where );

 return db_exec( $sql );
}

function db_dateConvert( $src, &$dest, $srcFmt ) {
 $result = strtotime( $src );
 $dest = $result;
 return ( $result != 0 );
}

function db_datetime( $timestamp = NULL ) {
 if (!$timestamp) {
  return NULL;
 }
 if (is_object($timestamp)) {
  return $timestamp->toString( '%Y-%m-%d %H:%M:%S');
 } else {
  return strftime( '%Y-%m-%d %H:%M:%S', $timestamp );
 }
}
?>