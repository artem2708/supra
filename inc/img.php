<?php
if(!defined('ABOCMS_VERSION')){die();}

function img_resize($VARS){
   if($TYPE=getimagesize($VARS['old_path'])){
     if($TYPE['mime']=='image/gif' && function_exists("imagecreatefromgif"))
        {$func1="imagecreatefromgif";$func2="imagegif";$ext="gif";}
     elseif(($TYPE['mime']=='image/jpeg' || $TYPE[mime]=='image/pjpeg') && function_exists("imagecreatefromjpeg"))
        {$func1="imagecreatefromjpeg";$func2="imagejpeg";$ext="jpg";}
     elseif($TYPE['mime']=='image/png' && function_exists("imagecreatefrompng"))
        {$func1="imagecreatefrompng";$func2="imagepng";$ext="png";}
     else {
        return false;
     }
     $begin_width=$TYPE[0];
     $begin_height=$TYPE[1];

     if($VARS['bg_color']){
       while(strlen($VARS['bg_color'])<6){$VARS['bg_color'].='0';}
       preg_match("/(?:#)?((?:\d|\w){2})+((?:\d|\w){2})+((?:\d|\w){1,2})+/i",$VARS['bg_color'],$tmp_color);
       $TMP['bg_color']['R']=hexdec('0x'.$tmp_color[1]);
       $TMP['bg_color']['G']=hexdec('0x'.$tmp_color[2]);
       $TMP['bg_color']['B']=hexdec('0x'.$tmp_color[3]);
     }

     $P['x']=0;$P['y']=0; $P['w']=$begin_width; 	$P['h']=$begin_height;
     $B['x']=0;$B['y']=0; $B['w']=$VARS['width'];	$B['h']=$VARS['height'];
     
     if($VARS['width']>0){$rez_width=$begin_width/$VARS['width'];}
     if($VARS['height']>0){$rez_height=$begin_height/$VARS['height'];}
     
     if($VARS['resize'] == 'fixed'){
       if($rez_width && $rez_height){$koef=($rez_width<$rez_height)?$rez_width:$rez_height;}
        elseif($rez_width && !$rez_height){$koef=$rez_width;}
        elseif(!$rez_width && $rez_height){$koef=$rez_height;}
        
        $P['x']=($begin_width - $koef*$B['w'])/2;
        $P['y']=($begin_height- $koef*$B['h'])/2;
        $P['w']=$koef*$B['w'];//$begin_width/$koef;
        $P['h']=$koef*$B['h'];//$begin_height/$koef;
        
        $I['w'] = $VARS['width'];
        $I['h'] = $VARS['height'];
     }
      else if($VARS['resize'] == 'slice'){
        if ($VARS['slice']['n_left']) 	{$B['x'] =  $VARS['slice']['n_left'];}
        if ($VARS['slice']['n_top']) 	{$B['y'] =  $VARS['slice']['n_top'];}
        
        if ($VARS['slice']['left']) 	{$P['x'] =  $VARS['slice']['left'];}
        if ($VARS['slice']['top']) 		{$P['y'] =  $VARS['slice']['top'];}
        if ($VARS['slice']['width']) 	{$P['w'] =  $VARS['slice']['width'];}
        if ($VARS['slice']['height']) 	{$P['h'] =  $VARS['slice']['height'];}
        
        $koef = 1;
        $I['w'] = $VARS['width'];
        $I['h'] = $VARS['height'];
        
     }
      else {
       if($rez_width && $rez_height){$koef=($rez_width<$rez_height)?$rez_height:$rez_width;}
        elseif($rez_width && !$rez_height){$koef=$rez_width;}
        elseif(!$rez_width && $rez_height){$koef=$rez_height;}
        
        $I['w'] = ($koef>0) ? (int) $begin_width/$koef : $begin_width;
        $I['h'] = ($koef>0) ? (int) $begin_height/$koef: $begin_height;
        
         $B['x'] = $B['y'] = $P['x'] = $P['y'] = 0;
         $B['w'] = $I['w']; $P['w'] = $begin_width;
         $B['h'] = $I['h']; $P['h'] = $begin_height;
     }
   }
    else {
     return false;
   }

   if(function_exists("imagecreatetruecolor") && $ext=="jpg"){
       $ImageCreate="imagecreatetruecolor";
       $ImageResize="imagecopyresampled";
   }
     else{
       $ImageCreate="imagecreate";
       $ImageResize="imagecopyresized";
   }
   if(!function_exists($func2)){$func2="imagejpeg";$ext="jpg";}
   $im=@$func1($VARS['old_path']);
   if(!$im){return false;}
   $NewName = ($VARS['extExists']) ? $VARS['new_path'] : $VARS['new_path'].'.'.$ext;
   if ($koef && $koef!=1){
     $im1=$ImageCreate($I['w'], $I['h']);
     $bg_color=($VARS['bg_color'])?
              ImageColorAllocate ($im1,$TMP['bg_color']['R'],$TMP['bg_color']['G'],$TMP['bg_color']['B']):
              ImageColorAllocate ($im1, 255, 255, 255);
     @imagefill($im1,1,1,$bg_color);
     
     $ImageResize($im1, $im, (int) $B['x'], (int) $B['y'], (int) $P['x'], (int) $P['y'], (int) $B['w'], (int) $B['h'], (int) $P['w'], (int) $P['h']);
     if ($VARS['filtr']){
       $TYPE_FLTR=getimagesize($VARS['filtr']);$im2=@imagecreatefromgif($VARS['filtr']);
       if ($TYPE_FLTR && $im2){
         @imagecopy ($im1, $im2, ($I['w']-$TYPE_FLTR[0])/2, ($I['h']-$TYPE_FLTR[1])/2, 0, 0, $TYPE_FLTR[0], $TYPE_FLTR[1]);
       }
     }
     $ret=$func2($im1,$NewName);
     @chmod($NewName, 0666);
   }
    else {
     $ret=@copy($VARS['old_path'],$NewName);
     @chmod($NewName, 0666);
   }
   if($ret){return $ext;}
}
?>