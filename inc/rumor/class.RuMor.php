<?php

/**
 *     class.RuMor.php, adapted by Armex
 *
 *           RuMor PHP
 *
 * morphological analysis, version 2.0
 * (c) Sergej Tarasov, 2002-2006
 *
 * Homepage: http://risearch.org/
 * email: risearch@risearch.org
 */


class RuMor {

	var $tree = "";
	var $profiles = array();
	var $profiles_length = array();
	var $profiles_full = array();
	var $profiles_morf = array();
	var $profiles_lll = array();
	var $profiles_code;

	var $alf = 20;
	var $talf;

	var $use_MEM = 0;

	var $mf = array();
	var $dscr = array();

	# RuMor dictionary file
	var $rumor_dic = "tree.fin";
	var $rumor_dic2 = "prof.fin";

	# RuMor directory
	var $rumor_dir = ".";

	# RuMor dictionry descriptor
	var $fd = NULL;
	var $FH = NULL;

	function RuMor($path = '.') {
		$this->talf = $this->alf * $this->alf * $this->alf * 4;
		$this->rumor_dir = $path;

		#  существительные

		$this->mf['И']  = "ед,им";
		$this->mf['Р']  = "ед,рд";
		$this->mf['Д']  = "ед,дт";
		$this->mf['В']  = "ед,вн";
		$this->mf['Т']  = "ед,тв";
		$this->mf['П']  = "ед,пр";
		$this->mf['ИМ'] = "мн,им";
		$this->mf['РМ'] = "мн,рд";
		$this->mf['ДМ'] = "мн,дт";
		$this->mf['ВМ'] = "мн,вн";
		$this->mf['ТМ'] = "мн,тв";
		$this->mf['ПМ'] = "мн,пр";

		$this->mf['ВН']  = "ед,вн,но";
		$this->mf['ВО']  = "ед,вн,од";


		$this->mf['ФМИ']    = "мр,ед,им";
		$this->mf['ФМР']    = "мр,ед,рд";
		$this->mf['ФМД']    = "мр,ед,дт";
		$this->mf['ФМВ']    = "мр,ед,вн";
		$this->mf['ФМТ']    = "мр,ед,тв";
		$this->mf['ФМП']    = "мр,ед,пр";
		$this->mf['ФЖИ']    = "жр,ед,им";
		$this->mf['ФЖР']    = "жр,ед,рд";
		$this->mf['ФЖД']    = "жр,ед,дт";
		$this->mf['ФЖВ']    = "жр,ед,вн";
		$this->mf['ФЖТ']    = "жр,ед,тв";
		$this->mf['ФЖП']    = "жр,ед,пр";
		$this->mf['ФМНИ']   = "мн,им";
		$this->mf['ФМНР']   = "мн,рд";
		$this->mf['ФМНД']   = "мн,дт";
		$this->mf['ФМНВ']   = "мн,вн";
		$this->mf['ФМНТ']   = "мн,тв";
		$this->mf['ФМНП']   = "мн,пр";


		#     ПРИЛАГАТЕЛЬНЫЕ

		$this->mf['МИ']    = "мр,ед,им,";
		$this->mf['МР']    = "мр,ед,рд,од,но";
		$this->mf['МД']    = "мр,ед,дт,од,но";
		$this->mf['МВ']    = "мр,ед,вн,од,но";
		$this->mf['МВН']   = "мр,ед,вн,но";
		$this->mf['МВО']   = "мр,ед,вн,од";
		$this->mf['МТ']    = "мр,ед,тв,од,но";
		$this->mf['МП']    = "мр,ед,пр,од,но";
		$this->mf['СИ']    = "жр,ед,им,од,но";
		$this->mf['СР']    = "жр,ед,рд,од,но";
		$this->mf['СД']    = "жр,ед,дт,од,но";
		$this->mf['СВ']    = "жр,ед,вн,од,но";
		$this->mf['СТ']    = "жр,ед,тв,од,но";
		$this->mf['СП']    = "жр,ед,пр,од,но";
		$this->mf['ЖИ']    = "ср,ед,им,од,но";
		$this->mf['ЖР']    = "ср,ед,рд,од,но";
		$this->mf['ЖД']    = "ср,ед,дт,од,но";
		$this->mf['ЖВ']    = "ср,ед,вн,од,но";
		$this->mf['ЖТ']    = "ср,ед,тв,од,но";
		$this->mf['ЖП']    = "ср,ед,пр,од,но";
		$this->mf['МНИ']   = "мн,им,од,но";
		$this->mf['МНР']   = "мн,рд,од,но";
		$this->mf['МНД']   = "мн,дт,од,но";
		$this->mf['МНВ']   = "мн,вн,од,но";
		$this->mf['МНВН']  = "мн,вн,но";
		$this->mf['МНВО']  = "мн,вн,од";
		$this->mf['МНТ']   = "мн,тв,од,но";
		$this->mf['МНП']   = "мн,пр,од,но";
		$this->mf['МКР']   = "мр,ед,кр,од,но";
		$this->mf['ЖКР']   = "жр,ед,кр,од,но";
		$this->mf['СКР']   = "ср,ед,кр,од,но";
		$this->mf['МНКР']  = "мн,кр,од,но";
		$this->mf['СР']    = "сравн,од,но";



		#     глагол

		$this->mf['ИНФ']  = "дст,инф";


		#  ========   личные формы глагола  ============

		$this->mf['НЕД1']  = "дст,нст,1л,ед";
		$this->mf['НЕД2']  = "дст,нст,2л,ед";
		$this->mf['НЕД3']  = "дст,нст,3л,ед";
		$this->mf['НМН1']  = "дст,нст,1л,мн";
		$this->mf['НМН2']  = "дст,нст,2л,мн";
		$this->mf['НМН3']  = "дст,нст,3л,мн";
		$this->mf['БЕД1']  = "дст,буд,1л,ед";
		$this->mf['БЕД2']  = "дст,буд,2л,ед";
		$this->mf['БЕД3']  = "дст,буд,3л,ед";
		$this->mf['БМН1']  = "дст,буд,1л,мн";
		$this->mf['БМН2']  = "дст,буд,2л,мн";
		$this->mf['БМН3']  = "дст,буд,3л,мн";
		$this->mf['ПМУ']   = "дст,прш,мр,ед";
		$this->mf['ПЖЕ']   = "дст,прш,жр,ед";
		$this->mf['ПСР']   = "дст,прш,ср,ед";
		$this->mf['ПМН']   = "дст,прш,мн";


		# ===================================================
		# ==============   ДЕЕПРИЧАСТИЕ ====================
		# ===================================================

		$this->mf['ДЕЕН']  = "дст,дпр,нст";
		$this->mf['ДЕЕП']  = "дст,дпр,прш";


		# ===================================================
		# ==============   ИМПЕРАТИВ    ====================

		$this->mf['ИМПЕ1'] = "дст,пвл,1л,ед";
		$this->mf['ИМПМ1'] = "дст,пвл,1л,мн";
		$this->mf['ИМПЕ2'] = "дст,пвл,2л,ед";
		$this->mf['ИМПМ2'] = "дст,пвл,2л,мн";

		#  действительное причастие настоящего времени

		$this->mf['ПРИМИ']    = "прч,од,но,нст,дст,ед,мр,им";
		$this->mf['ПРИМР']    = "прч,од,но,нст,дст,ед,мр,рд";
		$this->mf['ПРИМД']    = "прч,од,но,нст,дст,ед,мр,дт";
		$this->mf['ПРИМВН']   = "прч,но,нст,дст,ед,мр,вн";
		$this->mf['ПРИМВО']   = "прч,од,нст,дст,ед,мр,вн";
		$this->mf['ПРИМТ']    = "прч,од,но,нст,дст,ед,мр,тв";
		$this->mf['ПРИМП']    = "прч,од,но,нст,дст,ед,мр,пр";
		$this->mf['ПРИСИ']    = "прч,од,но,нст,дст,ед,ср,им";
		$this->mf['ПРИСР']    = "прч,од,но,нст,дст,ед,ср,рд";
		$this->mf['ПРИСД']    = "прч,од,но,нст,дст,ед,ср,дт";
		$this->mf['ПРИСВ']    = "прч,од,но,нст,дст,ед,ср,вн";
		$this->mf['ПРИСТ']    = "прч,од,но,нст,дст,ед,ср,тв";
		$this->mf['ПРИСП']    = "прч,од,но,нст,дст,ед,ср,пр";
		$this->mf['ПРИЖИ']    = "прч,од,но,нст,дст,ед,жр,им";
		$this->mf['ПРИЖР']    = "прч,од,но,нст,дст,ед,жр,рд";
		$this->mf['ПРИЖД']    = "прч,од,но,нст,дст,ед,жр,дт";
		$this->mf['ПРИЖВ']    = "прч,од,но,нст,дст,ед,жр,вн";
		$this->mf['ПРИЖТ']    = "прч,од,но,нст,дст,ед,жр,тв";
		$this->mf['ПРИЖП']    = "прч,од,но,нст,дст,ед,жр,пр";
		$this->mf['ПРИМНИ']   = "прч,од,но,нст,дст,мн,им";
		$this->mf['ПРИМНР']   = "прч,од,но,нст,дст,мн,рд";
		$this->mf['ПРИМНД']   = "прч,од,но,нст,дст,мн,дт";
		$this->mf['ПРИМНВН']  = "прч,но,нст,дст,мн,вн";
		$this->mf['ПРИМНВО']  = "прч,од,нст,дст,мн,вн";
		$this->mf['ПРИМНТ']   = "прч,од,но,нст,дст,мн,тв";
		$this->mf['ПРИМНП']   = "прч,од,но,нст,дст,мн,пр";



		#  действительное причастие прошедшего времени

		$this->mf['ПРИПМИ']    = "прч,од,но,прш,дст,ед,мр,им";
		$this->mf['ПРИПМР']    = "прч,од,но,прш,дст,ед,мр,рд";
		$this->mf['ПРИПМД']    = "прч,од,но,прш,дст,ед,мр,дт";
		$this->mf['ПРИПМВН']   = "прч,но,прш,дст,ед,мр,вн";
		$this->mf['ПРИПМВО']   = "прч,од,прш,дст,ед,мр,вн";
		$this->mf['ПРИПМТ']    = "прч,од,но,прш,дст,ед,мр,тв";
		$this->mf['ПРИПМП']    = "прч,од,но,прш,дст,ед,мр,пр";
		$this->mf['ПРИПСИ']    = "прч,од,но,прш,дст,ед,ср,им";
		$this->mf['ПРИПСР']    = "прч,од,но,прш,дст,ед,ср,рд";
		$this->mf['ПРИПСД']    = "прч,од,но,прш,дст,ед,ср,дт";
		$this->mf['ПРИПСВ']    = "прч,од,но,прш,дст,ед,ср,вн";
		$this->mf['ПРИПСТ']    = "прч,од,но,прш,дст,ед,ср,тв";
		$this->mf['ПРИПСП']    = "прч,од,но,прш,дст,ед,ср,пр";
		$this->mf['ПРИПЖИ']    = "прч,од,но,прш,дст,ед,жр,им";
		$this->mf['ПРИПЖР']    = "прч,од,но,прш,дст,ед,жр,рд";
		$this->mf['ПРИПЖД']    = "прч,од,но,прш,дст,ед,жр,дт";
		$this->mf['ПРИПЖВ']    = "прч,од,но,прш,дст,ед,жр,вн";
		$this->mf['ПРИПЖТ']    = "прч,од,но,прш,дст,ед,жр,тв";
		$this->mf['ПРИПЖП']    = "прч,од,но,прш,дст,ед,жр,пр";
		$this->mf['ПРИПМНИ']   = "прч,од,но,прш,дст,мн,им";
		$this->mf['ПРИПМНР']   = "прч,од,но,прш,дст,мн,рд";
		$this->mf['ПРИПМНД']   = "прч,од,но,прш,дст,мн,дт";
		$this->mf['ПРИПМНВН']  = "прч,но,прш,дст,мн,вн";
		$this->mf['ПРИПМНВО']  = "прч,од,прш,дст,мн,вн";
		$this->mf['ПРИПМНТ']   = "прч,од,но,прш,дст,мн,тв";
		$this->mf['ПРИПМНП']   = "прч,од,но,прш,дст,мн,пр";



		#  страдательное причастие настоящего времени


		$this->mf['ПРСНМИ']     = "прч,од,но,нст,стр,ед,мр,им";
		$this->mf['ПРСНМР']     = "прч,од,но,нст,стр,ед,мр,рд";
		$this->mf['ПРСНМД']     = "прч,од,но,нст,стр,ед,мр,дт";
		$this->mf['ПРСНМВН']    = "прч,но,нст,стр,ед,мр,вн";
		$this->mf['ПРСНМВО']    = "прч,од,нст,стр,ед,мр,вн";
		$this->mf['ПРСНМТ']     = "прч,од,но,нст,стр,ед,мр,тв";
		$this->mf['ПРСНМП']     = "прч,од,но,нст,стр,ед,мр,пр";
		$this->mf['ПРСНСИ']     = "прч,од,но,нст,стр,ед,ср,им";
		$this->mf['ПРСНСР']     = "прч,од,но,нст,стр,ед,ср,рд";
		$this->mf['ПРСНСД']     = "прч,од,но,нст,стр,ед,ср,дт";
		$this->mf['ПРСНСВ']     = "прч,од,но,нст,стр,ед,ср,вн";
		$this->mf['ПРСНСТ']     = "прч,од,но,нст,стр,ед,ср,тв";
		$this->mf['ПРСНСП']     = "прч,од,но,нст,стр,ед,ср,пр";
		$this->mf['ПРСНЖИ']     = "прч,од,но,нст,стр,ед,жр,им";
		$this->mf['ПРСНЖР']     = "прч,од,но,нст,стр,ед,жр,рд";
		$this->mf['ПРСНЖД']     = "прч,од,но,нст,стр,ед,жр,дт";
		$this->mf['ПРСНЖВ']     = "прч,од,но,нст,стр,ед,жр,вн";
		$this->mf['ПРСНЖТ']     = "прч,од,но,нст,стр,ед,жр,тв";
		$this->mf['ПРСНЖП']     = "прч,од,но,нст,стр,ед,жр,пр";
		$this->mf['ПРСНМНИ']    = "прч,од,но,нст,стр,мн,им";
		$this->mf['ПРСНМНР']    = "прч,од,но,нст,стр,мн,рд";
		$this->mf['ПРСНМНД']    = "прч,од,но,нст,стр,мн,дт";
		$this->mf['ПРСНМНВН']   = "прч,но,нст,стр,мн,вн";
		$this->mf['ПРСНМНВО']   = "прч,од,нст,стр,мн,вн";
		$this->mf['ПРСНМНТ']    = "прч,од,но,нст,стр,мн,тв";
		$this->mf['ПРСНМНП']    = "прч,од,но,нст,стр,мн,пр";

		$this->mf['ПРСМ']  = "прч,од,но,нст,стр,ед,мр,кр";
		$this->mf['ПРСЖ']  = "прч,од,но,нст,стр,ед,жр,кр";
		$this->mf['ПРСС']  = "прч,од,но,нст,стр,ед,ср,кр";
		$this->mf['ПРСМН'] = "прч,од,но,нст,стр,мн,кр";


		#  страдательное причастие прошедшего времени


		$this->mf['ПРСПМИ']     = "прч,од,но,прш,стр,ед,мр,им";
		$this->mf['ПРСПМР']     = "прч,од,но,прш,стр,ед,мр,рд";
		$this->mf['ПРСПМД']     = "прч,од,но,прш,стр,ед,мр,дт";
		$this->mf['ПРСПМВН']    = "прч,но,прш,стр,ед,мр,вн";
		$this->mf['ПРСПМВО']    = "прч,од,прш,стр,ед,мр,вн";
		$this->mf['ПРСПМТ']     = "прч,од,но,прш,стр,ед,мр,тв";
		$this->mf['ПРСПМП']     = "прч,од,но,прш,стр,ед,мр,пр";
		$this->mf['ПРСПСИ']     = "прч,од,но,прш,стр,ед,ср,им";
		$this->mf['ПРСПСР']     = "прч,од,но,прш,стр,ед,ср,рд";
		$this->mf['ПРСПСД']     = "прч,од,но,прш,стр,ед,ср,дт";
		$this->mf['ПРСПСВ']     = "прч,од,но,прш,стр,ед,ср,вн";
		$this->mf['ПРСПСТ']     = "прч,од,но,прш,стр,ед,ср,тв";
		$this->mf['ПРСПСП']     = "прч,од,но,прш,стр,ед,ср,пр";
		$this->mf['ПРСПЖИ']     = "прч,од,но,прш,стр,ед,жр,им";
		$this->mf['ПРСПЖР']     = "прч,од,но,прш,стр,ед,жр,рд";
		$this->mf['ПРСПЖД']     = "прч,од,но,прш,стр,ед,жр,дт";
		$this->mf['ПРСПЖВ']     = "прч,од,но,прш,стр,ед,жр,вн";
		$this->mf['ПРСПЖТ']     = "прч,од,но,прш,стр,ед,жр,тв";
		$this->mf['ПРСПЖП']     = "прч,од,но,прш,стр,ед,жр,пр";
		$this->mf['ПРСПМНИ']    = "прч,од,но,прш,стр,мн,им";
		$this->mf['ПРСПМНР']    = "прч,од,но,прш,стр,мн,рд";
		$this->mf['ПРСПМНД']    = "прч,од,но,прш,стр,мн,дт";
		$this->mf['ПРСПМНВН']   = "прч,но,прш,стр,мн,вн";
		$this->mf['ПРСПМНВО']   = "прч,од,прш,стр,мн,вн";
		$this->mf['ПРСПМНТ']    = "прч,од,но,прш,стр,мн,тв";
		$this->mf['ПРСПМНП']    = "прч,од,но,прш,стр,мн,пр";


		$this->mf['ПРСПМ']   = "прч,од,но,прш,стр,ед,мр,кр";
		$this->mf['ПРСПЖ']   = "прч,од,но,прш,стр,ед,жр,кр";
		$this->mf['ПРСПС']   = "прч,од,но,прш,стр,ед,ср,кр";
		$this->mf['ПРСПМН']  = "прч,од,но,прш,стр,мн,кр";

		#  наречие
		$this->mf['Н']  = "неизм.";

		#  предлог
		$this->mf['ПР']  = "неизм.";

		#  союз
		$this->mf['СЗ']  = "неизм.";


		#=====================================================================


		$this->dscr[1]  = "существительное мужского рода неодушевленное";
		$this->dscr[2]  = "существительное мужского рода одушевленное";
		$this->dscr[3]  = "существительное женского рода неодушевленное";
		$this->dscr[4]  = "существительное женского рода одушевленное";
		$this->dscr[5]  = "существительное среднего рода неодушевленное";
		$this->dscr[6]  = "существительное среднего рода одушевленное";
		$this->dscr[7]  = "существительное общего рода одушевленное";
		$this->dscr[8]  = "существительное мн. число";
		$this->dscr[9]  = "числительное";
		$this->dscr[10] = "местоимение";
		$this->dscr[11] = "местоименное прилагательное";
		$this->dscr[12] = "глагол несовершенного вида";
		$this->dscr[13] = "глагол совершенного вида";
		$this->dscr[14] = "глагол совершенного и несовершенного вида";
		$this->dscr[15] = "прилагательное";
		$this->dscr[16] = "порядковое числительное";
		$this->dscr[17] = "наречие";
		$this->dscr[18] = "предлог";
		$this->dscr[19] = "союз";

	}

	#=====================================================================

	function prep_dict($dir = "") {
	    if ($dir) {
			$this->rumor_dir = $dir;
		}
	    $this->fd = fopen ($this->rumor_dir."/".$this->rumor_dic, "rb");
	}

	#=====================================================================

	function prep_dict_MEM($dir) {

	    if (!$dir) {
			$this->rumor_dir = $dir;
		}
	    $this->fd = fopen ($this->rumor_dir."/".$this->rumor_dic, "rb");
	    $size = filesize($this->rumor_dir."/".$this->rumor_dic);
	    $GLOBALS["tree"] = fread ($this->fd, $size);

	    $dum = unpack("Nnum",substr($GLOBALS["tree"],$this->talf,4));
	    $profiles_num = $dum['num'];

	    for ($i=1; $i <= $profiles_num; $i++) {
	        $dum = substr($GLOBALS["tree"],$this->talf+4+$i*6,6);
	        $dum = unpack("Npos/nlen",$dum);
	        $profile = substr($GLOBALS["tree"],$dum['pos'],$dum['len']);
	        $this->profiles[$i] = $profile;
	        $this->profiles_length[$i] = strpos($profile,"|",1) - 1;
	    }
	    $this->use_MEM = 1;

	}

	#=====================================================================

	function find_word_HD($word) {

	    if ($this->use_MEM == 1) {
	        return $this->find_word_MEM($word);
	    }

	    $next_pos = 0;
	    $res = array();

	    if (strlen($word) > 2) {
	        $pos = (ord($word[0])%$this->alf+(ord($word[1])%$this->alf)*$this->alf+(ord($word[2])%$this->alf)*$this->alf*$this->alf)*4;
	    } elseif (strlen($word) == 2) {
	        $pos = (ord($word[0])%$this->alf+(ord($word[1])%$this->alf)*$this->alf)*4;
	    } elseif (strlen($word) == 1) {
	        $pos = (ord($word[0])%$this->alf)*4;
	    } else {
	        return $res;
	    }


	    fseek($this->fd,$pos,0);
	    $dum = unpack("Npos",fread($this->fd,4));
	    $next_pos = $dum['pos'];
	    if ($next_pos == 0) { return $res; }

	    $base = substr($word,0,3);


	    fseek($this->fd,$next_pos+1,0);
	    $dum = unpack("Ndum",fread($this->fd,4));
	    $pos = $dum['dum'];

	    while ($pos) {
	        fseek($this->fd,$pos,0);
	        $dum = unpack("Clen/Npos",fread($this->fd,5));
	        $len = $dum['len'];
	        $res[] = fread($this->fd,$len);
	        $pos = $dum['pos'];
	    }
	    $next_pos += 5;

	    $base = "";
	    for($i=0; $i < strlen($word); $i++) {
	        $letter = ord($word[$i]);
	        $base .= $word[$i];

	        while (1) {
	        	fseek($this->fd,$next_pos,0);
	        	$dum = unpack("Clet/Nless/Nequal/Nbigger/Nval_pos",fread($this->fd,17));
	          	if ($letter == $dum['let']) { $next_pos = $dum['equal']; break; };
	          	if ($letter < $dum['let']) {
	          	    $next_pos = $dum['less'];
	          	} else {
	          	    $next_pos = $dum['bigger'];
	          	}
	          	if ($next_pos == 0)  { break; };
	        }

	        $val_pos = $dum['val_pos'];
	        $let = $dum['let'];
	        $equal = $dum['equal'];
	    	if ( $let == $letter && $val_pos != 0 ) {
	    	    while ($val_pos != 0) {
	    	     	fseek($this->fd,$val_pos,0);
	    	     	$dum = unpack("Clen/Nval_pos", fread($this->fd,5));
	    	     	$len = $dum['len'];
	    	     	$val_pos = $dum['val_pos'];
	    	     	$res[] = $base.fread($this->fd,$len);
	    	    }
	    	}

	    	if ($next_pos == 0) {
	    	    break;
	    	}

	    }

	    return $res;

	}
	#=====================================================================

	function find_word_MEM($word) {

	    $next_pos = 0;
	    $res = array();

	    if (strlen($word) > 2) {
	        $pos = (ord($word[0])%$this->alf+(ord($word[1])%$this->alf)*$this->alf+(ord($word[2])%$this->alf)*$this->alf*$this->alf)*4;
	    } elseif (strlen($word) == 2) {
	        $pos = (ord($word[0])%$this->alf+(ord($word[1])%$this->alf)*$this->alf)*4;
	    } elseif (strlen($word) == 1) {
	        $pos = (ord($word[0])%$this->alf)*4;
	    } else {
	        return $res;
	    }

	    $dum = unpack("Npos",substr($GLOBALS["tree"],$pos,4));
	    $next_pos = $dum['pos'];
	    if ($next_pos == 0) { return $res; }

	    $base = substr($word,0,3);


	    $dum = unpack("Ndum",substr($GLOBALS["tree"],$next_pos+1,4));
	    $pos = $dum['dum'];

	    while ($pos) {
	        $dum = unpack("Clen/Npos",substr($GLOBALS["tree"],$pos,5));
	        $len = $dum['len'];
	        $new_pos = $dum['pos'];
	        $res[] = substr($GLOBALS["tree"],$pos+5,$len);
	        $pos = $new_pos;
	    }
	    $next_pos += 5;

	    $base = "";
	    for($i=0; $i < strlen($word); $i++) {
	        $letter = ord($word[$i]);
	        $base .= $word[$i];

	        while (1) {
	        	$dum = unpack("Clet/Nless/Nequal/Nbigger/Nval_pos",substr($GLOBALS["tree"],$next_pos,17));
	          	if ($letter == $dum['let']) { $next_pos = $dum['equal']; break; };
	          	if ($letter < $dum['let']) {
	          	    $next_pos = $dum['less'];
	          	} else {
	          	    $next_pos = $dum['bigger'];
	          	}
	          	if ($next_pos == 0)  { break; };
	        }

	        $val_pos = $dum['val_pos'];
	        $let = $dum['let'];
	        $equal = $dum['equal'];
	    	if ( $let == $letter && $val_pos != 0 ) {
	    	    while ($val_pos != 0) {
	    	     	$dum = unpack("Clen/Nval_pos", substr($GLOBALS["tree"],$val_pos,5));
	    	     	$len = $dum['len'];
	    	     	$res[] = $base.substr($GLOBALS["tree"],$val_pos+5,$len);
	    	     	$val_pos = $dum['val_pos'];
	    	    }
	    	}

	    	if ($next_pos == 0) {
	    	    break;
	    	}

	    }

	    return $res;

	}
	#=====================================================================

	function get_base_form($query) {

	    $base = array();

	    $words = $this->find_word_HD($query);

	    foreach ($words as $word) {
	        $flag = substr($word,strpos($word,"/")+1);
	        $word = substr($word,0,strpos($word,"/"));
	        if ($word == $query) {
	            $base[] = $word;
	        } else {
	            $dum = unpack("nflag",$flag);
	            $flag = $dum['flag'];
	            if (! isset($this->profiles[$flag]) ) {
	                fseek($this->fd,$this->talf+4+$flag*6,0);
	                $dum = unpack("Npos/nlen",fread($this->fd,6));
	                fseek($this->fd,$dum['pos'],0);
	                $dum = fread($this->fd,$dum['len']);
	                $this->profiles[$flag] = $dum;
	                $this->profiles_length[$flag] = strpos($dum,"|",1) - 1;
	            }
	            $pref_len = strlen($word) - $this->profiles_length[$flag];
	            if ($pref_len > strlen($query)) { continue; }
	            if ( $pref_len > 0 && strpos($query,substr($word,0,$pref_len)) !== 0 ) { continue; }
	            $aff = substr($query,$pref_len);
	            if ( strpos($this->profiles[$flag],"|$aff|") !== FALSE) {
	                $base[] = $word;
	            }
	        }
	    }
	    return $base;
	}

	#=====================================================================

	function get_all_forms($query) {

	    $forms = array();

	    $words = $this->find_word_HD($query);
	    foreach ($words as $word) {
	        $flag = substr($word,strpos($word,"/")+1);
	        $word = substr($word,0,strpos($word,"/"));
	        $dum = unpack("nflag",$flag);
	        $flag = $dum['flag'];
	        if (! isset($this->profiles[$flag]) ) {
	            fseek($this->fd,$this->talf+4+$flag*6,0);
	            $dum = unpack("Npos/nlen",fread($this->fd,6));
	            $pos = $dum['pos'];
	            $profile_length = $dum['len'];
	            fseek($this->fd,$pos,0);
	            $dum = fread($this->fd,$profile_length);
	            $this->profiles[$flag] = $dum;
	            $this->profiles_length[$flag] = strpos($dum,"|",1) - 1;
	        }
	        if ($word == $query) {
	            $new_forms = $this->gen_all_words(substr($word,0,strlen($word)-$this->profiles_length[$flag]),$flag);
	            foreach ($new_forms as $v) {
	                $forms[] = $v;
	            }
	        } else {
	            $pref_len = strlen($word) - $this->profiles_length[$flag];
	            if ($pref_len > strlen($query)) { continue; }
	            if ( $pref_len > 0 && strpos($query,substr($word,0,$pref_len)) !== 0 ) { continue; }
	            $aff = substr($query,$pref_len);
	            if ( strpos($this->profiles[$flag],"|$aff|") !== FALSE) {
	                $new_forms = $this->gen_all_words(substr($word,0,strlen($word)-$this->profiles_length[$flag]),$flag);
	                foreach ($new_forms as $v) {
	                    $forms[] = $v;
	                }
	            }
	        }
	    }
	    return $forms;
	}

	#=====================================================================

	function get_paradigm($query) {

	    $forms = array();

	    $words = $this->find_word_HD($query,$this->fd);
	    foreach ($words as $word) {
	        $flag = substr($word,strpos($word,"/")+1);
	        $word = substr($word,0,strpos($word,"/"));
	        $dum = unpack("nflag",$flag);
	        $flag = $dum['flag'];
	        if (! isset($this->profiles[$flag]) ) {
	            fseek($this->fd,$this->talf+4+$flag*6,0);
	            $dum = unpack("Npos/nlen",fread($this->fd,6));
	            $pos = $dum['pos'];
	            $profile_length = $dum['len'];
	            fseek($this->fd,$pos,0);
	            $dum = fread($this->fd,$profile_length);
	            $this->profiles[$flag] = $dum;
	            $this->profiles_length[$flag] = strpos($dum,"|",1) - 1;
	        }
	        if ($word == $query) {
	            $new_forms = $this->gen_all_words(substr($word,0,strlen($word)-$this->profiles_length[$flag]),$flag);
	            foreach ($new_forms as $v) {
	                $forms[] = $v;
	            }
	        }
	    }
	    return $forms;
	}

	#=====================================================================

	function get_full_paradigm($query) {

	    $word = "";
	    $flag = "";
	    $res = array();

	    $words = $this->find_word_HD($query);
	    foreach ($words as $word) {
	        $flag = substr($word,strpos($word,"/")+1);
	        $word = substr($word,0,strpos($word,"/"));
	        $dum = unpack("nflag",$flag);
	        $flag = $dum['flag'];
	        if (! isset($this->profiles_full[$flag]) ) {

	            if (! isset($this->FH)) {
	                $this->FH = fopen ($this->rumor_dir."/".$this->rumor_dic2, "rb");
	            }


	            fseek($this->FH,$flag*7,0);
	            $dum = unpack("Npos/nlen/Clll",fread($this->FH,7));
	            $pos = $dum['pos'];
	            $profile_length = $dum['len'];
	            $lll = $dum['lll'];
	            if ($pos == 0) { continue; }
	            fseek($this->FH,$pos,0);
	            $dum = fread($this->FH,$profile_length);
	            $this->profiles[$flag] = $dum;
	            $this->profiles_full[$flag] = substr($dum,0,strpos($dum,"#",1));
	            $this->profiles_morf[$flag] = substr($dum,strpos($dum,"#"));
	            $this->profiles_lll[$flag] = $this->dscr[$lll];
	            $this->profiles_code[$flag] = $lll;
	            $this->profiles_length[$flag] = strpos($dum,"|",1) - 1;
	        }

	        $forms = array();
	        $morfs = array();

	        if ($word == $query) {

	            $aff = preg_split ("/\|/",substr($this->profiles_full[$flag],1));
	            unset($aff[count($aff)-1]);
	            $base = substr($word,0,strlen($word)-$this->profiles_length[$flag]);
	            foreach ($aff as $k => $v) {
	                $forms[] = $base.$v;
	            }

	            $aff = preg_split ("/\#/",substr($this->profiles_morf[$flag],1));
	            unset($aff[count($aff)-1]);
	            foreach ($aff as $k => $v) {
	                $morfs[] = $this->mf[$v];
	            }

	            $res[] = array($this->profiles_lll[$flag],$forms,$morfs);

	        } else {

	            $pref_len = strlen($word) - $this->profiles_length[$flag];
	            if ($pref_len > strlen($query)) { continue; }
	            if ( $pref_len > 0 && strpos($query,substr($word,0,$pref_len)) !== 0 ) { continue; }
	            $aff = substr($query,$pref_len);
	            if ( strpos($this->profiles_full[$flag],"|$aff|") !== FALSE) {

	                $aff = preg_split ("/\|/",substr($this->profiles_full[$flag],1));
	                unset($aff[count($aff)-1]);
	                $base = substr($word,0,$pref_len);
	                foreach ($aff as $k => $v) {
	                    $forms[] = $base.$v;
	                }

	                $aff = preg_split ("/\#/",substr($this->profiles_morf[$flag],1));
	                unset($aff[count($aff)-1]);
	                foreach ($aff as $k => $v) {
	                    $morfs[] = $this->mf[$v];
	                }

	                $res[] = array($this->profiles_lll[$flag],$forms,$morfs);

	            }

	        }

	    }
	    return $res;
	}

	#=====================================================================

	function rumor_get_word_info($query) {

	    $word = "";
	    $flag = "";
	    $res = array();

	    $words = $this->find_word_HD($query);
	    foreach ($words as $word) {
	        $flag = substr($word,strpos($word,"/")+1);
	        $word = substr($word,0,strpos($word,"/"));
	        $dum = unpack("nflag",$flag);
	        $flag = $dum['flag'];
	        if (! isset($this->profiles_full[$flag]) ) {
	            if (! isset($this->FH)) {
	                $this->FH = fopen ($this->rumor_dir."/".$this->rumor_dic2, "rb");
	            }
	            fseek($this->FH,$flag*7,0);
	            $dum = unpack("Npos/nlen/Clll",fread($this->FH,7));
	            $pos = $dum['pos'];
	            $profile_length = $dum['len'];
	            $lll = $dum['lll'];
	            if ($pos == 0) { continue; }
	            fseek($this->FH,$pos,0);
	            $dum = fread($this->FH,$profile_length);
	            $this->profiles[$flag] = $dum;
	            $this->profiles_full[$flag] = substr($dum,0,strpos($dum,"#",1));
	            $this->profiles_morf[$flag] = substr($dum,strpos($dum,"#"));
	            $this->profiles_lll[$flag] = $this->dscr[$lll];
	            $this->profiles_code[$flag] = $lll;
	            $this->profiles_length[$flag] = strpos($dum,"|",1) - 1;
	        }

	        $pref_len = strlen($word) - $this->profiles_length[$flag];
	        if ($pref_len > strlen($query)) { continue; }
	        if ( $pref_len > 0 && strpos($query,substr($word,0,$pref_len)) !== 0 ) { continue; }
	        $aff = substr($query,$pref_len);

	        $codes = preg_split ("/\#/",substr($this->profiles_morf[$flag],1));
	        $affixes = preg_split ("/\|/",substr($this->profiles_full[$flag],1));
	        unset($codes[count($codes)-1]);
	        unset($affixes[count($affixes)-1]);


	        for ($i=0;$i<count($codes);$i++) {
	            if ($aff == $affixes[$i]) {
	                $res[] = array (
	                        "word" => $query,
	                        "baseword" => $word,
	                        "rule" => $flag,
	                        "wordcode" => $this->profiles_code[$flag],
	                        "wordinfo" => $this->profiles_lll[$flag],
	                        "formcode" => $codes[$i],
	                        "forminfo" => $this->mf[$codes[$i]],
	                        );
	            }
	        }

	    }
	    return $res;
	}

	#=====================================================================

	function rumor_set_wordform($word,$flag,$formcode) {

        if (! isset($this->profiles_full[$flag]) ) {
            if (! isset($this->FH)) {
                $this->FH = fopen ($this->rumor_dir."/".$this->rumor_dic2, "rb");
            }
            fseek($this->FH,$flag*7,0);
            $dum = unpack("Npos/nlen/Clll",fread($this->FH,7));
            $pos = $dum['pos'];
            $profile_length = $dum['len'];
            $lll = $dum['lll'];
            fseek($this->FH,$pos,0);
            $dum = fread($this->FH,$profile_length);
            $this->profiles[$flag] = $dum;
            $this->profiles_full[$flag] = substr($dum,0,strpos($dum,"#",1));
            $this->profiles_morf[$flag] = substr($dum,strpos($dum,"#"));
            $this->profiles_lll[$flag] = $this->dscr[$lll];
            $this->profiles_code[$flag] = $lll;
            $this->profiles_length[$flag] = strpos($dum,"|",1) - 1;
        }

	    $pref_len = strlen($word) - $this->profiles_length[$flag];
	    $base = substr($word,0,$pref_len);

	    $codes = preg_split ("/\#/",substr($this->profiles_morf[$flag],1));
	    $affixes = preg_split ("/\|/",substr($this->profiles_full[$flag],1));
	    unset($codes[count($codes)-1]);
	    unset($affixes[count($affixes)-1]);

	    for ($i=0;$i<count($codes);$i++) {
	        if ($formcode == $codes[$i]) {
	            return $base.$affixes[$i];
	        }
	    }
	    return -1;

	}
	#=====================================================================

	function gen_all_words($word,$flag) {

	#    $aff = explode("|",substr($this->profiles[$flag],1));
	    $aff = preg_split ("/\|/",substr($this->profiles[$flag],1));
	    unset($aff[count($aff)-1]);
	    foreach ($aff as $k => $v) {
	        $aff[$k] = $word.$v;
	    }
	    return $aff;

	}

	#=====================================================================
	#=====================================================================
	#
	#    These functions are obsolete
	#
	#=====================================================================
	#=====================================================================
	function get_full_paradigm_MEM($query) {
	    return $this->get_full_paradigm($query);
	}
	function get_paradigm_MEM($query) {
	    return $this->get_paradigm($query);
	}
	function get_all_forms_MEM($query) {
	    return $this->get_all_forms($query);
	}
	function get_base_form_MEM($query) {
	    return $this->get_base_form($query);
	}
	#=====================================================================
	#=====================================================================
}
?>