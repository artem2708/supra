<?php
//$_SERVER["DOCUMENT_ROOT"] = "";
//$_SERVER["HTTP_HOST"] = "";
if (ini_get('safe_mode') != '1' && function_exists('set_time_limit'))
	 set_time_limit(0);

require_once($_SERVER["DOCUMENT_ROOT"].'/common.php');

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Cache-Control: no-cache");
header("Cache-Control: post-check=0, pre-check=0");
header("Pragma: no-cache");
header("Last-Modified: " . gmdate('D, d M Y H:i:s \G\M\T'));


require_once(RP.'mod/catalog/lib/class.Catalog.php');
$catalog = new Catalog('only_create_object');
// получим последний файл
$file_name = "";
$import_dir = RP.$CONFIG["catalog_import_path"];
$oItem	= opendir($import_dir);
while ($sItem = readdir($oItem)) {
	if($sItem=="."||$sItem=="..") continue;
	if(substr($sItem, 0, 8) !== "catalog_") continue;
	if($sItem > $file_name) $file_name = $sItem;
}
if (empty($file_name))
{
	echo "Файл не найден<br>";
	exit;
}
else
{
	echo "Найден файл " . $import_dir.$file_name . "<br>";
}
// загрузим каталог
if ($dom = domxml_open_file($import_dir.$file_name)) {
    $request_clear_xml = true;
	$catalog->doImportXMLData($dom);
	unlink($import_dir.$file_name);
	echo "Загрузка каталога завершена<br>";
}
else
{
	echo "Ошибка парсера<br>";
}
exit;
?>