$(document).ready(function(){
    $('a.lightbox_a').colorbox({
		rel: 'gallery',
        opacity: 0.8,
		transition: "elastic",
		maxWidth: "68%",
		title: $('.custom_h2').text(),
		onComplete:function() {
			$('div#cboxContent').append('<a class="dowlimg" href="#" rel="nofollow" type="image/jpeg">Скачать фото</a>');
			$('.dowlimg').attr('href' , $('.cboxPhoto').attr('src') );
			},
		onClosed:function() {
			$('.dowlimg').remove();
			}
    });
	
})

function addtocartfunction(is_now){
	
	if(!is_now && $('#cart_message_gift').length > 0){
		showChoose();
		return false;
	}
	
	$.ajax({
			type : 'post', 
			url  : '/shop/?action=addtocart',
			data : { 'choose' : $('#cart_message_gift input:radio:checked').val(), qty: $('form[name="addToCart"] input[name="qty"]').val(),id: $('form[name="addToCart"] input[name="id"]').val()},
			success : function(){
				$('.header_auth_1_2').text($('.header_auth_1_2').text()*1+$('form[name="addToCart"] input[name="qty"]').val()*1);
				$.colorbox({
									inline:true,
									href: '#cart_message',
									opacity: 0.8,
									transition:	'elastic',
									speed: 250,
									title: '',
									onOpen:function(){
										$('#colorbox').addClass('class_cart');
										$('#cboxOverlay').addClass('class_cart');
									},
									onComplete:function(){
										
									},
									onClosed:function(){
										$('#colorbox').removeClass('class_cart');
										$('#cboxOverlay').removeClass('class_cart');
									}
				});
			},
			error :function(){
			//	$('.error_vote').text('Извините. Что-то пошло не так...').show();
			}
	});
	setTimeout(second_passed, 8000);
}
function second_passed() {
	$.colorbox.close();
}

function showChoose(){
				$.colorbox({
									inline:true,
									href: '#cart_message_gift',
									opacity: 0.8,
									transition:	'elastic',
									speed: 250,
									title: '',
									onOpen:function(){
										$('#colorbox').addClass('class_cart');
										$('#cboxOverlay').addClass('class_cart');
									},
									onComplete:function(){
										
									},
									onClosed:function(){
										$('#colorbox').removeClass('class_cart');
										$('#cboxOverlay').removeClass('class_cart');
									}
				});
}
function addtocartfunction_gift(){
	
	$.ajax({
			type : 'post', 
			url  : '/shop/?action=addtocart',
			data : { qty: $('form[name="addToCart"] input[name="qty"]').val(),id: $('form[name="addToCart"] input[name="id"]').val()},
			success : function(){
				$('.header_auth_1_2').text($('.header_auth_1_2').text()*1+$('form[name="addToCart"] input[name="qty"]').val()*1);
				$.colorbox({
									inline:true,
									href: '#cart_message_gift',
									opacity: 0.8,
									transition:	'elastic',
									speed: 250,
									title: '',
									onOpen:function(){
										$('#colorbox').addClass('class_cart');
										$('#cboxOverlay').addClass('class_cart');
									},
									onComplete:function(){
										
									},
									onClosed:function(){
										$('#colorbox').removeClass('class_cart');
										$('#cboxOverlay').removeClass('class_cart');
									}
				});
			},
			error :function(){
			//	$('.error_vote').text('Извините. Что-то пошло не так...').show();
			}
	});
}