$(document).ready(function(){


//		***	 			Validate form					***	 //

//		***	 INIT		***	 //
/* type of required fields:
-3: // selector
-2: // e-mail
-1: //
0: 5..80 // standart
1: 5..30 // phone
2: 20..2000 // textarea
3: 5 //captcha
*/
//var type_of_field=[0, -3, 1, -2, 2];
//var type_of_field=[0, -3, -3, -2, 2, 3];

// if one form
//var type_of_field=[0, -3, -3, -2, 2, 3];
//var name_form_for_validation=" #MyForm8 ";

// if 2 and more forms


var type_of_field=[];
var name_form_for_validation="";


	

if ($("form.usual").hasClass("MyForm8") == 1){

	name_form_for_validation=" .MyForm8 ";
	type_of_field=[0, 1, -2, 2, 3];
	
} else if ($("form.usual").hasClass("MyForm9") == 1){
	//alert("MyForm9")
	
	name_form_for_validation=" .MyForm9 ";
	type_of_field=[0, 0, -2, 0, 3];
} else{
	name_form_for_validation=" .nonenameform ";
}
/*
*/

//		***	 SCRIPT		***	 //
var j=0;
var min_max= [[5, 80], [5, 30], [20, 2000], [5, 5]];

// if 2 and more forms
//var min_max= [[5, 10], [4, 6], [10, 20], [5, 5]];
var rule_correct="Поле заполнено верно.";
var rule_mail="Введите корректный e-mail.";
var rule_select="Выберите вариант ответа.";
var fields_all= name_form_for_validation+" textarea,"+ name_form_for_validation +" select,"+ name_form_for_validation +" input[type=text]";
var fields_required= name_form_for_validation+"input.required,"+ name_form_for_validation +" select.required,"+ name_form_for_validation +" textarea.required";
var field_select_req= name_form_for_validation+"select.required";
var button_name= name_form_for_validation+".submitForm";
//alert(field_select_req)

// set rules for the tooltips
var len_f=$(fields_all).size();
for (var i = 0; i < len_f; i++) { 		
	// hide tooltips for non-required fields
	if ($(fields_all).eq(i).hasClass("required") == 0) {  $(name_form_for_validation+" .arrow_box").eq(i).removeClass("wrong_value"); } else{ $(name_form_for_validation+" .arrow_box").eq(i).addClass("tooltip_sh"); SetRuleInTooltip( j ); j++; }
}


//				EVENTS
// onchange text fields
if ($.support.leadingWhitespace) {
	// if IE9+, other browsers // used on
	$(fields_required).on(fields_required, function (e) { ValidateField( $(fields_required).index(this) ); });
} else{ // if IE7 or IE8 // used keyup
	$(fields_required).keyup(function () { ValidateField( $(fields_required).index(this) ); });
}//EO else	

// onchange select ( for IE7, IE8 )
$(field_select_req).change(function () { ValidateSelect( $(field_select_req).index(this) ); });

// submit
$(button_name).click(function () {
	
	// validate fields
	var len_r = $(fields_required).size();
	var j=0;
	for (var i = 0; i < len_r; i++) { 
		if ( $(fields_required).eq(i).find('option').size() == 0 ) { ValidateField( i ); } 
		else{ ValidateSelect( j ); j++; }
	}
	
	// show tooltips
	for (var i = 0; i < len_f; i++) { 
		ShowTooltipIfNeeded(i);			
	}


	if ($(name_form_for_validation+".arrow_box").hasClass("wrong_value") == 1 ){ return false; } 
	else{ return true; }
/*
*/
	
});
	
//				FUNCTIONS
// Validate text field
function ValidateField(index){
	var len = $(fields_required).eq(index).val().length;
	
	// if text type field
	if ( type_of_field[index] > -1 && type_of_field[index] < 4){
		if ( min_max [type_of_field[index] ][0] <=len && min_max [type_of_field[index] ][1] >=len ){ FieldIsValid(fields_required, index); } 
		else{ FieldIsNotValid(index);}
	} 
	// if e-mail field
	else if (type_of_field[index] == -2) {
		if ( IsEmail( $(fields_required).eq(index).val() )  == 1){ FieldIsValid(fields_required, index); } 
		else{ FieldIsNotValid(index); }
	}
	ShowCurrentTooltip(fields_required, index);
}
// Validate select
function ValidateSelect(index){
	FieldIsValid(field_select_req, index);
	/*
	if ( $(field_select_req).eq(index).prop("selectedIndex") > 0 ){ FieldIsValid(field_select_req, index); } 
	else{ $(field_select_req).eq(index).prevAll(".arrow_box").addClass("wrong_value"); $(field_select_req).eq(index).prevAll(".arrow_box").html(rule_select);}				
	ShowCurrentTooltip(field_select_req, index);
	*/
}

function ShowTooltipIfNeeded(index){
	if ($(fields_all).eq(index).hasClass("required") == 1){ $(name_form_for_validation+".arrow_box").eq(index).fadeIn(); }		
}

function IsEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9])+$/;
	return regex.test(email);
}

function SetRuleInTooltip(i){
	
	if (type_of_field[i]==0){
		$(name_form_for_validation+".arrow_box.tooltip_sh").eq(i).html("Введите от "+ min_max[0][0] +" до "+ min_max[0][1] +" символов.");
	}
	else if (type_of_field[i]==1){
		$(name_form_for_validation+".arrow_box.tooltip_sh").eq(i).html("Введите от "+ min_max[1][0] +" до "+ min_max[1][1] +" символов.");
	}
	else if (type_of_field[i]==2){
		$(name_form_for_validation+".arrow_box.tooltip_sh").eq(i).html("Введите от "+ min_max[2][0] +" до "+ min_max[2][1] +" символов.");
	}
	else if (type_of_field[i]==3){		
		$(name_form_for_validation+".arrow_box.tooltip_sh").eq(i).html("Введите от "+ min_max[3][0] +" до "+ min_max[3][1] +" символов.");
	}
	else if (type_of_field[i]==-2){
		$(name_form_for_validation+".arrow_box.tooltip_sh").eq(i).html(rule_mail);
	}
	else if (type_of_field[i]==-3){
		$(name_form_for_validation+".arrow_box.tooltip_sh").eq(i).html(rule_select);
	}
}

function FieldIsValid(class_name, index){
	$(class_name).eq(index).prevAll(name_form_for_validation+".arrow_box").removeClass("wrong_value");
	$(class_name).eq(index).prevAll(name_form_for_validation+".arrow_box").html(rule_correct);
}
function FieldIsNotValid(index){
	$(fields_required).eq(index).prevAll(name_form_for_validation+".arrow_box").addClass("wrong_value");
	SetRuleInTooltip(index);
}
function ShowCurrentTooltip(class_name, index){
	$(class_name).eq(index).prevAll(name_form_for_validation+".arrow_box").fadeIn();
}

//		***	 			EO Validate form					***	 //

	
});