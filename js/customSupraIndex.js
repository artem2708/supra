	/* horizontal scrollbar */
		(function($){
			$(window).load(function(){
				/* init scrollbar */

				$(".content_6").mCustomScrollbar({
					horizontalScroll:true,
					scrollButtons:{
						enable:true
					},
					advanced:{
						autoExpandHorizontalScroll:true
					}
				});

				
				
				
				/*
				custom functions
				*/
				
				var index_mcs_t_5=0;
				var index_mcs_t_5_next=4;
				var index_mcs_change_right=1;
				var index_mcs_change_left=0;
				var index_mcs_length=$(".hz_item").size();				

				
				$(".index_horiz_ra").click(function(e){
					
					index_mcs_t_5=SetVeiwingBlock();
					
					if (index_mcs_t_5+index_mcs_t_5_next<index_mcs_length){
						index_mcs_t_5+=index_mcs_t_5_next;
					}

					$(".hz_item").removeClass("mcs_t_5")				
					$(".hz_item").eq(index_mcs_t_5).addClass("mcs_t_5")
					e.preventDefault();
					$(".content_6").mCustomScrollbar("scrollTo",".mcs_t_5");
				});
				
				
				$(".index_horiz_la").click(function(e){		
				
					index_mcs_t_5=SetVeiwingBlock();

					if (index_mcs_t_5>=index_mcs_t_5_next){
						index_mcs_t_5-=index_mcs_t_5_next;
					} else{
						index_mcs_t_5=0;
					}

					$(".hz_item").removeClass("mcs_t_5")
					$(".hz_item").eq(index_mcs_t_5).addClass("mcs_t_5")
					
					e.preventDefault();
					$(".content_6").mCustomScrollbar("scrollTo",".mcs_t_5");

					
				});
			});
			
			function SetVeiwingBlock(){
				var index_mcs_section=33;
				
				// get value of current "left" attribute
				var css_left_str=$(".mCSB_dragger").css("left")
				// perform str to int
				var css_left=css_left_str.substr(0, css_left_str.length-2);
				css_left=parseInt(css_left)
					
				// length of block in slider is 63px
				sl_ind=css_left/index_mcs_section;
					
				$(".hz_item").removeClass("mcs_t_5")
				// define viewing block
				if (sl_ind%index_mcs_section!=0){
					sl_ind=Math.floor(sl_ind)
					//$(".hz_item").eq(sl_ind+1).addClass("mcs_t_5")
					sl_ind+=1;
				} else{
					//$(".hz_item").eq(sl_ind).addClass("mcs_t_5")					
				}
				
				return sl_ind;
			}
			
		})(jQuery);
	
	//
  $(document).ready(function(){
	
	// menu on catalog page
    $(".inlinks").click(function () {
      var index = $(".inlinks").index(this);
	  
	  if ($(".inlinks").eq(index).hasClass("plus")==1){
		var curclass=".inlinks"+index;
		// show hidden menu
		$("ul"+curclass).fadeIn(200);
		
		$(curclass).removeClass("plus")
		$(curclass).addClass("minus")
		return false;
	  } else{
				if ($(".inlinks").eq(index).hasClass("minus")==1){
					var curclass=".inlinks"+index;

					$("ul"+curclass).fadeOut(50);
					
					$(curclass).removeClass("minus")
					$(curclass).addClass("plus")	  
			  
					return false;
				}
			}
	  
	  return true;
    });
	
  });