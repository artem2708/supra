  $(document).ready(function(){

  	// if cookie not exist, set cookie, call the pop-up
	if ($.cookie('supratown') == null){ 
		$('#header_where_a').text('Москва');
	  $.cookie('supratown', 'Москва', { expires: 60, path: '/' });
	  if($('.not_popup').size()>0){return false;}
	  if(!$('.poll_pop').length){
          Call_pop_up_towns_pop_up();
      }
	} else {
	   if($(".pop_up_social").size()){
	       if ($.cookie('social') == null){ 
          $.cookie('social', 'social_user', { expires: 7, path: '/' }); 
 //         Call_pop_up_social();
        }
	   }
	   
	} 
    if($.cookie('suprapoll') == null && $('.poll_pop').length){
        $.cookie('suprapoll', '1', { expires: 60, path: '/' });
        Call_pop_up_poll_pop_up();

    }
    /*
    
    $(window).load(function(){
        Call_pop_up_social();
    });*/
	
  
	$(".up_search_town input").attr("value", $.cookie('supratown'))
  
	// set 1 if town one of 9 in list of .up_search_link
	var flag=0;
	var town_from_cookie = $.cookie('supratown').toLowerCase();

	// towns in lowercase (only for comparing)
	var mas_towns=['москва','санкт-петербург','ростов-на-дону','екатеринбург','казань','нижний новгород','минск','новосибирск','челябинск','могилев','брест', 'витебск', 'петропавловск-Камчатский'];
	// towns for content
	var mas_towns_content=['Москва','Санкт-Петербург','Ростов-на-Дону','Екатеринбург','Казань','Нижний Новгород','Минск','Новосибирск','Челябинск','Могилев','Брест', 'Витебск', 'Петропавловск-Камчатский'];

	///////////////////////////////////////////////
	///////////////////////////////////////////////
	///////////////////////////////////////////////
	
	
	// get current town
	//var town_from_cookie=$(".header_where a").html().toLowerCase();	
	//var town_from_cookie="ЯРОславль".toLowerCase();

	

	// search town in 9 items, if exist, set this word in the end of massive [mas_towns_content]
	SearchStringInArrayReplace (town_from_cookie, mas_towns, mas_towns_content);
	if (flag==1){
		SetNewArrayOrderInDivs (mas_towns_content, mas_towns_content[12]);
	}	else{
		if ( SearchStringInArray(town_from_cookie)!= -1 ){
			SetNewArrayOrderInDivs (mas_towns_content, $.cookie('supratown'));
		} else{
			SetNewArrayOrderInDivs (mas_towns_content, "Некорректные данные");
		}
		
	}
	
	//alert(mas_towns_content)

	//							Events
	// button "Verno"
	$(".pop_up_towns_btn").click(function(){	   
		var selected_town=$(".up_search_town input").val().toLowerCase();


		// delete cookie, set new cookie, if choosed new town
		if (selected_town != town_from_cookie ){			
			// set cookie if town exist, set new cookie. if doesn't exist, save the cookie.
			if ( SearchStringInArray(selected_town)!= -1 ){
				selected_town=availableTags[SearchStringInArray(selected_town)];
				
				CookiesRemoveOldSetNew(selected_town);
				return true;
			} else{
				alert("Пожалуйста, выберетие город из предложенного списка.")
			}
		}

		$(".pop_up_towns_bg").fadeOut()
		$(".pop_up_towns").fadeOut()
        if($(".pop_up_social").size()){
	       if ($.cookie('social') == null){ 
          $.cookie('social', 'social_user', { expires: 7, path: '/' }); 
//          Call_pop_up_social();
        }
	   }
	
	return false;
	   
	});
	
	
	
	/*
	
	$.fn.enterKey = function (fnc) {
		return this.each(function () {
			$(this).keypress(function (ev) {
				var keycode = (ev.keyCode ? ev.keyCode : ev.which);
				if (keycode == '13') {
					fnc.call(this, ev);
				}
			})
		})
	}
	
	
	$("#ui-id-1").live("enterKey",function () {
		alert('Enter!');
	})
	*/
	
	$("#ui-id-1").live("keyup",function (e) {
		if (e.keyCode == 13) {
			// Do something
		}
	});
	
	// if clicked at one link of 9 in list
    $(".up_search_link a").click(function () {
		var name_of_city=$(".up_search_link a span").eq( $(".up_search_link a").index(this) ).html();
		CookiesRemoveOldSetNew(name_of_city)
    });


	
	
	//							Functions
	//
	function SearchStringInArrayReplace (str, strArray, mas_towns_content) {
				
		var length=strArray.length;
		var selected_city="";
		
		for (var j=0; j<length; j++) {	

			if (strArray[j].match(str)) {
				flag=1;
				selected_city=mas_towns_content[j];
				length--;				
			}
			
			if (flag==1){
				mas_towns_content[j]=mas_towns_content[j+1];
			}
		}

		if (flag==1){				
			mas_towns_content[length]=selected_city;
		}
		
		return 0;
	}	
	
	//
	function SearchStringInArray (str) {
		for (var j=0; j<availableTags.length; j++) {
			if (availableTags[j].toLowerCase().match(str)) return j;
		}
		return -1;
	}	
	
	//
	function SetNewArrayOrderInDivs (mas_towns_content, firstTown){
		for (var j=0; j<mas_towns_content.length-1; j++) {
			var k=j+1;
			$(".up_search_link"+ k +" a span").html(mas_towns_content[j]);
		}
		// insert choosed town in first div
		$(".up_search_link"+ 0 +" a span").html( firstTown );	
	}
	
	
	
	function CookiesRemoveOldSetNew(city){
			$.removeCookie("supratown");
			$.cookie('supratown', city, { expires: 60, path: '/' });	
	}

	


	//							Pop-up
	//
	

	
    $(".header_where a").click(function () {
	//	if($(this).hasClass('not_popup')) return false;
		Call_pop_up_towns_pop_up();
		
	  return false;
    });
	
	$(".header_where_icon").click(function () {
	//	if($(this).hasClass('not_popup')) return false;
		Call_pop_up_towns_pop_up();	
		
	  return false;
    });
	
	$(".where_buy_switch_town a").click(function () {
	
		Call_pop_up_towns_pop_up();	
		
	  return false;
    });
	
	function Call_pop_up_towns_pop_up(){
		$(".pop_up_towns_bg").css("height", $(document).height() )
		$(".pop_up_towns_bg").css("width", $(document).width() )
		$(".pop_up_towns_bg").fadeIn()		
		$(".pop_up_towns").fadeIn()	
	}
      $(".pop_up_poll .pop_up_towns_close").click(function () {
          $(".pop_up_towns_bg").fadeOut();
          $(".pop_up_poll").fadeOut();
          return false;
      });
      function Call_pop_up_poll_pop_up(){
          $(".pop_up_towns_bg").css("height", $(document).height() )
          $(".pop_up_towns_bg").css("width", $(document).width() )
          $(".pop_up_towns_bg").fadeIn()
          $(".pop_up_poll").fadeIn()
      }
	$(".pop_up_towns .pop_up_towns_close").click(function () {	
		$(".pop_up_towns_bg").fadeOut();
		$(".pop_up_towns").fadeOut();
        if($(".pop_up_social").size()){
	       if ($.cookie('social') == null){ 
          $.cookie('social', 'social_user', { expires: 7, path: '/' }); 
 //         Call_pop_up_social();
        }
	   }
	  return false;	
	});
    
    function Call_pop_up_social(){
		$(".pop_up_towns_bg").css("height", $(document).height() )
		$(".pop_up_towns_bg").css("width", $(document).width() )
		$(".pop_up_towns_bg").fadeIn()		
		$(".pop_up_social").fadeIn()	
	}
    
    $(".pop_up_social .pop_up_towns_close").click(function () {	
		$(".pop_up_towns_bg").fadeOut()
		$(".pop_up_social").fadeOut()	
	  return false;	
	});
    
	
	//							EO Pop-up

  });
  
    var availableTags = [
	
		"Абакан",
		"Амурск",
		"Арзамас",
		"Армавир",
		"Архангельск",
		"Астрахань",
		"Ачинск",
		"Барнаул",
		"Белгород",
		"Белово",
		"Белогорск",
		"Березники",
		"Биробиджан",
		"Братск",
		"Брянск",
		"Великий Новгород",
		"Владивосток",
		"Владикавказ",
		"Владимир",
		"Волгоград",
		"Вологда",
		"Воронеж",
		"Горно-Алтайск",
		"Дзержинск",
		"Димитровград",
		"Екатеринбург",
		"Иваново",
		"Ижевск",
		"Иркутск",
		"Ишим",
		"Йошкар-Ола",
		"Казань",
		"Калининград",
		"Калуга",
		"Каменск-Уральский",
		"Каменск-Шахтинский",
		"Кемерово",
		"Кинешма",
		"Киров",
		"Коми",
		"Кореновск",
		"Кострома",
		"Краснодар",
		"Краснотурьинск",
		"Красноярск",
		"Курган",
		"Курск",
		"Липецк",
		"Магнитогорск",
		"Махачкала",
		"Минск",
		"Москва",
		"Мурманск",
		"Набережные Челны",
		"Нарьян-Мар",
		"Нижнекамск",
		"Нижний Новгород",
		"Новокузнецк",
		"Новомоссковск",
		"Новороссийск",
		"Новосибирск",
		"Обнинск",
		"Омск",
		"Оренбург",
		"Орел",
		"Орск",
		"Пенза",
		"Пермь",
		"Петрозаводск",
		"Псков",
		"Пятигорск",
		"Ростов-на-Дону",
		"Рязань",
		"Самара",
		"Санкт-Петербург",
		"Саранск",
		"Саратов",
		"Северодвинск",
		"Смоленск",
		"Ставрополь",
		"Старый Оскол",
		"Сургут",
		"Сызрань",
		"Сыктывкар",
		"Таганрог",
		"Тамбов",
		"Тверь",
		"Тольятти",
		"Томск",
		"Тула",
		"Тюмень",
		"Улан-Удэ",
		"Ульяновск",
		"Уфа",
		"Хабаровск",
		"Ханты-Мансийск",
		"Чебоксары",
		"Челябинск",
		"Черемхово",
		"Череповец",
		"Чита",
		"Шахты",
		"Элиста",
		"Якутск",
		"Ярославль",
        "Могилев",
        "Брест",
        "Витебск",
        "Петропавловск-Камчатский"
	  
    ];  
  
  $(function() {
    $( "#tags" ).autocomplete({
      source: availableTags,
      		    	select: function( event, ui ) {
			        		CookiesRemoveOldSetNew( selected_town=$(".up_search_town input").val() );
							location.reload();
				}
    });
  });