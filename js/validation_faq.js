  $(document).ready(function(){

	$("#form_fields1").keyup(function () {
		field1()
	});
	
	$("#form_fields3").keyup(function () {
		field3()
	});
  

	$("#form_fields4").keyup(function () {
		field4()
	});  

	$("#captcha").keyup(function () {
		field5()
	});  	
  
  
    $(".submitForm").click(function () { 
		
		var key1=field1()
		var key3=field3()
		var key4=field4()
		var key5=field5()
		
		if (key1==1 && key3==1 && key4==1 && key5==1){
			return true;
		} else{	
			return false;
		}
		
		
    });
	
	
	/*				*/
	
	/*	**	*/
	
	function field1(){
	
		var vfield="#form_fields_1";
		var per1=$("#form_fields1").attr("value")

		if (per1.length < 3 || per1.length > 80){
			$(vfield).attr("value","Введите от 3 до 80 букв.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")		
		} else{
			$(vfield).attr("value","Поле заполнено верно.")
			$(vfield).removeClass("incorrectfield")
			$(vfield).addClass("correctfield")
			return 1;
		}	
	
	}
	
	/*	**	*/
	
	function field3(){
		var vfield="#form_fields_3";
		var per1=$("#form_fields3").attr("value")

		if (per1.length > 0){
			per=IsEmail(per1)
			if (per==0){
			$(vfield).attr("value","Введите корректный е-мейл.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")
			}else{
			$(vfield).attr("value","Поле заполнено верно.")
			$(vfield).removeClass("incorrectfield")
			$(vfield).addClass("correctfield")
			return 1;
			}
		} 	else{		
			$(vfield).attr("value","Введите е-мейл.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")
		}	
	}
	
	/*	**	*/
	
	function field4(){	
		var vfield="#form_fields_4";
		var per1=$("#form_fields4").attr("value")

		if (per1.length < 10 || per1.length > 800){
			$(vfield).attr("value","Введите сообщение от 10 до 800 символов.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")
		} 		
		else{
			$(vfield).attr("value","Поле заполнено верно.")
			$(vfield).removeClass("incorrectfield")
			$(vfield).addClass("correctfield")
			return 1;
		}
	}
	
	/*	**	*/
	
	function field5(){	
		var vfield="#form_fields_5";
		var per1=$("#captcha").attr("value")
		if (per1.length != 3){
			$(vfield).attr("value","Введите капчу.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")		
		} else{
			$(vfield).attr("value","Поле заполнено верно.")
			$(vfield).removeClass("incorrectfield")
			$(vfield).addClass("correctfield")
			return 1;
		}	
		
	}
	
	function IsEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9])+$/;
	return regex.test(email);
	}

  });