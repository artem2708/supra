$(document).ready(function(){
    $('a.lightbox_a').lightBox({
        imageLoading: '/i/lightbox/loading.gif',
	    imageBtnClose: '/i/lightbox/close.gif',
	    imageBtnPrev: '/i/lightbox/prev.gif',
	    imageBtnNext: '/i/lightbox/next.gif',
        txtImage: 'Изображение',
	    txtOf: 'из'
    });
})
