$(function(){
    $("div.abo-tree").each(function() {
        $(this).click(function(event) {
            event = event || window.event;
            var target = event.target || event.srcElement;        
            if ('DT' == target.tagName.toUpperCase()) {
                if (target.parentNode.getElementsByTagName('DL')) {
                    var p = $(target.parentNode);
                    if (p.hasClass('open')) {
                        p.removeClass('open');
                        p.addClass('close');
                    } else {                    
                        p.removeClass('close');
                        p.addClass('open');
                    }
                }
            }
        });
    });
});