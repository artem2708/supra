var prop_number		= 9;				//	(action + 5 properties + linked_page... from 0 to 8)
var lp				= prop_number - 3;	// индекс элемента соответствующего linked_page в массиве prop (последний элемент)
var	material_url	= '';
var	material_id		= '';
var link			= '';
var name			= '';
var field_id		= 0;
var block_id		= 0;
var page_id = 0;
var gPagesField = false;

function selectElm(what, id) {
	unSelect(what);

	sel_elm = what + '_' + id;
	if (document.getElementById(sel_elm) != null) {
		e = document.getElementById(sel_elm);
		if (what == 'module') {
			e.className = "head1";
		} else if (what == 'field') {
			e.className = "head1";
		}
		e.title = ""
		eval("selected_" + what + " = " + id + ";");
	}
	return true;
}


function unSelect(what) {
	var prev;
	if (what == 'field') {
		prev = 'field_' + selected_field;
	} else if (what == 'module') {
		prev = 'module_' + selected_module;
	}

	if (document.getElementById(prev) != null) {
		e = document.getElementById(prev);
		if (what == 'module') {
			e.className = "head2";
			e.title = gMsg.Click_to_select_module;
		} else if (what == 'field') {
			e.className = "head";
			e.title = gMsg.Click_to_select+" " + selected_field + " "+gMsg.field+".";
		}
	}
	return true;
}


function onImgs(f) {
	last_pos = (ids[f].length == 0) ? 0 : ids[f].length - 1; // кол-во элементов

	if (ids[f][1] != null) {
		var img=eval("document.getElementById('up_img_" + f + "_" + ids[f][1] + "')");
		img.src = img_up_url;
		img.style.cursor = "hand";
	}

	if (ids[f][last_pos] != null) {
		var img=eval("document.getElementById('down_img_" + f + "_" + ids[f][last_pos] + "')");
		img.src = img_down_url;
		img.style.cursor = "hand";
	}

	return true;
}


function offImgs(f) {
	last_pos = (ids[f].length == 0) ? 0 : ids[f].length - 1; // кол-во элементов

	if (ids[f][1] != null) {
		var img=eval("document.getElementById('up_img_" + f + "_" + ids[f][1] + "')");
		img.src = img_up_na_url;
		img.style.cursor = "";
	}

	if (ids[f][last_pos] != null) {
		var img=eval("document.getElementById('down_img_" + f + "_" + ids[f][last_pos] + "')");
		img.src = img_down_na_url;
		img.style.cursor = "";
	}

	return true;
}


function up(f, id) {
	// id - id блока
	last_pos = (ids[f].length == 0) ? 0 : ids[f].length - 1; // кол-во элементов

	// на какой позиции стоит элемент с указанным id
	pos_from=pos[f][id]; // переходим c места
	if(pos_from==1) return;
	onImgs(f);

	// меняем местами элементы в массивах
	id_to=ids[f][pos_from-1]; // кто на месте куда мы целим

	pos[f][id_to]=pos_from;
	pos[f][id]=pos_from-1;

	ids[f][pos_from-1]=id;
	ids[f][pos_from]=id_to;

	var arg1=document.getElementById('row_'+f+'_'+id_to);
	var arg2=document.getElementById('row_'+f+'_'+id);

	if (arg1 != null && arg2 != null) {
		arg2.parentNode.insertBefore(arg2,arg1);
	}
	offImgs(f);

	return true;
}


function down(f, id) {
	// id - id блока
	 last_pos = (ids[f].length == 0) ? 0 : ids[f].length - 1; // кол-во элементов

	// на какой позиции стоит элемент с указанным id
	pos_from=pos[f][id]; // переходим c места
	if(pos_from==last_pos) return;
	onImgs(f);

	 // меняем местами элементы в массивах
	id_to=ids[f][pos_from+1]; // кто на месте куда мы целим

	pos[f][id_to]=pos_from;
	pos[f][id]=pos_from+1;

	ids[f][pos_from+1]=id;
	ids[f][pos_from]=id_to;

	var arg1=document.getElementById('row_'+f+'_'+id_to);
	var arg2=document.getElementById('row_'+f+'_'+id);

	if (arg1 != null && arg2 != null) {
		arg1.parentNode.insertBefore(arg1,arg2);
	}
	offImgs(f);

	return true;
}


function remove(f, id) {
	if (confirm(gMsg.Confirm_delete_this_block)) {
		last_pos = (ids[f].length == 0) ? 0 : ids[f].length - 1; // кол-во элементов
		y = pos[f][id]; // на каком месте удаляемый элемент

		// механизм удаления блока такой: смещаем его в самый низ,
		// последовательно перебирая все нижележащие элементы и вызывая функцию down,
		// потом удаляем из html-кода и массивов
		while(y < last_pos) {
			down(f, ids[f][y]);
			y++;
		}

		// 'true' possible value specifies removal of childNodes also
		var remove_elm = document.getElementById('row_'+f+'_'+id);
		if (remove_elm != null) {
			//remove_elm.removeNode(true);
			remove_elm.parentNode.removeChild(remove_elm);
			ids[f].pop();
			pos[f][id] = null;
			mod[f][id] = null;
			prop[f][id] = null;
			offImgs(f);
			if (ids[f].length <= 1) mmod_is_selected = 0;
		}
	}
	return true;
}

function stopError() {
	return true;
}

function showPropertiesDiv(url, f, id, prop) {
	properties_div.innerHTML = "<div align=center><h6>Loading...</h6></div>";
	properties_div.style.display = "block";
	block_prop = prop;
	/* Create new JsHttpRequest object. */
	var req = new JsHttpRequest();
	/* Code automatically called on load finishing. */
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			/* Write result to page element. */
			properties_div.innerHTML = req.responseText;
			if (winLoaded()) {
				getActionPrp(f);
			} else {
				processResult(field_id, block_id, block_prop);
			}
		}
	};
	/* Prepare request object (automatically choose GET or POST). */
	req.open("GET", url, true);
	/* Send data to backend. */
	req.send();
}

function processResult(f, id, ret) {
	if (!prop[f]) {
		prop[f] = [];					
	}
	if (!prop[f][id]) {
		prop[f][id] = [];
	}	
	if (typeof(ret) == 'object' && ret[9]) {
		url_2 = ret[9] + '&field=' + f;
		prop[f][id].splice(9, (prop[f][id].length - 9));
		showPropertiesDiv(url_2, f, id, prop[f][id]);
		return true;
	}
	properties_div.style.display = "none";	
	
	ret.splice(9, (prop[f][id].length - 9));
	if (ret != null) {
		for (var i = 0; i < ret.length; i++) {
			if (ret[i] != null) {				
				prop[f][id][i] = ret[i];
			}
		}
		if (prop[f][id].length > 9) {
			prop[f][id].splice(9, (prop[f][id].length - 9));
		}
	}
	if (gPagesField) {
		gPagesField[page_id][id].action = prop[f][id][0];
		gPagesField[page_id][id].property1 = prop[f][id][1];
		gPagesField[page_id][id].property2 = prop[f][id][2];
		gPagesField[page_id][id].property3 = prop[f][id][3];
		gPagesField[page_id][id].property4 = prop[f][id][4];
		gPagesField[page_id][id].property5 = prop[f][id][5];
		gPagesField[page_id][id].site_id = prop[f][id][7];
		gPagesField[page_id][id].lang_id = prop[f][id][8];
		gPagesField[page_id][id].tpl = tpl[f][id] ? tpl[f][id] : '';
		gPagesField[page_id][id].linked_page = $('#linked_page').val();
		
	}
	redrawElement(f, id);
	return true;
}

function properites(f, id) {
	window.onerror = stopError;
	sel_module = mod[f][id];
	name = mod_names[sel_module];
	field_id = f;
	block_id = id;
	url = '/admin.php?name=' + name + '&action=properties&field=' + f + '&lang=' + language + '&block_id=' + id;
	showPropertiesDiv(url, f, id, prop[f][id]);
}


function addElement(f) {
	if (f == null) return;
	if (f == 0 && mmod_is_selected == 1) {
		alert(gMsg.Alert_main_block_single);
		return false;
	}

	if (selected_module) {
		if (f != 0 && mod_as_main[selected_module] == 1) {
			alert(gMsg.Alert_main_block_module);
			return false;
		}
		last_pos = (ids[f].length == 0) ? 0 : ids[f].length - 1; // кол-во элементов
		onImgs(f);

		max_id = max_id + 1
		new_elm_id = max_id;
		last_pos = last_pos+1;

		// задаем значения в массивах для нового элемента
		ids[f][last_pos] = new_elm_id;
		pos[f][new_elm_id] = last_pos;
		mod[f][new_elm_id] = selected_module;
		prop[f][new_elm_id] = new Array();

		if (f == 0) mmod_is_selected = 1;
		drawElement(f, last_pos);
		offImgs(f);
	}
	return true;
}


function drawFields() {
	for (f = (main_module) ? 0 : 1; f<=number_of_fields; f++) {
		drawField(f);
		offImgs(f);
	}
	return true;
}


function drawField(f) {
	last_pos = (ids[f].length == 0) ? 0 : ids[f].length - 1; // кол-во элементов

	for (i=1; i<=last_pos; i++) {
		if (ids[f][i] && ids[f][i] > max_id) max_id = ids[f][i];
		if (ids[f][i] != null) {
			drawElement(f, i);
			if (f == 0) mmod_is_selected = 1;
		}
	}

	return true;
}


function drawForms() {
	for (f = (main_module) ? 0 : 1; f<=number_of_fields; f++) {
		drawForm(f);
	}
	return true;
}


function drawForm(f) {
	last_pos = (ids[f].length == 0) ? 0 : ids[f].length - 1; // кол-во элементов

	for (var i=1; i<=last_pos; i++) {
		var id = ids[f][i];
		var row = document.getElementById("array_buffer");
		var forms;
		if (prop[f][id][0]) { /* Если действие определено */
		    var k, y, forms = '<input type="hidden" name="fields_blocks[' + f + '][' + id + ']" value="' + mod[f][id] + '">\n'
    		+ '<input type="hidden" name="block_properties[' + f + '][' + id + '][tpl]" value="' + (tpl[f][id] ? tpl[f][id] : '') + '">\n';
    
    		if (prop[f][id] != null) {
    			for (y=0; y<prop[f][id].length; y++) {
    				if (prop[f][id][y] != null) {
    				    if ('object' == typeof prop[f][id][y]) {
    				        for (k in prop[f][id][y]) {
    				            forms += '<input type="hidden" name="block_properties[' + f + '][' + id + '][' + y + '][' + k + ']" value="' + prop[f][id][y][k] + '">\n';
    				        }
    				    } else {
    				        forms += '<input type="hidden" name="block_properties[' + f + '][' + id + '][' + y + ']" value="' + prop[f][id][y] + '">\n';
    				    }					
    				}
    			}
    		}
    		if (row != null) row.innerHTML = row.innerHTML + forms;
		}		
	}
	return true;
}


function drawElement(f, i) {
// i - это индекс в массиве, где значение элементов массива есть id
	var id = ids[f][i];
	var id_elm = 'row_' + f + '_' + id;
	var id_img = 'img_' + f + '_' + id;

	module_id = mod[f][id];
	action = prop[f][id][0];
	pdescr = (module_actions[module_id] && module_actions[module_id][action]) ? module_actions[module_id][action] : '';
	pdescr = ' <span id="pdescr_' + f + '_' + id + '" style="text-transform: lowercase; color: #ABC;"> ' + (pdescr ? '&#151; ' : '') + pdescr + '</span>';

	var img_remove = '<img src="' + img_remove_url + '" alt="'+gMsg.Delete+'" border=0 vspace=1 hspace=1 align=absmiddle  onClick="remove(' + f + ', ' + id + ');" style="cursor: hand">';

	var img_prop = '<img src="' + img_prop_url + '" alt="'+gMsg.Properties+'" border=0 vspace=1 hspace=1 align=absmiddle  onClick="properites(' + f + ', ' + id + ');" style="cursor: hand">';

	var img_up = '<img src="' + img_up_url + '" alt="'+gMsg.U+'" border=0 vspace=1 hspace=1 align=absmiddle id="up_' + id_img + '" onClick="up(' + f + ', ' + id + ');" style="cursor: hand">';

	var img_down = '<img src="' + img_down_url + '" alt="'+gMsg.Down+'" border=0 vspace=1 hspace=1 align=absmiddle id="down_' + id_img + '" onClick="down(' + f + ', ' + id + ');" style="cursor: hand">';

	var new_elm =	'<div id="' + id_elm + '"><table><tr><td><span id=grl></span></td><td width=98%>' + mod_titles[mod[f][id]] + pdescr + '</td><td>' + img_up + img_down + img_prop + img_remove + '</td></tr></table></div>';

	var field = document.getElementById("field" + f); // поле, внутрь которого будем добавлять новые элементы
	field.innerHTML = field.innerHTML + new_elm;

	return true;
}


function redrawElement(f, id) {
	var oPropdescr = document.getElementById('pdescr_' + f + '_' + id);
	if (oPropdescr && mod[f] && mod[f][id]) {
		module_id = mod[f][id];
		action = prop[f][id][0];
		pdescr = (module_actions[module_id] && module_actions[module_id][action]) ? module_actions[module_id][action] : '';
		pdescr = pdescr ? ' &#151; ' + pdescr + '' : '';
		oPropdescr = document.getElementById('pdescr_' + f + '_' + id);
		oPropdescr.innerHTML = pdescr;
	}
	
	return true;
}

function showList() {
	for (f = (main_module) ? 0 : 1; f<=number_of_fields; f++) {
		last_pos = (ids[f].length == 0) ? 0 : ids[f].length - 1; // кол-во элементов
		var order = 'Поле ' + f + ':\n';
		for(i=1;i<=last_pos;i++) {

			order = order +
			' '+gMsg.place+'=' + pos[f][ids[f][i]] +
			' '+gMsg.id+'=' + ids[f][i] +
			' '+gMsg.module+'=' + mod_titles[mod[f][ids[f][i]]] +"\n";

		}
		if (order) alert(order);
	}

	return true;
}

function winLoaded() {
	var el, i, window_url = document.getElementById('window_url'),
		site_target = document.getElementById('site_target');
	link = '&action=properties&name='+name+'&block_id='+block_id;

	if (window_url && window_url.value != '' && window_url.value != site_target.options[site_target.options.selectedIndex].value) {
		block_prop[prop_number] = window_url.value + link;
		return false;
	}
	el = document.getElementById('block_action');
	if (el && block_prop[0] != null && block_prop[0] != '') {
	    for (i=0; i<el.options.length; i++) {
	        if (el.options[i].value == block_prop[0]) {
	            el.options.selectedIndex = i;
	        }
	    }
	}
	el = document.getElementById('linked_page');
	if (el && block_prop[lp] != null && block_prop[lp] != '') {
	    for (i=0; i<el.options.length; i++) {
	        if (el.options[i].value == block_prop[lp]) {
	            el.options.selectedIndex = i;
	        }
	    }
	}
	return true;
}
//test AJAX
function jsUnitSetOnLoad(el, onloadHandler)
{
    var isKonqueror = navigator.userAgent.indexOf('Konqueror/') != -1 ||
                      navigator.userAgent.indexOf('Safari/') != -1;
    if (typeof(el.attachEvent) != 'undefined') {
        el.attachEvent("onload", onloadHandler());
    } else if (typeof(el.addEventListener) != 'undefined' && !isKonqueror) {
        el.addEventListener("load", onloadHandler, false);
    } else if (typeof(el.document.addEventListener) != 'undefined' && !isKonqueror) {
        el.document.addEventListener("load", onloadHandler, false);
    } else if (typeof(el.onload) != 'undefined' && el.onload) {
        el.onload = function() {
            onloadHandler();
        };
    } else {
        el.onload = onloadHandler;
    }
}

function newOnLoadEvent() {
	var d = new Date();
    window.status= 'AJAX LOADED in '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();return true;
}
//
function getActionPrp(field) {
	var target = document.getElementById('target_span');
	var action = document.getElementById('block_action');
	site_target = document.getElementById('site_target');
	site_target_url = site_target.options[site_target.selectedIndex].value;

	var newscript = document.createElement("SCRIPT");
	newscript.src = site_target_url+'&action=block_prp&name='+name+'&sub_action='+action.options[action.selectedIndex].value+'&block_id='+block_id+'&field='+field;
	newscript.type = "text/javascript";
	target.parentNode.appendChild(newscript);
	//jsUnitSetOnLoad(newscript, newOnLoadEvent);
}

function changeAction() {
	for (var l, j, p, i = 1; i < prop_number - 3; i++) {		
		p = document.getElementById('property'+i);
		if (block_prop[i] != null && p != null && block_prop[i] != '') {
		    if ('object' == typeof block_prop[i] || 'array' == typeof block_prop[i]) {
		        l = p.getElementsByTagName('INPUT');
		        if (l) {
		            for (j in l) {
		                if (l[j].name && block_prop[i][l[j].name]) {
		                    l[j].checked = true;
		                }
		            }
		        }
		    } else {		    	
		        if ('input' == p.tagName.toLowerCase()) {
		        	if ('checkbox' == p.type) {
		        		p.checked = true;
		        	} else {
		        		p.value		= block_prop[i];
		        	}    		        
    		    } else if ('select' == p.tagName.toLowerCase()) {
    		        var s = "";
    		        for (j = 0; j<p.options.length; j++) {
    		            if (block_prop[i] == p.options[j].value) {
    		                p.options.selectedIndex = j;
    		            }
    		        }
    		    }
		    }		    
		}
	}
	if (tpl[field_id] && tpl[field_id][block_id]) {
	    p = document.getElementById('slctTpl');
	    if (p) {
	         for (j = 0; j<p.options.length; j++) {
	             if (tpl[field_id][block_id] == p.options[j].value) {
	                 p.options.selectedIndex = j;
	             }
	         }
	    }
	}
}

function btnOkClicked(site) {
	// получить значение свойств
	block_prop = getPropVals();
	block_action = document.getElementById('block_action');
	if (block_action && block_action.value != '') {
		block_prop[0] = block_action.options[block_action.options.selectedIndex].value;
	}
	// получить страницу перехода linked_page
	linked_page = document.getElementById('linked_page');
	block_prop[lp] = linked_page && linked_page.options[linked_page.selectedIndex].value
	    ? linked_page.options[linked_page.selectedIndex].value
	    : '';

	block_prop[prop_number - 2]	= document.getElementById('site_id').value;
	block_prop[prop_number - 1]	= document.getElementById('lang_id').value;
	if ('' != site) {
		block_prop[prop_number] = site;
	}
	block_prop[prop_number + 1] = '';
	p = document.getElementById('slctTpl');
	tpl[field_id][block_id] = '';
	if (p) {
	    if (p.options.selectedIndex > 0) {
	        tpl[field_id][block_id] = p.options[p.options.selectedIndex].value;
	    }
	}
	
	processResult(field_id, block_id, block_prop);
	return false;
}

function btnCancelClicked() {
	properties_div.style.display = "none";
	return false;
}

function gotoURL(site_target) {
	popupWin = window.open("http://"+site_target+material_url+material_id, 'small', 'resizable=yes,scrollbars=yes,location=no,width=800,height=600,top=0');
	popupWin.focus();
}

function setURL(prop_id) {
	var p_id	= document.getElementById(prop_id);
	var id		= document.getElementById('edit_material');

	if (p_id && p_id.options) {
		material_id	= p_id.options[p_id.options.selectedIndex].value;
	} else {
		material_id	= '';
	}

	if (id && (material_id == '' || material_id == 0 || material_id == 'all_cat' || material_id == 'all')) {
		id.style.display = 'none';
	} else if (id) {
		id.style.display = 'block';
	}
}

function changeSiteTarget() {
	var s_id = document.getElementById('site_target');
	btnOkClicked(s_id.options[s_id.options.selectedIndex].value + '&site=change' + link);
}

function getPropVals()
{
	ret = [];
	for (var l, p, i = 1; i < prop_number-3; i++) {
		p = document.getElementById('property'+i);
		ret[i] = '';
		if (p) {
		    if ('input' == p.tagName.toLowerCase()) {
		    	ret[i] = p.value;
		    } else if ('select' == p.tagName.toLowerCase()) {
		    	ret[i] = p.options[p.options.selectedIndex].value;
		    } else if ('fieldset' == p.tagName.toLowerCase()) {
		        l = p.getElementsByTagName('input');
		        if (l) {
		        	ret[i] = {};
		            for (var j=0; j<l.length; j++) {
		                if (l[j].checked) {
		                	ret[i][l[j].name] =  l[j].value;
		                }
		            }
		        }		        
		    }
		}
	}
	
	return ret;
}