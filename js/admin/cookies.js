function setCookie(name, value, expires, path, domain, secure) {
    if (! name) return false;
    var str = name + '=' + encodeURIComponent(value);
    if (expires) str += '; expires=' + expires.toGMTString();
    if (path)    str += '; path=' + path;
    if (domain)  str += '; domain=' + domain;
    if (secure)  str += '; secure';
    document.cookie = str;

    return true;
}
	 
function getCookie(name) {
	var pattern = "(?:; )?" + name + "=([^;]*);?";
	var regexp  = new RegExp(pattern);
	
	return regexp.test(document.cookie) ? decodeURIComponent(RegExp["$1"]) : false;
}

function delCookie(name, path, domain) {
    setCookie(name, null, new Date(0), path, domain);    
    return true;
}