$().ready(function() {
    init_tinymce();
});

function init_tinymce()
{
    $('textarea.tinymce').tinymce({
        
        language : 'ru',
        width: '100%',
        height: '450px',
        
    	// Location of TinyMCE script
    	script_url : '/js/admin/tiny_mce/tiny_mce.js',
    
    	// General options
    	theme : "advanced",
    	plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist,imagemanager,filemanager",
    
    	// Theme options
    	theme_advanced_buttons1 : "newdocument,preview,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect,|,insertlayer,moveforward,movebackward,absolute,|,print,fullscreen",
    	theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,cleanup,code,|,forecolor,backcolor,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|",
    	theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,insertfile,insertimage,image,media,charmap,emotions,advhr,insertdate,inserttime,|,ltr,rtl,|,visualchars,nonbreaking",
    	//theme_advanced_buttons4 : "",
    	theme_advanced_toolbar_location : "top",
    	theme_advanced_toolbar_align : "left",
    	theme_advanced_statusbar_location : "bottom",
    	theme_advanced_resizing : true,
    	theme_advanced_fonts : "Andale Mono=andale mono,times;"+
                "Arial=arial,helvetica,sans-serif;"+
                "Arial Black=arial black,avant garde;"+
                "Book Antiqua=book antiqua,palatino;"+
                "Comic Sans MS=comic sans ms,sans-serif;"+
                "Courier New=courier new,courier;"+
                "Georgia=georgia,palatino;"+
                "Helvetica=helvetica;"+
                "Impact=impact,chicago;"+
                "Symbol=symbol;"+
                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
                //"Terminal=terminal,monaco;"+
                "Times New Roman=times new roman,times;"+
                "Trebuchet MS=trebuchet ms,geneva;"+
                "Verdana=verdana,geneva;"+
                "Webdings=webdings;"+
                "Wingdings=wingdings,zapf dingbats",

    
    	// Example content CSS (should be your site CSS)
    	content_css : "/css/screen.css",
    
    	// Drop lists for link/image/media/template dialogs
    	template_external_list_url : "lists/template_list.js",
    	external_link_list_url : "/admin.php?name=pages&action=showlist_js",
    	external_image_list_url : "lists/image_list.js",
    	media_external_list_url : "lists/media_list.js",
    	document_base_url : '/',
    	relative_urls : false
	});
}