  $(document).ready(function(){

	$("#form_fields1").keyup(function () {
		field1()
	});

	$("#form_fields2").keyup(function () {
		field2()
	});
	
	$("#form_fields3").keyup(function () {
		field3()
	});
  

	$("#form_fields4").keyup(function () {
		field4()
	});
	
	
	$("#captcha").keyup(function () {
		field5()
	});  	

	$("#form_fields6").keyup(function () {
		field6()
	});
	
	$("#form_fields9").keyup(function () {
		field9()
	});
	
	$("#form_fields10").keyup(function () {
		field10()
	});

  
  
    $("#MyForm8 .submitForm").click(function () { 	
		$(".formright.formright_1").html("");
		$(".formright.formright_0").css("display","block");		
		var key1=field1()
		var key2=field2()
		var key3=field3()
		var key4=field4()
		var key5=field5()
		
		if (key1==1 && key2==1 && key3==1 && key4==1 && key5==1){
			return true;
		} else{	
			return false;
		}
    });
	
    $("#MyForm9 .submitForm").click(function () { 
		$(".formright.formright_0").html("");
		$(".formright.formright_1").css("display","block");
		
		var key2=field2()
		var key6=field6()
		var key9=field9()
		var key10=field10()
		var key5=field5()
		
		if ( key2==1 && key6==1 && key9==1 && key10==1 && key5==1 ){
			return true;
		} else{	
			return false;
		}
		
		//return false;
		
    });
	
	/*				*/
	
	/*	**	*/
	
	function field1(){
	
		var vfield="#form_fields_1";
		var per1=$("#form_fields1").attr("value")

		if (per1.length < 3 || per1.length > 80){
			$(vfield).attr("value","Введите от 3 до 80 символов.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")		
		} else{
			$(vfield).attr("value","Поле заполнено верно.")
			$(vfield).removeClass("incorrectfield")
			$(vfield).addClass("correctfield")
			return 1;
		}
	
	}
	
	/*	**	*/
	
	function field2(){
		// different forms, different fields
		if ( $("form#MyForm8").hasClass("usual") == 1 ){		
			var vfield="#form_fields_2";
			var per1=$("#form_fields2").attr("value")

			if (per1.length < 7 || per1.length > 14){
				$(vfield).attr("value","Введите от 7 до 14 символов.")
				$(vfield).removeClass("correctfield")
				$(vfield).addClass("incorrectfield")		
			} else{
				$(vfield).attr("value","Поле заполнено верно.")
				$(vfield).removeClass("incorrectfield")
				$(vfield).addClass("correctfield")
				return 1;
			}
		} else if ( $("form#MyForm9").hasClass("usual") == 1 ){		
			var vfield="#form_fields_2";
			var per1=$("#form_fields2").attr("value")

			if (per1.length < 4 || per1.length > 30){
				$(vfield).attr("value","Введите от 4 до 30 символов.")
				$(vfield).removeClass("correctfield")
				$(vfield).addClass("incorrectfield")		
			} else{
				$(vfield).attr("value","Поле заполнено верно.")
				$(vfield).removeClass("incorrectfield")
				$(vfield).addClass("correctfield")
				return 1;
			}
		}
	
	}
	
	/*	**	*/
	
	function field3(){
		var vfield="#form_fields_3";
		var per1=$("#form_fields3").attr("value")

		if (per1.length > 0){
			per=IsEmail(per1)
			if (per==0){
			$(vfield).attr("value","Введите корректный е-мейл.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")
			}else{
			$(vfield).attr("value","Поле заполнено верно.")
			$(vfield).removeClass("incorrectfield")
			$(vfield).addClass("correctfield")
			return 1;
			}
		} 	else{		
			$(vfield).attr("value","Введите е-мейл.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")
		}
	}
	
	/*	**	*/
	
	function field4(){	
		var vfield="#form_fields_4";
		var per1=$("#form_fields4").attr("value")

		if (per1.length < 10 || per1.length > 800){
			$(vfield).attr("value","Введите сообщение от 10 до 800 символов.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")
		} 		
		else{
			$(vfield).attr("value","Поле заполнено верно.")
			$(vfield).removeClass("incorrectfield")
			$(vfield).addClass("correctfield")
			return 1;
		}
	}
	
	/*	**	*/
	
	function field5(){	
		var vfield="#form_fields_5";
		var per1=$("#captcha").attr("value")
		if (per1.length != 5){
			$(vfield).attr("value","Введите капчу.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")		
		} else{
			$(vfield).attr("value","Поле заполнено верно.")
			$(vfield).removeClass("incorrectfield")
			$(vfield).addClass("correctfield")
			return 1;
		}	
		
	}
	
	/*	**	*/
	
	function field6(){
	
		var vfield="#form_fields_6";
		var per1=$("#form_fields6").attr("value")

		if (per1.length < 3 || per1.length > 80){
			$(vfield).attr("value","Введите от 3 до 80 символов.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")		
		} else{
			$(vfield).attr("value","Поле заполнено верно.")
			$(vfield).removeClass("incorrectfield")
			$(vfield).addClass("correctfield")
			return 1;
		}
	
	}	
	
	/*	**	*/
	
	function field9(){	
		var vfield="#form_fields_9";
		var per1=$("#form_fields9").attr("value")

		if (per1.length > 0){
			per=IsEmail(per1)
			if (per==0){
			$(vfield).attr("value","Введите корректный е-мейл.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")
			}else{
			$(vfield).attr("value","Поле заполнено верно.")
			$(vfield).removeClass("incorrectfield")
			$(vfield).addClass("correctfield")
			return 1;
			}
		} 	else{		
			$(vfield).attr("value","Введите е-мейл.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")
		}
		
	}
	
	/*	**	*/
	
	function field10(){
	
		var vfield="#form_fields_10";
		var per1=$("#form_fields10").attr("value")

		if (per1.length < 5 || per1.length > 80){
			$(vfield).attr("value","Введите от 5 до 80 символов.")
			$(vfield).removeClass("correctfield")
			$(vfield).addClass("incorrectfield")		
		} else{
			$(vfield).attr("value","Поле заполнено верно.")
			$(vfield).removeClass("incorrectfield")
			$(vfield).addClass("correctfield")
			return 1;
		}
	
	}	
	
	function IsEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9])+$/;
	return regex.test(email);
	}

  });