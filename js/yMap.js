// <![CDATA[
ymaps.ready(init);
var myMap, groute;
function init() {
myMap = new ymaps.Map("map", {
center: [55.606121,37.460495],
zoom: 13,
behaviors: ['scrollZoom', 'drag']
});
myMap.controls.add(
new ymaps.control.ZoomControl()
);
myPlacemark1 = new ymaps.Placemark([55.606121,37.460495], {
// Свойства.
// Содержимое иконки, балуна и хинта.
//iconContent: '1',
balloonContent: 'Тут адрес будет',
//hintContent: 'Стандартный значок метки'
}, {
// Опции.
// Стандартная фиолетовая иконка.
preset: 'twirl#blueIcon'
});
myMap.geoObjects.add(myPlacemark1);
myMap.events.add('click', function (e) {
if (!myMap.balloon.isOpen()) {
$('#list').html('');
if(groute) myMap.geoObjects.remove(groute)
var coords = e.get('coordPosition');
buildRoute([coords[0], coords[1]]);
}
else {
myMap.balloon.close();
}
});
}
function buildRoute(param){
ymaps.route([
param,
[55.606121,37.460495]
], {
avoidTrafficJams: true
}).then(function (route) {
groute = route;
myMap.geoObjects.add(route);
// Зададим содержание иконок начальной и конечной точкам маршрута.
// С помощью метода getWayPoints() получаем массив точек маршрута.
// Массив транзитных точек маршрута можно получить с помощью метода getViaPoints.
var points = route.getWayPoints(),
lastPoint = points.getLength() - 1;
// Задаем стиль метки - иконки будут красного цвета, и
// их изображения будут растягиваться под контент.
// points.get(0).options.set('preset', 'twirl#redStretchyIcon');
// Задаем контент меток в начальной и конечной точках.
// points.get(0).properties.set('iconContent', 'Точка отправления');
points.get(lastPoint).properties.set('balloonContent', 'Москва, Мосрентген пос., ул. Героя России Соломатина, 31 +7 (495) 337-40-01');
// Проанализируем маршрут по сегментам.
// Сегмент - участок маршрута, который нужно проехать до следующего
// изменения направления движения.
// Для того, чтобы получить сегменты маршрута, сначала необходимо получить
// отдельно каждый путь маршрута.
// Весь маршрут делится на два пути:
// 1) от улицы Крылатские холмы до станции "Кунцевская";
// 2) от станции "Кунцевская" до "Пионерская".
var moveList = 'Трогаемся,</br>',
way,
segments;
// Получаем массив путей.
for (var i = 0; i < route.getPaths().getLength(); i++) {
way = route.getPaths().get(i);
segments = way.getSegments();
for (var j = 0; j < segments.length; j++) {
var street = segments[j].getStreet();
moveList += ('Едем ' + segments[j].getHumanAction() + (street ? ' на ' + street : '') + ', проезжаем ' + segments[j].getLength() + ' м.,');
moveList += '</br>'
}
}
moveList += 'Останавливаемся.';
// Выводим маршрутный лист.
$('#list').append(moveList);
}, function (error) {
alert('Возникла ошибка: ' + error.message);
});
}
// ]]>