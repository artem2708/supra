var timer=0;

$(document).ready(function(){
	$(".index_pp_cats a").eq(0).addClass("activelink");
	pp_slider(0);
	
	
	// slider index_pp_cats
    $(".index_pp_cats a").hover(function () {
	  
	  pp_slider(1);
	  var index = $(".index_pp_cats a").index(this);
	  var curClass=".g_item_m"+index;
	  $(".index_pp_cats a").removeClass("activelink");
	  $(".index_pp_cats a").eq(index).addClass("activelink");
	  $(".g_item_m").removeClass("display");
      $(curClass).addClass("display");
    }, function () {
	  pp_slider(0);
	}
	);
	
});





function pp_slider(stop_flag){
	
if (stop_flag==1){
	clearTimeout(timer);
}
else{

	var i = 1;
	var prev_index=0;
	var cur_index=0;
	var curClass="";


	var size_of_index=$('.index_pp_cats a').length;

	timer = setTimeout(function run() { 
		
		cur_index=$('.index_pp_cats a.activelink').index() + 1;

		// if the last link in the group
		if (cur_index == size_of_index) {cur_index=0;}
		
		$(".index_pp_cats a").removeClass("activelink");
		$(".index_pp_cats a").eq(cur_index).addClass("activelink");
		$(".g_item_m").removeClass("display");
		$(".g_item_m"+cur_index).addClass("display");

		timer = setTimeout(run, 10000);
	}, 10000);
}
}