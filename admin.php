<?php
if($_SERVER['HTTP_HOST'] == 'suprashop.ru') {$_SERVER['HTTP_HOST'] = 'new.suprashop.ru'; $back = 'suprashop.ru'; }// это метод Игоря Тептеева - все права защищены - не копировать!! Copyright@
require_once('common.php');

define('ABO_ADM_MODE', true);

if( strstr(php_sapi_name(), "cgi") ) {
	$_SERVER['SCRIPT_NAME'] = $_SERVER['SCRIPT_FILENAME'] = basename(__FILE__);
}
$request_action = strip_tags(trim($request_action));
$request_action = str_replace(array("<", ">", "\"", "'", ".", "@"), "", $request_action);
$request_action = str_replace('\\', '', $request_action);
if ($request_action == 'changesite') {
	$login		= $_SESSION["session_login"];
	$password	= $_SESSION['session_password'];
	unset($_SESSION['permissions']);
	if ($main->fillPermissions($login, $password)) {
		header('Location: admin.php?lang='.$lang);
		exit;
	} else {
		unset($_SESSION);
		header('Location: login.php');
		exit;
	}
}

if (empty($_SESSION['session_is_admin'])) {
	header("Location: login.php?".$_SERVER['QUERY_STRING']);
	exit;
}

// если передан флажок phpinfo, то показать настройки PHP на хостинге через модуль pages
if (isset($_REQUEST['phpinfo'])) {
	$_REQUEST['name']	= 'pages';
	$_REQUEST['action']	= 'phpinfo';
}

// получить имя запрашиваемого модуля
$name = empty($_REQUEST['name']) ? $CONFIG['admin_default_module'] : $db->escape(strtolower(substr($_REQUEST['name'], 0, 20)));

// проверить существование модуля с таким именем в базе данных
$db->query('SELECT * FROM modules WHERE name="'.$name.'"');
$db->next_record();
if (!$db->nf())	$main->message_die( 'Запрашиваемый модуль не существует');

// определить настройки для работы с модулем
$name					= $db->f('name');										// Название модуля (переопределяем через базу)
$module_id				= $db->f('id');											// ID модуля
$Name					= ucwords($name);										// Название модуля с заглавной буквы
$title					= $db->f('title');										// Название модуля (на русском)
$lang_query				= ($CONFIG['multilanguage']) ? 'lang='.$lang.'&' : '';
$baseurl				= 'admin.php?'.$lang_query.'name='.$name;				// Основной URL для обращению к модулю
$tpl_path				= $CONFIG['admin_tpl_path'];							// Путь к папке с шаблонами
$class_path				= $CONFIG['class_path']; 								// Путь к папке с классами-модулями
//$admin_main_template	= ($request_menu == 'false') ? $CONFIG['admin_main_template']['2'] : $CONFIG['admin_main_template']['1'];
$action					= !empty($request_action) ? $request_action : $_REQUEST['action']; 		// запрашиваемое действие

if ($request_menu == 'false') {
	$admin_main_template	= $CONFIG['admin_main_template']['2'];
} else {
	$admin_main_template	= $CONFIG['admin_main_template']['1'];
}

// права доступа из переменных сессии, массив $permissions используется для проверки прав доступа в модуле $name
$permissions['r'] = $_SESSION['permissions'][SITE_PREFIX][$name]['r'];		// чтение
$permissions['e'] = $_SESSION['permissions'][SITE_PREFIX][$name]['e'];		// редактирование
$permissions['p'] = $_SESSION['permissions'][SITE_PREFIX][$name]['p'];		// публикация
$permissions['d'] = $_SESSION['permissions'][SITE_PREFIX][$name]['d'];		// удаление

// проверить существует ли файл с классом для данного модуля
if (!file_exists($class_path)) $main->message_die('Не определен класс для данного модуля');
// проверить существует ли основной html шаблон для адм.части
if (!file_exists($tpl_path.$admin_main_template)) $main->message_die('Нет основного шаблона');

// установить шаблон для работы с админской частью
$tpl = new AboTemplate($tpl_path.$admin_main_template);

Core::logAction();

// подключить класс для данного модуля

if(isset($back)) $_SERVER['HTTP_HOST'] = $back;

require_once($class_path.$name.'/lib/class.'.$Name.'.php');

$mod = new $Name($action);
Main::checkCrc($mod, $action);
$http_host = isset($_GET["site_target"]) ? $_GET["site_target"] : $CONFIG['domen'];
$site		= '/admin.php?site_target='.$http_host.'&amp;lang='.$lang;
$page_title = 'ABO.CMS: '.$title;

if ($mod->main_title) $page_title .= ' &raquo; '.$mod->main_title;
if ($ErrKEY) $page_title = '[ Неверный лицензионный ключ, доступ на сайт ограничен ]';
$tpl->assign(
	array(
		'_ROOT.abocms_version'			=> ABOCMS_VERSION,
		'_ROOT.abocms_release'			=> ABOCMS_RELEASE_NAME.' '.ABOCMS_RELEASE,
		'_ROOT.page_title'				=> $page_title,
		'_ROOT.main_title'				=> $mod->main_title,
		'_ROOT.url_change_site'			=> '/admin.php?lang=',
		'_ROOT.select_sites_langs'		=> $main->getSitesLangsSelect(),
		'_ROOT.material_sites_langs'	=> $main->getMeterialSitesLangsSelect($site, $name),
		'_ROOT.site_target'				=> $http_host,
		'_ROOT.site_id'					=> $core->aSitesLangs[$main->parseSiteName($http_host)]['site_id'],
		'_ROOT.lang_id'					=> $core->aSitesLangs[$main->parseSiteName($http_host)]['site_lang'][$lang],
		'_ROOT.window_url'				=> ($request_site == 'change') ? '' : $main->getURLByBlock_id($request_block_id),
		'_ROOT.copy_year'				=> date("Y"),
		'_ROOT.MSG_Site'				=> $main->_msg["Site"],
		'_ROOT.MSG_Exit'				=> $main->_msg["Exit"],
		'_ROOT.MSG_To_leave_system'		=> $main->_msg["To_leave_system"],
	)
);

$main->show_admin_menu($mod->admin_default_action);
$tpl->printToScreen($CONFIG['back_office_zip']);
exit;
?>