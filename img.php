<?php
require_once('common.php');
require ('mod/gallery/config.php');

$pref = $_REQUEST['p'] ? preg_replace('/\W/', '', $_REQUEST['p']) : SITE_PREFIX;
$image = "files/gallery/{$pref}/{$_REQUEST['img']}";
$thumbs = "files/gallery/{$pref}/{$NEW['gallery_photo_imposition']}";
$size = getimagesize($image);
$size_thumbs = getimagesize($thumbs);
header("Content-type: image/png");

$res = ImageCreateTrueColor($size[0],$size[1]);

if ($size['mime'] == "image/jpeg" || $size['mime'] == "image/pjpeg" || $size['mime'] == "image/jpg"){
    $img = imagecreatefromjpeg($image);
}	else if ($size['mime'] == "image/gif"){
    $img = imagecreatefromgif($image);
}	else if ($size['mime'] == "image/x-png" || $size['mime'] == "image/png"){
    $img = imagecreatefrompng($image);
}

$logo = false;
if ($size_thumbs['mime'] == "image/jpeg" || $size_thumbs['mime'] == "image/pjpeg" || $size_thumbs['mime'] == "image/jpg"){
    $logo = imagecreatefromjpeg($thumbs);
}	else if ($size_thumbs['mime'] == "image/gif"){
    $logo = imagecreatefromgif($thumbs);
}	else if ($size_thumbs['mime'] == "image/x-png" || $size_thumbs['mime'] == "image/png"){
    $logo = imagecreatefrompng($thumbs);
}
$trans = imagecolorat($logo,0,0);
imagecolortransparent($logo,$trans);

imagecopyresized ($res,$img,0,0,0,0,$size[0],$size[1],imagesx($img),imagesy($img));
$x = ($NEW['gallery_photo_x'] == 0) ? ($size[0]/2)-($size_thumbs[0]/2) : $NEW['gallery_photo_x'];
$y = ($NEW['gallery_photo_y'] == 0) ? ($size[1]/2)-($size_thumbs[1]/2) : $NEW['gallery_photo_y'];
if ($x < 0) {
    $x = $size[0]+$x;
}
if ($y < 0) {
    $y = $size[1]+$y;
}
if ($logo) {
    imagecopymerge ($res,$logo,$x,$y,0,0,$size_thumbs[0],$size_thumbs[1], $NEW['gallery_photo_merge']);
}
/*
$trans = imagecolorat($res,0,0);
imagecolortransparent($res,$trans);
*/
imagejpeg($res);
?>