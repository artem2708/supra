<?
require_once("../common.php");
header('Content-type: application/json;charset=utf-8');

$action = $_GET['action'];
$request = $_POST;
$data = json_decode(file_get_contents('php://input'), true);
$server="sup_";


switch ($action) {
	case 'cart':
		$items = array();
		$deliveryOptions = array();
		foreach ($data['cart']['items'] as $i => $item) {
			$db->query('SELECT price_1 FROM '.$server.$lang.'_catalog_products WHERE id='.$item['offerId'].' AND active=1');
			if($db->nf() > 0){
				$db->next_record();
				$price = ($db->f('price_1') * 100) / 100;
				$count = $item['count'];
				$delivery = true;
			}else{
				$price = 0;
				$count = 0;
				$delivery = false;
			}
			$items[$i] = array('feedId' => $item['feedId'], 'offerId' => $item['offerId'], 'price' => $price, 'count' => $count, 'delivery' => $delivery);
		}
		$deliveryOptions = array(array('type' => 'DELIVERY', 'serviceName' => 'Suprashop', 'price' => 0, 'dates' => array('fromDate' => date('d-m-Y'))));
		$paymentMethods = array('CASH_ON_DELIVERY');
		$return = json_encode(array('cart' => array('items' => $items, 'deliveryOptions' => $deliveryOptions, 'paymentMethods' => $paymentMethods)));
		print_r($return);
		break;

	case 'order_accept':
	GLOBAL $CONFIG;
		$accept = true;
		foreach ($data['order']['items'] as $i => $item) {
			$db->query('SELECT * FROM '.$server.$lang.'_catalog_products WHERE id='.$item['offerId'].' AND active=1');
			if(! $db->nf() > 0){
				$accept = false;
				break;
			}
		}

		if($accept){
			include_once('../inc/class.Mailer.php');
			$ad = $data['order']['delivery']['address'];
			$addr = "Страна: ".$ad['country']."\r\nИндекс:".$ad['postcode']."\r\nГород:".$ad['city']."\r\nМетро:".$ad['subway']."\r\nУлица:".$ad['street']."\r\nДом:".$ad['house']."\r\nЭтаж:".$ad['floor'];
			$db->query("INSERT INTO ".$server.$lang."_shop_orders(type, date, notes, address, customer) VALUES('Яндекс Маркет', '".date('Y-m-d H:i:s')."', '".$data['order']['notes']."', '".$addr."', 'Заказ с Yandex Маркета')");
			$order_id = $db->lid();
			if($order_id){
				$db->query("INSERT INTO ".$server.$lang."_shop_yandex_orders(yandex_order_id, order_id) VALUES(".$data['order']['id'].", ".$order_id.")");
				$sum = 0;
				foreach ($data['order']['items'] as $i => $item) {
					$db->query("INSERT INTO ".$server.$lang."_shop_order_items(order_id, item_id, item_name, item_price, item_qty, item_cost, gift_id, kupon) 
						VALUES(".$order_id.", ".$item['offerId'].", '".$item['offerName']."', ".$item['price'].", ".$item['count'].", ".($item['price'] * $item['count']).", 0, 0)");
					$sum += ($item['price'] * $item['count']);
					/*if(! $db->nf() > 0){
						$accept = false;
						break;
					}*/
				}
				$db->query("UPDATE ".$server.$lang."_shop_orders SET sum=".$sum.", total_sum=".$sum." WHERE id=".$order_id);
				//$to      = 'la2-olympia@mail.ru';
				$subject = 'Новый заказ с Yandex маркета';
				/*$message = 'hello';
				$headers = 'X-Mailer: PHP/' . phpversion();*/

				/*mail($to, $subject, $message, $headers);
				$mail = new Mailer();
	            $mail->CharSet		= $CONFIG['email_charset'];
	            $mail->ContentType	= "text/html"; //$CONFIG['email_type'];
	            $mail->From			= $email;
	            $mail->AddAddress    ($CONFIG['shop_order_recipient']);
	            $mail->Subject		= $subject;
	            $mail->Body			= nl2br($addr."\n Если у вас возникнут вопросы по вашему заказу, вы можете связаться с нашими менеджерами по телефону:  7 (800) 775-49-37 или написать нам на электронную почту shop@supra.ru");
	            $mail->Send();*/
	            $email = "shop@supra.ru";
	            $mail = new Mailer();
				//$addr = $this->_msg["Order_confirmed"] . "\n\n".$addr;
				$mail->CharSet		= $CONFIG['email_charset'];
	            $mail->ContentType	= "text/html"; //$CONFIG['email_type'];
	            $mail->From			= $CONFIG['shop_order_recipient'];
	            $mail->FromName		= $CONFIG['sitename_rus'];
	            $mail->AddAddress($email);
	            $mail->Subject		= $subject;
	            $mail->Body			= nl2br($addr."\n Если у вас возникнут вопросы по вашему заказу, вы можете связаться с нашими менеджерами по телефону:  7 (800) 775-49-37 или написать нам на электронную почту shop@supra.ru");
				//$mail->AddAttachment(RP.'kvitancia.htm');
	            $mail->Send();

			}else{
				$accept = false;
			}
		}
		if($accept) 
			$return = json_encode(array('order' => array('id' => "".$data['order']['id'], 'accepted' => $accept)));
		else
			$return = json_encode(array('order' => array('id' => "".$data['order']['id'], 'accepted' => $accept, 'reason' => 'OUT_OF_DATE')));
		print_r($return);
		break;

	case 'order_status':
		$yandex_id = $data['order']['id'];
		$db->query("SELECT * from ".$server.$lang."_shop_yandex_orders WHERE yandex_order_id=".$yandex_id);
		if($db->nf() > 0){
			$db->next_record();
			$order_id = $db->f('order_id');
		}
		var_dump($order_id);

		if($order_id){
			$order_status = 0;
			switch ($data['order']['status']) {
				case 'PROCESSING':
					$order_status = 0;
					break;
				
				case 'DELIVERY':
					$order_status = 6;
					break;
				
				case 'PICKUP':
					$order_status = 6;
					break;
				
				case 'DELIVERED':
					$order_status = 8;
					break;
				
				case 'CANCELLED':
					$order_status = 7;
					break;
			}
			$customer = $data['order']['buyer']['lastName']." ".$data['order']['buyer']['firstName']." ".$data['order']['buyer']['middleName'];
			$phone = $data['order']['buyer']['phone'];
			$email = $data['order']['buyer']['email'];
			var_dump($data['order']['buyer']);

			$db->query("UPDATE ".$server.$lang."_shop_orders SET customer='".$customer."', phone='".$phone."', email='".$email."', status=".$order_status." WHERE id=".$order_id);
		}
		break;
}
//var_dump(json_decode($req));
?>

