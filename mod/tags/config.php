<?php // Vers 5.8.2 27.06.2012
/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
/*
    'tags_max_row' => array(
        'type' => 'integer',
        'defval' => '20',
        'descr'	=> array(
            'rus' => 'Кол-во элементов на странице',
            'eng' => 'Кол-во элементов на странице'
        ),
        'access' => 'editable'
    ),
*/
    'tags_def_color' => array(
        'type' => 'string',
        'defval' => 'cccccc',
        'descr'	=> array(
            'rus' => 'Цвет тега по умолчанию',
            'eng' => 'Цвет тега по умолчанию'
        ),
        'access' => 'editable'
    ),
    'tags_def_width' => array(
        'type' => 'integer',
        'defval' => '200',
        'descr'	=> array(
            'rus' => 'Ширина флеша по умолчания',
            'eng' => 'Ширина флеша по умолчания'
        ),
        'access' => 'editable'
    ),
    'tags_def_height' => array(
        'type' => 'integer',
        'defval' => '200',
        'descr'	=> array(
            'rus' => 'Высота флеша по умолчания',
            'eng' => 'Высота флеша по умолчания'
        ),
        'access' => 'editable'
    ),
);

$OPEN_NEW = array (
    'tags_xml_path' => $CONFIG['files_upload_path'].'tags/{SITE_PREFIX}/',
    'tags_wt_list' => array(
        1 => 10,
        2 => 13,
        3 => 16,
        4 => 20,
        5 => 24,
    ),
);

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'tags_def_color' => 'FE8711',
'tags_def_width' => '200',
'tags_def_height' => '250',
/* ABOCMS:END */
);
?>