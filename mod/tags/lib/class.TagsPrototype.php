<?php
require_once(RP.'mod/siteusers/lib/class.SiteusersPrototype.php');
require_once(RP.'inc/class.Mailer.php');

class TagsPrototype extends Module {
    
	const PROP_SEPRT = ';';
	
	var $main_title;
	var $table_prefix;
	var $addition_to_path;
	var $module_name				= 'tags';
	var $default_action				= 'tags';
	var $admin_default_action		= 'tags';
	var $tpl_path					= 'mod/tags/tpl/';
	var $block_module_actions		= array();
	var $block_main_module_actions	= array();
	var $action_tpl = array(
	    'cloud' => 'tags_cloud.html',
	);

	function TagsPrototype($action = '', $transurl = '?', $properties = array(), $prefix = NULL, $rewrite_mod_params = NULL) {
		global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $core, $server, $lang, $permissions, $db;
		
		/* Get $action, $tpl_name, $permissions */
		extract( $this->init(array(
		        'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
		        'action' => $action, 'tpl_name' => $properties['tpl']
		)), EXTR_OVERWRITE);
		
		$CONFIG['tags_xml_path'] = str_replace("{SITE_PREFIX}", $this->table_prefix, $CONFIG['tags_xml_path']);
		
		if ('only_create_object' == $action) {
		    return;
		}
		
		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}
		
		if (! self::is_admin()) {
//---------------------------------- обработка действий с сайта --------------------------------//
			switch ($action) {
    			case "cloud":
    			    $tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                    $main->include_main_blocks($tpl_name, 'main');                    
    				$tpl->prepare();
    				
    				$cloud = $this->getCloud($properties[1]);
    				if ($cloud && file_exists(RP.$CONFIG['tags_xml_path']."tagcloud{$cloud['id']}.xml")) {
    				    $tpl->newBlock('cloud');
        				$assign = array(
    					    'baseurl' => $baseurl,
        					'action' => $action,
        					'referer' => $_SERVER['HTTP_REFERER'],
        					'width' => $properties[2] > 0 ? (int) $properties[2] : (int) $CONFIG['tags_def_width'],
        					'height' => $properties[3] > 0 ? (int) $properties[3] : (int) $CONFIG['tags_def_height'],
        					'xmlpath' => '/'.$CONFIG['tags_xml_path']."tagcloud{$cloud['id']}.xml"
    					);
    					$tpl->assign($assign);
    					$tpl->assignList('tag_', $cloud['tags'], $assign);
    				} else {
    				    $tpl->newBlock('none');
    				}
    				break;
// Default...
			default:
				if (is_object($tpl)) $tpl->prepare();
				return;
			}
//------------------------------- конец обработки действий с сайта -----------------------------//
		} else {
//------------------------------ обработка действий из админ части -----------------------------//
        
        $this->block_module_actions = array(
            'cloud' => 'Показать группу меток'
        );
        $this->block_main_module_actions = array(
            'cloud' => 'Показать группу меток'
        );
        
        if (!isset($tpl)) {
            $main->message_die('Не подключен класс для работы с шаблонами');
        }
            
        switch ($action) {
                case 'tags':
					if (!$permissions['r']) {
						$main->message_access_denied($siteusers->module_name, $action);
					}
					$main->include_main_blocks_2("{$this->module_name}_{$action}.html", $this->tpl_path);
					$tpl->prepare();		
					$assign = array(
					   'baseurl' => $baseurl,
    					'action' => $action,
    					'color' => $CONFIG['tags_def_color'],
    					'referer' => $_SERVER['HTTP_REFERER']
					);					
					$tpl->assign($assign + array('css_hide_class' => 'hide'));
    				$cloud_list = $this->getCloudList();
    				Module::tpl_assign_array($tpl, 'cloud_list', $cloud_list, $assign);
    				
					$this->main_title = 'Группа меток';
					break;
					
                case 'cloud_edit':
					if (!$permissions['e']) {
						$main->message_access_denied($siteusers->module_name, $action);
					}
					$main->include_main_blocks_2("{$this->module_name}_{$action}.html", $this->tpl_path);
					$tpl->prepare();
					
					if (! empty($_POST)) {
					    $this->setCloud($_POST);
					    $this->generateXml();
					    Module::go_back($baseurl."&action=tags");
					    exit;
					}
					$assign = $this->getCloud($_REQUEST['id']);
					$assign['baseurl'] = $baseurl;
    				$assign['action'] = $action;
    				$assign['referer'] = $_SERVER['HTTP_REFERER'];
    				$assign['color'] = $CONFIG['tags_def_color'];
					$tpl->assign($assign);
					Module::show_select('wt_list', 0, '', $CONFIG['tags_wt_list']);
					$max_id = 1;
					if ($assign['tags']) {
					    foreach ($assign['tags'] as $val) {
					    	$tpl->newBlock('tag_list');
					    	$val['color'] = $val['color'] ? $val['color'] : $CONFIG['tags_def_color'];
					    	$tpl->assign($val);
					    	Module::show_select('tag_wt_list', $val['wt'], '', $CONFIG['tags_wt_list']);
					    	if ($val['id'] > $max_id) {
					    	    $max_id = $val['id'];
					    	}
					    }
					}
					$tpl->gotoBlock('_ROOT');
					$tpl->assign('max_id', $max_id);
					
					$this->main_title = 'Редактировать';
					break;
					
                case 'cloud_del':
					if (!$permissions['d']) {
						$main->message_access_denied($siteusers->module_name, $action);
					}
					
					$this->delCloud($_REQUEST['id']);
					Module::go_back($baseurl.'&action=tags');
					exit;
					
                case 'reload':
					if (!$permissions['e']) {
						$main->message_access_denied($siteusers->module_name, $action);
					}
					
					$this->generateXml();
					Module::go_back($baseurl.'&action=tags');
					exit;
					
// Редактирование свойств (параметров) блока
			case 'properties':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $module_id, $request_field, $request_block_id, $title;
				$main->include_main_blocks('_block_properties.html', 'main');
				$tpl->prepare();

				if ($main->show_actions($request_field, '', $this->block_main_module_actions, $this->block_module_actions)) {
					$main->show_linked_pages($module_id, $request_field);
				}
				$tpl->assign(array(	'_ROOT.title'		=> $title,
									'_ROOT.username'	=> $_SESSION['session_login'],
									'_ROOT.password'	=> $_SESSION['session_password'],
									'_ROOT.name'		=> $this->module_name,
									'_ROOT.lang'		=> $lang,
									'_ROOT.block_id'	=> $request_block_id,
									));
				$this->main_title	= $this->_msg["Block_properties"];
				// Load JsHttpRequest backend.
				require_once RP."inc/class.JsHttpRequest.php";
				// Create main library object. You MUST specify page encoding!
				$JsHttpRequest =& new JsHttpRequest("utf-8");
				break;
                case 'prp':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_step;
                    $main->include_main_blocks('_properties.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Info_siteusers"	=> $this->_msg["Info_siteusers"],
                        "MSG_Information"	=> $main->_msg["Information"],
                        "MSG_Value"			=> $main->_msg["Value"],
                        "MSG_Save"			=> $main->_msg["Save"],
                        "MSG_Cancel"		=> $main->_msg["Cancel"],
                    ));
                    if (!$request_step || $request_step == 1) {
                        $tpl->assign(array(
                            'form_action'	=> $baseurl,
                            'step'			=> 2,
                            'lang'			=> $lang,
                            'name'			=> $this->module_name,
                            'action'			=> 'prp'
                        ));
                        $prp_html = $main->getModuleProperties($this->module_name);
                        $this->main_title = $this->_msg['Settings'];
                    } elseif (2 == $request_step) {
                        $main->setModuleProperties($this->module_name, $_POST);
                        header('Location: '.$baseurl.'&action=prp&step=1');
                    }
                    break;

                case 'block_prp':
                    global $request_sub_action, $request_block_id;
                    $arr  = $this->getEditLink($request_sub_action, $request_block_id);
                    $cont = 'var container = document.getElementById(\'target_span\');
		    		  	 container.innerHTML	= "'.$this->getPropertyFields().'";
		    		  	 material_id			= "'.$arr['material_id'].'";
		    		  	 material_url			= "'.$arr['material_url'].'";
		    		  	 changeAction();
						//setURL("property1");';
                    header('Content-Length: '.strlen($cont));
                    echo $cont;
                    exit;
                    break;

// Default...
			default:
				$main->include_main_blocks();
				$tpl->prepare();
			}
//--------------------------- конец обработки действий из админ части --------------------------//
		}
	}

    function getPropertyFields() {
        GLOBAL $request_sub_action;
        if ('' != $request_sub_action) {

            $tpl = $this->getTmpProperties();

            switch ($request_sub_action) {
                case 'cloud':
                    $tpl->newBlock('block_properties');
                    Module::tpl_assign_array($tpl, 'cloud_list', $this->getCloudList());
                    break;
                default:
                    $tpl->newBlock('block_properties_none');
                    $tpl->assign(Array(
                        "MSG_No" => $this->_msg["No"],
                    ));
                    break;
            }
            return $this->getOutputContent($tpl->getOutputContent());
        }
        return FALSE;
    }

    function getEditLink($sub_action = NULL, $block_id = NULL) {
        return FALSE;
    }
	
	function getCloudList()
	{   global $db, $CONFIG;
	    static $ret = false;
	    
	    if (!$ret) {
	        $db->query("SELECT * FROM {$this->table_prefix}_tags_cloud ORDER BY id");
    	    while ($db->next()) {
    	        $ret[$db->Record['id']] = $db->Record;
    	    }
	    }	    	   
		
		return $ret;
	}
	
	function getCloud($id)
	{   global $db, $CONFIG;
	    
	    $ret = false;
	    
	    $db->query("SELECT C.id AS c_id, C.title AS cloud_title, T.* FROM"
	       ." {$this->table_prefix}_tags_cloud C LEFT JOIN {$this->table_prefix}_tags T"
	       ." ON C.id=T.cloud_id WHERE C.id=".intval($id));
    	if ($db->next()) {
    	    $ret['id'] = $db->Record['c_id'];
    	    $ret['title'] = $db->Record['cloud_title'];
    	    $ret['tags'] = array();
    	    if ($db->Record['id']) {
    	        $ret['tags'][$db->Record['id']] = array(
    	            'id' => $db->Record['id'],
    	            'title' => $db->Record['title'],
    	            'link' => $db->Record['link'],
    	            'wt' => $db->Record['wt'],
    	            'color' => $db->Record['color'],
    	        );
    	        while ($db->next()) {
        	        $ret['tags'][$db->Record['id']] = array(
        	            'id' => $db->Record['id'],
        	            'title' => $db->Record['title'],
        	            'link' => $db->Record['link'],
        	            'wt' => $db->Record['wt'],
        	            'color' => $db->Record['color'],
        	        );
        	    }
    	    }
    	}
		return $ret;
	}
	
	function setCloud($data)
	{   global $db, $CONFIG;

        $id = false;
        if ($data['title']) {
            $_data = array(
    	        'title' => $data['title'],
    	    );
    		if ($id = $data['id']) {
    		    $cloud = $this->getCloud($id);
    		    if ($cloud) {
    		        $db->set("{$this->table_prefix}_tags_cloud", $_data, array('id' => $id));
    		    } else {
    		        $id = false;
    		    }
    		} else {
    		    $db->set("{$this->table_prefix}_tags_cloud", $_data);
    		    $id = $db->lid();
    		}
    		if ($id && $data['tags']) {
    		    foreach ($data['tags'] as $val) {
    		    	if ($val['title'] && $val['link']) {
    		    	    $_data = array(
                	        'title' => $val['title'],
                	        'link' => $val['link'],
                	        'wt' => $val['wt'] ? $val['wt'] : 1,
                	        'color' => $val['color'] ? $val['color'] : $CONFIG['tags_def_color'],
                	        'cloud_id' => $id,
                	    );
    		    	    if ($val['id']) {
    		    	        $db->set("{$this->table_prefix}_tags", $_data, array('id' => $val['id']));
    		    	    } else {
    		    	        $db->set("{$this->table_prefix}_tags", $_data);
    		    	    }
    		    	}
    		    }
    		}
        }
		return $id;
	}
	
	function delCloud($id)
	{   global $db, $CONFIG;
	
	    $cloud = $this->getCloud($id);
	    if ($cloud) {
	        $db->del("{$this->table_prefix}_tags_cloud", array('id' => $id));
	        $db->del("{$this->table_prefix}_tags", array('cloud_id' => $id));
	        if (file_exists(RP.$CONFIG['tags_xml_path']."tagcloud{$cloud['id']}.xml")) {
	            unlink(RP.$CONFIG['tags_xml_path']."tagcloud{$cloud['id']}.xml");
	        }
	    }		

		return $id;
	}
	
	function generateXml()
	{   global $db, $CONFIG;

        $db->query("SELECT * FROM {$this->table_prefix}_tags ORDER BY cloud_id");
        $cloud_id = 0;
        $tags = array();
    	while ($db->next()) {
    	    if ($cloud_id && $cloud_id != $db->Record['cloud_id']) {
    	        $fp = fopen(RP.$CONFIG['tags_xml_path']."tagcloud{$cloud_id}.xml", 'w');
    	        if ($fp) {
    	            fwrite($fp, "<tags>\n".implode("\n", $tags)."</tags>");
    	            fclose($fp);
    	        }
    	        $tags = array();
    	    }
    	    $cloud_id = $db->Record['cloud_id'];
    	    $size = $CONFIG['tags_wt_list'][$db->Record['wt']];
    	    $color = $db->Record['color'] ?  $db->Record['color'] : $CONFIG['tags_def_color'];
    	    $tags[] = '<a href="http://'.$_SERVER['HTTP_HOST'].$db->Record['link']
    	        .'" class="tag-link-'.$db->Record['id'].'" title="'.$db->Record['title']
    	        .'" rel="tag" style="font-size: '.$size.'pt;" color="0x'.$color.'">'
    	        .$db->Record['title'].'</a>';
    	}
    	if ($cloud_id) {
    	    $fp = fopen(RP.$CONFIG['tags_xml_path']."tagcloud{$cloud_id}.xml", 'w');
    	    if ($fp) {
    	        fwrite($fp, "<tags>\n".implode("\n", $tags)."\n</tags>");
    	        fclose($fp);
    	    }
    	}
	}
}
?>