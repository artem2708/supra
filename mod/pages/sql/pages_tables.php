<?php

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_pages` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parent_id` smallint(5) unsigned default NULL,
  `level` smallint(6) NOT NULL default '0',
  `page_rank` smallint(6) NOT NULL default '0',
  `title` varchar(255) NOT NULL default '',
  `add_title` varchar(255) NOT NULL,
  `main_module` smallint(5) unsigned NOT NULL default '0',
  `active` tinyint(1) NOT NULL default '0',
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `link` varchar(50) NOT NULL default '',
  `address` varchar(255) default NULL,
  `branch` varchar(100) default NULL,
  `tpl_id` smallint(5) unsigned NOT NULL default '0',
  `lang` varchar(10) default NULL,
  `alt_title` varchar(255) NOT NULL default '',
  `keywords` text,
  `description` text,
  `meta` text,
  `redirect_url` varchar(127) default NULL,
  `image` varchar(100) default NULL,
  `is_default` tinyint(1) NOT NULL default '0',
  `can_be_edited` tinyint(4) NOT NULL default '1',
  `permission_type` tinyint(4) NOT NULL default '0',
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',
  `img1` varchar(63) NOT NULL default '',
  `img2` varchar(63) NOT NULL default '',
  `img3` varchar(63) NOT NULL default '',
  `main_access` tinyint(1) NOT NULL default 1,
  PRIMARY KEY  (`id`),
  KEY `index_2` (`id`,`parent_id`,`active`,`is_default`),
  KEY `index_3` (`parent_id`,`active`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_pages_blocks` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `page_id` smallint(5) unsigned default NULL,
  `field_number` smallint(5) unsigned default NULL,
  `module_id` smallint(5) unsigned default NULL,
  `site_id` int(11) default NULL,
  `lang_id` int(11) default NULL,
  `action` char(20) default NULL,
  `linked_page` char(50) default NULL,
  `linked_page_address` char(255) default NULL,
  `block_rank` smallint(5) default NULL,
  `property1` varchar(255) default NULL,
  `property2` varchar(255) default NULL,
  `property3` varchar(255) default NULL,
  `property4` varchar(255) default NULL,
  `property5` varchar(255) default NULL,
  `tpl` varchar(31) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_pages_context_menu` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `name` char(255) default NULL,
  `active` tinyint(4) NOT NULL default '1',
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_pages_context_menu_items` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `page_id` smallint(5) unsigned default NULL,
  `menu_id` smallint(5) unsigned default NULL,
  `item_rank` smallint(5) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_pages_internal_links` (
  `module_name` char(100) default NULL,
  `table_name` char(100) default NULL,
  `field_name` char(100) default NULL,
  `entry_id` int(11) default NULL,
  `page_id` smallint(6) default NULL,
  `link` char(255) default NULL,
  `description` char(255) default NULL,
  `status` char(50) default NULL
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_pages_templates` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `tpl_name` char(255) NOT NULL default '',
  `main_module` tinyint(3) unsigned NOT NULL default '0',
  `number_of_fields` tinyint(3) unsigned NOT NULL default '0',
  `image_url` char(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

?>