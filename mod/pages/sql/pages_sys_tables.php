<?php
$MOD_TABLES[] = "CREATE TABLE `modules` (
					`id` int(11) NOT NULL auto_increment,
          `title` varchar(255) NOT NULL default '',
          `name` varchar(255) NOT NULL default '',
          `only_as_main` tinyint(1) unsigned NOT NULL default '0',
          `admin_only` tinyint(1) NOT NULL default '0',
          `Vers` varchar(50) default NULL,
          `LU` int(12) unsigned default NULL,
          `LKEY` varchar(32) default NULL,
          `parent_id` int(11) NOT NULL default '0',
          `rank` int(11) NOT NULL,
          PRIMARY KEY  (`id`)
					) ENGINE=MyISAM";
?>