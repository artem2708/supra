<?php // Vers 5.8.2 27.06.2012

/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
			  'pages_order_by'			=> array('type'		=> 'select',
												 'defval'	=> array('id DESC'	=> array(	'rus' => 'в порядке обратном добавлению',
												 											'eng' => 'reverce adding order'),
												 					 'id'		=> array(	'rus' => 'в порядке добавления',
												 											'eng' => 'adding order'),
												 					 'parent_id, id' => array(	'rus' => 'сгруппировать по родительскому элементу',
																	 							'eng' => 'group by parent element'),
												 					 'page_rank desc'  => array('rus' => 'согласно структуры сайта',
												 											    'eng' => 'from tree structure'),
												 					 ),
												 'descr'	=> array(	'rus' => 'Порядок вывода записей',
												 						'eng' => 'Show records order'),
												 'access'	=> 'editable',
												 ),

			   'pages_max_rows'			=> array('type'		=> 'integer',
			   									 'defval'	=> '20',
			   									 'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу',
 												 						'eng' => 'Maximum quantity of records on page'),
		   									 'access'	=> 'editable'
			   									 ),

			   'pages_view_new'			=> array('type'		=> 'integer',
			   									 'defval'	=> '5',
			   									 'descr'	=> array(	'rus' => 'Количество ссылок в блоке обзора нового на сайте',
 												 						'eng' => 'Links quantity in overview new on site block'),
			   									 'access'	=> 'editable'
			   									 ),

			   'pages_default_rights'	=> array('type'		=> 'string',
			   									 'defval'	=> '111111110001',
			   									 'descr'	=> array(	'rus' => 'Права для страницы по-умолчанию (формат <img src="/i/admin/rights_format.gif" width=72 height=18 alt="формат прав">, где x - 0 или 1, каждая группа представляет набор прав: удаление, публикация, редактирование, чтение)',
 												 						'eng' => 'Page default rights (format <img src="/i/admin/rights_format.gif" width=72 height=18 alt="rights format">, where x - 0 or 1, every group is rights set: delete, publication, edit, read)'),
			   									 'access'	=> 'editable'
			   									 ),
			  'pages_menu_mod'			=> array('type'		=> 'select',
												 'defval'	=> array(''	=> array(	'rus' => 'только текущий раздел',
												 											'eng' => 'only the current section'),
												 					 'all'		=> array(	'rus' => 'отображать все разделы',
												 											'eng' => 'display all sections'),
												 					 ),
												 'descr'	=> array(	'rus' => 'Отображение меню',
												 						'eng' => 'Displaying the menu'),
												 'access'	=> 'editable',
												 ),

			   'cm_default_rights'		=> array('type'		=> 'string',
			   									 'defval'	=> '111111110001',
			   									 'descr'	=> array(	'rus' => 'Права для контекстного меню по-умолчанию (формат <img src="/i/admin/rights_format.gif" width=72 height=18 alt="формат прав">, где x - 0 или 1, каждая группа представляет набор прав: удаление, публикация, редактирование, чтение)',
 												 						'eng' => 'Context menu default rights (format <img src="/i/admin/rights_format.gif" width=72 height=18 alt="rights format">, where x - 0 or 1, every group is rights set: delete, publication, edit, read)'),
			   									 'access'	=> 'editable'
			   									 ),
			   );

$OPEN_NEW = array(
	'pages_tpl_image_path' 	=> 'i/admin/tpl/',		 			// где лежат скриншоты шаблонов
   	'pages_img_path' 		=> 'files/pages/{SITE_PREFIX}/',		// куда класть закачанные на сервер иконки для страниц
    'pages_no_icon' 		=> 'non-page-icon.gif',
    'pages_date_format' 	=> '%d.%m.%Y %H:%i',
    'pages_img_formats' => array(
		'image/jpeg'	=> 'jpg',
		'image/jpg'		=> 'jpg',
		'image/pjpeg'	=> 'jpg',
		'image/gif'		=> 'gif',
		'image/x-png'	=> 'png',
		'image/png'		=> 'png',
	),
	'pages_prioritet_step' => array(0.1, 0.2, 0.3),
	'pages_prioritet_step_default_key' => 1,
	'pages_sitemap_path' => '',
);

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'pages_order_by' => 'page_rank desc',
'pages_max_rows' => '20',
'pages_view_new' => '5',
'pages_default_rights' => '111111110001',
'pages_menu_mod' => 'all',
/* ABOCMS:END */
);
?>