<?php

class CPagesCMItems_DB extends TObject {
	var $id			= NULL;
	var $page_id	= NULL;
	var $menu_id	= NULL;
	var $item_rank	= NULL;
	function CPagesCMData_DB($table, $PK) {
		$this->TObject($table, $PK);
	}

	function load ($id) {
		$sql = "SELECT * FROM $this->_tbl WHERE $this->_tbl_key = ".intval($id);
		return db_loadObject($sql,$this);
	}

	function getItems ($id) {
		global $lang, $server;
		$sql = "SELECT pcmi.*, p.title as title FROM $this->_tbl as pcmi, ".$server.$lang."_pages as p WHERE pcmi.menu_id = ".intval($id)." AND p.id = pcmi.page_id ORDER BY item_rank";
		return db_loadList($sql);
	}

	function listByMenu($id) {
		$sql = "SELECT * FROM $this->_tbl WHERE menu_id = ".intval($id);
		return db_loadList($sql);
	}

	function deleteByCategory ($id) {
		global $lang;
		$sql = "DELETE FROM $this->_tbl WHERE menu_id = ".intval($id);
		db_exec($sql);
		if (db_affected_rows() > 0) return 1;
		else return 0;
	}
}

class CPagesCM_DB extends TObject {
	var $id		= NULL;
	var $name	= NULL;
	var $active	= NULL;
	function CPagesCMData_DB($table, $PK) {
		$this->TObject($table, $PK);
	}

	function load ($id) {
		$sql = "SELECT * FROM $this->_tbl WHERE $this->_tbl_key = ".intval($id);
		return db_loadObject($sql,$this);
	}

	function listAll() {
		$sql = "SELECT * FROM $this->_tbl ORDER BY id";
		return db_loadList($sql);
	}
}


class CPagesLinks_DB extends TObject {

	var $module_name	= NULL;
	var $table_name		= NULL;
	var $field_name		= NULL;
	var $entry_id		= NULL;
	var $page_id		= NULL;
	var $link			= NULL;
	var $description	= NULL;
	var $status			= NULL;

	function CPagesCMData_DB($table, $PK) {
		$this->TObject($table, $PK);
	}

	function load ($id) {
		$sql = "SELECT * FROM $this->_tbl WHERE $this->_tbl_key = ".intval($id);
		return db_loadObject($sql,$this);
	}

	function listAll($where_str = null, $start_row, $rows) {
		$sql = "SELECT * FROM $this->_tbl ".My_Sql::escape($where_str)." LIMIT ".intval($start_row).",".intval($rows);
		return db_loadList($sql);
	}
}

?>