<?php

require_once($CONFIG['inc_path'].'class.TreeMenu.php');
require_once ('class.pagesDB.php');

class PagesPrototype extends Module {

	var $main_title;
	var $table_prefix;
	var $addition_to_path;
	var $page_titles;
	var $page_links;
	var $page_imgs;
	var $module_name			= 'pages';
	var $default_action			= 'viewnew';
	var $admin_default_action	= 'str';
	var $tpl_path				= 'mod/pages/tpl/';
	var $default_page_id		= -1;
	var $tmpl_img_path			=	'i/admin/tpl/';
	var $tmpl_file_path			=	'tpl/';
	var $block_module_actions		= array();
	var $block_main_module_actions	= array();
	var $action_tpl = array(
	    'viewnew' => 'pages_viewnew.html',
	    'cmenu' => 'pages_cm_menu.html',
	    'showmenu' => 'pages_menu.html',
	    'showsites' => 'pages_sites.html',
	);

	function PagesPrototype($action = '', $transurl = '?', $properties = array(), $prefix = NULL, $rewrite_mod_params = NULL) {
		global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang, $permissions;
		
		/* Get $action, $tpl_name, $permissions */
		extract( $this->init(array(
		        'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
		        'action' => $action, 'tpl_name' => $properties['tpl']
		)), EXTR_OVERWRITE);
		
		$sitemap_type		= (int)$properties[1];										// тип карты сайта
		$context_menu_id	= (int)$properties[2];										// id контекстного меню

		$CONFIG['pages_img_path'] = str_replace("{SITE_PREFIX}", $this->table_prefix, $CONFIG['pages_img_path']);
		
		if ('only_create_object' == $action) {
		    return;
		}
		
		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}

		if (! self::is_admin()) {		
//---------------------------------- обработка действий с сайта --------------------------------//
			switch ($action) {
// Вывод сайтов
			case "showsites":
				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main');
				$tpl->prepare();
				$this->show_sites();
				break;

// Вывод основного меню сайта
			case "showmenu":
				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main');
				$tpl->prepare();
				$this->show_nav_menu();
				break;

// Вывод контекстного меню
			case "cmenu":
				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main');
				$tpl->prepare();
				$this->show_context_menu($context_menu_id);
				break;

// Последние добавления на сайте
			case "viewnew":
				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main');
				$tpl->prepare();
				$this->view_new_pages();
				break;
				
// Вывод заголовка страницы между тегами <title></title>
			case "showheadtitle":
				$main->include_main_blocks($this->module_name."_title.html", "main");
				$tpl->prepare();
				$this->show_head_title();
				break;

// Вывод основного меню сайта с неограниченной вложенностью
			case "showmenufull":
				$main->include_main_blocks($this->module_name.'_menufull.html', 'main');
				$tpl->prepare();
				$this->show_nav_menufull();
				break;

// Вывод под-меню
			case 'showsubmenu':
				$main->include_main_blocks($this->module_name.'_sub_menu.html', 'main');
				$tpl->prepare();
				$this->show_sub_nav_menu();
				break;

// Вывод выпадающего (select) меню сайта
			case "showselectmenu":
				$main->include_main_blocks($this->module_name."_select_menu.html", "main");
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Company_products" => $this->_msg["Company_products"],
				));
				$this->show_selectlist_menu();
				break;

// Вывод списка продуктов в верхнем меню
			case "showtopmenu":
				$main->include_main_blocks($this->module_name."_top_menu.html", "main");
				$tpl->prepare();
				$this->show_topmenu();
				break;

// Вывод контекстного меню управляющего родительским окном
			case "cmenu_parent":
				$main->include_main_blocks($this->module_name."_cm_menu_parent.html", "main");
				$tpl->prepare();
				$this->show_context_menu($context_menu_id);
				break;

// Вывод дерева сайта
			case "sitemap":
				global $core;
				if ($sitemap_type == 1) {
					// показать карту сайта в виде двух колонок
					$main->include_main_blocks($this->module_name."_map_columns.html", "main");
					$tpl->prepare();
					$tpl->assign("search_form_action", $core->formPageLink("search", "search/", $lang));
					$this->show_sitemap_in_columns();
				} elseif($sitemap_type == 2) {
					// показать карту сайта в виде дерева (без js)
					$main->include_main_blocks($this->module_name."_map_fill.html", "main");
					$tpl->prepare();
					$tpl->assign("search_form_action", $core->formPageLink("search", "search/", $lang));
					$this->show_nav_menufull();
				} else {
					// показать карту сайта в виде дерева
					$main->include_main_blocks($this->module_name."_map_tree.html", "main");
					$tpl->prepare();
					$tpl->assign("search_form_action", $core->formPageLink("search", "search/", $lang));
					$this->create_sitemap_tree();
				}

				break;

// Формочка отправки ссылки по почте
			case "sendform":
				global $request_from, $request_email, $request_url;
				if ($request_url && preg_match("~^http:\/\/".$_SERVER["HTTP_HOST"]."~i", $request_url)) {
					$url = $request_url;
					$sess = strstr($url, 'PHPSESSID');
					if ($sess) {
						$url = str_replace("?" . $sess, "", $url);
						$url = str_replace("&" . $sess, "", $url);
					}
					$main->include_main_blocks($this->module_name."_sendform.html", "main");
					$tpl->prepare();
					$tpl->newBlock('block_send_form');
					$tpl->assign(Array(
						"MSG_Enter_your_name_email" => $this->_msg["Enter_your_name_email"],
						"MSG_Enter_recipient_email" => $this->_msg["Enter_recipient_email"],
						"MSG_Info_sendform" => $this->_msg["Info_sendform"],
						"MSG_From_who" => $this->_msg["From_who"],
						"MSG_Email" => $this->_msg["Email"],
						"MSG_Send" => $this->_msg["Send"],
						"MSG_Close_window" => $this->_msg["Close_window"],
						'send_url'			=> $url,
						'form_send_action'	=> $baseurl.'&action=sendurl&onlymain&nocash',
					));
				} else {
					$main->include_main_blocks($this->module_name."_sendurl_message.html", "main");
					$tpl->prepare();
					$tpl->newBlock("block_result_message");
					$tpl->assign(Array(
						"MSG_Close_window" => $this->_msg["Close_window"],
					));
					$tpl->assign("result_message", $this->_msg["Alert_link_not_inner"]);
				}
				break;

// Отправка ссылки по почте
			case "sendurl":
				global $request_from, $request_email, $request_url;
				$main->include_main_blocks($this->module_name."_sendurl_message.html", "main");
				$tpl->prepare();
				if ($this->send_url($request_from, $request_email, $request_url)) {
					$result_message = $this->_msg["Link_successfully_sent"];
				} else {
					$result_message = $this->_msg["Link_send_failure"];
				}
				if ($result_message) {
					$tpl->newBlock("block_result_message");
					$tpl->assign(Array(
						"MSG_Close_window" => $this->_msg["Close_window"],
					));
					$tpl->assign("result_message", $result_message);
				}
				break;

// Вывод пути к странице
			case "showpath":
				$main->include_main_blocks($this->module_name."_path.html", "main");
				$tpl->prepare();
				$this->show_nav_path();
				break;

// Вывод формы регистрации если доступ запрещен и пользователь не авторизован
			case "show_register_form":
				$main->include_main_blocks($this->module_name.'_show_register_form.html', 'main');
				$tpl->prepare();
				$this->show_register_form();
				break;

// Вывод сообщения если группа пользователя не имеет доступа к данной странице
			case 'show_group_denied':
				$main->include_main_blocks($this->module_name.'_show_group_denied.html', 'main');
				$tpl->prepare();
				$tpl->newBlock('block_show_group_denied');
				$tpl->assign(Array(
					"MSG_Access_denied" => $this->_msg["Access_denied"],
					"MSG_You_have_no_rights" => $this->_msg["You_have_no_rights"],
					"MSG_Write_to" => $this->_msg["Write_to"],
					"MSG_Administator" => $this->_msg["Administator"],
					"MSG_to_get_access" => $this->_msg["to_get_access"],
					"MSG_return_back" => $this->_msg["return_back"],
				));
				$tpl->assign($this->getGroupDenied());
				break;

// Меняем местами два блока с id1 и id2
			case 'pagesblockreverce':
				global $request_id1, $request_id2, $_RESULT, $db, $server, $lang;

				$res = 0;
				$id1 = substr($request_id1, 10);
				$id2 = substr($request_id2, 10);
				$sql = $this->get_list_with_rights("pb.id, pb.block_rank",
                                            array (
                                              'pb' =>  $server.$lang."_pages_blocks",
                                              'p'  =>  $server.$lang."_pages",
                                            ),
											 "p",
											"p.id = pb.page_id AND pb.id IN (".$id1.",".$id2.")");
				if ($db->nf() == 2) {
					while ($db->next_record()) {
						$can_edit = ($db->f('user_rights') & 2) >> 1;
						if ($id1 == $db->f('id')) {
							$rank2 = $db->f('block_rank');
						} else {
							$rank1 = $db->f('block_rank');
						}
					}
					// test edit right
					if ($can_edit) {
						$db->query("UPDATE ".$server.$lang."_pages_blocks SET block_rank=".$rank1." WHERE id=".$id1);
						if ($db->affected_rows()) $res++;
						$db->query("UPDATE ".$server.$lang."_pages_blocks SET block_rank=".$rank2." WHERE id=".$id2);
						if ($db->affected_rows()) $res++;
					}
				}
				$_RESULT = array(
					"res"		=> $res,
				);
				exit;
				break;

// �?зменим свойства блока с id и вернем его новый контент
			case 'pagesblockproperties':
				global $core, $request_id, $request_block_prop, $request_tpl, $db, $server, $lang;

				$res = 0; $content = "";
				$sql = $this->get_list_with_rights("p.id",
                                            array (
                                              'pb' =>  $server.$lang."_pages_blocks",
                                              'p'  =>  $server.$lang."_pages",
                                            ),
											"p",
											"p.id = pb.page_id AND pb.id = ".$request_id);
				if ($db->nf() == 1) {
					$fields = array("action", "property1", "property2", "property3", "property4", "property5", "linked_page", "site_id", "lang_id", "tpl");
	                $query = array();
					for ($i=0; $i < sizeof($fields); $i++) {
						if ($i == 6) { // linked page
							if (empty($request_block_prop[$i])) {
								$query[] = $fields[$i]." = NULL";
								$query[] = "linked_page_address = NULL";
							} else {
								$db->query("SELECT address FROM ".$server.$lang."_pages WHERE link = '".$request_block_prop[$i]."'");
								if ($db->nf()) {
									$db->next_record();
									$query[] = $fields[$i]." = '".$request_block_prop[$i]."'";
									$query[] = "linked_page_address = '".$db->f('address')."'";
								} else {
									$query[] = $fields[$i]." = NULL";
									$query[] = "linked_page_address = NULL";
								}
							}
						} elseif ($i > 0 && $i < 6) {
						    $query[] = $fields[$i].(empty($request_block_prop[$i]) ? " = NULL" : " = '".
						        (is_scalar($request_block_prop[$i]) 
						            ? $db->escape($request_block_prop[$i])
						            : mysql_real_escape_string(json_encode($request_block_prop[$i])))
						        ."'");
						} else {
							$query[] = $fields[$i].(empty($request_block_prop[$i]) ? " = NULL" : " = '".$request_block_prop[$i]."'");
						}
					}
					$db->query("UPDATE ".$server.$lang."_pages_blocks SET ".implode(", ", $query)." WHERE id=".$request_id);
					$res = $db->affected_rows();
					list($content, $next_block_id) = empty($res) ? array("", 0) : $core->get_block_content_with_menu($request_id);
				}
				if ($GLOBALS['JsHttpRequest']) {
				    $GLOBALS['_RESULT'] = array(				
    					"res"		=> $res,
    					"content"	=> $content,
    					"next_block"=> $next_block_id,
    				);
				}	
				exit;
				break;

// Добавляем блок с page_id, module_id и block_nam
			case 'pagesblockadd':
				global $core, $request_page_id, $request_module_id, $request_field_num, $_RESULT, $db, $server, $lang;

				$res = 0;
				$content = "";
				$this->get_list_with_rights("p.id",
											$server.$lang."_pages p", "p",
											"p.id = ".$request_page_id);
				if ($db->nf() == 1) {
					$db->next_record();
					// test edit right
					if (($db->f('user_rights') & 2) >> 1) {
						$db->query("SELECT max(block_rank) as max_rank FROM ".$server.$lang."_pages_blocks
									WHERE page_id=".$request_page_id." AND field_number=".$request_field_num);
						$db->next_record();
						$block_rank = $db->f('max_rank') + 1;
						if ($request_field_num > 0 || $block_rank == 1) {
							// для главного поля (0) больше одного блока не добавляем
							$db->query("INSERT INTO ".$server.$lang."_pages_blocks
										SET page_id=".$request_page_id
											.",field_number=".$request_field_num
											.",module_id=".$request_module_id
											.",block_rank=".$block_rank);
							if ($db->affected_rows()) {
								$res++;
								$block_id = $db->lid();
								list($content, $next_block_id) = $core->get_block_content_with_menu($block_id);
								#var_dump($content);
							}
						}
					}
				}
				$_RESULT = array(
					"res"		=> $res,
					"block_id"	=> $block_id,
					"content"	=> $content,
				);
				exit;
				break;

// Удаляем блок с id
			case 'pagesblockdelete':
				global $request_id, $_RESULT, $db, $server, $lang;

				$res = 0;
				$id = substr($request_id, 10);
				$this->get_list_with_rights("pb.id",
                                            array (
                                              'pb' =>  $server.$lang."_pages_blocks",
                                              'p'  =>  $server.$lang."_pages",
                                            ),
											"p",
											"p.id = pb.page_id AND pb.id = ".$id);
				if ($db->nf() == 1) {
					$db->next_record();
					// test delete right
					if (($db->f('user_rights') & 8) >> 3) {
						$db->query("DELETE FROM ".$server.$lang."_pages_blocks WHERE id=".$id);
						if ($db->affected_rows()) $res++;
					}
				}
				$_RESULT = array(
					"res"		=> $res,
				);
				exit;
				break;
                case 'sendPatch':


                    break;
// Default...
			default:
				if (is_object($tpl)) $tpl->prepare();
				return;
			}

//------------------------------- конец обработки действий с сайта -----------------------------//
		} else {
//------------------------------ обработка действий из админ части -----------------------------//
            $this->block_module_actions		= array(
                'viewnew'		=> $this->_msg["Show_new_on_site"],
    			'cmenu'			=> $this->_msg["Show_context_menu"],
  //			'cmenu_parent'	=> $this->_msg["Show_parent_context_menu"],
    			'showmenu'		=> $this->_msg["Show_main_menu"],
    			'showsites'		=> $this->_msg["Show_sites"],
    		);
            $this->block_main_module_actions	= array(
                'sitemap'	=> $this->_msg["Show_site_map"],
    			'cmenu'		=> $this->_msg["Show_context_menu"],
    			'sendform'	=> $this->_msg["Send_link_by_mail"],
    			'showsites'		=> $this->_msg["Show_sites"],
    		);
			if (!isset($tpl)) {
			    $main->message_die(iconv("WINDOWS-1251", "UTF-8", 'Не подключен класс для работы с шаблонами'));
			}
			switch ($action) {
		    case 'prp':
		    	if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step;
				$main->include_main_blocks('_properties.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Information"	=> $main->_msg["Information"],
					"MSG_Value"			=> $main->_msg["Value"],
					"MSG_Save"			=> $main->_msg["Save"],
					"MSG_Cancel"		=> $main->_msg["Cancel"],
				));
		        if (!$request_step || $request_step == 1) {
					$tpl->assign(array('form_action'	=> $baseurl,
									   'step'			=> 2,
									   'lang'			=> $lang,
									   'name'			=> $this->module_name,
									   'action'			=> 'prp'));
					$prp_html = $main->getModuleProperties($this->module_name);
		            $this->main_title = $this->_msg["Controls"];
				} elseif (2 == $request_step) {
					$main->setModuleProperties($this->module_name, $_POST);
					header('Location: '.$baseurl.'&action=prp&step=1');
				}
		    	break;

		     case 'block_prp':
				global $request_sub_action, $request_block_id;
		    	$arr  = $this->getEditLink($request_sub_action, $request_block_id);
				$cont = 'var container = document.getElementById(\'target_span\');
		    		  	 container.innerHTML = "'.$this->getPropertyFields().'";
		    		  	 material_id		  = "'.$arr['material_id'].'";
		    		  	 material_url		  = "'.$arr['material_url'].'";
		    		  	 changeAction();
		    	      	 setURL("property2");';
		       	header('Content-Length: '.strlen($cont));
		    	echo $cont;
		    	exit;
		    	break;
		    	
		    case 'showlist_js':
                $main->include_main_blocks_2($this->module_name.'_showlist_js.html', $this->tpl_path, 'main');
                $tpl->prepare();
                list(, $items) = $this->get_editable_pages_tree();
                if(!is_array($items)) return false;
                foreach($items as $v){
                    $tpl->newBlock('page');
            	    $tpl->assign((array)$v);
                }
              	break;

			case 'showlist':
				if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
				global $request_start, $request_order;
				$start = ($request_start) ? $request_start : 1;
				$main->include_main_blocks_2($this->module_name.'_list.html', $this->tpl_path);
				$tpl->assignInclude('search_form', $tpl_path.'_search_form.html');
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Add_page" => $this->_msg["Add_page"],
					"MSG_Activ" => $this->_msg["Activ"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Title" => $this->_msg["Title"],
					"MSG_Link" => $this->_msg["Link"],
					"MSG_Template" => $this->_msg["Template"],
					"MSG_Bind" => $this->_msg["Bind"],
					"MSG_Strt" => $this->_msg["Strt"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Select_action" => $this->_msg["Select_action"],
					"MSG_Activate" => $this->_msg["Activate"],
					"MSG_Suspend" => $this->_msg["Suspend"],
					"MSG_Delete" => $this->_msg["Delete"],
					"MSG_Rights" => $this->_msg["Rights"],
					"MSG_Execute" => $this->_msg["Execute"],
					'_ROOT.page_add_url'		=> $baseurl.'&action=add&step=1',
					'_ROOT.form_search_action'	=> $baseurl.'&action=search',
					'_ROOT.lang'				=> $lang,
					'_ROOT.name'				=> $this->module_name,
					'_ROOT.action'				=> 'search',
				));
				// Функция для разбивки записей по страницам и вывода навигации по ним
				// �?мена полей и их назначение :
				// ($table = таблица, $where = условие выборки, $blockname = имя блока,
				// $action = добавка к урл, $start = текущая страница/начало выборки,
				// $rows = кол-во строк, $order_by = порядок выборки, $separator = разделитель)
				$pages_cnt = $this->show_pages_list('block_pages_list', 'list', $start, "", $request_order);
				$main->_show_nav_block($pages_cnt, null, 'action='.$action, $start);
				$tpl->assign(Array(
					"MSG_Pages" => $this->_msg["Pages"],
				));
				$this->main_title = $this->_msg["Site_pages_list"];
				break;

// вывести на экран структуру сайта (иерархическое дерево)
			case 'str':
				if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
				global $request_sid;
				$main->include_main_blocks_2($this->module_name.'_sitemap.html', $this->tpl_path);
				$tpl->prepare();
				$dflt_id = $this->get_default_page();
				$tpl->assign(Array(
					"MSG_Info_page_structure" => $this->_msg["Info_page_structure"],
					"MSG_Main_page" => $this->_msg["Main_page"],
					"MSG_Trash" => $this->_msg["Trash"],
					"MSG_Save" => $this->_msg["Save"],
					"MSG_Cancel" => $this->_msg["Cancel"],
					"MSG_Edit" => $this->_msg["Edit"],
                    "MSG_View" => $this->_msg["View"],
					"MSG_Suspend" => $this->_msg["Suspend"],
					"MSG_Delete" => $this->_msg["Delete"],
                    "menu_add_url" => $baseurl.'&action=addcm',
                    "MSG_Add_page" => $this->_msg["Add_page"],
                    '_ROOT.page_add_url'		=> $baseurl.'&action=add&step=1',
                    'default_page_edit_link' => $baseurl.'&action=edit&id='.$dflt_id,
				    'baseurl' => $baseurl
				));
				list($childes, $items) = $this->get_editable_pages_tree();
				$html = $this->show_bound_pages($childes, $items);
				$tpl->assign("_ROOT.locked", in_array(1, $_SESSION["siteuser"]["group_id"]) ? '' : ' locked');
				$tpl->assign("_ROOT.bound_pages", $html);
				$html = $this->show_unbound_pages();
				$tpl->assign("_ROOT.unbound_pages", $html);
				$tpl->assign(
					array(
						'_ROOT.selected_id'	=> (int)$request_sid,
						'_ROOT.form_action'	=> $baseurl.'&action=str_update'
					)
				);
				$this->main_title = $this->_msg["Edit_site_structure"];
				break;

// Обновить структуру сайта
			case 'str_update':
				global $db, $request_bound_pages, $request_unbound_pages, $request_trash_pages;
				$bound_pages = explode(",", $request_bound_pages);
				$unbound_pages = explode(",", $request_unbound_pages);
				$trash_pages = explode(",", $request_trash_pages);
				$i = 0;
				foreach($bound_pages as $k => $v) {
					$tmp = explode("-", trim($v));
					$id = intval($tmp[0]);
					$parent_id = intval($tmp[1]);
					$active = intval($tmp[2]);
					if ($this->test_item_rights("e", "id", $server.$lang."_pages", $parent_id)
						&& $this->test_item_rights("e", "id", $server.$lang."_pages", $id)
					) {
						$this->bind_page($id, $parent_id, $active, --$i);
					}
					//$db->query("UPDATE ".$server.$lang."_pages SET parent_id = '" . $parent_id . "', active = '" . $active . "', page_rank = '-" . ($k + 1) . "' WHERE id = '" . $id . "' LIMIT 1");
				}
				foreach($unbound_pages as $k => $v)
				{
					$tmp = explode("-", trim($v));
					$id = intval($tmp[0]);
					$active = intval($tmp[1]);
					if ($this->test_item_rights("e", "id", $server.$lang."_pages", $id)) {
						$this->unbind_page($id);
					}
					$db->query("UPDATE ".$server.$lang."_pages SET active = ".$active." WHERE id = ".$id);
					//$db->query("UPDATE " . $server . $lang . "_pages SET parent_id = NULL, active = '" . $active . "', page_rank = '1' WHERE id = '" . $id . "' LIMIT 1");
				}
				foreach($trash_pages as $k => $v)
				{
					$id = intval($v);
					if ($this->test_item_rights("d", "id", $server.$lang."_pages", $id, true)) {
						if ($this->delete_page($id)) {
							@$cache->delete_cache_files();
							$lc->change_internal_link_status_to_404($id, 'delete');
						}
					}
					//$db->query("DELETE FROM " . $server . $lang . "_pages WHERE id = '" . $id . "' LIMIT 1");
				}
				$db->query("UPDATE ".$server.$lang."_pages SET page_rank = '0' WHERE is_default = '1' LIMIT 1");
				header("Location: " . $baseurl . "&action=str");
				break;

/*// Редактировать структуру сайта (иерархическое дерево)
			case 'editmap':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_operation, $request_child_id, $request_parent_id;
				if ($request_operation == 'bind') {
					$this->bind_page($request_child_id, $request_parent_id);
//					$sid = $request_child_id;									// выбранная страница
					$sid = $request_parent_id;									// выбранная страница
					$cache->delete_all_cache_files();
				} else if ($request_operation == 'unbind') {
					$this->unbind_page($request_parent_id);
					$sid = $request_child_id;									// выбранная страница
					$cache->delete_all_cache_files();
				} else if ($request_operation == 'up' || $request_operation == 'down') {
					$this->change_order($request_parent_id, $request_operation);
					$sid = $request_parent_id;									// выбранная страница
					@$cache->delete_cache_files();
				}
				if($sid != -1) {
					header('Location: '.$baseurl.'&action=str&sid='.$sid);
				}
				else {
					header('Location: '.$baseurl.'&action=str');
				}
				exit;
				break;
*/
// Добавление новой страницы в базу данных разбивается на три этапа
			case 'add':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);

				global $db, $request_id, $request_step, $request_title, $request_tpl_id,
						$request_alt_title, $request_add_title, $request_link, $request_keywords, $request_description,
						$request_meta, $request_redirect_url, $request_fields_blocks, $request_ret,
						$request_block_properties, $request_permission_type, $request_tryon;

				$request_title = strip_tags($request_title);
				$request_link = strip_tags($request_link);
				$request_keywords = strip_tags($request_keywords);
				$request_description = strip_tags($request_description);
				$request_alt_title = strip_tags($request_alt_title);
				$request_meta = strip_tags($request_meta);
				$request_redirect_url = strip_tags($request_redirect_url);
				// Добавление новой страницы, первый шаг
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name.'_add.html', $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(Array(
						"MSG_Enter_page_name" => $this->_msg["Enter_page_name"],
						"MSG_Enter_link" => $this->_msg["Enter_link"],
						"MSG_Enter_page_type" => $this->_msg["Enter_page_type"],
						"MSG_Such_link_already_exists" => $this->_msg["Such_link_already_exists"],
						"MSG_Reserved_link" => $this->_msg["Reserved_link"],
						"MSG_spaces_in_link" => $this->_msg["spaces_in_link"],
						"MSG_disallowed_symbol" => $this->_msg["disallowed_symbol"],
						"MSG_Page_name" => $this->_msg["Page_name"],
						"MSG_Info_page_name" => $this->_msg["Info_page_name"],
						"MSG_Link" => $this->_msg["Link"],
						"MSG_Info_allowed_symbols" => $this->_msg["Info_allowed_symbols"],
						"MSG_Copy_page_type" => $this->_msg["Copy_page_type"],
						"MSG_Select_page" => $this->_msg["Select_page"],
						"MSG_Info_page_type" => $this->_msg["Info_page_type"],
						"MSG_Select_template" => $this->_msg["Select_template"],
						"MSG_Info_templates" => $this->_msg["Info_templates"],
						"MSG_required_fields" => $this->_msg["required_fields"],
						"MSG_Cancel" => $this->_msg["Cancel"],
						"MSG_Try_on" => $this->_msg["Try_on"],
						"MSG_More" => $this->_msg["More"],
						"MSG_ret" => $request_ret,
					));
					$tpl->assign(
						array(
							'_ROOT.cancel_url'	=>	$baseurl,
							'_ROOT.form_action'	=>	$baseurl.'&action=add&step=2&ret='.$request_ret,
						)
					);
					$this->show_pages_list('block_pages_links', 'all');
					$this->show_templates_list('block_select_template');
					$this->main_title = $this->_msg["Add_page"];
				// Добавление новой страницы, шаг второй
				} else if ($request_step == 2 && $request_title) {
					$main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(Array(
						"MSG_Fill_fields_by_modules" => $this->_msg["Fill_fields_by_modules"],
						"MSG_Choose_field_module_add" => $this->_msg["Choose_field_module_add"],
						"MSG_Click_to_select" => $this->_msg["Click_to_select"],
						"MSG_field" => $this->_msg["field"],
						"MSG_Add_module" => $this->_msg["Add_module"],
						"MSG_Click_to_select_module" => $this->_msg["Click_to_select_module"],
						"MSG_Info_page_activate_bind" => $this->_msg["Info_page_activate_bind"],
						"MSG_Save" => $this->_msg["Save"],
						"MSG_Cancel" => $this->_msg["Cancel"],
						"MSG_Selected_template" => $this->_msg["Selected_template"],
						"MSG_Change_template" => $this->_msg["Change_template"],
						"MSG_Page_name" => $this->_msg["Page_name"],
						"MSG_Link" => $this->_msg["Link"],
						"MSG_Access" => $this->_msg["Access"],
						"MSG_inherit" => $this->_msg["inherit"],
						"MSG_free" => $this->_msg["free"],
						"MSG_limited" => $this->_msg["limited"],
						"MSG_Keywords" => $this->_msg["Keywords"],
						"MSG_Page_description" => $this->_msg["Page_description"],
						"MSG_Page_Title" => $this->_msg["Page_Title"],
						"MSG_Another_meta" => $this->_msg["Another_meta"],
						"MSG_Redirect_url" => $this->_msg["Redirect_url"],
						"MSG_Confirm_delete_this_block" => $this->_msg["Confirm_delete_this_block"],
						"MSG_Alert_main_block_single" => $this->_msg["Alert_main_block_single"],
						"MSG_Alert_main_block_module" => $this->_msg["Alert_main_block_module"],
						"MSG_Delete" => $this->_msg["Delete"],
						"MSG_Properties" => $this->_msg["Properties"],
						"MSG_Up" => $this->_msg["Up"],
						"MSG_Down" => $this->_msg["Down"],
						"MSG_Enter_page_name" => $this->_msg["Enter_page_name"],
						"MSG_Enter_link" => $this->_msg["Enter_link"],
						"MSG_spaces_in_link" => $this->_msg["spaces_in_link"],
						"MSG_disallowed_symbol" => $this->_msg["disallowed_symbol"],
						"MSG_place" => $this->_msg["place"],
						"MSG_id" => $this->_msg["id"],
						"MSG_module" => $this->_msg["module"],
						"MSG_Try_on" => $this->_msg["Try_on"],
						"MSG_Img_1" => $this->_msg["Img_1"],
						"MSG_Img_2" => $this->_msg["Img_2"],
						"MSG_Img_3" => $this->_msg["Img_3"],
					));
					
    				$pages_links = $db->fetchColumn($this->table_prefix.'_pages', 'link', array('clause' => array(
    				    new DbExp("link!='".My_Sql::escape($_page['link'])."'"
    				))));
    				$tpl->assign('_ROOT.json_pages_links', json_encode($pages_links));
    				
					$this->show_module_list('block_select_modules');
					$this->show_module_list('block_modules_array', false);
					// создание страницы либо по новому шаблону (request_tpl_id),
					// либо по типу уже существующей страницы (request_id)
					if ($request_id) {
						$_page = $this->show_page($request_id);
						$tpl_id = $_page['tpl_id'];
						// получить информацию о выбранном шаблоне
						// получить кол-во полей в шаблоне и узнать, есть ли главный модуль
						list($main_module, $number_of_fields) = $this->show_template($tpl_id, '_ROOT.');
						// вывести поля
						$this->show_fields('block_fields', $main_module, $number_of_fields);
						// сформировать массивы элементами которых являются параметры блоков
						$begin = ($main_module) ? 0 : 1;
						for ($i = $begin; $i <= $number_of_fields; $i++) {
							$this->show_blocks('block_array_blocks', $request_id, $i);
						}
					} elseif (isset($request_tpl_id) && is_numeric($request_tpl_id)) {
						// получить информацию о выбранном шаблоне
						// получить кол-во полей в шаблоне и узнать, есть ли главный модуль
						list($main_module, $number_of_fields) = $this->show_template($request_tpl_id, '_ROOT.');
						// вывести поля
						$this->show_fields('block_fields', $main_module, $number_of_fields);
					} else {
							header('Location: '.$baseurl.'&action=add');
							exit;
//						$main->message_die("не выбран шаблон страницы!");
					}
					// вывести данные из формы с предыдущей страницы,
					// а также информацию о выбранном шаблоне
					$tpl->assign(
						array(
							'_ROOT.title'				=> htmlspecialchars($request_title),
							'_ROOT.link'				=> htmlspecialchars($request_link),
							'_ROOT.p2_checked'			=> 'checked',
							'_ROOT.perm_class'			=> 'inh',
							'_ROOT.form_action'			=> $baseurl.'&action=add&step=3&ret='.$request_ret,
							'_ROOT.number_of_fields'	=> $number_of_fields,
							'_ROOT.main_module'			=> $main_module,
							'_ROOT.language'			=> $lang,
						)
					);
					$this->main_title = $this->_msg["Add_page"];
				// Добавление новой страницы, шаг третий: занесение в базу данных
				} else if ($request_step == 3 && $request_title) {
					if ($_FILES['image']['name']) {
						$imagename = $main->upload_file('image', $CONFIG['files_img_max_size'], $CONFIG['pages_img_path']);
					}
					$lid = $this->add_page($request_title, $request_add_title, $request_alt_title, $request_link, $request_keywords, $request_description, $request_meta, $request_redirect_url, $imagename, $request_tpl_id, $request_fields_blocks, $request_block_properties, $request_permission_type);
					if ($lid) {
					    $lc->change_internal_link_status_to_200($lid, 'add', $request_link);
					    if ($request_tryon) {
					    	header('Location: '.$baseurl.'&action=edit&id='.$lid);
					    	exit;
					    }
					}

					header('Location: '.$baseurl.'&action='.$this->admin_default_action);
					exit;
				}
				break;

// Вывод на экран и редактирование страницы
			case 'edit':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start, $db;
				if (!$this->test_item_rights("e", "id", $server.$lang."_pages", $request_id))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Fill_fields_by_modules" => $this->_msg["Fill_fields_by_modules"],
					"MSG_Choose_field_module_add" => $this->_msg["Choose_field_module_add"],
					"MSG_Click_to_select" => $this->_msg["Click_to_select"],
					"MSG_field" => $this->_msg["field"],
					"MSG_Add_module" => $this->_msg["Add_module"],
					"MSG_Click_to_select_module" => $this->_msg["Click_to_select_module"],
					"MSG_Info_page_activate_bind" => $this->_msg["Info_page_activate_bind"],
					"MSG_Save" => $this->_msg["Save"],
					"MSG_Cancel" => $this->_msg["Cancel"],
					"MSG_Selected_template" => $this->_msg["Selected_template"],
					"MSG_Change_template" => $this->_msg["Change_template"],
					"MSG_Page_name" => $this->_msg["Page_name"],
					"MSG_Link" => $this->_msg["Link"],
					"MSG_Access" => $this->_msg["Access"],
					"MSG_inherit" => $this->_msg["inherit"],
					"MSG_free" => $this->_msg["free"],
					"MSG_limited" => $this->_msg["limited"],
					"MSG_Keywords" => $this->_msg["Keywords"],
					"MSG_Page_description" => $this->_msg["Page_description"],
					"MSG_Page_Title" => $this->_msg["Page_Title"],
					"MSG_Another_meta" => $this->_msg["Another_meta"],
					"MSG_Redirect_url" => $this->_msg["Redirect_url"],
					"MSG_Confirm_delete_this_block" => $this->_msg["Confirm_delete_this_block"],
					"MSG_Alert_main_block_single" => $this->_msg["Alert_main_block_single"],
					"MSG_Alert_main_block_module" => $this->_msg["Alert_main_block_module"],
					"MSG_Delete" => $this->_msg["Delete"],
					"MSG_Properties" => $this->_msg["Properties"],
					"MSG_Up" => $this->_msg["Up"],
					"MSG_Down" => $this->_msg["Down"],
					"MSG_Enter_page_name" => $this->_msg["Enter_page_name"],
					"MSG_Enter_link" => $this->_msg["Enter_link"],
					"MSG_spaces_in_link" => $this->_msg["spaces_in_link"],
					"MSG_disallowed_symbol" => $this->_msg["disallowed_symbol"],
					"MSG_place" => $this->_msg["place"],
					"MSG_id" => $this->_msg["id"],
					"MSG_module" => $this->_msg["module"],
					"MSG_Try_on" => $this->_msg["Try_on"],
					"MSG_Img_1" => $this->_msg["Img_1"],
					"MSG_Img_2" => $this->_msg["Img_2"],
					"MSG_Img_3" => $this->_msg["Img_3"],
					"MSG_Such_link_already_exists" => $this->_msg["Such_link_already_exists"],
				));
				$_page = $this->show_page($request_id);
				$tpl_id = $_page['tpl_id'];
				if (!isset($tpl_id) || !is_numeric($tpl_id)) {
					header('Location: '.$baseurl.'&action=add');
					exit;
				}
				$pages_links = $db->fetchColumn($this->table_prefix.'_pages', 'link', array('clause' => array(
				    new DbExp("link!='".My_Sql::escape($_page['link'])."'"
				))));
				$tpl->assign('_ROOT.json_pages_links', json_encode($pages_links));
				
				$this->show_module_list('block_select_modules');
				$this->show_module_list('block_modules_array', false);
				// получить информацию о выбранном шаблоне
				// получить кол-во полей в шаблоне и узнать, есть ли главный модуль
				list($main_module, $number_of_fields) = $this->show_template($tpl_id, '_ROOT.');

				$tpl->assign(
					array(
						'_ROOT.form_action'			=> $baseurl.'&action=update&start='.$request_start,
						'_ROOT.properties_window'	=> $baseurl.'&action=properties',
						'_ROOT.readonly'			=> 'readonly class=disa',
						'_ROOT.number_of_fields'	=> $number_of_fields,
						'_ROOT.main_module'			=> $main_module,
						'_ROOT.language'			=> $lang,
						'_ROOT.chage_tpl_link'		=> $baseurl.'&action=tmplchange&id='.$request_id.'&start='.$request_start,
					)
				);
				// вывести поля
				#$this->show_actions_list();
				$this->show_fields('block_fields', $main_module, $number_of_fields);
				// сформировать массивы элементами которых являются параметры блоков
				$begin = ($main_module) ? 0 : 1;
				for ($i = $begin; $i <= $number_of_fields; $i++) {
					$this->show_blocks('block_array_blocks', $request_id, $i);
				}
				$this->show_page_files($request_id);
				$this->main_title = $this->_msg["Edit_page"];
				break;

// Обновление страницы
			case 'update':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start,
						$request_title, $request_alt_title, $request_add_title, $request_keywords,
						$request_description, $request_meta, $request_redirect_url,
						$imagename, $request_fields_blocks, $request_block_properties,
						$request_permission_type, $request_old_ptype, $request_tryon,
				        $request_link, $request_main_access;
//print_r($_REQUEST); exit;
				if (!$this->test_item_rights("e", "id", $server.$lang."_pages", $request_id))
					$main->message_access_denied($this->module_name, $action);

				if ($_FILES['image']['name']) {
					$imagename = $main->upload_file('image', $CONFIG['files_img_max_size'], $CONFIG['pages_img_path']);
				}

				if ($this->update_page($request_title,
				                        $request_add_title,
										$request_alt_title,
										$request_keywords,
										$request_description,
										$request_meta,
										$request_redirect_url,
										$imagename,
										$request_fields_blocks,
										$request_block_properties,
										$request_id,
										$request_permission_type,
										$request_old_ptype,
										$request_link,
				                        $request_main_access
				        )
				) {
				    $this->update_page_images($request_id);
					@$cache->delete_cache_files($request_id);
					if ($request_tryon) {
					    header('Location: '.$baseurl.'&action=edit&id='.$request_id.($request_start > 1 ? "&start=".$request_start : ''));
					    exit;
					}
				}
				header('Location: '.$baseurl.'&action='.$this->admin_default_action.'&start='.$request_start);
				exit;
				break;

// Удаляем страницу
			case "del":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				if (!$this->test_item_rights("d", "id", $server.$lang."_pages", $request_id))
					$main->message_access_denied($this->module_name, $action);

				if ($this->delete_page($request_id)) {
					@$cache->delete_cache_files();
					$lc->change_internal_link_status_to_404($request_id, 'delete');
				}
				header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				exit;
				break;

// Отключение публикации
			case "suspend":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				if (!$this->test_item_rights("p", "id", $server.$lang."_pages", $request_id))
					$main->message_access_denied($this->module_name, $action);

				if ($main->suspend($server.$lang.'_pages', $request_id)) {
					@$cache->delete_cache_files();
					$lc->change_internal_link_status_to_404($request_id, 'suspend');
				}
				header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				exit;
				break;

// Включение публикации
			case "activate":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				if (!$this->test_item_rights("p", "id", $server.$lang."_pages", $request_id))
					$main->message_access_denied($this->module_name, $action);

				if ($main->activate($server.$lang.'_pages', $request_id)) {
					@$cache->delete_cache_files();
					$lc->change_internal_link_status_to_200($request_id, 'activate');
				}
				header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				exit;
				break;


// Удалить выбранные страницы
			case "delete_checked":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_start;
				if (is_array($request_check)) {
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("d", "id", $server.$lang."_pages", $v)) {
							if ($this->delete_page($v)) {
								@$cache->delete_cache_files();
								$lc->change_internal_link_status_to_404($v, 'delete');
							}
						}
					}
				}
				header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				exit;
				break;

// Отключить публикацию выбранных страниц
			case "suspend_checked":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_start;
				if (is_array($request_check)) {
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $server.$lang."_pages", $v)) {
							if ($main->suspend($server.$lang.'_pages', $v)) {
								@$cache->delete_cache_files();
								$lc->change_internal_link_status_to_404($v, 'suspend');
							}
						}
					}
				}
				header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				exit;
				break;

// Включить публикацию выбранных страниц
			case "activate_checked":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_start;
				if (is_array($request_check)) {
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $server.$lang."_pages", $v)) {
							if ($main->activate($server.$lang.'_pages', $v)) {
								@$cache->delete_cache_files();
								$lc->change_internal_link_status_to_200($v, 'activate');
							}
						}
					}
				}
				header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				exit;
				break;



// Сделать страницу главной
			case "setdefault":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				if (!$this->test_item_rights("e", "id", $server.$lang."_pages", $request_id))
					$main->message_access_denied($this->module_name, $action);

				$old_id = $this->set_default_page($request_id);
				if ($old_id) {
					@$cache->delete_cache_files($old_id);
					@$cache->delete_cache_files($request_id);
				}
				header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				exit;
				break;

// Вывод списка страниц для создания ссылки в визуальном редакторе
			case "choice":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks($this->module_name.'_choice.html', 'main');
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Insert_link_on_page" => $this->_msg["Insert_link_on_page"],
					"MSG_Link_on_page" => $this->_msg["Link_on_page"],
					"MSG_Select_page" => $this->_msg["Select_page"],
				));
//				$tpl->assign("web_address", $CONFIG["web_address"]);
				$this->show_pages_list('block_pages_list', 'active');
				$this->main_title = $this->_msg["Site_pages_list"];
				break;

// Вывод списка битых внутренних ссылок
			case "ilinks":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_type, $request_start;
				if (!$request_type) $request_type = "all";
				$start	= ($request_start) ? (int)$request_start : 1;
				$rows	= 10;
				$main->include_main_blocks_2($this->module_name.'_internal_links.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Show_all_internal_links" => $this->_msg["Show_all_internal_links"],
					"MSG_Show_only_broken_links" => $this->_msg["Show_only_broken_links"],
				));
				$tpl->assign('redirect_link', $baseurl.'&action=ilinks&type=');
				$tpl->assign('request_type', $request_type);
				if ($request_type == 'broken') {
					$where_str = ' AND status = "404"';
				} else {
					$where_str = '';
				}
				$main->_show_nav_string($server.$lang.'_pages_internal_links',
										$where_str, '',
										'action='.$action.'&type='.$request_type,
										$start, $rows);
				$tpl->assign(Array(
					"MSG_Pages" => $this->_msg["Pages"],
				));
				$this->show_internal_links($start, $rows, $request_type);
				$this->main_title = $this->_msg["Internal_links"];
				break;

// Вывод списка контекстных меню
			case "cmlist":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2($this->module_name.'_cm_list.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Add_context_menu" => $this->_msg["Add_context_menu"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Rights" => $this->_msg["Rights"],
					"MSG_Context_menu" => $this->_msg["Context_menu"],
					"MSG_Del" => $this->_msg["Del"],
				));
				$tpl->assign('menu_add_url', $baseurl.'&action=addcm');
				$this->show_context_menu_list();
				$this->main_title = $this->_msg["Context_menus"];
				break;

// Добавить контекстное меню
			case "addcm":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_mname, $request_items, $request_step;
				// Добавление новой записи, первый шаг
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name."_cm_edit.html", $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(Array(
						"MSG_Enter_menu_name" => $this->_msg["Enter_menu_name"],
						"MSG_Menu_name" => $this->_msg["Menu_name"],
						"MSG_Add_link_to_menu" => $this->_msg["Add_link_to_menu"],
						"MSG_Select_page" => $this->_msg["Select_page"],
						"MSG_Add" => $this->_msg["Add"],
						"MSG_Confirm_delete_menu_item" => $this->_msg["Confirm_delete_menu_item"],
						"MSG_Choose_page" => $this->_msg["Choose_page"],
						"MSG_Page_already_in_menu" => $this->_msg["Page_already_in_menu"],
						"MSG_Delete" => $this->_msg["Delete"],
						"MSG_Up" => $this->_msg["Up"],
						"MSG_Down" => $this->_msg["Down"],
						"MSG_place" => $this->_msg["place"],
						"MSG_id" => $this->_msg["id"],
						"MSG_value" => $this->_msg["value"],
						"MSG_required_fields" => $this->_msg["required_fields"],
						"MSG_Save" => $this->_msg["Save"],
						"MSG_Cancel" => $this->_msg["Cancel"],
						'_ROOT.form_action'	=>	$baseurl.'&action=addcm&step=2',
					));
					$this->show_pages_list('block_pages_list', 'active');
					$this->main_title = $this->_msg["Add_context_menu"];
				// Добавление новой записи, шаг второй
				} else if ($request_step == 2) {
					$this->add_context_menu($request_mname, $request_items);
					header('Location: '.$baseurl.'&action=cmlist');
				}
				break;

// Вывод контекстного меню на экран для редактирования
			case "editcm":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_id;
				if (!$this->test_item_rights("e", "id", $server.$lang."_pages_context_menu", $request_id))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name.'_cm_edit.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Enter_menu_name" => $this->_msg["Enter_menu_name"],
					"MSG_Menu_name" => $this->_msg["Menu_name"],
					"MSG_Add_link_to_menu" => $this->_msg["Add_link_to_menu"],
					"MSG_Select_page" => $this->_msg["Select_page"],
					"MSG_Add" => $this->_msg["Add"],
					"MSG_Confirm_delete_menu_item" => $this->_msg["Confirm_delete_menu_item"],
					"MSG_Choose_page" => $this->_msg["Choose_page"],
					"MSG_Page_already_in_menu" => $this->_msg["Page_already_in_menu"],
					"MSG_Delete" => $this->_msg["Delete"],
					"MSG_Up" => $this->_msg["Up"],
					"MSG_Down" => $this->_msg["Down"],
					"MSG_place" => $this->_msg["place"],
					"MSG_id" => $this->_msg["id"],
					"MSG_value" => $this->_msg["value"],
					"MSG_required_fields" => $this->_msg["required_fields"],
					"MSG_Save" => $this->_msg["Save"],
					"MSG_Cancel" => $this->_msg["Cancel"],
					'_ROOT.form_action'	=>	$baseurl.'&action=updatecm',
				));
				$this->show_context_menu_adm($request_id);
				$this->show_pages_list('block_pages_list', 'active');
				$this->main_title = $this->_msg["Edit_context_menu"];
				break;

// Сделать обновление контекстного меню
			case "updatecm":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_mname, $request_id, $request_items;
				if (!$this->test_item_rights("e", "id", $server.$lang."_pages_context_menu", $request_id))
					$main->message_access_denied($this->module_name, $action);

				$this->update_context_menu($request_mname, $request_id, $request_items);
				header('Location: '.$baseurl.'&action=cmlist');
				break;

// Удаляем контекстное меню
			case "delcm":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_id;
				if (!$this->test_item_rights("d", "id", $server.$lang."_pages_context_menu", $request_id, true))
					$main->message_access_denied($this->module_name, $action);

				if ($this->delete_context_menu($request_id));
				header('Location: '.$baseurl.'&action=cmlist');
				break;

// Редактирование прав контекстного меню
			case 'editcmrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id;
				if (!$this->test_item_rights("e", "id", $server.$lang."_pages_context_menu", $request_id, true))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($server.$lang."_pages_context_menu", "name", $request_id, '&id='.$request_id, $this->_msg["Context_menu"], 'savecmrights')) {
					header('Location: '.$baseurl.'&action=cmlist');
				}
				break;

// Сохранение прав контекстного меню
			case 'savecmrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start, $request_rights;
				if (!$this->test_item_rights("e", "id", $server.$lang."_pages_context_menu", $request_id, true))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($server.$lang."_pages_context_menu", $request_id, $request_rights);
				header('Location: '.$baseurl.'&action=cmlist');
				break;

// Вывод результата поиска страницы
			case "search":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_search;
				$search = strip_tags(trim($request_search));
				$main->include_main_blocks_2($this->module_name.'_list.html', $this->tpl_path);
				$tpl->assignInclude('search_form', 'tpl/admin/_search_form.html');
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Add_page" => $this->_msg["Add_page"],
					"MSG_Activ" => $this->_msg["Activ"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Title" => $this->_msg["Title"],
					"MSG_Link" => $this->_msg["Link"],
					"MSG_Template" => $this->_msg["Template"],
					"MSG_Bind" => $this->_msg["Bind"],
					"MSG_Strt" => $this->_msg["Strt"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Select_action" => $this->_msg["Select_action"],
					"MSG_Activate" => $this->_msg["Activate"],
					"MSG_Rights" => $this->_msg["Rights"],
					"MSG_Suspend" => $this->_msg["Suspend"],
					"MSG_Delete" => $this->_msg["Delete"],
					"MSG_Execute" => $this->_msg["Execute"],
				));
				$result = $this->show_pages_list('block_pages_list', 'search', '', '', '', $search);
				if (!$result) $no_res = $this->_msg["Didnt_match_anything"];
				$tpl->assign(
					array(
						'_ROOT.page_add_url'		=> $baseurl.'&action=add&step=1',
						'_ROOT.form_search_action'	=>	$baseurl.'&action=search',
						'_ROOT.lang'				=>	$lang,
						'_ROOT.name'				=>	$this->module_name,
						'_ROOT.action'				=>	'search',
						'_ROOT.search'				=>	$search,
						'_ROOT.search_result'		=>	$this->_msg["Search_results"] . ' "'.$search.'": '.$no_res,
					)
				);
				$this->main_title = $this->_msg["Page_search_results"];
				break;

// Редактирование свойств блока
			case 'properties':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_field, $request_block_id, $title;
				$main->include_main_blocks('_block_properties.html', 'main');
				$tpl->prepare();

				if ($main->show_actions($request_field, '', $this->block_main_module_actions, $this->block_module_actions));

				$tpl->assign(array(	'_ROOT.title'		=> $title,
									'_ROOT.username'	=> $_SESSION['session_login'],
									'_ROOT.password'	=> $_SESSION['session_password'],
									'_ROOT.name'		=> $this->module_name,
									'_ROOT.lang'		=> $lang,
									'_ROOT.block_id'	=> $request_block_id,
									));
				$this->main_title	= $this->_msg["Block_properties"];
				break;

// Редактирование прав страницы
			case 'editrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
                if (!$this->test_item_rights("e", "id", $server.$lang."_pages", $request_id, true))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($server.$lang."_pages", "title", $request_id, "&id=".$request_id.(($request_start)?'&start='.$request_start:''),  $this->_msg["Page"])) {
					header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				}
				break;

// Сохранение прав страницы
			case 'saverights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start, $request_rights;
				if (!$this->test_item_rights("e", "id", $server.$lang."_pages", $request_id, true))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($server.$lang."_pages", $request_id, $request_rights);
				header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				break;

/**
 * Шаблоны
 */

/* �?зменить шаблон страницы */
			case 'tmplchange':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);

				global $request_step, $request_id, $request_start, $request_tpl_id;
				if (!$request_step) {
					$main->include_main_blocks_2($this->module_name.'_tmpl_change.html', $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(Array(
						"MSG_Choose_template" => $this->_msg["Choose_template"],
						"MSG_Page_name" => $this->_msg["Page_name"],
						"MSG_Page_template" => $this->_msg["Page_template"],
						"MSG_Select_template" => $this->_msg["Select_template"],
						"MSG_Info_different_fields_num" => $this->_msg["Info_different_fields_num"],
						"MSG_required_fields" => $this->_msg["required_fields"],
						"MSG_Save" => $this->_msg["Save"],
						"MSG_Cancel" => $this->_msg["Cancel"],
					));
					/* Отображение инф-ии о странице */
					$_page = $this->show_page($request_id);
					$tpl_id = $_page['tpl_id'];
					if (!isset($tpl_id) || !is_numeric($tpl_id)) {
						header('Location: '.$baseurl.'&action=add');
						exit;
					}

					$tpl->assign(array(
							'form_action' => $baseurl.'&action='.$action.'&step=2',
							'start' => $request_start
						)
					);

					/* Оторражение списка шаблонов */
					$this->show_templates_list('block_tpls_list', $tpl_id);
					$this->main_title	= $this->_msg["Change_template"];
				} elseif (2 == $request_step) {
					$this->change_page_tmpl($request_id, $request_tpl_id);
					header('Location: '.$baseurl.'&action=edit&id='.$request_id.'&start='.$request_start);
					exit;
				}

				break;

/* Вывод списка шаблонов */
			case 'tmpllist':
				if (!$permissions["r"]) {
					$main->message_access_denied($this->module_name, $action);
				}
				$main->include_main_blocks_2($this->module_name.'_tmpl_list.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Add_template" => $this->_msg["Add_template"],
					"MSG_ID" => $this->_msg["ID"],
					"MSG_Name" => $this->_msg["Name"],
					"MSG_Fields_num" => $this->_msg["Fields_num"],
					"MSG_With_main_module" => $this->_msg["With_main_module"],
					"MSG_Filename" => $this->_msg["Filename"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Info_delete_template" => $this->_msg["Info_delete_template"],
				));
				$tpl->assign('_ROOT.tmpl_add_url', $baseurl."&action=tmpladd");
				$this->show_templates_list("block_tmpls_list");
				$this->show_tpl_files();
				$this->main_title = $this->_msg["Site_templates_list"];

				break;

/* Добавление шаблона */
			case 'tmpladd':
				if (!$permissions['e'] || !$this->is_group_owner()) {
					$main->message_access_denied($this->module_name, $action);
				}
				global $request_step, $request_title, $request_fields_cnt, $request_main_module;
				// Добавление шаблона, первый шаг
				if (!$request_step) {
					$main->include_main_blocks_2($this->module_name.'_tmpl_add.html', $this->tpl_path);
					$tpl->prepare();
					if (!$this->is_writable_tpl_file_dir() || !$this->is_writable_tpl_img_dir()) {
						$tpl->newBlock('block_error_msg');
						$tpl->assign(Array(
							"MSG_Info_add_template" => $this->_msg["Info_add_template"],
							"MSG_and" => $this->_msg["and"],
						));
						$tpl->assign(
							array(
								'tmpl_file_path' => "/".$this->tmpl_file_path.SITE_PREFIX,
								'tmpl_img_path'  => "/".$this->tmpl_img_path.SITE_PREFIX
							)
						);
					} else {
						$tpl->newBlock('block_tmpl_form');
						$tpl->assign(Array(
							"MSG_Enter_template_name" => $this->_msg["Enter_template_name"],
							"MSG_Enter_fields_num" => $this->_msg["Enter_fields_num"],
							"MSG_Fields_num_must_be_positive" => $this->_msg["Fields_num_must_be_positive"],
							"MSG_Enter_template_image" => $this->_msg["Enter_template_image"],
							"Template_image_must_be_GIF" => $this->_msg[""],
							"MSG_Enter_template_file" => $this->_msg["Enter_template_file"],
							"MSG_Template_file_must_be_HTML" => $this->_msg["Template_file_must_be_HTML"],
							"MSG_Template_name" => $this->_msg["Template_name"],
							"MSG_Fields_num" => $this->_msg["Fields_num"],
							"MSG_Info_fields" => $this->_msg["Info_fields"],
							"MSG_With_main_block_fields" => $this->_msg["With_main_block_fields"],
							"MSG_Yes" => $this->_msg["Yes"],
							"MSG_No" => $this->_msg["No"],
							"MSG_Template_image" => $this->_msg["Template_image"],
							"MSG_Template_file" => $this->_msg["Template_file"],
							"MSG_required_fields" => $this->_msg["required_fields"],
							"MSG_Save" => $this->_msg["Save"],
							"MSG_Cancel" => $this->_msg["Cancel"],
						));
						$tpl->assign(
							array(
								'cancel_url'	=>	$baseurl,
								'form_action'	=>	$baseurl.'&action=tmpladd&step=2',
							)
						);
					}
					$this->main_title = $this->_msg["Add_template"];
				// Добавление шаблона, шаг второй
				} else if ($request_step == 2) {
					$this->add_tmpl($request_title, $request_fields_cnt, $request_main_module);
					header('Location: '.$baseurl.'&action=tmpllist');
				}

				break;

/* Редактировать файл шаблона */
			case 'edittpl':
				if (!$permissions["r"]) {
					$main->message_access_denied($this->module_name, $action);
				}
				global $request_filename;
				$main->include_main_blocks_2("{$this->module_name}_{$action}.html", $this->tpl_path);
				$tpl->prepare();
				if ($request_filename && file_exists(RP."{$this->tmpl_file_path}{$this->table_prefix}/{$request_filename}")) {
				    $tpl->newBlock('block_edit');
				    $tpl->assign(array(
				        'baseurl' => $baseurl,
				        'filename' => $request_filename,
				        'filepath' => "/{$this->tmpl_file_path}{$this->table_prefix}/$request_filename",
				        'content' => htmlspecialchars(file_get_contents(RP."{$this->tmpl_file_path}{$this->table_prefix}/{$request_filename}")),
				        "MSG_Save" => $this->_msg["Save"],
						"MSG_Cancel" => $this->_msg["Cancel"],
						"MSG_Try_on" => $this->_msg["Try_on"]
				    ));
				} else {
				    $tpl->newBlock('block_not_found');
				    $tpl->assign(array(
				        "MSG_File_not_found" => $this->_msg["File_not_found"]
				    ));
				}
				$this->main_title = $this->_msg["Edit_tpl"];

				break;

/* Редактировать файл шаблона */
			case 'updatetpl':
				if (!$permissions["e"]) {
					$main->message_access_denied($this->module_name, $action);
				}
				global $request_filename, $request_content, $request_tryon;
				if ($request_content && $request_filename && file_exists(RP."{$this->tmpl_file_path}{$this->table_prefix}/{$request_filename}")
				    && is_writable(RP."{$this->tmpl_file_path}{$this->table_prefix}/{$request_filename}")
				) {
				    $fp = fopen(RP."{$this->tmpl_file_path}{$this->table_prefix}/{$request_filename}", 'w');
				    if ($fp) {
				        fwrite($fp, $request_content);
				    }
				    fclose($fp);
				}
				if ($request_tryon) {
				    header("Location: {$baseurl}&action=edittpl&filename={$request_filename}");
				} else {
				    header("Location: {$baseurl}&action=tmpllist");
				}
				exit;

/**
 * Удаление шаблона.
 * Только если не существует страниц использующих этот шаблон
 */
			case 'tmpldel':
				if (!$permissions['d'] || !$this->is_group_owner()) {
					$main->message_access_denied($this->module_name, $action);
				}
				global $request_id;
				$this->del_tmpl($request_id);
				header("Location: ".$baseurl."&action=tmpllist");

				break;

			case 'sitemap_xml':
				$main->include_main_blocks_2("{$this->module_name}_{$action}.html", $this->tpl_path);
				$tpl->prepare();
				
				if (isset($_REQUEST['generate']) && $GLOBALS['JsHttpRequest']) {
				    $comand = '';
				    if ($_REQUEST['start']) {
				        $comand = 'start';
				    }
				    $GLOBALS['_RESULT'] = self::genereteSitemap($comand, array('step' => $_REQUEST['step']));
				    exit;
				}
				
				$file_path = $CONFIG['pages_sitemap_path'].($CONFIG['main_domen'] == $CONFIG['main_domen'] ? 'sitemap.xml' : $CONFIG['site_pref'].'_sitemap.xml');
				$assign = array(
					"baseurl" => $baseurl,
					"action" => $action,
					"MSG_Prioritet_step" => $this->_msg["Prioritet_step"],
					"MSG_What_is" => $this->_msg["What_is"],
					"MSG_Title" => $this->_msg["Title"],
					"MSG_Generatin_date" => $this->_msg["Generatin_date"],
					"MSG_Size" => $this->_msg["Size"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Delete" => $this->_msg["Delete"],
					"MSG_Create" => $this->_msg["Create"],
					'sitemap_xml_path' => '',
					'size' => '',
					'date' => '',
				);
				if (file_exists(RP.$file_path)) {
				    $assign['sitemap_xml_path'] = '/'.$file_path;
				    $assign['size'] = number_format(filesize(RP.$file_path)/1024, 2, '.', ' ').' Kb';
				    $assign['date'] = date('Y-m-d H:i:s', filemtime(RP.$file_path));
				}
				$tpl->assign($assign);
				Module::show_select('step_list', $CONFIG['pages_prioritet_step_default_key'], '', $CONFIG['pages_prioritet_step']);
				$this->main_title = $this->_msg['sitemap_xml'];
				break;
				
			case 'sitemap_xml_del':
			    if (!$permissions['d'] || !$this->is_group_owner()) {
					$main->message_access_denied($this->module_name, $action);
				}
				
				$file_path = RP.$CONFIG['pages_sitemap_path'].($CONFIG['main_domen'] == $CONFIG['main_domen'] ? 'sitemap.xml' : $CONFIG['site_pref'].'_sitemap.xml');
				if (file_exists($file_path)) {
				    unlink($file_path);
				}
				Module::go_back();
				exit;
				
			case 'clean_field':
				$main->include_main_blocks_2("{$this->module_name}_group_acts.html", $this->tpl_path);
				$tpl->prepare();
				global $db;
				
				if (empty($_REQUEST['ids'])) {
				    Module::go_back($baseurl.'&action=str');
				}
				
				$pages_ids = array_map('intval', explode(',', $_REQUEST['ids']));
				$page_list = $db->getArrayOfResult("SELECT id, title FROM {$this->table_prefix}_pages WHERE id IN (".
				        implode(',', $pages_ids).")");
				if (empty($page_list)) {
				    Module::go_back($baseurl.'&action=str');
				}
				$pages_ids = array();
				foreach ($page_list as $val) {
				    $pages_ids[] = $val['id'];
				}
				$this->getPage($pages_ids[0]);
				$db->next();
				$page = $db->Record;
				
				if (isset($_POST['field'])) {
				    $this->clearPagesBlock($_POST['ids'], $_POST['field']);
				    Module::go_back($baseurl.'&action='.($_REQUEST['fine_tuning'] ? 'fine_tuning' : $action)
				        .'&ids='.implode(',', $pages_ids));
				}
				
				$mods_mete = $this->getModsMeta();
				$tpl->gotoBlock('_ROOT');
				$tpl->assign(Array(
				        "MSG_Click_to_select" => $this->_msg["Click_to_select"],
				        "MSG_field" => $this->_msg["field"],
				        "MSG_Add_module" => $this->_msg["Add_module"],
				        "MSG_Click_to_select_module" => $this->_msg["Click_to_select_module"],
				        "MSG_Save" => $this->_msg["Save"],
				        "MSG_Cancel" => $this->_msg["Cancel"],
				        "MSG_Confirm_delete_this_block" => $this->_msg["Confirm_delete_this_block"],
				        "MSG_Delete" => $this->_msg["Delete"],
				        "MSG_Properties" => $this->_msg["Properties"],
				        "MSG_Up" => $this->_msg["Up"],
				        "MSG_Down" => $this->_msg["Down"],
				        "MSG_place" => $this->_msg["place"],
				        "MSG_id" => $this->_msg["id"],
				        "MSG_module" => $this->_msg["module"],
				        "MSG_Alert_main_block_single" => $this->_msg["Alert_main_block_single"],
				        "MSG_Alert_main_block_module" => $this->_msg["Alert_main_block_module"],
				        "MSG_Try_on" => $this->_msg["Try_on"]
				));
				$tpl->assign(array(
				    'adm_uri' => '/admin.php?lang='.$lang.'&site_target='.$CONFIG['domen'],
				    'baseurl' => $baseurl,
				    'action' => $action,
				    'page_ids' => implode(',', $pages_ids),
				    'tpl_name' => $page['tpl_name'],
				    'tpl_id' => $page['tpl_id'],
				    'tpl_path' => $CONFIG['pages_tpl_image_path'].$this->table_prefix.'/'.$page['image_url'],
				    'css_submit_display' => 'inline',
				    'json_mods_meta' => json_encode($mods_mete),
				    'json_find_mods_meta' => 'false'
				));
				$tpl->assignList('page_', $page_list, array('baseurl' => $baseurl, 'divider' => ','));
				$tpl->newBlock('field_block');
				$this->showFieldsList($page['number_of_fields'], $page['main_module']);
				
				$this->main_title = "Шаг 2. Очистить поле";
				break;
				
			case 'del_block':
				$main->include_main_blocks_2("{$this->module_name}_group_acts.html", $this->tpl_path);
				$tpl->prepare();
				global $db;
				
				if (empty($_REQUEST['ids'])) {
				    Module::go_back($baseurl.'&action=str');
				}
				
				$pages_ids = array_map('intval', explode(',', $_REQUEST['ids']));
				$page_list = $db->getArrayOfResult("SELECT id, title FROM {$this->table_prefix}_pages WHERE id IN (".
				        implode(',', $pages_ids).")");
				if (empty($page_list)) {
				    Module::go_back($baseurl.'&action=str');
				}
				$pages_ids = array();
				foreach ($page_list as $val) {
				    $pages_ids[] = $val['id'];
				}
				$this->getPage($pages_ids[0]);
				$db->next();
				$page = $db->Record;
				
				if (isset($_POST['field'])) {
				    $this->clearPagesBlock($_POST['ids'], $_POST['field'], $_POST);
				    Module::go_back($baseurl.'&action='.($_REQUEST['fine_tuning'] ? 'fine_tuning' : $action)
				        .'&ids='.implode(',', $pages_ids));
				}
				
				$mods_mete = $this->getModsMeta($pages_ids);
				$tpl->gotoBlock('_ROOT');
				$tpl->assign(Array(
				        "MSG_Click_to_select" => $this->_msg["Click_to_select"],
				        "MSG_field" => $this->_msg["field"],
				        "MSG_Add_module" => $this->_msg["Add_module"],
				        "MSG_Click_to_select_module" => $this->_msg["Click_to_select_module"],
				        "MSG_Save" => $this->_msg["Save"],
				        "MSG_Cancel" => $this->_msg["Cancel"],
				        "MSG_Confirm_delete_this_block" => $this->_msg["Confirm_delete_this_block"],
				        "MSG_Delete" => $this->_msg["Delete"],
				        "MSG_Properties" => $this->_msg["Properties"],
				        "MSG_Up" => $this->_msg["Up"],
				        "MSG_Down" => $this->_msg["Down"],
				        "MSG_place" => $this->_msg["place"],
				        "MSG_id" => $this->_msg["id"],
				        "MSG_module" => $this->_msg["module"],
				        "MSG_Alert_main_block_single" => $this->_msg["Alert_main_block_single"],
				        "MSG_Alert_main_block_module" => $this->_msg["Alert_main_block_module"],
				        "MSG_Try_on" => $this->_msg["Try_on"]
				));
				$tpl->assign(array(
				    'adm_uri' => '/admin.php?lang='.$lang.'&site_target='.$CONFIG['domen'],
				    'baseurl' => $baseurl,
				    'action' => $action,
				    'page_ids' => implode(',', $pages_ids),
				    'tpl_name' => $page['tpl_name'],
				    'tpl_id' => $page['tpl_id'],
				    'tpl_path' => $CONFIG['pages_tpl_image_path'].$this->table_prefix.'/'.$page['image_url'],
				    'css_submit_display' => 'none',
				    'json_mods_meta' => json_encode($mods_mete),
				    'json_find_mods_meta' => 'false',
				    'del_act' => 1
				));
				$tpl->assignList('page_', $page_list, array('baseurl' => $baseurl, 'divider' => ','));
				$tpl->newBlock('field_block');
				$this->showFieldsList($page['number_of_fields'], $page['main_module']);
				$tpl->newBlock('mod_block');
				$tpl->newBlock('mod_prop_block');
				$tpl->newBlock($action);
				
				$this->main_title = "Шаг 2. Удалить блок";
				break;
				
			case 'add_block':
				$main->include_main_blocks_2("{$this->module_name}_group_acts.html", $this->tpl_path);
				$tpl->prepare();
				global $db;
				if (empty($_REQUEST['ids'])) {
				    Module::go_back($baseurl.'&action=str');
				}
				
				$pages_ids = array_map('intval', explode(',', $_REQUEST['ids']));
				$page_list = $db->getArrayOfResult("SELECT id, title FROM {$this->table_prefix}_pages WHERE id IN (".
				    implode(',', $pages_ids).")");
				if (empty($page_list)) {
				    Module::go_back($baseurl.'&action=str');
				}
				$pages_ids = array();
				foreach ($page_list as $val) {
				    $pages_ids[] = $val['id'];
				}
				$this->getPage($pages_ids[0]);
				$db->next();
				$page = $db->Record;
				
				if (isset($_POST['prop'])) {
				    $this->addBlockToPages($_POST);
				    Module::go_back($baseurl.'&action='.($_REQUEST['fine_tuning'] ? 'fine_tuning' : $action)
				        .'&ids='.implode(',', $pages_ids));
				}
				
				$mods_mete = $this->getModsMeta();
				$tpl->gotoBlock('_ROOT');
				$tpl->assign(Array(
				        "MSG_Click_to_select" => $this->_msg["Click_to_select"],
				        "MSG_field" => $this->_msg["field"],
				        "MSG_Add_module" => $this->_msg["Add_module"],
				        "MSG_Click_to_select_module" => $this->_msg["Click_to_select_module"],
				        "MSG_Save" => $this->_msg["Save"],
				        "MSG_Cancel" => $this->_msg["Cancel"],
				        "MSG_Confirm_delete_this_block" => $this->_msg["Confirm_delete_this_block"],
				        "MSG_Delete" => $this->_msg["Delete"],
				        "MSG_Properties" => $this->_msg["Properties"],
				        "MSG_Up" => $this->_msg["Up"],
				        "MSG_Down" => $this->_msg["Down"],
				        "MSG_place" => $this->_msg["place"],
				        "MSG_id" => $this->_msg["id"],
				        "MSG_module" => $this->_msg["module"],
				        "MSG_Alert_main_block_single" => $this->_msg["Alert_main_block_single"],
				        "MSG_Alert_main_block_module" => $this->_msg["Alert_main_block_module"],
				        "MSG_Try_on" => $this->_msg["Try_on"]
				));
				$tpl->assign(array(
				    'adm_uri' => '/admin.php?lang='.$lang.'&site_target='.$CONFIG['domen'],
				    'baseurl' => $baseurl,
				    'action' => $action,
				    'page_ids' => implode(',', $pages_ids),
				    'tpl_name' => $page['tpl_name'],
				    'tpl_id' => $page['tpl_id'],
				    'tpl_path' => $CONFIG['pages_tpl_image_path'].$this->table_prefix.'/'.$page['image_url'],
				    'css_submit_display' => 'none',
				    'json_mods_meta' => json_encode($mods_mete),
				    'json_find_mods_meta' => 'false',
				    'add_mode' => 1
				));
				$tpl->assignList('page_', $page_list, array('baseurl' => $baseurl, 'divider' => ','));
				$tpl->newBlock('field_block');
				$this->showFieldsList($page['number_of_fields'], $page['main_module']);
				$tpl->newBlock('mod_block');
				$tpl->newBlock('mod_prop_block');
				$this->main_title = "Шаг 2. Добавить блок";
				break;
				
			case 'change_block':
				$main->include_main_blocks_2("{$this->module_name}_group_acts.html", $this->tpl_path);
				$tpl->prepare();
				global $db;
				if (empty($_REQUEST['ids'])) {
				    Module::go_back($baseurl.'&action=str');
				}
				
				$pages_ids = array_map('intval', explode(',', $_REQUEST['ids']));
				$page_list = $db->getArrayOfResult("SELECT id, title FROM {$this->table_prefix}_pages WHERE id IN (".
				    implode(',', $pages_ids).")");
				if (empty($page_list)) {
				    Module::go_back($baseurl.'&action=str');
				}
				$pages_ids = array();
				foreach ($page_list as $val) {
				    $pages_ids[] = $val['id'];
				}
				$this->getPage($pages_ids[0]);
				$db->next();
				$page = $db->Record;
				
				if (isset($_POST['prop'])) {
				    $this->changeBlockOnPages($_POST);
				    Module::go_back($baseurl.'&action='.($_REQUEST['fine_tuning'] ? 'fine_tuning' : $action)
				        .'&ids='.implode(',', $pages_ids));
				}
				
				$mods_mete = $this->getModsMeta();
				$find_mods_mete = $this->getModsMeta($pages_ids);
				$tpl->gotoBlock('_ROOT');
				$tpl->assign(Array(
				        "MSG_Click_to_select" => $this->_msg["Click_to_select"],
				        "MSG_field" => $this->_msg["field"],
				        "MSG_Add_module" => $this->_msg["Add_module"],
				        "MSG_Click_to_select_module" => $this->_msg["Click_to_select_module"],
				        "MSG_Save" => $this->_msg["Save"],
				        "MSG_Cancel" => $this->_msg["Cancel"],
				        "MSG_Confirm_delete_this_block" => $this->_msg["Confirm_delete_this_block"],
				        "MSG_Delete" => $this->_msg["Delete"],
				        "MSG_Properties" => $this->_msg["Properties"],
				        "MSG_Up" => $this->_msg["Up"],
				        "MSG_Down" => $this->_msg["Down"],
				        "MSG_place" => $this->_msg["place"],
				        "MSG_id" => $this->_msg["id"],
				        "MSG_module" => $this->_msg["module"],
				        "MSG_Alert_main_block_single" => $this->_msg["Alert_main_block_single"],
				        "MSG_Alert_main_block_module" => $this->_msg["Alert_main_block_module"],
				        "MSG_Try_on" => $this->_msg["Try_on"]
				));
				$tpl->assign(array(
				    'adm_uri' => '/admin.php?lang='.$lang.'&site_target='.$CONFIG['domen'],
				    'baseurl' => $baseurl,
				    'action' => $action,
				    'page_ids' => implode(',', $pages_ids),
				    'tpl_name' => $page['tpl_name'],
				    'tpl_id' => $page['tpl_id'],
				    'tpl_path' => $CONFIG['pages_tpl_image_path'].$this->table_prefix.'/'.$page['image_url'],
				    'css_submit_display' => 'none',
				    'json_mods_meta' => json_encode($mods_mete),
				    'json_find_mods_meta' => json_encode($find_mods_mete)
				));
				$tpl->assignList('page_', $page_list, array('baseurl' => $baseurl, 'divider' => ','));				
				$tpl->newBlock('mod_block');
				$tpl->newBlock('mod_prop_block');
				$tpl->newBlock('change');
				$this->showFieldsList($page['number_of_fields'], $page['main_module'], 'change_field_list_item');
				$this->main_title = "Шаг 2. Заменить блок";
				break;
				
			case 'fine_tuning':
				$main->include_main_blocks_2("{$this->module_name}_{$action}.html", $this->tpl_path);
				$tpl->prepare();
				global $db;
				
				
				if (! empty($_POST['pages'])) {
				    $pages = json_decode($_POST['pages']);
				    $pages_ids = array();
				    foreach ($pages as $k => $v) {
				        if ($v) {
				            $pages_ids[] = (int)$k;
				        }				        
				    }
				    //array_map('intval', array_keys($pages));
				    $this->setPagesBlocks($pages, $_POST['field']);
				    if ($_POST['tryon']) {
				        Module::go_back($baseurl."&action={$action}&ids=".implode(',', $pages_ids));
				    } else {
				        Module::go_back($baseurl);
				    }
				}
				if (empty($_REQUEST['ids'])) {
				    Module::go_back($baseurl.'&action=str');
				}
				
				$pages_ids = array_map('intval', explode(',', $_REQUEST['ids']));
				$page_list = $db->getArrayOfResult("SELECT id, title FROM {$this->table_prefix}_pages WHERE id IN (".
				    implode(',', $pages_ids).")");
				if (empty($page_list)) {
				    Module::go_back($baseurl.'&action=str');
				}
				$pages_ids = array();
				foreach ($page_list as $val) {
				    $pages_ids[] = $val['id'];
				}
				$this->getPage($pages_ids[0]);
				$db->next();
				$page = $db->Record;

				
				//$mods_mete = $this->getModsMeta();
				$tpl->gotoBlock('_ROOT');
				$tpl->assign(Array(
				        "MSG_Click_to_select" => $this->_msg["Click_to_select"],
				        "MSG_field" => $this->_msg["field"],
				        "MSG_Add_module" => $this->_msg["Add_module"],
				        "MSG_Click_to_select_module" => $this->_msg["Click_to_select_module"],
				        "MSG_Save" => $this->_msg["Save"],
				        "MSG_Cancel" => $this->_msg["Cancel"],
				        "MSG_Confirm_delete_this_block" => $this->_msg["Confirm_delete_this_block"],
				        "MSG_Delete" => $this->_msg["Delete"],
				        "MSG_Properties" => $this->_msg["Properties"],
				        "MSG_Up" => $this->_msg["Up"],
				        "MSG_Down" => $this->_msg["Down"],
				        "MSG_place" => $this->_msg["place"],
				        "MSG_id" => $this->_msg["id"],
				        "MSG_module" => $this->_msg["module"],
				        "MSG_Alert_main_block_single" => $this->_msg["Alert_main_block_single"],
				        "MSG_Alert_main_block_module" => $this->_msg["Alert_main_block_module"],
				        "MSG_Try_on" => $this->_msg["Try_on"]
				));
				$tpl->assign(array(
				    'adm_uri' => '/admin.php?lang='.$lang.'&site_target='.$CONFIG['domen'],
				    'baseurl' => $baseurl,
				    'action' => $action,
				    'page_ids' => implode(',', $pages_ids),
				    'tpl_name' => $page['tpl_name'],
				    'tpl_id' => $page['tpl_id'],
				    'tpl_path' => $CONFIG['pages_tpl_image_path'].$this->table_prefix.'/'.$page['image_url'],
				    //'json_mods_meta' => json_encode($mods_mete)
				));
				$tpl->assignList('page_', $page_list, array('baseurl' => $baseurl, 'divider' => ','));
				$this->showFieldsList($page['number_of_fields'], $page['main_module']);
				$this->main_title = "Шаг 2. Тонкая настройка";
				break;
				
			case "pages_field":
			    $ids = explode(',', $_REQUEST['ids']);
				if ($GLOBALS['JsHttpRequest'] && !empty($ids) && isset($_REQUEST['field']) ) {
				    global $db;
				    $ids = array_map('intval', $ids);
				    $f = (int)$_REQUEST['field'];
				    $res = false;
				    $db->query("SELECT B.*, M.title AS mod_title, M.name AS mod_name FROM {$this->table_prefix}_pages_blocks B INNER JOIN"
				        ." modules AS M ON module_id=M.id WHERE field_number={$f} AND page_id IN (".
				        implode(',', $ids).") ORDER BY page_id, block_rank");
				    //echo $db->sql;
				    while ($db->next()) {
				        $res[$db->Record['page_id']][$db->Record['id']] = $db->Record;
				        $res[$db->Record['page_id']][$db->Record['id']]['property1'] = preg_match("/^[{\[].*[}\]]$/", $db->Record['property1'])
				            ? json_decode($db->Record['property1']) : $db->Record['property1'];
				        $res[$db->Record['page_id']][$db->Record['id']]['property2'] = preg_match("/^[{\[].*[}\]]$/", $db->Record['property2'])
				            ? json_decode($db->Record['property2']) : $db->Record['property2'];
				        $res[$db->Record['page_id']][$db->Record['id']]['property3'] = preg_match("/^[{\[].*[}\]]$/", $db->Record['property3'])
				            ? json_decode($db->Record['property3']) : $db->Record['property3'];
				        $res[$db->Record['page_id']][$db->Record['id']]['property4'] = preg_match("/^[{\[].*[}\]]$/", $db->Record['property4'])
				            ? json_decode($db->Record['property4']) : $db->Record['property4'];
				        $res[$db->Record['page_id']][$db->Record['id']]['property5'] = preg_match("/^[{\[].*[}\]]$/", $db->Record['property5'])
				            ? json_decode($db->Record['property5']) : $db->Record['property5'];
				    }
				    $GLOBALS['_RESULT'] = $res;
				}
				exit;


// Показать настройки PHP на данном хостинге
			case "phpinfo":
				phpinfo();
				exit;

// Создание объекта для дальнейшего использования
			case 'only_create_object':
				break;

// Default...
			default:
				$main->include_main_blocks();
				$tpl->prepare();
			}
//--------------------------- конец обработки действий из админ части --------------------------//
		}
	}

/**
 * Формирует массивы $this->page_titles, $this->page_links, $this->page_parents
 * @version	3.0
 * @access	private
 * @return	mixed	FALSE в случае неудачи, в случае успеха TRUE
 */
	function  form_branch_structure($type=NULL)
	{   global $CONFIG, $core, $PAGE, $db, $main, $tpl, $server, $lang;
	
		if ($type=='full') {
		    $branch_str = ' OR branch!="" ';
		} else {
		    if ($PAGE['branch']) {
		        $branch_str = ' OR id IN ('.$PAGE['branch'].') OR parent_id IN ('.$PAGE['branch'].')';
		    }
        }
        $db->query('SELECT * FROM '.$this->table_prefix.'_pages USE INDEX (index_2) WHERE ((parent_id = 0 '
            .$branch_str.') AND active = 1) OR is_default = 1 ORDER BY level, page_rank DESC');
		if ($db->nf() > 0) {
			while($db->next_record()) {
				$cur_link	= $db->Record['redirect_url'] ? $db->Record['redirect_url'] : $core->formPageLink($db->f('link'), $db->f('address'), $lang);
				$cur_level	= $db->f('level');
				$cur_id		= $db->f('id');
				$cur_title	= $db->f('title');

				$this->page_titles[$cur_level][$cur_id]		= $cur_title;
				$this->page_links[$cur_level][$cur_id]		= $cur_link;
				$this->page_parents[$cur_level][$cur_id]	= $db->f('parent_id');
				$this->page_imgs[$cur_id] = array(
				    'path1' => $db->Record['img1'] && file_exists(RP.$CONFIG['pages_img_path'].$db->Record['img1'])
		                ? "/".$CONFIG['pages_img_path'].$db->Record['img1'] : '',
		            'path2' => $db->Record['img2'] && file_exists(RP.$CONFIG['pages_img_path'].$db->Record['img2'])
		                ? "/".$CONFIG['pages_img_path'].$db->Record['img2'] : '',
		            'path3' => $db->Record['img3'] && file_exists(RP.$CONFIG['pages_img_path'].$db->Record['img3'])
		                ? "/".$CONFIG['pages_img_path'].$db->Record['img3'] : ''		        
		    );
			}
			return TRUE;
		}
		return FALSE;
	}

/**
 * Возвращает массив всех подпунктов меню содержащегося в массиве $this->page_links[1]
 * @version	3.0
 * @access	private
 * @return	mixed	FALSE в случае неудачи, массив подпунктов всех пунктов меню в случае успеха
 */
 	function getSubNavMenu() {
		global $CONFIG, $db, $server, $lang;
		$num_rec = sizeof($this->page_titles[1]);
		if (0 == $num_rec) {
			$this->form_branch_structure("full");
			//$this->form_branch_structure();
			$num_rec = sizeof($this->page_titles[1]);
		}
		if ($num_rec > 0) {
			for ($i = 1; $i <= sizeof($this->page_titles); $i++) {
                $ids .= (is_array($this->page_titles[$i])) ? implode(',', array_keys($this->page_titles[$i])).',' : '';
			}
			$ids = substr($ids, 0, -1);

			$arr = $db->getArrayOfResult('SELECT id, parent_id, title, address
											FROM '.$this->table_prefix.'_pages USE INDEX (index_3)
											WHERE parent_id IN ('.$ids.') AND
												  active = 1
											ORDER BY '.$CONFIG['pages_order_by']);
            return $arr;
		}
		return FALSE;
	}

/**
 * Возвращает массив подпунктов конкретного пункта меню
 * @version	3.0
 * @access	private
 * @param	int		$key	id пункта меню в массиве $this->page_titles[1]
 * @param	array	$arr	массив подпунктов меню из которого производиться выборка
 * @return	mixed	FALSE в случае неудачи, массив подпунктов конкретного пункта меню в случае успеха
 */
	function getArrayOfChild($key, $arr) {
		$size_arr = sizeof($arr);
		if ($key && ($size_arr > 0)) {
			for ($i = 0; $i < $size_arr; $i++) {
				if ($arr[$i]['parent_id'] == $key) {
					$child[$i]['id']		= $arr[$i]['id'];
					$child[$i]['parent_id']	= $arr[$i]['parent_id'];
					$child[$i]['title']		= $arr[$i]['title'];
					$child[$i]['address']	= $arr[$i]['address'];
					$child[$i]['link']	= $arr[$i]['link'];
				}
			}
			return $child;
		}
		return FALSE;
	}

	function show_nav_menufull(){
        global $tpl;
		$this->form_branch_structure('full');
		$i=1;
		while($tpl->blocksIndx["level{$i}_menu"]) {
		    $i++;
		}
		$arr = $this->getSubNavMenu();
  		$this->show_nav_menu_level($arr, 1, $i, 'all', false, true);
		return TRUE;
	}
/**
 * получение подразделов вложености
 * @access	public
 * @return	array	(�?Д => ЗАГОЛОВОК) //для совместимости с $this->page_titles
 */
    function getChildMenu($cur, $arr){
        foreach($arr as $v){if($v['parent_id']==$cur){$ret[($v['id'])] = $v['title'];}}
        return (is_array($ret)) ? $ret : null;
    }
/**
 * Вывод одного уровня навигационного меню (рекурсивная функция)
 * @version	3.0
 * @access	public
 * @return	bool	возвращает TRUE
 */
	function show_nav_menu_level(&$arr, $level, $max=null, $type='all', $v=false, $reset_current = false) {
		global $PAGE, $tpl, $lang, $pg_reset;
        static $current;
        $current = $reset_current ? false : $current;
        if($v){$current=$v;}
		$num_rec = sizeof($this->page_titles[$level]);
		$counter = 0;
        $arr_rev = $current ? $this->getChildMenu($current, $arr): $this->page_titles[$level];
        if (!$arr_rev){return false;}
		$tpl->newBlock('level'.$level.'_menu');
		foreach($arr_rev as $idx => $title) {
			$counter++;
			if($counter == $num_rec) $last = 1;
			$flag = $this->getArrayOfChild($idx, $arr);
			if (is_array($flag) && (sizeof($flag) > 0)) {
				$this->show_menu_item($idx, $title, $this->page_links[$level][$idx], $level, 1, $last);
                if ( ($idx == $PAGE['branch_array'][$level-1] || $type=='all')
                    && (!$max || $max > $level)
                ) {
                    $current=$idx;
                    $this->show_nav_menu_level($arr, $level+1, $max, $type);
                }
  		    } else {
				$this->show_menu_item($idx, $title, $this->page_links[$level][$idx], $level, 0, $last);
			}
			$flag = NULL;
		}
	}


/**
 * Вывод навигационного меню
 * @version	3.0
 * @access	public
 * @return	bool	возвращает TRUE
 */
	function show_nav_menu() {
        global $tpl, $CONFIG;
        $i=1;
        while($tpl->blocksIndx["level{$i}_menu"]) {
            $i++;
        }
		$arr = $this->getSubNavMenu();
		$this->show_nav_menu_level($arr, 1, $i-1, $CONFIG['pages_menu_mod'] , false, true);
		return TRUE;
	}


/**
 * Вывод подменю (страницы 2 и 3 уровня вложенности)
 * @version	3.0
 * @return	bool	TRUE в случае успешного выполнение и FALSE в ином случае
 */
	function show_sub_nav_menu() {
        global $tpl,$PAGE,$CONFIG;
        if (!is_array($this->page_titles[2]) || sizeof($this->page_titles[2])==0) {
            return false;
        }
        $i=2;
        while($tpl->blocksIndx["level{$i}_menu"]) {
            $i++;
    	}
    	$arr = $this->getSubNavMenu();
    	$this->show_nav_menu_level($arr, 2, $i-1, $CONFIG['pages_menu_mod'], $PAGE['branch_array'][0]);
    	return TRUE;
	}


/**
 * Вывода меню пункта меню
 * @version 3.0
 * @param	int		$idx		id родительской категории
 * @param 	string	$title		название пункта меню
 * @param 	string	$link		ссылка на страницу
 * @param 	int		$level		уровень страницы
 * @param 	string	$extended	не используется
 * @param 	int		$last		не используется
 * @param 	int		$page_num	порядковый номер страницы
 * @return 	bool	возвращает TRUE
 */
	function show_menu_item($idx, $title, $link, $level = 1, $extended = '', $last = null, $page_link = '', $page_num = 0) {
		global $CONFIG, $PAGE, $tpl, $lang, $baseurl, $transurl;
		if ($idx) {
			$tpl->newBlock('level'.$level.'_menu_item');
			$tpl->assign(array(
			    'page_last'	=> ($last) ? " class=\"last\"" : "",
			));
			$assign = array(
				'page_id'	=> $idx,
				'page_name'	=> $title,
				'page_link'	=> $link,
				'page_abbr'	=> substr(strrchr(substr($link, 0, -1),"/"),1),
 				'page_num'	=> $page_num,
				'page_last'	=> ($last) ? " class=\"last\"" : "",
			);
			if ($idx == $PAGE['id'] && (!strstr($_SERVER['QUERY_STRING'], 'action='))) {
				$tpl->newBlock('level'.$level.'_menu_current_item');				
				$tpl->assign($assign);
				if ($this->page_imgs[$idx]['path2']) {
				    $tpl->newBlock('level'.$level.'_curr_img');
				    $assign['path'] = $this->page_imgs[$idx]['path2'];
				} else {
				    $tpl->newBlock('level'.$level.'_curr_no_img');
				}
			} elseif (in_array($idx, $PAGE['branch_array'])) {
				$tpl->newBlock('level'.$level.'_menu_current_children_item');
				$tpl->assign($assign);
				if ($this->page_imgs[$idx]['path3']) {
				    $tpl->newBlock('level'.$level.'_curr_child_img');
				    $assign['path'] = $this->page_imgs[$idx]['path3'];
				} else {
				    $tpl->newBlock('level'.$level.'_curr_child_no_img');
				}
			} else {
				$tpl->newBlock('level'.$level.'_menu_active_item');
				$tpl->assign($assign);
				if ($this->page_imgs[$idx]['path1']) {
				    $tpl->newBlock('level'.$level.'_active_img');
				    $assign['path'] = $this->page_imgs[$idx]['path1'];
				} else {
				    $tpl->newBlock('level'.$level.'_active_no_img');
				}
			}
			$tpl->assign($assign);
		}
		return TRUE;
	}


/**
 * Вывод выпадающего (select) меню
 * @version 3.0
 * @return	bool	TRUE в случае успешного выполнение и FALSE в ином случае
 */
	function show_selectlist_menu() {
		global $CONFIG, $core, $PAGE, $db, $main, $tpl, $server, $lang;
		$query = "SELECT id
					FROM ".$this->table_prefix."_pages
					WHERE link = 'products' AND active = 1";
		$db->query($query);
		if ($db->nf() > 0) {
			$db->next_record();
			$query = "SELECT * FROM ".$this->table_prefix."_pages WHERE parent_id = ".$db->f('id')." AND active = 1";
		}
		$db->query($query);
		$tpl->newBlock("level1_menu");
		if ($db->nf() > 0) {
		// 1 уровень -------------------------------------------------------------------
			while($db->next_record()) {
				$this->show_menu_item($db->f("id"), $db->f("title"), $core->formPageLink($db->f("link"), $db->f("address"), $lang), 1);
			}
			return TRUE;
		}
		// 1 уровень -------------------------------------------------------------------
		return FALSE;
	}

/**
 * вывод списка продуктов в верхнем меню
 * @version	3.0
 * @return	bool	возвращает TRUE
 */
	function show_topmenu() {
		global $CONFIG, $core, $PAGE, $db, $main, $tpl, $server, $lang;
		$query = "SELECT * FROM ".$this->table_prefix."_pages WHERE parent_id = 0 AND active = 1";
		$db->query($query);
		$tpl->newBlock("level1_menu");
		// 1 уровень -------------------------------------------------------------------
		while($db->next_record()) {
			$this->show_topmenu_item($db->f("id"), $db->f("title"), $core->formPageLink($db->f("link"), $db->f("address"), $lang), 1);
		}
		return TRUE;
		// 1 уровень -------------------------------------------------------------------
		return TRUE;
	}

/**
 * Вывод пунктов меню ТОП
 * @version 3.0
 * @param	int		$idx	id станицы
 * @param	string	$title	название страницы
 * @param	string	$link	ссылка на страницу
 * @param	int		$level	уровень страницы
 * @return	bool	возвращает TRUE
 */
	function show_topmenu_item($idx, $title, $link, $level = 1) {
		global $CONFIG, $PAGE, $tpl;
		if ($idx) {
			$tpl->newBlock("level${level}_menu_item");
			if ($idx == $PAGE["id"] && !strstr($_SERVER["QUERY_STRING"], "action=")) {
				$tpl->newBlock("level${level}_menu_current_item");
			} else {
				$tpl->newBlock("level${level}_menu_active_item");
			}
			$tpl->assign(
				array(
					"page_name"	=>	$title,
					"page_link"	=>	$link,
					"page_id"	=>	$idx,
				)
			);
		}
		return TRUE;
	}


/**
 * Формирование иерархического дерева сайта
 * @version 3.0
 * @return	mixed	HTML код дерева или FALSE
 */
	function get_sitemap_tree() {
		global $CONFIG, $core, $db, $tpl, $baseurl, $server, $lang;
		$home_page = (isset($lang) && $lang == "eng") ? "Home" : "Главная";
		$home_link = $core->formPageLink("", "", $lang);
		$query = "SELECT * FROM ".$this->table_prefix."_pages WHERE !ISNULL(parent_id) AND active = 1 AND is_default != 1 ORDER BY page_rank DESC, id";
		$db->query($query);
		if ($db->nf() > 0) {
			$icon = '';
//			$icon = 'blue-point.gif';
		    $menu	= new TreeMenu();
			$first	= 1; // указатель на первой позиции в выборке из БД
			$text	= "<a href=\"$home_link\">$home_page</a><br><img src=/i/1.gif width=1 height=4 border=0>";
			$array	= array('text' => $text, 'icon' => $icon, 'isDynamic'	=>	false, 'ensureVisible' => true);
			$node[0] = new TreeNode($array);
			$menu->addItem($node[0]);
			while($db->next_record()) {
				$id = $db->f('id');
				$pid = $db->f('parent_id');
				$title = addslashes($db->f('title'));
//				$cur_page = $db->f("link");
				$page_link = $core->formPageLink($db->f("link"), $db->f("address"), $lang);
				$text = "<a href=\"$page_link\">$title</a>";
				$array = array('text' => $text, 'icon' => $icon, 'isDynamic'	=>	false, 'ensureVisible' => true);
				/*
				if ($pid == 0) {
			    $node[$id] = new TreeNode($array);
					$menu->addItem($node[$id]);
				} else if ($pid != 0 && $first == 1) {
			    $node[$id] = new TreeNode($array);
					$menu->addItem($node[$id]);
				} else {
//			    $node[$id] = &$node[$pid]->addItem(new TreeNode($array));
				}
				*/
				/*
		    $node[$id] = new TreeNode($array);
				if ($pid == 0) {
					$child_nodes .= "\$menu->addItem(\$node[$id]);";
				} else if ($pid != 0 && $first == 1) {
					$child_nodes .= "\$menu->addItem(\$node[$id]);";
				} else {
					$arr[$id] = $array;
			    $child_nodes .= "if (is_object(\$node[$pid])) \$node[$pid]->addItem(\$node[$id]);";
				}*/
				$node[$id] = new TreeNode($array);
				$arr[$id] = $array;
				$child_nodes .= "if (is_object(\$node[$pid])) \$node[$pid]->addItem(\$node[$id]);";
				$first = 0;
			}
			eval($child_nodes);
			$treeMenu = &new TreeMenu_DHTML($menu, array(
			    'images' => "/" . $CONFIG["tree_img_path"],
			    'defaultClass' => 'treeMenuDefault',
			    'noTopLevelImages'	=>	false
			));
			//echo $treeMenu->toHTML();
			return $treeMenu->toHTML();
		}
		return FALSE;
	}

/**
 * Вывод иерархического дерева сайта в шаблон
 * @version 3.0
 * @return	bool	возвращает TRUE
 */
	// функция для вывода карты сайта
	function create_sitemap_tree() {
		global $tpl;
		$html_output = $this->get_sitemap_tree();
		/*
		if ($blockname) {
			$tpl->newBlock($blockname);
		} *//*else {
			$tpl->gotoBlock("_ROOT");
		}*/
		$tpl->assign(
			array(
				"_ROOT.sitemap"	=>	$html_output,
			)
		);
		return TRUE;
	}


/**
 * Вывод вывод иерархического дерева сайта в виде двух колонок в шаблон
 * @version 3.0
 * @return	bool	TRUE в случае успешного выполнение и FALSE в ином случае
 */
	function show_sitemap_in_columns() {
		global $CONFIG, $core, $PAGE, $db, $main, $tpl, $server, $lang;
		$query = "SELECT * FROM ".$this->table_prefix."_pages WHERE active = 1 AND is_default != 1 AND !ISNULL(parent_id) ORDER BY level, page_rank DESC";
		$db->query($query);
		if ($db->nf() > 0) {
			while($db->next_record()) {
				$cur_link		= $core->formPageLink($db->f("link"), $db->f("address"), $lang);
				$cur_level		= $db->f("level");
				$cur_id			= $db->f("id");
				$cur_parent_id	= $db->f("parent_id");
				$cur_title		= $db->f("title");
				$page_titles[$cur_parent_id][$cur_id] = $cur_title;
				$page_links[$cur_parent_id][$cur_id] = $cur_link;
			}
			// 1 уровень -------------------------------------------------------------------
			if (count($page_titles[0]) > 0) {
				$column_rows = ceil(count($page_titles[0])/2);
				$i = 0;
				foreach ($page_titles[0] as $idx1 => $title1) {
					$i++;
					if ($i == 1 || $i == $column_rows+1) $tpl->newBlock("level1_menu");
					$this->show_menu_item($idx1, $title1, $page_links[0][$idx1], 1);
					// 2 уровень -------------------------------------------------------------------
					if (count($page_titles[$idx1]) > 0) {
						$tpl->newBlock("level2_menu");
						foreach ($page_titles[$idx1] as $idx2 => $title2) {
							$this->show_menu_item($idx2, $title2, $page_links[$idx1][$idx2], 2);
						}
					}
					// 2 уровень -------------------------------------------------------------------
				}
			}
			// 1 уровень -------------------------------------------------------------------
			return TRUE;
		}
		return FALSE;
	}


/**
 * Вывод навигационной строки, отображающей путь к текущей странице
 * @version 3.0
 * @return	bool	TRUE в случае успешного выполнение и FALSE в ином случае
 */
	function show_nav_path() {
		global $PAGE, $tpl, $field;
		$path = $this->form_nav_path();
		if ($path) {
			$tpl->newBlock('block_path');
			$tpl->assign(
				array(
					"path"	=>	$path,
//					"level"	=>	$level,
				)
			);
			return TRUE;
		}
		return FALSE;
	}

/**
 * Формирование навигационной строки, отображающей путь к текущей странице (через массив $page_titles)
 * строчка вида Каталог >> Бытовая техника >> Пылесосы... и т.д.
 * @version 3.0
 * @return	string	сформированная навигационная строка
 */
	function form_nav_path() {
	
		global $CONFIG, $PAGE, $core, $db, $lang, $baseurl;
		$separator = $CONFIG["nav_separator"];
		$href_attr = " class=\"special\"";
		$span_attr = "";
		
		
		
		if (!(count($this->page_titles)>0)) $this->form_branch_structure();
		if (is_array($this->page_titles[0])) {
			foreach($this->page_titles[0] as $id => $title) {
				$home_page = $title;
			}
		}
		$home_link = $core->formPageLink("", "", $lang);
		$root = "<a href=\"$home_link\"$href_attr>$home_page</a>$separator";
		
		$branch_array = $PAGE["branch_array"];
	
		$addition_to_path = $PAGE["addition_to_path"];
	   
		$path = "";
		if ($PAGE["is_default"] == 1) return "<span>".$home_page."</span>";
		// если страница привязана
		if ($PAGE["level"]) {
		
			$level = 0;
			foreach ($branch_array as $node) {
				$level++;
				
				foreach ($this->page_titles[$level] as $idx => $title) {
				    $title = strip_tags($title);
					if ($idx == $node) {
					    if ($idx == $PAGE['id'] && $PAGE['path_title']) {
					        $title = strip_tags($PAGE['path_title']);
					    }
						$page_link = $this->page_links[$level][$idx];
						$add_str = "<a href=\"$page_link\"$href_attr>$title</a>$separator";
						if ($level == $PAGE["level"]) {
							if (is_array($addition_to_path)) {
							// если addition_to_path массив
								if (count($addition_to_path) > 0) {
									$i = 0;
									$add_str = "<a href=\"$page_link\"$href_attr>$title</a>$separator";
									foreach($addition_to_path as $title => $page_link) {
										$i++;
										$title = strip_tags($title);
										if ($i == count($addition_to_path)) {
											$add_str .= "<span$span_attr>$title</span>";
										} else {
											$add_str .= "<a href=\"$page_link\"$href_attr>$title</a>$separator";
										}
									}
								}
							// если addition_to_path строка
							} else {
								if (!$addition_to_path) {
								
								    if($_GET['action'] == 'search') $title = 'Поиск'; 
									$add_str = "<span$span_attr>$title</span>";
								} else if ($addition_to_path) { 
									$add_str = "<a href=\"$page_link\"$href_attr>$title</a>$separator";
									//$add_str .= "<span$span_attr>$addition_to_path</span>";
								}
							}
						}
						$path = $path . $add_str;
						break;
					}
				}
			}
		// если страница не привязана
		} else {
			if ($addition_to_path) {
			    /*
				$add_str = "<a href=\"" . str_replace("?", "", $baseurl) . "\"$href_attr>" . $PAGE["title"] . "</a>$separator";
				$path = $add_str . "<span$span_attr>$addition_to_path</span>";
				*/
			    if (is_array($addition_to_path)) {
				// если addition_to_path массив
				    if (count($addition_to_path) > 0) {
						$i = 0;
						$add_str = "<a href=\"" . str_replace("?", "", $baseurl) . "\"$href_attr>" . strip_tags($PAGE["title"]) . "</a>$separator";
						foreach($addition_to_path as $title => $page_link) {
							$i++;
							$title = strip_tags($title);
							if ($i == count($addition_to_path)) {
								$add_str .= "<span$span_attr>$title</span>";
							} else {
								$add_str .= "<a href=\"$page_link\"$href_attr>$title</a>$separator";
							}
						}
					}   // если addition_to_path строка
                } else {
                    if (!$addition_to_path) {
                        $add_str = "<span$span_attr>".strip_tags($PAGE['title'])."</span>";
                    } else if ($addition_to_path) {
                        $add_str = "<a href=\"" . str_replace("?", "", $baseurl) . "\"$href_attr>".strip_tags($PAGE['title'])."</a>$separator";
                    }
                }
                $path = $add_str;
			} else {
				$path = "<span$span_attr>" . strip_tags($PAGE["title"]) . "</span>";
			}
		}
		#var_dump($root . $path);
		return $root . $path;
	}

/**
 * Формирование навигационной строки
 * @version 3.0
 * @param	int	$id	id страницы
 * @return	array	сформированный массив
 */
	function get_nav_path($id) {
		global $CONFIG, $core, $db, $field, $server, $lang;
		$path		= "";
		$address	= "";
		$level		= 0;
		$idx_arr	= array();
		$id			= (int)$id;
		$separator	= $CONFIG["nav_separator"];
		$home_page	= ($lang == "eng") ? "Home" : $this->_msg["Mainpage"];
		$home_link	= $core->formPageLink("", "", $lang);
		$root = ($id) ? "<a href=\"$home_link\">$home_page</a>$separator" : "<span>$home_page</span>$separator";
		while ($id) {
			$query = "SELECT * FROM ".$this->table_prefix."_pages WHERE id = ".$id;
			$db->query($query);
			if ($db->nf() > 0) {
				$db->next_record();
				$level++;
				$id = $db->f('id');
				$idx_arr[$db->f('level')] = $id;
				$address	= $db->f('link') . "/" . $address;
				$title		= $db->f('title');
//				$cur_page = $db->f("link");
				$page_link	= $core->formPageLink($db->f("link"), $db->f("address"), $lang);

				if ($level == 1) {
					$add_str = "<span>$title</span>$separator";
				} else {
					$add_str = "<a href=\"$page_link\">$title</a>$separator";
				}
				#var_dump($path);
				$path = $add_str . $path;
				$id = $db->f("parent_id");
			}
		}
		$path = $root . $path;
		return array($level, $path, $address, $idx_arr);
	}


/**
 * Отправка ссылки по почте
 * @version 3.0
 * @param	string	$from	адрес отправителя
 * @param	string	$email	адрес получателя
 * @param	string	$url	ссылка
 * @return	bool	TRUE в случае успешного выполнение и FALSE в ином случае
 */
	function send_url($from, $email, $url) {
		global $CONFIG, $tpl;
		if ($email && $url && preg_match("~^http://" . $_SERVER["HTTP_HOST"]."~i", $url)) {
			$subject = $this->_msg["From"] . " $from";
			$body = $url;
//			$subject = convert_cyr_string($subject,"w","k");
//			$body = convert_cyr_string($body,"w","k");
			if (mail($email, $subject, $body, "From:" . $CONFIG["return_email"] . "\n" . $CONFIG["email_headers"])) {
				return TRUE;
			}
		}
		return FALSE;
	}

/**
 * Вывод списка страниц, добавленных последними
 * @version 3.0
 * @return 	bool	TRUE в случае успешного выполнение и FALSE в ином случае
 */
	function view_new_pages() {
		global $CONFIG, $core, $db, $tpl, $baseurl, $server, $lang;
		if ($CONFIG["pages_view_new"]) $limit_str = " LIMIT 0," . $CONFIG["pages_view_new"];
		$query = "SELECT *, DATE_FORMAT(date,'" . $CONFIG["pages_date_format"] . "') as page_date
					FROM ".$this->table_prefix."_pages
					WHERE is_default != 1 AND active = 1
					ORDER BY date DESC ".$limit_str;
		$db->query($query);
		if ($db->nf() > 0) {
			while($db->next_record()) {
				$page_main_link = $core->formPageLink($db->f("link"), $db->f("address"), $lang);
				if ($db->f("is_default")) $page_main_link = "/";
				// вывод на экран списка
				$tpl->newBlock("block_new_pages");
				$tpl->assign(
					array(
						page_title		=> $db->f("title"),
						page_link		=> $db->f("link"),
						page_date		=> $db->f("page_date"),
						page_main_link	=> $page_main_link,
					)
				);
			}
			return TRUE;
		}
		return FALSE;
	}


/**
 * Вывод контекстного меню по id
 * @version 3.0
 * @param 	int		id	id меню
 * @return 	bool	TRUE в случае успешного выполнение и FALSE в ином случае
 */
	function show_context_menu($id = "") {
		global $CONFIG, $PAGE, $core, $db, $tpl, $baseurl, $server, $lang;
		$id		= (int)$id;
		if (!$id) return FALSE;
		$query	= "SELECT pcm.*, pcmi.page_id, p.id, p.title, p.address, p.link, p.img1, p.img2, p.img3
					FROM ".$this->table_prefix."_pages_context_menu as pcm, ".$this->table_prefix."_pages_context_menu_items as pcmi, ".$this->table_prefix."_pages as p
					WHERE pcm.id = $id AND pcmi.menu_id = $id AND p.id = pcmi.page_id AND p.active = 1
					ORDER BY pcmi.item_rank";
		$db->query($query);
		if ($db->nf() > 0) {
			$tpl->newBlock("block_context_menu");
			while ($db->next_record()) {
				$tpl->assign('context_menu_name', $db->f('name'));
				if ($db->f("is_default")) {
					$page_link = "/";
				} else {
					$page_link = $core->formPageLink($db->f("link"), $db->f("address"), $lang);
				}
				$tpl->newBlock("block_context_menu_item");
				$assign = array(
				    'page_id'	=>	$db->f("id"),
					'page_name'	=>	$db->f("title"),
					'page_link'	=>	$page_link
				);
				if ($PAGE["id"] == $db->f("id")) {
					$tpl->newBlock("block_context_menu_current_item");
					$tpl->assign($assign);
					if ($db->Record['img2'] && file_exists(RP.$CONFIG['pages_img_path'].$db->Record['img2'])) {
					    $assign['path'] = "/".$CONFIG['pages_img_path'].$db->Record['img2'];
					    $tpl->newBlock("block_curr_img");
					} else {
					    $tpl->newBlock("block_curr_no_img");
					}
				} else {
					$tpl->newBlock("block_context_menu_active_item");
					$tpl->assign($assign);
					if ($db->Record['img1'] && file_exists(RP.$CONFIG['pages_img_path'].$db->Record['img1'])) {
					    $assign['path'] = "/".$CONFIG['pages_img_path'].$db->Record['img1'];
					    $tpl->newBlock("block_active_img");
					} else {
					    $tpl->newBlock("block_active_no_img");
					}
				}
				$tpl->assign($assign);
			}
			return TRUE;
		}
		return FALSE;
	}

/**
 * Вывод заголовка страницы между тегами <title></title>
 * @version 3.0
 * @return 	bool	TRUE в случае успешного выполнение и FALSE в ином случае
 */
	function show_head_title()
	{   GLOBAL $PAGE, $tpl;

		$tpl->assign(array(
		    "head_title" => $PAGE["title_from_main_module"] ? $PAGE["title_from_main_module"] : $PAGE["title"]
		));
		return TRUE;
	}

/**
 * Вывод регистрационной формы
 * @version 3.0
 * @return bool	возвращает TRUE
 */
	function show_register_form() {
		global $CONFIG, $core, $PAGE, $db, $main, $tpl, $lang;
		$link = $core->formPageLink($CONFIG['siteusers_page_link'], $CONFIG['siteusers_page_address'], $lang, 1);
		$tpl->newBlock("block_show_form");
		$tpl->assign(Array(
			"MSG_Access_denied" => $this->_msg["Access_denied"],
			"MSG_Authorize_or" => $this->_msg["Authorize_or"],
			"MSG_register" => $this->_msg["register"],
			"MSG_Login" => $this->_msg["Login"],
			"MSG_Password" => $this->_msg["Password"],
			"MSG_Enter" => $this->_msg["Enter"],
			"MSG_Forgot_password" => $this->_msg["Forgot_password"],
			"MSG_return_back" => $this->_msg["return_back"],
			"MSG_authorization" => $this->_msg["Authorization"],
		));
		$tpl->assign(
			array(
				'register_link'	=>	$link.'&action=registration',
				'form_action'	=>	$link.'&action=login',
				'remind_link'	=>	$link.'&action=remind',
			)
		);
		return TRUE;
	}

/*
 * Вывод блока block_show_group_denied с установленный значением $CONFIG['contact_email'] для метки contact_email
 * @version 3.0
 * @return	bool	возвращает FALSE
 */
	function getGroupDenied() {
		global $CONFIG;
		$arr['contact_email'] = $CONFIG['contact_email'];
		return $arr;
	}

	function getPropertyFields() {
		GLOBAL $request_site_target, $request_sub_action;
		if ('' != $request_sub_action) {

			$tpl = $this->getTmpProperties();

			switch ($request_sub_action) {
				case 'cmenu':
					$tpl->newBlock('block_properties');
					$tpl->newBlock('block_properties_cmenu');
					$tpl->assign(Array(
						"MSG_Context_menu" => $this->_msg["Context_menu"],
						"MSG_Select" => $this->_msg["Select"],
					));
					$list = $this->getContextMenuList();
					abo_str_array_crop($list);
					$tpl->assign_array('block_menu_list', $list);
					$tpl->newBlock('block_page_edit');
					$tpl->assign(Array(
						"MSG_Goto_edit" => $this->_msg["Goto_edit"],
						"site_target" => $request_site_target,
					));
					break;

				case 'sitemap':
					$tpl->newBlock('block_properties');
					$tpl->newBlock('block_properties_map');
					$tpl->assign(Array(
						"MSG_Map_type" => $this->_msg["Map_type"],
						"MSG_with_tree" => $this->_msg["with_tree"],
						"MSG_with_blocks" => $this->_msg["with_blocks"],
						"MSG_with_tree_nojs" => $this->_msg["with_tree_nojs"],
					));
					break;

				default:
					$tpl->newBlock('block_properties_none');
					$tpl->assign(Array(
						"MSG_No" => $this->_msg["No"],
					));
					break;
			}
			return $this->getOutputContent($tpl->getOutputContent());
		}
		return FALSE;
	}

	function getEditLink($sub_action = NULL, $block_id = NULL) {
		GLOBAL $request_name, $db, $lang, $server;
		$block_id	= (int)$block_id;
		if ($sub_action && $block_id) {
			$db->query('SELECT	property1,
								property2,
								property3,
								property4,
								property5
							FROM '.$server.$lang.'_pages_blocks
							WHERE id = '.$block_id);
			if ($db->nf() > 0) {
				$db->next_record();
				$arr[]	= $db->f('property1');
				$arr[]	= $db->f('property2');
				$arr[]	= $db->f('property3');
				$arr[]	= $db->f('property4');
				$arr[]	= $db->f('property5');

				switch ($sub_action) {
					case 'cmenu':
						$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=editcm&menu=false&id=';
						$link['material_id']	= $arr[0];
						break;
				}
				return $link;
			}

			switch ($sub_action) {
				case 'cmenu':
					$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=editcm&menu=false&id=';
					$link['material_id']	= '';
					break;
			}
			return $link;
		}
		return FALSE;
	}

/**
 * Получение страницы по id
 * @version 3.0
 * @param	int		$id		id страницы
 * @return	bool	TRUE в случае успешного выполнение и FALSE в ином случае
 */
	function getPage($id = "") {
		global $CONFIG, $db, $tpl, $server, $lang;
		$id = (int)$id;
		if (!$id) return FALSE;
		$db->query('SELECT P.*, DATE_FORMAT(date,"'.$CONFIG['pages_date_format'].'") as date, '
		    .'T.tpl_name, T.main_module, T.number_of_fields, T.image_url FROM '
		    .$this->table_prefix.'_pages P INNER JOIN '.$this->table_prefix.'_pages_templates T'
		    .' ON P.tpl_id=T.id WHERE P.id = '.$id);
		return ($db->nf() > 0) ? TRUE : FALSE;
	}

/**
 * Вывод страницы по id, используется в основном в административной части для вывода данных из базы в форму редактирования
 * @version 3.0
 * @param	int		$id		id страницы
 * @return	mixed	tpl_id шаблона или FALSE
 */
	function show_page($id = '') {
		global $CONFIG, $db, $tpl, $lang;
		$id = (int)$id;
		if (!$id) return FALSE;
		if ($this->getPage($id)) {
			$db->next();
			$title			= htmlspecialchars($db->f('title'));
			$add_title		= htmlspecialchars($db->f('add_title'));
			$keywords		= $db->f('keywords');
			$description	= $db->f('description');
			$alt_title		= $db->f('alt_title');
			$meta			= $db->f("meta");
			$redirect_url	= $db->f("redirect_url");


			$permission_type = $db->f('permission_type');
			switch($permission_type) {
				case 1:
					$tpl->assign(
						array(
							'p1_checked'	=>	'checked',
							'perm_class'	=>	'deny',
							)
					);
					break;
				case 2:
					$tpl->assign(
						array(
							'p2_checked'	=>	'checked',
							'perm_class'	=>	'inh',
							)
					);
					break;
				case 3:
					$tpl->assign(
						array(
							"p2_checked"	=>	"checked",
							"perm_class"	=>	"inh",
							)
					);
					break;
				default:
					$tpl->assign(
						array(
							"p0_checked"	=>	"checked",
							"perm_class"	=>	"free",
							)
					);
					break;
			}

			//вывод на экран
			$tpl->assign(
				array(
					"title"			=>	$title,
					"add_title"		=>	$add_title,
					"alt_title"		=>	$alt_title,
					"link"			=>	$db->f("link"),
					"keywords"		=>	$keywords,
					"description"	=>	$description,
					"meta"			=>	$meta,
					"redirect_url"	=>	$redirect_url,
					"image_exists"	=>	$image_exists,
					"date"			=>	$db->f("date"),
					"id"			=>	$db->f('id'),
					"old_ptype"		=>	$permission_type,
				    'main_access_checked' => $db->Record['main_access'] ? 'checked' : ''
				)
			);
			return $db->Record;
		}
		return TRUE;
	}

/**
 * Вывод стартовой страницы
 * @version 3.0
 * @return	mixed	id страницы или -1
 */
	function get_default_page() {
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;

		$db->query('SELECT * FROM '.$server.$lang.'_pages WHERE is_default = 1');
		
		return $db->next_record() ? $db->f("id") : 0;
	}

/**
 * Вывод списка страниц
 * @version 3.0
 * @param
 * @param
 * @param
 * @param
 * @param
 * @param
 * @return	mixed	возвращает TRUE или 0
 */
	function show_pages_list($blockname, $show = "list", $start = "", $rows = "", $order_by = "", $search = "") {
		global $CONFIG, $core, $db, $tpl, $baseurl, $server, $lang, $permissions;

		$ret = TRUE;
		$this->default_page_id = $this->get_default_page();
    	if (!$order_by) $order_by = $CONFIG["pages_order_by"];

		$order_how = $order_by;
		$desc = 0 === strpos($order_by, "-") ? TRUE : FALSE;
		$order_by = $desc ? substr($order_by, 1) : $order_by;
		$active_order           = (!$desc && 'active' == $order_by ? "-" : "").'active';
		$title_order        = (!$desc && 'title' == $order_by ? "-" : "").'title';
		$link_order        = (!$desc && 'link' == $order_by ? "-" : "").'link';
		$tmpl_order        = (!$desc && 'tmpl' == $order_by ? "-" : "").'tmpl';
		$bind_order        = (!$desc && 'bind' == $order_by ? "-" : "").'bind';
		$index_order        = (!$desc && 'index' == $order_by ? "-" : "").'index';

		$active_order_mark        = 'active' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
		$title_order_mark        = 'title' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
		$link_order_mark        = 'link' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
		$tmpl_order_mark        = 'tmpl' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
		$bind_order_mark        = 'bind' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
		$index_order_mark        = 'index' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");

		$active_order_mark2        = 'active' != $order_by ? "" : " class=\"order_curr\"";
		$title_order_mark2        = 'title' != $order_by ? "" : " class=\"order_curr\"";
		$link_order_mark2        = 'link' != $order_by ? "" : " class=\"order_curr\"";
		$tmpl_order_mark2        = 'tmpl' != $order_by ? "" : " class=\"order_curr\"";
		$bind_order_mark2        = 'bind' != $order_by ? "" : " class=\"order_curr\"";
		$index_order_mark2        = 'index' != $order_by ? "" : " class=\"order_curr\"";

		$tpl->assign(array(
			"_ROOT.active_order" => $baseurl."&action=showlist&order=".$active_order,
			"_ROOT.title_order" => $baseurl."&action=showlist&order=".$title_order,
			"_ROOT.link_order" => $baseurl."&action=showlist&order=".$link_order,
			"_ROOT.tmpl_order" => $baseurl."&action=showlist&order=".$tmpl_order,
			"_ROOT.bind_order" => $baseurl."&action=showlist&order=".$bind_order,
			"_ROOT.index_order" => $baseurl."&action=showlist&order=".$index_order,

			"_ROOT.active_order_mark" => $active_order_mark,
			"_ROOT.title_order_mark" => $title_order_mark,
			"_ROOT.link_order_mark" => $link_order_mark,
			"_ROOT.tmpl_order_mark" => $tmpl_order_mark,
			"_ROOT.bind_order_mark" => $bind_order_mark,
			"_ROOT.index_order_mark" => $index_order_mark,

			"_ROOT.active_order_mark2" => $active_order_mark2,
			"_ROOT.title_order_mark2" => $title_order_mark2,
			"_ROOT.link_order_mark2" => $link_order_mark2,
			"_ROOT.mpl_order_mark2" => $tmpl_order_mark2,
			"_ROOT.bind_order_mark2" => $bind_order_mark2,
			"_ROOT.index_order_mark2" => $index_order_mark2,
		));

		$ord_vals = array('title' => 'title', 'active' => 'active', 'link' => 'link', 'tmpl' => 'tpl_id', 'bind' => 'bind_status', 'index' => 'is_default', );
		$how = 0 === strpos($order_how, "-") ? " DESC" : "";
		$order_by = $how ? substr($order_how, 1) : $order_how;
		if (in_array($order_by, array_keys($ord_vals))) {
			$order = $ord_vals[$order_by] . $how;
		} else {
			$order = $CONFIG['pages_order_by'];
		}

		if ($show == 'unbound') {
			$this->get_list_with_rights(
									"p.*",
                                    $server.$lang."_pages p",
									"p",
									"ISNULL(parent_id) AND is_default != 1",
									"",
									"page_rank DESC");
		} else if ($show == 'bound') {
			$this->get_list_with_rights(
									"p.*",
                                    $server.$lang."_pages p",
									"p",
									"!ISNULL(parent_id) AND is_default != 1",
									"",
									"page_rank DESC");
		} else if ($show == 'all') {
			$this->get_list_with_rights(
									"p.*",
                                    $server.$lang."_pages p",
									"p",
									"",
									"",
									"title");
		} else if ($show == 'active') {
			$this->get_list_with_rights(
									"p.*",
                                    $server.$lang."_pages p",
									"p",
									"active = 1",
									"",
									"id");
		} else if ($show == 'list') {
			$start = intval($start);
	    	if (!$rows) $rows = $CONFIG["pages_max_rows"];
			$total_cnt = $this->get_list_with_rights(
									"p.id as id, !ISNULL(parent_id) as bind_status, parent_id, active, title, link, address, is_default, tpl_name, DATE_FORMAT(date,'" . $CONFIG["pages_date_format"] . "') as date, can_be_edited",
                                    array (
                                              'pt' =>  $server.$lang."_pages_templates",
                                              'p'  =>  $server.$lang."_pages",
                                    ),
									"p",
									"pt.id = p.tpl_id",
									"",
									$order,
									$start,
									$rows,
									true);
			$ret = ceil($total_cnt/$rows);
		} else if ($show == 'search') {
			if (!$search || strlen($search) < 2) return 0;
			$search	= $db->escape($search);
			$this->get_list_with_rights(
									"p.id as id, !ISNULL(parent_id) as bind_status, parent_id, active, title, link, address, is_default, tpl_name, DATE_FORMAT(date,'" . $CONFIG["pages_date_format"] . "') as date, can_be_edited",
                                    array (
                                              'pt' =>  $server.$lang."_pages_templates",
                                              'p'  =>  $server.$lang."_pages",
                                    ),
									"p",
									"(p.id LIKE '%$search%' OR p.link LIKE '%$search%' OR p.title LIKE '%$search%' OR p.id = '$search')
									AND pt.id = p.tpl_id",
									"p.id",
									$order);
		}

		if ($db->nf() > 0) {
			while($db->next_record()) {
				$is_owner = $db->f("is_owner");
				$rights = $db->f("user_rights");
				$rights = array("d" => (($rights & 8) >> 3) && $permissions["d"],
								"p" => (($rights & 4) >> 2) && $permissions["p"],
								"e" => (($rights & 2) >> 1) && $permissions["e"],
								"r" => ($rights & 1) && $permissions["r"],
							);
				// вывод на экран списка
				$page_main_link = $core->formPageLink($db->f("link"), $db->f("address"), $lang);

				// редактирование :
				$page_edit = $rights["e"]
							? "<a href=\"$baseurl&action=edit&id=" . $db->f("id") . "&start=$start\"><img src=\"/i/admin/ico_edit.gif\" alt=\"" . $this->_msg["Edit"] . "\" title=\"" . $this->_msg["Edit"] . "\" width=15 height=18 border=0></a>"
							: "<img src=\"/i/admin/ico_edit_.gif\" alt=\"" . $this->_msg["No_edit"] . "\" title=\"" . $this->_msg["No_edit"] . "\" width=15 height=18 border=0>";
				// ----------------------------------------------------------

				// редактирование прав :
				$page_edit_rights = $rights["e"] && $is_owner
							? "<a href=\"$baseurl&action=editrights&id=" . $db->f("id") . "&start=$start\"><img src=\"/i/admin/ico_edit_rights.gif\" alt=\"" . $this->_msg["Edit_rights"] . "\" title=\"" . $this->_msg["Edit_rights"] . "\" width=15 height=18 border=0></a>"
							: "<img src=\"/i/admin/ico_edit_rights_.gif\" alt=\"" . $this->_msg["No_edit"] . "\" title=\"" . $this->_msg["No_edit"] . "\" width=15 height=18 border=0>";
				// ----------------------------------------------------------

				// удаление :
				$page_delete = $rights["d"]
							? "<a href=\"javascript:if(confirm('" . $this->_msg["Confirm_delete_this_page"] . "')){location.href='$baseurl&action=del&id=" . $db->f("id") . "&start=$start';}\"><img src=\"/i/admin/ico_del.gif\" alt=\"" . $this->_msg["Delete"] . "\" title=\"" . $this->_msg["Delete"] . "\" width=15 height=18 border=0></a>"
							: "<img src=\"/i/admin/ico_nodel.gif\" alt=\"" . $this->_msg["No_delete"] . "\" title=\"" . $this->_msg["No_delete"] . "\" width=15 height=18 border=0>";
				// ----------------------------------------------------------

				// публикация :
				if ($rights["p"]) {
					$change_status = ($db->f("active")) ? "suspend" : "activate";
					$action_text = ($db->f("active")) ? $this->_msg["Suspend"] : $this->_msg["Activate"];
					$action_img = ($db->f("active")) ? "ico_swof.gif" : "ico_swon.gif";
					$page_change_status = "<a href=\"$baseurl&action=$change_status&id=" . $db->f("id") . "&start=$start\"><img src=\"/i/admin/$action_img\" alt=\"$action_text\" title=\"$action_text\" width=15 height=18 border=0></a>";
				} else {
					$action_text = ($db->f("active")) ? $this->_msg["No_suspend"] : $this->_msg["No_activate"];
					$action_img = ($db->f("active")) ? "ico_swof_.gif" : "ico_swon_.gif";
					$page_change_status = "<img src=\"/i/admin/$action_img\" alt=\"$action_text\" title=\"$action_text\" width=15 height=18 border=0>";
				}
				// ----------------------------------------------------------

				// сделать стартовой :
				if ($db->f("is_default")) {
					$page_set_default = "<img src=\"/i/admin/ico_ishome.gif\" alt=\"" . $this->_msg["Startpage"] . "\" title=\"" . $this->_msg["Startpage"] . "\" width=15 height=18 border=0>";
					$page_change_status = "<img src=\"/i/admin/ico_nosw.gif\" alt=\"" . $this->_msg["You_cannot_suspend_startpage"] . "\" title=\"" . $this->_msg["You_cannot_suspend_startpage"] . "\" width=15 height=18 border=0>";
					$page_delete = "<img src=\"/i/admin/ico_nodel.gif\" alt=\"" . $this->_msg["You_cannot_delete_startpage"] . "\" title=\"" . $this->_msg["You_cannot_delete_startpage"] . "\" width=15 height=18 border=0>";
					$page_is_default = " (" . $this->_msg["Startpage"] . ")";
					$page_main_link = "/";
				} else {
					// если страница с включенной публикацией
					if ($db->f("active")) {
						// если страница привязана к дереву сайта
						if ($db->f("bind_status")) {
							$page_set_default = "<img src=\"/i/admin/ico_nohome.gif\" alt=\"" . $this->_msg["Bind_page_cannot_be_start"] . "\" title=\"" . $this->_msg["Bind_page_cannot_be_start"] . "\" width=15 height=18 border=0>";
						} elseif ($rights["p"] && $rights["e"]) {
							$page_set_default = "<a href=\"javascript:if(confirm('" . $this->_msg["Confirm_make_page_start"] . "')){location.href='$baseurl&action=setdefault&id=" . $db->f("id") . "&start=$start';}\"><img src=\"/i/admin/ico_isnthome.gif\" alt=\"" . $this->_msg["Make_start"] . "\" title=\"" . $this->_msg["Make_start"] . "\" width=15 height=18 border=0></a>";
						} else {
							$page_set_default = "<img src=\"/i/admin/ico_nohome.gif\" alt=\"" . $this->_msg["No_bind"] . "\" title=\"" . $this->_msg["No_bind"] . "\" width=15 height=18 border=0>";
						}
					} else {
						$page_set_default = "<img src=\"/i/admin/ico_nohome.gif\" alt=\"" . $this->_msg["Suspend_page_cannot_be_start"] . "\" title=\"" . $this->_msg["Suspend_page_cannot_be_start"] . "\" width=15 height=18 border=0>";
					}

					$page_is_default = "";
				}
				// ----------------------------------------------------------

				// для страницы с флажком can_be_edited = 0
				// эти страницы нельзя удалить, отключить им публикацию, или сделать стартовой
				if (!$db->f("can_be_edited")) {
					$no_edit_text = $this->_msg["Info_service_page"];
					$page_set_default = "<img src=\"/i/admin/ico_nohome.gif\" alt=\"$no_edit_text\" title=\"$no_edit_text\" width=15 height=18 border=0>";
					$page_change_status = "<img src=\"/i/admin/ico_nosw.gif\" alt=\"$no_edit_text\" title=\"$no_edit_text\" width=15 height=18 border=0>";
					$page_delete = "<img src=\"/i/admin/ico_nodel.gif\" alt=\"$no_edit_text\" title=\"$no_edit_text\" width=15 height=18 border=0>";
				}
				if($db->f('parent_id') != $old_parent_id && $order == "parent_id, id") {
					$border_class = ' class="dark-border"';
				} else {
					$border_class = '';
				}
				$tpl->newBlock($blockname);
				$tpl->assign(Array(
					"MSG_Template" => $this->_msg["Template"],
					page_id						=>	$db->f('id'),
					page_parent_id				=>	$db->f('parent_id'),
					page_rank					=>	$db->f('page_rank'),
					page_title_id				=>	htmlspecialchars($this->_msg["Page_ID"] . ' "'.$db->f("title").'" ' . $this->_msg["is"] . ' '.$db->f('id')),
					page_title					=>	($show == 'bound' || $show == 'unbound') ? htmlspecialchars($db->f("title")) : $db->f("title"),
					page_link					=>	$db->f("link"),
					page_main_link				=>	$page_main_link,
					page_tpl_name				=>	$db->f("tpl_name"),
					page_tpl_id					=>	$db->f("tpl_id"),
					page_bind_status			=>	($db->f("bind_status")) ? $this->_msg["yes"] : "<span style=\"color: red\">" . $this->_msg["no"] . "</span>",
					page_date					=>	$db->f("date"),
					page_edit_action			=>	$page_edit,
					page_edit_rights_action		=>	$page_edit_rights,
					page_change_status_action	=>	$page_change_status,
					page_del_action				=>	$page_delete,
					page_set_default_action		=>	$page_set_default,
					page_is_default				=>	$page_is_default,
					border_class				=>	$border_class,
				));
				if ($rights["d"] || $rights["p"]) {
					$tpl->newBlock('block_check');
					$tpl->assign("page_id", $db->f('id'));
				}
				$old_parent_id = $db->f('parent_id');
			}
			return $ret;
		}
	}


	function show_actions_list() {
		global $CONFIG, $db, $tpl;
		$sql = "SELECT * FROM modules";
		$db->query($sql);
		if($db->nf()) {
			while($db->next_record()) {
				$module_id	 = $db->f("id");
				$module_name = $db->f("name");
				require_once("mod/".$module_name."/lib/class.".ucfirst($module_name).".php");
				$module = new $module_name('only_create_object');
				$tpl->newBlock("block_module");
				$tpl->assign(array(
								"id"	=> $module_id,
								"name"	=> $module_name,
								"title"	=> $db->f("title"),
							));
				if(is_array($module->block_main_module_actions))
				{
					foreach($module->block_main_module_actions as $k => $v)
					{
						$tpl->newBlock("block_main_module_action");
						$tpl->assign("id", $module_id);
						$tpl->assign("name", $k);
						$tpl->assign("title", addslashes($v));
					}
				}
				if(is_array($module->block_module_actions))
				{
					foreach($module->block_module_actions as $k => $v)
					{
						$tpl->newBlock("block_module_action");
						$tpl->assign("id", $module_id);
						$tpl->assign("name", $k);
						$tpl->assign("title", addslashes($v));
					}
				}
			}
		}
		return true;
	}




/**
 * Вывод списка модулей
 * @version	3.0
 * @param	string	$blockname	имя блока
 * @param	string	$active		не используется
 * @return	bool	TRUE в случае успешного выполнение и FALSE в ином случае
 */
	function show_module_list($blockname, $active = true) {
	  global $CONFIG, $db, $tpl, $server;

    $query = "SELECT modules.* FROM modules, sites_modules, sites";
    $query .= " WHERE alias='".substr($server, 0, -1)."' AND sites.id=site_id AND module_id=modules.id";
    $query .= ($active ? " AND !admin_only AND active" : "")." ORDER BY title";
    $db->query($query);
		if ($db->nf() > 0) {
			while($db->next_record()) {
				// вывод на экран списка
				$only_as_main = ($db->f('only_as_main')) ? ' (' . $this->_msg["only_as_main"] . ')' : '';
				$tpl->newBlock($blockname);
				$title = ($CONFIG["admin_lang"]!='rus')?$db->f('name') : $db->f('title');
				$tpl->assign(
					array(
						'module_title'		=> $title , #$db->f('title'),
						'module_name'		=> $db->f('name'),
						'module_id'			=> $db->f('id'),
						'module_available'	=> $db->f('only_as_main'),
						'only_as_main'		=> $only_as_main,
						'selected'			=> $slstr,
					)
				);
			}
			return TRUE;
		}
		return FALSE;
	}


/**
 * Вывод списка шаблонов
 * @version	3.0
 * @param	string		$blockname	имя блока
 * @param	string		$selected	выбранный элемент списка, по умолчанию равен пустой строке
 * @return	bool		возвращает TRUE
 */

	function show_templates_list($blockname, $selected = "") {
		global $CONFIG, $db, $tpl, $server, $lang, $baseurl;
		$db->query('SELECT * FROM '.$server.$lang.'_pages_templates ORDER BY id');
		if ($db->nf() > 0) {
			//$selected = "";
			$i = 0;
			while($db->next_record()) {
				// вывод на экран списка
				$i++;
				if ($i == 1) {
					$tr_begin = "<tr>";
					$tr_end = "";
				} else if ($i == 2) {
					$tr_begin = "";
					$tr_end = "</tr>";
					$i = 0;
				}
				$slstr = ($db->f('id') == $selected) ? "selected" : "";
				$tpl->newBlock($blockname);
				$tpl->assign(array(
					"MSG_Delete"	=>	$this->_msg["Delete"],
					"MSG_Template"	=>	$this->_msg["Template"],
					tpl_id			=>	$db->f('id'),
					tpl_name		=>	$db->f('tpl_name'),
					image_url		=>	$CONFIG['pages_tpl_image_path'].$this->table_prefix.'/'.$db->f('image_url'),
					selected		=>	$slstr,
					tr_begin		=>	$tr_begin,
					tr_end			=>	$tr_end,
					fields_cnt		=>	$db->f('number_of_fields'),
					main_module		=>	$db->f('main_module'),
					main_mod_str	=>	$db->f('main_module') ? $this->_msg["yes"] : $this->_msg["no"],
					tmpl_edit_link	=>	$baseurl."&action=tmpledit&id=".$db->f('id'),
					tmpl_file_path  =>	$this->tmpl_file_path.$this->table_prefix."/"."_index".$db->f('id').".html"
				));
				if (!$this->exist_tmpl_pages($db->f('id'))) {
					$tpl->newBlock('block_del_link');
					$tpl->assign(array(
						"MSG_Confirm_delete_template" => $this->_msg["Confirm_delete_template"],
						'tmpl_del_link'	=> $baseurl."&action=tmpldel&id=".$db->f('id'),
					));
				}
				if (file_exists(RP.$CONFIG['pages_tpl_image_path'].$this->table_prefix.'/'.$db->f('image_url'))) {
				    $tpl->newBlock('block_img');
				    $tpl->assign(array(
    					tpl_id			=>	$db->f('id'),
    					tpl_name		=>	$db->f('tpl_name'),
    					image_url		=>	$CONFIG['pages_tpl_image_path'].$this->table_prefix.'/'.$db->f('image_url'),
    					tmpl_file_path  =>	$this->tmpl_file_path.$this->table_prefix."/"."_index".$db->f('id').".html"
    				));
				}
			}
			return TRUE;
		}
	}
	
	function show_tpl_files()
	{   global $tpl, $baseurl;
		foreach (glob(RP.$this->tmpl_file_path.$this->table_prefix."/*.html") as $filename) {
            $tpl->newBlock('block_tmpl_file');
            $tpl->assign(array(
                'baseurl' => $baseurl,
                'filename' => basename($filename)
            ));
        }
	}


/**
 * Вывод информации о шаблоне по его id
 * @version 3.0
 * @param	int		$id			id шаблона
 * @param	string	$blockname	имя блока
 * @return	mixed	возвращает массив или FALSE в случае неудачи
 */
	function show_template($id, $blockname) {
		global $CONFIG, $db, $tpl, $server, $lang;
		$id = (int)$id;
		if (!isset($id) || !is_numeric($id)) return FALSE;
		$query = "SELECT * FROM ".$server.$lang."_pages_templates WHERE id = ".$id;
		$db->query($query);
		if ($db->nf() == 1) {
			$db->next_record();
			// вывод на экран
			$tpl->assign(
				array(
					$blockname."tpl_id"				=>	$db->f('id'),
					$blockname."tpl_name"			=>	$db->f("tpl_name"),
					$blockname."number_of_fields"	=>	$db->f("number_of_fields"),
					$blockname."image_url"			=>	$CONFIG['pages_tpl_image_path'].SITE_PREFIX.'/'.$db->f("image_url"),
				)
			);
			$main_module = ($db->f("main_module")) ? 1 : 0;
			// вернуть 1-ку если есть главный модуль и кол-во полей в шаблоне
			return array($main_module, $db->f("number_of_fields"));
		}
		return FALSE;
	}


/**
 * Вывод полей шаблона
 * @version	3.0
 * @param	string	$blockname			имя блока
 * @param	bool	$main_module		признак главного модуля
 * @param	int		$number_of_fields	количество полей в шаблоне
 * @return	bool	возвращает TRUE
 */
	function show_fields($blockname, $main_module, $number_of_fields) {
		global $CONFIG, $tpl;
		$begin = ($main_module) ? 0 : 1;
		for ($i = $begin; $i <= $number_of_fields; $i++) {
			$f_name = ($i == 0) ? $this->_msg["Main_block_field"] : $this->_msg["Block_field"];
			$tpl->newBlock($blockname);
			$tpl->assign(
				array(
					'f_name'	=> $f_name,
					'num'		=> $i,
//					'tr'		=> $tr,
				)
			);
		}
		return TRUE;
	}


/**
 * Вывод блоков страницы
 * @version	3.0
 * @param	string	$blockname		имя блока
 * @param	int		$page_id		id страницы
 * @param	int		$field_number	количество полей
 * @return	bool	TRUE в случае успешного выполнения, FALSE в обратном случае
 */
	function show_blocks($blockname, $page_id, $field_number) {
		global $CONFIG, $db, $tpl, $server, $lang;
		$page_id = (int) $page_id;
		if (!$page_id) return FALSE;
		$db->query('SELECT	pb.*,
							pb.id as id,
							pb.site_id site_id,
							pb.lang_id lang_id,
							pb.field_number as fn,
							m.name as name,
							m.id as module_id
						FROM	'.$server.$lang.'_pages_blocks as pb, modules as m
						WHERE	page_id = '.$page_id.' AND
								field_number = '.$field_number.' AND
								m.id = module_id
						ORDER BY block_rank');
		if ($db->nf() > 0) {
			$i = 0; // итератор для массива на JavaScript
			while ($db->next_record()) {
				$tpl->newBlock($blockname);
				$tpl->assign(
					array(
						'f'			=> $db->f('fn'),
						'i'			=> ++$i,
						'block_id'	=> $db->f('id'),
						'module_id'	=> $db->f('module_id'),
						'tpl'     	=> str_replace("'", "\'", $db->f('tpl')),
					)
				);
				// ----- свойства блока: -----
				for ($p = 0; $p < 9; $p++) {
					if ($p == 0) {
						$property = "'{$db->Record['action']}'";
					} else if ($p == 6) {
						$property = "'{$db->Record['linked_page']}'";
					} else if ($p == 7) {
						$property = "'{$db->Record['site_id']}'";
					} else if ($p == 8) {
						$property = "'{$db->Record['lang_id']}'";
					} else {
						$property = preg_match("/^[{\[].*[}\]]$/", $db->Record['property'.$p]) ? $db->Record['property'.$p] : "'{$db->Record['property'.$p]}'";
					}
					$tpl->newBlock('block_array_properties');
					$tpl->assign(
						array(
							'f'			=> $db->f('fn'),
							'block_id'	=> $db->f('id'),
							'property'	=> $property,
							'p'			=> $p,
						)
					);
				}
				// ----------------------------
			}
			return TRUE;
		}
		return FALSE;
	}


/**
 * Добавление страницы в базу данных
 * @version	3.0
 * @param	string		$title				заголовок страниц
 * @param	string		$alt_title			альтернативный заголовок страницы
 * @param	string		$link				ссылка на страницу
 * @param	string		$keywords			ключевые слова страницы (keywords)
 * @param	string		$description		описание страницы (description)
 * @param	string		$meta				дополнительные метатеги
 * @param	string		$redirect_url       адрес перехода
 * @param	string		$imagename			не используется
 * @param	int			$tpl_id				id шаблона
 * @param	array		$fields_blocks		блоки полей
 * @param	array		$block_properties	свойства блоков
 * @param	int			$permission_type	права доступа
 * @return	mixed	id страницы или FALSE в случае неудачи
 */
	function add_page($title, $add_title, $alt_title, $link,  $keywords,  $description, $meta, $redirect_url, $imagename, $tpl_id, $fields_blocks, $block_properties, $permission_type, $main_access = 1) {
		global $CONFIG, $db, $server, $lang;
		$title			= htmlspecialchars($title);
		$add_title		= $db->escape($add_title);
		$alt_title		= htmlspecialchars($alt_title);

		$title			= $db->escape($title);
		$alt_title		= $db->escape($alt_title);
		$link			= $db->escape(preg_replace("/\s/", '', $link));
		$keywords		= $db->escape($keywords);
		$description	= $db->escape($description);
		$meta			= $db->escape($meta);
		$redirect_url	= $db->escape($redirect_url);

		$tpl_id				= (int)$tpl_id;
		$main_module		= ($fields_blocks[0]) ? 1 : 0;
		$nf					= (int)$nf;
		$imagename			= ($imagename) ? '"'.$imagename.'"' : "NULL";
		$permission_type	= (int)$permission_type;

		$owner_id	= $_SESSION["siteuser"]["id"];
		/*
		$db->query("SELECT id FROM core_groups WHERE owner_id = ".$owner_id." ORDER BY id LIMIT 0,1");
		if (!$db->next_record()) return FALSE;
		*/
		list($group_id, $rights) = $this->get_group_and_rights('pages_default_rights');

		$query = "INSERT INTO ".$this->table_prefix."_pages (title, add_title, link, address, lang, alt_title, tpl_id, main_module, keywords, description, meta, redirect_url, image, permission_type, owner_id, usr_group_id, rights, main_access)
					VALUES ('".$title."', '".$add_title."', '".$link."', '".$link."/', '".$lang."', '".$alt_title."', ".$tpl_id.", ".$main_module.", '".$keywords."', '".$description."', '".$meta."', '".$redirect_url."', ".$imagename.", '".$permission_type."', ".$owner_id.", ".$group_id.", ".$rights.", ".($main_access ? 1 : 0).")";
		$db->query($query);
		if ($db->affected_rows() > 0) {
			$page_id = $db->lid();
			if ($page_id) {
			    $this->add_blocks($page_id, $fields_blocks, $block_properties);
			    $this->update_page_images($page_id);
			}
			return $page_id;
		}
		return FALSE;
	}


/**
 * Добавление блоков страницы в базу данных
 * @version	3.0
 * @param	int		$id					id страницы
 * @param	array	$fields_blocks		массив полей блоков
 * @param	array	$block_properties	свойства блоков
 * @return	bool	TRUE в случае успешного завершения, FALSE в случае не удачи
 */
	function add_blocks($page_id, $fields_blocks, $block_properties) {
		global $CONFIG, $db, $main, $lc, $server, $lang;
		if (!$page_id) return FALSE;
		if (is_array($fields_blocks)) {
			foreach ($fields_blocks as $field => $blocks) {
				$block_rank = 0;
				if (is_array($blocks)) {
					foreach ($blocks as $block_idx => $module_id) {
						$block_rank++;
						list($query, $linked_page_address) = $this->form_query_string("NULL", $page_id, $field, $module_id, $block_rank, $block_properties[$field][$block_idx]);
						$db->query($query);
						// зарегистрировать в таблице взаимосвязей ссылку из поля linked page address
						if ($db->affected_rows() > 0 && $linked_page_address) {
							$lid = $db->lid();
							$lc->add_internal_links($this->module_name, $server.$lang."_pages_blocks", "linked_page_address", $lid, $this->_msg["linked_page"], "<a href=\"/$linked_page_address\">linked page</a>");
						}
					}
				}
			}
		}
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}


/**
 * Обновление блоков страницы и их свойств в базе данных
 * @version	3.0
 * @param	int			$page_id			id страницы
 * @param	array		$fields_blocks		массив блоков полей
 * @param	array		$block_properties	массив свойств блоков
 * @return	bool	TRUE в случае успешного выполнения иначе FALSE
 */
	function update_blocks($page_id, $fields_blocks, $block_properties) {
		GLOBAL $CONFIG, $db, $main, $lc, $server, $lang;
		if (!$page_id) return FALSE;
		// выборка уже существующих блоков для их обновления
		$blocks_array = array();
		$db->query('SELECT *
						FROM '.$server.$lang.'_pages_blocks
						WHERE page_id = '.$page_id);
		if ($db->nf() > 0) {
			while($db->next_record()) {
				$blocks_array[] = $db->f('id');
			}
		}
		// удаляем все блоки
		$this->delete_blocks($page_id);
		// удаляем из pages_internal_links все записи связанные с таблицей pages_blocks и обновляемой страницей
		// указываем параметры удаления - модуль, таблицу, entry_id (в данном случае id блока)
		// удалив все записи, связанные со старыми страницами перехода, новые получим ниже через form_query_string
		// и занесем в таблицу pages_internal_links посредством функции add_internal_links
		foreach ($blocks_array as $entry_id) {
			$lc->delete_internal_links($this->module_name, $CONFIG['pages_blocks_table'], $entry_id);
		}
		if (is_array($fields_blocks)) {
			foreach ($fields_blocks as $field => $blocks) {
				$block_rank = 0;
				if (is_array($blocks)) {
					foreach ($blocks as $block_idx => $module_id) {
						$block_rank++;
						$id = (in_array($block_idx, $blocks_array)) ? $block_idx : 'NULL';
						list($query, $linked_page_address) = $this->form_query_string($id, $page_id, $field, $module_id, $block_rank, $block_properties[$field][$block_idx]);
						$db->query($query);
						// зарегистрировать в таблице взаимосвязей ссылку из поля linked page address
						if ($db->affected_rows() > 0 && $linked_page_address) {
							$lid = $db->lid();
							$lc->add_internal_links($this->module_name, $CONFIG['pages_blocks_table'], 'linked_page_address', $lid, $this->_msg["linked_page"], "<a href=\"/$linked_page_address\">linked page</a>");
						}
					}
				}
			}
		}
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}

/**
 * Формирование строки для обновления или добавления в БД
 * @version	3.0
 * @param	int					$id					id блока
 * @param	int					$page_id			id страницы
 * @param	int					$field				номер поля
 * @param	int					$module_id			id модуля
 * @param	int					$block_rank			ранк блока
 * @param	array				$block_properties	массив свойств блоков
 * @return	array	массив с SQL запросом и адресом
 */
	function form_query_string($id, $page_id, $field, $module_id, $block_rank, $block_properties = array()) {
		global $CONFIG, $db, $main, $server, $lang;
//		$block_properties[0] - действие (action) для блока
//		$block_properties[6] - страница перехода (linked_page), если блок с главным модулем
//		$block_properties[7] - ID сайта перехода
//		$block_properties[8] - ID языка сайта
//		остальные элементы - дополнительные свойства
        $str_begin = $str_end = '';
		for ($p = 0; $p <= 8; $p++) {
			$property = $block_properties[$p];
			$property = is_scalar($property)
				? ($property ? $db->escape($property) : '')
				: mysql_real_escape_string(json_encode($property));
			// действие для блока
			if ($p == 0) {
				$pname = 'action';
			// страница перехода
			} else if ($p == 6) {
				$pname		= 'linked_page';
				$address	= 'NULL';
				if ($field != 0) {
					if ($property) {
						$db->query('SELECT id, link, address FROM '.$this->table_prefix.'_pages WHERE link = "'.$property.'"');
					} else {
						// если страница перехода не определена, то определить её автоматически
						$main->get_linked_pages($module_id);
					}
					if ($db->nf() > 0) {
						$db->next_record();
						$property	= $db->f('link');
						$address	= $db->f('address');
						if ($address) {
							$return_address = $address;
							$address = '"'.$address.'"';
						}
					}
				}
			// ID сайта
			} else if ($p == 7) {
				$pname	= 'site_id';
			// ID языка сайта
			} else if ($p == 8) {
				$pname	= 'lang_id';
			// остальные свойства блока
			} else {
				$pname = 'property'.$p;
			}
			$str_begin .= ', '.$pname;
			$str_end .= ', "'.$property.'"';		
		}
		$str_begin .= ', tpl';
		$str_end .= ', "'.($block_properties['tpl'] ? My_Sql::escape($block_properties['tpl']) : '').'"';
		
		$query = "REPLACE INTO ".$server.$lang."_pages_blocks (id, page_id, field_number, module_id, block_rank, linked_page_address ".$str_begin.")
					VALUES (".$id.", ".$page_id.", ".$field.", ".$module_id.", ".$block_rank.", ".$address." ".$str_end.")";

		return array($query, $return_address);
	}


/**
 * Удаление блоков страницы из базы данных
 * @version	3.0
 * @param	int		$id		id страницы
 * @return	bool	TRUE в случае успешного завершение, FALSE в случае недачи
 */
	function delete_blocks($id = '') {
		global $CONFIG, $db, $server, $lang;
		$id		= (int)$id;
		if (!$id) return FALSE;
		$db->query('DELETE
						FROM '.$server.$lang.'_pages_blocks
						WHERE page_id = '.$id);
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}


/**
 * �?зменение страницы в базе данных
 * @version	3.0
 * @param	string		$title				заголовок страницы
 * @param	string		$alt_title			альтернативный заголовок страницы
 * @param	string		$keywords			ключевые слова (keywords)
 * @param	string		$description		описание (description)
 * @param	string		$meta				дополнительные метатеги
 * @param	string		$redirect_url       адрес перехода
 * @param	string		$imagename			не используется
 * @param	array		$fields_blocks		массив блоков полей
 * @param	array		$block_properties	массив свойств блоков
 * @param	int			$id					id страницы
 * @param	int			$permission_type	новые права доступа к странице
 * @param	int			$old_ptype			старые права доступа к странице
 * @return	bool	TRUE в случае успешного выполнения, FALSE в случае неудачи
 */
	function update_page($title, $add_title, $alt_title, $keywords,  $description, $meta, $redirect_url, $imagename, $fields_blocks, $block_properties, $id, $permission_type, $old_ptype, $new_link = '', $main_access = 1)
	{   global $CONFIG, $db;
	
		$ret = false;
		$id = (int)$id;
		if ($this->getPage($id)) {
		    $db->next_record();
		    $link = $db->Record['link'];
		    $address = $db->Record['address'];
		    $new_link = $db->escape(preg_replace("/\s/", '', $new_link));
    		$title			= $db->escape($title);
    		$add_title		= $db->escape($add_title);
    		$alt_title		= $db->escape($alt_title);
    		$link			= $db->escape($link);
    		$keywords		= $db->escape($keywords);
    		$description	= $db->escape($description);
    		$meta			= $db->escape($meta);
    		$redirect_url	= $db->escape($redirect_url);
    
    		$permission_type	= (int)$permission_type;
    		$old_ptype			= (int)$old_ptype;
    		$update_img_str		= ($imagename) ? ', image = "'.$imagename.'"' : '';
    		$db->query("UPDATE {$this->table_prefix}_pages
    									SET title = '".$title."',
    									    add_title = '".$add_title."',
    										alt_title = '".$alt_title."',
    										keywords = '".$keywords."',
    										description = '".$description."',
    										meta = '".$meta."' ".$update_img_str.",
    										redirect_url = '".$redirect_url."',
    										main_access =".($main_access ? 1 : 0)."
    									WHERE id = ".$id);
    
    		$this->update_bind_pages($id, old_ptype, $permission_type);
    		$this->update_blocks($id, $fields_blocks, $block_properties);
    		$this->update_page_images($id);
    		
    		$patt = "/(^|\/)".preg_quote($link, '/')."\//";
    		$repl = "\\1{$new_link}/";
    		
    		if ($new_link && $new_link <> $link) {
    		    $ret = false;
    		    $find = $db->fetchOne("{$this->table_prefix}_pages", array('clause' => array(
    		       'link' => $new_link
    		    )));
    		    if (! $find) {
    		        $ret = true;
    		        $links = array($db->escape($link));
    		        $new_address = preg_replace($patt, $repl, $address);
    		        $db->set(
        		        "{$this->table_prefix}_pages",
        		        array(
        		            'link' => $new_link,
        		            'address' => $new_address,
        		        ),
        		        array('id' => $id)
        		    );
    		        $childs = $this->getChilds($id);
        		    if (! empty($childs)) {
        		        foreach ($childs as $val) {
        		            $new_address = preg_replace($patt, $repl, $val['address']);
        		            $db->set(
                		        "{$this->table_prefix}_pages",
                		        array(
                		            'address' => $new_address,
                		        ),
                		        array('id' => $val['id'])
                		    );
        		            $links[] = $db->escape($val['link']);
        		        }
        		    }
    		    }
    		    
    		    $blocks = $db->fetchAll($this->table_prefix.'_pages_blocks', array('clause' => array(
        		    new DbExp("linked_page IN ('".implode("', '", $links)."')")
        		)));
        		if (! empty($blocks)) {
        		    foreach ($blocks as $val) {
        		    	$new_address = preg_replace($patt, $repl, $val['linked_page_address']);
            		    $upd = array(
            		        'linked_page_address' => $new_address,
                    	);
                    	if ($val['linked_page'] == $link) {
                    	    $upd['linked_page'] = $new_link;
                    	}
            		    $db->set("{$this->table_prefix}_pages_blocks", $upd, array('id' => $val['id']));
        		    }
        		}
    		}
    		$ret = true;
		}
		
		return $ret;
	}


/**
 * Удаление страницы из базы данных в табл.pages
 * и всех сопутствующей этой странице записей в других таблицах
 * @version	3.0
 * @param	int		$id		id страницы
 * @return	bool	TRUE в случае успешного выполнения, FALSE в случае неудачи
 */
	function delete_page($id = '') {
		global $CONFIG, $db, $main, $lc, $server, $lang;
		$id = (int)$id;
		if (!$id || !$this->test_item_rights("d", "id", $server.$lang."_pages", $id)) return FALSE;

//		$arr = array(); array_push($arr, $id); // $arr - массив с id-никами элемента
//		$arr = $this->unbind_elements($arr); // Сначала сделаем отвязку страницы и всех дочерних страниц
		$this->unbind_page($id); // Сначала сделаем отвязку страницы и всех дочерних страниц
		$query = "DELETE FROM ".$server.$lang."_pages
					WHERE id = ".$id;
		$db->query($query);
		if ($db->affected_rows() > 0) {
//			$this->delete_blocks($id);
			$query = "SELECT *
						FROM	".$server.$lang."_pages_blocks
						WHERE	page_id = $id AND
								!ISNULL(linked_page_address)";
			$db->query($query);
			if ($db->nf() > 0) {
				while ($db->next_record()) {
					$internal_links_arr[] = $db->f("id");
				}
			}
			if ($this->delete_blocks($id) && count($internal_links_arr) > 0) {
				foreach ($internal_links_arr as $idx) {
					$lc->delete_internal_links($this->module_name, $server.$lang."_pages_blocks", $idx);
				}
			}
			return TRUE;
		} else {
			return FALSE;
		}
	}


/**
 * Функция для создания структуры сайта, привязывает страницу к иерархическому дереву сайта,
 * по умолчанию $parent_id = 0 (корневой раздел)
 * @version	3.0
 * @param	int		$id			id страницы
 * @param	int		$parent_id	id родительской страницы
 * @return	bool	TRUE в случае успешного выполнения, FALSE в случае неудачи
 */
	function bind_page($id, $parent_id, $active, $page_rank ) {
		global $CONFIG, $db, $db1, $main, $lc, $server, $lang;
		$id			= (int)$id;
		$parent_id	= (int)$parent_id;
		if (!$id) return FALSE;
		// получить уровень вложенности
		list($level, $path, $address, $idx_arr) = $this->get_nav_path($parent_id);
		$level				= ($level) ? $level + 1: 1;
		$idx_arr[$level]	= $id;
		ksort($idx_arr);// сортировать по level страницы
		$branch_str			= implode(",", $idx_arr);
//		Обновим страницу перехода у всех блоков
		$query = "SELECT link, address FROM " . $server . $lang . "_pages WHERE id = '" . $id . "' LIMIT 1";
		$db->query($query);
		$db->next_record();
		$old_link = $db->f("link");
		$old_address = $db->f("address");
		$query = "UPDATE " . $server . $lang . "_pages_blocks SET linked_page_address = CONCAT('" . $address . "', linked_page, '/') WHERE linked_page = '" . $old_link . "' AND linked_page_address = '" . $old_address . "'";
		$db->query($query);

		$query		= "UPDATE ".$server.$lang."_pages
						SET parent_id	= ".$parent_id.",
							page_rank	= ".intval($page_rank).",
							level		= ".$level.",
							address		= CONCAT('".$address."',link,'/'),
							branch		= '".$branch_str."'
							".($active == -1 ? "" : ", active = ".$active)."
						WHERE id = ".$id;
		$db->query($query);
		// Вывод permission_type привязываемой страницы
		$query = "SELECT permission_type
					FROM ".$server.$lang."_pages
					WHERE id = ".$id;
		$db1->query($query);
		$db1->next_record();
		if($db1->f("permission_type") == 2) {
			$query = "SELECT permission_type
						FROM ".$server.$lang."_pages
						WHERE id = ".$parent_id;
			$db1->query($query);
			$db1->next_record();
			$parent_permission_type	= $db1->f("permission_type");
			if (($parent_permission_type == 1) || ($parent_permission_type == 3)) {
				$query = "UPDATE ".$server.$lang."_pages
							SET permission_type = 3
							WHERE id = ".$id;
				$db1->query($query);
			}
		}
		if ($db->affected_rows() > 0) {
			$lc->update_internal_links($id);
			return TRUE;
		} else {
			return FALSE;
		}
//		return ($db->affected_rows() > 0) ? 1 : 0;
	}


/**
 * Удаление элемента из иерархического дерева сайта
 * @version	3.0
 * @param	int		$id		id элемента
 * @return	bool	TRUE в случае успешного завершение выполнения, FALSE в случае неудачи
 */
	function unbind_page($id) {
		global $main, $lc;
		$id		= (int)$id;
		if (!$id) return FALSE;
		$arr	= array();
		array_push($arr, $id);								// $arr - массив с id-никами элемента и его дочерних элементов
		$arr	= $this->unbind_elements($arr);				// Рекурсивная функция
		if (count($arr) > 0) {
			foreach ($arr as $idx) {
				$lc->update_internal_links($idx);
			}
		}
		return TRUE;
	}


/**
 *
 * Рекурсивный метод, для удаления
 * из меню (иерархии сайта) элемента и его дочерних элементов
 * @version	3.0
 * @param	array	$arr
 * @return	array
 */
	function unbind_elements($arr) {
		global $CONFIG, $db, $main, $server, $lang;
		$str	= implode(",", $arr);
		$query	= "SELECT id, permission_type
					FROM ".$server.$lang."_pages
					WHERE parent_id IN (".$str.")
					GROUP BY id";
		$db->query($query);
		//echo $query;
		if ($db->nf() > 0) {
			while($db->next_record()) {
				array_push($arr,$db->f('id'));
			}
		}

//		Обновим страницу перехода у всех блоков
		foreach($arr as $k => $id)
		{
			$query = "SELECT link, address FROM " . $server . $lang . "_pages WHERE id = '" . $id . "' LIMIT 1";
			$db->query($query);
			$db->next_record();
			$old_link = $db->f("link");
			$old_address = $db->f("address");
			$query = "UPDATE " . $server . $lang . "_pages_blocks SET linked_page_address = CONCAT(linked_page, '/') WHERE linked_page = '" . $old_link . "' AND linked_page_address = '" . $old_address . "'";
			$db->query($query);
		}


		$str	= implode(",", $arr);
		$query	= "UPDATE ".$server.$lang."_pages
					SET parent_id = NULL,
						level = 0,
						page_rank = 0,
						address = CONCAT(link,'/'),
						branch = NULL
					WHERE id IN (".$str.")";
		$db->query($query);
		// При отвязывании 11 -> 10 для дочерних наследуемых элементов
		$query	= "UPDATE ".$server.$lang."_pages
					SET	permission_type = 2
					WHERE id IN (".$str.") AND
						  permission_type = 3";
		$db->query($query);
		if ($db->affected_rows() > 0) {
			return $this->unbind_elements($arr);
		}
		return $arr;
	}

/**
 *
 *
 * @param unknown_type $id
 * @param unknown_type $old_ptype
 * @param unknown_type $ptype
 * @return unknown
 */
	function update_bind_pages($id, $old_ptype, $ptype) {
		global $main, $lc, $db, $db1, $CONFIG, $server, $lang;
		$id = (int)$id;
		if (!$id) return FALSE;
		 // $arr - массив с id-никами элемента и его дочерних элементов
		$arr = array();
		array_push($arr, $id);
		//$old_ptype = 0;
		//$ptype = 1;
		// Двоичные представления видов доступа: бывшего и текущего
		$bin_old_ptype	= sprintf("%02d", base_convert($old_ptype, 10, 2));
		$bin_ptype		= sprintf("%02d", base_convert($ptype, 10, 2));
		// Разбиение двоичных представлений на биты. 1-й бит - Наследование, 2-й - Необходимость авторизации
		$bin_old_ptype1 = substr($bin_old_ptype, 0,1);
		$bin_old_ptype2 = substr($bin_old_ptype, 1,1);
		$bin_ptype1		= substr($bin_ptype, 0,1);
		$bin_ptype2		= substr($bin_ptype, 1,1);
		// Если из НЕнавследуемой страницу делаем наследуемую (т.е. первый бит 0->1), то возвратить permission_type родительского элемента
		if(($bin_old_ptype1 == 0) && ($bin_ptype1 == 1))  {
			// Выборка parent_id если известен id страницы
			$query = "SELECT parent_id
						FROM ".$server.$lang."_pages
						WHERE id = ".$id;
			$db->query($query);
			$db->next_record();
			if($db->f("parent_id")) {
				// Выборка permission_type
				$query = "SELECT permission_type
							FROM ".$server.$lang."_pages
							WHERE id = ".$db->f("parent_id");
				$db->query($query);
				$db->next_record();
				// 2-ой бит от родительского permission_type
				$bin_parent_ptype = sprintf("%02d", base_convert($db->f("permission_type"), 10, 2));
				//echo $bin_parent_ptype;
				$bin_parent_ptype2 = substr($bin_parent_ptype, 1, 1);
				// Новый тип permission_type на основании полученного родительского
				$ptype = base_convert($bin_ptype1.$bin_parent_ptype2, 2, 10);
				$query = "UPDATE ".$server.$lang."_pages
							SET permission_type = ".$ptype."
							WHERE id = ".$id;
				$db->query($query);
			}
		}

		//if(!(($permission_type == 2) && ($old_ptype == 3))) {
		// Если текущий доступ - 11, то не менять на 10
		if(!(($bin_old_ptype == 11) && ($bin_ptype == 10)))  {
			$query = "UPDATE ".$server.$lang."_pages
						SET permission_type = ".$ptype."
						WHERE id = ".$id;
			$db->query($query);
			$bin_ptype = sprintf("%02d", base_convert($ptype, 10, 2));
			$bin_ptype2 = substr($bin_ptype, 1, 1);
			$arr = $this->update_bind_elements($arr, $id, $bin_ptype2); // Рекурсивная функция
		}

		if (count($arr) > 0) {
			foreach ($arr as $idx) {
				$lc->update_internal_links($idx);
			}
		}
		return TRUE;
	}

	// Функция для редактирования структуры сайта
	// Рекурсивная функция (вызывающая сама себя) для удаления
	// из меню (иерархии сайта) элемента и его дочерних элементов
	function update_bind_elements($arr, $id, $bin_ptype2) {
		global $CONFIG, $db, $main, $server, $lang;
		$str = implode(",", $arr);
		$query = "SELECT id, permission_type
					FROM ".$server.$lang."_pages
					WHERE parent_id IN ($str)
					GROUP BY id";
		$db->query($query);
		//echo $query;
		if ($db->nf() > 0) {
			while($db->next_record()) {
				array_push($arr,$db->f('id'));
			}
		}
		//$str = implode(",", $arr);
		//$query = "UPDATE {$CONFIG["pages_table"]} SET parent_id = NULL, level = 0, address = CONCAT(link,'/'), branch = NULL WHERE id IN ($str)";
		//$db->query($query);
		// При отвязывании 11 -> 10 для дочерних наследуемых элементов
		if($bin_ptype2 == 0) {
			$query = "UPDATE ".$server.$lang."_pages
						SET permission_type = 2
						WHERE	parent_id IN ($str) AND
								permission_type = 3 AND
								id <>".$id;
		}
		if($bin_ptype2 == 1) {
			$query = "UPDATE ".$server.$lang."_pages
						SET permission_type = 3
						WHERE	parent_id IN (".$str.") AND
								permission_type = 2 AND
								id <>".$id;
		}
		//echo $query."<br>";
		$db->query($query);
		if ($db->affected_rows() > 0) {
			return $this->update_bind_elements($arr, $id, $bin_ptype2);
		}
		return $arr;
	}
	// *******************

/**
 * �?змененние структуры сайта, поднимает элемент вверх или опускает вниз
 * в иерархическом дереве сайта в зависимости от параметра operation
 * @version	3.0
 * @param	int		$id			id страницы
 * @param	int		$opertion	операция (по умолчанию _up_), возможные значения up и любое другое
 * @return	bool	TRUE в случае успешного выполнения, FALSE в случае не удачи
 */
	function change_order($id, $operation = "up") {
		global $CONFIG, $db, $server, $lang;
		$id			= (int)$id;
		if (!$id) return FALSE;
		$order_by	= ($operation == "up") ? "page_rank" : "page_rank DESC";			// от ORDER BY зависит
		if (!$this->getPage($id)) return FALSE;
		$db->next_record();
		$pid = $db->f('parent_id');
		$query = "SELECT *
					FROM ".$server.$lang."_pages
					WHERE parent_id = ".$pid."
					ORDER BY ".$order_by;
		$db->query($query);
		if ($db->nf() > 0) {
			while($db->next_record()) {
				if ($db->f('id') == $id) {
					$page_rank = $db->f('page_rank');
					if ($db->next_record()) {
						$page_rank1 = $db->f('page_rank'); $id1 = $db->f('id');
						$query = "UPDATE ".$server.$lang."_pages
									SET page_rank = ".$page_rank1."
									WHERE id = ".$id;
						$db->query($query);
						$query = "UPDATE ".$server.$lang."_pages
									SET page_rank = ".$page_rank."
									WHERE id = ".$id1;
						$db->query($query);
					} else {
						return FALSE;
					}
					break;
				}
			}
		}
		return TRUE;
	}


/**
 * Определение главной страницы (default)
 * @version	3.0
 * @param	int		$id		id страницы которую следует сделать корневой
 * @return	bool	TRUE в случае успеха, FALSE в случае неудачи
 */
	function set_default_page($id = "") {
		global $CONFIG, $db, $server, $lang;
		$id		= (int)$id;
		if (!$id) return FALSE;
		$query	= "SELECT *
					FROM ".$server.$lang."_pages
					WHERE is_default = 1";
		$db->query($query);
		if ($db->nf() > 0) {
			$db->next_record();
			$old_id = $db->f("id");
		}
		$query = "UPDATE ".$server.$lang."_pages
					SET is_default = 1
					WHERE id = ".$id;
		$db->query($query);
		if ($db->affected_rows() > 0) {
			if ($old_id) {
				$query = "UPDATE ".$server.$lang."_pages
							SET is_default = 0
							WHERE id = ".$old_id;
				$db->query($query);
			}
			return TRUE;
		}
		return FALSE;
	}


/**
 * Получение массива контекстных меню
 * @version	3.0
 * @param	int		$selected_id	id выбранного пункта меню
 * @return	mixed	Количество пунктов меню, в случае не удачи FALSE
 */
	function getContextMenuList($selected_id = '') {
		GLOBAL $CONFIG, $db, $baseurl, $server, $lang;
		$selected_id = (int)$selected_id;
		$db->query('SELECT * FROM '.$server.$lang.'_pages_context_menu ORDER BY id');
		if ($db->nf() > 0) {
			while($db->next_record()) {
				$selected	= ($db->f('id') == $selected_id) ? ' selected' : '';
				$arr[]		= array('menu_id'			=> $db->f('id'),
									'menu_name'			=> $db->f('name'),
									'menu_edit_link'	=> $baseurl.'&action=editcm&id='.$db->f('id'),
									'menu_del_link'		=> $baseurl.'&action=delcm&id='.$db->f('id'),
									'selected'			=> $selected,
									);
			}
			return $arr;
		}
		return FALSE;
	}

/**
 * Вывод списка контекстных меню
 * @version	3.0
 * @param	int		$selected_id	id выбранного пункта меню
 * @return	mixed	Количество пунктов меню, в случае не удачи FALSE
 */
	function show_context_menu_list($selected_id = "") {
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang, $permissions;
		$selected_id = (int)$selected_id;
		$this->get_list_with_rights("cm.*", $server.$lang."_pages_context_menu cm", "cm", "", "", "id");
		if ($db->nf() > 0) {
			while($db->next_record()) {
				$is_owner = $db->f("is_owner");
				$rights = $db->f("user_rights");
				$rights = array("d" => (($rights & 8) >> 3) && $permissions["d"],
								"p" => (($rights & 4) >> 2) && $permissions["p"],
								"e" => (($rights & 2) >> 1) && $permissions["e"],
								"r" => ($rights & 1) && $permissions["r"],
							);
				// вывод на экран списка
				$selected = ($db->f("id") == $selected_id) ? " selected" : "";
				// редактирование :
				$menu_edit_action = $rights["e"]
							? '<a href="'.$baseurl.'&action=editcm&id='.$db->f("id").'"><img src="/i/admin/ico_edit.gif" alt="'.$this->_msg["Edit"].'" title="'.$this->_msg["Edit"].'" width=15 height=18 border=0></a>'
							: "<img src=\"/i/admin/ico_edit_.gif\" alt=\"".$this->_msg["No_edit"]."\" title=\"".$this->_msg["No_edit"]."\" width=15 height=18 border=0>";

				// редактирование прав :
				$menu_edit_rights = $rights["e"] && $is_owner
							? "<a href=\"$baseurl&action=editcmrights&id=".$db->f("id")."\"><img src=\"/i/admin/ico_edit_rights.gif\" alt=\"".$this->_msg["Edit_rights"]."\" title=\"".$this->_msg["Edit_rights"]."\" width=15 height=18 border=0></a>"
							: "<img src=\"/i/admin/ico_edit_rights_.gif\" alt=\"".$this->_msg["No_edit"]."\" title=\"".$this->_msg["No_edit"]."\" width=15 height=18 border=0>";

				// удаление :
				$menu_delete = $rights["d"]
							? "<a href=\"$baseurl&action=delcm&id=".$db->f("id")."\"><img src=\"/i/admin/ico_del.gif\" alt=\"".$this->_msg["Delete"]."\" title=\"".$this->_msg["Delete"]."\" width=15 height=18 border=0></a>"
							: "<img src=\"/i/admin/ico_nodel.gif\" alt=\"".$this->_msg["No_delete"]."\" title=\"".$this->_msg["No_delete"]."\" width=15 height=18 border=0>";
				$tpl->newBlock("block_menu_list");
				$tpl->assign(Array(
					"menu_id"			=>	$db->f("id"),
					"menu_name"			=>	$db->f("name"),
					"menu_edit_action"	=>	$menu_edit_action,
					"menu_edit_rights_action"	=>	$menu_edit_rights,
					"menu_del_action"	=>	$menu_delete,
					"selected"			=>	$selected,
				));
			}
			return $db->nf();
		}
		return FALSE;
	}


/**
 * Вывод контекстного меню по id
 * @version	3.0
 * @param	int		$id		id контекстного меню
 * @return	bool	TRUE в случае успеха, FALSE в случае не удачи
 */
	function show_context_menu_adm($id = "") {
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;
		$id		= (int)$id;
		if (!$id) return FALSE;
		$query	= "SELECT * FROM ".$server.$lang."_pages_context_menu WHERE id = ".$id;
		$db->query($query);
		if ($db->nf() > 0) {
			$db->next_record();
			$tpl->assign(
				array(
					menu_name	=>	htmlspecialchars($db->f("name")),
					menu_id		=>	$db->f("id"),
				)
			);
			$this->show_context_menu_items($db->f('id'));
			return TRUE;
		}
		return FALSE;
	}


/**
 * Вывод пунктов контекстного меню
 * @version	3.0
 * @param	int		$id		id контекстного меню
 * @return	mixed	Количество пунктов контекстного меню, FALSE в случае не удачи
 */
	function show_context_menu_items($id = "") {
		global $CONFIG, $db, $tpl, $server, $lang;
		$id = (int)$id;
		if (!$id) return FALSE;
		$query = "SELECT pcmi.*, p.title as title
					FROM ".$server.$lang."_pages_context_menu_items as pcmi, ".$server.$lang."_pages as p
					WHERE	pcmi.menu_id = ".$id." AND
							p.id = pcmi.page_id
					ORDER BY item_rank";
		$db->query($query);
		if ($db->nf() > 0) {
			$i = 0;
			while($db->next_record()) {
				// вывод на экран списка, формируем массив на JScript
				$i++;
				$tpl->newBlock('fill_array');
				$tpl->assign(
					array(
						item_title		=> addslashes($db->f("title")),
						item_iterator	=> $i,
						item_id			=> $db->f("page_id"),
						/*
						v_answer	=>	$variant,
						v_iterator	=>	$i,
						v_id	=>	$db->f("id"),
						*/
					)
				);
			}
			return $db->nf();
		}
		return FALSE;
	}


/**
 * Добавление контекстного меню
 * @version	3.0
 * @param	string	$menu_name		название контекстного меню
 * @param	array	$menu_items		массив пунктов контекстного меню $menu_name
 * @return	mixed	Id созданного контекстного меню, FALSE в случае неудачи
 */
	function add_context_menu($menu_name, $menu_items = array()) {
		global $CONFIG, $main, $db, $server, $lang;
		$menu_name = $db->escape($menu_name);
		if (!$menu_name) return FALSE;
		$owner_id	= $_SESSION["siteuser"]["id"];
    list($group_id, $rights) = $this->get_group_and_rights('pages_default_rights');

		$query = "INSERT INTO ".$server.$lang."_pages_context_menu (name,owner_id,usr_group_id,rights)
					VALUES ('".$menu_name."',".$owner_id.",".$group_id.",".$rights.")";
		$db->query($query);
		if ($db->affected_rows() > 0) {
			$lid = $db->lid();
			$this->add_context_menu_items($lid, $menu_items);
			return $lid;
		}
		return FALSE;
	}


/**
 * Добавление пунктов контекстного меню
 * @version	3.0
 * @param	int		$id				id контекстного меню
 * @param	array	$menu_items		массив пунктов контекстного меню $id
 * @return	bool	TRUE в случае успеха, FALSE в случае неудачи
 */
	function add_context_menu_items($id = "", $menu_items = array()) {
		global $CONFIG, $db, $server, $lang;
		$id = (int)$id;
		if (!$id) return FALSE;
		if (!(count($menu_items) > 0)) return FALSE;
		$i = 0;
		while (list($key, $val) = each($menu_items)) {
			if ($val) {
				$i++;
				$val = (int)$val;
				$query = "INSERT INTO ".$server.$lang."_pages_context_menu_items (menu_id, page_id, item_rank)
							VALUES (".$id.", ".$val.", ".$i.")";
				$db->query($query);
			}
		}
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}


/**
 * �?зменение контекстного меню
 * @version	3.0
 * @param	string	$menu_name		название контекстного меню
 * @param	int		$id				id контекстного меню
 * @param	array	$menu_items		массив пунктов контекстного меню
 * @return	bool	TRUE в случае успеха, FALSE в случае неудачи
 */
	function update_context_menu($menu_name, $id, $menu_items = array()) {
		global $CONFIG, $main, $db, $server, $lang;
	    $id = (int)$id;
	    if (!$id) return FALSE;
		$menu_name = $db->escape($menu_name);
		if (!$menu_name) return FALSE;
		$query = "UPDATE ".$server.$lang."_pages_context_menu
					SET name = '".$menu_name."'
					WHERE id = ".$id;
		$db->query($query);
		$this->update_context_menu_items($id, $menu_items);
		return TRUE;
	}


/**
 * �?менение пунктов контекстного меню $id (предварительно посредством delete_context_menu_items удаляются все старые записи)
 * @version	3.0
 * @param	int		$id
 * @param	array	$menu_items
 * @return	bool	TRUE  в случае успеха, FALSE в случае неудачи
 */
	function update_context_menu_items($id = "", $menu_items = array()) {
		global $CONFIG, $db, $server, $lang;
		$id = (int)$id;
		if (!$id) return FALSE;
		$items_array = array();
		if (count($menu_items) > 0) {
			// сделать выборку уже существующих блоков для их обновления
			$query = "SELECT *
						FROM ".$server.$lang."_pages_context_menu_items
						WHERE menu_id = ".$id;
			$db->query($query);
			if ($db->nf() > 0) {
				while($db->next_record()) {
					$items_array[] = $db->f("id");
				}
			}
		}
		$this->delete_context_menu_items($id);
		if (count($menu_items) > 0) {
			while (list($key, $val) = each($menu_items)) {
				if(in_array($key, $items_array)) {
					$item_id = $key;
				} else {
					$item_id = "NULL";
				}
				if ($val) {
					$i++;
					$val = intval($val);
					$query = "INSERT INTO ".$server.$lang."_pages_context_menu_items (id, page_id, item_rank, menu_id)
								VALUES (".$item_id.", ".$val.", ".$i.", ".$id.")";
					$db->query($query);
				}
			}
		}
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}


/**
 * Удаление контекстного меню
 * @version	3.0
 * @param	int		$id		id контекстного меню
 * @return	bool	TRUE в случае успеха, FALSE в случае неудачи
 */
	function delete_context_menu($id = "") {
		global $CONFIG, $db, $server, $lang;
		$id = (int)$id;
		if (!$id) return FALSE;
		$query = "DELETE FROM ".$server.$lang."_pages_context_menu WHERE id = ".$id;
		$db->query($query);
		if ($db->affected_rows() > 0) {
			$this->delete_context_menu_items($id);
			return TRUE;
		}
		return FALSE;
	}


/**
 * Удаление пунктов контекстного меню $id
 * @version	3.0
 * @param	int		$id		id контекстного меню, которому принадлежат удаляемые пункты
 * @return	bool	TRUE в случае успеха, FALSE в случае неудачи
 */
	function delete_context_menu_items($id = "") {
		global $CONFIG, $db, $server, $lang;
		$id		= (int)$id;
		if (!$id) return FALSE;
		$query	= "DELETE FROM ".$server.$lang."_pages_context_menu_items WHERE menu_id = ".$id;
		$db->query($query);
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}


/**
 * Вывод списка внутренних битых ссылок
 * @version	3.0
 * @param	int		$start	начальная позиция
 * @param	int		$rows	количество строк
 * @param	string	$type	тип внутренних ссылок (_broken_ если требуются битые сслыки, иначе пустая строка)
 * @return	bool	TRUE в случае успешного выполнения, FALSE в случае неудачи
 */
	function show_internal_links($start, $rows, $type = "") {
		global $CONFIG, $db, $db1, $tpl, $core, $baseurl, $server, $lang;
		$start_row = ($start - 1) * $rows;
		if ($CONFIG["multilanguage"]) $lang_str = "lang=".$lang."&";
		if ($type == "broken") {
			$where_str = "WHERE status = '404'";
		} else {
			$where_str = "";
		}
		$query = "SELECT * FROM ".$server.$lang."_pages_internal_links ".$where_str." LIMIT ".$start_row.",".$rows;
		$db->query($query);
		if ($db->nf() > 0) {
			$tpl->newBlock("block_internal_links");
			$tpl->assign(Array(
				"MSG_Link" => $this->_msg["Link"],
				"MSG_Description" => $this->_msg["Description"],
				"MSG_Edit_record" => $this->_msg["Edit_record"],
			));
			$i = 0;
			while($db->next_record()) {
				/*
				$link = (substr($db->f("link"),0,1) != "/") ? "/" . $db->f("link") : $db->f("link");
				if ($CONFIG["multilanguage"]) $link = "/$lang$link";
				*/
//				$link = (substr($db->f("link"),0,1) != "/") ? "/" . $db->f("link") : $db->f("link");
				preg_match("/([^/]*)/([[:space:]]*)$/i", $db->f("link"), $arr);
				$link				= $core->formPageLink($arr[1], $db->f("link"), $lang);
				$entry_id			= $db->f("entry_id");
				$module_name		= $db->f("module_name");
				$status_color		= ($db->f("status") == "404") ? "red" : "green";
				$table_name			= $db->f("table_name");
				$field_name			= $db->f("field_name");
				$description		= $db->f("description");
				$entry_edit_link	= "admin.php?".$lang_str."name=".$module_name."&action=edit&id=".$entry_id;
				if ($table_name == "pages_blocks" && $field_name == "linked_page_address") {
					$query = "SELECT ".$server.$lang."_pages.id as id
								FROM	".$server.$lang."_pages,
										".$server.$lang."_pages_blocks
								WHERE	".$server.$lang."_pages_blocks.id = ".$entry_id." AND
										".$server.$lang."_pages.id = ".$server.$lang."_pages_blocks.page_id";
					$db1->query($query);
					if ($db1->nf() > 0) {
						$db1->next_record();
						$entry_edit_link = "admin.php?".$lang_str."name=".$module_name."&action=edit&id=".$db1->f("id");
					} else {
						$entry_edit_link = "";
					}
				}
				$entry_edit_icon = ($entry_edit_link) ? "<a href=\"$entry_edit_link\"><img src=\"/i/admin/ico_edit.gif\" alt=\"" . $this->_msg["Edit"] . "\" title=\"" . $this->_msg["Edit"] . "\" width=15 height=18 border=0></a>" : "-";
				// вывод на экран списка
				$tpl->newBlock("block_internal_link");
				$tpl->assign(Array(
					"MSG_module" => $this->_msg["module"],
					"MSG_table" => $this->_msg["table"],
				));
				$tpl->assign(
					array(
						"link"				=> $link,
						"status_color"		=> $status_color,
						"module_name"		=> $module_name,
						"module_link"		=> "admin.php?".$lang_str."name=".$module_name,
						"table_name"		=> $table_name,
						"field_name"		=> $field_name,
						"entry_id"			=> $entry_id,
						"description"		=> $description,
						"entry_edit_link"	=> $entry_edit_link,
						"entry_edit_icon"	=> $entry_edit_icon,
					)
				);
			}
			return TRUE;
		}
		return FALSE;
	}


/**
 * Вывод карты сайта
 * @version	3.0
 * @param	string	$page_type	тип (возможные значения: bound, unbound)
 * @param	string	$tpl_label	название метки
 * @return	mixed	TRUE в случае успешного выполнения, иначе Void
 */
/*	function create_sitemap_tree($page_type, $tpl_label) {
		global $tpl;
		if ($page_type == "bound") {
			$html_output = $this->get_bound_pages_in_tree();
		} else if ($page_type == "unbound") {
			$html_output = $this->get_unbound_pages_in_tree();
		} else {
			return;
		}
		$tpl->assign(
			array(
				"_ROOT.".$tpl_label	=>	$html_output,
			)
		);
		return TRUE;
	}
*/

/**
 * Получение дерева сайта для редактирования
 * @version	3.0
 * @return	string	HTML код
 */
	function get_bound_pages_in_tree() {
		global $CONFIG, $core, $db, $tpl, $baseurl, $CONFIG, $server, $lang;
		$home_page	= (isset($lang) && $lang == "eng") ? "Home" : "Главная";
		$home_link	= $core->formPageLink("", "", $lang);
 	  	$icon		= "";
    	$menu		= new TreeMenu();
		$text		= "<span id=b_page_0><a href=$home_link target=_blank title=\"" . $this->_msg["see_page_on_site"] . "\"><img src=\"/" . $CONFIG["admin_img_path"] . "ico_ishome.gif\" border=0 align=middle></a>&nbsp;<span onClick=\"selectParentPage(0);\" style=\"cursor: hand;\">$home_page</span></span>";
		$array		= array('text' => $text, 'icon' => $icon, 'isDynamic'	=>	false, 'ensureVisible' => true);
		$node[0]	= new TreeNode($array);
		$menu->addItem($node[0]);
		$query		= "SELECT *
						FROM ".$server.$lang."_pages
						WHERE !ISNULL(parent_id) AND
							  is_default != 1
						ORDER BY page_rank DESC";
		$db->query($query);
		if ($db->nf() > 0) {
			while($db->next_record()) {
				$id			= $db->f('id');
				$pid		= $db->f('parent_id');
				$title		= str_replace(array("\n", "\r"), array(" ", "2&nbsp;"), $db->f('title'));
				$title = addslashes($title);
				$edit_link = "/admin.php?lang=" . $lang . "&name=pages&action=edit&id=" . $id . "";
				$page_link	= $core->formPageLink($db->f("link"), $db->f("address"), $lang);
				$img		= ($db->f('active')) ? "ico_page.gif" : "ico_page_noactive.gif";
				$text		= "<span id=b_page_$id><a href=\"$edit_link\" title=\"" . $this->_msg["edit_page"] . "\"><img src=\"/" . $CONFIG["admin_img_path"] . "ico_edit.gif\" border=0 align=middle></a>&nbsp;<a href=$page_link target=_blank title=\"" . $this->_msg["see_page_on_site"] . "\"><img src=\"/" . $CONFIG["admin_img_path"] . "$img\" border=0 align=middle></a>&nbsp;<span onClick=\"selectParentPage($id);\" style=\"cursor: hand;\">".$title."</span></span>";
				$array		= array('text' => $text, 'icon' => $icon, 'isDynamic'	=>	false, 'ensureVisible' => true);
			    $node[$id]	= new TreeNode($array);
				$arr[$id]	= $array;
			    $child_nodes .= "if (is_object(\$node[$pid])) \$node[$pid]->addItem(\$node[$id]);";
			}
			eval($child_nodes);
		}
		$treeMenu = &new TreeMenu_DHTML($menu, array('images' => "/" . $CONFIG["tree_img_path"], 'defaultClass' => 'treeMenuDefault', 'noTopLevelImages'	=>	false));
		return $treeMenu->toHTML();
	}


/**
 * Получение списка непривязанных страниц
 * @version	3.0
 * @return	strign	HTML код
 */
	function get_unbound_pages_in_tree() {
		global $CONFIG, $core, $db, $tpl, $baseurl, $CONFIG, $server, $lang;
	 	$icon = "";
		$menu  = new TreeMenu();
		$text = "<span><img src=\"/" . $CONFIG["admin_img_path"] . "ico_tree.gif\" border=0 align=middle>&nbsp;<span style=\"cursor: hand;\">" . $this->_msg["Unbound_pages"] . "</span></span>";
		$array = array('text' => $text, 'icon' => $icon, 'isDynamic'	=>	false, 'ensureVisible' => true);
		$node[0] = new TreeNode($array);
		$menu->addItem($node[0]);
		$query = "SELECT *
					FROM ".$server.$lang."_pages
					WHERE ISNULL(parent_id) AND
						  is_default != 1
					ORDER BY page_rank DESC";
		$db->query($query);
		if ($db->nf() > 0) {
			while($db->next_record()) {
				$id			= $db->f('id');
				$pid		= 0; // так как parent на самом деле NULL для непривязанных страниц
				$title		= str_replace(array("\n", "\r"), array(" ", "2&nbsp;"), $db->f('title'));
				$title      = addslashes($title);
				$edit_link = "/admin.php?lang=" . $lang . "&name=pages&action=edit&id=" . $id . "";
				$page_link	= $core->formPageLink($db->f("link"), $db->f("address"), $lang);
				$img		= ($db->f('active')) ? "ico_page.gif" : "ico_page_noactive.gif";
				$text		= "<span id=unb_page_$id><a href=\"$edit_link\" title=\"" . $this->_msg["edit_page"] . "\"><img src=\"/" . $CONFIG["admin_img_path"] . "ico_edit.gif\" border=0 align=middle></a>&nbsp;<a href=$page_link target=_blank title=\"" . $this->_msg["see_page_on_site"] . "\"><img src=\"/" . $CONFIG["admin_img_path"] . "$img\" border=0 align=middle></a>&nbsp;<span onClick=\"selectChildPage($id);\" style=\"cursor: hand;\">$title</span></span>";
				$array		= array('text' => $text,
									'icon' => $icon,
									'isDynamic'	=>	false,
									'ensureVisible' => true);
			    $node[$id]		= new TreeNode($array);
				$arr[$id]		= $array;
			    $child_nodes   .= "if (is_object(\$node[$pid])) \$node[$pid]->addItem(\$node[$id]);";
			}
			eval($child_nodes);
		}
		$treeMenu = &new TreeMenu_DHTML($menu, array(	'images'			=> "/".$CONFIG["tree_img_path"],
															'defaultClass'		=> 'treeMenuDefault',
															'noTopLevelImages'	=>	false));
		return $treeMenu->toHTML();
	}

/**
 * Получение списка страниц, доступных для редактирования и их родителей
 * @version	3.0
 * @return	array
 */
	function get_editable_pages_tree() {
		global $db, $server, $lang;
		$childs = array();
		$items = array();
		$this->get_list_with_rights("*", $server.$lang."_pages p", "p",
									"is_default != 1 AND parent_id IS NOT NULL", "", "page_rank DESC");
		if($db->nf() > 0) {
			$showble_pages = array(0);
			while($db->next_record()) {
				$id = $db->f('id');
				$a_class = $li_class = array();
				$childs[$db->f('parent_id')][] = $id;
				$items[$id]->id = $id;
				$items[$id]->title = str_replace(array("\n", "\r"), array(" ", "&nbsp;"), $db->f('title'));
				$items[$id]->tpl_id = $db->f('tpl_id');
				$items[$id]->editlink = "/admin.php?lang=" . $lang . "&name=pages&action=edit&id=" . $db->f('id') . "";
                $items[$id]->viewlink =  Core::formPageLink($db->f('link'), $db->f('address'), $lang) . "";
				if (! $db->f('active')) {
				    $a_class[] = 'inactive';
				}
				if ($db->f('user_rights') & 2) {
					$tmp_showble_pages = explode(",", $db->f('branch'));
					for ($i=0; $i < sizeof($tmp_showble_pages); $i++) {
						if (!empty($tmp_showble_pages[$i]) && !in_array($tmp_showble_pages[$i], $showble_pages))
							$showble_pages[] = $tmp_showble_pages[$i];
					}
				} else {
					$li_class[] = 'locked';
				}
				$items[$id]->classname = implode(' ', $a_class);
				$items[$id]->liclassname = implode(' ', $li_class);
			}

			foreach ($childs as $pid => $ids) {
				if (in_array($pid, $showble_pages)) {
					for ($i=0; $i < sizeof($ids); $i++) {
						if (!in_array($childs[$pid][$i], $showble_pages)) unset($childs[$pid][$i]);
					}
				} else {
					unset($childs[$pid]);
				}
			}
		}
		return array($childs, $items);
	}

/**
 * Получение дерева сайта для редактирования (рекурсивная)
 * @version	3.0
 * @return	string	HTML код
 */
	function show_bound_pages(&$childs, &$items, $parent_id = 0) {
		global $CONFIG, $core, $db, $tpl, $baseurl, $CONFIG, $server, $lang;

		if(!isset($childs[$parent_id])) return "";

		$html = '';
		if($parent_id != 0 && sizeof($items)) $html .= '<ul>';
		foreach($childs[$parent_id] as $idx => $id)
		{
		    $html .= sprintf('<li id="page%s" class="%s"><i class="ico %s"></i><a href="%s" xhref="%s" data-tpl-id="%s" data-id="%s">%s</a>',
		        $id, $items[$id]->liclassname, $items[$id]->classname, $items[$id]->editlink,
		        $items[$id]->viewlink, $items[$id]->tpl_id, $items[$id]->id, $items[$id]->title);
		    
		    $html .= $this->show_bound_pages($childs, $items, $id);
		}
		if($parent_id != 0 && sizeof($items)) $html .= '</ul>';

		return $html;
	}


/**
 * Получение списка непривязанных страниц
 * @version	3.0
 * @return	strign	HTML код
 */
	function show_unbound_pages() {
		global $CONFIG, $core, $db, $tpl, $baseurl, $CONFIG, $server, $lang;
		$this->get_list_with_rights("*", $server.$lang."_pages p", "p",
									"ISNULL(parent_id) AND is_default != 1", "", "page_rank DESC");
		$html = '';
		if ($db->nf() > 0) {
			while($db->next_record()) {
				if ($db->f('user_rights') & 2) {
					$id = $db->f('id');
					$title = str_replace(array("\n", "\r"), array(" ", "&nbsp;"), $db->f('title'));
					$title = addslashes($title);
					$editlink = "/admin.php?lang=" . $lang . "&name=pages&action=edit&id=" . $id . "";
					$classname = $db->f('active') ? '' : 'nactive';
					/*
					$html .= '<li id="page'. $id .'"><a href="' . $editlink . '" xhref="'
					. Core::formPageLink($db->f('link'), $db->f('address'), $lang).'"'
					. $classname . '>' . $title . '</a>';
					*/
					$html .= sprintf('<li id="page%s"><i class="ico %s"></i><a href="%s" xhref="%s" data-tpl-id="%s" data-id="%s">%s</a>',
					    $id, $classname, $editlink, Core::formPageLink($db->f('link'), $db->f('address'), $lang),
					    $db->Record['tpl_id'], $id, $title
					);
				}
			}
		}
		return $html ? $html : '&nbsp;';
	}


	/**
	 * �?зменить шаблон страницы
	 * @param int $page_id ID страницы
	 * @param int $tmpl_id ID шаблона
	 * @return boolean
	 */
	function change_page_tmpl($page_id, $tmpl_id)
	{   global $db;

		$page_id = (int) $page_id;
		$tmpl_id = (int) $tmpl_id;

		/* Получаем информацию о странице и её текущем шаблоне */
		$query = "SELECT P.id AS p_id, T.id AS t_id, T.main_module, number_of_fields";
		$query .= " FROM ".SITE_PREFIX."_pages_templates AS T, ".SITE_PREFIX."_pages AS P";
		$query .= " WHERE P.id='".$page_id."' AND tpl_id=T.id";
		$db->query($query);
		if ($db->nf() < 1) {
			return FALSE;
		}
		$db->next_record();
		$old_m_mod = $db->f('main_module');
		$old_num_f = $db->f('number_of_fields');

		if ($db->f('t_id') == $tmpl_id) {
			return TRUE;
		}

		/* Получаем информацию о новом шаблоне */
		$query = "SELECT id, main_module, number_of_fields";
		$query .= " FROM ".SITE_PREFIX."_pages_templates WHERE id='".$tmpl_id."'";
		$db->query($query);
		if ($db->nf() < 1) {
			return FALSE;
		}
		$db->next_record();
		$new_m_mod = $db->f('main_module');
		$new_num_f = $db->f('number_of_fields');

		/* Меняем шаблон у страницы */
		$query = "UPDATE ".SITE_PREFIX."_pages SET tpl_id='".$tmpl_id."', main_module='".$new_m_mod."'";
		$query .= " WHERE id='".$page_id."'";
		$db->query($query);

		$fields = array();
		/* Если в новом шаблоне нет главного модуля а в старом есть */
		if (0 == $new_m_mod && 0 != $old_m_mod) {
			$fields[] = 0;
		}

		if ($new_num_f < $old_num_f) {
			for ($i=$new_num_f; $i<$old_num_f; $i++) {
				$fields[] = $i+1;
			}
		}

		/**
		 * Если в новом шаблоне меньше полей чем в старом и/или
		 * новом шаблоне нет главного модуля а в старом есть
		 * удаляем лишние блоки
		 */
		if (count($fields)) {
			$query = "DELETE FROM ".SITE_PREFIX."_pages_blocks WHERE page_id='".$page_id."'";
		    $query .= " AND field_number IN ('".implode("', '", $fields)."')";
		    $db->query($query);
		}

		return TRUE;
	}

	/**
	 * Добавление шаблона.
	 *
	 * @param string  $title       Заголовок
	 * @param int     $fields_cnt  Кол-во полей
	 * @param boolean $main_module TRUE - �?меет поле главного модуля, FALSE - нет
	 * @return boolean
	 */
	function add_tmpl($title, $fields_cnt, $main_module)
	{   global $CONFIG, $db, $server, $lang;

		$title       = $db->escape(htmlspecialchars($title));
		$fields_cnt	 = (int) $fields_cnt;
		$main_module = $main_module ? 1 : 0;

		if ('' === $title
			|| $fields_cnt < 1
			|| !$this->is_writable_tpl_file_dir()
			|| !$this->is_writable_tpl_img_dir()
			|| !is_uploaded_file($_FILES['tmpl_img']['tmp_name'])
			|| 'image/gif' != $_FILES['tmpl_img']['type']
			|| !is_uploaded_file($_FILES['tmpl_file']['tmp_name'])) {
				print_r($_FILES); exit;
			return FALSE;
		}



		$query = "INSERT INTO ".$server.$lang."_pages_templates (tpl_name, main_module,number_of_fields )
					VALUES ('".$title."', '".$main_module."', '".$fields_cnt."')";
		$db->query($query);
		if ($db->affected_rows() > 0) {
			$tmpl_id = $db->lid();
			/* Закачиваем изображение шаблона */
			$tmpl_img = "tmpl".$tmpl_id.".gif";
			move_uploaded_file($_FILES['tmpl_img']['tmp_name'], RP.$this->tmpl_img_path.SITE_PREFIX."/".$tmpl_img);
			@chmod(RP.$this->tmpl_img_path.SITE_PREFIX."/".$tmpl_img, 0666);
			/* Закачиваем файл шаблона */
			$tmpl_file = "_index".$tmpl_id.".html";
			move_uploaded_file($_FILES['tmpl_file']['tmp_name'], RP.$this->tmpl_file_path.SITE_PREFIX."/".$tmpl_file);
			@chmod(RP.$this->tmpl_file_path.SITE_PREFIX."/".$tmpl_file, 0666);

			$query = "UPDATE ".$server.$lang."_pages_templates SET image_url='".$tmpl_img."' WHERE id='".$tmpl_id."'";
	        $db->query($query);

	        return TRUE;
		}

		return FALSE;
	}


	/**
	 * Удалить шаблон
	 * @param int $tmpl_id Id шаблона
	 * @return boolean FALSE - если существуют статьи с таким шаблоном или шаблона не существует,
	 *                 TRUE - в противном случае
	 */
	function del_tmpl($tmpl_id)
	{   global $db;

		$tmpl_id = (int) $tmpl_id;
		if ($this->exist_tmpl_pages($tmpl_id)) {
			return FALSE;
		}
		$db->query("DELETE FROM ".SITE_PREFIX."_pages_templates WHERE id='".$tmpl_id."'");
		if ($db->affected_rows() > 0) {
			/* Удалить файл шаблона и макет */
			unlink(RP.$this->tmpl_img_path.SITE_PREFIX."/tmpl".$tmpl_id.".gif");
			unlink(RP.$this->tmpl_file_path.SITE_PREFIX."/_index".$tmpl_id.".html");

			return TRUE;
		}

		return FALSE;
	}

	/**
	 * Есть ли права на запись в директорию с изображениями шаблонов
	 * @param void
	 * @return bolean
	 */
	function is_writable_tpl_img_dir()
	{
		return is_writable(RP.$this->tmpl_img_path.SITE_PREFIX);
	}

	/**
	 * Есть ли права на запись в директорию с файлами шаблонов
	 * @param void
	 * @return bolean
	 */
	function is_writable_tpl_file_dir()
	{
		return is_writable(RP.$this->tmpl_file_path.SITE_PREFIX);
	}

	/**
	 * Существуют ли страницы с указанным шаблоном
	 * @param int $tmpl_id Id шаблона
	 * @return boolean
	 */
	function exist_tmpl_pages($tmpl_id)
	{   global $db2;

		$tmpl_id = (int)$tmpl_id;
		$db2->query("SELECT id FROM ".SITE_PREFIX."_pages WHERE tpl_id='".$tmpl_id."' LIMIT 1");

		return $db2->nf() > 0 ? TRUE : FALSE;
	}

	function show_sites()
	{
		global $db, $tpl;

		$db->query("SELECT * FROM sites ORDER BY sitename_rus");
		while ($db->next_record()) {
			$tpl->newBlock('block_site');
			$tpl->assign(array(
			    'domain' => $db->f('title'),
			    'sitename_rus' => $db->f('sitename_rus'),
			    'sitename' => $db->f('sitename'),
			));
		}
	}
	
	function get_page_imgs($page_id)
	{   global $db, $CONFIG;
	
		$db->query("SELECT img1, img2, img3 FROM {$this->table_prefix}_pages WHERE id=".intval($page_id));
		$ret = false;
		if ($db->next_record()) {
		    $ret = array(
		        'img1' => $db->Record['img1'],
		        'path1' => $db->Record['img1'] && file_exists(RP.$CONFIG['pages_img_path'].$db->Record['img1'])
		            ? "/".$CONFIG['pages_img_path'].$db->Record['img1'] : '',
		        'img2' => $db->Record['img2'],
		        'path2' => $db->Record['img2'] && file_exists(RP.$CONFIG['pages_img_path'].$db->Record['img2'])
		            ? "/".$CONFIG['pages_img_path'].$db->Record['img2'] : '',
		        'img3' => $db->Record['img3'],
		        'path3' => $db->Record['img3'] && file_exists(RP.$CONFIG['pages_img_path'].$db->Record['img3'])
		            ? "/".$CONFIG['pages_img_path'].$db->Record['img3'] : ''		        
		    );
		}
		return $ret;
	}
	
	function update_page_images($page_id)
	{   global $db, $CONFIG;
	    $page_id = (int) $page_id;
	    $imgs = $this->get_page_imgs($page_id);
	    if ($imgs) {
	        if ($_REQUEST['delimg1'] && $imgs['img1']) {	            
	            if ($imgs['path1']) {
	                unlink(RP.$CONFIG['pages_img_path'].$imgs['img1']);
	            }
	            $imgs['img1'] = $imgs['path1'] = '';
	        }
	        if ($_REQUEST['delimg2'] && $imgs['img2']) {	            
	            if ($imgs['path2']) {
	                unlink(RP.$CONFIG['pages_img_path'].$imgs['img2']);
	            }
	            $imgs['img2'] = $imgs['path2'] = '';
	        }
	        if ($_REQUEST['delimg3'] && $imgs['img3']) {	            
	            if ($imgs['path3']) {
	                unlink(RP.$CONFIG['pages_img_path'].$imgs['img3']);
	            }
	            $imgs['img3'] = $imgs['path3'] = '';
	        }
	        
	        if ($_FILES['img1']['tmp_name'] && is_uploaded_file($_FILES['img1']['tmp_name'])) {
	            if (isset($CONFIG['pages_img_formats'][$_FILES['img1']['type']])) {
	                $ext = $CONFIG['pages_img_formats'][$_FILES['img1']['type']];
	                if (move_uploaded_file($_FILES['img1']['tmp_name'], RP.$CONFIG['pages_img_path']."{$page_id}-1.{$ext}")) {
	                    $imgs['img1'] = "{$page_id}-1.{$ext}";
	                }
	            }
	        }
	        if ($_FILES['img2']['tmp_name'] && is_uploaded_file($_FILES['img2']['tmp_name'])) {
	            if (isset($CONFIG['pages_img_formats'][$_FILES['img2']['type']])) {
	                $ext = $CONFIG['pages_img_formats'][$_FILES['img2']['type']];
	                if (move_uploaded_file($_FILES['img2']['tmp_name'], RP.$CONFIG['pages_img_path']."{$page_id}-2.{$ext}")) {
	                    $imgs['img2'] = "{$page_id}-2.{$ext}";
	                }
	            }
	        }
	        if ($_FILES['img3']['tmp_name'] && is_uploaded_file($_FILES['img3']['tmp_name'])) {
	            if (isset($CONFIG['pages_img_formats'][$_FILES['img3']['type']])) {
	                $ext = $CONFIG['pages_img_formats'][$_FILES['img3']['type']];
	                if (move_uploaded_file($_FILES['img3']['tmp_name'], RP.$CONFIG['pages_img_path']."{$page_id}-3.{$ext}")) {
	                    $imgs['img3'] = "{$page_id}-3.{$ext}";
	                }
	            }
	        }
	        $db->query("UPDATE {$this->table_prefix}_pages SET img1='{$imgs['img1']}',"
	            ." img2='{$imgs['img2']}', img3='{$imgs['img3']}' WHERE id={$page_id}");
	    }
	}
	
	function show_page_files($page_id)
	{   global $tpl;
	
		$l = $this->get_page_imgs($page_id);
		if ($l) {
		    if ($l['img1']) {
		        $tpl->newBlock('block_img1');
    		    $tpl->assign(array(
    		        'path' => $l['path1'],
    		        'img' => $l['img1'],
					"MSG_Delete" => $this->_msg["Delete"]
    		    ));
		    }
		    if ($l['img2']) {
		        $tpl->newBlock('block_img2');
    		    $tpl->assign(array(
    		        'path' => $l['path2'],
    		        'img' => $l['img2'],
					"MSG_Delete" => $this->_msg["Delete"]
    		    ));
		    }
		    if ($l['img3']) {
		        $tpl->newBlock('block_img3');
    		    $tpl->assign(array(
    		        'path' => $l['path3'],
    		        'img' => $l['img3'],
					"MSG_Delete" => $this->_msg["Delete"]
    		    ));
		    }
		}
	}
	
	function _getChilds($id)
	{   global $db;
	    
	    $id = array_map('intval', (array)$id);
	    $id = implode(',', $id);
	    $ret = false;
	    $db->query("SELECT id, parent_id, title, link, address FROM {$this->table_prefix}_pages WHERE parent_id IN ({$id})");
	    while ($db->next()) {
	    	$ret[$db->Record['id']] = $db->Record;
	    }

	    return $ret;
	}
	
	function getChilds($id)
	{	    
	    $ret = array();
	    while ($childs = $this->_getChilds($id) ) {
	    	$ret = $ret + $childs;
	    	$id = array_keys($childs);
	    }

	    return $ret;
	}
	
	function genereteSitemap($comand = '', $param = array())
	{   global $CONFIG, $lang;
	    
	    $error = false;
	    $end = false;
	    $ret = array();
	    
		$host = 'http://'.$_SERVER['HTTP_HOST'];
	    $file_name = $CONFIG['main_domen'] == $CONFIG['main_domen'] ? 'sitemap.xml' : $CONFIG['site_pref'].'_sitemap.xml';
	    $file_path = $CONFIG['pages_sitemap_path'].$file_name;
	    switch ($comand) {
	    	case 'start':
	    		$fp = fopen(RP.$file_path, 'w');
	    		if ($fp) {
	    		    $_SESSION['abo_sitemap_links_stak'] = array(array(
	    		        'url' => $host,
	    		        'level' => 0,
	    		        'repeat' => 0
	    		    ));
	    		    $_SESSION['abo_sitemap_links'] = array('/');
	    		    fwrite($fp, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
	    		} else {
	    		    $error = "Can not open sitemap file - '/{$CONFIG['pages_sitemap_path']}.{$file_name}'";
	    		    $end = true;
	    		}
	    		break;
	    		
	    	case 'end':
	    		$fp = fopen(RP.$file_path, 'a');
	    		if ($fp) {
	    		    fwrite($fp, "</urlset>");
	    		} else {
	    		    $error = "Can not open sitemap file - '/{$CONFIG['pages_sitemap_path']}.{$file_name}'";
	    		    $end = true;
	    		}
	    		break;
	    
	    	default:
	    	    $fp = fopen(RP.$file_path, 'a');
	    		if (!$fp) {
	    		    $error = "Can not open sitemap file - '/{$CONFIG['pages_sitemap_path']}.{$file_name}'";
	    		    $end = true;
	    		}
	    		break;
	    }
	    
	    include_once(RP.'inc/class.chttp.php');
	    
	    if (!$error && !empty($_SESSION['abo_sitemap_links_stak'])) {
	        $url = array_shift($_SESSION['abo_sitemap_links_stak']);
	    	$h = new chttp();
    		$h->support_cookies = 1;
    		$parse_url = parse_url($url['url']);
    		if (!$parse_url["path"]) {
    			$parse_url["path"] = "/";
    		}
    		if ($parse_url["query"]) {
    			$parse_url["path"].="?".$parse_url["query"];
    		}
    		$error = $h->Open(array("HostName" => $parse_url["host"]));
    		if ('' == $error) {
    			$error = $h->SendRequest(array( "RequestURI" => $parse_url["path"], "Headers" => array(
    			    "Host" => $parse_url["host"],
    			    "User-Agent" => "Mozilla/4.0 (compatible; MSIE 5.0; Windows 98; DigExt)",
    			    "Pragma" => "no-cache"
    			)));
    			if ('' == $error) {
    				$body = "";
    				$headers = array();
    				$error = $h->ReadReplyHeaders(&$headers);
    				if ('' == $error) {
        				reset($headers);
        				$p = 0;
        				while(!$error && list($k, $v) = each($headers)) {
        				    if (0 == $p++) {
        				        if (strstr($k, '404')) {
            						$error = "'{$url['url']}' 404 page not found";
            					}
        				    }    					
        					if ($k == 'content-type' && false === strpos($v, 'text/html')) {
        					    $error = "'{$url['url']}' is not HTML page";
        					}
            	        }
            	        while (! $error && $p ) {
            	            $p = '';
            	        	$error = $h->ReadReplyBody(&$p, 1024);
            	        	$body .= $p;
            	        }
            	        if ($body) {
            	            $links = LinksControl::get_internal_links($body);
            				if (! empty($links)) {
            				    $l = $url['level'] + 1;
            				    foreach ($links as $p) {
            				    	if (! in_array($p, $_SESSION['abo_sitemap_links'])) {
            				    	    $_SESSION['abo_sitemap_links'][] = $p;
            				    	    $_SESSION['abo_sitemap_links_stak'][] = array('url' => $host.$p, 'level' => $l);
            				    	}
            				    }
            				}
            				$p = $param['step'] > 0 ? floatval($param['step']) : $CONFIG['pages_prioritet_step'][$CONFIG['pages_prioritet_step_default_key']];
            				$p = 0 == $url['level'] ? 1 : 1 - ($p *  ($url['level']-1));
            				$p = $p > 0 && $p <= 1 ? $p : 0.1;
            				fwrite($fp, "<url>\n<loc>".htmlspecialchars($host.$parse_url["path"])."</loc>\n<priority>{$p}</priority>\n<lastmod>"
            				    .date('c')."</lastmod>\n</url>\n\n");
            	        }
    				}
    				
    			}
			}
			
			if ($error && !$url['repeat']) {
			    array_unshift($_SESSION['abo_sitemap_links_stak'], array(
	    		    'url' => $url['url'],
	    		    'level' => $url['level'],
	    		    'repeat' => 1
	    		));
			}
			
			$h->Close();
    	    if (empty($_SESSION['abo_sitemap_links_stak'])) {
    	        fwrite($fp, "</urlset>");
    	        $msg = "Файл создан - /{$CONFIG['pages_sitemap_path']}{$file_name}";
    	        $ret['path'] = '/'.$file_path;
				$ret['size'] = number_format(filesize(RP.$file_path)/1024, 2, '.', ' ').' Kb';
				$ret['date'] = date('Y-m-d H:i:s', filemtime(RP.$file_path));
    	    }
    	    fclose($fp);
	    }
	    
	    $ret['end'] = $end || empty($_SESSION['abo_sitemap_links_stak']);
	    $ret['link'] = $parse_url["path"];
	    $ret['error'] = $error ? $error : false;
	    $ret['msg'] = $msg ? $msg : false;
	    
	    return $ret;
	}
	
	function log($str)
	{   static $fp = false;
	    return;
	    if (false === $fp) {
	        $fp = fopen(RP.'log.txt', 'a');
	        if ($fp) {
	            fwrite($fp, "START ".date('H:i:s')."\n");
	        }
	    }
	    if ($fp) {
	        fwrite($fp, var_export($str, true)."\n");
	    }
	    if ('end' == $str && $fp) {
	        fclose($fp);
	        $fp = false;
	    }
	}
	
	function showFieldsList($filds_cnt, $has_main, $block_name = 'field_list_item')
	{   global $tpl;
	
	   if ($has_main) {
	       $tpl->newBlock($block_name);
	       $tpl->assign(array(
	           'key' => 0,
	           'title' => 'Поле главного модуля'
	       ));
	   }
	   for ($i = 1; $i <= $filds_cnt; $i++) {
	       $tpl->newBlock($block_name);
	       $tpl->assign(array(
	           'key' => $i,
	           'title' => 'Поле'
	       ));
	   }
	}
	/*
	function getModsMeta($page_ids = array())
	{   global $db, $CONFIG, $server;
	    
	    if (! empty($page_ids)) {
	        $page_ids = implode(',', array_map('intval', $page_ids));
	    }
    	$mods = $db->getArrayOfResult("SELECT M.*, GROUP_CONCAT(DISTINCT field_number ORDER BY field_number DESC SEPARATOR ',') AS fields"
    	    ." FROM sites S INNER JOIN sites_modules SM ON S.id=site_id "
    	    ." INNER JOIN modules M ON SM.module_id=M.id"
    	    ." INNER JOIN {$this->table_prefix}_pages_blocks BP ON BP.module_id=M.id"
    	    ." WHERE S.alias='".substr($server, 0, -1)."' AND !M.admin_only AND SM.active"
    	    .($page_ids ? " AND BP.page_id IN (".$page_ids.")" : '')
    	    ." GROUP BY M.id ORDER BY M.title"
    	);
    	$ret = false;
    	if ($mods) {
    	    foreach($mods as $row) {
    	        $mod_id = $row['id'];
    	        $ret[$mod_id] = array(
    	                'title' => $row['title'],
    	                'name' => $row['name'],
    	                'only_as_main' => $row['only_as_main'],
    	                'pages' => array(),
    	                'main_acts' => array(),
    	                'acts' => array(),
    	                'fields' => '' !== $row['fields'] ? explode(',', $row['fields']) : array() 
    	        );
    	        $mod = false;
    	        $mod = Module::load_mod($row['name'], 'only_create_object');
    	        if (is_array($mod->block_main_module_actions)) {
    	            foreach($mod->block_main_module_actions as $k => $v) {
    	                $ret[$mod_id]['main_acts'][$k] = $v;
    	            }
    	        }
    	        if (is_array($mod->block_module_actions)){
    	            foreach($mod->block_module_actions as $k => $v) {
    	                $ret[$mod_id]['acts'][$k] = $v;
    	            }
    	        }
    	    }
    	    $db->query("SELECT page_id, title, module_id FROM {$this->table_prefix}_pages_blocks B"
        	    ." INNER JOIN {$this->table_prefix}_pages P ON B.page_id=P.id"
        	    ." WHERE field_number=0"
    	    );
    	    while($db->next()) {
        	    if (isset($ret[$db->Record['module_id']])) {
            	    $ret[$db->Record['module_id']]['pages'][$db->Record['page_id']] = $db->Record['title'];
            	}
    	    }
    	}
    	return $ret;
	}
	*/
	
	function getModsMeta($page_ids = array())
	{
	    global $db, $CONFIG, $server;
	
	    if (! empty($page_ids)) {
	        $page_ids = implode(',', array_map('intval', $page_ids));
	    }
	    
	    $fields = array();
	    $db->query("SELECT DISTINCT field_number, module_id, action FROM {$this->table_prefix}_pages_blocks"
	        .($page_ids ? " WHERE page_id IN (".$page_ids.")" : '')." ORDER BY page_id, field_number"
	    );
	    while ($db->next()) {
	        $fields[$db->Record['module_id']][$db->Record['field_number']][] = $db->Record['action'];
	    }
	    
	    $mods = $db->getArrayOfResult("SELECT M.*"
	        ." FROM sites S INNER JOIN sites_modules SM ON S.id=site_id "
	        ." INNER JOIN modules M ON SM.module_id=M.id"
	        ." WHERE S.alias='".substr($server, 0, -1)."' AND !M.admin_only AND SM.active"
	        ." ORDER BY M.title"
	    );
	    $ret = false;
	    if ($mods) {
	        foreach($mods as $row) {
	            $mod_id = $row['id'];
	            if (!$page_ids || ($page_ids && $fields[$mod_id]) ) {
	                $ret[$mod_id] = array(
	                    'title' => $row['title'],
	                    'name' => $row['name'],
	                    'only_as_main' => $row['only_as_main'],
	                    'pages' => array(),
	                    'main_acts' => array(),
	                    'acts' => array(),
	                    'fields' => (array)$fields[$mod_id]
	                );
	                $mod = false;
	                $mod = Module::load_mod($row['name'], 'only_create_object');
	                if (is_array($mod->block_main_module_actions)) {
	                    foreach($mod->block_main_module_actions as $k => $v) {
	                        $ret[$mod_id]['main_acts'][$k] = $v;
	                    }
	                }
	                if (is_array($mod->block_module_actions)){
	                    foreach($mod->block_module_actions as $k => $v) {
	                        $ret[$mod_id]['acts'][$k] = $v;
	                    }
	                }
	            }	            
            }
        	$db->query("SELECT page_id, title, module_id FROM {$this->table_prefix}_pages_blocks B"
            	." INNER JOIN {$this->table_prefix}_pages P ON B.page_id=P.id"
            	." WHERE field_number=0"
        	);
        	while($db->next()) {
            	if (isset($ret[$db->Record['module_id']])) {
            	    $ret[$db->Record['module_id']]['pages'][$db->Record['page_id']] = $db->Record['title'];
            	}
        	}
	}
	return $ret;
	}
	
	function addBlockToPages($param)
	{   global $db;
	    
	    $page_ids = array_map('intval', explode(',', $param['ids']));
	    if (!empty($page_ids) && $param['act'] && $param['module'] && $param['act']) {
	        $field = (int)$param['field'];
	        $mod_id = (int)$param['module'];
	        $site_id = $_SESSION['session_cur_site_id'];
	        $lang_id = $_SESSION['session_cur_lang_id'];
	        $action = My_Sql::escape($param['act']);
	        $linked_page = '';
	        $linked_page_address = '';
	        $tpl = My_Sql::escape($param['tpl']);
	        $prop = json_decode($param['prop']);
	        $prop = is_object($prop) ? get_object_vars($prop) : $prop;
	        for ($i=1; $i<6; $i++) {
	            if ($prop[$i] && 'null'!=$prop[$i]) {
	                $prop[$i] = is_scalar($prop[$i]) ? mysql_real_escape_string($prop[$i])
	                    : mysql_real_escape_string(json_encode($prop[$i]));
	            } else {
	                $prop[$i] = '';
	            }
	        }
	        if ($this->getPage($param['linked_page'])) {
	            $db->next();
	            $linked_page = $db->Record['link'];
	            $linked_page_address = $db->Record['address'];
	        }
	        $max_ranks = array();
	        $db->query("SELECT page_id, MAX(block_rank) AS max_rank FROM {$this->table_prefix}_pages_blocks"
	            ." WHERE field_number={$field} AND page_id IN (".implode(',', $page_ids).") GROUP BY page_id");
	        while ($db->next()) {
	            $max_ranks[$db->Record['page_id']] = $db->Record['max_rank'];
	        }
	        foreach ($page_ids as $id) {
	            $rank = $max_ranks[$id] ? $max_ranks[$id]+1 : 1;
	            if (0 == $field && $rank > 1) {
	                continue;
	            }
	            $db->query("INSERT INTO {$this->table_prefix}_pages_blocks ("
	                ." page_id, field_number, module_id, site_id, lang_id,"
	                ." action, linked_page, linked_page_address, block_rank,"
	                ." property1, property2, property3, property4, property5, tpl"
	                .") VALUE ("
	                ."{$id}, {$field}, {$mod_id}, {$site_id}, {$lang_id},"
	                ." '{$action}', '{$linked_page}', '{$linked_page_address}', {$rank},"
	                ." '{$prop['1']}', '{$prop['2']}', '{$prop['3']}', '{$prop['4']}', '{$prop['5']}', '{$tpl}')"
	            );
	        }
	    }	    
	}
	
	function clearPagesBlock($pages_ids, $field, $param = array())
	{   global $db;
	
	    $page_ids = array_map('intval', explode(',', $pages_ids));
	    if (!empty($page_ids)) {
	        $clause = array(
	                0 => new DbExp('page_id IN ('.implode(',', $page_ids).')'),
	                'field_number' => (int)$field
	        );
	        if ($param['module'] >= 1) {
	            $clause['module_id'] = (int)$param['module'];
	        }
	        if ($param['linked_page']) {
	            $clause['linked_page'] = $param['linked_page'];
	        }
	        if ($param['act']) {
	            $clause['action'] = $param['act'];
	        }
	        $db->del("{$this->table_prefix}_pages_blocks", $clause);
	    }	    
	}
	
	function changeBlockOnPages($param)
	{   global $db;
	
	    $page_ids = array_map('intval', explode(',', $param['ids']));
	    if (!empty($page_ids) && $param['find_module'] && $param['act'] && $param['module']) {
	        $clause = array(
	            0 => new DbExp('page_id IN ('.implode(',', $page_ids).')'),
	            'field_number' => (int)$param['find_field'],
	            'module_id' => (int)$param['find_module'],
	        );
	        if ($param['find_act']) {
	            $clause['action'] = $param['find_act'];
	        }
	        $find_blocks = $db->fetchAll("{$this->table_prefix}_pages_blocks", array('clause' => $clause));
	        
	        if (! empty($find_blocks)) {
	            $field = (int)$param['find_field'];
	            $mod_id = (int)$param['module'];
	            $site_id = $_SESSION['session_cur_site_id'];
	            $lang_id = $_SESSION['session_cur_lang_id'];
	            $action = My_Sql::escape($param['act']);
	            
	            $tpl = My_Sql::escape($param['tpl']);
	            $prop = json_decode($param['prop']);
	            $prop = is_object($prop) ? get_object_vars($prop) : $prop;
	            for ($i=1; $i<6; $i++) {
	                if ($prop[$i]) {
	                    $prop[$i] = is_scalar($prop[$i]) ? mysql_real_escape_string($prop[$i])
	                    : mysql_real_escape_string(json_encode($prop[$i]));
	                } else {
	                    $prop[$i] = '';
	                }
	            }
	            $linked_page = '';
	            $linked_page_address = '';
	            if ($this->getPage($param['linked_page'])) {
	                $db->next();
	                $linked_page = $db->Record['link'];
	                $linked_page_address = $db->Record['address'];
	            }
	            
	            foreach ($find_blocks as $val) {
	                $db->query("UPDATE {$this->table_prefix}_pages_blocks SET "
    	                ." module_id={$mod_id}, action='{$action}',"
    	                ." linked_page='{$linked_page}', linked_page_address='{$linked_page_address}',"
    	                ." property1='{$prop[1]}', property2='{$prop[2]}', property3='{$prop[3]}',"
    	                ." property4='{$prop[4]}', property5='{$prop[5]}', tpl='{$tpl}'"
    	                ." WHERE id={$val['id']}"
	                );
	            }
	        }
	    }
	}
	
	function setPagesBlocks($param, $field)
	{   global $CONFIG, $db;
	    
	    $page_ids = $block_ids = $linked_pages = array();
	    foreach ($param as $page_id => $val) {
	        $rank = 0;
	        $page_ids[] = (int)$page_id;
	        if (! empty($val)) {	            
	            foreach ($val as $block_id => $v) {
	                if (! empty($v)) {
	                    $v = (array)$v;
	                    if (! isset($linked_pages[$v['linked_page']])) {
	                        $db->query('SELECT link, address FROM '.$this->table_prefix.'_pages WHERE link = "'.mysql_real_escape_string($v['linked_page']).'"');
	                        $db->next();
	                        $linked_pages[$v['linked_page']] = $db->Record['address'] ? $db->Record['address'] : '';
	                    }
	                    $block_ids[] = (int)$block_id;
	                    $set = array(
	                            'property1' => is_scalar($v['property1']) ? $v['property1']
	                            : json_encode($v['property1']),
	                            'property2' => is_scalar($v['property2']) ? $v['property2']
	                            : json_encode($v['property2']),
	                            'property3' => is_scalar($v['property3']) ? $v['property3']
	                            : json_encode($v['property3']),
	                            'property4' => is_scalar($v['property4']) ? $v['property4']
	                            : json_encode($v['property4']),
	                            'property5' => is_scalar($v['property5']) ? $v['property5']
	                            : json_encode($v['property5']),
	                            'action' => $v['action'],
	                            'site_id' => $v['site_id'],
	                            'lang_id' => $v['lang_id'],
	                            'linked_page' => $v['linked_page'],
	                            'linked_page_address' => $linked_pages[$v['linked_page']],
	                            'tpl' => $v['tpl'],
	                            'block_rank' => ++$rank
	                    );
	                    $db->set("{$this->table_prefix}_pages_blocks", $set, array('id' => $block_id));
	                    //echo $db->sql."<br>";
	                }	                
	            }
	        }	        
	    }
	    if (! empty($page_ids)) {
	        $db->query("DELETE FROM {$this->table_prefix}_pages_blocks WHERE field_number=".intval($field)
	            ." AND page_id IN (".implode(',', $page_ids).")"
	            .(!empty($block_ids) ? ' AND id NOT IN ('.implode(',', $block_ids).')' : '')
	        );
	    }
	}
}
?>
