var cur_block_id;
function showEditDiv(obj,module,action,item_id,adminurl) {
	var menu_div = obj.parentNode.parentNode.parentNode;
	setClass(menu_div,'hover',0);
	cur_block_id = menu_div.parentNode.parentNode.parentNode.parentNode.id;
	properties_div = document.getElementById("properties_div");
	var properties_div_shadow = document.getElementById("properties_div_shadow");
	var properties_div_bgr = document.getElementById("properties_div_bgr");
	properties_div.innerHTML = "<div align=center><h6 style='marginL: 0; padding-top: 30px; font-size: 100%;'>Loading...</h6></div>";
	if (isMSIE && notIE7)
	{
	    properties_div_shadow.style.height = "1px";
        properties_div_shadow.style.height = ieCanvas.scrollHeight + "px";
		properties_div.style.top = ieCanvas.scrollTop + Math.round(KL_getBody(window).clientHeight/2) + "px";
		properties_div_bgr.style.top = ieCanvas.scrollTop + Math.round(KL_getBody(window).clientHeight/2) + "px";
	}
	properties_div.className = "admin-properties-div-big";
	properties_div.style.display = "block";
	properties_div_bgr.style.display = "block";
	properties_div_shadow.style.display = "block";
	/* Create new JsHttpRequest object. */
	var req = new JsHttpRequest();
	/* Code automatically called on load finishing. */
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			/* Write result to page element. */
			//properties_div.innerHTML = req.responseText;

			switch (action) {
			case "add":
			case "edit":
				properties_div.innerHTML = "<br><h3>Редактирование статьи</h3>" + req.responseText;
				break;
			case "editrights":
				properties_div.innerHTML = "<br><h3>Редактирование прав доступа к статье</h3>" + req.responseText;
				var newscript = document.createElement("SCRIPT");
				var contaner = document.getElementById('updateForm');
				newscript.src = '/js/rightsDialog.js';
				newscript.type = "text/javascript";
				contaner.appendChild(newscript);
				// array initialisation
				newscript = document.createElement("SCRIPT");
				newscript.type = "text/javascript";
				newscript.text = req.responseJS.js_arrays;
				contaner.appendChild(newscript);
				break;
			}
			init_tinymce();
		}
	};
	/* Prepare request object (automatically choose GET or POST). */
	req.open("GET", adminurl+"&name="+module+"&type=JsHttpRequest&action="+action, true);
	/* Send data to backend. */
	if (action=="add") item_id = menu_div.parentNode.parentNode.id;
	req.send( { id: item_id } );
	return false;
}

function do_save(myForm) {
	var SendItem= 0;
	var AlertMessage;

	if (myForm.title.value == '') {
		SendItem= 1;
		AlertMessage = "Введите заголовок статьи";
	}
	if (!SendItem) {
		/* Create new JsHttpRequest object. */
		var req = new JsHttpRequest();
		/* Code automatically called on load finishing. */
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
				/* Write result to page element. */
				if (req.responseJS.res) {
					if (req.responseJS.replace_content) {
	                    var div = document.getElementById(cur_block_id);
						var prnt = div.parentNode;
						var button = prnt.removeChild(prnt.lastChild);
						prnt.removeChild(div);
						prnt.innerHTML = prnt.innerHTML + req.responseJS.content;
						if (req.responseJS.next_block > 0) {
							// insert before block with id = req.responseJS.next_block
							var next_div = document.getElementById('mod_block_'+req.responseJS.next_block);
							div = document.getElementById(cur_block_id);
							prnt.insertBefore(div,next_div);
						}
						/*var divs = document.getElementsByName("mod_article_"+req.responseJS.id);
						for(var i=0; i<divs.length; i++) divs[i].parentNode.nextSibling.innerHTML = myForm.txt_content.value;*/
						prnt.appendChild(button);
					}

					properties_div.style.display = "none";
					document.getElementById("properties_div_shadow").style.display = "none";
					document.getElementById("properties_div_bgr").style.display = "none";
					document.getElementById("properties_div_bgr_2").style.display = "none";
				}
			}
		};
		req.loader = 'FORM';
		/* Prepare request object (automatically choose GET or POST). */
		req.open("POST", myForm.action, true);
		/* Send data to backend. */
		req.send( { myForm: myForm, block_id: cur_block_id, cur_url: window.location.href } );
		//req.send( { title: myForm.title.value, txt_content: myForm.txt_content.value, block_id: cur_block_id, cur_url: window.location.href } );
	} else {
		alert(AlertMessage);
	}
	return false;
}

