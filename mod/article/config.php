<?php // Vers 5.8.2 27.06.2012
/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
			  'article_order_by'		=> array('type'		=> 'select',
												 'defval'	=> array('id DESC'	=> array(	'rus' => 'в порядке обратном добавлению',
												 											'eng' => 'reverce adding order'),
												 					 'id'		=> array(	'rus' => 'в порядке добавления',
												 											'eng' => 'adding order'),
												 					 'title'		=> array(	'rus' => 'По алфавиту А-Я',
												 											'eng' => 'A-Z'),
												 					 'title DESC'		=> array(	'rus' => 'По алфавиту Я-А',
												 											'eng' => 'Z-A'),
												 					 ),
												 'descr'	=> array(	'rus' => 'Порядок вывода записей',
												 						'eng' => 'Show records order'),
												 'access'	=> 'editable',
												 ),

			   'article_max_rows'		=> array('type'		=> 'integer',
			   									 'defval'	=> '20',
			   									 'descr'	=> array(	'rus' => 'Максимальное кол-во записей на страницу по умолчанию',
												 						'eng' => 'Default maximum quantity of records on page'),
			   									 'access'	=> 'editable'
			   									 ),

			   'article_tag_separator' 	=> array('type'		=> 'string',
			   									 'defval'	=> 'h2',
			   									 'descr'	=> array(	'rus' => 'Тэг для постраничной разбивки статьи',
												 						'eng' => 'Tag for paginal breakdown of clause'),
			   									 'access'	=> 'editable'
			   									 ),

			   'article_count_hits' 	=> array('type'		=> 'checkbox',
			   									 'defval'	=> 'on',
			   									 'descr'	=> array(	'rus' => 'Учитывать статистику отображения контента',
												 						'eng' => 'To consider statistics of display of a content'),
			   									 'access'	=> 'editable'
			   									 ),

			   'article_default_rights'	=> array('type'		=> 'string',
			   									 'defval'	=> '111111110001',
			   									 'descr'	=> array(	'rus' => 'Права для статьи по-умолчанию (формат <img src="/i/admin/rights_format.gif" width=72 height=18 alt="формат прав">, где x - 0 или 1, каждая группа представляет набор прав: удаление, публикация, редактирование, чтение)',
 												 						'eng' => 'Article default rights (format <img src="/i/admin/rights_format.gif" width=72 height=18 alt="rights format">, where x - 0 or 1, every group is rights set: delete, publication, edit, read)'),
			   									 'access'	=> 'editable'
			   									 ),

			   'article_comment_cnt'		=> array('type'		=> 'integer',
			   									 'defval'	=> '20',
			   									 'descr'	=> array(	'rus' => 'Кол-во комментариев на страницу',
												 						'eng' => 'Comments count on page'),
			   									 'access'	=> 'editable'
			   									 ),
			   );

$OPEN_NEW = array();

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'article_order_by' => 'title',
'article_max_rows' => '30',
'article_tag_separator' => 'opo',
'article_count_hits' => '',
'article_default_rights' => '111111110001',
'article_comment_cnt' => '5',
/* ABOCMS:END */
);
?>