<?php
$MOD_TABLES[] = "CREATE TABLE `".$prefix."_articles` (
  `id` int unsigned NOT NULL auto_increment,
  `ctgr_id` int unsigned NOT NULL default 0,
  `title` varchar(255) NOT NULL default '',
  `text` LONGTEXT,
  `active` tinyint(3) unsigned NOT NULL default '0',
  `hits` bigint(20) unsigned NOT NULL default '0',
  `commentable` tinyint(1) unsigned NOT NULL default 0,
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY (`ctgr_id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_articles_ctgr` (
  `id` int unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";
?>