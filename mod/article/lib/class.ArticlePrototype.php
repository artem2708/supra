<?php
require_once(RP.'mod/article/lib/class.articleDB.php');

class ArticlePrototype extends Module {

	var $main_title;
	var $table_prefix;
	var $addition_to_path;
	var $module_name				= 'article';
	var $default_action				= 'showarticle';
	var $admin_default_action		= 'showlist';
	var $tpl_path					= 'mod/article/tpl/';
	var $_msg						= array();
	var $block_module_actions		= array();
	var $block_main_module_actions	= array();
	var $action_tpl = array(
	    'showarticle' => 'article.html'
	);

	function ArticlePrototype($action = 'only_create_object', $transurl = '', $properties = array(), $prefix = NULL, $rewrite_mod_params = NULL, $adminurl = NULL, $noadmin = false)
	{    global $main, $core, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang, $request_start, $permissions, $request_type, $_RESULT;
	
		/* Get $action, $tpl_name, $permissions */
		extract( $this->init(array(
		    'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
		    'action' => $action, 'tpl_name' => $properties['tpl']
		)), EXTR_OVERWRITE);
		
		$id = (int)$properties[1];
		$show_pages = (int)$properties[2];

		if ('only_create_object' == $action) {
		    return;
		}
		
		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}
		
		if (! self::is_admin()) {
		  
//---------------------------------- обработка действий с сайта --------------------------------//
			switch ($action) {
// Вывод статьи
			case 'showarticle':
				$main->include_main_blocks($tpl_name, 'main');
                $tpl->prepare();
				$art_data = $this->getArticle($id, $CONFIG['edit_on_site'] && $_SESSION["session_is_admin"] && $_COOKIE['edit_content']);
				if ($art_data[0]["description"]) {
					$PAGE['description'] = strip_tags($art_data[0]["description"]);
				}
				if ($art_data[0]["keywords"]) {
					$PAGE['keywords'] = strip_tags($art_data[0]["keywords"]);
				}

				if (file_exists(RP."mod/comments")) {
				    require_once(RP."mod/comments/lib/class.CommentsPrototype.php");
				    $comm = new CommentsPrototype('only_create_object');
				    $comm->showArticleComments($id, $CONFIG['article_comment_cnt']);
				}
                if($id == 278){
					$s = file_get_contents(RP.'/y.txt');
					$ret = unserialize($s);
					foreach($ret as $k => $v){
						if(is_array($v)) $art_data[0]["text"] = str_replace('#'.$k.'_list#', implode(', ',$v), $art_data[0]["text"]);
						else $art_data[0]["text"] = str_replace('#'.$k.'#', $v, $art_data[0]["text"]);
					}
					
				} 				
				if ($show_pages == 1) {					
					
					$start_patt = "~<".preg_quote($CONFIG["article_tag_separator"], '~')."[^>]*>~si";
					$end_patt = "~</".preg_quote($CONFIG["article_tag_separator"], '~').">~si";
					$art_pages = preg_split($start_patt, $art_data[0]["text"]);

					$pages = sizeof($art_pages);
					$start = $request_start > 1 ? (int)$request_start : 1;
					$start = ($start > $pages) ? $pages : $start;
					
					if ($pages == 1) {
						// нет такого тэга
						$tpl->assign_array('block_article', $art_data);
					} else {
						if (sizeof($art_pages) >= 2) {
							$tpl->newBlock('block_pages');
							for ($i=0; $i < sizeof($art_pages); $i++) {
								$tpl->newBlock('block_page_row');
								$tpl->newBlock('block_page_title'.(1+$i==$start ? '_current' : ''));
								$l = preg_split($end_patt, $art_pages[$i]);
								$tpl->assign(array(
								    "page_link"	=> $transurl.($i==0 ? "" : "&start=".($i+1)),
								    "page_title"=> count($l) > 1 ? $l[0] : $art_data[0]["title"],
								    "i"=> $i+1
								));
							}
						}
						
						$tpl->newBlock('block_pages_article');
						$l = preg_split($end_patt, $art_pages[$start-1]);
						$tpl->assign(array(
							'id'	=> $art_data[0]["id"],
							'title'	=> count($l) > 1 ? $l[0] : $art_data[0]["title"],
							'text'	=> count($l) > 1 ? $l[1] : $art_pages[$start-1]
						));
						if ($pages > 1) {
							$nav_string = $main->_show_nav_block($pages, "nav", "", $start);
						}
					}
				} else {
					$tpl->assign_array('block_article', $art_data);
				}

				if ($CONFIG['edit_on_site'] && $_SESSION["session_is_admin"] && $_COOKIE['edit_content']) {
					$tmp_arr = array(
						"article_id"	=> $art_data[0]["id"],
						"adminurl"		=> "/admin.php?lang={$lang}",
					);
					$tpl->newBlock('block_edit_article_menu_start');
					$tpl->assign("article_id", $art_data[0]["id"]);
					if (!empty($art_data)) $tpl->newBlock('block_article_edit_start');
					if ($permissions['e'] && empty($art_data)) {
						$tpl->newBlock('block_article_add');
						$tpl->assign("adminurl", "/admin.php?lang={$lang}");
					}
					if ($permissions['e'] && $art_data[0]["rights"]["e"]) {
						$tpl->newBlock('block_article_edit');
						$tpl->assign($tmp_arr);
					}
					if ($permissions['p'] && $art_data[0]["rights"]["p"]) {
						$tpl->newBlock('block_article_activate');
						$tpl->assign($tmp_arr);
						$tpl->assign(array(
							"activate_display"	=> $art_data[0]["active"] ? 'none' : 'inline',
							"suspend_display"	=> $art_data[0]["active"] ? 'inline' : 'none',
						));
					}
					if ($art_data[0]["is_owner"]) {
						$tpl->newBlock('block_article_rights');
						$tpl->assign($tmp_arr);
					}
					if ($permissions['d'] && $art_data[0]["rights"]["d"]) {
						$tpl->newBlock('block_article_delete');
						$tpl->assign($tmp_arr);
					}
					if (!empty($art_data)) $tpl->newBlock('block_article_edit_end');
					if (empty($art_data)) $tpl->newBlock('block_empty_content');
					$tpl->newBlock('block_edit_article_menu_end');
				} else {
					Main::addHits($this->table_prefix."_articles", $id);
				}
				break;

// Default...
			default:
				if (is_object($tpl)) $tpl->prepare();
				return;
			}
//------------------------------- конец обработки действий с сайта -----------------------------//
		} else {
//------------------------------ обработка действий из админ части -----------------------------//
            // действия, которые пользователь имеет возможность задать для блока с данным модулем
            $this->block_module_actions			= array('showarticle'	=> $this->_msg["Show_article"]);
            $this->block_main_module_actions	= array('showarticle'	=> $this->_msg["Show_article"]);
            
			if (!isset($tpl)) {
			    $main->message_die('Не подключен класс для работы с шаблонами');
			}

			switch ($action) {
		    case 'prp':
		    	if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step;
				$main->include_main_blocks('_properties.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Information"	=> $main->_msg["Information"],
					"MSG_Value"			=> $main->_msg["Value"],
					"MSG_Save"			=> $main->_msg["Save"],
					"MSG_Cancel"		=> $main->_msg["Cancel"],
				));
		        if (!$request_step || $request_step == 1) {
					$tpl->assign(array('form_action'	=> $baseurl,
									   'step'			=> 2,
									   'lang'			=> $lang,
									   'name'			=> $this->module_name,
									   'action'			=> 'prp'));
					$prp_html = $main->getModuleProperties($this->module_name);
		            $this->main_title = $this->_msg["Controls"];
				} elseif (2 == $request_step) {
		        	$main->setModuleProperties($this->module_name, $_POST);
					header('Location: '.$baseurl.'&action=prp&step=1');
				}
		    	break;

		    case 'block_prp':
				global $request_sub_action, $request_block_id;
		    	$arr	= $this->getEditLink($request_sub_action, $request_block_id);
		    	$prop_html = $this->getPropertyFields();
		    	$js = '';
		    	if (preg_match_all("/<script[^>]*>(.*)<\/script>/iuU", $prop_html, $matches)) {
		    	    foreach ($matches[1] as $key => $val) {
		    	    	$js .= $val."\n";
		    	    }
		    	    $prop_html = preg_replace("/<script[^>]*>.*<\/script>/iuU", '', $prop_html);
		    	}
		    	$cont = 
'var container = document.getElementById(\'target_span\');
container.innerHTML = "'.$prop_html.'";
material_id = "'.$arr['material_id'].'";
material_url = "'.$arr['material_url'].'";
changeAction();
setURL("property1");
'.str_replace(array('\\\\', '\"'), array('\\', '"'), $js);
		    	header('Content-Length: '.strlen($cont));
		    	echo $cont;
		    	exit;
		    	break;

			// Редактирование свойств блока
			case 'properties':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);

				global $request_block_id, $request_field, $title;
				$main->include_main_blocks('_block_properties.html', 'main');
				$tpl->prepare();
				$main->show_actions($request_field, '', $this->block_main_module_actions, $this->block_module_actions);

				$tpl->assign(array(
				    '_ROOT.title'		=> $title,
					'_ROOT.username'	=> $_SESSION['session_login'],
					'_ROOT.password'	=> $_SESSION['session_password'],
					'_ROOT.name'		=> $this->module_name,
					'_ROOT.lang'		=> $lang,
					'_ROOT.block_id'	=> $request_block_id,
				));
				$this->main_title = $this->_msg["Block_properties"];
				break;

			
			case 'cas':
			    $id = (int)$_GET['id'];
				$support = (int)$_GET['support'] ? 0 : 1;
				$query = 'UPDATE '.$server.$lang.'_articles SET support='.$support.' WHERE id='.$id;
				if(!$result = mysql_query($query)) die(mysql_error());
				header('Location: '.$_SERVER['HTTP_REFERER']);
				exit;
			break;
			case 'showlist1':
                global $db;
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				$start = ($request_start) ? (int)$request_start : 1;
				$main->include_main_blocks_2($this->module_name."_list1.html", $this->tpl_path);
                $tpl->prepare();
				//$tpl->assignInclude("search_form", $tpl_path."_search_form.html");				


				$tpl->assign(
					array(
						"_ROOT.form_search_action"	=> $baseurl."&action=search",
						"_ROOT.lang"				=> $lang,
						"_ROOT.name"				=> $this->module_name,
						"_ROOT.baseurl"				=> $baseurl,
						"_ROOT.action"				=> "search",
						"MSG_Activ"					=> $this->_msg["Activ"],
						"MSG_Ed"					=> $this->_msg["Ed"],
						"MSG_Rights"				=> $main->_msg["Rights"],
						"MSG_Title"					=> $this->_msg["Title"],
						"MSG_Shows"					=> $this->_msg["Shows"],
						"MSG_Del"					=> $this->_msg["Del"],
						"MSG_Select_action"			=> $this->_msg["Select_action"],
						"MSG_Activate"				=> $this->_msg["Activate"],
						"MSG_Suspend"				=> $this->_msg["Suspend"],
						"MSG_Delete"				=> $this->_msg["Delete"],
						"MSG_Execute"				=> $this->_msg["Execute"],
						"MSG_Status"				=> $this->_msg["Status"],
						"MSG_Save"				    => $this->_msg["Create"],
						"MSG_Add_ctgr"				=> $this->_msg["Add_ctgr"],
						"MSG_Ctgr_title"			=> $this->_msg["Ctgr_title"],
						"MSG_Ctgrs"       			=> $this->_msg["Ctgrs"],
						"MSG_Ctgr"       			=> $this->_msg["Ctgr"],
						"MSG_Cancel"       			=> $this->_msg["Cancel"],
						"MSG_Art_list"       			=> $this->_msg["Art_list"],
				));
                //$tpl->assignInclude("ctgr_set", $this->tpl_path.$this->module_name."_ctgr_set.html");
                $tpl->newBlock('AddCtg');
                $arr=$db->getArrayOfResult("Select * from sup_rus_country ");
                if(!empty($arr))foreach($arr as $val){
                    $tpl->newBlock('ctgr_country');
                    $tpl->assign(array('id'=>$val['id'],'title'=>$val['title'],'select'=>''));
                }
				if ($_REQUEST['ctgr_id'] && ($ctgr = $this->getCtgr($_REQUEST['ctgr_id'])) ) {
				    $tpl->newBlock('ctgr');
				    $tpl->assign(array(
				        'baseurl' => $baseurl,
				        'title' => $ctgr['title'],
				        'MSG_article' => $this->_msg['article'],
						"MSG_Save" => $this->_msg["Create"],
						"MSG_Add_ctgr" => $this->_msg["Add_ctgr"],
						"MSG_Ctgr_title" => $this->_msg["Ctgr_title"],
						"MSG_Ctgrs" => $this->_msg["Ctgrs"],
						"MSG_Ctgr" => $this->_msg["Ctgr"],
						"MSG_Cancel" => $this->_msg["Cancel"],
				    ));
				} else {
				    $tpl->newBlock('categoties');
				    $tpl->assign(array(
				        'baseurl' => $baseurl,
    				    'action' => $action,
				        'MSG_article' => $this->_msg['article'],
						"MSG_Save" => $this->_msg["Create"],
						"MSG_Add_ctgr" => $this->_msg["Add_ctgr"],
						"MSG_Ctgr_title" => $this->_msg["Ctgr_title"],
						"MSG_Ctgrs" => $this->_msg["Ctgrs"],
						"MSG_Ctgr" => $this->_msg["Ctgr"],
						"MSG_Cancel" => $this->_msg["Cancel"],
				    ));
    				$tpl->assignList('ctgr_', $this->getCtgrList(1), array(
    				    'baseurl' => $baseurl,
    				    'action' => $action,
    				    'MSG_Ctgrs' => $this->_msg['Ctgrs'],
    				    'MSG_Title' => $this->_msg['Title'],
    				    'MSG_Ed' => $this->_msg['Ed'],
    				    'MSG_Del' => $this->_msg['Del'],
    				    'MSG_Edit' => $this->_msg['Edit'],
    				    'MSG_Delete' => $this->_msg['Delete'],
						"MSG_Save" => $this->_msg["Create"],
						"MSG_Add_ctgr" => $this->_msg["Add_ctgr"],
						"MSG_Ctgr_title" => $this->_msg["Ctgr_title"],
						"MSG_Ctgrs" => $this->_msg["Ctgrs"],
						"MSG_Ctgr" => $this->_msg["Ctgr"],
						"MSG_Cancel" => $this->_msg["Cancel"],
    				));
    				$tpl->newBlock('ctgr_form');
				    $tpl->assign(array(
				        'baseurl' => $baseurl,
    				    'action' => $action,
				        'MSG_article' => $this->_msg['article'],
						"MSG_Save" => $this->_msg["Create"],
						"MSG_Add_ctgr" => $this->_msg["Add_ctgr"],
						"MSG_Ctgr_title" => $this->_msg["Ctgr_title"],
						"MSG_Ctgrs" => $this->_msg["Ctgrs"],
						"MSG_Ctgr" => $this->_msg["Ctgr"],
						"MSG_Cancel" => $this->_msg["Cancel"],
				    ));
				}
				if((int)$_REQUEST['ctgr_id']){
                list($articles_list, $pages_cnt) = $this->getArticlesList(
				    'pages', $start, null, null, null, (int)$_REQUEST['ctgr_id']);
				if ($articles_list) {
				    $artcl_pages = $this->getArticlesPages(array_keys($articles_list));
				    foreach ($articles_list as $key => $val) {
					$tpl->newBlock('block_pages_list');
					$val['link'] = $baseurl.'&action=cas&id='.$val['article_id'].'&support='.$val['support'];
					$val['img'] = $val['support'] ? 'b_select.gif' : 'b_select_off.gif';
					$tpl->assign($val);
    					if ($val["rights"]["d"] || $val["rights"]["p"]) {
    						$tpl->newBlock('block_check');
    						$tpl->assign("article_id", $val["article_id"]);
    					}
    					if (!isset($artcl_pages[$key])) {
    					    $tpl->newBlock('block_not_uses');
    						$tpl->assign("MSG_Not_uses", $this->_msg["Not_uses"]);
    					} else if (1 == count($artcl_pages[$key])) {
    					    $tpl->newBlock('block_one');
    						$tpl->assign(array(
    						    "edit_link" => $baseurl."&name=pages&action=edit&id=".$artcl_pages[$key][0]['id'],
    						    "title" => htmlentities($artcl_pages[$key][0]['title'], ENT_QUOTES, 'UTF-8'),
    						    "address" => $artcl_pages[$key][0]['address'],
    						));
    					} else if (isset($artcl_pages)) {
    					    $tpl->newBlock('block_more');
    						$tpl->assign(array(
    						    'MSG_Of_pages' => $this->_msg["Of_pages"],
    						    'MSG_Hide' => $this->_msg["Hide"],
    						    'page_cnt' => count($artcl_pages[$key])
    						));
    						$tpl->newBlock('block_list');
    						foreach ($artcl_pages[$key] as $v) {
    							$tpl->newBlock('block_item');
        						$tpl->assign(array(
        						    "edit_link" => $baseurl."&name=pages&action=edit&id=".$v['id'],
        						    "title" => htmlentities($v['title'], ENT_QUOTES, 'UTF-8'),
        						    "address" => $v['address'],
        						));
    						}
    					}
    				}
    				Module::show_select('block_ctgr_list', 0, 'title', $this->getCtgrList());
				}				
				$main->_show_nav_block($pages_cnt, null, 'action='.$action.($ctgr ? '&ctgr_id='.$ctgr['id'] : ''), $start);
				$tpl->assign(Array(
					"MSG_Pages" => $this->_msg["Pages"],
				)); 
                }
				
				
				$this->main_title = $this->_msg["Articles_list1"];
				break;

                case 'showlist2':
                    global $db;
                    if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
                    $start = ($request_start) ? (int)$request_start : 1;
                    $main->include_main_blocks_2($this->module_name."_list2.html", $this->tpl_path);
                    $tpl->prepare();
                    //$tpl->assignInclude("search_form", $tpl_path."_search_form.html");

                    //$tpl->assignInclude("ctgr_set", $this->tpl_path.$this->module_name."_ctgr_set.html");
                    $tpl->newBlock('AddCtg');
                        $tpl->newBlock('categoties');
                        $tpl->assign(array(
                            'baseurl' => $baseurl,
                            'action' => $action,
                            'MSG_article' => $this->_msg['article'],
                            "MSG_Save" => $this->_msg["Create"],
                            "MSG_Add_ctgr" => $this->_msg["Add_ctgr"],
                            "MSG_Ctgr_title" => $this->_msg["Ctgr_title"],
                            "MSG_Ctgrs" => $this->_msg["Ctgrs"],
                            "MSG_Ctgr" => $this->_msg["Ctgr"],
                            "MSG_Cancel" => $this->_msg["Cancel"],
                        ));
                        $tpl->assignList('ctgr_', $this->getCtgrListCountry(1), array(
                            'baseurl' => $baseurl,
                            'action' => $action,
                            'MSG_Ctgrs' => $this->_msg['Ctgrs'],
                            'MSG_Title' => $this->_msg['Title'],
                            'MSG_Ed' => $this->_msg['Ed'],
                            'MSG_Del' => $this->_msg['Del'],
                            'MSG_Edit' => $this->_msg['Edit'],
                            'MSG_Delete' => $this->_msg['Delete'],
                            "MSG_Save" => $this->_msg["Create"],
                            "MSG_Add_ctgr" => $this->_msg["Add_ctgr"],
                            "MSG_Ctgr_title" => $this->_msg["Ctgr_title"],
                            "MSG_Ctgrs" => $this->_msg["Ctgrs"],
                            "MSG_Ctgr" => $this->_msg["Ctgr"],
                            "MSG_Cancel" => $this->_msg["Cancel"],
                        ));


                    $this->main_title = $this->_msg["Articles_list2"];
                    break;
			case 'showlist':
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				$start = ($request_start) ? (int)$request_start : 1;
				$main->include_main_blocks_2($this->module_name."_list.html", $this->tpl_path);
				$tpl->assignInclude("search_form", $tpl_path."_search_form.html");				
				$tpl->assignInclude("ctgr_set", $this->tpl_path.$this->module_name."_ctgr_set.html");
				$tpl->prepare();
				$tpl->assign(
					array(
						"_ROOT.form_search_action"	=> $baseurl."&action=search",
						"_ROOT.lang"				=> $lang,
						"_ROOT.name"				=> $this->module_name,
						"_ROOT.baseurl"				=> $baseurl,
						"_ROOT.action"				=> "search",
						"MSG_Activ"					=> $this->_msg["Activ"],
						"MSG_Ed"					=> $this->_msg["Ed"],
						"MSG_Rights"				=> $main->_msg["Rights"],
						"MSG_Title"					=> $this->_msg["Title"],
						"MSG_Shows"					=> $this->_msg["Shows"],
						"MSG_Del"					=> $this->_msg["Del"],
						"MSG_Select_action"			=> $this->_msg["Select_action"],
						"MSG_Activate"				=> $this->_msg["Activate"],
						"MSG_Suspend"				=> $this->_msg["Suspend"],
						"MSG_Delete"				=> $this->_msg["Delete"],
						"MSG_Execute"				=> $this->_msg["Execute"],
						"MSG_Status"				=> $this->_msg["Status"],
						"MSG_Save"				    => $this->_msg["Create"],
						"MSG_Add_ctgr"				=> $this->_msg["Add_ctgr"],
						"MSG_Ctgr_title"			=> $this->_msg["Ctgr_title"],
						"MSG_Ctgrs"       			=> $this->_msg["Ctgrs"],
						"MSG_Ctgr"       			=> $this->_msg["Ctgr"],
						"MSG_Cancel"       			=> $this->_msg["Cancel"],
						"MSG_Art_list"       			=> $this->_msg["Art_list"],
				));
				
					
				list($articles_list, $pages_cnt) = $this->getArticlesList(
				    'pages', $start, null, null, null, (int)$_REQUEST['ctgr_id']);
				if ($articles_list) {
				    $artcl_pages = $this->getArticlesPages(array_keys($articles_list));
				    foreach ($articles_list as $key => $val) {
					$tpl->newBlock('block_pages_list');
					$tpl->assign($val);
    					if ($val["rights"]["d"] || $val["rights"]["p"]) {
    						$tpl->newBlock('block_check');
    						$tpl->assign("article_id", $val["article_id"]);
    					}
    					if (!isset($artcl_pages[$key])) {
    					    $tpl->newBlock('block_not_uses');
    						$tpl->assign("MSG_Not_uses", $this->_msg["Not_uses"]);
    					} else if (1 == count($artcl_pages[$key])) {
    					    $tpl->newBlock('block_one');
    						$tpl->assign(array(
    						    "edit_link" => $baseurl."&name=pages&action=edit&id=".$artcl_pages[$key][0]['id'],
    						    "title" => htmlentities($artcl_pages[$key][0]['title'], ENT_QUOTES, 'UTF-8'),
    						    "address" => $artcl_pages[$key][0]['address'],
    						));
    					} else if (isset($artcl_pages)) {
    					    $tpl->newBlock('block_more');
    						$tpl->assign(array(
    						    'MSG_Of_pages' => $this->_msg["Of_pages"],
    						    'MSG_Hide' => $this->_msg["Hide"],
    						    'page_cnt' => count($artcl_pages[$key])
    						));
    						$tpl->newBlock('block_list');
    						foreach ($artcl_pages[$key] as $v) {
    							$tpl->newBlock('block_item');
        						$tpl->assign(array(
        						    "edit_link" => $baseurl."&name=pages&action=edit&id=".$v['id'],
        						    "title" => htmlentities($v['title'], ENT_QUOTES, 'UTF-8'),
        						    "address" => $v['address'],
        						));
    						}
    					}
    				}
    				Module::show_select('block_ctgr_list', 0, 'title', $this->getCtgrList());
				}				
				$main->_show_nav_block($pages_cnt, null, 'action='.$action.($ctgr ? '&ctgr_id='.$ctgr['id'] : ''), $start);
				$tpl->assign(Array(
					"MSG_Pages" => $this->_msg["Pages"],
				));
				$this->main_title = $this->_msg["Articles_list"];
				break;

		// Вывод результата поиска статьи
			case 'search':
				if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
				global $request_search;
				$search = strip_tags(trim($request_search));
				$main->include_main_blocks_2($this->module_name.'_list.html', $this->tpl_path);
				$tpl->assignInclude('search_form', $tpl_path.'_search_form.html');
				$tpl->prepare();

				$tpl->assign(Array(
					"MSG_Activ" => $this->_msg["Activ"],
					"MSG_Rights" => $main->_msg["Rights"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Title" => $this->_msg["Title"],
					"MSG_Shows" => $this->_msg["Shows"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Select_action" => $this->_msg["Select_action"],
					"MSG_Activate" => $this->_msg["Activate"],
					"MSG_Suspend" => $this->_msg["Suspend"],
					"MSG_Delete" => $this->_msg["Delete"],
					"MSG_Execute" => $this->_msg["Execute"],
					"MSG_To_comment" => $this->_msg["To_comment"],
					"MSG_Status"				=> $this->_msg["Status"],
				));

				$articles_list = $this->getArticlesList('search', '', '', '', $search);
				if ($articles_list) {
				    $artcl_pages = $this->getArticlesPages(array_keys($articles_list));
				    foreach ($articles_list as $key => $val) {
					$tpl->newBlock('block_pages_list');
					$tpl->assign($val);
    					if ($val["rights"]["d"] || $val["rights"]["p"]) {
    						$tpl->newBlock('block_check');
    						$tpl->assign("article_id", $val["article_id"]);
    					}
    					if ($artcl_pages && !isset($artcl_pages[$key])) {
    					    $tpl->newBlock('block_not_uses');
    						$tpl->assign("MSG_Not_uses", $this->_msg["Not_uses"]);
    					} else if (1 == count($artcl_pages[$key])) {
    					    $tpl->newBlock('block_one');
    						$tpl->assign(array(
    						    "edit_link" => $baseurl."&name=pages&action=edit&id=".$artcl_pages[$key][0]['id'],
    						    "title" => htmlentities($artcl_pages[$key][0]['title'], ENT_QUOTES, 'UTF-8'),
    						    "address" => $artcl_pages[$key][0]['address'],
    						));
    					} else {
    					    $tpl->newBlock('block_more');
    						$tpl->assign(array(
    						    'MSG_Of_pages' => $this->_msg["Of_pages"],
    						    'MSG_Hide' => $this->_msg["Hide"],
    						    'page_cnt' => count($artcl_pages[$key])
    						));
    						$tpl->newBlock('block_list');
    						foreach ($artcl_pages[$key] as $v) {
    							$tpl->newBlock('block_item');
        						$tpl->assign(array(
        						    "edit_link" => $baseurl."&name=pages&action=edit&id=".$v['id'],
        						    "title" => htmlentities($v['title'], ENT_QUOTES, 'UTF-8'),
        						    "address" => $v['address'],
        						));
    						}
    					}
    				}
    				Module::show_select('block_ctgr_list', 0, 'title', $this->getCtgrList());   
				}

		//		if (!$result) $no_res = "ничего не найдено";

				$tpl->assign(array(	'_ROOT.form_search_action'	=> $baseurl.'&action=search',
									'_ROOT.lang'				=> $lang,
									'_ROOT.name'				=> $this->module_name,
									'_ROOT.action'				=> 'search',
									'_ROOT.search'				=> $search,
									'_ROOT.search_result'		=> $this->_msg["Search_results"] . ' "'.$search.'": '.$no_res,
								));
				$this->main_title = $this->_msg["Article_search_results"];
				break;

		// Добавление новой статьи в базу данных
			case "add":
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step, $request_txt_content, $request_title, $request_block_id, 
				    $request_tryon, $request_ctgr_id;
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name."_edit.html", $this->tpl_path, $request_type == "JsHttpRequest" ? 'main' : '');
					$tpl->prepare();

					$tpl->assign(
						array(
							'html_editor'		=>	$htmlEditor,
							"_ROOT.form_action"	=>	"http://".$_SERVER["HTTP_HOST"]."/".$baseurl.'&action=add&step=2'
													.($request_type == "JsHttpRequest" ? '&type=JsHttpRequest' : ''),
							"_ROOT.cancel_link"	=>	"http://".$_SERVER["HTTP_HOST"]."/".$baseurl.'&action=showlist',
						)
					);
					$tpl->assign(Array(
						"MSG_Enter_title" => $this->_msg["Enter_title"],
						"MSG_Title" => $this->_msg["Title"],
						"MSG_required_fields" => $this->_msg["required_fields"],
						"MSG_Save" => $this->_msg["Save"],
						"MSG_Try_on" => $this->_msg["Try_on"],
						"MSG_Cancel" => $this->_msg["Cancel"],
						"MSG_Ctgr" => $this->_msg["Ctgr"],
						"MSG_Select" => $this->_msg["Select"],
					));
					
					if (file_exists(RP."mod/comments")) {
    				    require_once(RP."mod/comments/lib/class.CommentsPrototype.php");
    				    $comm = new CommentsPrototype('only_create_object');
    				    $comm->showCommentableForm();
    				}
    				
    				Module::show_select('block_ctgr_list', 0, 'title', $this->getCtgrList());
    				
					$this->main_title = $this->_msg["Add_article"];					
				} else if ($request_step == 2) {
					//$typograph = new Typograph();
					//$request_txt_content = $typograph->typograph($request_txt_content);
					$block_id = intval(substr($request_block_id, 10));
					$lid = $this->add_article($request_title, $request_txt_content, $request_ctgr_id, $block_id);
					if ($lid) {
						@$cache->delete_cache_files();
						$lc->add_internal_links($this->module_name, $this->table_prefix.'_articles', 'text', $lid, $this->_msg["article_link"], $request_txt_content);
					}
					if ($request_tryon) {
						header('Location: '.$baseurl.'&action=edit&id='.$lid);
					} else if ($request_type == "JsHttpRequest") {
						$_RESULT = array(
						    "res" => $lid,
						    "replace_content" => false
						);
					} else {
						$art = $this->getArticle($lid);
						if($art[0]['ctgr_id']) header('Location: '.$baseurl.'&action=showlist1'.($request_ctgr_id ? '&ctgr_id='.intval($request_ctgr_id) : ''));
						else header('Location: '.$baseurl.'&action=showlist'.($request_ctgr_id ? '&ctgr_id='.intval($request_ctgr_id) : ''));
					}
					exit;
				}
				break;

		// Вывод на экран и редактирование статьи
			case "edit":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_id;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_articles", $request_id))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name."_edit.html", $this->tpl_path, $request_type == "JsHttpRequest" ? 'main' : '');
				$tpl->prepare();

				$art = $this->show_article($request_id);
				$tpl->assign(
					array(
						'_ROOT.form_action'	=>	"http://".$_SERVER["HTTP_HOST"]."/".$baseurl.'&action=update&id='.$request_id.'&start='.$request_start
												.($request_type == "JsHttpRequest" ? '&type=JsHttpRequest' : ''),
						'_ROOT.cancel_link'	=>	"http://".$_SERVER["HTTP_HOST"]."/".$baseurl.'&action=showlist&start='.$request_start,
					)
				);
				$tpl->assign(Array(
					"MSG_Enter_title" => $this->_msg["Enter_title"],
					"MSG_Title" => $this->_msg["Title"],
					"MSG_required_fields" => $this->_msg["required_fields"],
					"MSG_Save" => $this->_msg["Save"],
					"MSG_Cancel" => $this->_msg["Cancel"],
					"MSG_Try_on" => $this->_msg["Try_on"],
					"MSG_Ctgr" => $this->_msg["Ctgr"],
					"MSG_Select" => $this->_msg["Select"],
				));

				Module::show_select('block_ctgr_list', $art['ctgr_id'], 'title', $this->getCtgrList());			
				
				if (file_exists(RP."mod/comments")) {
    				require_once(RP."mod/comments/lib/class.CommentsPrototype.php");
    				$comm = new CommentsPrototype('only_create_object');
    				$comm->showCommentableForm($art['commentable']);
    			}
				$this->main_title = $this->_msg["Edit_article"];
				break;

		// Обновление статьи в БД
			case 'update':
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_title, $request_txt_content, $request_block_id, 
				    $request_tryon, $request_ctgr_id;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_articles", $request_id))
					$main->message_access_denied($this->module_name, $action);

				//$typograph = new Typograph();
				//$request_txt_content = $typograph->typograph($request_txt_content);
				$res = $this->update_article($request_title, $request_txt_content, $request_ctgr_id, $request_id);
				if ($res) {
					@$cache->delete_cache_files();
					$lc->add_internal_links($this->module_name,
											$this->table_prefix.'_articles',
											'text',
											$request_id,
											$this->_msg["article_link"],
											$request_txt_content);
				}
				if ($request_type == "JsHttpRequest") {
					list($block_content, $next_block_id) = $core->get_block_content_with_menu(intval(substr($request_block_id, 10)));
					$_RESULT = array(
									"res"		=> $res,
									"id"		=> $request_id,
									"content"	=> $block_content,
									"next_block"=> $next_block_id,
									"replace_content"	=> true,
								);
				} else {
				  if ($request_tryon) {
					    header('Location: '.$_SERVER['HTTP_REFERER']);
					    exit;
					}
					if($request_ctgr_id) header('Location: '.$baseurl.'&action=showlist1&start='.$request_start.($request_ctgr_id ? '&ctgr_id='.intval($request_ctgr_id) : ''));
					else header('Location: '.$baseurl.'&action=showlist&start='.$request_start.($request_ctgr_id ? '&ctgr_id='.intval($request_ctgr_id) : ''));
				}
				exit;
				break;

		// Удаляем статью из БД
			case 'del':
				if (!$permissions['d']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_block_id;
				if (!$this->test_item_rights("d", "id", $this->table_prefix."_articles", $request_id))
					$main->message_access_denied($this->module_name, $action);
$art = $this->getArticle($request_id);
				$res = $this->delete_article($request_id);
				if ($res) {
					@$cache->delete_cache_files();
					$lc->delete_internal_links($this->module_name, $this->table_prefix.'_articles', $request_id);
				}
				if ($request_type == "JsHttpRequest") {
					list($block_content, $next_block_id) = $core->get_block_content_with_menu(intval(substr($request_block_id, 10)));
					$_RESULT = array(
									"res"		=> $res,
									"content"	=> $block_content,
									"cur_block"	=> $request_block_id,
									"next_block"=> $next_block_id,
								);
				} else {
					
					//var_dump($art);exit;
					if($art[0]['ctgr_id']) header('Location: '.$baseurl.'&action=showlist1&start='.$request_start);
					else header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				}
				exit;
				break;

		// Отключение/Включение публикации
			case "suspend":
			case "activate":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				if (!$this->test_item_rights("p", "id", $this->table_prefix."_articles", $request_id))
					$main->message_access_denied($this->module_name, $action);

				$res = ($action == "suspend")
						? $main->suspend($this->table_prefix.'_articles', $request_id)
						: $main->activate($this->table_prefix.'_articles', $request_id);
				if ($res) @$cache->delete_cache_files();

				if ($request_type == "JsHttpRequest") {
					$_RESULT = array("res"	=> $res);
				} else {
					Module::go_back();
				}
				exit;
				break;


		//	Удалить выбранные статьи
			case 'delete_checked':
				if (!$permissions['d']) $main->message_access_denied($this->module_name, $action);
				global $request_check;
				if (is_array($request_check)) {
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("d", "id", $this->table_prefix."_articles", $v)) {
							if ($this->delete_article($v)) {
								@$cache->delete_cache_files();
								$lc->delete_internal_links($this->module_name, $this->table_prefix.'_articles', $v);
							}
						}
					}
				}
				header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				exit;
				break;

		//	Отключить публикацию выбранных статей
			case "suspend_checked":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_check;
				if (is_array($request_check)) {
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $this->table_prefix."_articles", $v)) {
							if ($main->suspend($this->table_prefix.'_articles', $v)) {
								@$cache->delete_cache_files();
							}
						}
					}
				}
				header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				exit;
				break;

		// Включить публикацию выбранных статей
			case 'activate_checked':
				if (!$permissions['p']) $main->message_access_denied($this->module_name, $action);
				global $request_check;
				if (is_array($request_check)) {
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $this->table_prefix."_articles", $v)) {
							if ($main->activate($this->table_prefix.'_articles', $v)) {
								@$cache->delete_cache_files();
							}
						}
					}
				}
				header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				exit;
				break;


		// Редактирование прав статьи
			case 'editrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_articles", $request_id, true))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($this->table_prefix."_articles", "title", $request_id,
											"&id=".$request_id."&start=".$request_start, $this->_msg["Article"])) {
					header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				}
				break;

		// Сохранение прав статьи
			case 'saverights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start, $request_rights;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_articles", $request_id, true))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($this->table_prefix."_articles", $request_id, $request_rights);
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
					    "res" => 1,
					    "id" => $request_id,
					);
				} else {
					header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				}
				exit;
				break;

			// Переключить комментирование
			case "changecommentable":
				if (!$permissions["e"]) {
					$main->message_access_denied($this->module_name, $action);
				}
				global $request_id, $request_start;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_articles", $request_id))
					$main->message_access_denied($this->module_name, $action);

				$c = $this->getCommentable($request_id);
				$res = false;
				if (false !== $c) {
					$c = $c ? false : true;
					if ($this->setCommentable($request_id, $c)) {
						$res = array('id' => $request_id, 'commentable' => $c);
					}
				}

				if ($request_type == "JsHttpRequest") {
					$_RESULT = $res;
				} else {
					Module::go_back();
					header('Location: '.$baseurl.'&action=showlist&start='.$request_start);
				}
				exit;
				break;
				
			case 'ctgr_set':
				if (!$permissions['e']) {
				    $main->message_access_denied($this->module_name, $action);
				}
				global $db;
				if ($_POST['title']) {
				    if ($_POST['id']) {
				        $db->set(
				            "{$this->table_prefix}_articles_ctgr", 
				            array('title' => $_POST['title'],'country_id'=>$_POST['country']),
				            array('id' => $_POST['id'])
				        );
				    } else {
				        $db->set(
				            "{$this->table_prefix}_articles_ctgr", 
				            array('title' => $_POST['title'],'country_id'=>$_POST['country'])
				        );
				    }
				    Module::go_back($baseurl.'&action=showlist');
				    exit;
				}
				if ($_REQUEST['id'] && ($ctgr = $this->getCtgr($_REQUEST['id']))) {
				    $main->include_main_blocks_2("{$this->module_name}_{$action}.html", $this->tpl_path);
				    $tpl->prepare();
				    
				    $tpl->assign($ctgr + array(
						"lang" => $lang,
						"name" => $this->module_name,
						"baseurl"	 => $baseurl,
						"action" => $action,
						"MSG_Save"				    => $this->_msg["Create"],
						"MSG_Add_ctgr"				=> $this->_msg["Add_ctgr"],
						"MSG_Ctgr_title"			=> $this->_msg["Ctgr_title"],
						"MSG_Ctgrs"       			=> $this->_msg["Ctgrs"],
						"MSG_Ctgr"       			=> $this->_msg["Ctgr"],
						"MSG_Cancel"       			=> $this->_msg["Cancel"],
					));

                   $arr=$db->getArrayOfResult("Select * from sup_rus_country ");
                    if(!empty($arr))foreach($arr as $val){
                        if($val['id']==$ctgr['country_id'])$selected='selected';
                        else $selected='';
                        $tpl->newBlock('ctgr_country');
                        $tpl->assign(array('id'=>$val['id'],'title'=>$val['title'],'select'=>$selected));
                    }
				}
				$this->main_title = $this->_msg["Edit_ctgr"];
				break;

                case 'ctgr_set_country':
                    if (!$permissions['e']) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $db;
                    if ($_POST['title']) {
                        if ($_POST['id']) {
                            $db->set(
                                "{$this->table_prefix}_country",
                                array('title' => $_POST['title']),
                                array('id' => $_POST['id'])
                            );
                        } else {
                            $db->set(
                                "{$this->table_prefix}_country",
                                array('title' => $_POST['title'])
                            );
                        }
                        Module::go_back($baseurl.'&action=showlist2');
                        exit;
                    }
                    if ($_REQUEST['id'] && ($ctgr = $this->getCtgrCountry($_REQUEST['id']))) {
                        $main->include_main_blocks_2("{$this->module_name}_{$action}.html", $this->tpl_path);
                        $tpl->prepare();

                        $tpl->assign($ctgr + array(
                                "lang" => $lang,
                                "name" => $this->module_name,
                                "baseurl"	 => $baseurl,
                                "action" => $action,
                                "MSG_Save"				    => $this->_msg["Create"],
                                "MSG_Add_ctgr"				=> $this->_msg["Add_ctgr"],
                                "MSG_Ctgr_title"			=> $this->_msg["Ctgr_title"],
                                "MSG_Ctgrs"       			=> $this->_msg["Ctgrs"],
                                "MSG_Ctgr"       			=> $this->_msg["Ctgr"],
                                "MSG_Cancel"       			=> $this->_msg["Cancel"],
                            ));
                    }
                    break;
			case 'ctgr_del':
				if (!$permissions['d']) {
				    $main->message_access_denied($this->module_name, $action);
				}
				$this->delCtgr($_REQUEST['id']);
				Module::go_back($baseurl.'&action=showlist');
				break;
            case 'ctgr_del_country':
                    if (!$permissions['d']) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    $this->delCtgrCountry($_REQUEST['id']);
                    Module::go_back($baseurl.'&action=showlist2');
                    break;
			case 'changectgr':
				if (!$permissions['e']) {
				    $main->message_access_denied($this->module_name, $action);
				}				
				if ('none' == $_REQUEST['new_ctgr'] || ( $_REQUEST['new_ctgr'] && ($ctgr = $this->getCtgr($_REQUEST['new_ctgr']))
					&& !empty($_REQUEST['check']) )
				) {
					global $db;
					$id = 'none' == $_REQUEST['new_ctgr'] ? 0 : $ctgr['id'];
					$ids = array_map('intval', $_REQUEST['check']);
					$db->set("{$this->table_prefix}_articles", array('ctgr_id' => $id),
						array(new DbExp('id IN ('.implode(',', $ids).')'))
					);
				}
				Module::go_back($baseurl.'&action=showlist'.($ctgr ? '&ctgr_id='.$ctgr['id'] : ''));
				exit;
				break;

			default:
				$main->include_main_blocks();
				$tpl->prepare();
			}
//--------------------------- конец обработки действий из админ части --------------------------//
		}
	}

	// получение статьи по id
	function getArticle($id = NULL, $adm = true) {
		GLOBAL $db, $CONFIG;
		$id	= (int)$id;
		if ($id) {
			$this->get_list_with_rights("a.*", $this->table_prefix."_articles a", "a",
										'id = '.$id.(($adm) ? '' : ' AND active=1'));
			if ($db->nf() > 0) {
				$db->next_record();
//				$this->main_title	= $db->f('title');
				$rights = $db->f("user_rights");
				if ($adm) {
					$permissions = $_SESSION["permissions"][$this->table_prefix][$this->module_name];
					$rights = array("d" => (($rights & 8) >> 3) && $permissions["d"],
									"p" => (($rights & 4) >> 2) && $permissions["p"],
									"e" => (($rights & 2) >> 1) && $permissions["e"],
									"r" => ($rights & 1) && $permissions["r"],
								);
				} else {
					$rights = array("d" => 0, "p" => 0, "e" => 0, "r" => $rights & 1);
				}
				$arr[]	= array('id'			=> $db->f('id'),
								'title' 		=> $db->f('title'),
								'ctgr_id' 		=> $db->f('ctgr_id'),
								'text'			=> $db->f('text'),
								'description'	=> $db->f('description'),
								'keywords'		=> $db->f('keywords'),
								'active'		=> $db->f('active'),
								'commentable'	=> $db->f('commentable'),
								'is_owner'		=> $db->f('is_owner'),
								'rights'		=> $rights,
							);
				return $arr;
			}
		}
		return FALSE;
	}

	function getPropertyFields() {
		GLOBAL $request_site_target, $request_sub_action, $request_block_id;
		if ('' != $request_sub_action) {

			$tpl = $this->getTmpProperties();

			switch ($request_sub_action) {
				case 'showarticle':
					$tpl->newBlock('block_properties');
					$arr = $this->getArtListByCtgr();
					abo_str_array_crop($arr);
					$tpl->assign(Array(
						"MSG_Article" => $this->_msg["Article"],
						"MSG_Ctgrs" => $this->_msg["Ctgrs"],
						"MSG_Select" => $this->_msg["Select"],
						"json_ctgr_art_list" => json_encode($arr),
					));				
					if ($arr) {
					    foreach ($arr as $val) {
					    	$tpl->newBlock('block_ctgr_list');
        					$tpl->assign(array(
        					   'id' => $val['id'],
        					   'title' => $val['title'],
        					));
        					if (! empty($val['art_list'])) {
        					    foreach ($val['art_list'] as $k => $v) {
        					    	$tpl->newBlock('block_art_list');
                					$tpl->assign(array(
                					   'id' => $k,
                					   'title' => $v,
                					));
        					    }
        					}
					    }
					}
					$tpl->newBlock('block_page_edit');
					$tpl->assign(Array(
						"MSG_go_edit" => $this->_msg["go_edit"],
						"site_target" => $request_site_target,
					));
					$tpl->newBlock('block_paging_property');
					$tpl->assign(Array(
						"MSG_Pager" => $this->_msg["Pager"],
						"MSG_no" => $this->_msg["no"],
						"MSG_yes" => $this->_msg["yes"],
					));
					break;

				default:
					$tpl->newBlock('block_properties_none');
					$tpl->assign(Array(
						"MSG_No" => $this->_msg["No"],
					));
					break;
			}
			return $this->getOutputContent($tpl->getOutputContent());
		}
		return FALSE;
	}

	function getEditLink($sub_action = NULL, $block_id = NULL) {
		GLOBAL $request_name, $db, $lang;
		$block_id	= (int)$block_id;
		if ($sub_action && $block_id) {
			$db->query('SELECT	property1,
								property2,
								property3,
								property4,
								property5
							FROM '.$this->table_prefix.'_pages_blocks
							WHERE id = '.$block_id);
			if ($db->nf() > 0) {
				$db->next_record();
				$arr[]	= $db->f('property1');
				$arr[]	= $db->f('property2');
				$arr[]	= $db->f('property3');
				$arr[]	= $db->f('property4');
				$arr[]	= $db->f('property5');

				switch ($sub_action) {
					case 'showarticle':
						$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=edit&menu=false&id=';
						$link['material_id']	= $arr[0];
						break;
				}
				return $link;
			}

			switch ($sub_action) {
				case 'showarticle':
					$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=edit&menu=false&id=';
					$link['material_id']	= '';
					break;
			}
			return $link;
		}
		return FALSE;
	}

	function getArticlesList($type = 'all', $start = NULL, $rows = NULL, $order_by = NULL,
	   $search = NULL, $ctgr_id = NULL
	) {
		GLOBAL $CONFIG, $db, $baseurl, $server, $lang, $permissions;

		$pages_cnt = -1;
		if (!$order_by) $order_by = $CONFIG['article_order_by'];

		if (0 === $ctgr_id) {
		    $clause = ' AND ctgr_id=0';
		} else if ($ctgr_id > 0) {
		    $clause = ' AND ctgr_id='.intval($ctgr_id);
		}
		
		if ($type == 'active') {
			$this->get_list_with_rights("a.*", $this->table_prefix."_articles a", "a", "active = 1".$clause, "", $order_by);
		} else if ($type == 'pages') {
			$start = (int)$start;
			$start = $start>1 ? $start : 1;
			if (is_null($rows)) {
			    $rows = $CONFIG['article_max_rows'];
			} else {
			    $rows = (int) $rows;
			}
			$total_cnt = $this->get_list_with_rights(
									"a.*", $this->table_prefix."_articles a", "a",
									"1 {$clause}", "", $order_by,
									$start, $rows,
									true);
			$pages_cnt = ceil($total_cnt/$rows);
		} else if ($type == 'search') {
			$search	= $db->escape($search);
			$this->get_list_with_rights("a.*", $this->table_prefix."_articles a", "a",
									'(title LIKE "%'.$search.'%" OR
									text LIKE "%'.$search.'%" OR
									id = "'.$search.'")'.$clause,
									"a.id", $order_by);
		} else {
			$this->get_list_with_rights("a.*", $this->table_prefix."_articles a", "a", "1 {$clause}", "", $order_by);
		}

		if ($db->nf() > 0) {
			$actions = array(	"edit"		=> "edit",
								"editrights"=> "editrights",
								"activate"	=>	array("activate","suspend"),
								"delete"	=> "del",
								"confirm_delete"	=> $this->_msg["Confirm_delete"]
							);
			while($db->next_record()) {
				$id		= $db->f('id');
				$is_owner = $db->f("is_owner");
				$rights = $db->f("user_rights");
				$adm_actions = $this->get_actions_by_rights($baseurl."&id=".$id."&start=".$start, $db->f("active"), $db->f("is_owner"), $db->f("user_rights"), $actions);
				$arr[$id]	= array('article_text'			=>	$db->f('text'),
								'article_title'			=>	$db->f('title'),
								'support'			=>	$db->f('support'),
								'article_hits'			=>	$db->f('hits'),
								'article_edit_action'	=>	$adm_actions["edit_action"],
								'article_edit_rights_action'	=>	$adm_actions["edit_rights_action"],
								'article_del_action'	=>	$adm_actions["delete_action"],
								'change_status_action'	=>	$adm_actions["change_status_action"],
								'article_id'			=>	$id,
								'rights'				=>	$adm_actions["rights"],
								);
			}
			return $pages_cnt == -1 ? $arr : array($arr, $pages_cnt);
		}
		return FALSE;
	}

	// функция для вывода статьи по id
	function show_article($id = null) {
		global $tpl;
		
		$art = $this->getArticle($id);
		if ($art[0]) {
		    $tpl->assign($art[0]);
		}
		
		return $art ? $art[0] : false;
	}


	// функция для добавления статьи в базу данных
	function add_article($title, $text, $ctgr_id, $block_id = 0)
	{
		global $db, $CONFIG;
		if (!$title) return FALSE;

		$owner_id = $_SESSION["siteuser"]["id"];
		list($group_id, $rights) = $this->get_group_and_rights('article_default_rights', $block_id);

		$db->query("INSERT INTO ".$this->table_prefix."_articles
					SET ctgr_id = ".intval($ctgr_id).",
						title = '".$db->escape($title)."',
						text = '".$db->escape($text)."',
						owner_id = ".$owner_id.",
						usr_group_id = ".$group_id.",
						rights = ".$rights.",
						commentable = ".intval($_REQUEST['commentable']));
		if (!$db->affected_rows()) {
			return FALSE;
		}
		return $db->lid();
	}


	// функция для обновления статьи
	function update_article($title, $text, $ctgr_id, $id)
	{
		global $db;
    	$id = (int)$id;
		if ($id < 1) return FALSE;
		$db->query("UPDATE ".$this->table_prefix."_articles SET ctgr_id = ".intval($ctgr_id)
		    .", title = '".$db->escape($title)."', text = '".$db->escape($text)."', commentable = "
		    .intval($_REQUEST['commentable'])." WHERE id=".$id);
		return TRUE;
	}


	// функция для удаления статьи из базы данных
	function delete_article($id) {
		global $CONFIG;
		$id = (int)$id;
		if (!$id) return FALSE;
		$AD	= new CArticleData_DB($this->table_prefix.'_articles', 'id');
		if (!$AD->delete($id)) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	function getCommentableItem($id)
	{   global $db;

		$db->query("SELECT id, title, commentable FROM ".$this->table_prefix."_articles WHERE id=".intval($id)." AND commentable>0");
		return !$db->next_record() || (!$_SESSION['siteuser']['id'] && 2 == $db->Record['commentable']) 
		    ? false : array(
			'id' => $db->Record['id'],
			'title' => $db->Record['title']
		);
	}

	function setCommentable($id, $commentable)
	{   global $db;
		 
		$db->query("UPDATE ".$this->table_prefix."_articles SET commentable="
			.($commentable ? 1 : 0)." WHERE id=".intval($id));

		return $db->affected_rows() ? true : false;
	}

	function getCommentable($id)
	{   global $db;
		 
		$db->query("SELECT commentable FROM ".$this->table_prefix."_articles WHERE id=".intval($id));

		return $db->next_record() ? $db->Record['commentable'] : false;
	}
	
	function getArticlesPages($ids)
	{   global $db;
		$ids = implode(",", array_map('intval', (array) $ids));
		$ret = false;
		$db->query("SELECT DISTINCT P.id, P.title, P.address, property1 FROM {$this->table_prefix}_pages_blocks AS B"
		    ." INNER JOIN {$this->table_prefix}_pages AS P ON P.id=B.page_id WHERE"
		    ." B.action='showarticle' AND property1 IN ({$ids})");
		while ($db->next_record()) {
		    $ret[$db->Record['property1']][] = array(
		        'id' => $db->Record['id'],
		        'title' => $db->Record['title'],
		        'address' => $db->Record['address']
		    );
		}
		return $ret;
	}
	
	function getCtgrList()
	{   global $db;
	
		return $db->fetchAll("{$this->table_prefix}_articles_ctgr", false, 'id');
	}
    function getCtgrCountry($id)
    {   global $db;

        return $db->fetchOne(
            "{$this->table_prefix}_country",
            array('clause' => array('id' => $id))
        );
    }
    function getCtgrListCountry()
    {   global $db;

        return $db->fetchAll("{$this->table_prefix}_country", false, 'id');
    }
	function getCtgr($id)
	{   global $db;
	
		return $db->fetchOne(
		    "{$this->table_prefix}_articles_ctgr",
		    array('clause' => array('id' => $id))
		);
	}
	
	function delCtgr($id)
	{   global $db;
	
		$db->del(
		    "{$this->table_prefix}_articles_ctgr",
		    array('id' => $id)
		);
		$db->del(
		    "{$this->table_prefix}_articles",
		    array('ctgr_id' => $id)
		);
	}
    function delCtgrCountry($id)
    {   global $db;

        $db->del(
            "{$this->table_prefix}_country",
            array('id' => $id)
        );
        $db->del(
            "{$this->table_prefix}_articles",
            array('ctgr_id' => $id)
        );
    }
	function getArtListByCtgr()
	{   global $db, $CONFIG;
	
		$ret = $this->getCtgrList();
		$ret[0] = array(
		    'id' => 0,
		    'title' => 'Без категорий',
		    'art_list' => array()
		);
		$db->query("SELECT * FROM {$this->table_prefix}_articles ORDER BY ".$CONFIG['article_order_by']);
		while ($db->next()) {
			if ($db->Record['ctgr_id'] && $ret[$db->Record['ctgr_id']]) {
				$ret[$db->Record['ctgr_id']]['art_list'][$db->Record['id']] = $db->Record['title'];
			} else {
			    $ret[0]['art_list'][$db->Record['id']] = $db->Record['title'];
			}
		}
		
		return $ret;
	}
}
?>