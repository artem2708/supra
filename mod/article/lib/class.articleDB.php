<?php

class CArticleData_DB extends TObject {
	var $id					= NULL;
	var $title				= NULL;
	var $text				= NULL;
	var $active				= NULL;
	var $commentable		= NULL;

	function CArticleData_DB ($table, $PK) {
		$this->TObject($table, $PK);
	}

	function load ($id) {
		global $CONFIG, $admin;
		$id = (int) $id;
		$active = $admin ? "AND active = 1" : "";
		$sql = "SELECT * FROM $this->_tbl WHERE $this->_tbl_key = '$id' $active";
		return db_loadObject($sql, $this);
	}

	function list_by_type($type, $order_by, $start_row, $rows, $search) {
	    $order_by = My_Sql::escape($order_by);
	    $start_row = $start_row >= 1 ? (int)$start_row : 0;
	    $rows = $rows > 1 ? (int)$rows : 1;
		if ($type == "active") {
			$sql = "SELECT * FROM $this->_tbl WHERE active = 1 ORDER BY $order_by";
		} else if ($type == "pages") {
			$sql = "SELECT * FROM $this->_tbl ORDER BY $order_by LIMIT $start_row,$rows";
		} else if ($type == "search") {
		    $search = str_replace(array("_", "%"), array("\_", "\%"), My_Sql::escape($search));
			$sql = "SELECT * FROM $this->_tbl WHERE title LIKE '%$search%' OR text LIKE '%$search%' OR id = '$search' GROUP BY id ORDER BY $order_by";
		} else {
			$sql = "SELECT * FROM $this->_tbl ORDER BY $order_by";
		}
		return db_loadList($sql);
	}
}

?>