<?php

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_poll_other_results` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `poll_id` smallint(6) default NULL,
  `variant_text` varchar(255) default NULL,
  `host` varchar(255) default NULL,
  `siteusername` varchar(15) default NULL,
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_poll_results` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `poll_id` smallint(6) default NULL,
  `variant_id` smallint(6) default NULL,
  `host` varchar(255) default NULL,
  `siteusername` varchar(15) default NULL,
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_poll_variants` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `variant` varchar(255) default NULL,
  `v_rank` smallint(5) default NULL,
  `poll_id` smallint(5) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_polls` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `question` text,
  `begin_date` date default NULL,
  `end_date` date default NULL,
  `other_variant` tinyint(1) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  `hits` bigint(20) unsigned NOT NULL default '0',
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

?>