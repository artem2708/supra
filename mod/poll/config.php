<?php // Vers 5.8.2 27.06.2012

/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
			   'poll_max_rows'					=> array('type'		=> 'integer',
			   									 		 'defval'	=> '10',
			   									 		 'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу',
												 								'eng' => 'Maximum quantity of records on page'),
			   									 		 'access'	=> 'editable',
			   									 		 ),

			   'poll_order_by' 					=> array('type'		=> 'select',
														 'defval'	=> array('id DESC'	=> array(	'rus' => 'в порядке обратном добавлению',
														 											'eng' => 'reverce adding order'),
														 					 'id'		=> array(	'rus' => 'в порядке добавления',
														 											'eng' => 'adding order'),
														 					 ),
														 'descr'	=> array(	'rus' => 'Порядок вывода записей',
														 						'eng' => 'Show records order'),
			   									 		 'access'	=> 'editable',
			   									 		 ),

			   'poll_repeated_voting_interval'	 => array('type'	=> 'integer',
			   											  'defval'	=> '10',
			   									 		  'descr'	=> array(	'rus' => 'Интервал в минутах для повторного голосования для хоста',
														  						'eng' => 'Time period in minutes of repeat vote for host'),
			   									 		  'access'	=> 'editable',
			   									 		  ),

			    'poll_date_format'				=> array('type'		=> 'select',
					   									 'defval'	=> array('%d.%m.%Y'			=> array(	'rus' => 'День.Месяц.Год',
														 													'eng' => 'Day.Month.Year'),
					   									 					 '%Y.%m.%d'			=> array(	'rus' => 'Год.Месяц.День',
														 													'eng' => 'Year.Month.Day'),
					   									 					 ),
														 'descr'	=> array(	'rus' => 'Формат выводимой даты',
														 						'eng' => 'Format of deduced date'),
												 		 'access'	=> 'editable',
												 ),

			   'poll_count_hits'      			=> array('type'		=> 'checkbox',
					   									 'defval'	=> 'on',
					   									 'descr'	=> array(	'rus' => 'Учитывать статистику отображения контента',
														 						'eng' => 'To consider statistics of display of a content'),
					   									 'access'	=> 'editable'
				   									 ),

			   'poll_default_rights'			=> array('type'		=> 'string',
					   									 'defval'	=> '111111110001',
					   									 'descr'	=> array(	'rus' => 'Права для голосования по-умолчанию (формат <img src="/i/admin/rights_format.gif" width=72 height=18 alt="формат прав">, где x - 0 или 1, каждая группа представляет набор прав: удаление, публикация, редактирование, чтение)',
		 												 						'eng' => 'Poll default rights (format <img src="/i/admin/rights_format.gif" width=72 height=18 alt="rights format">, where x - 0 or 1, every group is rights set: delete, publication, edit, read)'),
					   									 'access'	=> 'editable'
													),
			   );

$OPEN_NEW = array();

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'poll_max_rows' => '10',
'poll_order_by' => 'id DESC',
'poll_repeated_voting_interval' => '10',
'poll_date_format' => '%d.%m.%Y',
'poll_count_hits' => 'on',
'poll_default_rights' => '111111110011',
/* ABOCMS:END */
);
?>