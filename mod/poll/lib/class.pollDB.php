<?php

class CPollData_DB extends TObject {
	var $id			= NULL;
	var $question	= NULL;
	var $begin_date	= NULL;
	var $end_date	= NULL;
	var $calendar_begin_date= NULL;
	var $calendar_end_date	= NULL;
	var $other_variant		= NULL;
	var $active		= NULL;
	var $owner_id	= NULL;
	var $usr_group_id	= NULL;
	var $rights		= NULL;

	function CPollData_DB ($table, $PK) {
		$this->TObject($table, $PK);
	}

	function ListAll () {
		$sql = "SELECT * FROM $this->_tbl";
		return db_loadList($sql);
	}

	function showRandom($date_format, $not_in_str) {
		$sql = "SELECT *, DATE_FORMAT(begin_date,'".My_Sql::escape($date_format)."') as begin_date, DATE_FORMAT(end_date,'".My_Sql::escape($date_format)."') as end_date
					FROM $this->_tbl
					WHERE begin_date <= CURDATE() AND end_date >= CURDATE() AND active = 1 ".My_Sql::escape($not_in_str)."
					ORDER BY RAND()
					LIMIT 0, 1";
		return db_loadObject($sql, $this);
	}

	function showPoll ($date_format, $calendar_date_format, $id) {
		$sql = "SELECT id, question, other_variant, active, DATE_FORMAT(begin_date,'" .My_Sql::escape($date_format). "') as begin_date, DATE_FORMAT(end_date,'".My_Sql::escape($date_format)."') as end_date, DATE_FORMAT(begin_date,'".My_Sql::escape($calendar_date_format)."') as calendar_begin_date, DATE_FORMAT(end_date,'".My_Sql::escape($calendar_date_format)."') as calendar_end_date FROM $this->_tbl WHERE id = ".intval($id);
		return db_loadObject($sql, $this);
	}

	function showPoll2($date_format, $active_str, $future_polls_str, $start_row, $rows, $order_by) {
		$sql = "SELECT *, DATE_FORMAT(begin_date,'".My_Sql::escape($date_format)."') as bd, DATE_FORMAT(end_date,'".My_Sql::escape($date_format)."') as ed FROM $this->_tbl WHERE 1=1 "
		    .My_Sql::escape($active_str)." ".My_Sql::escape($future_polls_str)." ORDER BY ".My_Sql::escape($order_by)." LIMIT ".intval($start_row).",".intval($rows);
		return db_loadList($sql);
	}

	function showVariants($id) {
		global $server, $lang;
		$sql = "SELECT * FROM ".$server.$lang."_poll_variants WHERE poll_id = ".intval($id)." ORDER BY v_rank";
		return db_loadList($sql);
	}

	function DeletePoll($id) {
		$sql = "DELETE FROM $this->_tbl WHERE id = ".intval($id);
		if (!db_exec($sql)) { return 0; } else { return 1; }
	}
}

###############################################################

class CPollVariants_DB extends TObject {
	var $id			= NULL;
	var $variant	= NULL;
	var $v_rank		= NULL;
	var $poll_id		= NULL;

	function CPollVariants_DB ($table, $PK) {
		$this->TObject($table, $PK);
	}

	function DeleteVariants($id) {
		$sql = "DELETE FROM $this->_tbl WHERE poll_id = ".intval($id);
		if (!db_exec($sql)) { return 0; } else { return 1; }
	}

	function getVariants($id) {
		$sql = "SELECT * FROM $this->_tbl WHERE poll_id = ".intval($id)." ORDER BY v_rank";
		return db_loadList($sql);
	}

	function stats($poll_id) {
		global $server, $lang;
		$sql = "SELECT a.poll_id as poll_id,
					   a.id as variant_id,
					   a.variant as variant,
					   COUNT(".$server.$lang."_poll_results.id) as result
					FROM $this->_tbl a LEFT JOIN ".$server.$lang."_poll_results ON ".$server.$lang."_poll_results.variant_id = a.id
					WHERE a.poll_id = ".intval($poll_id)."
					GROUP BY variant_id
					ORDER BY v_rank";
		return db_loadList($sql);
	}
}

###############################################################

class CPollResults_DB extends TObject {
	var $id			= NULL;
	var $variant	= NULL;
	var $v_rank		= NULL;
	var $poll_id		= NULL;

	function CPollResults_DB ($table, $PK) {
		$this->TObject($table, $PK);
	}

	function DeleteResults($id) {
		global $server, $lang;
		$sql = "DELETE FROM ".$server.$lang."_poll_results WHERE poll_id = ".intval($id);
		if (!db_exec($sql)) { return 0; } else { return 1; }
	}

	function serachHost($id, $where_id, $search_h, $search_a, $interval) {
		$host_condition = "";
		if ($search_h) {
			$host_condition = "host='".My_Sql::escape($search_h)."'";
			//$host_condition = "host LIKE '%$search_h%'";
		}
		if ($search_a) {
			$host_condition = ($host_condition)
							? $host_condition." OR host='".My_Sql::escape($search_a)."'"
							: "host='".My_Sql::escape($search_a)."'";
		}
		if ($host_condition) {
			$host_condition = " AND (".$host_condition.")";
		}
		$sql = "SELECT * FROM $this->_tbl WHERE !ISNULL(host) ".My_Sql::escape($where_id).$host_condition." AND date > DATE_SUB(NOW(), INTERVAL " . intval($interval)." MINUTE)";
		if (db_loadList($sql)) return 0; else return 1;
	}
}
?>