<?php

require_once (RP.'mod/poll/lib/class.pollDB.php');

class PollPrototype extends Module {

	var $main_title;
	var $table_prefix;
	var $addition_to_path;
	var $module_name				= 'poll';
	var $default_action				= 'showpoll';
	var $admin_default_action		= 'showpolls';
	var $tpl_path					= 'mod/poll/tpl/';
	var $already_displayed			= array();
	var $block_module_actions		= array();
	var $block_main_module_actions	= array();
	var $action_tpl = array(
	    'poll' => 'poll_show.html',
	    'showpoll' => 'poll_show.html',
	    'showpolls' => 'poll_list.html',
	    'stats' => 'poll_results.html',
	);

	function PollPrototype($action = '', $transurl = '', $properties = array(), $prefix = NULL, $rewrite_mod_params = NULL, $adminurl = NULL, $noadmin = false) {
		global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang, $permissions, $request_type, $_RESULT;
		/* Get $action, $tpl_name, $permissions */
		extract( $this->init(array(
		        'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
		        'action' => $action, 'tpl_name' => $properties['tpl']
		)), EXTR_OVERWRITE);
		
		$rows	= (int)$properties[1];
		$start	= 1;
		if (!$rows) $rows = $CONFIG['poll_max_rows'];
		
		if ('only_create_object' == $action) {
		    return;
		}
		
		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}

		if (! self::is_admin()) {
//---------------------------------- обработка действий с сайта --------------------------------//
			switch ($action) {
// Вывод опроса
			case "poll":
			    global $request_poll, $request_voteblock;
				if ($request_poll) {
				    $main->include_main_blocks($this->module_name.'_results_shot.html', 'main');
				    $tpl->prepare();
					$this->show_poll($transurl, $request_poll);

					break;

				}
				
				$main->include_main_blocks($tpl_name, "main");
				$tpl->prepare();
				$poll_id = $this->show_poll($transurl, $properties[2], $PAGE['block_id']);
				if ($CONFIG['poll_count_hits']) {
					Main::addHits($this->table_prefix."_polls", $poll_id);
				}
				break;
// Вывод случайного опроса
			case "showpoll":
			    global $request_poll, $request_voteblock,$request_success;
				if ($request_poll) {
				    $main->include_main_blocks($this->module_name.'_results_shot.html', 'main');
				    $tpl->prepare();
					//$this->show_poll($transurl, $request_poll);
                    if($request_success =='1'){
                        $tpl->newBlock("block_success") ;
                    }elseif($request_success =='-1'){
                        $tpl->newBlock("block_no_success");
                    } else{
                        $tpl->newBlock("block_no_success_err");
                    }
					//$tpl->newBlock("block_success");
					break;
                    exit;
				}
				
				$main->include_main_blocks($tpl_name, "main");
				$tpl->prepare();
				$poll_id = $this->show_poll($transurl, 0, $PAGE['block_id']);
				if ($CONFIG['poll_count_hits']) {
					Main::addHits($this->table_prefix."_polls", $poll_id);
				}
				break;

// Вывод списка опросов
			case "showpolls":
				global $request_id, $request_success;
				$success = intval($request_success);
				$main->include_main_blocks($tpl_name, "main");
				$tpl->prepare();
				$this->show_polls('block_polls', $transurl, $start, $rows);
				break;


// Голосовать
			case "vote":
				global $request_id, $request_variant, $request_other_text, $request_block_id,
				        $request_name, $request_email, $request_age, $request_sex;
//				if ($this->test_item_rights("r", "id", $this->table_prefix."_polls", $request_id)) {


                    if(!empty($request_id))foreach($request_id as $id){

                        $v=$this->search_such_host($id);
                        if($v){
                            $search_result=true;
                            break;
                        }
                        $search_result=false;
                    }
					if ($request_variant && $search_result) {
						$success =  -1;
					} else if ($request_variant && $this->add_result($request_id, $request_variant, $request_other_text,$request_name, $request_email, $request_age, $request_sex)) {
						@$cache->delete_cache_files();
						$success =  1;
					} else {
						$success =  0;
					}
//				}
                header("Location: ".$_SERVER['HTTP_REFERER'].'?voteblock='.intval($request_block_id).'&poll='.intval($request_id).'&success='.$success);
//				if ($success && $request_block_id) {
//				    //Module::go_back('', 'voteblock='.intval($request_block_id).'&poll='.intval($request_id));
//                    header("Location: ".$_SERVER['HTTP_REFERER'].'?voteblock='.intval($request_block_id).'&poll='.intval($request_id).'&success='.$success);
//				} else {
//				    header("Location: {$transurl}&action=stats&id={$request_id}&success={$success}&nocache");
//				}
				exit;
				break;


// Статистика голосований
			case "stats":
				global $request_id, $request_start, $request_success,$db;
				$start		= ($request_start) ? (int)$request_start : $start;
				$success	= $request_success;
				$main->include_main_blocks($tpl_name, 'main');
				$tpl->prepare();
				if ($request_id) {

                        $id = $request_id ? (int)$request_id : $properties[2];
                        $this->show_poll($transurl, $id);

				} else {
					header('Location: '.$baseurl.'&action=showpolls');
				}
				if ($success == 1) {
					$tpl->newBlock("block_success");
				} else if ($success == -1) {
					$tpl->newBlock("block_no_success");
				}
				break;


// Default...
			default:
				if (is_object($tpl)) $tpl->prepare();
				return;
			}
//------------------------------- конец обработки действий с сайта -----------------------------//
		} else {
		    $this->block_module_actions = array(
		      'showpoll'	=> $this->_msg["Show_random_poll"],
		      'poll'	=> $this->_msg["Show_poll"],
		    );
		    $this->block_main_module_actions = array(
		      'stats'		=> $this->_msg["Poll_results"],
		      'showpoll'	=> $this->_msg["Show_random_poll"],
		      'showpolls'	=> $this->_msg["Polls_list"],
		      'poll'	=> $this->_msg["Show_poll"],
		    );
			if (!isset($tpl)) {
			    $main->message_die(iconv("WINDOWS-1251", "UTF-8", 'Не подключен класс для работы с шаблонами'));
			}

			switch ($action) {
		    case 'prp':
		    	if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step;
				$main->include_main_blocks('_properties.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Information"	=> $main->_msg["Information"],
					"MSG_Value"			=> $main->_msg["Value"],
					"MSG_Save"			=> $main->_msg["Save"],
					"MSG_Cancel"		=> $main->_msg["Cancel"],
				));
		        if (!$request_step || $request_step == 1) {
					$tpl->assign(array('form_action'	=> $baseurl,
									   'step'			=> 2,
									   'lang'			=> $lang,
									   'name'			=> $this->module_name,
									   'action'			=> 'prp'));
					$prp_html = $main->getModuleProperties($this->module_name);
		            $this->main_title = $this->_msg["Controls"];
				} elseif (2 == $request_step) {
		        	$main->setModuleProperties($this->module_name, $_POST);
					header('Location: '.$baseurl.'&action=prp&step=1');
				}
		    	break;

			case 'block_prp':
				global $request_sub_action, $request_block_id;
		    	$arr  = $this->getEditLink($request_sub_action, $request_block_id);
				$cont = 'var container = document.getElementById(\'target_span\');
		    		  	 container.innerHTML = "'.$this->getPropertyFields().'";
		    		  	 material_id		  = "'.$arr['material_id'].'";
		    		  	 material_url		  = "'.$arr['material_url'].'";
		    		  	 changeAction();';
		    	header('Content-Length: '.strlen($cont));
		    	echo $cont;
		    	exit;
		    	break;

			case "showpolls":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_start;
				$start = ($request_start) ? $request_start : 1;
				$main->include_main_blocks_2($this->module_name."_list.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Activ" => $this->_msg["Activ"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Rights" => $main->_msg["Rights"],
					"MSG_Date_begin" => $this->_msg["Date_begin"],
					"MSG_Date_end" => $this->_msg["Date_end"],
					"MSG_Question" => $this->_msg["Question"],
					"MSG_Shows" => $this->_msg["Shows"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Select_action" => $this->_msg["Select_action"],
					"MSG_Activate" => $this->_msg["Activate"],
					"MSG_Suspend" => $this->_msg["Suspend"],
					"MSG_Delete" => $this->_msg["Delete"],
					"MSG_Execute" => $this->_msg["Execute"],
				));
				if (!($pages_cnt = $this->show_polls('block_polls', $baseurl, $request_start, "", 1))) {
					header("Location: $baseurl&action=add");
					exit;
				}
				$main->_show_nav_block($pages_cnt, null, "action=".$action, $start);
				$tpl->assign(Array(
					"MSG_Pages" => $this->_msg["Pages"],
				));
				$this->main_title = $this->_msg["Polls_list"];
				break;

// Добавление опроса в базу данных
			case "add":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_step, $request_question, $request_begin_date, $request_end_date,
						$request_variants, $request_other_variant,$request_type, $request_block_id;
				// Добавление новой записи, первый шаг
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name."_edit.html", $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(array(
						"MSG_Enter_question" => $this->_msg["Enter_question"],
						"MSG_Enter_date_begin" => $this->_msg["Enter_date_begin"],
						"MSG_Enter_date_end" => $this->_msg["Enter_date_end"],
						"MSG_Question" => $this->_msg["Question"],
						"MSG_Date_begin" => $this->_msg["Date_begin"],
						"MSG_Select" => $this->_msg["Select"],
						"MSG_Date_end" => $this->_msg["Date_end"],
						"MSG_Another_variant" => $this->_msg["Another_variant"],
						"MSG_Variants" => $this->_msg["Variants"],
						"MSG_Add" => $this->_msg["Add"],
						"MSG_Confirm_delete_this_variant" => $this->_msg["Confirm_delete_this_variant"],
						"MSG_Delete" => $this->_msg["Delete"],
						"MSG_Up" => $this->_msg["Up"],
						"MSG_Down" => $this->_msg["Down"],
						"MSG_Place" => $this->_msg["Place"],
						"MSG_ID" => $this->_msg["ID"],
						"MSG_Value" => $this->_msg["Value"],
						"MSG_required_fields" => $this->_msg["required_fields"],
						"MSG_Save" => $this->_msg["Save"],
						"MSG_Cancel" => $this->_msg["Cancel"],
						"_ROOT.form_action"	=>	"$baseurl&action=add&step=2",
						"_ROOT.cancel_link"	=>	"$baseurl&action=showpolls",
					));
					$this->main_title = $this->_msg["Add_poll"];
				// Добавление новой записи, шаг второй
				} else if ($request_step == 2) {
					$block_id = intval(substr($request_block_id, 10));
					if ($this->add_poll($request_question, $request_begin_date, $request_end_date, $request_variants, $request_other_variant, $block_id, $request_type)) @$cache->delete_cache_files();
					header("Location: $baseurl&action=showpolls");
					exit;
				}
				break;

// Редактирование опроса и ответов к нему
			case "edit":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_polls", $request_id))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name."_edit.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					"MSG_Enter_question" => $this->_msg["Enter_question"],
					"MSG_Enter_date_begin" => $this->_msg["Enter_date_begin"],
					"MSG_Enter_date_end" => $this->_msg["Enter_date_end"],
					"MSG_Question" => $this->_msg["Question"],
					"MSG_Date_begin" => $this->_msg["Date_begin"],
					"MSG_Select" => $this->_msg["Select"],
					"MSG_Date_end" => $this->_msg["Date_end"],
					"MSG_Another_variant" => $this->_msg["Another_variant"],
					"MSG_Variants" => $this->_msg["Variants"],
					"MSG_Add" => $this->_msg["Add"],
					"MSG_Confirm_delete_this_variant" => $this->_msg["Confirm_delete_this_variant"],
					"MSG_Delete" => $this->_msg["Delete"],
					"MSG_Up" => $this->_msg["Up"],
					"MSG_Down" => $this->_msg["Down"],
					"MSG_Place" => $this->_msg["Place"],
					"MSG_ID" => $this->_msg["ID"],
					"MSG_Value" => $this->_msg["Value"],
					"MSG_required_fields" => $this->_msg["required_fields"],
					"MSG_Save" => $this->_msg["Save"],
					"MSG_Cancel" => $this->_msg["Cancel"],
					"_ROOT.form_action"	=>	"$baseurl&action=update&id=$request_id",
					"_ROOT.cancel_link"	=>	"$baseurl&action=showpolls&start=$request_start",
				));
				if (!$this->show_poll_adm($request_id, $request_start)) { // если нет такого опроса то редирект
					header("Location: $baseurl&action=showpolls");
					exit;
				}
				$this->main_title = $this->_msg["Edit_poll"];
				break;

// Обновление опроса
			case "update":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_question, $request_begin_date, $request_end_date, $request_variants,
						$request_other_variant,$request_type, $request_id, $request_start;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_polls", $request_id))
					$main->message_access_denied($this->module_name, $action);

				if ($this->update_poll($request_question, $request_begin_date, $request_end_date, $request_variants, $request_other_variant, $request_type, $request_id)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showpolls&start=$request_start");
				exit;
				break;

// Отключение опроса
			case "suspend":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				if (!$this->test_item_rights("p", "id", $this->table_prefix."_polls", $request_id))
					$main->message_access_denied($this->module_name, $action);

				if ($main->suspend($this->table_prefix."_polls", $request_id)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showpolls&start=" . $request_start);
				exit;
				break;

// Включение опроса
			case "activate":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				if (!$this->test_item_rights("p", "id", $this->table_prefix."_polls", $request_id))
					$main->message_access_denied($this->module_name, $action);

				if ($main->activate($this->table_prefix."_polls", $request_id)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showpolls&start=" . $request_start);
				exit;
				break;

// Удаляем запись
			case "del":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				if (!$this->test_item_rights("d", "id", $this->table_prefix."_polls", $request_id))
					$main->message_access_denied($this->module_name, $action);

				if ($this->delete_poll($request_id)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showpolls&start=$request_start");
				exit;
				break;


//	Отключить публикацию выбранных опросов
			case "suspend_checked":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_start;
				if(is_array($request_check)) {
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $this->table_prefix."_polls", $v)) {
							if ($main->suspend($this->table_prefix."_polls", $v)) @$cache->delete_cache_files();
						}
					}
				}
				header("Location: $baseurl&action=showpolls&start=".$request_start);
				exit;
				break;

//	Включить публикацию выбранных опросов
			case "activate_checked":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_start;
				if(is_array($request_check)) {
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $this->table_prefix."_polls", $v)) {
							if ($main->activate($this->table_prefix."_polls", $v)) @$cache->delete_cache_files();
						}
					}
				}
				header("Location: $baseurl&action=showpolls&start=".$request_start);
				exit;
				break;

//	Удалить выбранные опросы
			case "delete_checked":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_start;
				if(is_array($request_check)) {
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("d", "id", $this->table_prefix."_polls", $v)) {
							if ($this->delete_poll($v)) @$cache->delete_cache_files();
						}
					}
				}
				header("Location: $baseurl&action=showpolls&start=$request_start");
				exit;
				break;


// Удаляем вариант другого ответа
			case "del_variant":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_ids;
				if ($this->delete_other_variant($request_ids)) @$cache->delete_cache_files();
				header("Location: ".$_SERVER["HTTP_REFERER"]);
				exit;
				break;

// Статистика голосований
			case "stats":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start,$db;
				$main->include_main_blocks_2($this->module_name.'_results.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Our_question" => $this->_msg["Our_question"],
					"MSG_poll_from" => $this->_msg["poll_from"],
					"MSG_to" => $this->_msg["to"],
				));
                $ids=array();
                if($request_id=='all')
                {
                    $active_str = "AND active = 1";
                    $future_polls_str = "AND begin_date <= CURDATE()";
                    $total_cnt = $this->get_list_with_rights(
                        "p.*, DATE_FORMAT(begin_date,'".$CONFIG["poll_date_format"]."') as bd, DATE_FORMAT(end_date,'".$CONFIG["poll_date_format"]."') as ed",
                        $this->table_prefix."_polls p", "p",
                        "1=1 $active_str $future_polls_str", "", $order_by,
                        null, null,
                        true);

                    while($db->next_record())$temp[]=$db->Record;
                        foreach($temp as $t){
                            $tpl->newBlock('block_results_table');
                            $tpl->assign(Array(
                                "MSG_Our_question" => $this->_msg["Our_question"],
                                "MSG_poll_from" => $this->_msg["poll_from"],
                                "MSG_to" => $this->_msg["to"],
                            ));
                            $this->show_poll_adm($t['id'], $request_start);
                            $ids[]=$t['id'];
                            $this->show_stats($t['id']);
                    }
                }

                else {
                    $tpl->newBlock('block_results_table');
                    $tpl->assign(Array(
                        "MSG_Our_question" => $this->_msg["Our_question"],
                        "MSG_poll_from" => $this->_msg["poll_from"],
                        "MSG_to" => $this->_msg["to"],
                    ));
                    $ids[]=$request_id;
                    $this->show_poll_adm($request_id, $request_start);
                    $this->show_stats($request_id);
                }
				$this->main_title = $this->_msg["Poll_results"]."(".$this->show_poll_users($ids).")";

				break;

// Редактирование прав голосования
			case 'editrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_polls", $request_id, true))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($this->table_prefix."_polls", "question", $request_id,
											"&id=".$request_id."&start=".$request_start, $this->_msg["poll"])) {
					header('Location: '.$baseurl.'&action=showpolls&start='.$request_start);
				}
				break;

// Сохранение прав голосования
			case 'saverights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start, $request_rights;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_polls", $request_id, true))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($this->table_prefix."_polls", $request_id, $request_rights);
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
						"res" => 1,
						"id" => $request_id,
					);
				} else {
					header('Location: '.$baseurl.'&action=showpolls&start='.$request_start);
				}
				exit;
				break;


// Редактирование свойств блока
			case 'properties':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $module_id, $request_field, $request_block_id, $title;

				$main->include_main_blocks('_block_properties.html', 'main');
				$tpl->prepare();

				if ($main->show_actions($request_field, '', $this->block_main_module_actions, $this->block_module_actions)) {
					$main->show_linked_pages($module_id, $request_field);
				}

				$tpl->assign(array(	'_ROOT.title'		=> $title,
									'_ROOT.username'	=> $_SESSION['session_login'],
									'_ROOT.password'	=> $_SESSION['session_password'],
									'_ROOT.name'		=> $this->module_name,
									'_ROOT.lang'		=> $lang,
									'_ROOT.block_id'	=> $request_block_id,
									));

				$this->main_title	= $this->_msg["Block_properties"];
				break;

// Создание объекта для дальнейшего использования
			case 'only_create_object':
				break;

// Default...
			default:
				$main->include_main_blocks();
				$tpl->prepare();
			}
//--------------------------- конец обработки действий из админ части --------------------------//
		}
	}


	// функция для вывода опроса и ответа к нему по id
	function show_poll($transurl = "?", $id = null, $block_id = 0) {
		global $CONFIG, $tpl, $baseurl, $db;
		$id = (int)$id;
		$PD = new CPollData_DB($this->table_prefix."_polls", "id");
		if ($id) {
			$this->get_list_with_rights("p.*", $this->table_prefix."_polls p", "p", "id = ".$id);
		} else {
			$id		= 0;
			$arr	= array();
            $not_in_str='';
//			if (count($this->already_displayed) > 0) {
//				$not_in_str = " AND id NOT IN (".implode(",", $this->already_displayed).")";
//			}
            $total_cnt=$this->get_list_with_rights(
						"p.*, DATE_FORMAT(begin_date,'".$CONFIG["poll_date_format"]."') as begin_date, DATE_FORMAT(end_date,'".$CONFIG["poll_date_format"]."') as end_date",
						$this->table_prefix."_polls p", "p",
						"begin_date <= CURDATE() AND end_date >= CURDATE() AND active = 1 ".$not_in_str);
		}
        while($db->next_record())$temp[]=$db->Record;
        if($temp)foreach($temp as $t){
                $id = $t["id"];
                $tpl->newBlock('block_poll');
                $arr = array(
                    poll_question		=> htmlspecialchars(stripslashes($t["question"])),
                    poll_begin_date		=> $t["begin_date"],
                    poll_end_date		=> $t["end_date"],
                    poll_id				=> $id,
                    start				=> $start,
                    checked				=> $t["active"] ? 'checked' : '',
                    polls_result		=> $transurl.'&action=stats',
                    form_action			=> $transurl.'&action=vote&nocache'.($block_id > 0 ? '&block_id='.intval($block_id) : ''),
                );
                $tpl->assign($arr);
                $other_variant = $t["other_variant"];
                $this->show_stats($id);
                $this->already_displayed[] = $id;
                $this->show_variants('block_variants', $id, $other_variant,$t["type"]);
                if($id) $this->addition_to_path = $this->_msg['Statistics_of'].' "'.$arr["poll_question"].'"';

        }
        return (int)$db->f("id");
		return FALSE;
	}


	// функция для вывода вариантов ответа
	function show_variants($blockname, $id = null, $other_variant = 0, $type=0) {
		global $CONFIG, $tpl, $baseurl, $server, $lang;
		$id = intval($id);
		if (!$id) return 0;
		$PV = new CPollVariants_DB($this->table_prefix."_poll_variants", "id");
		$temp_v = $PV->getVariants($id);
		if (count($temp_v) > 0) {
			$i = 0;
			foreach($temp_v as $tv) {
				$i++;
				$tpl->newBlock($blockname);
				$tpl->assign(array(
					v_answer	=>	htmlspecialchars(stripslashes($tv["variant"])),
					v_iterator	=>	$i,
					v_id			=>	$tv["id"],
                    type        => $type,
                    poll_id     => $id,
                    v_id_type   => $type ? $tv["id"] : 0,
                    type_input   => $type ? 'checkbox' : 'radio',
                    required   => !$type ?'required': '',
				));
			}
			if ($other_variant)for($k=0; $k < $other_variant;$k++){
                $tpl->newBlock($blockname."_other");
                if($k==0){
                    $i  ? $tpl->assign(
                    array(
                    'name'=>'другой ответ',
                    'none'=>'',
                    'v_id'=>$id,
                    'type' => $type, 
                    v_id_type   => $type ? $tv["id"] : 0,
                    type_input   => $type ? 'checkbox' : 'radio',
                    'disabled'	=>  "disabled='disabled'",
                    )) : 
                    $tpl->assign(
                    array('name'=>'',
                    'none'=>'none',
                     'required'=>  
                     'required', 
                     'v_id'=>$id, 
                     
                     'type' => $type,
                      v_id_type   => $type ? $tv["id"] : 0, 
                      type_input   => $type ? 'checkbox' : 'radio',
                      'disabled'	=>  "disabled='disabled'",
                      ));
                }else{
                     $tpl->assign(array('name'=>'',
                     'none'=>'none',
                      'v_id'=>$id, 
                      'type' => $type, 
                      v_id_type   => $type ? $tv["id"] : 0, 
                      type_input   => $type ? 'checkbox' : 'radio',
                      'disabled'	=>"disabled='disabled'"));
                }

            }
           return count($temp_v);
            }
            if ($other_variant) for ($k = 0; $k < $other_variant; $k++) {
                $tpl->newBlock($blockname . "_other");
                if ($k == 0) {
                    count($temp_v) > 0 ? $tpl->assign(
                    array('name' => 'другой ответ', 
                    'none' => '', 
                    'v_id' => $id, 
                    'type' => $type,
                     v_id_type => $type ? $tv["id"] : 0, 
                     type_input => $type ? 'checkbox' : 'radio',
                     'disabled'	=>  "disabled='disabled'",)) : $tpl->assign(
                     array('name' => '', 'none' => 'none', 
                     'required' => 'required', 'v_id' => $id, 
                     'type' => $type, v_id_type => $type ? $tv["id"] : 0, 
                     type_input => $type ? 'checkbox' : 'radio',
                     'disabled'	=> count($temp_v) > 0 ? "disabled='disabled'" : ""));
 
                }
                else{
                    $tpl->assign(array('name' => '', 'none' => 'none', 'v_id' => $id, 'type' => $type, v_id_type => $type ? $tv["id"] : 0, type_input => $type ? 'checkbox' : 'radio',
                    'disabled'	=>  "disabled='disabled'"));
                }
            }
            return FALSE;
	}


	// функция для вывода списка опросов
	function show_polls($blockname= 'block_polls', $baseurl, $start = null, $rows = null, $admin = 0) {
		global $CONFIG, $tpl, $db;
		$start = (int)$start;
		$start = ($start) ? $start : 1;
    	if (!$rows) $rows = $CONFIG["poll_max_rows"];
		if (!$admin) {
			$active_str = "AND active = 1";
			$future_polls_str = "AND begin_date <= CURDATE()";
		}
		$total_cnt = $this->get_list_with_rights(
								"p.*, DATE_FORMAT(begin_date,'".$CONFIG["poll_date_format"]."') as bd, DATE_FORMAT(end_date,'".$CONFIG["poll_date_format"]."') as ed",
								$this->table_prefix."_polls p", "p",
								"1=1 $active_str $future_polls_str", "", $order_by,
								$start, $rows,
								true);
		$pages_cnt = ceil($total_cnt/$rows);
		if ($db->nf()) {
			$actions = array(	"edit"		=> "edit",
								"editrights"=> "editrights",
								"activate"	=>	array("activate","suspend"),
								"delete"	=> "del",
								"confirm_delete"	=> $this->_msg["Confirm_delete_this_poll"]
							);
			while ($db->next_record()) {
				$id		= $db->f('id');
				$adm_actions = $this->get_actions_by_rights($baseurl."&id=".$id."&start=".$start, $db->f("active"), $db->f("is_owner"), $db->f("user_rights"), $actions);

				$tpl->newBlock($blockname);
				$tpl->assign(array(
					'poll_question'			=> $db->f("question"),
					'poll_begin_date'		=> $db->f("bd"),
					'poll_end_date'			=> $db->f("ed"),
					'poll_hits'			    => $db->f("hits"),
					'poll_edit_action'		=>	$adm_actions["edit_action"],
					'poll_edit_rights_action'	=>	$adm_actions["edit_rights_action"],
					'poll_del_action'		=>	$adm_actions["delete_action"],
					'change_status_action'	=>	$adm_actions["change_status_action"],
					'poll_link'				=> $baseurl."&action=stats&id=".$id."&start=".$start,
					'poll_id'				=> $id,
					'start'					=> $start,
				));
				if ($adm_actions["rights"]["d"] || $adm_actions["rights"]["p"]) {
					$tpl->newBlock('block_check');
					$tpl->assign("poll_id", $id);
				}
			}
			return $pages_cnt;
		}
		return FALSE;
	}


	// функция для добавления результата
	function add_result($poll_ids = null, $variants = null, $other_text = null,$name, $email, $age, $sex) {
		global $CONFIG, $server, $lang, $db;
        $username = ($_SESSION["siteuser"]) ? "'".$_SESSION["siteuser"]["siteusername"]."'" : "NULL";
        if ($_SERVER['REMOTE_HOST']) {
            $host = "'".$_SERVER['REMOTE_HOST']."'";
        } else if ($_SERVER['REMOTE_ADDR']) {
            $host = "'".$_SERVER['REMOTE_ADDR']."'";
        } else {
            $host = "NULL";
        }

        foreach($poll_ids as $poll_id){
            $poll_id		= (int)$poll_id;
            if(isset($variants[$poll_id]))foreach($variants[$poll_id] as $v){
                if($v){
                  //  foreach($variant as $v){

                        if ($v!='other'){
                            $query = 'INSERT INTO '.$this->table_prefix.'_poll_results
						SET poll_id		= '.$poll_id.',
							variant_id	= '.$v.',
							host		= '.$host.',
							siteusername= '.$username.',
							name= "'.$name.'",
							email= "'.$email.'",
							age= "'.$age.'",
							sex= "'.$sex.'",
							date		= NOW()';
                            $db->query($query);
                        } elseif ($v == "other") {

                            foreach($other_text[$poll_id] as $oth_poll){
                                if($oth_poll){
                                    $query = 'INSERT INTO '.$this->table_prefix.'_poll_other_results
						SET poll_id		= '.$poll_id.',
							variant_text= "'.$db->escape(trim($oth_poll)).'",
							host		= '.$host.',
							siteusername= '.$username.',
							name= "'.$name.'",
							email= "'.$email.'",
							age= "'.$age.'",
							sex= "'.$sex.'",
							date		= NOW()';
                                    $db->query($query);
                                }
                            }

                        }

                  //  }
                }

            }

            else{
                foreach($other_text[$poll_id] as $oth_poll){
                    var_dump($oth_poll);
                    $query = 'INSERT INTO '.$this->table_prefix.'_poll_other_results
						SET poll_id		= '.$poll_id.',
							variant_text= "'.$db->escape(trim($oth_poll)).'",
							host		= '.$host.',
							siteusername= '.$username.',
							name= "'.$name.'",
							email= "'.$email.'",
							age= "'.$age.'",
							sex= "'.$sex.'",
							date		= NOW()';
                    $db->query($query);

                }

            }

        }
        return TRUE;
//
//		$variant_id		= (int)$variant;
//		if ($_SERVER['REMOTE_HOST']) {
//			$host = "'".$_SERVER['REMOTE_HOST']."'";
//		} else if ($_SERVER['REMOTE_ADDR']) {
//			$host = "'".$_SERVER['REMOTE_ADDR']."'";
//		} else {
//			$host = "NULL";
//		}
//
//		if ($variant_id) {
//			$query = 'INSERT INTO '.$this->table_prefix.'_poll_results
//						SET poll_id		= '.$poll_id.',
//							variant_id	= '.$variant_id.',
//							host		= '.$host.',
//							siteusername= '.$username.',
//							date		= NOW()';
//		} elseif ($variant == "other") {
//			$query = 'INSERT INTO '.$this->table_prefix.'_poll_other_results
//						SET poll_id		= '.$poll_id.',
//							variant_text= "'.$db->escape(trim($other_text)).'",
//							host		= '.$host.',
//							siteusername= '.$username.',
//							date		= NOW()';
//		} else {
//			return FALSE;
//		}
//		$db->query($query);
//		return TRUE;
	}

	// функция поиска результата связанного с хостом
	// для предотвращения повторного голосования
	function search_such_host($id) {
		global $CONFIG, $db, $server, $lang;
		$id = intval($id);
		if ($id) $where_id = " AND poll_id = $id";
		$search_h = $_SERVER['REMOTE_HOST'];
		$search_a = $_SERVER['REMOTE_ADDR'];
		$PR = new CPollResults_DB($this->table_prefix."_poll_results", "id");
		if ($PR->serachHost($id, $where_id, $search_h, $search_a, $CONFIG["poll_repeated_voting_interval"])) {
			$PR = new CPollResults_DB($this->table_prefix."_poll_other_results", "id");
			if ($PR->serachHost($id, $where_id, $search_h, $search_a, $CONFIG["poll_repeated_voting_interval"])) {
				return 0;
			} else {
				return 1;
			}
		} else {
			return 1;
		}
	}


	// функция для показа результатов опроса по id
	function show_stats($poll_id = null) {
		global $CONFIG, $db, $tpl, $server, $lang, $baseurl;
		$poll_id		= (int)$poll_id;
		if (!$poll_id) return FALSE;
		$variants		= array();
		$results		= array();
		$max_width		= 400;
		$sum			= 0;
		$max			= 0;
		$max_element_id = 0;
		$PV = new CPollVariants_DB($this->table_prefix."_poll_variants", "id");
		$temp = $PV->stats($poll_id);
		$db->query("SELECT id,variant_text
					FROM ".$this->table_prefix."_poll_other_results
					WHERE poll_id=".$poll_id."
					ORDER BY variant_text");

		$first_record = true;
		while ($db->next_record()) {
			if (!$first_record && $last_text == $db->f('variant_text')) {
				$idx = sizeof($others) - 1;
				$others[$idx]["count"]++;
				$others[$idx]["ids"] .= ",".$db->f('id');
			} else {
				$first_record = false;
				$last_text = $db->f('variant_text');
				$others[] = array(
								"variant_text"	=> $last_text,
								"count"			=> 1,
								"ids"			=> $db->f('id'),
							);
			}
		}
		if (count($temp) > 0 || is_array($others)) {
			if (count($temp) > 0) {
				foreach($temp as $idx => $v) {
					$variants[$v["variant_id"]]		= $v["variant"];
					$results[$v["variant_id"]]		= intval($v["result"]);
					$sum += $results[$v["variant_id"]];
					if ($results[$v["variant_id"]] > $max) {
						$max = $results[$v["variant_id"]];
						$max_element_id = $v["variant_id"];
					}
				}
			}
			if (is_array($others)) {
				$others_cnt = 0;
				foreach($others as $idx => $v) {
					$others_cnt += $v["count"];
				}
				$variants["others"]		= $this->_msg["Another_answer"];
				$results["others"]		= $others_cnt;
				$sum += $results["others"];
				if ($results["others"] > $max) {
					$max = $results["others"];
					$max_element_id = "others";
				}
			}
			$one_percent_votes = $sum / 100;
			$gif_width = ($max_element_id) ? round(($results[$max_element_id] / $one_percent_votes) * 4) + 20 : 20;
			$cnt_variants = sizeof($variants);
			$last_proc = 100;
			foreach ($variants as $v_id => $variant) {
				if ($sum) {
					$result	= round($results[$v_id] / $one_percent_votes);
					$cnt_variants--;
					if ($cnt_variants == 0) {
						$result	= $last_proc;
					}
					$last_proc -= $result;
					$width	= round($result * 4);
				} else {
					$result	= 0;
					$width	= 0;
				}
				$tpl->newBlock('block_results');
				$tpl->assign(array(
					variant			=> ($v_id=='others') ? $this->_msg['Another_answer'] :$variant,
					result			=> $result.'%',
					vote			=> $results[$v_id],
					width			=> $width + 1,
					gif_width		=> $gif_width,
					vgif_width		=> $gif_width.'   '.$width,
				));
			}
			if (is_array($others)) {
				$tpl->newBlock('block_other_results');
				$tpl->assign(Array(
					"MSG_Another_answers" => $this->_msg["Another_answers"],
				));
				foreach($others as $idx => $v) {
					$tpl->newBlock('block_other_result');
					$tpl->assign(Array(
						"MSG_Confirm_delete_this_answer" => $this->_msg["Confirm_delete_this_answer"],
						"MSG_Delete" => $this->_msg["Delete"],
					));
					$tpl->assign(array(
						variant				=> ($v["variant_text"]) ? htmlspecialchars($v["variant_text"]) : "&laquo;" . $this->_msg["Without_variant"] . "&raquo;",
						vote				=> $v["count"],
						variant_del_link	=> $baseurl."&action=del_variant&ids=".$v["ids"],
					));
				}
			}
			return TRUE;
		}
		return FALSE;
	}


	// вывести навигационную строчку, отображающую путь
	function show_nav_path($id = 0, $transurl = "?") {
		$id = intval($id);
		global $CONFIG, $tpl, $lang;
		$path = $this->get_nav_path($id, $transurl);
		$tpl->assign(array(
			"_ROOT.path"	=>	$path,
		));
		return 1;
	}


	// получить навигационную строчку, отображающую путь к опросу
	function get_nav_path($id, $transurl = "?") {
		global $CONFIG, $db, $server, $lang;
		$id = intval($id);
		$separator = $CONFIG["nav_separator"];
		$root = ($id) ? "$separator<a href=\"$transurl&action=stats\">" . $this->_msg["Polls"] . "</a>" : "$separator<span class=red>" . $this->_msg["Polls"] . "</span>";
		if ($id) {
			$query = "SELECT * FROM ".$this->table_prefix."_polls WHERE id = $id";
			$db->query($query);
			if ($db->nf() > 0) {
				$db->next_record();
				$path = "$separator<span class=red>" . $db->f("question") . "</span>";
			}
		}
		$path = $root . $path;
		return $path;
	}

	function getPropertyFields() {
		GLOBAL $request_sub_action, $db;
		if ('' != $request_sub_action) {

			$tpl = $this->getTmpProperties();

			switch ($request_sub_action) {
				case 'showpolls':
					$tpl->newBlock('block_properties');
					$tpl->newBlock('block_per_page');
					$tpl->assign(Array(
						"MSG_Polls_per_page" => $this->_msg["Polls_per_page"],
					));
				case 'poll':
				case 'stats':
					$tpl->newBlock('block_properties');
					$tpl->newBlock('block_poll');
					$tpl->assign(Array(
						"MSG_Select_poll" => $this->_msg["Select_poll"],
					));
					$list = $db->fetchAll($this->table_prefix.'_polls');
					abo_str_array_crop($list);
					Module::show_select(
					   'block_poll_list',
					   '',
					  array('id', 'question'),
					  $list,
					  $tpl
					);
					break;

				default:
					$tpl->newBlock('block_properties_none');
					$tpl->assign(Array(
						"MSG_No" => $this->_msg["No"],
					));
					break;
			}
			return $this->getOutputContent($tpl->getOutputContent());
		}
		return FALSE;
	}

	function getEditLink($sub_action = NULL, $block_id = NULL) {
		GLOBAL $request_name, $db, $lang, $server;
		$block_id	= (int)$block_id;
		if ($sub_action && $block_id) {
			$db->query('SELECT	property1,
								property2,
								property3,
								property4,
								property5
							FROM '.$this->table_prefix.'_pages_blocks
							WHERE id = '.$block_id);
			if ($db->nf() > 0) {
				$db->next_record();
				$arr[]	= $db->f('property1');
				$arr[]	= $db->f('property2');
				$arr[]	= $db->f('property3');
				$arr[]	= $db->f('property4');
				$arr[]	= $db->f('property5');

				switch ($sub_action) {
					default:
						$link['material_url']	= '';
						$link['material_id']	= '';
						break;
				}
				return $link;
			}

			switch ($sub_action) {
				default:
					$link['material_url']	= '';
					$link['material_id']	= '';
					break;
			}
			return $link;
		}
		return FALSE;
	}

	// функция для вывода опроса и ответа к нему по id
	function show_poll_adm($id = null, $start = null) {
		global $CONFIG, $tpl, $db;
		$id = intval($id);
		if (!$id) return 0;
		$this->get_list_with_rights(
					"id, question, other_variant, type, active, DATE_FORMAT(begin_date,'" .$CONFIG["poll_date_format"]. "') as begin_date, DATE_FORMAT(end_date,'".$CONFIG["poll_date_format"]."') as end_date, DATE_FORMAT(begin_date,'".$CONFIG["calendar_date"]."') as calendar_begin_date, DATE_FORMAT(end_date,'".$CONFIG["calendar_date"]."') as calendar_end_date",
					$this->table_prefix."_polls p", "p",
					"id = ".$id);
		if ($db->next_record()) {
			$tpl->assign(array(
				poll_question				=>	htmlspecialchars(stripslashes($db->f("question"))),
				poll_begin_date				=>	$db->f("begin_date"),
				poll_end_date				=>	$db->f("end_date"),
				calendar_poll_begin_date	=>	$db->f("calendar_begin_date"),
				calendar_poll_end_date		=>	$db->f("calendar_end_date"),
				other_variant_checked		=>	($db->f("other_variant")) ? ' checked' : '',
                other_variant               =>  $db->f("other_variant"),
				type		                =>	($db->f("type")) ? ' checked' : '',
				poll_id						=>	intval($db->f("id")),
				start						=>	$start,
			));
			$this->show_variants('fill_array', intval($db->f("id")));
			return 1;
		}
	}


	// функция для добавления опроса в базу данных
	function add_poll($question, $begin_date, $end_date, $variants, $other_variant, $block_id = 0, $type) {
		global $CONFIG, $main, $db;

		$owner_id = $_SESSION["siteuser"]["id"];
		list($group_id, $rights) = $this->get_group_and_rights('poll_default_rights', $block_id);

		$PD = new CPollData_DB($this->table_prefix."_polls", "id");
		$PD->id				= 0;
		$PD->question		= $db->escape($question);
		$PD->begin_date		= $main->transform_date($begin_date);
		$PD->end_date		= $main->transform_date($end_date);
		$PD->other_variant	= intval($other_variant);
		$PD->type	= intval($type);
		$PD->owner_id		= $owner_id;
		$PD->usr_group_id	= $group_id;
		$PD->rights			= $rights;
		if (!$PD->store()) {
			return FALSE;
		} else {
			$lid = db_insert_id();
			if (sizeof($variants)>0) {
				$this->add_variants($variants, $lid);
			}
			return $lid;
		}
	}

	// функция для добавления вариантов ответа по id опроса
	function add_variants($variants = array(), $id = null) {
		global $CONFIG, $db;
		$PD = new CPollData_DB($this->table_prefix."_polls", "id");
		$id = (int)$id;
		if (!$id) return FALSE;
		$i = 0;
		foreach($variants as $key=>$value) {
			if ($value) {
				$i++;
				$PV = new CPollVariants_DB($this->table_prefix."_poll_variants", "id");
				$PV->id			= 0;
				$PV->variant	= $db->escape($value);
				$PV->v_rank	    = $i;
				$PV->poll_id	= $id;
				if (!$PV->store()) {
					return FALSE;
				}
			}
		}
		return db_affected_rows() ? TRUE : FALSE;
	}

	// функция для редактирования опроса в базе данных
	function update_poll($question, $begin_date, $end_date, $variants, $other_variant, $type, $id) {
		global $CONFIG, $main;
	    $id = intval($id);
		if (!$id) return 0;
		$PD = new CPollData_DB($this->table_prefix."_polls", "id");
		$PD->question		= addslashes($question);
		$PD->begin_date		= $main->transform_date($begin_date);
		$PD->end_date		= $main->transform_date($end_date);
		$PD->other_variant	= intval($other_variant);
		$PD->type	= intval($type);
		$PD->id				= $id;
		if (!$PD->store()) {
			return 0;
		} else {
			$this->update_variants($variants, $id);
			return 1;
		}
	}


	// функция для обновления вариантов ответа по id опроса
	// сначала через delete_poll_variants удаляются все старые записи
	function update_variants($variants = array(), $id = null) {
		global $CONFIG, $db;
		$id = intval($id);
		if (!$id) return 0;
		$variants_array = array();
		$PV = new CPollVariants_DB($this->table_prefix."_poll_variants", "id");
		if (count($variants) > 0) {
			$temp_v = $PV->getVariants($id);
			if (count($temp_v) > 0) {
				foreach($temp_v as $tv) {
					$variants_array[] = $tv["id"];
				}
			}
		}
		$this->get_list_with_rights("p.id", $this->table_prefix."_polls p", "p", "id = ".$id);
		if ($db->next_record() && ($db->f("user_rights") & 8)) $PV->DeleteVariants($id);

		if (count($variants) > 0) {
			foreach($variants as $key => $value) {
				$v_id = in_array($key, $variants_array) ? 'id = '.$key.', ' : '';
				$i++;
				$value = $db->escape($value);
				$query = 'REPLACE INTO '.$this->table_prefix.'_poll_variants SET '.$v_id.'poll_id = '.$id.', v_rank = '.$i.', variant = "'.$value.'"';
				$db->query($query);
			}
		}
		return db_affected_rows() ? 1 : 0;
	}


	// функция для удаления опроса из базы данных
	function delete_poll($id) {
		global $CONFIG, $db;
		$id = intval($id);
		if (!$id) return 0;
		$PD = new CPollData_DB($this->table_prefix."_polls", "id");
		if (!$PD->DeletePoll($id)) {
			return 0;
		} else {
			$PV = new CPollVariants_DB($this->table_prefix."_poll_variants", "id");
			$PR = new CPollResults_DB($this->table_prefix."_poll_results", "id");
			$PV->DeleteVariants($id);
			$PR->DeleteResults($id);
			$db->query('DELETE FROM '.$this->table_prefix.'_poll_other_results WHERE poll_id = '.$id);
			return 1;
		}
	}


	// функция для удаления варианта другого ответа из базы данных
	function delete_other_variant($ids) {
		global $db;
		$db->query("DELETE FROM ".$this->table_prefix."_poll_other_results WHERE id IN(".$ids.")");
		return $db->affected_rows();
	}

    function show_poll_users($ids){
        global $db,$tpl;
        $arr_result=array();
        $count=0;
        if(!empty($ids)){
            foreach($ids as $id){
                $arr=$db->getArrayOfResult('SELECT * FROM `sup_rus_poll_results` Where poll_id='.$id.' ORDER BY date');
                if(!empty($arr))foreach($arr as $r){
                    $r['sex']=$r['sex']==1 ? 'мужской': 'женский';
                    $r['email']!=="" ? $arr_result[$r['email']]=$r : $arr_result[$r['sex'].$r['date'].$r['name']]=$r;
                }
                $arr=$db->getArrayOfResult('SELECT * FROM `sup_rus_poll_other_results` Where poll_id='.$id.' ORDER BY date');
                if(!empty($arr))foreach($arr as $r){
                    $r['sex']=$r['sex']==1 ? 'мужской': 'женский';
                    $r['email']!=="" ? $arr_result[$r['email']]=$r : $arr_result[$r['sex'].$r['date'].$r['name']]=$r;
                }
            }
            if($arr_result)foreach($arr_result as $r){

                $tpl->newBlock('block_users');
                $tpl->assign($r);
            }
            $count= count($arr_result);
        }
        return $count;
    }
}

?>