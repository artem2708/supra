<?php
require_once(RP.'/inc/class.Mailer.php');

class FormsPrototype extends Module {

	var $main_title;
	var $table_prefix;
	var $addition_to_path;
	var $module_name	= 'forms';
	var $types_array	= array();
	var $block_module_actions		= array();
	var $block_main_module_actions	= array();
	var $default_action				= 'show';
	var $admin_default_action		= 'showctg';
	var $tpl_path					= 'mod/forms/tpl/';
	var $file_path					= 'files/forms/';
    var $DisallowedTypes = "html|phtml|php|php3|php4|php5|shtml|cgi|exe|pl|asp|aspx|htaccess|htgroup|htpasswd";
    var $action_tpl = array(
	    'show' => 'forms.html',
	);

	function FormsPrototype($action = '', $transurl = '', $properties = array(), $prefix = NULL, $rewrite_mod_params = NULL, $adminurl = NULL, $noadmin = false) {
		global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang, $permissions, $request_type, $_RESULT;

		/* Get $action, $tpl_name, $permissions */
		extract( $this->init(array(
		        'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
		        'action' => $action, 'tpl_name' => $properties['tpl']
		)), EXTR_OVERWRITE);
		
		$id = (int)$properties[1];					// показывать форму
		
		if ('only_create_object' == $action) {
		    return;
		}
		
		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}
		
		if (! self::is_admin()) {
//---------------------------------- обработка действий с сайта --------------------------------//
			switch ($action) {

// Вывод формы по id
			case "show":
				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                if($id == 12) $tpl_name = "forms_pp.html";
				$main->include_main_blocks($tpl_name, 'main');
				$tpl->prepare();
				$this->show_form($id, '', $transurl);
				if ($CONFIG['forms_count_hits']) {
					Main::addHits($this->table_prefix."_forms", $id);
				}
				break;

// Получение данных формы
			case "post":
 				global $db, $request_id, $request_form_name, $request_ids, $request_captcha;
 				if($request_id == 12) $tpl_name = "forms_pp.html";
 				$tpl_name = $tpl_name ? $tpl_name : $this->module_name.".html";
 				
				$this->get_list_with_rights(
						"f.*", $this->table_prefix."_forms f", "f",
						'id = '.(integer)$request_id);
				if ($db->nf() > 0) {
					$db->next_record();
					$is_captcha = (integer)$db->f("captcha");
				}

				if($is_captcha == 1 && (!$request_captcha || $request_captcha != $_SESSION[$CONFIG["forms_captcha_secretword"].($request_id == 12 ? '_' : '')]))
				{
					unset($_SESSION[$CONFIG["forms_captcha_secretword"].($request_id == 12 ? '_' : '')]);
					$errBlock = "block_errorcode";$showForm = true;
				} else {
					unset($_SESSION[$CONFIG["forms_captcha_secretword"]]);
					$form_name = strip_tags($request_form_name);
					$fields_ids = explode(":", $request_ids);

					if (count($fields_ids) > 0) {
						foreach($fields_ids as $id => $fid) {
							$form_fields[$fid] = $_REQUEST["form_fields".$fid];
						}
					}
					if ($form_name) {
						$form_name_str = " \"$form_name\"";
					}
					// Отсылка данных формы
					$mail_res = $this->mail_form_data($request_id, $form_fields);
					// Письмо отправленно

					if (TRUE === $mail_res) {
						$errBlock = "block_message"; $showForm = false;
					// Ошибка при отправке письма или не все обязательные поля заполнены
					} else {
						$errBlock = "block_error"; $showForm = true;
					}
				}
				$main->include_main_blocks($tpl_name, "main");
				$tpl->prepare();

				if($errBlock) $tpl->newBlock($errBlock);
				if($showForm) $this->show_form($request_id, '', $transurl);
				break;

// Default...
			default:
				if (is_object($tpl)) $tpl->prepare();
				return;
			}
//------------------------------- конец обработки действий с сайта -----------------------------//
		} else {
//------------------------------ обработка действий из админ части -----------------------------//
		  $this->types_array	= array('text'		=> $this->_msg["Textfield"],
  									'textarea'	=> $this->_msg["Textarea"],
  									'radio'		=> $this->_msg["Radio_list"],
  									'select'	=> $this->_msg["Select_list"],
  									'checkbox'	=> $this->_msg["Checkbox"],
  									'date'		=> $this->_msg["Date"],
  									'comments'	=> $this->_msg["Comment"],
  									'file'	    => $this->_msg["File"],
  									);
		  
		  $this->block_module_actions			= array('show'	=> $this->_msg["Show_form"]);
		  $this->block_main_module_actions	= array('show'	=> $this->_msg["Show_form"]);

		  if (!isset($tpl)) {
		      $main->message_die(iconv("WINDOWS-1251", "UTF-8", 'Не подключен класс для работы с шаблонами'));
		  }
			switch ($action) {
		    case 'prp':
		    	if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step;
				$main->include_main_blocks('_properties.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Information"	=> $main->_msg["Information"],
					"MSG_Value"			=> $main->_msg["Value"],
					"MSG_Save"			=> $main->_msg["Save"],
					"MSG_Cancel"		=> $main->_msg["Cancel"],
				));
		        if (!$request_step || $request_step == 1) {
					$tpl->assign(array('form_action'	=> $baseurl,
									   'step'			=> 2,
									   'lang'			=> $lang,
									   'name'			=> $this->module_name,
									   'action'			=> 'prp'));
					$prp_html = $main->getModuleProperties($this->module_name);
		            $this->main_title = $this->_msg["Controls"];
				} elseif (2 == $request_step) {
		        	$main->setModuleProperties($this->module_name, $_POST);
					header('Location: '.$baseurl.'&action=prp&step=1');
				}
		    	break;

		    case 'block_prp':
				global $request_sub_action, $request_block_id;
		    	$arr  = $this->getEditLink($request_sub_action, $request_block_id);
		    	$cont = 'var container = document.getElementById(\'target_span\');
		    		  	 container.innerHTML = "'.$this->getPropertyFields().'";
		    		  	 material_id		  = "'.$arr['material_id'].'";
		    		  	 material_url		  = "'.$arr['material_url'].'";
		    		  	 changeAction();
		    	      	 setURL("property1");';
		    	header('Content-Length: '.strlen($cont));
		    	echo $cont;
		    	exit;
		    	break;

		    case "list":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_start, $request_category;
				$start = ($request_start) ? $request_start : 1;
				$category = ($request_category) ? $request_category : 1;
				$main->include_main_blocks_2($this->module_name."_list.html", $this->tpl_path);
				$tpl->assignInclude("search_form", $tpl_path ."_search_form.html" );
				$tpl->prepare();
				$tpl->assign(array(
					"MSG_Activ" => $this->_msg["Activ"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Rights" => $main->_msg["Rights"],
					"MSG_Name" => $this->_msg["Name"],
					"MSG_Recipient_email" => $this->_msg["Recipient_email"],
					"MSG_Shows" => $this->_msg["Shows"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Select_action" => $this->_msg["Select_action"],
					"MSG_Activate" => $this->_msg["Activate"],
					"MSG_Suspend" => $this->_msg["Suspend"],
					"MSG_Delete" => $this->_msg["Delete"],
					"MSG_Execute" => $this->_msg["Execute"],
					"_ROOT.form_search_action"	=>	"$baseurl&action=search",
					"_ROOT.lang"	=>	$lang,
					"_ROOT.name"	=>	$this->module_name,
					"_ROOT.action"	=>	"search",
				));
				list($category_title, $ctg_owner_id) = $this->show_category_adm($request_category, false);
				$pages_cnt = $this->show_forms_adm("pages", $request_category, $request_start, "", $ctg_owner_id);
				$main->_show_nav_block($pages_cnt, null, "action=".$action."&category=".$category, $start);
				$tpl->assign(Array(
					"MSG_Pages" => $this->_msg["Pages"],
				));
				$this->main_title = ($category_title) ? $category_title : "";
				break;

// Вывод списка заполненных форм
			case "datalist":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_start, $request_id, $request_start;
				$start = ($request_start) ? $request_start : 1;
				//$category = ($request_category) ? $request_category : 1;
				$main->include_main_blocks_2($this->module_name."_datalist.html", $this->tpl_path);
				//$tpl->assignInclude( "search_form", $tpl_path . "_search_form.html" );
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Text" => $this->_msg["Text"],
					"MSG_Del" => $this->_msg["Del"],
				));
				/*
				$tpl->assign(
					array(
						"_ROOT.form_search_action"	=>	"$baseurl&action=datasearch",
						"_ROOT.lang"	=>	$lang,
						"_ROOT.name"	=>	$this->module_name,
						"_ROOT.action"	=>	"datasearch",
					)
				);
				*/
				$pages_cnt = $this->show_data_list("pages", $request_id, $request_start);
				$main->_show_nav_block($pages_cnt, null, "action=".$action."&id=".$request_id, $start);
				$tpl->assign(Array(
					"MSG_Pages" => $this->_msg["Pages"],
				));
				$form_name = $this->show_form($request_id);
				$form_name = $this->_msg["Filled_form_data"] . " \"" . $form_name . "\"";
				$this->main_title = ($form_name) ? $form_name : "";
				break;

// Удаление записи
//-----------------------
			case "deldata":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_data_id, $request_id, $request_start, $db;
				$db->query("SELECT category_id FROM ".$this->table_prefix."_forms WHERE id=".$request_id);
				if ($db->next_record()) {
					list($category_title, $ctg_owner_id) = $this->show_category_adm($db->f("category_id"), false);
					if (!$this->test_item_rights("d", "id", $this->table_prefix."_forms", $request_id, false, $ctg_owner_id))
						$main->message_access_denied($this->module_name, $action);

					$this->delete_form_data($request_data_id);
				}
				header("Location: $baseurl&action=datalist&id=$request_id&start=$request_start");
				exit;
				break;

// Вывод результата поиска
//-------------------------------
			case "search":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_search, $request_category, $request_start;
				$search = strip_tags(trim($request_search));
				$main->include_main_blocks_2($this->module_name."_list.html", $this->tpl_path);
				$tpl->assignInclude("search_form", $tpl_path."_search_form.html" );
				$tpl->prepare();
				$result = $this->show_forms_adm("search", $request_category, $request_start, $search);
				if (!$result) $no_res = $this->_msg["Didnt_match_anything"];
				$tpl->assign(
					array(
						"_ROOT.form_search_action"	=>	"$baseurl&action=search",
						"_ROOT.lang"	=>	$lang,
						"_ROOT.name"	=>	$this->module_name,
						"_ROOT.action"	=>	"search",
						"_ROOT.search"	=>	$search,
						"_ROOT.search_result"	=>	$this->_msg["Search_results"] . " \"$search\": $no_res",
					)
				);
				$this->main_title = $this->_msg["Search_results_in_forms"];
				break;

// Добавление записи в базу данных
//---------------------------------------
			case "add":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_step, $request_form_name, $request_form_email, $request_captcha,
						$request_category, $request_field_types, $request_field_titles, $request_field_additional,
						$request_field_required, $request_field_text_type, $request_field_text_len, $request_block_id;
				// Добавление новой записи, первый шаг
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name."_edit.html", $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(array(
						"MSG_Category" => $this->_msg["Category"],
						"MSG_Select_category" => $this->_msg["Select_category"],
						"MSG_Form_name" => $this->_msg["Form_name"],
						"MSG_Sending_email" => $this->_msg["Sending_email"],
						"MSG_Add_field" => $this->_msg["Add_field"],
						"MSG_you_can_see_fields" => $this->_msg["you_can_see_fields"],
						"MSG_Textfield" => $this->_msg["Textfield"],
						"MSG_for_string_255" => $this->_msg["for_string_255"],
						"MSG_Example" => $this->_msg["Example"],
						"MSG_Textarea" => $this->_msg["Textarea"],
						"MSG_for_big_text" => $this->_msg["for_big_text"],
						"MSG_Big_text" => $this->_msg["Big_text"],
						"MSG_Checkbox" => $this->_msg["Checkbox"],
						"MSG_for_flag_yes_no" => $this->_msg["for_flag_yes_no"],
						"MSG_Select_list" => $this->_msg["Select_list"],
						"MSG_for_radio_list" => $this->_msg["for_radio_list"],
						"MSG_List_value" => $this->_msg["List_value"],
						"MSG_Date" => $this->_msg["Date"],
						"MSG_for_date" => $this->_msg["for_date"],
						"MSG_Select" => $this->_msg["Select"],
						"MSG_Radio_list" => $this->_msg["Radio_list"],
						"MSG_Comment" => $this->_msg["Comment"],
						"MSG_for_comments" => $this->_msg["for_comments"],
						"MSG_Field_type" => $this->_msg["Field_type"],
						"MSG_Select_field_type" => $this->_msg["Select_field_type"],
						"MSG_Textfield" => $this->_msg["Textfield"],
						"MSG_Textarea" => $this->_msg["Textarea"],
						"MSG_Radio_list" => $this->_msg["Radio_list"],
						"MSG_Select_list" => $this->_msg["Select_list"],
						"MSG_Checkbox" => $this->_msg["Checkbox"],
						"MSG_Date" => $this->_msg["Date"],
						"MSG_Comment" => $this->_msg["Comment"],
						"MSG_File" => $this->_msg["File"],
						"MSG_Field_name_title" => $this->_msg["Field_name_title"],
						"MSG_Required" => $this->_msg["Required"],
						"MSG_Form_fields" => $this->_msg["Form_fields"],
						"MSG_Add_CAPTCHA_code" => $this->_msg["Add_CAPTCHA_code"],
						"MSG_required_fields" => $this->_msg["required_fields"],
						"MSG_Save" => $this->_msg["Save"],
						"MSG_Cancel" => $this->_msg["Cancel"],
						"MSG_Confirm_delete_this_field" => $this->_msg["Confirm_delete_this_field"],
						"MSG_Select_field_type" => $this->_msg["Select_field_type"],
						"MSG_Enter_field_name" => $this->_msg["Enter_field_name"],
						"MSG_Name_title" => $this->_msg["Name_title"],
						"MSG_Required" => $this->_msg["Required"],
						"MSG_Type_symbol_max_num" => $this->_msg["Type_symbol_max_num"],
						"MSG_List_elements" => $this->_msg["List_elements"],
						"MSG_each_element_with_br" => $this->_msg["each_element_with_br"],
						"MSG_Enter_sending_form_email" => $this->_msg["Enter_sending_form_email"],
						"MSG_Enter_form_name" => $this->_msg["Enter_form_name"],
						"MSG_Choose_category" => $this->_msg["Choose_category"],
						"_ROOT.form_action"	=>	"$baseurl&action=add&step=2",
						"_ROOT.cancel_link"	=>	"$baseurl&action=showctg",
					));
					$nf = $this->show_categories("", true);
					if ($nf == 0) { // если нет ни одной категории то редирект
						header("Location: $baseurl&action=showctg&empty");
						exit;
					}
					$this->main_title = $this->_msg["Add_form"];
				// Добавление новой записи, шаг второй
				} else if ($request_step == 2) {
					list($allow, $ctg_rights) = $this->test_item_rights("e", "id", $this->table_prefix."_forms_categories",
																		$request_category, false, 0, true);
					if (!$allow) $main->message_access_denied($this->module_name, $action);

					$block_id = intval(substr($request_block_id, 10));
					/*
					print "<PRE>";
					print_r($_REQUEST);
					print "</PRE>";
					exit;
					*/
					$lid = $this->add_form(
						$request_form_name,
						$request_form_email,
						$request_captcha,
						$request_category,
						$request_field_types,
						$request_field_titles,
						$request_field_additional,
						$request_field_required,
						$request_field_text_type,
						$request_field_text_len,
						$ctg_rights, $block_id
					);
					if ($lid) {
						@$cache->delete_cache_files();
						header("Location: $baseurl&action=list&category=$request_category");
					} else {
						header("Location: $baseurl&action=showctg");
					}
					exit;
				}
				break;

// Редактирование записи
//-----------------------------
			case "edit":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category_adm($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_forms", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name."_edit.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					"MSG_Category" => $this->_msg["Category"],
					"MSG_Select_category" => $this->_msg["Select_category"],
					"MSG_Form_name" => $this->_msg["Form_name"],
					"MSG_Sending_email" => $this->_msg["Sending_email"],
					"MSG_Add_field" => $this->_msg["Add_field"],
					"MSG_you_can_see_fields" => $this->_msg["you_can_see_fields"],
					"MSG_Textfield" => $this->_msg["Textfield"],
					"MSG_for_string_255" => $this->_msg["for_string_255"],
					"MSG_Example" => $this->_msg["Example"],
					"MSG_Textarea" => $this->_msg["Textarea"],
					"MSG_for_big_text" => $this->_msg["for_big_text"],
					"MSG_Big_text" => $this->_msg["Big_text"],
					"MSG_Checkbox" => $this->_msg["Checkbox"],
					"MSG_for_flag_yes_no" => $this->_msg["for_flag_yes_no"],
					"MSG_Select_list" => $this->_msg["Select_list"],
					"MSG_for_radio_list" => $this->_msg["for_radio_list"],
					"MSG_List_value" => $this->_msg["List_value"],
					"MSG_Date" => $this->_msg["Date"],
					"MSG_for_date" => $this->_msg["for_date"],
					"MSG_Select" => $this->_msg["Select"],
					"MSG_Radio_list" => $this->_msg["Radio_list"],
					"MSG_Comment" => $this->_msg["Comment"],
					"MSG_for_comments" => $this->_msg["for_comments"],
					"MSG_Field_type" => $this->_msg["Field_type"],
					"MSG_Select_field_type" => $this->_msg["Select_field_type"],
					"MSG_Textfield" => $this->_msg["Textfield"],
					"MSG_Textarea" => $this->_msg["Textarea"],
					"MSG_Radio_list" => $this->_msg["Radio_list"],
					"MSG_Select_list" => $this->_msg["Select_list"],
					"MSG_Checkbox" => $this->_msg["Checkbox"],
					"MSG_Date" => $this->_msg["Date"],
					"MSG_Comment" => $this->_msg["Comment"],
					"MSG_File" => $this->_msg["File"],
					"MSG_Field_name_title" => $this->_msg["Field_name_title"],
					"MSG_Required" => $this->_msg["Required"],
					"MSG_Form_fields" => $this->_msg["Form_fields"],
					"MSG_Add_CAPTCHA_code" => $this->_msg["Add_CAPTCHA_code"],
					"MSG_required_fields" => $this->_msg["required_fields"],
					"MSG_Save" => $this->_msg["Save"],
					"MSG_Cancel" => $this->_msg["Cancel"],
					"MSG_Confirm_delete_this_field" => $this->_msg["Confirm_delete_this_field"],
					"MSG_Select_field_type" => $this->_msg["Select_field_type"],
					"MSG_Enter_field_name" => $this->_msg["Enter_field_name"],
					"MSG_Name_title" => $this->_msg["Name_title"],
					"MSG_Required" => $this->_msg["Required"],
					"MSG_Type_symbol_max_num" => $this->_msg["Type_symbol_max_num"],
					"MSG_List_elements" => $this->_msg["List_elements"],
					"MSG_each_element_with_br" => $this->_msg["each_element_with_br"],
					"MSG_Enter_sending_form_email" => $this->_msg["Enter_sending_form_email"],
					"MSG_Enter_form_name" => $this->_msg["Enter_form_name"],
					"MSG_Choose_category" => $this->_msg["Choose_category"],
					"_ROOT.form_action"	=>	"$baseurl&action=update&id=$request_id&start=$request_start",
					"_ROOT.cancel_link"	=>	"$baseurl&action=list&category=$request_category&start=$request_start",
				));
				if (!$this->show_form($request_id, $request_start)) { // если нет такой записи то редирект
					header("Location: $baseurl&action=showctg");
					exit;
				}
				$this->show_categories($request_category, true);
				$this->main_title = $this->_msg["Edit_form"];
				break;

// Обновление записи
			case "update":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_form_name, $request_form_email, $request_captcha, $request_category,
						$request_field_types, $request_field_titles, $request_field_additional,
						$request_field_required, $request_field_text_type, $request_field_text_len,
						$request_id, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category_adm($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_forms", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($this->update_form(
					$request_form_name,
					$request_form_email,
					$request_captcha,
					$request_category,
					$request_field_types,
					$request_field_titles,
					$request_field_additional,
					$request_field_required,
					$request_field_text_type,
					$request_field_text_len,
					$request_id
				)) {
					@$cache->delete_cache_files();
				}
				header("Location: $baseurl&action=list&category=$request_category&start=$request_start");
				exit;
				break;

// Удаление записи
			case "del":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category_adm($request_category, false);
				if (!$this->test_item_rights("d", "id", $this->table_prefix."_forms", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($this->delete_form($request_id)) {
					@$cache->delete_cache_files();
				}
				header("Location: $baseurl&action=list&category=$request_category&start=$request_start");
				exit;
				break;

// Отключение публикации
			case "suspend":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category_adm($request_category, false);
				if (!$this->test_item_rights("p", "id", $this->table_prefix."_forms", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($main->suspend($this->table_prefix."_forms", $request_id)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=list&category=$request_category&start=$request_start");
				exit;
				break;

// Включение публикации
			case "activate":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category_adm($request_category, false);
				if (!$this->test_item_rights("p", "id", $this->table_prefix."_forms", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($main->activate($this->table_prefix."_forms", $request_id)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=list&category=$request_category&start=$request_start");
				exit;
				break;


// Удалить выбранные формы
			case "delete_checked":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_category, $request_start;
				if (is_array($request_check)) {
					list($category_title, $ctg_owner_id) = $this->show_category_adm($request_category, false);
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("d", "id", $this->table_prefix."_forms", $v, false, $ctg_owner_id)) {
							if ($this->delete_form($v)) {
								@$cache->delete_cache_files();
							}
						}
					}
				}
				header("Location: $baseurl&action=list&category=$request_category&start=$request_start");
				exit;
				break;

// Отключить публикацию выбранных форм
			case "suspend_checked":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_category, $request_start;
				if (is_array($request_check)) {
					list($category_title, $ctg_owner_id) = $this->show_category_adm($request_category, false);
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $this->table_prefix."_forms", $v, false, $ctg_owner_id)) {
							if ($main->suspend($this->table_prefix."_forms", $v)) @$cache->delete_cache_files();
						}
					}
				}
				header("Location: $baseurl&action=list&category=$request_category&start=$request_start");
				exit;
				break;

// Включить публикацию выбранных форм
			case "activate_checked":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_category, $request_start;
				if (is_array($request_check)) {
					list($category_title, $ctg_owner_id) = $this->show_category_adm($request_category, false);
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $this->table_prefix."_forms", $v, false, $ctg_owner_id)) {
							if ($main->activate($this->table_prefix."_forms", $v)) @$cache->delete_cache_files();
						}
					}
				}
				header("Location: $baseurl&action=list&category=$request_category&start=$request_start");
				exit;
				break;


// Редактирование прав элемента галереи
			case 'editrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category_adm($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_forms", $request_id, true, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($this->table_prefix."_forms", "name", $request_id,
											"&id=".$request_id."&category=".$request_category."&start=".$request_start, $this->_msg["Form"])) {
					header('Location: '.$baseurl.'&action=list&category='.$request_category.'&start='.$request_start);
				}
				break;

// Сохранение прав элемента галереи
			case 'saverights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start, $request_rights;
				list($category_title, $ctg_owner_id) = $this->show_category_adm($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_forms", $request_id, true, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($this->table_prefix."_forms", $request_id, $request_rights);
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
						"res" => 1,
						"id" => $request_id,
					);
				} else {
					header('Location: '.$baseurl.'&action=list&category='.$request_category.'&start='.$request_start);
				}
				exit;
				break;


// Показать категории
//--------------------------
			case "showctg":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2($this->module_name."_categories.html", $this->tpl_path);
				$tpl->assignInclude("search_form", $tpl_path."_search_form.html");
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Empty_field" => $this->_msg["Empty_field"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Rights" => $main->_msg["Rights"],
					"MSG_Category" => $this->_msg["Category"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Create_new_category" => $this->_msg["Create_new_category"],
					"MSG_Create" => $this->_msg["Create"],
					"_ROOT.form_search_action"	=>	"$baseurl&action=search",
					"_ROOT.lang"	=>	$lang,
					"_ROOT.name"	=>	$this->module_name,
					"_ROOT.action"	=>	"search",
					"_ROOT.form_action"	=>	"$baseurl&action=addctg",
				));
				$res = $this->show_categories();
				if (!$res && isset($_REQUEST['empty'])) {
				    $tpl->newBlock('warning');
				    $tpl->assign('txt', 'Список категорий пуст.');
				}
				$this->main_title = $this->_msg["Form_categories"];
				break;

// Добавить категорию
			case "addctg":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_title, $request_block_id;
				if ($this->add_category($request_title, intval(substr($request_block_id, 10)))) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showctg");
				exit;
				break;

// Вывод категории для редактирования
			case "editctg":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_category;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_forms_categories", $request_category))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name."_edit_category.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Empty_field" => $this->_msg["Empty_field"],
					"MSG_Save" => $this->_msg["Save"],
					"MSG_Cancel" => $this->_msg["Cancel"],
				));
				$tpl->assign(
					array(
						"_ROOT.form_action"	=>	"$baseurl&action=updatectg",
						"_ROOT.cancel_link"	=>	"$baseurl&action=showctg",
					)
				);
				$this->show_category_adm($request_category);
				$this->main_title = $this->_msg["Edit_category"];
				break;

// Обновить категорию
			case "updatectg":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_title;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_forms_categories", $request_category))
					$main->message_access_denied($this->module_name, $action);

				if ($this->update_category($request_category, $request_title)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showctg");
				exit;
				break;

// Удалить категорию
			case "delctg":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_step, $request_category;
				if (!$this->test_item_rights("d", "id", $this->table_prefix."_forms_categories", $request_category))
					$main->message_access_denied($this->module_name, $action);

				// Удаление категории, первый шаг
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name."_del_category.html", $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(Array(
						"MSG_Deleting_category" => $this->_msg["Deleting_category"],
						"MSG_deletes_all_its_forms" => $this->_msg["deletes_all_its_forms"],
						"MSG_Confirm_delete_this_category" => $this->_msg["Confirm_delete_this_category"],
						"MSG_Delete" => $this->_msg["Delete"],
						"MSG_Cancel" => $this->_msg["Cancel"],
					));
					$tpl->assign(
						array(
							"_ROOT.form_action"	=>	"$baseurl&action=delctg&step=2&category=$request_category",
							"_ROOT.cancel_link"	=>	"$baseurl&action=showctg",
						)
					);
					$this->show_category_adm($request_category);
					$this->main_title = $this->_msg["Delete_category"];
				// Удаление категории, шаг второй
				} else if ($request_step == 2) {
					if ($this->delete_category($request_category)) @$cache->delete_cache_files();
					header("Location: $baseurl&action=showctg");
					exit;
				}
				break;

// Редактирование прав категории
			case 'editctgrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_start;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_forms_categories", $request_category, true))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($this->table_prefix."_forms_categories", "title", $request_category,
											"&category=".$request_category, $this->_msg["Category"], "savectgrights")) {
					header('Location: '.$baseurl);
				}
				break;

// Сохранение прав категории
			case 'savectgrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_start, $request_rights;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_forms_categories", $request_category, true))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($this->table_prefix."_forms_categories", $request_category, $request_rights);
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
						"res"	=> 1,
						"category"	=> $request_category,
					);
				} else {
					header('Location: '.$baseurl);
				}
				exit;
				break;


// Редактирование свойств блока
			case 'properties':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $module_id, $request_field, $request_block_id, $title;
				$main->include_main_blocks('_block_properties.html', 'main');
				$tpl->prepare();

				if ($main->show_actions($request_field, '', $this->block_main_module_actions, $this->block_module_actions)) {
					$main->show_linked_pages($module_id, $request_field);
				}
				$tpl->assign(array(	'_ROOT.title'		=> $title,
									'_ROOT.username'	=> $_SESSION['session_login'],
									'_ROOT.password'	=> $_SESSION['session_password'],
									'_ROOT.name'		=> $this->module_name,
									'_ROOT.lang'		=> $lang,
									'_ROOT.block_id'	=> $request_block_id,
									));
				$this->main_title	= $this->_msg["Block_properties"];
				break;

// Создание объекта для дальнейшего использования
			case 'only_create_object':
				break;

// Default...
			default:
				$main->include_main_blocks();
				$tpl->prepare();
			}
//--------------------------- конец обработки действий из админ части --------------------------//
		}
	}


// функция для вывода списка записей
	function show_forms($transurl, $category = "", $start = 1, $rows = "", $order_by = "") {
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;
    	if ('' == $order_by)	$order_by = $CONFIG["forms_order_by"];
		$rows		= (int)$rows;
		$start_row	= ($start - 1) * $rows;
		$category	= (int)$category;
		$ctg_str	= ($category) ? 'category_id = '.$category.' AND' : '';
		$db->query('SELECT * FROM '.$this->table_prefix.'_forms WHERE '.$ctg_str.' active = 1 ORDER BY '.$order_by.' LIMIT '.$start_row.','.$rows);
		if ($db->nf() > 0) {
			while($db->next_record()) {
				$tpl->newBlock("block_form_list");
				$tpl->assign(
					array(
						'form_name'		=> $db->f('name'),
						'form_email'	=> $db->f('email'),
						'form_link'		=> $transurl.'&action=show&id='.$db->f('id'),
						'form_id'		=> $db->f('id'),
					)
				);
			}
			return TRUE;
		}
		return FALSE;
	}


// функция для вывода записи по id
	function show_form($id = '', $start = '', $transurl = '') {
		$id = intval($id); if (!$id) return 0;
		global $CONFIG, $main, $db, $tpl, $baseurl, $server, $lang;
		$admin = Module::is_admin();
		if (!$admin) $active_str = 'AND active = 1';
		$this->get_list_with_rights(
				"f.*", $this->table_prefix."_forms f", "f",
				"id = ".$id.' '.$active_str);
		if ($db->nf() > 0) {
			$db->next_record();
			if ($admin) {
				$form_name	= htmlspecialchars($db->f('name'));
				$form_email	= htmlspecialchars($db->f('email'));
				$captcha	= htmlspecialchars($db->f('captcha'));
			} else {
				$form_name	= $db->f('name');
				$form_email	= $db->f('email');
				$captcha	= $db->f('captcha');
			}
			$form_fields_array = unserialize($db->f('fields_info'));
			if ($admin) {
				$this->show_form_fields_for_edit($form_name, $form_email, $captcha, $db->f('id'), $form_fields_array);
			} else {
				$this->show_form_fields($form_name, $captcha, $db->f('id'), $form_fields_array, $transurl);
			}
			return $form_name;
		}
		return FALSE;
	}


// функция для вывода полей формы
	function show_form_fields($form_name, $captcha, $id, $form_fields_array = array(), $transurl = "") {
		global $CONFIG, $tpl, $lang, $baseurl;
			$requied_fields = array();
			$fields_ids = array();
			$tag_form_name = "MyForm$id";
			$show_form_notice = false;
			static $date_fields;
			$text_types = array("text","email","number","phone");

		if (is_array($form_fields_array) && count($form_fields_array) > 0) {
			//$i = 0;
			$tpl->newBlock("block_form");
//			require_once("class.HTML_Form.php");
//			$form = new HTML_Form("${transurl}action=post", "post", "myForm$id");

			foreach ($form_fields_array as $idx => $arr) {
//				list($ftype, $fname, $ftitle, $freq, $fadd) = $arr;
				list($ftype, $ftitle, $freq, $fadd, $ftext_type, $ftext_len) = $arr;
				$fname = "form_fields$idx";
				$fields_ids[] = $idx;
				/*
				if ($freq) {
					$show_form_notice = true; // есть ли по крайней мере одно обязательное поле

					$form->addRequiredElement($ftype, $fname, $ftitle);
					$ftitle = $ftitle . $CONFIG["forms_required_label"];
				}*/

				if ($freq) {
					$freq = $CONFIG["forms_required_label"];
					$requied_fields[$fname] = $ftitle;
					$show_form_notice = true;
				} else {
					$freq = "";
				}

				$tpl->newBlock("block_form_element");
				switch ($ftype) {
					// текстовое поле
					case "text":
						$tpl->newBlock("block_text_element");
						$tpl->assign(
							array(
								element_name	=>	$fname,
								element_title	=>	$ftitle,
								element_required	=>	$freq,
								element_type	=>	$text_types[intval($ftext_type)],
								element_text_len	=>	(intval($ftext_len))?intval($ftext_len):255,
								element_value	=>	(isset($_REQUEST[$fname])) ? htmlspecialchars($_REQUEST[$fname],ENT_QUOTES):'',
							)
						);
						if ($freq) {
						    $tpl->newBlock("block_text_required");
						}
						break;


					// большое текстовое поле
					case "textarea":
						$tpl->newBlock("block_textarea_element");
						$tpl->assign(
							array(
								element_name		=>	$fname,
								element_title		=>	$ftitle,
								element_required	=>	$freq,
								element_value	=>	(isset($_REQUEST[$fname])) ? htmlspecialchars($_REQUEST[$fname],ENT_QUOTES):'',
							)
						);
						if ($freq) {
						    $tpl->newBlock("block_textarea_required");
						}
						break;


					// список
					case "radio":
						$tpl->newBlock("block_radio_element");
						$tpl->assign("element_title", $ftitle);

						if (is_array($fadd) && count($fadd) > 0) {
							$n = 0;
							foreach ($fadd as $i => $value) {
								$n++;
								$ftitle = $value;
								$checked = (($n == 100 && !isset($_REQUEST[$fname])) || (isset($_REQUEST[$fname]) && $_REQUEST[$fname]==$ftitle)) ? " checked" : "";

								$tpl->newBlock("block_radio_element_item");
								$tpl->assign(
									array(
										element_name	=>	$fname,
										element_item_id	=>	$fname . $n,
										element_item_title	=>	$ftitle,
										checked	=>	$checked,
									)
								);
							}
						}
						break;


					// выпадающий список
					case "select":
						$tpl->newBlock("block_select_element");
						$tpl->assign(
							array(
								element_name	=>	$fname,
								element_required	=>	$freq,
								element_title	=>	$ftitle,
							)
						);



						if (is_array($fadd) && count($fadd) > 0) {
							$n = 0;
							foreach ($fadd as $i => $value) {
								$n++; $ftitle = $value;
								$select = (($n == 1 && !isset($_REQUEST[$fname])) || (isset($_REQUEST[$fname]) && $_REQUEST[$fname]==$ftitle)) ? " selected" : "";
								$tpl->newBlock("block_select_element_item");
								$tpl->assign("element_item_title", $ftitle);
								$tpl->assign("element_name", $fname);
								$tpl->assign("element_id", $fname.$n);
								$tpl->assign("selected", $select);

							}
						}
						break;


					// флаг
					case "checkbox":
						$checked = (isset($_REQUEST[$fname])) ? " checked" : "";
						$tpl->newBlock("block_checkbox_element");
						$tpl->assign(
							array(
								element_name	=>	$fname,
								element_title	=>	$ftitle,
								element_checked	=>	$checked
							)
						);
						break;


					// дата
					case "date":
						$date_fields++;
						$tpl->newBlock("block_date_element");
						$tpl->assign(
							array(
								element_name	=>	$fname,
								element_title	=>	$ftitle,
								element_required	=>	$freq,
								trigger_uid	=>	"trigger" . $date_fields,
								element_value	=>	(isset($_REQUEST[$fname])) ? htmlspecialchars($_REQUEST[$fname], ENT_QUOTES) : "",
							)
						);
						if ($freq) {
						    $tpl->newBlock("block_date_required");
						}
						break;


					// комментарий
					case "comments":
						$tpl->newBlock("block_comments_element");
						$tpl->assign(
							array(
								element_comments	=>	$fadd,
								element_title	=>	$ftitle
							)
						);
						break;


					// загрузка файла
					case "file":
						$tpl->newBlock("block_file_element");
						$tpl->assign(
							array(
								element_name		=>	$fname,
								element_title		=>	$ftitle,
								element_required	=>	$freq,
							)
						);
						if ($freq) {
						    $tpl->newBlock("block_file_required");
						}
						break;
				}
//				$i++;
			}
			$script_html = $this->return_check_form_script($tag_form_name, $requied_fields);

			if($captcha == "1"){
				$tpl->newBlock("block_captcha");
				$tpl->assign(
					array(
						'captcha_image_url'		=>	"/mod/forms/captcha/image".($id == 12 ? "1" : "").".php?rand=" . rand(0, 100000),
						'captcha_maxlength'		=>	$CONFIG["captcha_wordlength"],
						'MSG_Enter_code_on'		=>	$this->_msg["Enter_code_on"],
						'MSG_image'				=>	$this->_msg["image"],
						'img_id'				=>	$id,
					)
				);
			}
			$tpl->gotoBlock('block_form');
			$tpl->assign(
				array(
					'form_name'		=> $form_name,
					'form_id'		=> $id,
					'form_url'		=> $transurl,
					'form_action'	=> 'post',
					'tag_form_name'	=> $tag_form_name,
					'ids'			=> implode(':', $fields_ids),
					'script_html'	=> $script_html,
				)
			);

			if ($show_form_notice) {
				$tpl->newBlock("block_form_notice");
				$tpl->assign(
					array(
						MSG_Marked_fields	=>	$this->_msg["Marked_fields"],
						element_required	=>	$CONFIG["forms_required_label"],
						MSG_required_for_fill	=>	$this->_msg["required_for_fill"],
					)
				);
			}
			return TRUE;
		}
		return FALSE;
	}


	function return_check_form_script($tag_form_name, $requied_fields = array()) {
		$str = "";
		$i = 0;

		if (count($requied_fields) > 0) {
			foreach ($requied_fields as $fname	=>	$ftitle) {
				$ftitle = addslashes($ftitle);
				$str .= ($i == 0) ? "if" : "else if";
				$str .= " (form.$fname.value == '') {SendItem= 1; AlertMessage = '" . $this->_msg["Fill_required_field"] . " \"$ftitle\"';}\n";
				$i++;
			}

			$str = "
				var SendItem= 0;
				var AlertMessage;

				$str

			  if (!SendItem) {
					form.submit();
				} else {
					alert(AlertMessage);
			  }";

		} else {
			$str = "form.submit();";
		}

		$str = "
		<script>
		function check$tag_form_name(form) {
			$str
		}
		</script>";

		return $str;
	}



	// функция для вывода полей формы для редактирования в админке
	function show_form_fields_for_edit($form_name, $form_email, $captcha, $id, $form_fields_array = array()) {
		global $CONFIG, $tpl, $lang;

		//вывод на экран
		if($captcha == "1")
			$captcha_checked = " checked";
		else
			$captcha_checked = "";
		$tpl->assign(
			array(
				form_name	=>	$form_name,
				form_email	=>	$form_email,
				captcha	=>	$captcha,
				captcha_checked	=>	$captcha_checked,
				form_id	=>	$id,
			)
		);

		/*
		print "<PRE>";
		print_r($form_fields_array);
		print "</PRE>";
		exit;
		*/
		if (is_array($form_fields_array) && count($form_fields_array) > 0) {
			$i = 0;
			foreach ($form_fields_array as $idx => $arr) {
				$i++;
//				list($ftype, $fname, $ftitle, $freq, $fadd) = $arr;
				list($ftype, $ftitle, $freq, $fadd, $text_type, $text_len) = $arr;

				$ftype_text = $this->types_array[$ftype];
				if (is_array($fadd)) {
					$tmp = implode("\n", $fadd); $fadd = $tmp;
				}
				$fadd = preg_replace("/[\r]/i", "", $fadd);
				$fadd = preg_replace("/[\n]/i", "\\n", $fadd);

				// заполнить javascript-массив данными о полях формы для последующего редактирования
				// (в шаблоне forms_edit.html)
				$tpl->newBlock("fill_array");
				$tpl->assign(
					array(
						item_type	=>	$ftype,
						item_type_text	=>	$ftype_text,
//						item_name	=>	htmlspecialchars($fname),
						item_title	=>	htmlspecialchars($ftitle),
						item_required	=>	($freq) ? "true" : "false",
						item_text_type	=>	intval($text_type),
						item_text_len	=>	intval($text_len) ? intval($text_len) : 255,
						item_additional	=>	htmlspecialchars($fadd),
						item_iterator	=>	$i,
						item_id	=>	$idx,
					)
				);
			}

			return 1;
		}

		return 0;
	}

	// функция для вывода информации о категории по id
	function show_category($transurl, $id = "") {
		$id = intval($id);
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;
		if (!$id) {
			$tpl->assign(
				array(
					category_link	=>	"#",
					category_title	=>	$this->_msg["Forms_list"],
				)
			);
			return 0;
		}
		$query = "SELECT * FROM ".$this->table_prefix."_forms_categories WHERE id = $id";
		$db->query($query);

		if ($db->nf() > 0) {
			$db->next_record();
			//вывод на экран
			$tpl->assign(
				array(
					category		=>	$db->f("id"),
					category_link	=>	str_replace("?","",$transurl),
					category_title	=>	$db->f("title"),
				)
			);
			return $db->f("title");
		}
		return 0;
	}

	// функция для вывода списка категорий
	function show_categories($selected_category = "", $only_editable = false) {
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;
		$selected_category = intval($selected_category);
		$this->get_list_with_rights(
				"C.*", $this->table_prefix."_forms_categories AS C", "C",
				"", "", "id");

		if ($db->nf() > 0) {
			$actions = array(	"edit"		=> "editctg",
								"editrights"=> "editctgrights",
								"delete"	=> "delctg",
								"confirm_delete"	=> ""
							);
			while($db->next_record()) {
				if ($only_editable && !($db->f("user_rights") & 2)) continue;

				// вывод на экран списка
				$id = $db->f('id');
				$adm_actions = $this->get_actions_by_rights($baseurl."&category=".$id, 0, $db->f("is_owner"), $db->f("user_rights"), $actions);
				$selected = ($db->f("id") == $selected_category) ? " selected" : "";
				$tpl->newBlock('block_categories');
				$tpl->assign(
					array(
						category_title		=>	$db->f("title"),
						category_link		=>	"$baseurl&action=list&category=".$id,
						category_edit_action=>	$adm_actions["edit_action"],
						category_edit_rights_action	=>	$adm_actions["edit_rights_action"],
						category_del_action	=>	$adm_actions["delete_action"],
						category			=>	$id,
						selected			=>	$selected,
					)
				);
			}
			return $db->nf();
		}
		return 0;
	}

	// отправка данных формы
	function mail_form_data($form_id, $form_fields) {

		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;

		$form_id = intval($form_id); if (!$form_id) return 0;
//		$form_name = mysql_real_escape_string($form_name);
		$result_str	= "";
		$success	= 0;
		$query		= "SELECT * FROM ".$this->table_prefix."_forms WHERE id = $form_id";
		$db->query($query);

		if ($db->nf() > 0) {
			$db->next_record();

			$form_id = $db->f("id");
			$form_name = $db->f("name");
			$form_email = $db->f("email");
			$form_fields_array = unserialize($db->f("fields_info"));
			$err_msg = "";
            $label_supra=false;
            if($_SERVER['HTTP_HOST'] == 'supra.ru'){
                $label_supra=true;
            }
            if($form_id == '11'){
                if(!$label_supra){
                    $form_email = 'shop@supra.ru';
                }
            }
			$attachmets = array();

			foreach ($form_fields as $idx => $data) {
				if ($form_fields_array[$idx]) {
//					list($ftype, $fname, $ftitle, $freq, $fadd) = $form_fields_array[$idx];
					list($ftype, $ftitle, $freq, $fadd, $ftext_type, $ftext_len) = $form_fields_array[$idx];
					if(is_array($data)) $data = implode(', ', $data);

					$data = strip_tags($data);
					if ($ftype == "checkbox") {
						$data = ($data) ? $this->_msg["yes"] : $this->_msg["no"];
					}
//					if (!$data) $data = "-";
				    if ($ftype=='file' && ($freq || is_uploaded_file($_FILES['form_fields'.$idx]['tmp_name']))) {
				    	if (($_FILES['form_fields'.$idx]['error'] > 0)
				    		|| ($freq && $_FILES['form_fields'.$idx]['size'] == 0)) {
    						$err_msg .= $this->_msg["Error_loading_file_from"] . " '".$ftitle."'.<br /> \r \n";
				    	} elseif (!$this->isTypeAllowed($_FILES['form_fields'.$idx]['name'])) {
				    	    unset($_FILES['form_fields'.$idx]['tmp_name']);
    						$err_msg .= $this->_msg["Error_loading_file_from"] . " '".$ftitle."'.<br /> \r \n";
				    	} else {
					    	$data = $_FILES['form_fields'.$idx]['name'];
					    	if (file_exists($this->file_path.$data)) {
					    		if (strrpos($data, ".") === false) {
					    		    $data = $data.'_'.time();
					    		} else {
					    		    $data = substr_replace($data, '_'.time().'.', strrpos($data, "."), 1);
					    		}
					    	}
							Main::checkPath($this->file_path);
					    	if (!move_uploaded_file($_FILES['form_fields'.$idx]['tmp_name'], $this->file_path.$data)) {
				    	        $err_msg .= $this->_msg["Error_loading_file_from"] . " '".$ftitle."'.<br /> \r \n";
				    	    } else {
				    	        $attachmets[] = $data;
				    	    }
				    	}
				    } elseif ($freq && '' === $data) {
						$err_msg .= $this->_msg["You_didnt_fill_field"] . " - '".$ftitle."'.<br /> \r \n";
				    }
				    $result_str .= "<br />$ftitle :\r\n $data \r\n ";
			    }
			}
			if ($err_msg) {
			    return $err_msg;
			}

			if ($result_str) {
			    $subject = $this->_msg["Filled_form"] . " '".$form_name."' на сайте " . $CONFIG["sitename_rus"]; //" \"$form_name\"
			    $bodys = $subject;
			    //$subject = iconv("utf-8", "WINDOWS-1251",$subject);   //Убрал, так как слетала кодировка
				$result_str = date($CONFIG["forms_date_format"]) . "
				$bodys
				" . $this->_msg["Data_sending_email"] . ": $form_email
				-----------------------------------------------------------------------
				$result_str";

        		$attachmets_str = "";
				for ($i=0; $i < sizeof($attachmets); $i++){
                    $attachmets_str		.= '<a href="'.$this->file_path.$attachmets[$i].'">'.$attachmets[$i].'</a><br>';
				}

				$query = "INSERT INTO ".$this->table_prefix."_forms_data (data, form_id) VALUES ('".$db->escape($result_str.$attachmets_str)."', $form_id)";
				$db->query($query);
				if ($db->affected_rows() > 0) {
				    $success = 1;
					$lid = $db->lid();
				}
  			    //$subject = convert_cyr_string($subject,"w","k"); //Убрал, так как слетала кодировка
	    		//$body = convert_cyr_string($body,"w","k");

		    	if (mail($form_email, $subject, $result_str, "From: " . $CONFIG["return_email"] . "\n" . $CONFIG["email_headers"])) {
			    	return TRUE;
				}

                $mail = new Mailer();
                $mail->CharSet		= $CONFIG['email_charset'];
                $mail->ContentType	= $CONFIG['email_type'];
        		$mail->From			= $CONFIG['return_email'];
        		//$mail->FromName		= $CONFIG['sitename_rus'];
        		$mail->Mailer		= 'mail';
                $mail->Subject		= $subject;
        		$mail->Body			= nl2br($result_str);
        		for ($i=0; $i < sizeof($attachmets); $i++){
                    $mail->AddAttachment($this->file_path.$attachmets[$i]);
				}

               	$mail->to[0][0] = $form_email;
               	if ($mail->Send()) return TRUE;
			}
		}
	    return FALSE;
    }

    /**
     * Проверка валидности типа файла $sFileName
     */
    function isTypeAllowed($sFileName) {
    	if ($this->DisallowedTypes == '') return TRUE;

    	if(preg_match("/".$this->DisallowedTypes."/i", $this->getExt($sFileName)) ) {
    		return FALSE;
    	} else {
    		return TRUE;
    	}
    }

    function getExt($sFileName) {
    	$sTmp=$sFileName;
    	while ($sTmp != '') {
    		$sTmp	= strstr($sTmp, '.');
    		if($sTmp != '') {
    			$sTmp	= substr($sTmp, 1);
    			$sExt	= $sTmp;
    		}
    	}
    	return strtolower($sExt);
    }

	function getPropertyFields() {
		GLOBAL $request_site_target, $request_sub_action;
		if ('' != $request_sub_action) {

			$tpl = $this->getTmpProperties();

			switch ($request_sub_action) {
				case 'show':
					$tpl->newBlock('block_properties');
					$tpl->assign(Array(
						"MSG_Form" => $this->_msg["Form"],
						"MSG_Select" => $this->_msg["Select"],
					));
					$list = $this->getForms('active');
					abo_str_array_crop($list);
					$tpl->assign_array('block_form_list', $list);
					$tpl->newBlock('block_page_edit');
					$tpl->assign(Array(
						"MSG_Goto_edit" => $this->_msg["Goto_edit"],
						"site_target" => $request_site_target,
					));
					break;

				default:
					$tpl->newBlock('block_properties_none');
					$tpl->assign(Array(
						"MSG_No" => $this->_msg["No"],
					));
					break;
			}
			return $this->getOutputContent($tpl->getOutputContent());
		}
		return FALSE;
	}

	function getEditLink($sub_action = NULL, $block_id = NULL) {
		GLOBAL $request_name, $db, $lang, $server;
		$block_id	= (int)$block_id;
		if ($sub_action && $block_id) {
			$db->query('SELECT	property1,
								property2,
								property3,
								property4,
								property5
							FROM '.$this->table_prefix.'_pages_blocks
							WHERE id = '.$block_id);
			if ($db->nf() > 0) {
				$db->next_record();
				$arr[]	= $db->f('property1');
				$arr[]	= $db->f('property2');
				$arr[]	= $db->f('property3');
				$arr[]	= $db->f('property4');
				$arr[]	= $db->f('property5');

				switch ($sub_action) {
					case 'show':
						$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=edit&menu=false&id=';
						$link['material_id']	= $arr[0];
						break;
				}
				return $link;
			}

			switch ($sub_action) {
				case 'show':
					$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=edit&menu=false&id=';
					$link['material_id']	= '';
					break;
			}
			return $link;
		}
		return FALSE;
	}

	// функция для добавления записи в базу данных
	function add_form(	$form_name, $form_email, $captcha, $category, $field_types = array(), $field_titles = array(),
						$field_additional = array(), $field_required = array(), $field_text_type = array(),
						$field_text_len = array(), $item_rights, $block_id = 0) {
		global $CONFIG, $db, $server, $lang;
		if (!$form_name) return FALSE; $form_name = $db->escape(trim($form_name));
		if (!$form_email) return 0; $form_email	= $db->escape(trim($form_email));
    	$category = intval($category); if (!$category) return 0;

		$db->query("SELECT usr_group_id FROM ".$this->table_prefix."_forms_categories WHERE id=".$category);
		if (!$db->next_record()) return false;
		$owner_id = $_SESSION["siteuser"]["id"];
		list($group_id, $rights) = $this->get_group_and_rights('forms_default_rights', $block_id, $db->f("usr_group_id"));
		$rights = $item_rights ? $item_rights : $rights;

		// полученный массив преобразуем в строку с помощью process_fields_string для помещения в одно поле БД
		$form_fields_string = $this->process_fields_string($field_types, $field_titles, $field_additional, $field_required, $field_text_type, $field_text_len);

		// сгенерим uid формы
		$form_uid = mktime(); $captcha = (int) $captcha;

		$query = "INSERT INTO ".$this->table_prefix."_forms (name, email, uid, category_id, fields_info, captcha, owner_id, usr_group_id, rights)
					VALUES ('$form_name', '$form_email', '$form_uid', $category, '$form_fields_string', '$captcha', ".$owner_id.", ".$group_id.", ".$rights.")";
		$db->query($query);

		return ($db->affected_rows() > 0) ? $db->lid() : 0;
	}


	// функция для обновления записи в базе данных
	function update_form($form_name, $form_email, $captcha, $category, $field_types = array(), $field_titles = array(), $field_additional = array(), $field_required = array(), $field_text_type = array(), $field_text_len = array(), $id = "") {
		global $CONFIG, $db, $server, $lang;
		$id = intval($id); if (!$id) return 0;
		if (!$form_name) return 0; $form_name = $db->escape(trim($form_name));
		if (!$form_email) return 0; $form_email = $db->escape(trim($form_email));
	    $category = intval($category); if (!$category) return 0;

		// полученный массив преобразуем в строку с помощью process_fields_string для помещения в одно поле БД
		$form_fields_string = $this->process_fields_string($field_types, $field_titles, $field_additional, $field_required, $field_text_type, $field_text_len);

		$query = "UPDATE ".$this->table_prefix."_forms SET name = '$form_name', email = '$form_email', fields_info = '$form_fields_string', captcha = '$captcha', category_id = $category WHERE id = $id";
		$db->query($query);

		return ($db->affected_rows() > 0) ? 1 : 0;
	}


	// функция для преобразования данных формы из массива в строку для записи в одно поле БД
	function process_fields_string($field_types = array(), $field_titles = array(), $field_additional = array(), $field_required = array(), $field_text_type = array(), $field_text_len = array()) {
		global $CONFIG, $lang, $db;

		if (!is_array($field_types) || count($field_types) == 0) return;
		$form_fields_array = array(); // массив, содержащий все поля формы и всю инфу по этим полям
																	// с помощью мега-функции serialize будет жестко запихан в одно поле БД
		foreach ($field_types as $idx => $ftype) {
			$ftitle = $field_titles[$idx]; // заголовок поля
			$freq = intval($field_required[$idx]); // обязательность поля
			$fadd = ""; // дополнительно
			$ftext_type = 0; // тип текста в текстовом поле
			$ftext_len = 0; // длина текста в текстовом поле

			if ($ftype) {
				switch ($ftype) {
					// текстовое поле
					case "text":
						$ftext_type = intval($field_text_type[$idx]);
						$ftext_len = intval($field_text_len[$idx]);
						break;


					// большое текстовое поле
					case "textarea":
						break;


					// список
					case "radio":
						// обработать доп. поле для создания списка значений
						$tmp = explode("\n", $field_additional[$idx]);
						if (is_array($tmp) && count($tmp) > 0) {
							$fadd = array();
							foreach ($tmp as $i => $var) {
								$var = preg_replace("/[\r\n]/", "", $var);
								if ($var) {
									$fadd[] = $var;
								}
							}
						}
						break;


					// выпадающий список
					case "select":
						// обработать доп. поле для создания списка значений
//						$fadd = $field_additional[$idx];
						$tmp = explode("\n", $field_additional[$idx]);
						if (is_array($tmp) && count($tmp) > 0) {
							$fadd = array();
							foreach ($tmp as $i => $var) {
								$var = preg_replace("/[\r\n]/", "", $var);
								if ($var) {
									$fadd[$var] = $var;
								}
							}
						}
						break;


					// флаг
					case "checkbox":
						break;


					// дата
					case "date":
						break;

					// комментарий
					case "comments":
						$fadd = $field_additional[$idx];
						break;
				}
				$form_fields_array[$idx] = array($ftype, $ftitle, $freq, $fadd,	$ftext_type, $ftext_len);
			}
		}
//		$i++;

		/*
		print "<PRE>";
		print_r($form_fields_array);
		print "</PRE>";
		exit;
		*/

		// полученный массив преобразуем в строку с помощью serialize для помещения в одно поле БД
		$form_fields_string = $db->escape(serialize($form_fields_array));

		return $form_fields_string;
	}


	// функция для удаления записи из базы данных
	function delete_form($id) {
		global $CONFIG, $db, $server, $lang;
		$id = intval($id); if (!$id) return 0;

		$query = "DELETE FROM ".$this->table_prefix."_forms WHERE id = $id";
		$db->query($query);

		if ($db->affected_rows() > 0) {

			$query = "DELETE FROM ".$this->table_prefix."_forms_data WHERE form_id = $id";
			$db->query($query);

			return 1;
		}

		return 0;
	}


	// функция для удаления записи из базы данных
	function delete_form_data($id) {
		global $CONFIG, $db, $server, $lang;
		$id = intval($id); if (!$id) return 0;

		$query = "DELETE FROM ".$this->table_prefix."_forms_data WHERE id = $id";
		$db->query($query);

		return ($db->affected_rows() > 0) ? 1 : 0;
	}


	// вывод списка записей
	function show_forms_adm($type, $category = "", $start = "", $search = "", $owner_id = 0) {
		global $CONFIG, $main, $db, $tpl, $baseurl, $server, $lang;
		$order_by = $CONFIG["forms_admin_order_by"];

		$pages_cnt = 1;
		if ($type == "pages") {
			// постраничный список
			$category = intval($category); if (!$category) return 0;
			$start = intval($start); $start = ($start) ? $start : 1;
			$rows = $CONFIG["forms_max_rows"];
			$total_cnt = $this->get_list_with_rights(
									"f.*", $this->table_prefix."_forms f", "f",
									"category_id = ".$category, "", $order_by,
									$start, $rows,
									true, $owner_id);
			$pages_cnt = ceil($total_cnt/$rows);

		} else if ($type == "all") {
			// все записи в один список
			$this->get_list_with_rights(
						"f.*, fc.title as ctitle",
						array("f" => $this->table_prefix."_forms", "fc" => $this->table_prefix."_forms_categories"), "f",
						"f.category_id = fc.id", "", "category_id, ".$order_by,
						1, 0, false, $owner_id);

		} else if ($type == "active") {
			// все записи в один список
			$this->get_list_with_rights(
						"f.*, fc.title as ctitle",
						array("f" => $this->table_prefix."_forms", "fc" => $this->table_prefix."_forms_categories"), "f",
						"f.active = 1 AND f.category_id = fc.id", "", "category_id, ".$order_by,
						1, 0, false, $owner_id);

		} else if ($type == "search") {
			// поиск
			if (!$search || strlen($search) < 2) return 0;
			$search = $db->escape($search);
			$query = "SELECT * FROM ".$this->table_prefix."_forms
						WHERE name LIKE '%$search%' OR email LIKE '%$search%' OR id = '$search'
						GROUP BY id
						ORDER BY $order_by";
			$this->get_list_with_rights(
						"f.*", $this->table_prefix."_forms f", "f",
						"name LIKE '%$search%' OR email LIKE '%$search%' OR id = '$search'", "id", $order_by,
						1, 0, false, $owner_id);

		}
		$db->query($query);


		if ($db->nf() > 0) {
			$actions = array(	"edit"		=> "edit",
								"editrights"=> "editrights",
								"activate"	=>	array("activate","suspend"),
								"delete"	=> "del",
								"confirm_delete"	=> $this->_msg["Confirm_delete_record"]
							);
			while($db->next_record()) {
				// вывод на экран списка
				$id = $db->f('id');
				$category_id = $db->f('category_id');
				$adm_actions = $this->get_actions_by_rights($baseurl."&id=".$id."&category=".$category_id."&start=".$start, $db->f("active"), $db->f("is_owner"), $db->f("user_rights"), $actions);
				$tpl->newBlock("block_form_list");
				$tpl->assign(
					array(
					'form_id'				=>	$id,
					'form_name'				=>	htmlspecialchars($db->f("name")),
					'form_ctitle_and_name'	=>	htmlspecialchars($db->f("ctitle").": ".$db->f("name")),
					'form_email'			=>	$db->f("email"),
					'form_hits'				=>	$db->f("hits"),
					'form_data_link'		=>	$baseurl."&action=datalist&id=".$id,
					'form_edit_action'		=>	$adm_actions["edit_action"],
					'form_edit_rights_action'=>	$adm_actions["edit_rights_action"],
					'form_del_action'		=>	$adm_actions["delete_action"],
					'change_status_action'	=>	$adm_actions["change_status_action"],
					'start'					=>	$start,
				));
				if ($adm_actions["rights"]["d"] || $adm_actions["rights"]["p"]) {
					$tpl->newBlock('block_check');
					$tpl->assign("form_id", $id);
				}
			}
			return $pages_cnt;
		}
		return FALSE;
	}

		// вывод списка записей
	function getForms($type, $category = '', $start = '', $search = '') {
		global $CONFIG, $main, $db, $tpl, $baseurl, $server, $lang;
		$order_by = $CONFIG["forms_admin_order_by"];

		if ($type == "pages") {
			$category	= (int)$category; if (!$category) return FALSE;
			$start		= intval($start); $start = ($start) ? $start : 1;
			$rows		= $CONFIG['forms_max_rows'];
			$start_row	= ($start - 1) * $rows;
			$query		= "SELECT * FROM ".$this->table_prefix."_forms
								WHERE category_id = $category
								ORDER BY $order_by
								LIMIT $start_row,$rows";

		} else if ($type == "all") {
			// все записи в один список
			$query = "SELECT ".$this->table_prefix."_forms.*, ".$this->table_prefix."_forms_categories.title as ctitle
						FROM ".$this->table_prefix."_forms,
							 ".$this->table_prefix."_forms_categories
						WHERE ".$this->table_prefix."_forms_categories.id = ".$this->table_prefix."_forms.category_id
						ORDER BY category_id, $order_by";

		} else if ($type == "active") {
			// все записи в один список
			$query = "SELECT ".$this->table_prefix."_forms.*, ".$this->table_prefix."_forms_categories.title as ctitle
						FROM ".$this->table_prefix."_forms, ".$this->table_prefix."_forms_categories
						WHERE ".$this->table_prefix."_forms.active = 1 AND
							  ".$this->table_prefix."_forms_categories.id = ".$this->table_prefix."_forms.category_id
						ORDER BY category_id, $order_by";

		} else if ($type == "search") {
			// поиск
			if (!$search || strlen($search) < 2) return 0;
			$search = $db->escape($search);
			$query = "SELECT * FROM ".$this->table_prefix."_forms
						WHERE name LIKE '%$search%' OR email LIKE '%$search%' OR id = '$search'
						GROUP BY id
						ORDER BY $order_by";

		}
		$db->query($query);


		if ($db->nf() > 0) {
			while($db->next_record()) {
				$change_status	= ($db->f('active')) ? 'suspend'				: 'activate';
				$action_text	= ($db->f('active')) ? 'отключить публикацию'	: 'включить публикацию';
				$action_img		= ($db->f('active')) ? 'ico_swof.gif'			: 'ico_swon.gif';
				$arr[]			= array('form_id'				=> $db->f('id'),
										'form_name'				=> $db->f('name'),
										'form_ctitle_and_name'	=> $db->f('ctitle').': '.$db->f('name'),
										'form_email'			=> $db->f('email'),
										'form_data_link'		=> $baseurl.'&action=datalist&id='.$db->f('id'),
										'form_edit_link'		=> $baseurl.'&action=edit&id='.$db->f('id').'&category='.$db->f('category_id').'&start='.$start,
										'form_del_link'			=> $baseurl.'&action=del&id='.$db->f('id').'&category='.$db->f('category_id').'&start='.$start,
										'change_status_link'	=> $baseurl.'&action='.$change_status.'&id='.$db->f('id').'&category='.$db->f('category_id').'&start='.$start,
										'action_text'			=> $action_text,
										'action_img'			=> $action_img,
										'start'					=> $start,
										);
			}
			return $arr;
		}
		return FALSE;
	}


	// функция для вывода списка заполненных форм
	function show_data_list($type, $id = "", $start = "", $search = "") {
		global $CONFIG, $main, $db, $tpl, $baseurl, $server, $lang;
		$order_by = $CONFIG["forms_admin_order_by"];

		if ($type == "pages") {
			// постраничный список
			$id = intval($id); if (!$id) return 0;
			$start = intval($start); $start = ($start) ? $start : 1;
			$rows = $CONFIG["forms_max_rows"];
			$total_cnt = $this->get_list_with_rights(
										"fd.*", array("fd" => $this->table_prefix."_forms_data", "f" => $this->table_prefix."_forms"), "f",
										"fd.form_id = ".$id." AND fd.form_id = f.id", "", $order_by,
										$start, $rows, true);
			$pages_cnt = ceil($total_cnt/$rows);

		} else if ($type == "search") {
			// поиск
			if (!$search || strlen($search) < 2) return 0;
			$search = $db->escape($search);
			$this->get_list_with_rights(
						"fd.*", array("fd" => $this->table_prefix."_forms_data", "f" => $this->table_prefix."_forms"), "f",
						"data LIKE '%$search%' AND fd.form_id = f.id", "fd.id", $order_by);
			$pages_cnt = 1;

		}

		if ($db->nf() > 0) {
			$actions = array(	"delete"	=> "deldata",
								"confirm_delete"	=> $this->_msg["Confirm_delete_record"]
							);
			while($db->next_record()) {
				// вывод на экран списка
				$adm_actions = $this->get_actions_by_rights($baseurl."&data_id=".$db->f("id")."&id=".$id."&start=".$start, $db->f("active"), $db->f("is_owner"), $db->f("user_rights"), $actions);
				$tpl->newBlock("block_form_datalist");
				$tpl->assign(
					array(
					form_data_id	=>	$db->f("id"),
					form_id	=>	$id,
					form_data	=>	nl2br($db->f("data")),
					form_data_del_action	=>	$adm_actions["delete_action"],
					start	=>	$start,
					)
				);
			}
			return $pages_cnt;
		}
		return 0;
	}


	// функция для вывода категории по id
	function show_category_adm($id = "", $need_output = true) {
		$id = intval($id);		if (!$id) return 0;
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;

		$query = "SELECT * FROM ".$this->table_prefix."_forms_categories WHERE id = $id";
		$db->query($query);

		if ($db->nf() > 0) {
			$db->next_record();
			$title = htmlspecialchars($db->f("title"));

			if ($need_output) {
				//вывод на экран
				$tpl->assign(
					array(
						category	=>	$db->f("id"),
						category_title	=>	$title,
						category_link	=>	"$baseurl&action=list&category=" . $db->f("id"),
					)
				);
			}

			return array($db->f("title"), $db->f("owner_id"));
		}
		return 0;
	}

	// функция для добавления категории
	function add_category($title, $block_id = 0) {
		global $CONFIG, $db, $server, $lang;
		$title = $db->escape(trim($title));
		if (!$title) return FALSE;

		$owner_id = $_SESSION["siteuser"]["id"];
		list($group_id, $rights) = $this->get_group_and_rights('forms_default_rights', $block_id);

		$query = "INSERT INTO ".$this->table_prefix."_forms_categories (title,owner_id,usr_group_id,rights)
					VALUES ('$title',$owner_id,$group_id,$rights)";
		$db->query($query);
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}


	// функция для обновления категории по id
	function update_category($id, $title) {
		global $CONFIG, $db, $server, $lang;
		$id = intval($id); if (!$id) return 0;
		if (!$title) return 0;
		$title = $db->escape($title);

		$query = "UPDATE ".$this->table_prefix."_forms_categories SET title = '$title' WHERE id = $id";
		$db->query($query);

		return ($db->affected_rows() > 0) ? 1 : 0;
	}


	// функция для удаления категории по id
	function delete_category($id = "") {
		$id = intval($id);		if (!$id) return 0;
		global $CONFIG, $db, $tpl, $server, $lang;
		$query = "DELETE FROM ".$this->table_prefix."_forms_categories WHERE id = $id";
		$db->query($query);
		if ($db->affected_rows() > 0) {
			$query = "DELETE FROM ".$this->table_prefix."_forms WHERE category_id = $id";
			$db->query($query);
			return 1;
		}
		return 0;
	}
}

?>