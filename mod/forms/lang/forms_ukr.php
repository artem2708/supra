<?php
$_MSG = Array(
	"forms"							=>	"Формы", 

	"Form_categories"				=>	"Категории форм", 
	"Add_form"						=>	"Добавить форму", 

	"Sending_form_data"				=>	"Отсылка данных формы",
	"Fill_required_field"			=>	"Заполните обязательное поле",
	"Forms_list"					=>	"Список форм",
	"yes"							=>	"да", 
	"no"							=>	"нет",
	"Error_loading_file_from"		=>	"Ошибка загрузки файла из поля", 
	"You_didnt_fill_field"			=>	"Вы не заполнили поле",
	"Filled_form"					=>	"Заполнена форма", 
	"Data_sending_email"			=>	"E-mail отсылки данных", 
	"Textfield"						=>	"Текстовое поле",
	"Textarea"						=>	"Большое текстовое поле", 
	"Radio_list"					=>	"Список",
	"Select_list"					=>	"Выпадающий список", 
	"Checkbox"						=>	"Флаг", 
	"Date"							=>	"Дата",
	"Comment"						=>	"Примечание", 
	"File"							=>	"Файл", 
	"Show_form"						=>	"Показать форму",
	"Controls"						=>	"Настройки", 
	"Filled_form_data"				=>	"Данные заполненной формы",
	"Didnt_match_anything"			=>	"Ничего не найдено", 
	"Search_results"				=>	"Результаты поиска",
	"Search_results_in_forms"		=>	"Результаты поиска в формах", 
	"Edit_form"						=>	"Редактировать форму",
	"Edit_category"					=>	"Редактировать категорию", 
	"Delete_category"				=>	"Удаление категории",
	"Block_properties"				=>	"Свойства блока",
	"Form"							=>	"Форма",
	"Select"						=>	"Выбрать", 
	"Goto_edit"						=>	"Перейти к редактированию", 
	"No"							=>	"Нет", 
	"Empty_field"					=>	"Пустое поле!", 
	"Ed"							=>	"Ред.", 
	"Category"						=>	"Категория", 
	"Del"							=>	"Удл.", 
	"Edit"							=>	"Редактировать", 
	"Delete"						=>	"Удалить", 
	"Create_new_category"			=>	"Создать новую категорию", 
	"Create"						=>	"Создать", 
	"Category_ID"					=>	"ID категории", 
	"is"							=>	"равен", 
	"Text"							=>	"Текст", 
	"Confirm_delete_record"			=>	"Вы действительно хотите удалить эту запись?", 
	"Pages"							=>	"Страницы", 
	"Deleting_category"				=>	"Удаление категории", 
	"deletes_all_its_forms"			=>	"приведет к удалению всех связанных с ней форм!", 
	"Confirm_delete_this_category"	=>	"Вы действительно хотите удалить эту категорию?", 
	"Cancel"						=>	"Отменить", 
	"Select_category"				=>	"Выбрать категорию",
	"Form_name"						=>	"Название формы",
	"Sending_email"					=>	"E-mail отправки",
	"Add_field"						=>	"Добавить поле",
	"you_can_see_fields"			=>	"здесь можно посмотреть типы полей и пояснения к ним", 
	"for_string_255"				=>	"служит для введения строки длиной не более 255 символов", 
	"Example"						=>	"Пример", 
	"String_no_more_than_255"		=>	"Строка не более 255 символов...", 
	"for_big_text"					=>	"служит для введения большого объема текста", 
	"Select_field_type"				=>	"Выбрать тип поля", 
	"Field_type"					=>	"Тип поля", 
	"Big_text"						=>	"Большой объем текста...", 
	"for_flag_yes_no"				=>	"служит для создания элемента типа да/нет", 
	"for_radio_list"				=>	"служит для выбора значения из предложенного списка", 
	"List_value"					=>	"Значение списка", 
	"for_date"						=>	"служит для введения даты", 
	"for_comments"					=>	"служит для разделения полей формы на логические блоки", 
	"Field_name_title"				=>	"Название/заголовок поля", 
	"Required"						=>	"Обязательное для заполнения", 
	"Add_field"						=>	"Добавить поле", 
	"Form_fields"					=>	"Поля формы",
	"Add_CAPTCHA_code"				=>	"Добавить CAPTCHA-проверку", 
	"required_fields"				=>	"поля, обязательные для заполнения", 
	"Save"							=>	"Сохранить",
	"Confirm_delete_this_field"		=>	"Вы действительно хотите удалить это поле?", 
	"Enter_field_name"				=>	"Укажите название поля", 
	"Name_title"					=>	"Название/заголовок", 
	"Type_symbol_max_num"			=>	"Тип / макс. кол. симв.", 
	"List_elements"					=>	"Элементы списка", 
	"each_element_with_br"			=>	"каждый элемент с новой строки", 
	"Up"							=>	"Вверх", 
	"Down"							=>	"Вниз", 
	"Enter_sending_form_email"		=>	"Укажите email отправки данных формы",
	"Enter_form_name"				=>	"Укажите название формы", 
	"Choose_category"				=>	"Выберите категорию", 
	"Activ"							=>	"Публ.",
	"Name"							=>	"Название",
	"Recipient_email"				=>	"E-mail получателя", 
	"Shows"							=>	"Просмотры", 
	"Select_action"					=>	"Выберите действие",
	"Activate"						=>	"Включить публикацию", 
	"Suspend"						=>	"Выключить публикацию", 
	"Execute"						=>	"Выполнить",
	"Pages"							=>	"Страницы",
	"Marked_fields"					=>	"Поля, помеченные",
	"required_for_fill"				=>	"обязательны для заполнения",
	"Enter_code_on"					=>	"Введите код, указанный",
	"image"							=>	"на картинке",
);
?>