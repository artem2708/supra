<?php // Vers 5.8.2 27.06.2012
/**
 * индекс type задёт тип значения, возможные значения:
 *	integer  - целое число
 *	double   - вещественное число
 *	string   - строка
 *	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
 *	select   - выпадающий список
 * индекс defval задаёт значение по умоляанию,
 * индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
 * индекс access устанавливает уровень доступа, возможные значения
 * 	public	- элемент доступен для чтения/изменения
 *	final	- элемент доступен только для чтения
 *	private - элемент не доступен
 */
$TYPES_NEW = array(
    'forms_order_by'			=> array('type'		=> 'select',
        'defval'	=> array('id DESC'	=> array(	'rus' => 'В порядке обратном добавлению',
            'eng' => 'Reverce adding order'),
            'id'		=> array(	'rus' => 'В порядке добавлению',
                'eng' => 'Adding order'),
        ),
        'descr'	=> array(	'rus' => 'Порядок вывода записей на сайте',
            'eng' => 'Order of record displaying on site'),
        'access'	=> 'editable',
    ),

    'forms_admin_order_by'	=> array('type'		=> 'select',
        'defval'	=> array('id DESC'	=> array(	'rus' => 'В порядке обратном добавлению',
            'eng' => 'Reverce adding order'),
            'id'		=> array(	'rus' => 'В порядке добавлению',
                'eng' => 'Adding order'),
        ),
        'descr'	=> array(	'rus' => 'Порядок вывода записей в адм. интерфейсе',
            'eng' => 'Order of record displaying on admin part'),
        'access'	=> 'editable'
    ),

    'forms_max_rows' 		=> array('type'		=> 'integer',
        'defval'	=> '10',
        'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу',
            'eng' => 'Maximum quantity of records on page'),
        'access'	=> 'editable'
    ),

    'forms_required_label' 	=> array('type'		=> 'string',
        'defval'	=> '<sup>*</sup>',
        'descr'	=> array(	'rus' => 'Метка поля требующего заполнения',
            'eng' => 'Label of a field of demanding filling'),
        'access'	=> 'editable'
    ),

    'forms_count_hits'     	=> array('type'		=> 'checkbox',
        'defval'	=> 'on',
        'descr'	=> array(	'rus' => 'Учитывать статистику отображения контента',
            'eng' => 'to consider statistics of display of a content'),
        'access'	=> 'editable'
    ),

    'forms_default_rights'	=> array('type'		=> 'string',
        'defval'	=> '111111110001',
        'descr'	=> array(	'rus' => 'Права для форм по-умолчанию (формат <img src="/i/admin/rights_format.gif" width=72 height=18 alt="формат прав">, где x - 0 или 1, каждая группа представляет набор прав: удаление, публикация, редактирование, чтение)',
            'eng' => 'Forms default rights (format <img src="/i/admin/rights_format.gif" width=72 height=18 alt="rights format">, where x - 0 or 1, every group is rights set: delete, publication, edit, read)'),
        'access'	=> 'editable'
    ),

);

$CONFIG["forms_captcha_width"] = 110;
$CONFIG["forms_captcha_height"] = 32;
$CONFIG["forms_captcha_secretword"] = "SecRetABOCMSwORd";
$CONFIG["forms_captcha_fontsize"] = 16;
$CONFIG["forms_captcha_fontdepth"] = 2;
$CONFIG["forms_captcha_symbols"] = "0123456789";
$CONFIG["forms_captcha_wordlength"] = 5;
$CONFIG["forms_captcha_fonts"] = array(
    "arialbd.ttf",
    "comicbd.ttf",
    "courbd.ttf",
    "timesbd.ttf",
);
$CONFIG["forms_captcha_bgcolor"] = array(98, 118, 144);
$CONFIG["forms_captcha_color"] = array(rand(190, 210), rand(200, 220), rand(30, 50));
$CONFIG["forms_captcha_color2"] = array(rand(150, 170), rand(50, 70), rand(140, 160));

$OPEN_NEW = array(
    'forms_date_format' => 'd/m/Y G:i:s',
);

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
    /* ABOCMS:START */
    'forms_order_by' => 'id DESC',
    'forms_admin_order_by' => 'id DESC',
    'forms_max_rows' => '10',
    'forms_required_label' => ' <sup>*</sup>',
    'forms_count_hits' => 'on',
    'forms_default_rights' => '111111110001',
    /* ABOCMS:END */
);
?>