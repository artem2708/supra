<?php

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_forms` (
  `id` int(11) NOT NULL auto_increment,
  `category_id` smallint(5) unsigned NOT NULL default '0',
  `uid` varchar(50) NOT NULL default '',
  `name` varchar(255) NOT NULL default '',
  `email` text NOT NULL,
  `fields_info` text,
  `captcha` enum('0','1') NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  `hits` bigint(20) unsigned NOT NULL default '0',
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_forms_categories` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_forms_data` (
  `id` int(11) NOT NULL auto_increment,
  `form_id` smallint(5) unsigned NOT NULL default '0',
  `data` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

?>