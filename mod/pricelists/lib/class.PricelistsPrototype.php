<?php

class PricelistsPrototype extends Module {

	var $main_title;
	var $table_prefix;
	var $addition_to_path;
	var $module_name				= 'pricelists';
	var $default_action				= 'showpricelists_list';
	var $admin_default_action		= 'showpricelists';
	var $tpl_path					= 'mod/pricelists/tpl/';
	var $block_module_actions		= array();
	var $block_main_module_actions	= array();

	function PricelistsPrototype($action = '', $transurl = '', $properties = array(), $prefix = NULL, $rewrite_mod_params = NULL, $adminurl = NULL, $noadmin = false) {
		global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang, $permissions, $request_type, $_RESULT;
		
		/* Get $action, $tpl_name, $permissions */
		extract( $this->init(array(
		        'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
		        'action' => $action, 'tpl_name' => $properties['tpl']
		)), EXTR_OVERWRITE);
		
		$id	= (int)$properties[1];
		
		if ('only_create_object' == $action) {
		    return;
		}
		
		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}

		if (! self::is_admin()) {
		    
//---------------------------------- обработка действий с сайта --------------------------------//
			switch ($action) {
// Вывод статьи
			case "show":
				global $request_id;
				$id = $request_id > 0 ? (int)$request_id : $id;
				$main->include_main_blocks($this->module_name . ".html", "main");
				$tpl->prepare();
				$this->show_pricelist($transurl, $id);
				break;

			case "showpricelists_list":
				$main->include_main_blocks($this->module_name . "_list.html", "main");
				$tpl->prepare();
				$this->show_pricelists_list($transurl);
				break;

			case "order":
				$main->include_main_blocks($this->module_name . "_order.html", "main");
				$tpl->prepare();
				$this->order();
				break;

// Default...
			default:
				if (is_object($tpl)) $tpl->prepare();
				return;
			}
//------------------------------- конец обработки действий с сайта -----------------------------//
		} else {
//------------------------------ обработка действий из админ части -----------------------------//

		    $this->block_module_actions		= array(
		            'showpricelists_list' => $this->_msg["Show_pricelists_list"],
		            'show'	  => $this->_msg["Show_pricelist"],
  	        );
  	        $this->block_main_module_actions	= array(
  	                'showpricelists_list' => $this->_msg["Show_pricelists_list"],
  	                'show'	  => $this->_msg["Show_pricelist"],
  	        );
  	        
			if (!isset($tpl)) {
			    $main->message_die(iconv("WINDOWS-1251", "UTF-8", 'Не подключен класс для работы с шаблонами'));
			}

			switch ($action) {
		    case 'prp':
		    	if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step;
				$main->include_main_blocks('_properties.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Information"	=> $main->_msg["Information"],
					"MSG_Value"			=> $main->_msg["Value"],
					"MSG_Save"			=> $main->_msg["Save"],
					"MSG_Cancel"		=> $main->_msg["Cancel"],
				));
		        if (!$request_step || $request_step == 1) {
					$tpl->assign(array('form_action'	=> $baseurl,
									   'step'			=> 2,
									   'lang'			=> $lang,
									   'name'			=> $this->module_name,
									   'action'			=> 'prp'));
					$prp_html = $main->getModuleProperties($this->module_name);
		            $this->main_title = $this->_msg["Controls"];
				} elseif (2 == $request_step) {
		        	$main->setModuleProperties($this->module_name, $_POST);
					header('Location: '.$baseurl.'&action=prp&step=1');
				}
		    	break;

		    case 'block_prp':
				global $request_sub_action, $request_block_id;
		    	$arr	= $this->getEditLink($request_sub_action, $request_block_id);
		    	$cont = 'var container = document.getElementById(\'target_span\');
		    		  	 container.innerHTML = "'.$this->getPropertyFields().'";
		    		  	 material_id		  = "'.$arr['material_id'].'";
		    		  	 material_url		  = "'.$arr['material_url'].'";
		    		  	 changeAction();
		    	      	 setURL("property1");';
		    	header('Content-Length: '.strlen($cont));
		    	echo $cont;
		    	exit;
		    	break;

			case "showpricelists":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_start;
				$start = ($request_start) ? (int)$request_start : 1;
				$main->include_main_blocks_2($this->module_name.'_list.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Rights" => $main->_msg["Rights"],
					"MSG_Pricelist_name" => $this->_msg["Pricelist_name"],
					"MSG_Del" => $this->_msg["Del"],
				));
/*				$this->show_upload_forms();
				$tpl->assign(array(	'_ROOT.form_search_action'	=> $baseurl.'&action=search',
									'_ROOT.lang'				=> $lang,
									'_ROOT.name'				=> $this->module_name,
									'_ROOT.action'				=> 'search',
					)
				);
*/
				$pages_cnt = $this->show_pricelists_list_adm('block_pages_list', "pages", $start);
				$main->_show_nav_block($pages_cnt, null, 'action='.$action, $start);
				$tpl->assign(Array(
					"MSG_Pages" => $this->_msg["Pages"],
				));
				$this->main_title = $this->_msg["pricelists"];
				break;


// Добавление нового расстояния в базу данных
			case 'add':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step, $request_text;
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path);
					$tpl->prepare();
					$this->show_upload_forms();
					$tpl->assign(array(
						"MSG_Enter_pricelist_name" => $this->_msg["Enter_pricelist_name"],
						"MSG_Pricelist_name" => $this->_msg["Pricelist_name"],
						"MSG_Download_pricelist_CSV" => $this->_msg["Download_pricelist_CSV"],
						"MSG_Fields_order_in_users_CSV" => $this->_msg["Fields_order_in_users_CSV"],
						"MSG_separator" => $this->_msg["separator"],
						"MSG_required_fields" => $this->_msg["required_fields"],
						"MSG_Save" => $this->_msg["Save"],
						"MSG_Cancel" => $this->_msg["Cancel"],
						'_ROOT.form_action'		=> $baseurl.'&action=add&step=2',
						'_ROOT.cancel_link'		=> $baseurl.'&action=showpricelists',
						'_ROOT.csv_export_link'	=> $baseurl.'&action=export_to_csv',
					));
					$this->main_title = $this->_msg["Add_pricelist"];
				} else if ($request_step == 2) {
					if ($lid) {
						@$cache->delete_cache_files();
						$lc->add_internal_links($this->module_name, $CONFIG["pricelists_table"], "text", $lid, $this->_msg["Link_in_article"], $request_text);
					}
					header('Location: '.$baseurl.'&action=showpricelists');
					exit;
				}
				break;


			case 'edit':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_pricelists", $request_id))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path);
				$tpl->prepare();
				$this->show_upload_forms();
				$tpl->assign(array(
					"MSG_Enter_pricelist_name" => $this->_msg["Enter_pricelist_name"],
					"MSG_Pricelist_name" => $this->_msg["Pricelist_name"],
					"MSG_Download_pricelist_CSV" => $this->_msg["Download_pricelist_CSV"],
					"MSG_Fields_order_in_users_CSV" => $this->_msg["Fields_order_in_users_CSV"],
					"MSG_separator" => $this->_msg["separator"],
					"MSG_required_fields" => $this->_msg["required_fields"],
					"MSG_Save" => $this->_msg["Save"],
					"MSG_Cancel" => $this->_msg["Cancel"],
					'_ROOT.form_action'		=> $baseurl.'&action=add&step=2',
					'_ROOT.cancel_link'		=> $baseurl.'&action=showpricelists',
					'_ROOT.csv_export_link'	=> $baseurl.'&action=export_to_csv',
				));
				$this->show_pricelist_adm($request_id);
				$this->main_title	= $this->_msg["Pricelist_refresh"];
				break;

			case "export_to_csv":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_id;
				$id = intval($request_id);
				$csv = $this->get_export($id);
                $csv = UTF8toCP1251($csv);
				$fname = "pricelists_".$id.".csv";
				header('Content-Type: application/x-csv');
				header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
				header('Content-Disposition: inline; filename="'.$fname.'"');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
				echo $csv;
				exit;
				break;


// Обновление расстояния в БД
			case 'update':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_citya, $request_cityb, $request_cost, $request_id, $request_text, $request_start;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_pricelists", $request_id))
					$main->message_access_denied($this->module_name, $action);

				if ($this->update_pricelist($request_citya, $request_cityb, $request_cost, $request_id)) {
					@$cache->delete_cache_files();
					$lc->add_internal_links($this->module_name,
											$CONFIG['pricelists_table'],
											'text',
											$request_id,
											$this->_msg["Link_in_article"],
											$request_text);
				}
				header('Location: '.$baseurl.'&action=showpricelists&start='.$request_start);
				exit;
				break;

// Удаляем расстояние из БД
			case "del":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				if (!$this->test_item_rights("d", "id", $this->table_prefix."_pricelists", $request_id))
					$main->message_access_denied($this->module_name, $action);

				if ($this->delete_pricelist($request_id)) {
					@$cache->delete_cache_files();
					$lc->delete_internal_links($this->module_name, $CONFIG["pricelists_table"], $request_id);
				}
				header("Location: $baseurl&action=showpricelists&start=$request_start");
				exit;
				break;

// Обновление базы из CSV файла
			case 'uploadcsv':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_title, $request_txt_content;
				if ($request_id) {
					if (!$this->test_item_rights("e", "id", $this->table_prefix."_pricelists", $request_id))
						$main->message_access_denied($this->module_name, $action);

					$this->update_pricelist($request_id, $request_title, $request_txt_content);
					$lid		= $request_id;
					$fl_delete	= 1;
				} else {
					$lid		= $this->add_pricelist($request_title, $request_txt_content);
					$fl_delete	= 0;
				}
				if ($_FILES['pricelistfile']['name']) {
					if ($this->update_catalogue_from_csv($_FILES['pricelistfile']['tmp_name'], $lid, $fl_delete)) {
						@unlink($_FILES['pricelistfile']['tmp_name']);
						$clear_cash	= 1;
					}
				}
				header('Location: '.$baseurl);
				if ($clear_cash) @$cache->delete_cache_files();
					header('Location: '.$baseurl);
				exit;
				break;

		// Редактирование прав прайс-листа
			case 'editrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_pricelists", $request_id, true))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($this->table_prefix."_pricelists", "title", $request_id,
											"&id=".$request_id."&start=".$request_start, $this->_msg["Pricelist"])) {
					header('Location: '.$baseurl.'&action=showpricelists&start='.$request_start);
				}
				break;

		// Сохранение прав прайс-листа
			case 'saverights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start, $request_rights;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_pricelists", $request_id, true))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($this->table_prefix."_pricelists", $request_id, $request_rights);
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
						"res"	=> 1,
						"id"	=> $request_id,
					);
				} else {
					header('Location: '.$baseurl.'&action=showpricelists&start='.$request_start);
				}
				exit;
				break;


// Редактирование свойств блока
			case 'properties':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $module_id, $request_field, $request_block_id, $title;
				$main->include_main_blocks('_block_properties.html', 'main');
				$tpl->prepare();

				if ($main->show_actions($request_field, '', $this->block_main_module_actions, $this->block_module_actions)) {
//					$this->show_pricelists_list_adm('block_pages_list', 'active');
					$main->show_linked_pages($module_id, $request_field);
//					if ($request_field == 0) {
//						$tpl->newBlock("block_paging_property");
//					}
				}
				$tpl->assign(array(	'_ROOT.title'		=> $title,
									'_ROOT.username'	=> $_SESSION['session_login'],
									'_ROOT.password'	=> $_SESSION['session_password'],
									'_ROOT.name'		=> $this->module_name,
									'_ROOT.lang'		=> $lang,
									'_ROOT.block_id'	=> $request_block_id,
								));
				$this->main_title	= $this->_msg["Block_properties"];
				break;


// Создание объекта для дальнейшего использования
			case 'only_create_object':
				break;

// Default...
			default:
				$main->include_main_blocks();
				$tpl->prepare();
			}
//--------------------------- конец обработки действий из админ части --------------------------//
		}
	}


	// функция для вывода статьи по id
	function show_pricelist($transurl, $id = "") {
		global $CONFIG, $main, $db, $db1, $tpl, $core, $server, $lang;

		$id = intval($id);
		if (!$id) {
		    return 0;
		}
		$this->get_list_with_rights(
					"c.id, c.content, B.title, B.body, DATE_FORMAT(B.date, '%d.%m.%y %T') AS date",
                    $this->table_prefix."_pricelists_content c LEFT JOIN ".$this->table_prefix."_pricelists B ON c.pricelist = B.id", "B",
					"c.pricelist = ".$id, "", "c.id ASC");
		$counter = 0;
		if ($db->nf() > 0) {
			while($db->next_record()) {
				$counter++;
				$tpl->assign(
					array(
						"_ROOT.pricelist_title"	 =>	$db->f("title"),
						"_ROOT.pricelist_body"	 =>	$db->f("body"),
						"_ROOT.pricelist_date"	 =>	$db->f("date"),
						"_ROOT.transurl"	 =>	$transurl,
						)
				);
				$content = explode($CONFIG["pricelists_csv_separator"], $db->f("content"));

				$tpl->newBlock("block_pricelist_content");
				$tpl->assign(
					array(
						"_ROOT.pricelist_title"	 =>	$db->f("title"),
						"_ROOT.pricelist_body"	 =>	$db->f("body"),
						"_ROOT.pricelist_date"	 =>	$db->f("date"),
						"_ROOT.transurl"	 =>	$transurl,
						)
				);
				$cnt = count($content);
				if (1 == $counter && '' != $content[0] && '' != $content[1]) {
					$tpl->newBlock("block_captions");
				    foreach ($content as $key=>$val) {
				        $tpl->newBlock("block_captions_title");
				        $tpl->assign(array('title' => $val, "transurl" => $transurl));
				    }
				    continue;
				}
				if ('' == $content[1] && '' != $content[0]) {
					$tpl->newBlock("block_categories");
					$tpl->assign(array('title' => $content[0], 'property_count' => $cnt, "transurl" => $transurl));
					continue;
				}
				$tpl->newBlock("block_products");
				$tpl->assign(array('id' => $db->f("id"), "transurl" => $transurl));
				foreach ($content as $key=>$val) {
				    $tpl->newBlock("block_products_propertys");
				    $tpl->assign(array(property => $val));
				}
			}

			return 1;
		}
		return 0;
	}

	// функция для вывода списка прайслистов
	function show_pricelists_list($transurl) {
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;

		$query = "SELECT * FROM ".$this->table_prefix."_pricelists";
		$this->get_list_with_rights("p.*", $this->table_prefix."_pricelists p", "p");
		if ($db->nf() > 0) {
			//$tpl->assign("news_archive_link","$transurl&action=archive&start=$start");
			while($db->next_record()) {

				$tpl->newBlock("block_pricelist_list");
				$tpl->assign(
					array(
						pricelist_id	=>	$db->f('id'),
						pricelist_title	=>	$db->f("title"),
						pricelist_link 	=> "$transurl&action=show&id=" . $db->f("id"),
					)
				);
			}

			$tpl->gotoBlock("_ROOT");
			$tpl->assign(
				array(
					all_news_link	=>	$transurl,
					cols			=>	$rows
				)
			);
			return TRUE;
		}
		return FALSE;
	}


	// функция для вывода формочек для закачки файлов обновления каталога
	function show_upload_forms() {
		global $CONFIG, $tpl, $main, $baseurl;

			foreach ($CONFIG["pricelists_transfer_fields"] as $idx => $arr) {
				$pricelist_fields[$idx] = $arr["descr"];
			}

			$tpl->newBlock("block_upload_csv");
			$tpl->assign(
				array(
					"upload_csv_url"				=>	"$baseurl&action=uploadcsv",
					"available_pricelists_fields"	=>	implode(", ", $pricelist_fields),
					"csv_separator"					=>	$CONFIG["pricelists_csv_separator"],
				)
			);
		return;
	}


	// функция для обновления каталога через csv файл
	function update_catalogue_from_csv($file, $pricelist, $fl_delete) {
		global $CONFIG, $db, $server, $lang;

		$test_mode = 0;
		$updated = date("Y-m-d H-i-s"); // время закачки
		if($fl_delete) {
			$query = "DELETE FROM ".$this->table_prefix."_pricelists_content WHERE pricelist = ".$pricelist;
			$db->query($query);
		}
		foreach ($CONFIG["pricelists_transfer_fields"] as $idx => $arr) {
			$fields[$idx] = $arr["name"];
		}

		if ($file && $handle = fopen($file,"r")) {
			$row = 0;
			while ($line = fgets($handle, 2000)) {
				$line = "'".$db->escape($line)."'";
				$line = iconv('WINDOWS-1251','UTF-8', $line);
				// сформировать запрос к БД для обновления или занесения новой записи
				$query = "INSERT INTO ".$this->table_prefix."_pricelists_content (pricelist, content) VALUES (".$pricelist.", ".$line.")";

				if (!$test_mode) {
					$db->query($query);
				}

				// занести code в массив уже имеющихся в БД code
				if ($test_mode) {
					$lid++;
				} elseif ($db->affected_rows() > 0 ) {
					$lid = $db->lid();
				}

				if ($test_mode) {
					print $query . "<br>";
					continue;
				}

			}
			fclose ($handle);

			if ($test_mode) exit;
			return 1;
		}
		return 0;
	}

	/**
	* Получить список товаров по id
	* @param mixed $ids Идентификатор или список идентификаторов товаров
	* @return array
	*/
	function get_goods($ids)
	{   global $db, $tpl, $server, $lang, $CONFIG;

	    $ret = array();

	    if ((!is_array($ids) && $ids < 1) || (is_array($ids) && !count($ids))) {
	    	return ret;
	    }

	    $where = "=".(int)$ids;
	    if (is_array($ids)) {
	    	$where = " IN (";
	    	foreach ($ids as $key => $val) {
	    		$where .= (int)$val.", ";
	    	}
	    	$where = substr($where, 0, -2).")";
	    }
		$this->get_list_with_rights(
					"c.*",
                    $this->table_prefix."_pricelists_content c LEFT JOIN ".$this->table_prefix."_pricelists p ON c.pricelist = p.id", "p",
					"c.id".$where);
	    $db->query($query);
		if ($db->nf() > 0) {
			while($db->next_record()) {
				$content = explode($CONFIG["pricelists_csv_separator"], $db->f("content"));
				array_unshift($content, $db->f("id"));
				$ret[] = $content;
			}
		}

		return $ret;
	}

	/**
	* Отправка письма с выбранным товаром
	* @param mixed
	* @return mixed
	*/
	function order()
	{   global $db, $tpl, $server, $lang, $CONFIG;

		$goods_id = $_POST['id'];
		$goods_list = $this->get_goods($goods_id);
		$prop_cnt = count($goods_list[0]);

		$msg = iconv("UTF-8", "WINDOWS-1251", $this->_msg["PriseList"] . " - ".$_POST['priselist_title'])."<br />";
		$msg .= iconv("UTF-8", "WINDOWS-1251", $this->_msg["FIO"] . " - ".$_POST['fio'])."<br />";
		$msg .= $this->_msg["Email"] . " - ".$_POST['email']."<br />";
		$msg .= iconv("UTF-8", "WINDOWS-1251",$this->_msg["Phone"] . " - ".$_POST['tel'])."<hr />";
		$msg .= "<table border=\"1\">\n";
		$msg .= "<tr><th>№</th><th colspan=\"".($prop_cnt-1)."\">&nbsp;</th><th>" . iconv("UTF-8", "WINDOWS-1251", $this->_msg["Quantity"]) . "</th></tr>";

		foreach ($goods_list as $goods) {
			$msg .= "<tr>";
			foreach ($goods as $prop) {
				$msg .= "<td>".iconv("UTF-8", "WINDOWS-1251", $prop)."</td>";
			}
			$msg .= "<td>".$_POST['cnt'][$goods[0]]."</td>";
			$msg .= "</tr>\n";
		}
		$msg .= "</table>\n";

		$headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=windows-1251\r\n";
    $headers .= "To: ".$CONFIG['pricelists_email']."\r\n";
    $headers .= "From: ".$_POST['email']."\r\n";

    mail($CONFIG['pricelists_email'], iconv("UTF-8", "WINDOWS-1251", $CONFIG['pricelists_email_title']), $msg, $headers);
		$tpl->newBlock("block_pricelist_msg");

		return;
	}

	function getPropertyFields() {
		GLOBAL $request_site_target, $request_sub_action;

		if ('' != $request_sub_action) {

			$tpl = $this->getTmpProperties();

			switch ($request_sub_action) {
				case 'show'://pricelists':
					$tpl->newBlock('block_properties');
					$tpl->assign(Array(
						"MSG_Pricelists" => $this->_msg["pricelists"],
						"MSG_Select" => $this->_msg["Select"],
					));
					$list = $this->get_pricelist_list();
					abo_str_array_crop($list);
					$tpl->assign_array('block_pricelists_list', $list);
					$tpl->newBlock('block_page_edit');
					$tpl->assign(Array(
						"MSG_Goto_edit" => $this->_msg["Goto_edit"],
						"site_target" => $request_site_target,
					));
					break;
				default:
					$tpl->newBlock('block_properties_none');
					$tpl->assign(Array(
						"MSG_No" => $this->_msg["No"],
					));
					break;
			}
			return $this->getOutputContent($tpl->getOutputContent());
		}
		return FALSE;
	}

	function getEditLink($sub_action = NULL, $block_id = NULL) {
		GLOBAL $request_name, $db, $lang, $server;

		$block_id	= (int)$block_id;
		if ($sub_action && $block_id) {
			$db->query('SELECT	property1,
								property2,
								property3,
								property4,
								property5
							FROM '.$this->table_prefix.'_pages_blocks
							WHERE id = '.$block_id);
			if ($db->nf() > 0) {
				$db->next_record();
				$arr[]	= $db->f('property1');
				$arr[]	= $db->f('property2');
				$arr[]	= $db->f('property3');
				$arr[]	= $db->f('property4');
				$arr[]	= $db->f('property5');

				switch ($sub_action) {
					case 'show'://pricelists':
						$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=showpricelists&menu=false&category=';
						$link['material_id']	= $arr[0];
						break;
				}
				return $link;
			}

			switch ($sub_action) {
				case 'show'://pricelists':
					$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=showpricelists&menu=false&category=';
					$link['material_id']	= '';
					break;
			}
			return $link;
		}
		return FALSE;
	}

	// Функция для вывода списка записей
	function show_pricelists_list_adm($blockname, $type = "all", $start = "", $rows = "", $order_by = "", $search = "") {
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;
		if (!$order_by) $order_by = $CONFIG["pricelists_order_by"];

		$pages_cnt = -1;
		if ($type == "active") {
			$this->get_list_with_rights(
						"p.*", $this->table_prefix."_pricelists p", "p",
						"", "", $order_by);
		} else if ($type == "pages") {
			$start = intval($start); $start = ($start) ? $start : 1;
			if (!$rows) $rows = $CONFIG["pricelists_max_rows"];
			$total_cnt = $this->get_list_with_rights(
									"p.*", $this->table_prefix."_pricelists p", "p",
									"", "", $order_by,
									$start, $rows,
									true);
			$pages_cnt = ceil($total_cnt/$rows);
		} else if ($type == "search") {
			$search = $db->escape($search);
			$this->get_list_with_rights(
						"p.*", $this->table_prefix."_pricelists p", "p",
						"title LIKE '%$search%' OR text LIKE '%$search%' OR id = '$search'", "p.id", $order_by);
		} else {
			$this->get_list_with_rights(
						"p.*", $this->table_prefix."_pricelists p", "p",
						"", "", $order_by);
		}

		if ($db->nf() > 0) {
			$actions = array(	"edit"		=> "edit",
								"editrights"=> "editrights",
								"delete"	=> "del",
								"confirm_delete"	=> $this->_msg["Confirm_delete_this_pricelist"]
							);
			while($db->next_record()) {
				// вывод на экран списка
				$id = $db->f("id");
				$adm_actions = $this->get_actions_by_rights($baseurl."&id=".$id."&start=".$start, $db->f("active"), $db->f("is_owner"), $db->f("user_rights"), $actions);

				$tpl->newBlock($blockname);
				$tpl->assign(
					array(
						'pricelist_title'			=>	$db->f("title"),
						'pricelist_edit_action'		=>	$adm_actions["edit_action"],
						'pricelist_edit_rights_action'	=>	$adm_actions["edit_rights_action"],
						'pricelist_del_action'		=>	$adm_actions["delete_action"],
						"pricelist_csv_export_link"	=>	"$baseurl&action=export_to_csv&id=$id",
					)
				);
			}
			return $pages_cnt;
		}
	}


	// Функция, генерирующая CSV файл из текушей БД
	function get_export ($id) {
		global $CONFIG, $db, $db1, $server, $lang;
		$csv = "";
		$query = "SELECT content FROM ".$this->table_prefix."_pricelists_content WHERE pricelist = '$id' ORDER BY id ASC";
		$db->query($query);
		if ($db->nf() >0 ) {
			while($db->next_record()) {
				$csv .= $db->f("content")."\n";
			}
			return $csv;
		}
		else
			return 0;
	}


	// вывод прайслиста
	function show_pricelist_adm($id = '') {
		global $CONFIG, $main, $db, $tpl, $server, $lang;
		$id = (int)$id;
		if ($id) {
			$db->query('SELECT * FROM '.$this->table_prefix.'_pricelists WHERE id = '.$id);
			if ($db->nf() > 0) {
				$db->next_record();
				$title	= htmlspecialchars($db->f("title"));

				$tpl->assign(
					array(
						'id'				=> $db->f('id'),
						'pricelists_title'	=> $title,
						'text'				=> $db->f('body'),
					)
				);
				return TRUE;
			}
		}
		return FALSE;
	}

	// функция для добавления записи в базу данных
	function add_pricelist($title, $body, $block_id = 0) {
		global $CONFIG, $db, $server, $lang;
		$title	= $db->escape($title);
		$body	= $db->escape($body);

		$owner_id = $_SESSION["siteuser"]["id"];
		list($group_id, $rights) = $this->get_group_and_rights('article_default_rights', $block_id);

		$db->query('INSERT INTO '.$this->table_prefix.'_pricelists (title, date, body, owner_id, usr_group_id, rights)
						VALUES ("'.$title.'", NOW(), "'.$body.'", '.$owner_id.', '.$group_id.', '.$rights.')');
		return ($db->affected_rows() > 0) ? $db->lid() : FALSE;
	}


	// функция для обновления записи
	function update_pricelist($id, $title, $body) {
		global $CONFIG, $db, $server, $lang;
		$id		= (int)$id;
		if (!$id) return FALSE;
		$title	= $db->escape($title);
		$body	= $db->escape($body);
		$query = "UPDATE ".$this->table_prefix."_pricelists
					SET title = '$title', date = NOW(), body = '$body' WHERE id = $id";
		$db->query($query);
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}


	// функция для удаления записи из базы данных
	function delete_pricelist($id) {
		global $db, $server, $lang;

		if (!$id = intval($id)) return 0;

		$query = "DELETE FROM ".$this->table_prefix."_pricelists WHERE id = ".$id;
		$db->query($query);

		$query = "DELETE FROM ".$this->table_prefix."_pricelists_content WHERE pricelist = ".$id;
		$db->query($query);

		return ($db->affected_rows() > 0) ? 1 : 0;
	}

	/**
	* Получить список прайслистов
	* @param void
	* @return array
	*/
	function get_pricelist_list()
	{   global $CONFIG, $db, $server, $lang;

	    $ret = array();
	    $query = "SELECT * FROM ".$this->table_prefix."_pricelists ORDER BY title";
	    $db->query($query);
		if ($db->nf() >0 ) {
			while($db->next_record()) {
				$ret[] = array("pricelist_id" =>$db->f('id'), "pricelist_title" =>$db->f('title'));
			}
		}

		return $ret;
	}
}
function UTF8toCP1251($str){
  static $table = array(
    "\xD0\x81" => "\xA8", // Ё
    "\xD1\x91" => "\xB8", // ё
  );
  return preg_replace('#([\xD0-\xD1])([\x80-\xBF])#se',
                      'isset($table["$0"]) ? $table["$0"] :
                       chr(ord("$2")+("$1" == "\xD0" ? 0x30 : 0x70))
                      ',
                      $str
                     );
}

?>