<?php // Vers 5.8.2 27.06.2012
/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array('pricelists_order_by'      => array('type'   => 'select',
													   'defval'	=> array('id DESC'	=> array(	'rus' => 'в порядке обратном добавлению',
													 											'eng' => 'reverce adding order'),
													 					 'id'		=> array(	'rus' => 'в порядке добавления',
													 											'eng' => 'adding order'),
													 					 ),
													   'descr'	=> array(	'rus' => 'Порядок вывода записей',
													 						'eng' => 'Show records order'),
                                                       'access' => 'editable',
                                                      ),
                   'pricelists_max_rows'      => array('type' => 'integer',
                                                       'defval'	=> '20',
                                                       'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу',
												 							'eng' => 'Maximum quantity of records on page'),
                                                       'access'	=> 'editable'
                                                      ),
                   'pricelists_csv_separator' => array('type' => 'string',
                                                       'defval'	=> ';',
                                                       'descr'	=> array(	'rus' => 'Разделитель полей в CSV файле',
													   						'eng' => 'CSV file field spliter'),
                                                       'access'	=> 'editable'
                                                      ),
                   'pricelists_email'         => array('type' => 'string',
                                                       'defval'	=> 'admin@cms',
                                                       'descr'	=> array(	'rus' => 'Email на который будет приходить письмо с выбранным товаром',
													   						'eng' => 'Email on which the letter with the chosen goods will come'),
                                                       'access'	=> 'editable'
                                                      ),
                   'pricelists_email_title'         => array('type' => 'string',
                                                       'defval'	=> "Заказ товара на сайте",
                                                       'descr'	=> array(	'rus' => 'Заголовок письма',
													   						'eng' => 'E-mail title'),
                                                       'access'	=> 'editable'
                                                      )
                  );

$OPEN_NEW = array("pricelists_transfer_fields" => array(array("name"  => "content",
										  		              "descr" => "Свойства",
										  		              "type"  => "string"),
/*
										  array("name"	=> "cost",
										  		"descr" => "Цена за тонну",
										  		"type"	=> "string"),

										  array("name"	=> "warehouse",
										  		"descr" => "Склад",
										  		"type" => "string")
*/
										                 ) // порядок следования полей для категории в csv файле
                 );

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'pricelists_order_by' => 'id DESC',
'pricelists_max_rows' => '20',
'pricelists_csv_separator' => ';',
'pricelists_email' => 'abocms@abocms.ru',
'pricelists_email_title' => 'Демонстрационный заказ товара на сайте ABO.CMS',
/* ABOCMS:END */
);
?>