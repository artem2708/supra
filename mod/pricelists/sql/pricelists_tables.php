<?php

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_pricelists` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `title` varchar(255) default '',
  `date` datetime default NULL,
  `body` text,
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_pricelists_content` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `pricelist` smallint(5) unsigned NOT NULL default '0',
  `content` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

?>