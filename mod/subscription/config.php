<?php // Vers 5.8.2 27.06.2012

/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
	'subscription_order_by'			=> array('type'		=> 'select',
											 'defval'	=> array('date DESC'=> array(	'rus' => 'В порядке убывания даты',
											 											'eng' => 'Date decrease order'),
											 					 'date'		=> array(	'rus' => 'В порядке увеличения даты',
											 											'eng' => 'Date increase order'),
											 					 ),
											 'descr'	=> array(	'rus' => 'Порядок вывода записей на сайте',
											 						'eng' => 'Show records order on site'),
										 	 'access'	=> 'editable',
										 	 ),

	'subscription_admin_order_by'	=> array('type'		=> 'select',
											 'defval'	=> array('id DESC'	=> array(	'rus' => 'В порядке обратном добавлению',
											 											'eng' => 'Reverce adding order'),
											 					 'id'		=> array(	'rus' => 'В порядке добавления',
											 											'eng' => 'Adding order'),
											 					 ),
											 'descr'	=> array(	'rus' => 'Порядок вывода записей в адм. интерфейсе',
											 						'eng' => 'Show records order in admin interface'),
									 		 'access'	=> 'editable'
									 		 ),

	'subscription_max_rows' 		=> array('type'		=> 'integer',
	   										 'defval'	=> '10',
											 'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу',
												 					'eng' => 'Maximum quantity of records on page'),
	   									 	 'access'	=> 'editable'
	   									 	 ),

	'subscription_email_max_rows' 	=> array('type'		=> 'integer',
	   										 'defval'	=> '10',
											 'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу в списке рассылки',
												 					'eng' => 'Maximum quantity of records on page in dispatch list'),
	   									 	 'access'	=> 'editable'
	   									 	 ),

	'subscription_email_order_by' 	=> array('type'		=> 'select',
											 'defval'	=> array('id DESC'	=> array(	'rus' => 'В порядке обратном добавлению',
											 											'eng' => 'Reverce adding order'),
											 					 'id'		=> array(	'rus' => 'В порядке добавления',
											 											'eng' => 'Adding order'),
											 					 ),
											 'descr'	=> array(	'rus' => 'Порядок вывода списка рассылки',
											 						'eng' => 'Show records order in dispatch list'),
	   									 	 'access'	=> 'editable'
	   									 	 ),

	'subscription_date_format'		=> array('type'		=> 'select',
		   									 'defval'	=> array('%d.%m.%Y %H:%i'	=> array(	'rus' => 'День.Месяц.Год Часы:Минуты',
											 										  			'eng' => 'Day.Month.Year Hours:Minutes'),
		   									 					 '%d.%m.%Y'			=> array(	'rus' => 'День.Месяц.Год',
											 													'eng' => 'Day.Month.Year'),
		   									 					 '%Y.%m.%d'			=> array(	'rus' => 'Год.Месяц.День',
											 													'eng' => 'Year.Month.Day'),
		   									 					 '%Y.%m.%d %H:%i'	=> array(	'rus' => 'Год.Месяц.День Часы:Минуты',
											 													'eng' => 'Year.Month.Day Hours:Minutes'),
		   									 					 ),
											 'descr'	=> array(	'rus' => 'Формат выводимой даты',
											 						'eng' => 'Format of deduced date'),
	   									 	 'access'	=> 'editable',
	   									 	 ),

	'subscription_news_max_cnt'		=> array('type'		=> 'integer',
	   										 'defval'	=> '10',
											 'descr'	=> array(	'rus' => 'Максимальное количество новостей предлагаемых для рассылки',
											 						'eng' => 'Maximal quantity of news suggested for dispatch'),
	   									 	 'access'	=> 'editable',
	   									 	 ),
	'subscription_default_rights'	=> array('type'		=> 'string',
		   									 'defval'	=> '111111110001',
		   									 'descr'	=> array(	'rus' => 'Права для рассылки по-умолчанию (формат <img src="/i/admin/rights_format.gif" width=72 height=18 alt="формат прав">, где x - 0 или 1, каждая группа представляет набор прав: удаление, публикация, редактирование, чтение)',
												 						'eng' => 'Subscription default rights (format <img src="/i/admin/rights_format.gif" width=72 height=18 alt="rights format">, where x - 0 or 1, every group is rights set: delete, publication, edit, read)'),
		   									 'access'	=> 'editable'
		   									 ),

	'subscription_perpage'	=> array('type'		=> 'integer',
			   								 'defval'	=> '50',
			   								 'descr'	=> array(	'rus' => 'Количество писем рассылаемых за один раз',
											 						'eng' => 'Number of emails sent at one time'),
			   								 'access'	=> 'editable'
			   								 ),

	'subscription_sleep'	=> array('type'		=> 'integer',
			   								 'defval'	=> '50',
			   								 'descr'	=> array(	'rus' => 'Задержка в секундах, которая будет происходить после рассылки',
											 						'eng' => 'The delay in seconds, which will occur after the mailing'),
			   								 'access'	=> 'editable'
			   								 ),
			   );

$OPEN_NEW = array (
	'subscription_files_path' => 'files/subscription/'.SITE_PREFIX.'/mail_attach/',		// куда ложить закачанные на сервер картинки для новостных анонсов
//    'subscription_page_link'		=> 'subscription',
//    'subscription_page_address'		=> 'demomodules/subscription/',
);

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'subscription_order_by' => 'date DESC',
'subscription_admin_order_by' => 'id DESC',
'subscription_max_rows' => '5',
'subscription_email_max_rows' => '5',
'subscription_email_order_by' => 'id DESC',
'subscription_date_format' => '%d.%m.%Y',
'subscription_news_max_cnt' => '3',
'subscription_default_rights' => '111111110001',
'subscription_perpage' => '50',
'subscription_sleep' => '2'
/* ABOCMS:END */
);
?>