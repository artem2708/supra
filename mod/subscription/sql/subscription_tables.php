<?php

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_subscription` (
  `id` int(11) NOT NULL auto_increment,
  `category_id` smallint(5) unsigned NOT NULL default '0',
  `date` datetime default NULL,
  `title` varchar(255) NOT NULL default '',
  `body` text,
  `news_ids` varchar(255) default NULL,
  `filename1` varchar(255) default NULL,
  `filename2` varchar(255) default NULL,
  `filename3` varchar(255) default NULL,
  `was_mailed` tinyint(1) NOT NULL default '0',
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_subscription_categories` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_subscription_mailing_list` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `cid` int(11) unsigned NOT NULL default '0',
  `email` varchar(255) NOT NULL default '',
  `name` varchar(255) NOT NULL default '',
  `date` datetime default NULL,
  `uid` varchar(50) default NULL,
  `confirmation` tinyint(1) NOT NULL default '0',
  `active` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

?>