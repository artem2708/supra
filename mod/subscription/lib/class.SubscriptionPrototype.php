<?php

require_once(RP.'inc/class.Mailer.php');
require_once(RP.'inc/class.Mail.php');

class SubscriptionPrototype extends Module {

	var $main_title;
	var $table_prefix;
	var $addition_to_path;
	var $default_action 			= 'subform';
	var $admin_default_action		= 'showctg';
	var $tpl_path					= 'mod/subscription/tpl/';
	var $module_name				= 'subscription';
	var $block_module_actions		= array();
	var $block_main_module_actions	= array();

	function SubscriptionPrototype($action = '', $transurl = '', $properties = array(), $prefix = NULL, $rewrite_mod_params = NULL, $adminurl = NULL, $noadmin = false) {
		global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang, $permissions, $request_type, $_RESULT, $db;
		
		/* Get $action, $tpl_name, $permissions */
		extract( $this->init(array(
		        'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
		        'action' => $action, 'tpl_name' => $properties['tpl']
		)), EXTR_OVERWRITE);
		
		if ('only_create_object' == $action) {
		    return;
		}
		
		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}
		
		if (! self::is_admin()) {
//---------------------------------- обработка действий с сайта --------------------------------//
			switch ($action) {
			case "subform":
				$main->include_main_blocks($this->module_name.'.html', 'main');
				$tpl->prepare();
				if ($properties[1] == 'all_cat') {
					$cat = $this->getCategory($properties);
				} else {
				    $categ_name = $this->getCategory($properties);
				    $tpl->assign('categ_name', $categ_name[0]['category']);
				    $tpl->newBlock('block_one_category');
				    $tpl->assign('categ_id', $categ_name[0]['category_id']);
				}
                if (sizeof($cat) >= 1) {
                  $tpl->newBlock('block_all_categories');
                  $tpl->assign_array('block_categories', $cat);
                }
				$tpl->assign(array(
				    '_ROOT.form_action' => $transurl.'&action=subscribe&nocash',
				));
				break;

			case "subform_name":
				$main->include_main_blocks($this->module_name.'.html', 'main');
				$tpl->prepare();
				if ($properties[1] == 'all_cat') {
					$cat = $this->getCategory($properties);
				} else {
					$categ_name = $this->getCategory($properties);
					$tpl->assign('categ_name', $categ_name[0]['category']);
					$tpl->newBlock('block_one_category');
					$tpl->assign('categ_id', $categ_name[0]['category_id']);
				}
                if (sizeof($cat) >= 1) {
                  $tpl->newBlock('block_all_categories');
                  $tpl->assign_array('block_categories', $cat);
                }
				$tpl->newBlock("block_sub_name");
				$tpl->assign(array(
				    '_ROOT.form_action' => $transurl.'&action=subscribe&nocash',
				));
				break;

			case 'subscribe':
				global $request_sub_name, $request_sub_email, $request_cat;
				$main->include_main_blocks($this->module_name.'_result_message.html', 'main');
				$tpl->prepare();
                $categs = $this->search_such_email($request_cat, $request_sub_email);
				if ($categs) {
					$tpl->newBlock('block_subscribe_1');
					//$result_message = 'Указанный e-mail уже есть в списке почтовой рассылки "'.$categs.'" сайта "'.$CONFIG['sitename_rus'].'"!';
				} else {
					if ($request_sub_email) {
						if ($this->add_email($request_cat, $request_sub_email, $request_sub_name)) {
							$tpl->newBlock('block_subscribe_2');
							//$result_message = 'На ваш e-mail отправлено письмо с просьбой подтвердить желание получать информацию с нашего сайта . Зайдите по адресу, указанному в письме, и ваш e-mail будет добавлен в нашу почтовую рассылку.';
						} else {
							$tpl->newBlock('block_subscribe_3');
							//$result_message = 'Невозможно добавить e-mail.';
						}
					}
				}
				//if ($result_message) $main->show_result_message($result_message);
				break;

// Подтверждение подписки
			case "confirm":
				global $request_uid;
				$main->include_main_blocks($this->module_name.'_confirm_message.html', 'main');
				$tpl->prepare();
				if ($request_uid) {
                    $subscr = $this->confirm_subscription($request_uid);
					if ($subscr) {
						$tpl->newBlock('block_confirm_1');
						$tpl->assign('subscr', $subscr);
						//$result_message = 'Ваш e-mail успешно добавлен в почтовую рассылку ('.$subscr.'), сайта "'.$CONFIG['sitename_rus'].'"';
					} else {
						$tpl->newBlock('block_confirm_2');
						//$result_message = 'Невозможно подтвердить подписку! Подписчика с таким UID нет в списке почтовой рассылки сайта "'.$CONFIG['sitename_rus'].'"';
					}
				}
				//if ($result_message) $main->show_result_message($result_message);
				break;

// Отписаться от рассылки
			case "unsubscribe":
				global $request_uid;
				$main->include_main_blocks($this->module_name.'_unsubscribe_message.html', 'main');
				$tpl->prepare();

				if ($request_uid) {
					if ($this->remove_email("", $request_uid)) {
						$tpl->newBlock('block_unsubscribe_1');
						//$result_message = 'Ваш e-mail успешно удален из почтовой рассылки сайта "'.$CONFIG['sitename_rus'].'".';
					} else {
						$tpl->newBlock('block_unsubscribe_2');
						//$result_message = 'Невозможно удалить e-mail! Подписчика с таким UID нет в списке почтовой рассылки сайта "'.$CONFIG['sitename_rus'].'".';
					}
				}

				//if ($result_message) $main->show_result_message($result_message);
				break;

// Default...
			default:
				if (is_object($tpl)) $tpl->prepare();
				return;
			}
//------------------------------- конец обработки действий с сайта -----------------------------//
		} else {
//------------------------------ обработка действий из админ части -----------------------------//
            $this->block_module_actions		= array('subform'	=> $this->_msg['Display_subscription_form'],
  											'subform_name'	=> $this->_msg['Display_subscription_form_with_name'],
  											);
            $this->block_main_module_actions	= array('subscribe'	=> $this->_msg['Process_subscription_results'],
  	                                        'subform'	=> $this->_msg['Display_subscription_form'],
  											'subform_name'	=> $this->_msg['Display_subscription_form_with_name'],
  											);
			if (!isset($tpl)) {
			    $main->message_die(iconv("WINDOWS-1251", "UTF-8", 'Не подключен класс для работы с шаблонами'));
			}

			switch ($action) {
		    case 'prp':
		    	if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step;
				$main->include_main_blocks('_properties.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Information"	=> $main->_msg["Information"],
					"MSG_Value"			=> $main->_msg["Value"],
					"MSG_Save"			=> $main->_msg["Save"],
					"MSG_Cancel"		=> $main->_msg["Cancel"],
				));
		        if (!$request_step || $request_step == 1) {
					$tpl->assign(array('form_action'	=> $baseurl,
									   'step'			=> 2,
									   'lang'			=> $lang,
									   'name'			=> $this->module_name,
									   'action'			=> 'prp'));
					$prp_html = $main->getModuleProperties($this->module_name);
		            $this->main_title = $this->_msg['Settings'];
				} elseif (2 == $request_step) {
		        	$main->setModuleProperties($this->module_name, $_POST);
					header('Location: '.$baseurl.'&action=prp&step=1');
				}
		    	break;

			case 'block_prp':
				global $request_sub_action, $request_block_id;
		    	$arr  = $this->getEditLink($request_sub_action, $request_block_id);
		    	$cont = 'var container = document.getElementById(\'target_span\');
		    		  	 container.innerHTML	= "'.$this->getPropertyFields().'";
		    		  	 material_id			= "'.$arr['material_id'].'";
		    		  	 material_url			= "'.$arr['material_url'].'";
		    		  	 changeAction();
		    	      	 setURL("property1");';
				header('Content-Length: '.strlen($cont));
		    	echo $cont;
		    	exit;
		    	break;

			case 'import':
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2($this->module_name.'_import.html', $this->tpl_path);
				$tpl->prepare();
				
				if ( is_uploaded_file($_FILES['csv_file']['tmp_name']) && 0 == $_FILES['csv_file']['error']) {
				    $res = $this->import($_REQUEST['ctgr'], $_FILES['csv_file']['tmp_name']);
				    if ($res) {
				        $tpl->newBlock('block_ok');
				        $tpl->assign(array(
        					'res'	=>	$res
        				));
				    } else {
				        $tpl->newBlock('block_err');
				    }
				    $tpl->gotoBlock('_ROOT');
				}
				$tpl->assign(array(
					'baseurl'	=>	$baseurl,
					'action'	=>	$action,
				));
				$this->show_categories('block_categories');
				
				$this->main_title = $this->_msg["Add_recipient"];
				break;

			case "showsubscription":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_start, $request_category;
				$start = ($request_start) ? $request_start : 0;
				$category = ($request_category) ? $request_category : 1;
				$main->include_main_blocks_2($this->module_name."_list.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Rights" => $main->_msg["Rights"],
					"MSG_Date" => $this->_msg["Date"],
					"MSG_Header" => $this->_msg["Header"],
					"MSG_Subscription" => $this->_msg["subscription"],
					"MSG_Del" => $this->_msg["Del"],
				));
				list($category_title, $ctg_owner_id) = $this->show_category($request_category);
				$pages_cnt = $this->show_subscription_titles($category, $start, '', '', $ctg_owner_id);
				if (!$pages_cnt) {
						header('Location: '.$baseurl.'&action=add');
						exit;
				}
				$main->_show_nav_block($pages_cnt, null, "action=".$action."&category=".$category, $start);
				$tpl->assign(Array(
					"MSG_Pages" => $this->_msg["Pages"],
				));
				$this->main_title = ($category_title) ? $category_title : "";
				break;

// Добавление новостей в базу данных
			case 'add':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $db, $request_step, $request_category, $request_title, $request_txt_content,
						$request_news_list, $request_date, $request_hrs, $request_min, $request_body;
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(array(
						"MSG_Enter_header" => $this->_msg["Enter_header"],
						"MSG_Choose_category" => $this->_msg["Choose_category"],
						"MSG_Category" => $this->_msg["Category"],
						"MSG_Select_category" => $this->_msg["Select_category"],
						"MSG_Header" => $this->_msg["Header"],
						"MSG_Last_news_list" => $this->_msg["Last_news_list"],
						"MSG_File_for_upload" => $this->_msg["File_for_upload"],
						"MSG_required_fields" => $this->_msg["required_fields"],
						"MSG_Save" => $this->_msg["Save"],
						"MSG_Cancel" => $this->_msg["Cancel"],
						'_ROOT.form_action'	=> $baseurl.'&action=add&step=2',
						'_ROOT.cancel_link'	=> $baseurl.'&action=showctg',
						'_ROOT.checked'		=> 'checked',
					));
					$nf	= $this->show_categories('block_categories', '' ,true);
					if ($nf == 0) {
						header('Location: '.$baseurl.'&action=showctg&empty');
						exit;
					}
					$this->show_news('block_news');
					$this->main_title = $this->_msg['Add_subscription'];
				} else if ($request_step == 2) {
					list($allow, $ctg_rights) = $this->test_item_rights("e", "id", $this->table_prefix."_subscription_categories",
																		$request_category, false, 0, true);
					if (!$allow) $main->message_access_denied($this->module_name, $action);

					$block_id = intval(substr($request_block_id, 10));
					$lid = $this->add_subscription($request_category, $request_title, $request_txt_content, $request_news_list, $request_date, $request_hrs, $request_min, $ctg_rights, $block_id);
					if ($lid) {
						if($_FILES['file1']['name'] || $_FILES['file2']['name'] || $_FILES['file3']['name']) {
							mkdir(RP.$CONFIG["subscription_files_path"]."folder_".$lid, 0777) or die("Can not create folder");
						}
						$filename1 = ($_FILES['file1']['name']) ? $main->upload_file('file1', 0, $CONFIG["subscription_files_path"]."folder_".$lid."/") : '';
						$filename2 = ($_FILES['file2']['name']) ? $main->upload_file('file2', 0, $CONFIG["subscription_files_path"]."folder_".$lid."/") : '';
						$filename3 = ($_FILES['file3']['name']) ? $main->upload_file('file3', 0, $CONFIG["subscription_files_path"]."folder_".$lid."/") : '';
						$db->query('UPDATE '.$this->table_prefix.'_subscription
									SET filename1 = "'.$filename1.'",
										filename2 = "'.$filename2.'",
										filename3 = "'.$filename3.'"
									WHERE id = '.$lid);
						@$cache->delete_cache_files();
						$lc->add_internal_links($this->module_name, "subscription", "body", $lid, "ссылка в новостях", $request_body);
						header("Location: $baseurl&action=showsubscription&category=$request_category");
					} else {
						header("Location: $baseurl&action=showctg");
					}
					exit;
				}
				break;

// Редактирование рассылки
			case "copy":
			case "edit":
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_subscription", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Enter_header" => $this->_msg["Enter_header"],
					"MSG_Choose_category" => $this->_msg["Choose_category"],
					"MSG_Category" => $this->_msg["Category"],
					"MSG_Select_category" => $this->_msg["Select_category"],
					"MSG_Header" => $this->_msg["Header"],
					"MSG_Last_news_list" => $this->_msg["Last_news_list"],
					"MSG_File_for_upload" => $this->_msg["File_for_upload"],
					"MSG_required_fields" => $this->_msg["required_fields"],
					"MSG_Save" => $this->_msg["Save"],
					"MSG_Cancel" => $this->_msg["Cancel"],
				));
				list($category_id, $news_ids) = $this->show_subscription($request_id, $request_start);
				if (!$category_id) {
					header('Location: '.$baseurl.'&action=showctg');
					exit;
				}
				$tpl->assign(array(
					'_ROOT.form_action'	=>	$baseurl.('edit' == $action ? '&action=update&id='.$request_id : '&action=add&step=2'),
					'_ROOT.cancel_link'	=>	$baseurl.'&action=showsubscription&category='.$request_category.'&start='.$request_start,
				));

				$this->show_categories('block_categories', $category_id, true);
           		$this->show_news('block_news', $news_ids);
				$this->main_title = $this->_msg['Edit_subscription'];
				break;

// Обновление новости
			case "update":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_title, $request_txt_content,
						$request_news_list, $request_body, $request_start, $db;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_subscription", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$query = "SELECT filename1, filename2, filename3
							FROM ".$this->table_prefix."_subscription
							WHERE id=".$request_id;
				$db->query($query);
				$db->next_record();
				$old_filename1 = $db->f("filename1");
				$old_filename2 = $db->f("filename2");
				$old_filename3 = $db->f("filename3");
				if(!is_dir($CONFIG["subscription_files_path"]."folder_". $request_id)) {
					mkdir($CONFIG["subscription_files_path"]."folder_".$request_id, 0777) or die("Can not create folder");
				}
				if($_FILES['file1']['name']) {
					@unlink($CONFIG["subscription_files_path"]."folder_". $request_id . "/" . $old_filename1);
					$filename1 = $main->upload_file('file1', 0, $CONFIG["subscription_files_path"]."folder_".$request_id."/");
					$query = "UPDATE ".$this->table_prefix."_subscription
								SET filename1 = '$filename1'
								WHERE id = '$request_id'";
					$db->query($query);
				}
				if($_FILES['file2']['name']) {
					@unlink($CONFIG["subscription_files_path"]."folder_". $request_id . "/" . $old_filename2);
					$filename2 = $main->upload_file('file2', 0, $CONFIG["subscription_files_path"]."folder_".$request_id."/");
					$query = "UPDATE ".$this->table_prefix."_subscription
								SET filename2 = '$filename2'
								WHERE id = '$request_id'";
					$db->query($query);
				}
				if($_FILES['file3']['name']) {
					@unlink($CONFIG["subscription_files_path"]."folder_". $request_id . "/" . $old_filename3);
					$filename3 = $main->upload_file('file3', 0, $CONFIG["subscription_files_path"]."folder_".$request_id."/");
					$query = "UPDATE ".$this->table_prefix."_subscription
								SET filename3 = '$filename3'
								WHERE id = '$request_id'";
					$db->query($query);
				}
				if ($this->update_subscription($request_category, $request_title, $request_txt_content, $request_news_list, $request_id)) {
					@$cache->delete_cache_files();
					$lc->add_internal_links($this->module_name, "subscription", "body", $request_id, $this->_msg["link_in_news"], $request_body);
				}
				header("Location: $baseurl&action=showsubscription&category=$request_category&start=$request_start");
				exit;
				break;

// Удаляем подписку
			case "del":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("d", "id", $this->table_prefix."_subscription", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($this->delete_subscription($request_id)) {
					@$cache->delete_cache_files();
					$lc->delete_internal_links($this->module_name, 'subscription', $request_id);
				}
				header('Location: '.$baseurl.'&action=showsubscription&category='.$request_category.'&start='.$request_start);
				exit;
				break;

// Отключение публикации
			case "suspend":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("p", "id", $this->table_prefix."_subscription", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($main->suspend('subscription', $request_id)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showsubscription&category=$request_category&start=$request_start");
				exit;
				break;

// Включение публикации
			case "activate":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("p", "id", $this->table_prefix."_subscription", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($main->activate('subscription', $request_id)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showsubscription&category=$request_category&start=$request_start");
				exit;
				break;

// Редактирование прав элемента рассылки
			case 'editrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_subscription", $request_id, true, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($this->table_prefix."_subscription", "title", $request_id,
											"&id=".$request_id."&category=".$request_category."&start=".$request_start, $this->_msg["subscription"])) {
					header('Location: '.$baseurl.'&action=showsubscription&category='.$request_category.'&start='.$request_start);
				}
				break;

// Сохранение прав элемента рассылки
			case 'saverights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start, $request_rights;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_subscription", $request_id, true, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($this->table_prefix."_subscription", $request_id, $request_rights);
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
						"res" => 1,
						"id" => $request_id,
					);
				} else {
					header('Location: '.$baseurl.'&action=showsubscription&category='.$request_category.'&start='.$request_start);
				}
				exit;
				break;


// Показать категории новостей
			case 'showctg':
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2($this->module_name.'_categories.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					"MSG_Empty_field" => $this->_msg["Empty_field"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Rights" => $main->_msg["Rights"],
					"MSG_Category" => $this->_msg["Category"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Create_new_category" => $this->_msg["Create_new_category"],
					"MSG_Create" => $this->_msg["Create"],
					'_ROOT.form_action'	=>	$baseurl.'&action=addctg',
				));
				$res = $this->show_categories('block_categories');
				if (!$res && isset($_REQUEST['empty'])) {
				    $tpl->newBlock('warning');
				    $tpl->assign('txt', 'Список категорий пуст.');
				}
				$this->main_title = $this->_msg["Subscription_categories"];
				break;

// Добавить категорию новостей
			case "addctg":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_title, $request_block_id;
				if ($this->add_category($request_title, intval(substr($request_block_id, 10)))) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showctg");
				exit;
				break;

// Вывод категории на экран для редактирования
			case "editctg":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_category;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_subscription_categories", $request_category))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name."_edit_category.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					"MSG_Empty_field" => $this->_msg["Empty_field"],
					"MSG_Save" => $this->_msg["Save"],
					"MSG_Cancel" => $this->_msg["Cancel"],
					"_ROOT.form_action"	=>	"$baseurl&action=updatectg",
					"_ROOT.cancel_link"	=>	"$baseurl&action=showctg",
				));
				$this->show_category($request_category);
				$this->main_title = $this->_msg["Edit_category"];
				break;

// Сделать update для категории
			case "updatectg":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_title;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_subscription_categories", $request_category))
					$main->message_access_denied($this->module_name, $action);

				if ($this->update_category($request_category, $request_title)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showctg");
				exit;
				break;

// Удаляем категорию
			case "delctg":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_step, $request_category;
				if (!$this->test_item_rights("d", "id", $this->table_prefix."_subscription_categories", $request_category))
					$main->message_access_denied($this->module_name, $action);

				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name.'_del_category.html', $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(array(
						"MSG_Delete_category_start" => $this->_msg["Delete_category_start"],
						"MSG_Delete_category_end" => $this->_msg["Delete_category_end"],
						"MSG_Confirm_delete_category" => $this->_msg["Confirm_delete_category"],
						"MSG_Delete" => $this->_msg["Delete"],
						"MSG_Cancel" => $this->_msg["Cancel"],
						'_ROOT.form_action' =>	$baseurl.'&action=delctg&step=2&category='.$request_category,
						'_ROOT.cancel_link' =>	$baseurl.'&action=showctg',
					));
					$this->show_category($request_category);
					$this->main_title = $this->_msg['Delete_category'];
				} else if ($request_step == 2) {
					if ($this->delete_category($request_category)) @$cache->delete_cache_files();
					header('Location: '.$baseurl.'&action=showctg');
					exit;
				}
				break;

// Редактирование прав категории
			case 'editctgrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_start;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_subscription_categories", $request_category, true))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($this->table_prefix."_subscription_categories", "title", $request_category,
											"&category=".$request_category, $this->_msg["Category"], "savectgrights")) {
					header('Location: '.$baseurl);
				}
				break;

// Сохранение прав категории
			case 'savectgrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_start, $request_rights;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_subscription_categories", $request_category, true))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($this->table_prefix."_subscription_categories", $request_category, $request_rights);
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
						"res"	=> 1,
						"category"	=> $request_category,
					);
				} else {
					header('Location: '.$baseurl);
				}
				exit;
				break;


// Вывод списка адресов рассылки (emails)
			case "mlist":

				if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
				global $request_start, $request_eml;
				$start = ($request_start) ? (int)$request_start : 1;
				$main->include_main_blocks_2($this->module_name.'_mailing_list.html', $this->tpl_path);
				$tpl->assignInclude('search_form', $tpl_path.'_search_form.html');
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Enter_email" => $this->_msg["Enter_email"],
					"MSG_Enter_valid_email" => $this->_msg["Enter_valid_email"],
					"MSG_Add_email" => $this->_msg["Add_email"],
					"MSG_Name" => $this->_msg["Name"],
					"MSG_Email" => $this->_msg["Email"],
					"MSG_Add" => $this->_msg["Add"],
					"MSG_Email_posting" => $this->_msg["Email_posting"],
					"MSG_Begin_posting" => $this->_msg["Begin_posting"],
					"MSG_Confirm_begin_posting" => $this->_msg["Confirm_begin_posting"],
				));
                if ($request_eml == 'exist') {
                    $tpl->newBlock('block_eml_exist');
                } elseif ($request_eml == 'added') {
                    $tpl->newBlock('block_eml_added');
                } elseif ($request_eml == 'false') {
                    $tpl->newBlock('block_eml_false');
                }
				$tpl->assign(Array(
					"MSG_Such_email_already_exists" => $this->_msg["Such_email_already_exists"],
					"MSG_Info_confirm_subscription" => $this->_msg["Info_confirm_subscription"],
					"MSG_Fail_add_email" => $this->_msg["Fail_add_email"],
				));
                $tpl->newBlock('block_all_categ');
				$tpl->assign(Array(
					"MSG_All_categories" => $this->_msg["All_categories"],
				));
				$tpl->assign_array('block_categ_list', $this->getCategories());
				$tpl->assign(
					array(
						'_ROOT.form_search_action'	=> $baseurl.'&action=search',
						'_ROOT.form_add_action'		=> $baseurl.'&action=addemail',
						'_ROOT.lang'				=> $lang,
						'_ROOT.name'				=> $this->module_name,
						'_ROOT.action'				=> 'search',
						'_ROOT.mail_begin'			=> $baseurl.'&action=mbegin',
						'_ROOT.mail_launch'			=> $baseurl.'&action=mlaunch_all',
					)
				);

				$this->show_mailing_list($start);
                $main->_show_nav_string($this->table_prefix.'_subscription_mailing_list AS a LEFT JOIN '.$this->table_prefix.'_subscription_categories AS b
                						ON a.cid = b.id',
										'',
										'',
										'action='.$action,
										$start,
										$CONFIG['subscription_email_max_rows'],
										' a.'.$CONFIG['subscription_email_order_by'],
										'');
				$this->main_title = $this->_msg['Subscription_list'];
				break;

			case "addemail":
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_email, $request_sub_name;
                $categs = $this->search_such_email($request_category, $request_email);
                if ($categs) {
                    header('Location: '.$baseurl.'&action=mlist&eml=exist');
                } elseif ($this->add_email($request_category, $request_email, $request_sub_name)) {
                    @$cache->delete_cache_files();
                    header('Location: '.$baseurl.'&action=mlist&eml=added');
                } else {
                    header('Location: '.$baseurl.'&action=mlist&eml=false');
                }
                exit;
				break;

// Вывод результата поиска по списку рассылки (по e-mail)
			case "search":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_search;
				$search = strip_tags(trim($request_search));
				$main->include_main_blocks_2($this->module_name . "_mailing_list.html", $this->tpl_path);
				$tpl->assignInclude("search_form", $tpl_path . "_search_form.html");
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Enter_email" => $this->_msg["Enter_email"],
					"MSG_Enter_valid_email" => $this->_msg["Enter_valid_email"],
					"MSG_Add_email" => $this->_msg["Add_email"],
					"MSG_Name" => $this->_msg["Name"],
					"MSG_Email" => $this->_msg["Email"],
					"MSG_Add" => $this->_msg["Add"],
					"MSG_Email_posting" => $this->_msg["Email_posting"],
					"MSG_Begin_posting" => $this->_msg["Begin_posting"],
					"MSG_Confirm_begin_posting" => $this->_msg["Confirm_begin_posting"],
				));
				$result = $this->show_mailing_list(1, $search);
				if (!$result) $no_res = $this->_msg["nothing_is_found"];
                $tpl->newBlock('block_all_categ');
                $tpl->assign(Array(
					"MSG_All_categories" => $this->_msg["All_categories"],
				));
                $tpl->assign_array('block_categ_list', $this->getCategories());
				$tpl->assign(
					array(
						"_ROOT.form_search_action"	=>	$baseurl."&action=search",
						'_ROOT.form_add_action'		=> $baseurl.'&action=addemail',
						"_ROOT.lang"				=>	$lang,
						"_ROOT.name"				=>	$this->module_name,
						"_ROOT.action"				=>	"search",
						"_ROOT.search"				=>	$search,
						"_ROOT.search_result"		=>	$this->_msg["Search_result"]." \"$search\": $no_res",
						"_ROOT.mail_begin"			=>  $baseurl.'&action=mbegin',
						"_ROOT.mail_launch"			=>  $baseurl.'&action=mlaunch_all',
					)
				);
				$this->main_title = $this->_msg["Subscription_list_search_result"];
				break;

// Приглашение начать рассылку
			case "mbegin":
				if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2($this->module_name.'_mailing.html', $this->tpl_path, 'main');
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Subscription_title" => $this->_msg["Subscription_title"],
				));
				$tpl->newBlock('block_mailing_begin');
				$tpl->assign(Array(
					"MSG_Mailing_begin" => $this->_msg["Mailing_begin"],
				));
				break;

// Почтовая рассылка
			case 'mlaunch':
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_all, $request_category, $request_id, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("p", "id", $this->table_prefix."_subscription", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

                if (!$request_all) {
					$main->include_main_blocks_2($this->module_name.'_mailing.html', $this->tpl_path, 'main');
					$tpl->prepare();
					$tpl->assign(array(
						"MSG_Subscription_title" => $this->_msg["Subscription_title"],
					));
                }
				$start = (int) $request_start;
				$limit = $CONFIG["subscription_perpage"];
				#var_dump($start, $limit);
				$emails = $this->getEmails(@$request_category, $start, $limit);
				#var_dump($emails);exit;
				$subscr = $this->getSubscription(@$request_id);
				echo $this->_msg["subscription"].': ' . $subscr['title'] . '<br>';
				echo $this->_msg["It_is_sended_letters"].': ' . ($start + $limit) . '<br>';
				$subscription_titles = $this->launchMailSubscription($subscr, $emails);
				if(is_array($emails["emails"]) && sizeof($emails["emails"]) == $limit)
				{
					echo '<a href = "'.$baseurl.'&action=mlaunch&start='.($start +  $limit).'&id='.$request_id.'&category='.$request_category.'">'.$this->_msg['Reload_page'].'</a>';
					echo '<script>document.location.href = "'.$baseurl.'&action=mlaunch&start='.($start +  $limit).'&id='.$request_id.'&category='.$request_category.'";</script>';
				}
                else
				{
					$db->query('UPDATE '.$server.$lang.'_subscription SET was_mailed = 1 WHERE id = '.$subscr['id']);
					echo '<a href = "'.$baseurl.'&action=showsubscription&category='.$request_category.'">'.$this->_msg['Reload_page'].'</a>';
					echo '<script>document.location.href = "'.$baseurl.'&action=showsubscription&category='.$request_category.'";</script>';
                }
                exit;
				break;

// Почтовая рассылка
			case "mlaunch_all":
                if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
                $main->include_main_blocks_2($this->module_name.'_mailing.html', $this->tpl_path, 'main');
                $tpl->prepare();
				$tpl->assign(Array(
					"MSG_Subscription_title" => $this->_msg["Subscription_title"],
				));
                $arr = $this->getAllCategsAndSubscr();

                $this->launchSubscrByHttp($arr);
                exit;
                break;

// Удаляем email из списка рассылки
			case "delemail":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start;
				$this->remove_email($request_id, '', true);
				header('Location: '.$baseurl.'&action=mlist&start='.$request_start);
				exit;
				break;

// Удалить выбранные email'ы из списка рассылки
			case "delete_checked":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_start;
				if(is_array($request_check)) {
					foreach($request_check as $k => $v) {
						$this->remove_email($v, '', true);
					}
				}
				header('Location: '.$baseurl.'&action=mlist&start='.$request_start);
				exit;
				break;

// Активировать выбранные email'ы из списка рассылки
			case "activity_checked":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_start;
				if(is_array($request_check)) {
					foreach($request_check as $k => $v) {
						#$this->remove_email($v, '', true);
						$this->changeActive($v);
					}
				}
				header('Location: '.$baseurl.'&action=mlist&start='.$request_start);
				exit;
				break;

// Подтвердить выбранные email'ы из списка рассылки
			case "suspending_checked":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_start;
				if(is_array($request_check)) {
					foreach($request_check as $k => $v) {
						#$this->remove_email($v, '', true);
						$this->changeConf($v);
					}
				}
				header('Location: '.$baseurl.'&action=mlist&start='.$request_start);
				exit;
				break;

// Сменить состояние почты (подтвердил или нет)
			case "changeConf":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_id;
				$this->changeConf($request_id);
				header('Location: '.$_SERVER['HTTP_REFERER']);
				exit;
				break;

// Сменить состояние почты (активен или нет)
			case "changeActive":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_id;
				$this->changeActive($request_id);
				header('Location: '.$_SERVER['HTTP_REFERER']);
				exit;
				break;

// Редактирование свойств блока
			case 'properties':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $module_id, $request_field, $request_block_id, $title;

				$main->include_main_blocks('_block_properties.html', 'main');
				$tpl->prepare();

				if ($main->show_actions($request_field, '', $this->block_main_module_actions, $this->block_module_actions)) {
					$main->show_linked_pages($module_id, $request_field);
				}
				$tpl->assign(array(	'_ROOT.title'		=> $title,
									'_ROOT.username'	=> $_SESSION['session_login'],
									'_ROOT.password'	=> $_SESSION['session_password'],
									'_ROOT.name'		=> $this->module_name,
									'_ROOT.lang'		=> $lang,
									'_ROOT.block_id'	=> $request_block_id,
									));
				$this->main_title = $this->_msg["Block_properties"];
				break;

// Создание объекта для дальнейшего использования
			case 'only_create_object':
				break;

// Default...
			default:
				$main->include_main_blocks();
				$tpl->prepare();
			}
//--------------------------- конец обработки действий из админ части --------------------------//
		}
	}

    function getCategory($prp = array()) {
    	global $CONFIG, $db, $server, $lang;
		if (isset($prp[1])) {
			if (is_numeric($prp[1])) {
            	$pr = (int)$prp[1];
				$this->get_list_with_rights(
								"c.id, c.title",
								$this->table_prefix."_subscription_categories c", "c",
								"id = ".$pr);
			} else if ($prp[1] == 'all_cat') {
				$this->get_list_with_rights(
								"c.id, c.title",
								$this->table_prefix."_subscription_categories c", "c");
			}
        	if ($db->nf() > 0) {
                for ($i = 0; $i < $db->nf(); $i++) {
					$db->next_record();
                    $cat[$i]['category_id']	= $db->f('id');
					$cat[$i]['category']	= $db->f('title');
                }
            	return $cat;
        	}
		}
    	return FALSE;
	}

	// функция для поиска активных e-mail в списке почтовой рассылки
	function search_such_email($cat, $sub_email) {
		global $CONFIG, $db, $server, $lang;
		$sub_email = $db->escape(strip_tags(substr(trim($sub_email), 0, 100)));
		if (!$sub_email) return FALSE;
        if (is_array($cat) && sizeof($cat) > 0 && $cat[0] == 'all_categs') {
            $db->query('SELECT id, email
                            FROM '.$this->table_prefix.'_subscription_mailing_list
                            WHERE email = "'.$sub_email.'" AND
                                  confirmation = 1 AND
                                  active = 1');
            if ($db->nf() > 0) {
                $db->next_record();
                $categs = $db->f('email');
                return $categs;
            }
        } elseif (is_array($cat) && sizeof($cat) > 0) {
			foreach ($cat AS $k => $v) {
            	$cid =  (int)$v;
				$db->query('SELECT id, email
        						FROM '.$this->table_prefix.'_subscription_mailing_list
        						WHERE email = "'.$sub_email.'" AND
        							  cid = '.$cid.' AND
                                      confirmation = 1 AND
                                      active = 1');
            	$id = '';
            	if ($db->nf() > 0) {
					$db->next_record();
					$categs .= $db->f('email');
					if ($i < ($db->nf()-1)) $categs .= ', ';
				}
			}
        	if (strlen($categs) > 0) return $categs;
		}
		return FALSE;
	}

	// функция для добавления e-mail в список почтовой рассылки
	function add_email($cid, $sub_email, $sub_name) {
		global $CONFIG, $db, $tpl, $main, $core, $server, $lang;
        if (!is_array($cid) || sizeof($cid) < 1) return FALSE;
		$sub_email	= $db->escape(strip_tags(substr(trim($sub_email), 0, 100)));
		if (!$sub_email) return FALSE;
		$uid = md5(uniqid(rand(), true));
        if ($cid[0] == 'all_categs') {
            $ctgs = $this->getCategories();
            for($i = 0; $i < sizeof($ctgs); $i++) {
                $ctg = $ctgs[$i]['category_id'];
                $db->query('INSERT INTO '.$this->table_prefix.'_subscription_mailing_list (cid, email, name, uid, date)
                                   VALUES ('.$ctg.', "'.$sub_email.'", "'.$sub_name.'", "'.$uid.'", NOW())');
                if ($i < (sizeof($ctgs)-1)) {
                    $categs .= $ctg.', ';
                } else {
                    $categs .= $ctg;
                }
            }
        } else {
              $i = 0;
              foreach ($cid AS $k => $v) {
			    $cid_2 = NULL;
                $cid_2 = (int)$v;
                if (isset($cid_2)) {
				    $db->query('INSERT INTO '.$this->table_prefix.'_subscription_mailing_list (cid, email, name, uid, date)
					     			VALUES ("'.$cid_2.'", "'.$sub_email.'", "'.$sub_name.'", "'.$uid.'", NOW())');
               	    if ($i < (sizeof($cid)-1) && is_numeric($cid_2)) {
                	    $categs .= $cid_2.', ';
                    } else {
                	    $categs .= $cid_2;
                    }
			    }
                $i++;
		    }
        }
		if ($categs) {
			$db->query('SELECT title FROM '.$this->table_prefix.'_subscription_categories WHERE id IN ('.$categs.')');
            if ($db->nf() > 0) {
            	for ($i = 0; $i < $db->nf(); $i++) {
                	$db->next_record();
                    $title[$i]['site_subscribe'] = $db->f('title');
                }
            }
		}
		$confirmation_link = $core->formPageLink($CONFIG['subscription_page_link'],
												   $CONFIG['subscription_page_address'],
												   $lang, 1);
   		$tpl_2 = new AboTemplate(RP.'tpl/'.SITE_PREFIX.'/subscription_mails_2.html');
        $tpl_2->prepare();
		if(strlen($sub_name))
		{
			$tpl_2->newBlock("block_with_name");
			$tpl_2->assign("sub_name", $sub_name);
		}
		$tpl_2->assign(array(
							'_ROOT.web_address'	=> $CONFIG['web_address'],
							'_ROOT.sitename_rus'	=> $CONFIG['sitename_rus'],
							'_ROOT.link'			=> 'http://'.$_SERVER['HTTP_HOST'].$confirmation_link.'&action=confirm&uid='.$uid.'&nocash',
							));
		if (is_array($title)) {
			$tpl_2->assign_array('block_categs', $title);
		}
		$body = $tpl_2->getOutputContent();
        $mail = new Mailer();
        $mail->CharSet		= $CONFIG['email_charset'];
        $mail->ContentType	= $CONFIG['email_type'];
		$mail->From			= $CONFIG['return_email'];
		$mail->FromName		= $CONFIG['sitename_rus'];
		$mail->Mailer		= 'mail';
		$mail->AddAddress($sub_email);
        $mail->Subject		= $this->_msg["Confirm_subscription"].' "'.$CONFIG['sitename_rus'];
		$mail->Body			= $body;
		$mail->Send();
#		error_log(serialize($mail)."\n__________________________________________\n",3,RP.'test.txt');
		return TRUE;
	}

// функция для подтверждения подписки
	function confirm_subscription($uid) {
		GLOBAL $CONFIG, $db, $server, $lang;
		$uid = $db->escape(strip_tags(substr($uid, 0, 50)));
		if (!$uid) return FALSE;
		$db->query('UPDATE '.$this->table_prefix.'_subscription_mailing_list
						SET active = 1,
							confirmation = 1
						WHERE uid="'.$uid.'"');
        if ($db->affected_rows() > 0) {
            $db->query('SELECT a.email AS email, b.title AS title
            				FROM '.$this->table_prefix.'_subscription_mailing_list AS a,
            					 '.$this->table_prefix.'_subscription_categories AS b
            				WHERE a.cid = b.id AND
            					  a.uid = "'.$uid.'" AND
            					  a.active = 1 AND
            					  a.confirmation = 1');
        	if ($db->nf() > 0) {
            	for($i = 0; $i < $db->nf(); $i++) {
                    $db->next_record();
    				if (($db->nf()-1) > $i) {
                         $categs .= $db->f('title').', ';
                    } else {
                        $categs .= $db->f('title');
                    }
                    $email = $db->f('email');
            	}
                $this->removeDublicateEmail($email, $uid);
            	return $categs;
        	}
        }
		return FALSE;
	}

    // функция для удаления e-mail из списка почтовой рассылки
    function removeDublicateEmail($email = '', $uid = '') {
        global $CONFIG, $db, $server, $lang;
        $email   = $db->escape($email);
        $uid     = $db->escape($uid);
        if (!$email || !$uid) {
             return FALSE;
        }
        $db->query('DELETE FROM '.$this->table_prefix.'_subscription_mailing_list
                        WHERE email = "'.$email.'" AND
                              uid  != "'.$uid.'" AND
                              active = 0 AND
                              confirmation = 0');
        return TRUE;
    }


// функция для удаления e-mail из списка почтовой рассылки
	function remove_email($id = '', $uid = '', $del = false) {
		global $CONFIG, $db, $server, $lang;
		$id = (int)$id;
		if ($id) {
			$cond = 'id = '.$id;
		} else {
			$uid = $db->escape(strip_tags(substr($uid, 0, 50)));
			if (!$uid) return FALSE;
			$cond = 'uid = "'.$uid.'"';
		}
		if ($del) {
		  $db->query('DELETE FROM '.$this->table_prefix.'_subscription_mailing_list WHERE '.$cond);
		} else {
		  $db->query('UPDATE '.$this->table_prefix.'_subscription_mailing_list SET active=0	WHERE '.$cond);
		}
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}


// функция для получения html кода шаблона для формирования e-mail рассылки
	function get_mail_template($subject = '', $body = '', $additional_text = '') {
		global $CONFIG, $main, $lc, $lang;
		$html = '';
		$tpl2 = new AboTemplate($CONFIG["tpl_path"].'/'.$lang.'/'.$this->module_name."_mail_template.html");
		$tpl2->prepare();
		$tpl2->assign(
			array(
				'mail_date'			=> str_replace('%', '', date($CONFIG['subscription_date_format'])),
				'contact_email'		=> $CONFIG['contact_email'],
				'contact_phone'		=> $CONFIG['contact_phone'],
				'contact_address'	=> $CONFIG['contact_address'],
				'subject'			=> $subject,
				'body'				=> $body,
				'additional_text'	=> $additional_text
			)
		);
		$html = $tpl2->getOutputContent();
		unset($tpl2);
		$html = $lc->replace_internal_links_to_global($html);
		$html = str_replace('href="css/', "href=\"{$CONFIG["web_address"]}css/", $html);
		$html = str_replace('src="i/', "src=\"{$CONFIG["web_address"]}i/", $html);
		$html = str_replace('background="i/', "background=\"{$CONFIG["web_address"]}i/", $html);
		return $html;
	}


	function getPropertyFields() {
		GLOBAL $request_sub_action;
		if ('' != $request_sub_action) {

			$tpl = $this->getTmpProperties();

			switch ($request_sub_action) {
				case 'subform':
					$tpl->newBlock('block_properties');
					$tpl->assign(array( "MSG_Subscription_on"	=> $this->_msg["Subscription_on"],
										"MSG_All_categories"	=> $this->_msg["All_categories"],
									));
					$list = $this->getCategories();
					abo_str_array_crop($list);
					$tpl->assign_array('block_categories', $list);
					$tpl->newBlock('block_page_edit');
					$tpl->assign(array( "MSG_goto_edit"	=> $this->_msg["goto_edit"],
										"site_target"	=> $request_site_target,
								));
					break;
				case 'subform_name':
					$tpl->newBlock('block_properties');
					$tpl->assign(array( "MSG_Subscription_on"	=> $this->_msg["Subscription_on"],
										"MSG_All_categories"	=> $this->_msg["All_categories"],
									));
					$list = $this->getCategories();
					abo_str_array_crop($list);
					$tpl->assign_array('block_categories', $list);
					$tpl->newBlock('block_page_edit');
					$tpl->assign(array( "MSG_goto_edit"	=> $this->_msg["goto_edit"],
										"site_target"	=> $request_site_target,
								));
					break;

				default:
					$tpl->newBlock('block_properties_none');
					$tpl->assign(array(
						"MSG_No" => $this->_msg["No"],
					));
					break;
			}
			return $this->getOutputContent($tpl->getOutputContent());
		}
		return FALSE;
	}

	function getEditLink($sub_action = NULL, $block_id = NULL) {
		GLOBAL $request_name, $db, $lang, $server;
		$block_id	= (int)$block_id;
		if ($sub_action && $block_id) {
			$db->query('SELECT	property1,
								property2,
								property3,
								property4,
								property5
							FROM '.$this->table_prefix.'_pages_blocks
							WHERE id = '.$block_id);
			if ($db->nf() > 0) {
				$db->next_record();
				$arr[]	= $db->f('property1');
				$arr[]	= $db->f('property2');
				$arr[]	= $db->f('property3');
				$arr[]	= $db->f('property4');
				$arr[]	= $db->f('property5');

				switch ($sub_action) {
					case 'subform':
						$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=showsubscription&menu=false&category=';
						$link['material_id']	= $arr[0];
						break;
					case 'subform_name':
						$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=showsubscription&menu=false&category=';
						$link['material_id']	= $arr[0];
						break;
				}
				return $link;
			}

			switch ($sub_action) {
				case 'subform':
					$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=showsubscription&menu=false&category=';
					$link['material_id']	= '';
					break;
				case 'subform_name':
					$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=showsubscription&menu=false&category=';
					$link['material_id']	= '';
					break;
			}
			return $link;
		}
		return FALSE;
	}

	function getAllCategsAndSubscr() {
        global $CONFIG, $db, $server, $lang;
        $db->query('SELECT a.id AS cat_id, a.title AS cat_title, b.id AS id, b.title AS title
                        FROM '.$this->table_prefix.'_subscription_categories AS a,
                        	 '.$this->table_prefix.'_subscription AS b
                        WHERE a.id = b.category_id AND
                              b.was_mailed = 0
                        ORDER BY cat_id');
        if ($db->nf() > 0) {$i=0;
			while($db->next_record() && ++$i){
                $arr[$i]['cat_id'] 	  = $db->f('cat_id');
                $arr[$i]['cat_title'] = $db->f('cat_title');
                $arr[$i]['id']        = $db->f('id');
                $arr[$i]['title']     = $db->f('title');
            }
            return $arr;
        }
        return FALSE;
    }

    function launchSubscrByHttp($arr = array()) {
        global $CONFIG, $db, $server, $lang, $baseurl, $request_start, $request_i;
        $categ  = NULL;
        $size   = sizeof($arr);
		$i = (int) $request_i;
		$start = (int) $request_start;
		$limit = ($CONFIG["subscription_perpage"]) ? $CONFIG["subscription_perpage"] : 50;

        $tpl_2 = new AboTemplate(RP.'mod/subscription/tpl/subscription_mailing.html');
        $tpl_2->prepare();
		if (is_array($arr) && $size > 0)
		{
		    if (!$i && !$start) {
				$_SESSION["time_1"] = time();
				if($size) {
				    $i=1;
				}
			}
			$emails = $this->getEmails(@$arr[$i]['cat_id'], $start, $limit);
			echo 'Рассылка: ' . $arr[$i]['title'] . '<br>';
			echo 'Разослано писем: ' . ($start + count($emails)) . '<br>';
			$subscr = $this->getSubscription(@$arr[$i]['id']);
			$subscription_titles = $this->launchMailSubscription($subscr, $emails);
			if(isset($arr[$i]) && (int) sizeof($emails["emails"]) == $limit) {
				echo '<a href = "'.$baseurl.'&action=mlaunch_all&start='.($start +  $limit).'&i='.$i.'">'.$this->_msg['Reload_page'].'</a><br>';
				echo '<script>document.location.href = "'.$baseurl.'&action=mlaunch_all&start='.($start +  $limit).'&i='.$i.'";</script>';
			} elseif($i && $i < $size) {
			    echo '<a href = "'.$baseurl.'&action=mlaunch_all&start=0&i='.($i + 1).'">'.$this->_msg['Reload_page'].'</a><br>';
				echo '<script>document.location.href = "'.$baseurl.'&action=mlaunch_all&start=0&i='.($i + 1).'";</script>';
			} else {
				foreach($arr as $k => $v){
					$db->query('UPDATE '.$this->table_prefix.'_subscription SET was_mailed = 1 WHERE id = '.$v['id']);
					if ($categ != $v['cat_id'])
					{
						$categ = $v['cat_id'];
						$tpl_2->newBlock('block_mailing_new_categ');
						$tpl_2->assign(array(
									'categ_name'	=> $v['cat_title'],
									"MSG_Category"	=> $this->_msg["Category"],
								));
					}
					$tpl_2->newBlock('block_mailing_new_subscr');
					$tpl_2->assign(array(
								'subscr' => $v['title'],
								"MSG_Subscription_success" => $this->_msg["Subscription_success"],
							));
				}
    	        $time_2 = time();
				$time   = strftime("%M мин. %S сек.", ($time_2 - $_SESSION["time_1"]));
				$tpl_2->newBlock('block_mailing_success');
				$tpl_2->assign(array(
								'time'  => $time,
								'size' => $size,
								"MSG_Subscription_success_time" => $this->_msg["Subscription_success_time"],
								"MSG_Count_sended_news" => $this->_msg["Count_sended_news"],
							));
				$tpl_2->printToScreen();
			}
            return TRUE;
        } else {
			$tpl_2->newBlock('block_mailing_error');
			$tpl_2->assign(array("MSG_Mailing_error" => $this->_msg["Mailing_error"]));
			$tpl_2->printToScreen();
        }
        return FALSE;
    }

	function add_subscription($category, $title, $body, $news_arr, $date, $hrs, $min, $item_rights, $block_id = 0) {
    	global $CONFIG, $main, $db, $cash, $module_id;
		$category = (int)$category;
		if (!$category) return FALSE;

		$db->query("SELECT usr_group_id FROM ".$this->table_prefix."_subscription_categories WHERE id=".$category);
		if (!$db->next_record()) return false;
		$owner_id = $_SESSION["siteuser"]["id"];
		list($group_id, $rights) = $this->get_group_and_rights('subscription_default_rights', $block_id, $db->f("usr_group_id"));
		$rights = $item_rights ? $item_rights : $rights;

		$title	= $db->escape($title);
		$body	= $db->escape($body);
		$news_ids = (is_array($news_arr)) ? implode(',', $news_arr) : '';
		$imagename = ($imagename) ? "'$imagename'" : "NULL";
		$db->query("INSERT INTO ".$this->table_prefix."_subscription (title, body, news_ids, category_id, date, owner_id, usr_group_id, rights)
					VALUES ('$title', '$body', '$news_ids', $category, NOW(), $owner_id, $group_id, $rights)");
		return ($db->affected_rows() > 0) ? $db->lid() : FALSE;
	}

	function update_subscription($category, $title, $body, $news_arr, $id) {
		global $CONFIG, $main, $db, $cash, $module_id, $server, $lang;
		$id = intval($id); if (!$id) return FALSE;
    	$category	= intval($category); if (!$category) return FALSE;
		$title		= $db->escape($title);
		$body		= $db->escape($body);
		$news_ids = (is_array($news_arr)) ? implode(',', $news_arr) : '';
		$date		= $main->transform_date($date, $hrs, $min);
		$query		= "UPDATE ".$this->table_prefix."_subscription
							SET title = '$title',
								body = '$body',
								news_ids = '$news_ids',
								category_id = $category,
								date = NOW()
							WHERE id = $id";
		$db->query($query);
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}

	// функция для удаления новостей из базы данных
	function delete_subscription($id) {
		global $CONFIG, $db, $server, $lang;
		$id		= (int)$id;
        if (!$id) return FALSE;
		$this->delSubscriptionFiles($id);
		$db->query('DELETE FROM '.$this->table_prefix.'_subscription WHERE id = '.$id);
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}

    function delSubscriptionFiles($id) {
    	global $CONFIG, $db, $server, $lang;
    	$id = (int)$id;
    	if (!$id) return FALSE;
		$db->query('SELECT filename1, filename2, filename3
						FROM '.$this->table_prefix.'_subscription
						WHERE id = '.$id);
		if ($db->nf() > 0) {
			$db->next_record();
			if(file_exists($CONFIG['subscription_files_path'].'folder_'.$id.'/'.$db->f('filename1')))
				@unlink($CONFIG['subscription_files_path'].'folder_'.$id.'/'.$db->f('filename1'));
			if(file_exists($CONFIG['subscription_files_path'].'folder_'.$id.'/'.$db->f('filename2')))
				@unlink($CONFIG['subscription_files_path'].'folder_'.$id.'/'.$db->f('filename2'));
			if(file_exists($CONFIG['subscription_files_path'].'folder_'.$id.'/'.$db->f('filename3')))
				@unlink($CONFIG['subscription_files_path'].'folder_'.$id.'/'.$db->f('filename3'));
			if(file_exists($CONFIG['subscription_files_path'].'folder_'.$id))
            	@rmdir($CONFIG['subscription_files_path'].'folder_'.$id);
        	return TRUE;
		}
    	return FALSE;
    }

	// функция для вывода рассылки по id
	function show_subscription($id = '', $start = '') {
		global $CONFIG, $main, $db, $tpl, $server, $lang;
		$id		= (int)$id;
		if (!$id) return FALSE;
		$db->query('SELECT	*,
							DATE_FORMAT(date,"'.$CONFIG['subscription_date_format'].'") as date,
							DATE_FORMAT(date,"'.$CONFIG['calendar_date'].'") as calendar_date,
							DATE_FORMAT(date,"%H") as hrs,
							DATE_FORMAT(date,"%i") as min,
						 	filename1,
						 	filename2,
						 	filename3
						 FROM '.$this->table_prefix.'_subscription
						 WHERE id = '.$id);
		if ($db->nf() > 0) {
			$db->next_record();
			$title		= htmlspecialchars($db->f('title'));
			$announce	= htmlspecialchars($db->f('announce'));

			if ($db->f('filename1')) {
				$file1exists = '&nbsp;<a href="'.$CONFIG['subscription_files_path'].'folder_'.$id.'/'.$db->f('filename1').'" target=_blank>'.$this->_msg['alredy_uploaded'].'</a>';
			} else {
				$file1exists = '';
			}
			if ($db->f('filename2')) {
				$file2exists = '&nbsp;<a href="'.$CONFIG['subscription_files_path'].'folder_'.$id.'/'.$db->f('filename2').'" target=_blank>'.$this->_msg['alredy_uploaded'].'</a>';
			} else {
				$file2exists = '';
			}
			if ($db->f('filename3')) {
				$file3exists = '&nbsp;<a href="'.$CONFIG['subscription_files_path'].'folder_'.$id.'/'.$db->f('filename3').'" target=_blank>'.$this->_msg['alredy_uploaded'].'</a>';
			} else {
				$file3exists = '';
			}

			//вывод на экран
			$tpl->assign(
				array(
					'text'						=> $db->f('body'),
					'subscription_title'		=> $title,
					'subscription_display_date'	=> $db->f('display_date'),
					'date_hrs'					=> $db->f('hrs'),
					'date_min'					=> $db->f('min'),
					'date'						=> $db->f('date'),
					'calendar_date'				=> $db->f('calendar_date'),
					'subscription_announce'		=> $announce,
					'subscription_address'		=> $_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'],
					'subscription_id'			=> $db->f('id'),
					'file1exists'				=> $file1exists,
					'file2exists'				=> $file2exists,
					'file3exists'				=> $file3exists,
					'start'						=> $start,
				)
			);
			return array($db->f('category_id'), $db->f('news_ids'));
		}
		return FALSE;
	}


	// функция для вывода списка новостных заголовков
	function show_subscription_titles($category, $start = "", $rows = "", $order_by = "", $owner_id = 0) {
		$category = intval($category); if (!$category) return 0;
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;
		$start = $start > 0 ? (int) $start : 0;
		if (!$rows) $rows = $CONFIG["subscription_max_rows"];
		if (!$order_by) $order_by = $CONFIG["subscription_admin_order_by"];

		$total_cnt = $this->get_list_with_rights(
								"s.*, DATE_FORMAT(date,'" . $CONFIG["subscription_date_format"] . "') as subscription_date",
								$this->table_prefix."_subscription s", "s",
								"category_id = ".$category, "", $order_by,
								$start, $rows,
								true, $owner_id);
		$pages_cnt = ceil($total_cnt/$rows);

		if ($db->nf() > 0) {
			$actions = array(	"edit"		=> "edit",
								"editrights"=> "editrights",
								"mail"		=> "mlaunch",
								"delete"	=> "del",
								"confirm_delete"	=> $this->_msg["Confirm_delete_record"]
							);
			while($db->next_record()) {
				$id = $db->f('id');
				$title = htmlspecialchars($db->f("title"));
				$adm_actions = $this->get_actions_by_rights($baseurl."&id=".$id."&category=".$db->f("category_id")."&start=".$start, $db->f("active"), $db->f("is_owner"), $db->f("user_rights"), $actions);

				$tpl->newBlock('block_subscription_titles');
				$tpl->assign(array(
						subscription_title			=> $title,
						subscription_date			=> $db->f("subscription_date"),
						subscription_edit_action	=> $adm_actions["edit_action"],
						subscription_edit_rights_action	=> $adm_actions["edit_rights_action"],
						subscription_del_action		=> $adm_actions["delete_action"],
						subscription_id				=> $id,
						start						=> $start,
				        'baseurl'					=> $baseurl,
					));
                if ($db->f('was_mailed')) {
					$tpl->newBlock('block_subscr_passive');
                    $tpl->assign(array(
									"MSG_sent"		=> $this->_msg["sent"],
								));
				} else {
   					$tpl->newBlock('block_subscr_active');
					$tpl->assign(array(
									'mail_action'	=> $adm_actions["mail_action"],
								));
				}
			}
			return $pages_cnt;
		}
		return FALSE;
	}

	// получение массива категорий
	function getCategories($selected_category = '') {
		GLOBAL $CONFIG, $db, $baseurl, $lang, $server;
		$selected_category = (int)$selected_category;
		$db->query('SELECT * FROM '.$this->table_prefix.'_subscription_categories ORDER BY title');
		if ($db->nf() > 0) {
			while($db->next_record()) {
				$selected	= ($db->f('id') == $selected_category) ? ' selected' : '';
				$arr[]		= array('category_id'			=> $db->f('id'),
									'category_title'		=> $db->f('title'),
									'category_link'			=> $baseurl.'&action=showsubscription&category='.$db->f('id'),
									'category_edit_link'	=> $baseurl.'&action=editctg&category='.$db->f('id'),
									'category_del_link'		=> $baseurl.'&action=delctg&category='.$db->f('id'),
									'category'				=> $db->f('id'),
									'selected'				=> $selected,
									);
			}
			return $arr;
		}
		return FALSE;
	}

	// функция для вывода списка категорий
	function show_categories($blockname, $selected_category = '', $only_editable = false) {
		global $CONFIG, $db, $tpl, $baseurl, $lang;
		$selected_category = (int)$selected_category;
		$this->get_list_with_rights(
				"C.*",
				$this->table_prefix."_subscription_categories AS C", "C",
				"", "", "title");
		if ($db->nf()) {
			$actions = array(	"edit"		=> "editctg",
								"editrights"=> "editctgrights",
								"delete"	=> "delctg",
								"confirm_delete"	=> ""
							);
			while($db->next_record()) {
				if ($only_editable && !($db->f("user_rights") & 2)) continue;

				// вывод на экран списка
				$id = $db->f('id');
				$adm_actions = $this->get_actions_by_rights($baseurl."&category=".$id, 0, $db->f("is_owner"), $db->f("user_rights"), $actions);
				$tpl->newBlock($blockname);
				$tpl->assign(
					array(
						"MSG_edit"				=> $this->_msg["edit"],
						"MSG_delete"			=> $this->_msg["delete"],
						'category_title'		=> $db->f('title'),
					    'category_id'		=> $id,
						'category_edit_action'	=>	$adm_actions["edit_action"],
						'category_edit_rights_action'	=>	$adm_actions["edit_rights_action"],
						'category_del_action'	=>	$adm_actions["delete_action"],
						'category_link'			=> $baseurl.'&action=showsubscription&category='.$db->f('id'),
						'category'				=> $id,
						'selected'				=> $id == $selected_category ? " selected" : "",
					)
				);
			}
			return $db->nf();
		}
		return FALSE;
	}

   	// функция для получения списка категорий
	function getEmails($cid = '', $start = 0, $limit = 50) {
		global $CONFIG, $db, $server, $lang;
        $cid = (int)$cid;
		if(!$limit){$limit = 50;}
        if (is_numeric($cid)) {
			$db->query('SELECT email, name, uid FROM '
			    .$this->table_prefix.'_subscription_mailing_list WHERE confirmation = 1 AND active = 1 AND (cid = '
			    .$cid.' OR cid = 0) LIMIT ' . $start . ', ' . $limit);
		}
        if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
            	$emails[]	= $db->f('email');
            	$names[]	= $db->f('name');
            	$uid[]		= $db->f('uid');
			}
			$arr['emails']	= $emails;
			$arr['names']	= $names;
			$arr['uid']		= $uid;
        	return $arr;
		}
		return FALSE;
	}

	// функция для вывода инофрмации о категории по id
	function show_category($id = "", $need_output = true) {
		$id = intval($id);		if (!$id) return 0;
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;

		$query = "SELECT * FROM ".$this->table_prefix."_subscription_categories WHERE id = $id";
		$db->query($query);

		if ($db->nf() > 0) {
			$db->next_record();
			$title	= htmlspecialchars($db->f('title'));
			if ($need_output) {
				$tpl->assign(
					array(
						'category'			=>	$db->f("id"),
						'category_title'	=>	$title,
						'category_link'		=>	$baseurl.'&action=showsubscription&category='.$db->f('id'),
					)
				);
			}
			return array($db->f("title"), $db->f("owner_id"));
		}
		return 0;
	}

	function add_category($title, $block_id = 0) {
		global $CONFIG, $db, $cash, $module_id, $server, $lang;
		if (!$title) return FALSE;
		$title = $db->escape($title);

		$owner_id = $_SESSION["siteuser"]["id"];
		list($group_id, $rights) = $this->get_group_and_rights('subscription_default_rights', $block_id);

		$query = "INSERT INTO ".$this->table_prefix."_subscription_categories (title,owner_id,usr_group_id,rights)
					VALUES ('$title',$owner_id,$group_id,$rights)";
		$db->query($query);
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}

	// функция для update категории по id
	function update_category($id, $title) {
		global $CONFIG, $db, $cash, $module_id, $server, $lang;
		$id		= (int)$id;
		if (!$id || !$title) return FALSE;
		$title	= $db->escape($title);
		$query = "UPDATE ".$this->table_prefix."_subscription_categories
					SET title = '$title'
					WHERE id = $id";
		$db->query($query);
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}


	// функция для удаления категории по id
	function delete_category($id = "") {
		global $CONFIG, $db, $tpl, $server, $lang;
		$id		= (int)$id;
		if (!$id) return FALSE;
		$db->query('DELETE FROM '.$this->table_prefix.'_subscription_mailing_list WHERE cid = '.$id);
		$db->query('DELETE FROM '.$this->table_prefix.'_subscription_categories WHERE id = '.$id);
		if ($db->affected_rows() > 0) {
			$db->query('SELECT id
						FROM '.$this->table_prefix.'_subscription
						WHERE category_id = '.$id);
			if ($db->nf() > 0) {
            	for ($i = 0; $i < $db->nf(); $i++) {
                    $db->next_record();
					$arr[] = $db->f('id');
            	}
            	for ($i = 0; $i < sizeof($arr); $i++) {
                	$this->delete_subscription($arr[$i]);
				}
			}
			return TRUE;
		}
		return FALSE;
	}


	// функция для вывода списка рассылки
	function show_mailing_list($start = '', $search = '') {
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;
		$order_by = $CONFIG['subscription_email_order_by'];
		if ($search != '') {
			$query = 'SELECT a.*, b.title AS email_cat
						FROM '.$this->table_prefix.'_subscription_mailing_list AS a
							LEFT JOIN '.$this->table_prefix.'_subscription_categories AS b ON a.cid = b.id
						WHERE a.email LIKE "%'.$search.'%"
						ORDER BY a.'.$order_by;
		} else {
			$start = ((int)$start) ? $start : 1;
			$rows = $CONFIG['subscription_email_max_rows'];
			$start_row = ($start - 1) * $rows;
			$query = 'SELECT a.*, b.title AS email_cat
						FROM '.$this->table_prefix.'_subscription_mailing_list AS a
							LEFT JOIN '.$this->table_prefix.'_subscription_categories AS b ON a.cid = b.id
						ORDER BY a.'.$order_by.'
						LIMIT '.$start_row.', '.$rows;
		}
		$db->query($query);
		if ($db->nf() > 0) {
			$tpl->newBlock('block_mailing_list');
			$tpl->assign(Array(
				"MSG_Email" => $this->_msg["Email"],
				"MSG_Name" => $this->_msg["Name"],
				"MSG_Category" => $this->_msg["Category"],
				"MSG_Activ" => $this->_msg["Activ"],
				"MSG_Conf" => $this->_msg["Conf"],
				"MSG_Del" => $this->_msg["Del"],
				"MSG_Delete" => $this->_msg["Delete"],
				"MSG_Activity" => $this->_msg["Activity"],
				"MSG_Suspending" => $this->_msg["Suspending"],
				"MSG_Execute" => $this->_msg["Execute"],
			));

			while($db->next_record()) {
				$tpl->newBlock('block_email');
				$img_email = $db->f('confirmation') ? 'b_select.gif' : 'b_select_off.gif';
				$img_active = $db->f('active') ? 'b_select.gif' : 'b_select_off.gif';
					$tpl->assign(
						array(
							'email_id'			=> $db->f('id'),
							'email'				=> $db->f('email'),
							'name'				=> $db->f('name'),
							'email_cat'			=> $db->f('email_cat') ? $db->f('email_cat') : $this->_msg['All_categories'],
							'link_active'		=> $baseurl . '&action=changeActive&id='.$db->f('id'),
							'img_active'		=> $img_active,
							'img_conf'   		=> $img_email,
							'link_conf'  		=> $baseurl . '&action=changeConf&id='.$db->f('id'),
							'email_del_link'	=> $baseurl.'&action=delemail&id='.$db->f('id').'&start='.$start,
							'start'				=> $start,
							'MSG_Confirm_delete_record' => $this->_msg['Confirm_delete_record']
					)
				);
			}
			return TRUE;
		}
		return FALSE;
	}

    function getSubscription($id = NULL) {
    	global $CONFIG, $db, $core, $server, $lang;
        $id = (int)$id;
        if (!$id) return FALSE;
		$db->query('SELECT *
				  		FROM  '.$this->table_prefix.'_subscription
				  		WHERE id = '.$id);
		if ($db->nf() > 0) {
        	$db->next_record();
        	$arr['id']			= $db->f('id');
        	$arr['category_id']	= $db->f('category_id');
        	$arr['date']		= $db->f('date');
        	$arr['title']		= $db->f('title');
        	$arr['body']		= $db->f('body');
        	$arr['news_ids']	= $db->f('news_ids');
        	$arr['filename1']	= $db->f('filename1');
        	$arr['filename2']	= $db->f('filename2');
        	$arr['filename3']	= $db->f('filename3');
        	$arr['was_mailed']	= $db->f('was_mailed');
        	return $arr;
		}
    	return FALSE;
	}

	function launchMailSubscription($subscr, $emails) {
    	GLOBAL $CONFIG, $db, $core, $lang;

        if (is_array($subscr) && (sizeof($subscr) > 0) && is_array($emails) && (sizeof($emails) > 0)) {
			for($i = 0; $i < sizeof($emails['emails']); $i++) {
        		$this->sendSubscription($subscr, $emails['emails'][$i], $emails['names'][$i], $emails['uid'][$i]);
			}
			if ($CONFIG['subscription_sleep']) {
			    sleep($CONFIG['subscription_sleep']);
			}
        	return TRUE;
		}
        return FALSE;
	}

    function sendSubscription($subscr, $user_mail, $user_name, $uid) {
		GLOBAL $CONFIG, $db, $core, $server, $lang;
		static $body_mail;
		if(!$body_mail){
		$unsubscribe_link = $core->formPageLink($CONFIG['subscription_page_link'],
												  $CONFIG['subscription_page_address'],
												  $lang, 1);
   		$tpl_2 = new AboTemplate(RP.'/tpl/'.SITE_PREFIX.'/subscription_mails.html');
        $tpl_2->prepare();

		if(strlen($user_name)) {
			$tpl_2->newBlock("block_with_name");
			$tpl_2->assign("sub_name", '{USERNAME}');
		}

		$db->query('SELECT * FROM '.$this->table_prefix.'_subscription_categories WHERE id = '.$subscr['category_id']);
		if (!$db->nf()) return FALSE;
		$db->next_record();

		$tpl_2->assign(array(
							'_ROOT.web_address'	=> $CONFIG['web_address'],
                            '_ROOT.subscr_categ'	=> '"'.$db->f('title').'"',
                            '_ROOT.title'			=> $subscr['title'],
                            '_ROOT.date'			=> $subscr['date'],
                            '_ROOT.body'			=> $subscr['body'],
							'_ROOT.sitename_rus'	=> $CONFIG['sitename_rus'],
							'_ROOT.unsubscr_link' => 'http://'.$_SERVER['HTTP_HOST'].$unsubscribe_link.'&action=unsubscribe&uid={UID}',
							));

		if ($subscr['news_ids']) {
			$query = "SELECT b.action, b.property1, p.address
						FROM `".$this->table_prefix."_pages_blocks` b, `".$this->table_prefix."_pages` p
						WHERE b.`field_number`=0 AND b.`module_id`=5 AND b.`page_id`=p.id";
			$news_links = $db->getArrayOfResult($query);
			if ($cnt = sizeof($news_links)) {
				$query = "SELECT id,category_id,DATE_FORMAT(date,'".$CONFIG["subscription_date_format"]."') as formated_date,title,announce
							FROM ".$this->table_prefix."_news
							WHERE id IN (".$subscr['news_ids'].")
							ORDER BY date DESC";
				$db->query($query);
				$tpl_2->newBlock('block_news');
				while ($db->next_record()) {
					if ($CONFIG["rewrite_mod"]) {
						$newsurl = $lang."/".$news_links[0]["address"]."?";
						for($i=1; $i < $cnt; $i++) {
							if ($news_links[$i]["property1"] == $db->f('category_id')) {
								$newsurl = $lang."/".$news_links[$i]["address"]."?";
								break;
							}
						}
					} else {
						$newsurl = "index.php?lang=".$lang."&link=news&";
					}
					$tpl_2->newBlock('block_news_row');
					$tpl_2->assign(array(
								'news_date'		=> $db->f('formated_date'),
								'news_title'	=> $db->f('title'),
								'news_announce'	=> $db->f('announce'),
								'news_link'		=> "http://".$_SERVER["HTTP_HOST"]."/".$newsurl."action=show&id=".$db->f('id'),
								));
				}
			}
		}

		$body_mail = $tpl_2->getOutputContent();
		}

		$body = str_replace (
			array('{UID}', '{USERNAME}'),
			array($uid, $user_name),
			$body_mail
		);

        $path = RP.$CONFIG['subscription_files_path'].'folder_'.$subscr['id'].'/';

        $mail = new Mailer();
        $mail->CharSet		= 'utf-8';
        $mail->ContentType	= 'text/html';//$CONFIG['email_type'];
		$mail->From			= $CONFIG['return_email'];
		$mail->FromName		= $CONFIG['sitename_rus'];
		$mail->Mailer		= 'mail';
        $mail->Subject		= $subscr['title'];
		$mail->Body			= $body;
        if ($subscr['filename1']) $mail->AddAttachment($path.$subscr['filename1']);
        if ($subscr['filename2']) $mail->AddAttachment($path.$subscr['filename2']);
        if ($subscr['filename3']) $mail->AddAttachment($path.$subscr['filename3']);
       	$mail->to[0][0] = $user_mail;
       	$mail->Send();
#		error_log(serialize($mail)."\n__________________________________________\n",3,RP.'test.txt');
		return TRUE;
	}

    function get_news() {
		global $server, $lang, $CONFIG, $db;
		$query = "SELECT id,DATE_FORMAT(date,'".$CONFIG["subscription_date_format"]."') as formated_date,title
					FROM ".$this->table_prefix."_news
					WHERE date <= NOW()
					ORDER BY date DESC
					LIMIT 0,".$CONFIG["subscription_news_max_cnt"];
		return $db->getArrayOfResult($query);
	}

    function show_news($block_name, $sel_news_id = '') {
		global $tpl;
		$sel_news_id_arr = explode(',', $sel_news_id);
		if ($news = $this->get_news()) {
			foreach($news as $idx=>$news_info) {
				$tpl->newBlock($block_name);
				$tpl->assign(array(
							'news_id'	=> $news_info["id"],
							'news_date'	=> $news_info["formated_date"],
							'news_title'=> $news_info["title"],
							'selected'	=> (in_array($news_info["id"], $sel_news_id_arr)) ? ' selected' : '',
							));
			}
			return true;
		}
		return false;
	}

	function changeConf($id){
	  global $db;
		if (!$id = (int)$id) {
			return FALSE;
		}
		$db->query('UPDATE '.$this->table_prefix.'_subscription_mailing_list
		        	SET confirmation = IF (confirmation, 0, 1) WHERE id = '.$id);
		return $db->affected_rows() ? TRUE : FALSE;
	}

	function changeActive($id){
	  global $db;
		if (!$id = (int)$id) {
			return FALSE;
		}
		$db->query('UPDATE '.$this->table_prefix.'_subscription_mailing_list
		        	SET active = IF (active, 0, 1) WHERE id = '.$id);
		return $db->affected_rows() ? TRUE : FALSE;
	}
	
	function ctgr_exist($id)
	{   global $db;
		$db->query("SELECT * FROM ".$this->table_prefix."_subscription_categories WHERE id = ".intval($id));
		return $db->nf();
	}
	
	function import($ctgr_id, $file_path)
	{   global $db;
	
	    $ret = false;
	    $ctgr_id = (int) $ctgr_id;
		if ($this->ctgr_exist($ctgr_id) && file_exists($file_path)) {
		    $ret = 0;		    
            $handle = fopen($file_path, "r");
            $ins = array();
            $query = "INSERT INTO {$this->table_prefix}_subscription_mailing_list "
                ."(cid, email, name, date, uid, confirmation, active) VALUES ";
            while (($data = fgetcsv($handle, 1024, ";")) !== FALSE) {
                $email = My_Sql::escape($data[0]);
                if ($email && strpos($email, "@")) {
                    $ret++;
                    $name = $data[1] ? My_Sql::escape($data[1]) : '';
                    $uid = md5($email.$name.rand(1, 100000000000));
                    $ins[] = "({$ctgr_id}, '{$email}', '{$name}', NOW(), '{$uid}', 1, 1)";
                    if (count($ins)>=300) {
                        $db->query($query.implode(", ", $ins));
                        $ins = array();
                    }
                }                
            }
            if (! empty($ins)) {
                $db->query($query.implode(", ", $ins));
            }
            fclose($handle);

		}
		
		return $ret;
	}
}

?>