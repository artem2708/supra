<?php

$MOD_TABLES[] = "CREATE TABLE IF NOT EXISTS `counter_countries` (
					`first_long_ip` bigint(20) unsigned NOT NULL default '0',
          `last_long_ip` bigint(20) unsigned NOT NULL default '0',
          `zone` varchar(2) NOT NULL default '',
          `country` varchar(255) NOT NULL default '',
          KEY `first_long_ip` (`first_long_ip`,`last_long_ip`)
					) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE IF NOT EXISTS `counter_cities` (
          `id` int(10) unsigned NOT NULL auto_increment,
          `first_long_ip` bigint(20) unsigned NOT NULL default '0',
          `last_long_ip` bigint(20) unsigned NOT NULL default '0',
          `zone` char(2) NOT NULL default '',
          `city_id` int(10) unsigned NOT NULL default '0',
          `name` varchar(31) NOT NULL default '',
          `region` varchar(31) NOT NULL default '',
          PRIMARY KEY  (`id`),
          KEY `long_ip` (`first_long_ip`,`last_long_ip`)
					) ENGINE=MyISAM";

?>