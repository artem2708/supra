<?php
require(realpath("../../").'/common.php');
require_once(RP.'inc/lang/lang.ru_RU.php');

$graph			= new Graph($CONFIG, SITE_PREFIX, $_GET['type']);
$graph->getGraph();

class Graph {

    var $graph		= NULL;
    var $lang		= NULL;
    var $type		= NULL;
    var $total      = 0;
    var $color      = array();
    var $conf		= array();
	var $style		= array();
	var $tables		= array();
    var $values     = array();

	function Graph($conf, $lang, $type) {
        global $db;
        $db->query('SELECT name FROM '.$lang.'_counter_tables');
        if ($db->nf() > 0) {
			for($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
				$this->tables[] = $db->f('name');
			}
        }

        $this->conf		 = $conf;
        $this->lang		 = $db->escape($lang);
        $this->type		 = $type;
    	$this->graph	 = ImageCreate(555, 285);
		$this->color[1]	 = ImageColorAllocate($this->graph, 255, 255, 255);
		$this->color[2]	 = ImageColorAllocate($this->graph, 0, 0, 0);
        $this->color[3]	 = ImageColorAllocate($this->graph, 37, 125, 180);
		$this->color[4]	 = ImageColorAllocate($this->graph, 0, 210, 255);
		$this->color[5]	 = ImageColorAllocate($this->graph, 142, 24, 207);
		$this->color[6]	 = ImageColorAllocate($this->graph, 202, 21, 214);
		$this->color[7]	 = ImageColorAllocate($this->graph, 225, 16, 137);
		$this->color[8]	 = ImageColorAllocate($this->graph, 145, 67, 5);
        $this->color[9]  = ImageColorAllocate($this->graph, 93, 159, 131);
        $this->color[10]  = ImageColorAllocate($this->graph, 125, 180, 37);
        $this->color[11]  = ImageColorAllocate($this->graph, 201, 168, 44);
        $this->color[12]  = ImageColorAllocate($this->graph, 210, 255, 0);
	}

    function getArcs($time = NULL) {
        global $db;
		if (!isset($time) || $time == NULL) {
        	$time = $this->conf['counter_default_time'];
		}
       	switch ($time) {
			case 0:																			// ���
                $this->getArcsForHour();
				break;

			case 1:																			// ����
                $this->getArcsForDay();
				break;

			case 2:																			// �����
                $this->getArcsForMounth();
				break;

			case 3:																			// ���
                $this->getArcsForYear();
                break;

			case 4:																			// �� �����
        		$this->getArcsForAllTime();
        		break;

			default:																		// ������
        		$this->getArcsForPeriod($time);
		}
	}

    function getArc() {
        $curr   = 0;
		$size = sizeof($this->values);
		for($i = 0; $i < $size; $i++) {
            $this->values[$i]['percent'] = (360*($this->values[$i]['count'] / $this->total));
            $this->values[$i]['percent'] = $this->values[$i]['percent'] < 1 ? 1 : round($this->values[$i]['percent']);
            imagefilledarc($this->graph, 140, 140, 265, 265, $curr, ($curr + $this->values[$i]['percent']), $this->color[($i+3)], IMG_ARC_PIE);
            $curr = ($this->values[$i]['percent'] + $curr);
        }
        $this->getCountriesNames();
        return TRUE;
    }

    function getCountriesNames() {
        $curr  = 0;
        $size  = sizeof($this->values);
        for($i = 0; $i < $size; $i++) {
            imagefilledrectangle($this->graph, 300, 10+($i*20), 305, 20+($i*20), $this->color[($i+3)]);
            imagettftext($this->graph, 9, 0, 310, 20+($i*20),
                         $this->color[2],
                         $this->conf['counter_font'],
                         $this->values[$i]['country'].' - '.(((int)(100*(100*($this->values[$i]['percent']/360))))/100).'%');
        }
        $this->getCountriesBullet();
        return TRUE;
    }

    function getCountriesBullet() {
        $curr  = 0;
        $size  = sizeof($this->values);
        for($i = 0; $i < $size; $i++) {
            imagefilledrectangle($this->graph, 300, 10+($i*20), 305, 20+($i*20), $this->color[($i+3)]);
        }
        return TRUE;
    }

    function getArcsForHour() {
        global $db;
        $hour  = date('G');
        $day   = date('j');
        $table = date('m_Y');
        $query = 'SELECT IF(ISNULL(zone), "No data", zone) AS country,';
        $query .= ' COUNT('.($this->type == 'people' ? '' : 'DISTINCT ').'ip_addr) AS count';
        $query .= ' FROM  '.$this->lang.'_counter_'.$table;
        $query .= ' WHERE the_time = '.$hour.' AND days = '.$day;
        $query .= ' GROUP BY country ORDER BY count DESC';
        $db->query($query);
        if ($db->nf() > 0) {
            if ($db->nf() > $this->conf['counter_max_countries']) {
                $size = $this->conf['counter_max_countries'];
            } else {
                $size = $db->nf();
            }
            for($i = 0; $i < $size; $i++) {
                $db->next_record();
                $this->total                 += $db->f('count');
                $this->values[$i]['country']  = $db->f('country');
                $this->values[$i]['count']    = $db->f('count');
            }
        	$this->getArc();
            return TRUE;
        }
    }

    function getArcsForDay() {
        global $db;
        $day   = date('j');
        $table = date('m_Y');
        $query = 'SELECT IF(ISNULL(zone), "No data", zone) AS country,';
        $query .= ' COUNT('.($this->type == 'people' ? '' : 'DISTINCT ').'ip_addr) AS count';
        $query .= ' FROM  '.$this->lang.'_counter_'.$table;
        $query .= ' WHERE days = '.$day;
        $query .= ' GROUP BY country ORDER BY count DESC';
        $db->query($query);
        if ($db->nf() > 0) {
            if ($db->nf() > $this->conf['counter_max_countries']) {
                $size = $this->conf['counter_max_countries'];
            } else {
                $size = $db->nf();
            }
            for($i = 0; $i < $size; $i++) {
                $db->next_record();
                $this->total                 += $db->f('count');
                $this->values[$i]['country']  = $db->f('country');
                $this->values[$i]['count']    = $db->f('count');
            }
            $this->getArc();
            return TRUE;
        }
        return FALSE;
    }

    function getArcsForMounth() {
        global $db;
        $table = date('m_Y');
        $query = 'SELECT IF(ISNULL(zone), "No data", zone) AS country,';
        $query .= ' COUNT('.($this->type == 'people' ? '' : 'DISTINCT ').'ip_addr) AS count';
        $query .= ' FROM  '.$this->lang.'_counter_'.$table;
        $query .= ' GROUP BY country ORDER BY count DESC';
        $db->query($query);
        if ($db->nf() > 0) {
            if ($db->nf() > $this->conf['counter_max_countries']) {
                $size = $this->conf['counter_max_countries'];
            } else {
                $size = $db->nf();
            }
            for($i = 0; $i < $size; $i++) {
                $db->next_record();
                $this->total                 += $db->f('count');
                $this->values[$i]['country']  = $db->f('country');
                $this->values[$i]['count']    = $db->f('count');
            }
            $this->getArc();
            return TRUE;
        }
        return FALSE;
    }

    function getArcsForYear() {
        global $db;
        $year = date('y');
        $query = 'SELECT IF(ISNULL(zone), "No data", zone) AS country,';
        $query .= ' COUNT('.($this->type == 'people' ? '' : 'DISTINCT ').'ip_addr) AS count';
        $query .= ' FROM  '.$this->lang.'_counter_all';
        $query .= ' WHERE years = '.$year;
        $query .= ' GROUP BY country ORDER BY count DESC';
        $db->query($query);
        if ($db->nf() > 0) {
            if ($db->nf() > $this->conf['counter_max_countries']) {
                $size = $this->conf['counter_max_countries'];
            } else {
                $size = $db->nf();
            }
            for($i = 0; $i < $size; $i++) {
            	$db->next_record();
                $this->total                 += $db->f('count');
                $this->values[$i]['country']  = $db->f('country');
                $this->values[$i]['count']    = $db->f('count');
            }
            $this->getArc();
            return TRUE;
        }
        return FALSE;
    }

    function getArcsForAllTime() {
        global $db;
        $query = 'SELECT IF(ISNULL(zone), "No data", zone) AS country,';
        $query .= ' COUNT('.($this->type == 'people' ? '' : 'DISTINCT ').'ip_addr) AS count';
        $query .= ' FROM  '.$this->lang.'_counter_all';
        $query .= ' GROUP BY country ORDER BY count DESC';
        $db->query($query);
        if ($db->nf() > 0) {
        	if ($db->nf() > $this->conf['counter_max_countries']) {
                $size = $this->conf['counter_max_countries'];
            } else {
                $size = $db->nf();
            }

            for($i = 0; $i < $size; $i++) {
                $db->next_record();
                $this->total                 += $db->f('count');
                $this->values[$i]['country']  = $db->f('country');
                $this->values[$i]['count']    = $db->f('count');
            }
            $this->getArc();
            return TRUE;
        }
        return FALSE;
    }

    function getArcsForPeriod($time)
    {   global $db;

	    $from = @$time['from'] ? strtotime(str_replace(".", "-", $time['from'])) : -1;
	    $to = @$time['to'] ? strtotime(str_replace(".", "-", $time['to'])) : -1;
	    if (-1 != $from && -1 != $to) {
	        if ($from > $to) {
	            $t = $from;
	            $from = $to;
	            $to = $t;
	        }
	        $where = "utime BETWEEN '".$from."' AND '".$to."'";
	    } elseif (-1 != $from) {
	        $where = "utime >= ".$from;
	    } elseif (-1 != $to) {
	        $where = "utime <= ".$to;
	    } else {
	        $where = '1 = 1';
	    }

	    $query = 'SELECT IF(ISNULL(zone), "No data", zone) AS country,';
        $query .= ' COUNT('.($this->type == 'people' ? '' : 'DISTINCT ').'ip_addr) AS count';
        $query .= ' FROM  '.$this->lang.'_counter_all';
        $query .= ' WHERE '.$where;
        $query .= ' GROUP BY country ORDER BY count DESC';
        $db->query($query);

        if ($db->nf() > 0) {
        	if ($db->nf() > $this->conf['counter_max_countries']) {
                $size = $this->conf['counter_max_countries'];
            } else {
                $size = $db->nf();
            }

            for($i = 0; $i < $size; $i++) {
                $db->next_record();
                $this->total                 += $db->f('count');
                $this->values[$i]['country']  = $db->f('country');
                $this->values[$i]['count']    = $db->f('count');
            }
            $this->getArc();
            return TRUE;
        }
        return FALSE;
    }

	function sendImageGraph() {
    	Header('Content-type: image/png');
		ImagePng($this->graph);
		ImageDestroy($this->graph);
	}

	function getGraph() {
		$this->getArcs($_GET['time_type']);
		$this->sendImageGraph();
	}
}
?>