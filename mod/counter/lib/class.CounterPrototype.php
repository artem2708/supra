<?php
function abo_counter_cmp($a, $b)
{
    if ($a['count'] == $b['count']) {
        return 0;
    }

    return ($a['count'] > $b['count']) ? -1 : 1;
}

define('COUNTRY_IP_FILE',	'GeoIPCountryWhois');
define('CITY_IP_FILE',		'GeoIPCityWhois');

class CounterPrototype extends Module {

	var $main_title;
	var $table_prefix;
	var $module_name			= 'counter';
	var $default_action			= '';
	var $admin_default_action	= 'visit';
	var $tpl_path				= 'mod/counter/tpl/';
	var $mounth;
    var $year;
    var $tables	= array();
	var $_msg = array();

	function CounterPrototype($action = '') {
		global $main, $db, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang, $permissions;
		require_once(RP.'mod/'.$this->module_name.'/lang/'.$this->module_name.'_'.$CONFIG['admin_lang'].'.php');
		$this->_msg = $_MSG;
        $this->table_prefix = $server.$lang;
		$mounth	= date('m');
		$year	= date('Y');
		if ($mounth == 1) {
			$this->mounth	= 12;
			$this->year		= ($year - 1);
 		} else {
            $this->year		= $year;
  			if (($mounth - 1) < 10) {
  				$this->mounth	= '0'.($mounth - 1);
  			} else {
  				$this->mounth	= ($mounth - 1);
  			}
		}
		$db->query('SELECT name FROM '.$server.$lang.'_counter_tables');
		if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
            	$this->tables[]	= $db->f('name');
            }
		}

		$is_admin = preg_match('/admin\.php/', $_SERVER["SCRIPT_NAME"]);
		
		if ('only_create_object' == $action) {
		    return;
		}
		
		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}
		
		if (empty($is_admin)) {
//---------------------------------- обработка действий с сайта --------------------------------//
			if (empty($action)) $action = $this->default_action;
//------------------------------- конец обработки действий с сайта -----------------------------//
		} else {
//------------------------------ обработка действий из админ части -----------------------------//
			if (!isset($tpl)) $main->message_die(iconv("WINDOWS-1251", "UTF-8", 'Не подключен класс для работы с шаблонами'));
			if (empty($action)) $action = $this->admin_default_action;
			switch ($action) {
		    case 'prp':
		    	if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step;
				$main->include_main_blocks('_properties.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Information"	=> $main->_msg["Information"],
					"MSG_Value"			=> $main->_msg["Value"],
					"MSG_Save"			=> $main->_msg["Save"],
					"MSG_Cancel"		=> $main->_msg["Cancel"],
				));
		        if (!$request_step || $request_step == 1) {
					$tpl->assign(array('form_action'	=> $baseurl,
									   'step'			=> 2,
									   'lang'			=> $lang,
									   'name'			=> $this->module_name,
									   'action'			=> 'prp'));
					$prp_html = $main->getModuleProperties($this->module_name);
		            $this->main_title = $this->_msg["Controls"];
				} elseif (2 == $request_step) {
		        	$main->setModuleProperties($this->module_name, $_POST);
					header('Location: '.$baseurl.'&action=prp&step=1');
				}
		    	break;
// Посещаемость по контенту
			case 'visit':
	   		if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_mod, $request_time;
          if(!$this->check_table()){
              require_once(RP.'mod/counter/create_base.php');
          }
		   		if (isset($request_mod)) {
		   		// Посещаемость по модулям
  					$main->include_main_blocks_2($this->module_name.'_visit_mod.html', $this->tpl_path);
  		      $tpl->prepare();
  					$tpl->assign(Array(
  						"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
  						"MSG_Attendance_review" => $this->_msg["Attendance_review"],
  						"MSG_by_content" => $this->_msg["by_content"],
  						"MSG_by_modules" => $this->_msg["by_modules"],
  						"MSG_Module" => $this->_msg["Module"],
  						"MSG_Visits" => $this->_msg["Visits"],
  						"MSG_Unique_visitors" => $this->_msg["Unique_visitors"],
  					));

         		$this->assignCsvReportLink();
        		$tpl->assign('baseurl', $baseurl);

            $this->getSelectListOfTime(@$request_time, '&action='.$action."&mod");
  					$tpl->assign(Array(
  						"block_period.MSG_Enter_period" => $this->_msg["Enter_period"],
  						"block_period.MSG_from" => $this->_msg["from"],
  						"block_period.MSG_to" => $this->_msg["to"],
  						"block_period.MSG_Select" => $this->_msg["Select"],
  						"block_period.MSG_Show" => $this->_msg["Show"],
  						"block_period.MSG_Reset" => $this->_msg["Reset"],
  					));

            $tpl->assign_array('block_unique_time_period_bullet',	$this->getBulletForCurrentTime(@$request_time));
            $tpl->assign_array('block_time_period_bullet',			$this->getBulletForCurrentTime(@$request_time));
            $tpl->assign_array('block_visit_mod_row',			    $this->getVisitModStatistic(@$request_time));

            $this->main_title = $this->_msg["Visits"];
            break;
		   		}

				$main->include_main_blocks_2($this->module_name.'_visit.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
					"MSG_Attendance_review" => $this->_msg["Attendance_review"],
					"MSG_by_content" => $this->_msg["by_content"],
					"MSG_by_modules" => $this->_msg["by_modules"],
					"MSG_Current_hour" => $this->_msg["Current_hour"],
					"MSG_Today" => $this->_msg["Today"],
					"MSG_Yesterday" => $this->_msg["Yesterday"],
					"MSG_Current_month" => $this->_msg["Current_month"],
					"MSG_Last_month" => $this->_msg["Last_month"],
					"MSG_Total_time" => $this->_msg["Total_time"],
					"MSG_Total_visitors_num" => $this->_msg["Total_visitors_num"],
					"MSG_Unique_visitors_num" => $this->_msg["Unique_visitors_num"],
				));

				$this->assignCsvReportLink();
		        $this->getSelectListOfTime(@$request_time, '&action='.$action);
				$tpl->assign(Array(
					"block_period.MSG_Enter_period" => $this->_msg["Enter_period"],
					"block_period.MSG_from" => $this->_msg["from"],
					"block_period.MSG_to" => $this->_msg["to"],
					"block_period.MSG_Select" => $this->_msg["Select"],
					"block_period.MSG_Show" => $this->_msg["Show"],
					"block_period.MSG_Reset" => $this->_msg["Reset"],
				));
		        $tpl->assign($this->getVisitStatistic());

		        $period = $this->getBulletForCurrentTime($request_time);
		        if (is_array(@$request_time)) {
		        	$tpl->newBlock('block_no_graf');
					$tpl->assign(Array(
						"MSG_By" => $this->_msg["By"],
						"MSG_Total_visitors_num" => $this->_msg["Total_visitors_num"],
						"MSG_Unique_visitors_num" => $this->_msg["Unique_visitors_num"],
					));
		        	$tpl->assign(array('time' => $period[0]['time'],
		        	                   'period_users' => $this->getPeriodCountUsers($request_time),
		        	                   'period_users_unique' => $this->getPeriodCountUsers($request_time, 1)));
		        } else {
		            $tpl->newBlock('block_graf');
					$tpl->assign(Array(
						"MSG_Period" => $this->_msg["Period"],
						"MSG_All_visits_dynamics" => $this->_msg["All_visits_dynamics"],
						"MSG_Period" => $this->_msg["Period"],
						"MSG_Unique_visits_dynamics" => $this->_msg["Unique_visits_dynamics"],
						"MSG_Unique_visits_dynamics_by" => $this->_msg["Unique_visits_dynamics_by"],
					));
		            $tpl->assign(array('time' => $period[0]['time'],
		        	                   'time_type' => $period[0]['time_type'],
		        	                   'lang' => $lang));
		            $tpl->assign_array('block_unique_time_period_bullet',	$this->getBulletForCurrentTime(@$request_time));
		            $tpl->assign_array('block_time_period_bullet',			$this->getBulletForCurrentTime(@$request_time));
		        }

		        $this->main_title = $this->_msg["Visits"];
				break;

// Статистика по источникам пользователей
			case 'links':
		   	if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_time, $request_start;
				$main->include_main_blocks_2($this->module_name.'_links.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
					"MSG_Link" => $this->_msg["Link"],
					"MSG_Passage_num" => $this->_msg["Passage_num"],
				));

				$this->assignCsvReportLink();
				$this->getSelectListOfTime(@$request_time, '&action='.$action);
				$tpl->assign(Array(
					"block_period.MSG_Enter_period" => $this->_msg["Enter_period"],
					"block_period.MSG_from" => $this->_msg["from"],
					"block_period.MSG_to" => $this->_msg["to"],
					"block_period.MSG_Show" => $this->_msg["Show"],
					"block_period.MSG_Reset" => $this->_msg["Reset"],
				));
				$request_start = @$request_start > 1 ? (int)$request_start : 1;
				$tpl->assign_array('block_links', $this->getLinks(@$request_time, $request_start));
				$tpl->assign(Array(
					"block_links.MSG_Go_link" => $this->_msg["Go_link"],
				));
				$tables = $this->getTableByDate(@$request_time);
				$link = 'action='.$action.(@$request_time ? 'time='.@$request_time : '');
				$main->_show_nav_string_2('visit_from',
										  $tables['table'],
										' AND '.$tables['where'].'
										  AND visit_from NOT LIKE "%'.str_replace('www.', '', $_SERVER['HTTP_HOST']).'%"',

										'', $link, $request_start, $CONFIG['counter_max_rows'], '', '');
				$tpl->assign(Array(
					"nav.MSG_Pages" => $this->_msg["Pages"],
				));
		    $this->main_title = $this->_msg["Links_on_site"];
				break;

// Статистика по ссылающимся ресурсам
			case 'sites':
		   		if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_time, $request_start;
				$main->include_main_blocks_2($this->module_name.'_sites.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
					"MSG_Sites" => $this->_msg["Sites"],
					"MSG_Passage_num" => $this->_msg["Passage_num"],
				));

				$this->assignCsvReportLink();
		        $this->getSelectListOfTime(@$request_time, '&action='.$action);
				$tpl->assign(Array(
					"block_period.MSG_Enter_period" => $this->_msg["Enter_period"],
					"block_period.MSG_from" => $this->_msg["from"],
					"block_period.MSG_to" => $this->_msg["to"],
					"block_period.MSG_Show" => $this->_msg["Show"],
					"block_period.MSG_Reset" => $this->_msg["Reset"],
				));
		        $request_start = @$request_start > 1 ? (int)$request_start : 1;
		        $tpl->assign_array('block_sites',	$this->getReferringHosts(@$request_time, $request_start));

		        $tables = $this->getTableByDate(@$request_time);
		        $link = 'action='.$action.(@$request_time ? '&time='.@$request_time : '');
				$main->_show_nav_string_2('visit_from_site',
										  $tables['table'],
										  ' AND '.$tables['where'].'
		                        		    AND visit_from_site NOT LIKE "'.$_SERVER['HTTP_HOST'].'%"
		                        		    AND visit_from_site NOT LIKE "'.str_replace('www.', '', $_SERVER['HTTP_HOST']).'%"',
										  '', $link, $request_start, $CONFIG['counter_max_rows'], '', '');
				$tpl->assign(Array(
					"nav.MSG_Pages" => $this->_msg["Pages"],
				));
		        $this->main_title = $this->_msg["Linking_sites"];
				break;

// Статистика по посещаемости страниц ресурса
			case 'pages':
		   		if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_mod, $request_time, $request_start;
		   		switch ($request_mod) {
		   		    /* Пути */
		   		    case 'path':
		   		        $main->include_main_blocks_2($this->module_name.'_pages_path.html', $this->tpl_path);
				        $tpl->prepare();
						$tpl->assign(Array(
							"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
							"MSG_Pages" => $this->_msg["Pages"],
							"MSG_Site_paths" => $this->_msg["Site_paths"],
							"MSG_Entry_points" => $this->_msg["Entry_points"],
							"MSG_Exit_points" => $this->_msg["Exit_points"],
							"MSG_Single_loadings" => $this->_msg["Single_loadings"],
							"MSG_Site_review_depth" => $this->_msg["Site_review_depth"],
							"MSG_Path" => $this->_msg["Path"],
							"MSG_Q_ty" => $this->_msg["Q_ty"],
						));
				        $paths = $this->getPaths(@$request_time);
				        foreach ($paths as $key => $path) {
				        	$tpl->newBlock('block_pages_path');
				        	$tpl->assign(array('path' => $path['path'][0],
				        	                   'count' => $path['count'],
				        	                   'count_rate' => $path['count_rate']
				        	                  ));
				        	for ($i = 1, $cnt = count($path['path']); $i < $cnt; $i++) {
				        	    $tpl->newBlock('block_pages_path_row');
				        	    $tpl->assign('path', $path['path'][$i]);
				        	}
				        }

		   		        break;
		   		    /* Точки входа */
		   		    case 'entry':
		   		        $main->include_main_blocks_2($this->module_name.'_pages_entry.html', $this->tpl_path);
				        $tpl->prepare();
						$tpl->assign(Array(
							"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
							"MSG_Pages" => $this->_msg["Pages"],
							"MSG_Site_paths" => $this->_msg["Site_paths"],
							"MSG_Entry_points" => $this->_msg["Entry_points"],
							"MSG_Exit_points" => $this->_msg["Exit_points"],
							"MSG_Single_loadings" => $this->_msg["Single_loadings"],
							"MSG_Site_review_depth" => $this->_msg["Site_review_depth"],
							"MSG_Page" => $this->_msg["Page"],
							"MSG_Title" => $this->_msg["Title"],
							"MSG_Entry_q_ty" => $this->_msg["Entry_q_ty"],
							"MSG_Shows_q_ty" => $this->_msg["Shows_q_ty"],
							"MSG_Last" => $this->_msg["Last"],
						));

				        $tpl->assign_array('block_pages_entry', $this->getEntrys(@$request_time));

		   		        break;
		   		    /* Точки выхода */
		   		    case 'exit':
		   		        $main->include_main_blocks_2($this->module_name.'_pages_exit.html', $this->tpl_path);
				        $tpl->prepare();
						$tpl->assign(Array(
							"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
							"MSG_Pages" => $this->_msg["Pages"],
							"MSG_Site_paths" => $this->_msg["Site_paths"],
							"MSG_Entry_points" => $this->_msg["Entry_points"],
							"MSG_Exit_points" => $this->_msg["Exit_points"],
							"MSG_Single_loadings" => $this->_msg["Single_loadings"],
							"MSG_Site_review_depth" => $this->_msg["Site_review_depth"],
							"MSG_Page" => $this->_msg["Page"],
							"MSG_Title" => $this->_msg["Title"],
							"MSG_Entry_q_ty" => $this->_msg["Entry_q_ty"],
							"MSG_Shows_q_ty" => $this->_msg["Shows_q_ty"],
							"MSG_Last" => $this->_msg["Last"],
						));

				        $tpl->assign_array('block_pages_exit', $this->getExits(@$request_time));

		   		        break;
		   		    /* Единичные загрузки */
		   		    case 'load':
		   		        $main->include_main_blocks_2($this->module_name.'_pages_load.html', $this->tpl_path);
				        $tpl->prepare();
						$tpl->assign(Array(
							"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
							"MSG_Pages" => $this->_msg["Pages"],
							"MSG_Site_paths" => $this->_msg["Site_paths"],
							"MSG_Entry_points" => $this->_msg["Entry_points"],
							"MSG_Exit_points" => $this->_msg["Exit_points"],
							"MSG_Single_loadings" => $this->_msg["Single_loadings"],
							"MSG_Site_review_depth" => $this->_msg["Site_review_depth"],
							"MSG_Page" => $this->_msg["Page"],
							"MSG_Title" => $this->_msg["Title"],
							"MSG_Time" => $this->_msg["Time"],
						));

				        $tpl->assign_array('block_pages_load', $this->getLoads(@$request_time));

		   		        break;
		   		    /* Глубина просмотра */
		   		    case 'depth':
		   		        $main->include_main_blocks_2($this->module_name.'_pages_depth.html', $this->tpl_path);
				        $tpl->prepare();
						$tpl->assign(Array(
							"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
							"MSG_Pages" => $this->_msg["Pages"],
							"MSG_Site_paths" => $this->_msg["Site_paths"],
							"MSG_Entry_points" => $this->_msg["Entry_points"],
							"MSG_Exit_points" => $this->_msg["Exit_points"],
							"MSG_Single_loadings" => $this->_msg["Single_loadings"],
							"MSG_Site_review_depth" => $this->_msg["Site_review_depth"],
							"MSG_Review_depth" => $this->_msg["Review_depth"],
							"MSG_Session_q_ty" => $this->_msg["Session_q_ty"],
							"MSG_Last_entry" => $this->_msg["Last_entry"],
							"MSG_Last_exit" => $this->_msg["Last_exit"],
						));

				        $tpl->assign_array('block_pages_depth', $this->getDepths(@$request_time));

		   		        break;
		   		    /* Статистика по посещаемости страниц */
		   		    default:
		   		        $main->include_main_blocks_2($this->module_name.'_pages.html', $this->tpl_path);
				        $tpl->prepare();
						$tpl->assign(Array(
							"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
							"MSG_Pages" => $this->_msg["Pages"],
							"MSG_Site_paths" => $this->_msg["Site_paths"],
							"MSG_Entry_points" => $this->_msg["Entry_points"],
							"MSG_Exit_points" => $this->_msg["Exit_points"],
							"MSG_Single_loadings" => $this->_msg["Single_loadings"],
							"MSG_Site_review_depth" => $this->_msg["Site_review_depth"],
							"MSG_Page" => $this->_msg["Page"],
							"MSG_Title" => $this->_msg["Title"],
							"MSG_Shows_num" => $this->_msg["Shows_num"],
						));

						$arr = $this->getPages(@$request_time, $request_start);
                        $arr = is_array($arr) ? $arr :array ();
						foreach($arr as $k => $v)
						{
							$tpl->newBlock("block_pages");
							$tpl->assign(Array(
								"MSG_Go_link" => $this->_msg["Go_link"],
							));
							$tpl->assign($v);
						}

				        $link = 'action='.$action.(@$request_time ? '&time='.@$request_time : '');
				        $tables = $this->getTableByDate(@$request_time);
				        $main->_show_nav_string_2('page_visit',
										          $tables['table'],
										          ' AND '.$tables['where'],
										          '',
										          $link,
										          $request_start,
										          $CONFIG['counter_max_rows'],
										          '',
										          '');
						$tpl->assign(Array(
							"MSG_Pages" => $this->_msg["Pages"],
						));
		        }

		        $this->assignCsvReportLink();
		        $this->getSelectListOfTime(@$request_time, '&action='.$action.($request_mod ? '&mod='.$request_mod : ''));
				$tpl->assign(Array(
					"block_period.MSG_Enter_period" => $this->_msg["Enter_period"],
					"block_period.MSG_from" => $this->_msg["from"],
					"block_period.MSG_to" => $this->_msg["to"],
					"block_period.MSG_Show" => $this->_msg["Show"],
					"block_period.MSG_Reset" => $this->_msg["Reset"],
				));
		        $tpl->assign('_ROOT.page_link', $baseurl."&action=".$action);
		        $this->assignCsvReportLink();
		        $this->main_title = $this->_msg["Pages"];
				break;

// Подробно о посетителях
			case 'people':
		   		if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_time;
				$main->include_main_blocks_2($this->module_name.'_people.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
					"MSG_Languages" => $this->_msg["Languages"],
					"MSG_Using_OS" => $this->_msg["Using_OS"],
					"MSG_Browsers" => $this->_msg["Browsers"],
					"MSG_Screen_sizes" => $this->_msg["Screen_sizes"],
					"MSG_Address_IP" => $this->_msg["Address_IP"],
					"MSG_Site_review_depth" => $this->_msg["Site_review_depth"],
				));

				$this->assignCsvReportLink();
				$this->getSelectListOfTime(@$request_time, '&action='.$action);
				$tpl->assign(Array(
					"block_period.MSG_Enter_period" => $this->_msg["Enter_period"],
					"block_period.MSG_from" => $this->_msg["from"],
					"block_period.MSG_to" => $this->_msg["to"],
					"block_period.MSG_Show" => $this->_msg["Show"],
					"block_period.MSG_Reset" => $this->_msg["Reset"],
				));

		        $tpl->assign_array('block_users_lang',			$this->getUsersLang(@$request_time));
		        $tpl->assign_array('block_users_os',			$this->getUsersOS(@$request_time));
		        $tpl->assign_array('block_users_browsers',		$this->getUsersBrowsers(@$request_time));
		        $tpl->assign_array('block_users_resolution',	$this->getUsersResolution(@$request_time));
		        $tpl->assign_array('block_users_ip',			$this->getUsersIP(@$request_time));

				$tpl->assign(Array(
					"nav.MSG_Pages" => $this->_msg["Pages"],
				));
		        $this->main_title = $this->_msg["Visitors"];
				break;

// География посещаемости
			case 'geo':
		   		if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_mod, $request_time;
				$request_mod = $_REQUEST['mod'] = 'city';
				$main->include_main_blocks_2($this->module_name.'_geo'.('city' == @$request_mod ? '_city' : '').'.html', $this->tpl_path);
				$tpl->prepare();
				if ($this->isEmptyGeoIpTables()) {
					header("Location: ".$baseurl."&action=fillgeoip");
					exit;
				} else {
				    $tpl->newBlock('block_report');
    				$tpl->assign(Array(
    					"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
    					"MSG_Statistics" => $this->_msg["Statistics"],
    					"MSG_by_countries" => $this->_msg["by_countries"],
    					"MSG_by_cities" => $this->_msg["by_cities"],
    					"MSG_All_visits_of" => $this->_msg["All_visits_of"],
    					"MSG_Unique_visits_of" => $this->_msg["Unique_visits_of"],
    					"MSG_Geo_visits_of" => $this->_msg["Geo_visits_of"],
    					"MSG_Geo_unique_visits_of" => $this->_msg["Geo_unique_visits_of"],

    					"baseurl" => $baseurl,
    				));

    				$this->assignCsvReportLink('block_report');
    				$this->getSelectListOfTime(@$request_time, '&action='.$action.('city' == @$request_mod ? '&mod=city' : ''));
    				$tpl->assign(Array(
    					"block_period.MSG_Enter_period" => $this->_msg["Enter_period"],
    					"block_period.MSG_from" => $this->_msg["from"],
    					"block_period.MSG_to" => $this->_msg["to"],
    					"block_period.MSG_Show" => $this->_msg["Show"],
    					"block_period.MSG_Reset" => $this->_msg["Reset"],
    				));

    		        $tpl->assign($this->getGeoStatistic(@$request_time, 'block_report'));
				}

		        $this->main_title = $this->_msg["Geography"];
				break;

// Статистика по поисковым роботам
	case 'spiders':
		   		if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_time;
				$main->include_main_blocks_2($this->module_name.'_visit.html', $this->tpl_path);
				$tpl->prepare();

				$this->assignCsvReportLink();
		        $tpl->assign(array('action_url' => $baseurl,
		        				   'action'		=> 'spiders',
		        				   'module'		=> 'counter',));
		        $tpl->assign_array('block_time', $this->getSelectListOfTime(@$request_time));
		        $tpl->assign($this->getSpidersStatistic(@$request_time));
		        $this->main_title = $this->_msg["Search_robots"];
				break;

// Статистика по поисковикам
			case 'searcheng':
		   		if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_time;
				$main->include_main_blocks_2($this->module_name.'_searcheng.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
				));

				$this->assignCsvReportLink();
				$this->getSelectListOfTime(@$request_time, '&action='.$action);
				$tpl->assign(Array(
					"block_period.MSG_Enter_period" => $this->_msg["Enter_period"],
					"block_period.MSG_from" => $this->_msg["from"],
					"block_period.MSG_to" => $this->_msg["to"],
					"block_period.MSG_Show" => $this->_msg["Show"],
					"block_period.MSG_Reset" => $this->_msg["Reset"],
				));

				$engn = $this->getSearchEngStatistic(@$request_time);
				if (count($engn)) {
					$tpl->newBlock('block_searcheng');
					$tpl->assign(Array(
						"MSG_Search_engine" => $this->_msg["Search_engine"],
						"MSG_Passage_num" => $this->_msg["Passage_num"],
					));
		        	$tpl->assign_array('block_searcheng_row', $engn);
				} else  {
					$tpl->newBlock('block_searcheng_no');
					$tpl->assign(Array(
						"MSG_No_search_engine_passages" => $this->_msg["No_search_engine_passages"],
					));
				}

		        $this->main_title = $this->_msg["Search_engines"];
				break;

// Статистика по роисковым запросам
			case 'search_query':
		   		if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_time, $request_engine, $baseurl;
				$main->include_main_blocks_2($this->module_name.'_search_query.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Download_report_in_CSV" => $this->_msg["Download_report_in_CSV"],
				));

				$this->assignCsvReportLink();
				$this->getSelectListOfTime(@$request_time, '&action='.$action.'&engine='.$request_engine);

				$arr = $this->getSearchEngQueryStatistic($request_engine, @$request_time);
				if (count($arr)) {
					$tpl->newBlock('block_search_query');
					$tpl->assign(Array(
						"MSG_Request_string" => $this->_msg["Request_string"],
						"MSG_Passage_num" => $this->_msg["Passage_num"],
						"MSG_Show" => $this->_msg["Show"],
					  "MSG_Reset" => $this->_msg["Reset"],
					  'action' => $baseurl.'&action='.$action.'&engine='.$request_engine,
					));
		        	$tpl->assign_array('block_search_query_row', $arr);
				} else  {
					$tpl->newBlock('block_search_query_no');
					$tpl->assign(Array(
						"MSG_No_data" => $this->_msg["No_data"],
					));
				}

		        $this->main_title = $this->_msg["Search_engine_stat"].(@$request_engine && $CONFIG['search_engine_name'][$request_engine] ? ' "'.$CONFIG['search_engine_name'][$request_engine].'"' : '');
				break;
/**
 * *****************************************************
 *  CSV отчёт
 * *****************************************************
 */
			case 'csv':
		   		if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_time, $request_report, $request_mod;
		   		$out_array = array();
		   		$seprtr  = ";";
		   		$glue = '"';
		   		$time = time();
		   		$inf = $this->getBulletForCurrentTime($request_time);
		   		$title = $this->_msg["Report_of"] . " ".$inf[0]['time']." (" . $this->_msg["from"] . "  ".date('d-m-Y H:i:s', $time).")\n";
		   		$no_data = $this->_msg["No_data_of_this_period"];
				switch ($request_report) {
				    /**
				     * Посещаемость
				     **/
					case 'visit': default:
					    /* По модулям */
						if (isset($request_mod)) {
						    $out_array[] = $this->_msg["Attendance_content"];
						    $report= $this->getVisitModStatistic(@$request_time);
						    if (0 == count($report)) {
						    	$out_array[] = "";
						    	$out_array[] = $no_data;
						    } else {
						        $out_array[] = array($this->_msg["Module"], $this->_msg["Visits"], $this->_msg["Unique_visitors"], $this->_msg["Module_ID"]);
						        $out_array = array_merge($out_array, $report);

						    }

						    break;
						}

						/* По контенту */
						$out_array[0] = $this->_msg["Attendance_content"];
		    		    $out_array[1] = array('',
		     			                      $this->_msg["Current_hour"],
						                      $this->_msg["Today"],
						                      $this->_msg["Yesterday"],
						                      $this->_msg["Current_month"],
						                      $this->_msg["Last_month"],
						                      $this->_msg["Total_time"]);
					    $report = $this->getVisitStatistic();
						$out_array[2] = array($this->_msg["Total_visitors_num"],
						                      $report['_ROOT.hour_users'],
						                      $report['_ROOT.day_users'],
						                      $report['_ROOT.yesterday_users'],
						                      $report['_ROOT.mounth_users'],
						                      $report['_ROOT.old_mounth_users'],
						                      $report['_ROOT.all_users']);
						$out_array[3] = array($this->_msg["Unique_visitors_num"],
						                      $report['_ROOT.hour_users_unique'],
						                      $report['_ROOT.day_users_unique'],
						                      $report['_ROOT.yesterday_users_unique'],
						                      $report['_ROOT.mounth_users_unique'],
						                      $report['_ROOT.old_mounth_users_unique'],
						                      $report['_ROOT.all_users_unique']);
						if (is_array(@$request_time)) {
						    $out_array[1][] = $this->_msg["By"] . " ".$inf[0]['time'];
						    $out_array[2][] = $this->getPeriodCountUsers($request_time);
						    $out_array[3][] = $this->getPeriodCountUsers($request_time, 1);
						} else {
						    $report = $this->getVisits($request_time);
						    if (is_array($report)) {
						        $out_array[] = "";
						        $out_array[] = array('', $this->_msg["Total_visitors_num"], $this->_msg["Unique_visitors_num"]);
						        $out_array = array_merge($out_array, $report);
						    }
						}
						break;

				    /**
				     * Ссылки на сайт
				     **/
					case 'links':
						$out_array[] = $this->_msg["Links_on_site"];
						$report = $this->getLinks(@$request_time, 1, 1);
						if (0 == count($report)) {
						    $out_array[] = "";
						    $out_array[] = $no_data;
						} else {
						    $out_array[] = array($this->_msg["Link"], $this->_msg["Passage_num"], '%');
						    $out_array = array_merge($out_array, $report);
						}

						break;

					/**
				     * Ссылающиеся сайты
				     **/
					case 'sites':
						$out_array[] = $this->_msg["Linking_sites"];
						$report = $this->getReferringHosts(@$request_time, 1, 1);
						if (0 == count($report)) {
						    $out_array[] = "";
						    $out_array[] = $no_data;
						} else {
						    $out_array[] = array($this->_msg["Site"], $this->_msg["Passage_num"], '%');
						    $out_array = array_merge($out_array, $report);
						}

						break;

				    /**
				     * Страницы
				     **/
					case 'pages':
						switch ($request_mod) {
						    /**
						     * Пути
						     */
							case 'path':
								$out_array[] = $this->_msg["Paths"];

							    $report = $this->getPaths(@$request_time);
		        				if (0 == count($report)) {
		        				    $out_array[] = "";
		        				    $out_array[] = $no_data;
		        				} else {
		        				    $out_array[] = array($this->_msg["Path"], $this->_msg["Q_ty"], '%');
		        				    foreach ($report as $key => $path) {
		        				        $path_val = $path['path'][0];
		            		        	for ($i = 1, $cnt = count($path['path']); $i < $cnt; $i++) {
		            		        	    $path_val .=  "\n-> ".$path['path'][$i];
		            		        	}
		            		        	$out_array[] = array($path_val, $path['count'], $path['count_rate']);
		        				    }
		        				}
								break;
						    /**
						     * Точки входа
						     */
							case 'entry':
							    $out_array[] = $this->_msg["Entry_points"];
							    $report = $this->getEntrys(@$request_time);
		        				if (0 == count($report)) {
		        				    $out_array[] = "";
		        				    $out_array[] = $no_data;
		        				} else {
		        				    $out_array[] = array($this->_msg["Page"], $this->_msg["Title"], $this->_msg["Entry_q_ty"], $this->_msg["Shows_q_ty"], $this->_msg["Last"]);
		        				    $out_array = array_merge($out_array, $report);
		        				}
								break;
							/**
						     * Точки выхода
						     */
							case 'exit':
							    $out_array[] = $this->_msg["Exit_point"];
							    $report = $this->getExits(@$request_time);
		        				if (0 == count($report)) {
		        				    $out_array[] = "";
		        				    $out_array[] = $no_data;
		        				} else {
		        				    $out_array[] = array($this->_msg["Page"], $this->_msg["Title"], $this->_msg["Exit_q_ty"], $this->_msg["Shows_q_ty"], $this->_msg["Last"]);
		        				    $out_array = array_merge($out_array, $report);
		        				}
								break;
							/**
						     * Единичные загрузки
						     */
							case 'load':
							    $out_array[] = $this->_msg["Single_loadings"];
							    $report = $this->getLoads(@$request_time);
		        				if (0 == count($report)) {
		        				    $out_array[] = "";
		        				    $out_array[] = $no_data;
		        				} else {
		        				    $out_array[] = array($this->_msg["Page"], $this->_msg["Title"], $this->_msg["Time"]);
		        				    $out_array = array_merge($out_array, $report);
		        				}
								break;
							/**
						     * Глубина просмотра сайта
						     */
							case 'depth':
							    $out_array[] = $this->_msg["Site_review_depth"];
							    $report = $this->getDepths(@$request_time);
		        				if (0 == count($report)) {
		        				    $out_array[] = "";
		        				    $out_array[] = $no_data;
		        				} else {
		        				    $out_array[] = array($this->_msg["Review_depth"], $this->_msg["Session_q_ty"], '%', $this->_msg["Last_entry"], $this->_msg["Last_exit"]);
		        				    foreach ($report as $val) {
		        				        $out_array[] = array($val['depth'],
		        				                             $val['sess_count'],
		        				                             $val['sess_count_rate'],
		        				                             $val['last_entry'],
		        				                             $val['last_exit'],);
		        				    }
		        				}
								break;
							/**
						     * Страницы
						     */
							default:
							    $out_array[] = $this->_msg["Attendance_statistics"];
							    $report = $this->getPages(@$request_time, 1, 1);
		        				if (0 == count($report)) {
		        				    $out_array[] = "";
		        				    $out_array[] = $no_data;
		        				} else {
		        				    $out_array[] = array($this->_msg["Page"], $this->_msg["Title"], $this->_msg["Shows_num"], '%');
		        				    $out_array = array_merge($out_array, $report);
		        				}
								break;
						}

						break;

					/**
				     * Посетители
				     **/
					case 'people':
						$out_array[] = $this->_msg["Visitors"];

						$report = $this->getUsersLang(@$request_time);
						if (is_array($report)) {
							$out_array[] = "";
		    				$out_array[] = $this->_msg["Languages"];
		    				$out_array[] = array($this->_msg["Language"], $this->_msg["Q_ty"], '%');
		    				$out_array = array_merge($out_array, $report);
						}

						$report = $this->getUsersOS(@$request_time);
						if (is_array($report)) {
							$out_array[] = "";
		    				$out_array[] = $this->_msg["Using_OS"];
		    				$out_array[] = array($this->_msg["OS"], $this->_msg["Q_ty"], '%');
		    				$out_array = array_merge($out_array, $report);
						}

						$report = $this->getUsersBrowsers(@$request_time);
						if (is_array($report)) {
							$out_array[] = "";
		    				$out_array[] = $this->_msg["Browsers"];
		    				$out_array[] = array($this->_msg["Browser"], $this->_msg["Q_ty"], '%');
		    				$out_array = array_merge($out_array, $report);
						}

						$report = $this->getUsersResolution(@$request_time);
						if (is_array($report)) {
							$out_array[] = "";
		    				$out_array[] = $this->_msg["Screen_sizes"];
		    				$out_array[] = array($this->_msg["Screen_sizes"], $this->_msg["Q_ty"], '%');
		    				$out_array = array_merge($out_array, $report);
						}

						$report = $this->getUsersIP(@$request_time);
						if (is_array($report)) {
							$out_array[] = "";
		    				$out_array[] = $this->_msg["Address_IP"];
		    				$out_array[] = array($this->_msg["IP"], $this->_msg["Q_ty"], '%');
		    				$out_array = array_merge($out_array, $report);
						}

		                if (count($out_array) < 2) {
		        			$out_array[] = "";
		        			$out_array[] = $no_data;
		                }

						break;

					/**
				     * География
				     **/
					case 'geo':
					    /* по городам */
						if (isset($request_mod)) {
						    $out_array[] = $this->_msg["Geography_cities"];
						    $report = $this->getGeoCity(@$request_time);
						    if (0 == count($report)) {
						    	$out_array[] = "";
						    	$out_array[] = $no_data;
						    } else {
						        $out_array[] = array($this->_msg["City"], $this->_msg["Visits"], $this->_msg["Unique_visitors"]);
						        foreach ($report as $key => $val) {
						        	$out_array[] = array($val['city'],
						        	                     $val['count'],
						        	                     $val['uniq']);
						        }
						    }

						    break;
						}

					/**
				     * Поисковые системы
				     **/
					case 'searcheng':
						$out_array[] = $this->_msg["Search_engines"];
						$report = $this->getSearchEngStatistic(@$request_time);
						if (0 == count($report)) {
						    $out_array[] = "";
						    $out_array[] = $no_data;
						} else {
						    $out_array[] = array('№', $this->_msg["Passages"], $this->_msg["Search_engine"], $this->_msg["Request_string"], '%');
						    foreach ($report as $key => $val) {
						        $out_array[] = array($val['num'],
						                             $val['visit'],
						                             $val['engine_name'],
						                             $val['query_link'],
						                             $val['visit_percent']);
						    }
						}

						break;
				}

				if (count($out_array) > 0) {
                    array_unshift($out_array, $title);

                    $out_array = utf2ansii($out_array);


					$file	= $request_report.'-('.date('d-m-Y_H-i-s', $time).').csv';
                    $content = '';
					foreach ($out_array as $key=>$val) {
						 $content .= $this->getCsvStr($val);
					}
					header('Content-Type: application/x-csv');
					header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
					header('Content-Disposition: inline; filename="'.$file.'"');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
                    header('Content-Length: '.strlen($content));
                    print $content;
					exit;
				}
				break;

			/**
			 * Наполнение базы Geo IP
			 **/
			case 'fillgeoip':
			    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2($this->module_name.'_'.$action.'.html', $this->tpl_path);
				$tpl->prepare();
				global $request_load;
				if (! isset($request_load)) {
					$block = 'block_load';
					$assign = array(
					   'form_action' => $baseurl."&action=".$action."&load",
					   'MSG_Select_csv_file' => $this->_msg['Select_csv_file'],
					   'MSG_CSV_files_charset' => $this->_msg['CSV_files_charset'],
					   'MSG_CSV_files_fields' => $this->_msg['CSV_files_fields'],
					   'MSG_CSV_files_fields_order' => $this->_msg['CSV_files_fields_order'],
					   'MSG_Load' => $this->_msg['Load'],
					);
				} else {
				    if (! is_uploaded_file($_FILES['csv_file']['tmp_name'])) {
				    	$block = 'block_result_error';
				    	$assign = array(
				    	   'MSG_Data_not_loaded' => $this->_msg['Data_not_loaded'],
				    	);
				    } else if (($load_cnt =$this->loadGeoIpCity($_FILES['csv_file']['tmp_name']))) {
				    	$block = 'block_result';
				    	$assign = array(
				    	   'load_count' => $load_cnt,
				    	   'MSG_Fields_loaded' => $this->_msg['Fields_loaded'],
				    	);
				    } else {
				        $block = 'block_result_error';
				        $assign = array(
				    	   'MSG_Data_not_loaded' => $this->_msg['Data_not_loaded'],
				    	);
				    }
				}
				$tpl->newBlock($block);
				$tpl->assign($assign);

				$this->main_title = $this->_msg["Fill_geoip"];
				break;

		// Создание объекта для дальнейшего использования
			case 'only_create_object':
				break;

			default:
				$main->include_main_blocks();
				$tpl->prepare();
			}
//--------------------------- конец обработки действий из админ части --------------------------//
		}
	}
    function check_table(){
        global $CONFIG, $db, $server, $lang;
        $query = "show tables like '{$server}{$lang}_counter%'";
        $db->query($query);
        if(!$db->nf()){return false;}
		$ret = 0;
        while($db->next_record()){
			if($db->f(0)==sprintf('%s%s_counter_all', $server, $lang)){$ret++;}
            if($db->f(0)==sprintf('%s%s_counter_%02d_%04d', $server, $lang, date('m'), date('Y'))){$ret++;}
			if($ret==2){return true;}
        }
        return false;
    }
	function checkPeriod($time)
	{   global $CONFIG;

	    return '' === $time
		       || (!is_array($time) && !array_key_exists($time, $CONFIG['counter_variants_of_time'][$CONFIG['admin_lang']]))
		       || (is_array($time) && !isset($time['from'])&& !isset($time['to']) )
		       ? $CONFIG['counter_default_time']
		       : $time;
	}
    function getSelectListOfTime($time = '', $link_suff = '') {
		global $CONFIG, $tpl, $baseurl;

		$time = $this->checkPeriod($time);

		$tpl->newBlock('block_period');
		$tpl->assign(array('action' => $baseurl.$link_suff,
		                   'date_from_val' => is_array($_REQUEST['time']) && @$_REQUEST['time']['from'] ? $_REQUEST['time']['from'] : "",
		                   'date_to_val' => is_array($_REQUEST['time']) && @$_REQUEST['time']['to']  ? $_REQUEST['time']['to'] : ""));

		for($i = 0; $i < sizeof($CONFIG['counter_variants_of_time'][$CONFIG['admin_lang']]); $i++) {
			$tpl->newBlock('block_time');
			if ($i != $time) {
				$tpl->newBlock('block_active_time');
				$tpl->assign(array('link' => $baseurl.$link_suff."&time=".$i,
				                   'name' => $CONFIG['counter_variants_of_time'][$CONFIG['admin_lang']][$i]
				                  )
				            );
			} else {
				$tpl->newBlock('block_no_active_time');
				$tpl->assign('name', $CONFIG['counter_variants_of_time'][$CONFIG['admin_lang']][$i]);
			}
        }
    }

	function getVisitStatistic()
	{   global $baseurl;

		$arr['_ROOT.hour_users']				= $this->getHourCountUsers();
		$arr['_ROOT.day_users']					= $this->getDayCountUsers();
		$arr['_ROOT.yesterday_users']			= $this->getYesterdayCountUsers();
		$arr['_ROOT.mounth_users']				= $this->getMounthCountUsers();
		$arr['_ROOT.old_mounth_users']			= $this->getOldMounthCountUsers($this->mounth, $this->year);
        $arr['_ROOT.all_users']					= $this->getAllCountUsers();
        $arr['_ROOT.hour_users_unique']			= $this->getHourCountUniqueUsers();
   		$arr['_ROOT.day_users_unique']			= $this->getDayCountUniqueUsers();
   		$arr['_ROOT.yesterday_users_unique']	= $this->getYesterdayCountUniqueUsers();
		$arr['_ROOT.mounth_users_unique']		= $this->getMounthCountUniqueUsers();
		$arr['_ROOT.old_mounth_users_unique']	= $this->getOldMounthCountUniqueUsers($this->mounth, $this->year);
        $arr['_ROOT.all_users_unique']			= $this->getAllCountUniqueUsers();
		$arr['_ROOT.baseurl']			        = $baseurl;

    	return $arr;
	}

    function getHourCountUsers() {
		global $CONFIG, $db, $server, $lang;
		$db->query('SELECT COUNT(ip_addr) AS total
						FROM '.$server.$lang.'_counter_'.date("m_Y").'
						WHERE the_time = "'.date('G').'" AND
							  days = "'.date('j').'" AND
							  mounths = "'.date('m').'" AND
							  years = "'.date('y').'"');
		if ($db->nf() > 0) {
			$db->next_record();
			return $db->f('total');
		} else {
			return $this->_msg["No_data"];
		}
	}

    function getHourCountUniqueUsers() {
		global $CONFIG, $db, $server, $lang;
		$db->query('SELECT COUNT(DISTINCT ip_addr) AS total
						FROM '.$server.$lang.'_counter_'.date("m_Y").'
						WHERE the_time = "'.date('G').'" AND
							  days = "'.date('j').'" AND
							  mounths = "'.date('m').'" AND
							  years = "'.date('y').'"');
		if ($db->nf() > 0) {
			$db->next_record();
			return $db->f('total');
		} else {
			return $this->_msg["No_data"];
		}
	}

    function getDayCountUsers() {
		global $CONFIG, $db, $server, $lang;
		$db->query('SELECT COUNT(*) AS total
						FROM '.$server.$lang.'_counter_'.date("m_Y").'
						WHERE days="'.date("j").'" AND
							  mounths="'.date("m").'" AND
							  years="'.date("y").'"');
		if ($db->nf() > 0) {
			$db->next_record();
			return $db->f('total');
		} else {
			return $this->_msg["No_data"];
		}
	}

	function getYesterdayCountUsers() {
		global $CONFIG, $db, $server, $lang;
		$db->query("SELECT COUNT(*) AS total
						FROM ".$server.$lang."_counter_all
						WHERE DATE_SUB(CURDATE(), INTERVAL 1 DAY)=FROM_UNIXTIME(utime, '%Y-%m-%d')");
		if ($db->nf() > 0) {
			$db->next_record();
			return $db->f('total');
		} else {
			return $this->_msg["No_data"];
		}
	}

    function getMounthCountUsers() {
        global $CONFIG, $db, $server, $lang;
    	$db->query('SELECT COUNT(*) AS total
    					FROM '.$server.$lang.'_counter_'.date("m_Y").'
    					WHERE mounths="'.date("m").'" AND
    						  years="'.date("y").'"');
   		if ($db->nf() > 0) {
			$db->next_record();
        	return $db->f('total');
		} else {
			return $this->_msg["No_data"];
		}
    	return FALSE;
	}

    function getOldMounthCountUsers($mounth, $year) {
        global $CONFIG, $db, $server, $lang;
		$table	= $this->mounth.'_'.$this->year;
        if (in_array($table, $this->tables)) {
			$db->query('SELECT COUNT(*) AS total FROM '.$server.$lang.'_counter_'.$table);
   			if ($db->nf() > 0) {
				$db->next_record();
        		return $db->f('total');
			}
		}
    	return $this->_msg["No_data"];
	}

	function getAllCountUsers() {
        global $CONFIG, $db, $server, $lang;
		$db->query('SELECT COUNT(ip_addr) AS total FROM '.$server.$lang.'_counter_all');
		if ($db->nf() > 0) {
			$db->next_record();
			return $db->f('total');
		} else {
			return $this->_msg["No_data"];
		}
    	return FALSE;
	}

	function getPeriodCountUsers($time, $uniqu = FALSE)
	{   global $CONFIG, $db, $server, $lang;

	    $tbl = $this->getTableByDate($time);
	    $query = "SELECT COUNT(".($uniqu ? "DISTINCT " : "")."ip_addr) AS total";
	    $query .= " FROM ".$tbl['table']." WHERE ".$tbl['where'];
		$db->query($query);
		if ($db->nf() > 0) {
			$db->next_record();
			return $db->f('total');
		} else {
			return $this->_msg["No_data"];
		}

    	return FALSE;
	}

    function getDayCountUniqueUsers() {
		global $CONFIG, $db, $server, $lang;
		$db->query('SELECT COUNT(DISTINCT ip_addr) AS total
						FROM '.$server.$lang.'_counter_'.date("m_Y").'
						WHERE days="'.date("j").'" AND
							  mounths="'.date("m").'" AND
							  years="'.date("y").'"');
		if ($db->nf() > 0) {
			$db->next_record();
			return $db->f('total');
		} else {
			return $this->_msg["No_data"];
		}
	}

	    function getYesterdayCountUniqueUsers() {
		global $CONFIG, $db, $server, $lang;
		$db->query("SELECT COUNT(DISTINCT ip_addr) AS total
						FROM ".$server.$lang."_counter_all
						WHERE DATE_SUB(CURDATE(), INTERVAL 1 DAY)=FROM_UNIXTIME(utime, '%Y-%m-%d')");
		if ($db->nf() > 0) {
			$db->next_record();
			return $db->f('total');
		} else {
			return $this->_msg["No_data"];
		}
	}

    function getMounthCountUniqueUsers() {
        global $CONFIG, $db, $server, $lang;
    	$db->query('SELECT COUNT(DISTINCT ip_addr) AS total
    					FROM '.$server.$lang.'_counter_'.date("m_Y").'
    					WHERE mounths="'.date("m").'" AND
    						  years="'.date("y").'"');
   		if ($db->nf() > 0) {
			$db->next_record();
        	return $db->f('total');
		} else {
			return $this->_msg["No_data"];
		}
    	return FALSE;
	}

    function getOldMounthCountUniqueUsers($mounth, $year) {
        global $CONFIG, $db, $server, $lang;
		$table	= $this->mounth.'_'.$this->year;
        if (in_array($table, $this->tables)) {
			$db->query('SELECT COUNT(DISTINCT ip_addr) AS total FROM '.$server.$lang.'_counter_'.$table);
   			if ($db->nf() > 0) {
				$db->next_record();
        		return $db->f('total');
			}
		}
    	return $this->_msg["No_data"];
	}

	function getAllCountUniqueUsers() {
        global $CONFIG, $db, $server, $lang;
		$db->query('SELECT COUNT(DISTINCT ip_addr) AS total FROM '.$server.$lang.'_counter_all');
		if ($db->nf() > 0) {
			$db->next_record();
			return $db->f('total');
		} else {
			return $this->_msg["No_data"];
		}
    	return FALSE;
	}

	function getLinks($time, $start = 1, $all_links = true)
	{   global $CONFIG, $db, $lang;

        if (!$start) {$start = 1;}
    		$selects = $this->getTableByDate($time);
        $guest_host=str_replace('www.', '', $_SERVER['HTTP_HOST']);
       	$db->query('SELECT COUNT(visit_from) AS count_visit FROM '.$selects['table']
       	    .' WHERE '.$selects['where'].' AND visit_from NOT LIKE "%'.$guest_host.'%"');
     	if ($db->nf() > 0) {
     		$db->next_record();
     		$count = $db->f('count_visit');
     	}

        $limit = $all_links ? 'LIMIT '.(($start - 1)*$CONFIG['counter_max_rows']).', '.$CONFIG['counter_max_rows'] : '';
        $db->query('SELECT visit_from_site, visit_from, COUNT(visit_from) AS count_visit
          					FROM '.$selects['table'].'
          					WHERE '.$selects['where'].'
          					GROUP BY visit_from
                    HAVING visit_from_site NOT LIKE "%'.$guest_host.'%"
                    ORDER BY count_visit DESC
    					      ' . $limit);

 		if ($db->nf() > 0) {
 		    for($i = 0; $i < $db->nf(); $i++) {
 		        $db->next_record();
 		        $arr[$i]['link'] = str_replace('http://','',$db->f('visit_from'));
 		        $arr[$i]['count_link'] = $db->f('count_visit');
                if ($count > 0 && $db->f('count_visit') > 0) {
                	$arr[$i]['percent_link'] = (float)(((int)((($db->f('count_visit')/$count)*100)*100))/100);
                } else {
                	$arr[$i]['percent_link'] = 0;
                }
 		    }
			return $arr;
		} else {
			return $this->_msg["No_data"];
		}
    	return FALSE;
	}

	function getReferringHosts($time, $start = 1, $all_links = true)
	{   global $CONFIG, $db, $lang;

        if (!$start) {$start = 1;}
		$selects = $this->getTableByDate($time);
        $guest_host=str_replace('www.', '', $_SERVER['HTTP_HOST']);
   		$db->query('SELECT COUNT(visit_from_site) AS count_visit
						FROM '.$selects['table'].'
						WHERE '.$selects['where'].' AND
                        	  visit_from_site NOT LIKE "'.$_SERVER['HTTP_HOST'].'%" AND
					  		  visit_from_site NOT LIKE "'.$guest_host.'%"');
   		if ($db->nf() > 0) {
           	$db->next_record();
			$count = $db->f('count_visit');
        }
        $limit = $all_links ? 'LIMIT '.(($start - 1)*$CONFIG['counter_max_rows']).', '.$CONFIG['counter_max_rows'] : '';
   		$db->query('SELECT visit_from_site, COUNT(visit_from_site) AS count_visit
						FROM '.$selects['table'].'
						WHERE '.$selects['where'].'
						GROUP BY visit_from_site
                        HAVING visit_from_site NOT LIKE "'.$_SERVER['HTTP_HOST'].'%" AND
					  		   visit_from_site NOT LIKE "'.$guest_host.'%"
                        ORDER BY count_visit DESC '.$limit);
   		if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
            	$db->next_record();
				$arr[$i]['link']			= $db->f('visit_from_site');
				$arr[$i]['count_link']		= $db->f('count_visit');
                if ($count > 0 && $db->f('count_visit') > 0) {
                	$arr[$i]['percent_link']	= (float)(((int)((($db->f('count_visit')/$count)*100)*100))/100);
                } else {
                	$arr[$i]['percent_link']	= 0;
                }
			}
			return $arr;
		} else {
			return $this->_msg["No_data"];
		}
		return FALSE;
	}

	function getPages($time, $start, $all_links = true)
	{   global $CONFIG, $db, $lang, $server;

        if (!$start) {$start = 1;}
		$selects = $this->getTableByDate($time);
        $guest_host=str_replace('www.', '', $_SERVER['HTTP_HOST']);

   		$db->query('SELECT COUNT(page_visit) AS count_visit
						FROM '.$selects['table'].'
						WHERE '.$selects['where']);
   		if ($db->nf() > 0) {
           	$db->next_record();
			$count = $db->f('count_visit');
        }

        $limit = $all_links ? 'LIMIT '.(($start - 1)*$CONFIG['counter_max_rows']).', '.$CONFIG['counter_max_rows'] : '';
   		$db->query('SELECT page_visit, page_title, COUNT(page_visit) AS count_visit
						FROM '.$selects['table'].'
						LEFT JOIN '.$server.$lang.'_counter_page_titles ON page_url = page_visit
						WHERE '.$selects['where'].'
						GROUP BY page_visit
                        ORDER BY count_visit DESC '.$limit);

   		if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
            	$db->next_record();
				$arr[$i]['link']			= $db->f('page_visit');
				$arr[$i]['title']			= $db->f('page_title');
				$arr[$i]['count_link']		= $db->f('count_visit');
                if ($count > 0 && $db->f('count_visit') > 0) {
                	$arr[$i]['percent_link']	= (float)(((int)((($db->f('count_visit')/$count)*100)*100))/100);
                } else {
                	$arr[$i]['percent_link']	= 0;
                }

			}
			return $arr;
		} else {
			return $this->_msg["No_data"];
		}
    	return FALSE;
	}

    function getUsersLang($time)
    {   global $CONFIG, $db, $lang;

		$selects = $this->getTableByDate($time);
   		$db->query('SELECT COUNT(user_lang) AS count_lang
						FROM '.$selects['table'].'
						WHERE '.$selects['where']);
   		if ($db->nf() > 0) {
           	$db->next_record();
			$count = $db->f('count_lang');
        }

   		$db->query('SELECT user_lang, COUNT(user_lang) AS count_lang
						FROM '.$selects['table'].'
						WHERE '.$selects['where'].'
						GROUP BY user_lang
                        ORDER BY count_lang DESC');
   		if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
            	$db->next_record();
				$arr[$i]['users_lang']			= strtoupper($db->f('user_lang'));
				$arr[$i]['users_count_lang']	= $db->f('count_lang');
                if ($count > 0 && $db->f('count_lang') > 0) {
                	$arr[$i]['users_lang_percent']	= (float)(((int)((($db->f('count_lang')/$count)*100)*100))/100);
                } else {
                	$arr[$i]['users_lang_percent']	= 0;
                }

			}
			return $arr;
		} else {
        	return $this->_msg["No_data"];
		}
   		return FALSE;
	}

    function getUsersOS($time)
    {   global $CONFIG, $db, $lang;

		$selects = $this->getTableByDate($time);
   		$db->query('SELECT COUNT(os_user) AS count_os
						FROM '.$selects['table'].'
						WHERE '.$selects['where']);
   		if ($db->nf() > 0) {
           	$db->next_record();
			$count = $db->f('count_os');
        }

   		$db->query('SELECT os_user, COUNT(os_user) AS count_os
						FROM '.$selects['table'].'
						WHERE '.$selects['where'].'
						GROUP BY os_user
                        ORDER BY count_os DESC');
   		if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
            	$db->next_record();
				$arr[$i]['users_os']		= $db->f('os_user');
				$arr[$i]['users_count_os']	= $db->f('count_os');
                if ($count > 0 && $db->f('count_os') > 0) {
                	$arr[$i]['users_os_percent']	= (float)(((int)((($db->f('count_os')/$count)*100)*100))/100);
                } else {
                	$arr[$i]['users_os_percent']	= 0;
                }

			}
			return $arr;
		} else {
        	return $this->_msg["No_data"];
		}
    	return FALSE;
	}

    function getUsersBrowsers($time)
    {   global $CONFIG, $db, $lang;

		$selects = $this->getTableByDate($time);
   		$db->query('SELECT COUNT(user_agent) AS count_user_agent
						FROM '.$selects['table'].'
						WHERE '.$selects['where']);
   		if ($db->nf() > 0) {
           	$db->next_record();
			$count = $db->f('count_user_agent');
        }

   		$db->query('SELECT user_agent, COUNT(*) AS count_user_agent
						FROM '.$selects['table'].'
						WHERE '.$selects['where'].' AND ""!=user_agent
						GROUP BY user_agent
                        ORDER BY count_user_agent DESC');
   		if ($db->nf() > 0) {
   		    $agent_cnt = 0;
            for($i = 0; $i < $db->nf(); $i++) {
            	$db->next_record();
            	$agent_cnt += intval($db->f('count_user_agent'));
				$arr[$i]['users_browsers']			= $db->f('user_agent');
				$arr[$i]['users_count_browsers']	= $db->f('count_user_agent');
                if ($count > 0 && $db->f('count_user_agent') > 0) {
                	$arr[$i]['users_browsers_percent']	= (float)(((int)((($db->f('count_user_agent')/$count)*100)*100))/100);
                } else {
                	$arr[$i]['users_browsers_percent']	= 0;
                }
			}
			if ($agent_cnt < $count) {
			    $arr[$i]['users_browsers'] = 'N\A';
			    $arr[$i]['users_count_browsers'] = $count - $agent_cnt;
			    $arr[$i]['users_browsers_percent'] = (float)(((int)((($arr[$i]['users_count_browsers']/$count)*100)*100))/100);
			}
			return $arr;
		} else {
        	return $this->_msg["No_data"];
		}
    	return FALSE;
	}

    function getUsersResolution($time)
    {   global $CONFIG, $db, $lang;

		$selects = $this->getTableByDate($time);
   		$db->query('SELECT COUNT(user_resolution) AS count_user_resolution
						FROM '.$selects['table'].'
						WHERE '.$selects['where']);
   		if ($db->nf() > 0) {
           	$db->next_record();
			$count = $db->f('count_user_resolution');
        }

   		$db->query('SELECT user_resolution, COUNT(user_resolution) AS count_user_resolution
						FROM '.$selects['table'].'
						WHERE '.$selects['where'].'
						GROUP BY user_resolution
                        ORDER BY count_user_resolution DESC');
   		if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
            	$db->next_record();
            	$user_resolution = $db->f('user_resolution');
				$arr[$i]['users_resolution']		= $user_resolution ? $user_resolution : "n/a";
				$arr[$i]['users_count_resolution']	= $db->f('count_user_resolution');
                if ($count > 0 && $db->f('count_user_resolution') > 0) {
                	$arr[$i]['users_resolution_percent']	= (float)(((int)((($db->f('count_user_resolution')/$count)*100)*100))/100);
                } else {
                	$arr[$i]['users_resolution_percent']	= 0;
                }

			}
			return $arr;
		} else {
        	return $this->_msg["No_data"];
		}
    	return FALSE;
	}

    function getUsersIP($time)
    {   global $CONFIG, $db, $lang;

		$selects = $this->getTableByDate($time);
   		$db->query('SELECT COUNT(ip_addr) AS count_ip_addr
						FROM '.$selects['table'].'
						WHERE '.$selects['where']);
   		if ($db->nf() > 0) {
           	$db->next_record();
			$count = $db->f('count_ip_addr');
        }
   		$db->query('SELECT ip_addr, COUNT(ip_addr) AS count_ip_addr
						FROM '.$selects['table'].'
						WHERE '.$selects['where'].'
						GROUP BY ip_addr
                        ORDER BY count_ip_addr DESC
                        LIMIT '.$CONFIG['counter_max_rows']);
   		if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
            	$db->next_record();
				$arr[$i]['users_ip']		= long2ip($db->f('ip_addr'));
				$arr[$i]['users_count_ip']	= $db->f('count_ip_addr');
                if ($count > 0 && $db->f('count_ip_addr') > 0) {
                	$arr[$i]['users_ip_percent']	= (float)(((int)((($db->f('count_ip_addr')/$count)*100)*100))/100);
                } else {
                	$arr[$i]['users_ip_percent']	= 0;
                }
			}
			return $arr;
		} else {
        	return $this->_msg["No_data"];
		}
    	return FALSE;
	}

	function getSpidersStatistic($time)
	{
    	return FALSE;
	}

	function getSearchEngStatistic($time)
	{   global $CONFIG, $db, $baseurl;

        $selects = $this->getTableByDate($time);

        $query = 'SELECT COUNT(visit_from) AS CNT, engine FROM '.$selects['table'].' WHERE '.$selects['where'];
        $query .= ' AND engine > 0 GROUP BY engine ORDER BY CNT DESC';
        $db->query($query);

        $arr = array();
        $sum = 0;
        $num = 0;
        while ($db->next_record()) {
        	$num++;
        	$engine_code = $db->f('engine');
        	$cnt = $db->f('CNT');
        	$sum += $cnt;
        	$arr[] = array('num' => $num,
        	               'visit' => $cnt,
        	               'engine_code' => $engine_code,
        	               'engine_name' => $CONFIG['search_engine_name'][$engine_code],
        	               'query_link' => $baseurl."&action=search_query&engine=".$engine_code."&time=".$time
        	              );
        }
        for ($i=0, $cnt = count($arr); $i < $cnt; $i++) {
        	$arr[$i]['visit_percent'] = number_format(($arr[$i]['visit'] * 100 / $sum), 2, '.', '');
        }

    	return $arr;
	}

	function getSearchEngQueryStatistic($engine_code, $time)
	{   global $CONFIG, $db;

        if (!array_key_exists($engine_code, $CONFIG['search_engine_name'])) {
        	return FALSE;
        }

        $selects = $this->getTableByDate($time);

        $query = 'SELECT engine_str, COUNT(engine_str) AS CNT FROM '.$selects['table'].' WHERE '.$selects['where'];
        $query .= ' AND engine = \''.$engine_code.'\' GROUP BY engine_str ORDER BY CNT DESC';
        $db->query($query);

        $arr = array();
        $num = 0;
        $sum = 0;
        while ($db->next_record()) {
        	$num++;
        	$cnt = $db->f('CNT');
        	$sum += $cnt;
        	$engine_str = $db->f('engine_str');
        	$arr[] = array('num' => $num,
        	               'cnt' => $cnt,
        	               'query' => $engine_str ? $engine_str : "n\a"
        	              );
        }
        for ($i=0, $cnt = count($arr); $i < $cnt; $i++) {
        	$arr[$i]['cnt_percent'] = number_format(($arr[$i]['cnt'] * 100 / $sum), 2, '.', '');
        }

    	return $arr;
	}

	function getGeoStatistic($time, $block_name = '_ROOT')
	{   global $lang, $baseurl;

	    $period = $this->getBulletForCurrentTime($time);
        $arr[$block_name.'.time']       = $period[0]['time'];
        $arr[$block_name.'.time_type']  = $period[0]['time_type'];
        $arr[$block_name.'.lang']       = $lang;
        $arr[$block_name.'.baseurl']    = $baseurl;

        return $arr;
	}

	function getBulletForCurrentTime($time)
	{   global $CONFIG, $db, $lang;

        $time = $this->checkPeriod($time);
        switch ($time) {
        	case 0:
                $arr[0]['time_number']	= '1';
                $arr[0]['time']			= $this->_msg["Current_hour"];
                $arr[0]['time_type']    = "time_type=0";
                $arr[1]['time_number']	= '2';
                $arr[1]['time']			= $this->_msg["Last_hour"];
        		break;

           	case 1:
                $arr[0]['time_number']	= '1';
                $arr[0]['time']			= $this->_msg["Current_day"];
                $arr[0]['time_type']    = "time_type=1";
                $arr[1]['time_number']	= '2';
                $arr[1]['time']			= $this->_msg["Old_day"];
        		break;

           	case 2:
                $arr[0]['time_number']	= '1';
                $arr[0]['time']			= $this->_msg["Current_month"];
                $arr[0]['time_type']    = "time_type=2";
                $arr[1]['time_number']	= '2';
                $arr[1]['time']			= $this->_msg["Last_month"];
        		break;

            case 3:
                $arr[0]['time_number']	= '1';
                $arr[0]['time']			= $this->_msg["Current_year"];
                $arr[0]['time_type']    = "time_type=3";
                $arr[1]['time_number']	= '2';
                $arr[1]['time']			= $this->_msg["Old_year"];
        		break;

            case 4:
                $arr[0]['time_number']	= '1';
                $arr[0]['time']			= $this->_msg["Total_time"];
                $arr[0]['time_type']    = "time_type=4";
        		break;

        	default:
                $arr[0]['time_number']	= '1';
                $arr[0]['time']			= $this->_msg["Period"] . "".($time['from'] ? " c ".$time['from'] : "").($time['to'] ? " по ".$time['to'] : "");
                $arr[0]['time_type']    = "time_type[from]=".@$time['from']."&time_type[to]=".@$time['to'];
        		break;
        }
		return $arr;
	}

	function getTableByDate($time)
	{   global $CONFIG, $db, $server, $lang;

        $time = $this->checkPeriod($time);

        switch ($time) {
			case 0: /* Час */
        		$where = 'the_time = '.date('G').' AND days = '.date('j');
                $table = $server.$lang.'_counter_'.date('m_Y');
				break;

			case 1: /* День */
        		$where = 'days = '.date('j');
                $table = $server.$lang.'_counter_'.date('m_Y');
				break;

			case 2: /* Месяц */
        		$where = '1 = 1';
                $table = $server.$lang.'_counter_'.date('m_Y');
				break;

   			case 3: /* Год */
        		$where = 'years = '.(int)date('y');
                $table = $server.$lang.'_counter_all';
                break;

   			case 4: /* Всё время */
        		$where = '1 = 1';
                $table = $server.$lang.'_counter_all';
                break;

            default: /*  Период */
        		if (is_array($time) && (isset($time['from']) || isset($time['to'])) ) {
        		    $from = @$time['from'] ? strtotime(str_replace(".", "-", $time['from'])) : -1;
        		    $to = @$time['to'] ? strtotime(str_replace(".", "-", $time['to'])) : -1;
        		    $to = -1 == $to ? $to : $to + 60*60*24;
        		    if (-1 != $from && -1 != $to) {
        		        if ($from > $to) {
        		        	$t = $from;
        		        	$from = $to;
        		        	$to = $t;
        		        }
        		    	$where = "utime BETWEEN '".$from."' AND '".$to."'";
        		    } elseif (-1 != $from) {
        		        $where = "utime >= ".$from;
        		    } elseif (-1 != $to) {
        		        $where = "utime <= ".$to;
        		    } else {
        		        $where = '1 = 1';
        		    }
        		    $table = $server.$lang.'_counter_all';
        		}
                break;
		}
        $arr['where'] = $where;
        $arr['table'] = $table;
    	return $arr;
	}

	function getVisitModStatistic($time)
	{   global $CONFIG, $db, $server, $lang;

		$time = $this->checkPeriod($time);

		$tbl = $this->getTableByDate($time);

		$ret = array();
		$query = "SELECT count(*) AS visit, COUNT(DISTINCT ip_addr) AS uniq, module_id, M.title";
		$query .= " FROM ".$tbl['table']." LEFT JOIN modules AS M ON module_id=M.id";
		$query .= " WHERE ".$tbl['where']." GROUP BY module_id ORDER BY visit DESC";
		$db->query($query);
		while ($db->next_record()) {
		    $title = $db->f('title');
			$ret[] = array('title' => $title ? $title : "n\a",
			               'visit' => $db->f('visit'),
			               'uniq' => $db->f('uniq'),
			               'module_id' => $db->f('module_id'));
		}

		//echo "<pre>";print_r($ret);echo "</pre>";
		return $ret;
	}

	function getEntrys($time)
	{   global $CONFIG, $db, $server, $lang;

		$time = $this->checkPeriod($time);

		$tbl = $this->getTableByDate($time);

		$ret = array();

		$query = "CREATE TEMPORARY TABLE tmp (";
        $query .= "sid BIGINT NOT NULL DEFAULT 0,";
        $query .= "hits BIGINT NOT NULL DEFAULT 0,";
        $query .= "min_time BIGINT NOT NULL DEFAULT 0,";
        $query .= "max_time BIGINT NOT NULL DEFAULT 0)";
        $db->query($query);

        /*
		$query = "LOCK TABLES shop read";
		$db->query($query);
        */
		$query = "INSERT INTO tmp SELECT sid, COUNT(sid), MIN(utime), MAX(utime) FROM ".$tbl['table'];
        $query .= " WHERE ".$tbl['where']." GROUP BY sid";
        $db->query($query);

		$query = "SELECT page_visit, page_title, hits, COUNT(page_visit) AS entrys, max_time FROM";
		$query .= " tmp AS T, ".$tbl['table']." AS C LEFT JOIN";
		$query .= " ".$server.$lang."_counter_page_titles ON page_visit=page_url";
		$query .= " WHERE ".$tbl['where']." AND T.sid=C.sid AND min_time=utime";
		$query .= " GROUP BY page_visit ORDER BY entrys DESC ";
		$db->query($query);

		while ($db->next_record()) {
	        $ret[] = array('link' => $db->f('page_visit'),
	                       'title' => $db->f('page_title'),
		                   'hits' => $db->f('hits'),
		                   'entry_count' => $db->f('entrys'),
		                   'last_time' => date("Y-m-d H:m:s", $db->f('max_time')));
		}

		/*
		$query = "UNLOCK TABLES";
		$db->query($query);
        */

		$query = "DROP TABLE tmp";
		$db->query($query);
		//echo "<pre>";print_r($ret);echo "</pre>";
		return $ret;
	}

	function getExits($time)
	{   global $CONFIG, $db, $server, $lang;

		$time = $this->checkPeriod($time);

		$tbl = $this->getTableByDate($time);

		$ret = array();

		$query = "CREATE TEMPORARY TABLE tmp (";
        $query .= "sid BIGINT NOT NULL DEFAULT 0,";
        $query .= "hits BIGINT NOT NULL DEFAULT 0,";
        $query .= "max_time BIGINT NOT NULL DEFAULT 0)";
        $db->query($query);

        /*
		$query = "LOCK TABLES shop read";
		$db->query($query);
        */
		$query = "INSERT INTO tmp SELECT sid, COUNT(sid), MAX(utime) FROM ".$tbl['table'];
        $query .= " WHERE ".$tbl['where']." GROUP BY sid";
        $db->query($query);

		$query = "SELECT page_visit, page_title, hits, COUNT(page_visit) AS exits, max_time FROM";
		$query .= " tmp AS T, ".$tbl['table']." AS C LEFT JOIN";
		$query .= " ".$server.$lang."_counter_page_titles ON page_visit=page_url";
		$query .= " WHERE ".$tbl['where']." AND T.sid=C.sid AND max_time=utime";
		$query .= " GROUP BY page_visit ORDER BY exits DESC ";
		$db->query($query);

		while ($db->next_record()) {
	        $ret[] = array('link' => $db->f('page_visit'),
		                   'title' => $db->f('page_title'),
	                       'hits' => $db->f('hits'),
		                   'exit_count' => $db->f('exits'),
		                   'last_time' => date("Y-m-d H:m:s", $db->f('max_time')));
		}

		/*
		$query = "UNLOCK TABLES";
		$db->query($query);
        */

		$query = "DROP TABLE tmp";
		$db->query($query);
		//echo "<pre>";print_r($ret);echo "</pre>";
		return $ret;
	}

	function getLoads($time)
	{   global $CONFIG, $db, $server, $lang;

		$time = $this->checkPeriod($time);

		$tbl = $this->getTableByDate($time);

		$ret = array();

		$query = "SELECT page_visit, page_title, utime, COUNT(sid) AS hits FROM";
		$query .= " ".$tbl['table']." AS C LEFT JOIN";
		$query .= " ".$server.$lang."_counter_page_titles ON page_visit=page_url";
		$query .= " WHERE ".$tbl['where'];
		$query .= " GROUP BY sid HAVING hits = 1 ORDER BY utime DESC";
		$db->query($query);

		while ($db->next_record()) {
	        $ret[] = array('link' => $db->f('page_visit'),
		                   'title' => $db->f('page_title'),
	                       'time' => date("Y-m-d H:m:s", $db->f('utime')));
		}

		//echo $query."<pre>";print_r($ret);echo "</pre>";
		return $ret;
	}

	function getDepths($time)
	{   global $CONFIG, $db, $server, $lang;

		$time = $this->checkPeriod($time);

		$tbl = $this->getTableByDate($time);

		$depth_key = $ret = array();

		$query = "SELECT COUNT(sid) AS depth, MIN(utime) AS entry, MAX(utime) AS `exit`";
		$query .= " FROM ".$tbl['table']." WHERE ".$tbl['where']." GROUP BY sid ORDER BY depth DESC";
		$db->query($query);

		$i = $total = 0;
		while ($db->next_record()) {
		    $depth = $db->f('depth');
		    $entry = $db->f('entry');
		    $exit = $db->f('exit');
		    $total++;
		    if (!isset($depth_key[$depth])) {
		    	$depth_key[$depth] = array($entry, $exit, $i);
		    	$ret[$i] = array('depth' => $depth,
		    	                 'sess_count' => 1,
		    	                 'entry' => $entry,
		    	                 'exit' => $exit,
		    	                 'last_entry' => date("Y-m-d H:m:s", $entry),
		    	                 'last_exit' => date("Y-m-d H:m:s", $exit));
		    	$i++;
		    } else {
		        $ret[$depth_key[$depth][2]]['sess_count']++;
		        if ($entry > $depth_key[$depth][0]) {
		            $ret[$depth_key[$depth][2]]['entry'] = $entry;
		            $ret[$depth_key[$depth][2]]['last_entry'] = date("Y-m-d H:m:s", $entry);
		        }
		        if ($exit > $depth_key[$depth][1]) {
		            $ret[$depth_key[$depth][2]]['exit'] = $exit;
		            $ret[$depth_key[$depth][2]]['last_exit'] = date("Y-m-d H:m:s", $exit);
		        }
		    }
		}
		$total = $total/100;
		foreach ($ret as $key => $val) {
			$ret[$key]['sess_count_rate'] = number_format($ret[$key]['sess_count']/$total, 1, ".", "");
		}

		//echo $query."<pre>";print_r($ret);echo "</pre>";
		return $ret;
	}

	function getPaths($time)
	{   global $CONFIG, $db, $server, $lang;

		$time = $this->checkPeriod($time);

		$tbl = $this->getTableByDate($time);

		$ret = array();

		$query = "SELECT page_visit, sid FROM ".$tbl['table'];
		$query .= " WHERE ".$tbl['where']." GROUP BY sid, page_visit ORDER BY sid, utime";
		$db->query($query);

		$nf = $db->nf();
		if (0 == $nf) {
			return $ret;
		}

		$db->next_record();
		$cur_sid = $db->f('sid');
		$path = array($db->f('page_visit'));
		if (1 == $nf) {
			return array(array('count' => 1, 'path' => $path));
		}

		$paths = array();
		$i = $total = 0;
		while ($db->next_record()) {
		    $sid = $db->f('sid');
		    if ($cur_sid != $sid) {
		        $k = array_search($path, $paths);
		    	if (FALSE === $k || NULL === $k) {
		    		$paths[$i] = $path;
		    		$ret[$i] = array('count' => 1, 'path' => $path);
		    		$path = array($db->f('page_visit'));
		    		$i++;
		    	} else {
		    	    $ret[$k]['count']++;
		    	    $path = array();
		    	}
		    	$cur_sid = $sid;
		    	$total++;
		    } else {
		        $path[] = $db->f('page_visit');
		    }
		}

		$k = array_search($path, $paths);
		if (FALSE === $k || NULL === $k) {
		    $ret[$i] = array('count' => 1, 'path' => $path);
		} else {
		    $ret[$k]['count']++;
		}
		$total++;

		usort($ret, abo_counter_cmp);

		$total = $total/100;
		foreach ($ret as $key => $val) {
			$ret[$key]['count_rate'] = number_format($ret[$key]['count']/$total, 1, ".", "");
		}

		//echo $query."<pre>";print_r($ret);echo "</pre>";
		return $ret;
	}

	function getVisits($time)
	{   global $db, $server, $lang;

	    $time = $this->checkPeriod($time);
        $ret = array();

        switch ($time) {
			case 0: /* Час */
        		$where = 'the_time = '.date('G').' AND days = '.date('j');
                $table = $server.$lang.'_counter_'.date('m_Y');
                $group = 'minuts';
                $period = "FROM_UNIXTIME(utime, '%d-%m-%Y %h:%i')";
				break;

			case 1: /* День */
        		$where = 'days = '.date('j');
                $table = $server.$lang.'_counter_'.date('m_Y');
                $group = 'the_time';
                $period = "FROM_UNIXTIME(utime, '%d-%m-%Y %h:00')";
				break;

			case 2: /* Месяц */
        		$where = '1 = 1';
                $table = $server.$lang.'_counter_'.date('m_Y');
                $group = 'days';
                $period = "FROM_UNIXTIME(utime, '%d-%m-%Y')";
				break;

   			case 3: /* Год */
        		$where = 'years = '.(int)date('y');
                $table = $server.$lang.'_counter_all';
                $group = 'mounths';
                $period = "FROM_UNIXTIME(utime, '%m-%Y')";
                break;

   			case 4: /* Всё время */
        		$where = '1 = 1';
                $table = $server.$lang.'_counter_all';
                $group = 'years, mounths';
                $period = "FROM_UNIXTIME(utime, '%m-%Y')";
                break;

            default: /*  Период */
        		if (is_array($time) && (isset($time['from']) || isset($time['to'])) ) {
        		    $from = @$time['from'] ? strtotime(str_replace(".", "-", $time['from'])) : -1;
        		    $to = @$time['to'] ? strtotime(str_replace(".", "-", $time['to'])) : -1;
        		    $to = -1 == $to ? $to : $to + 60*60*24;
        		    if (-1 != $from && -1 != $to) {
        		        if ($from > $to) {
        		        	$t = $from;
        		        	$from = $to;
        		        	$to = $t;
        		        }
        		    	$where = "utime BETWEEN '".$from."' AND '".$to."'";
        		    } elseif (-1 != $from) {
        		        $where = "utime >= ".$from;
        		    } elseif (-1 != $to) {
        		        $where = "utime <= ".$to;
        		    } else {
        		        $where = '1 = 1';
        		    }
        		    $table = $server.$lang.'_counter_all';
        		    $group = 'years, mounths, days';
        		    $period = "FROM_UNIXTIME(utime, '%d-%m-%Y')";
        		}
                break;
		}

		if (!isset($where)) {
			return FALSE;
		}

		$query = "SELECT ".$period." AS period, COUNT(ip_addr) AS VISIT, COUNT(DISTINCT ip_addr) AS uniq";
		$query .= " FROM ".$table." WHERE ".$where." GROUP BY ".$group;
		//echo $query; exit;
		return $db->getArrayOfResult($query);
	}

	function getGeoCity($time)
	{   global $db, $server, $lang;

	    $time = $this->checkPeriod($time);
        $ret = array();

        switch ($time) {
			case 0: /* Час */
        		$where = 'the_time = '.date('G').' AND days = '.date('j');
                $table = $server.$lang.'_counter_'.date('m_Y');
				break;

			case 1: /* День */
        		$where = 'days = '.date('j');
                $table = $server.$lang.'_counter_'.date('m_Y');
				break;

			case 2: /* Месяц */
        		$where = '1 = 1';
                $table = $server.$lang.'_counter_'.date('m_Y');
				break;

   			case 3: /* Год */
        		$where = 'years = '.(int)date('y');
                $table = $server.$lang.'_counter_all';
                break;

   			case 4: /* Всё время */
        		$where = '1 = 1';
                $table = $server.$lang.'_counter_all';

                break;

            default: /*  Период */
        		if (is_array($time) && (isset($time['from']) || isset($time['to'])) ) {
        		    $from = @$time['from'] ? strtotime(str_replace(".", "-", $time['from'])) : -1;
        		    $to = @$time['to'] ? strtotime(str_replace(".", "-", $time['to'])) : -1;
        		    $to = -1 == $to ? $to : $to + 60*60*24;
        		    if (-1 != $from && -1 != $to) {
        		        if ($from > $to) {
        		        	$t = $from;
        		        	$from = $to;
        		        	$to = $t;
        		        }
        		    	$where = "utime BETWEEN '".$from."' AND '".$to."'";
        		    } elseif (-1 != $from) {
        		        $where = "utime >= ".$from;
        		    } elseif (-1 != $to) {
        		        $where = "utime <= ".$to;
        		    } else {
        		        $where = '1 = 1';
        		    }
        		    $table = $server.$lang.'_counter_all';
        		}
                break;
		}

		if (!isset($where)) {
			return FALSE;
		}

		$query = "CREATE TEMPORARY TABLE ".$server.$lang."_temp_geo_city";
		$query .= " SELECT ip_addr, COUNT(ip_addr) AS count, COUNT(DISTINCT ip_addr) AS uniq";
		$query .= " FROM  ".$table." WHERE ".$where." GROUP BY ip_addr";
		$db->query($query);

		$query = "SELECT IF(ISNULL(b.name ), '".UNKNOWN."', b.name) AS city,";
		$query .= " a.count AS count, a.uniq AS uniq";
		$query .= " FROM  ".$server.$lang."_temp_geo_city AS a";
		$query .= " LEFT JOIN counter_cities AS b USE INDEX (long_ip)";
		$query .= "ON (a.ip_addr BETWEEN b.first_long_ip AND b.last_long_ip)";
		$query .= "GROUP BY city ORDER BY count DESC";
		//echo $query; exit;
		return $db->getArrayOfResult($query);
	}

	function assignCsvReportLink($block_name = '_ROOT')
	{   global $tpl, $baseurl, $CONFIG;

	    $link = $baseurl."&action=csv&report=".@$_REQUEST['action'];
	    $link .= @isset($_REQUEST['mod']) ? "&mod=".$_REQUEST['mod'] : "";
	    if (is_array($_REQUEST['time']) && (isset($_REQUEST['time']['from']) || isset($_REQUEST['time']['to'])) ) {
	    	$link .= "&time[from]=".@$_REQUEST['time']['from']."&time[to]=".@$_REQUEST['time']['to'];
	    } elseif (isset($_REQUEST['time']) && array_key_exists($_REQUEST['time'], $CONFIG['counter_variants_of_time'][$CONFIG['admin_lang']])) {
	        $link .= "&time=".$_REQUEST['time'];
	    }
	    $tpl->assign($block_name.'.cvs_report_link', $link);
	}

	function getCsvStr($arg, $seprt = ";", $quot = '"')
	{
		if (is_array($arg)) {
			foreach ($arg as $key=>$val) {
			    $out_val = FALSE !== strpos($val, $quot)
			               ? str_replace($quot, $quot.$quot, $val)
			               : $val;
				$arg[$key] = FALSE !== strpos($out_val, $seprt) || $out_val != $val || FALSE !== strpos($val, "\n")
				             ? $quot.$out_val.$quot
				             : $out_val;
			}

			return implode($seprt, $arg)."\n";
		}

		$out_val = FALSE !== strpos($arg, $quot)
			       ? str_replace($quot, $quot.$quot, $arg)
			       : $arg;

		return FALSE !== strpos($out_val, $seprt) || $out_val != $arg || FALSE !== strpos($arg, "\n")
				             ? $quot.$out_val.$quot."\n"
				             : $out_val."\n";
	}

	/**
	 * Пустые или нет таблицы GEO IP
	 *
	 * @return boolean
	 */
	function isEmptyGeoIpTables()
	{
		global $db;
		$db->query("SELECT COUNT(*) FROM counter_cities");
		if (! $db->nf()) {
			return TRUE;
		}
		$db->next_record();
		if (0 == $db->Record[0]) {
			return TRUE;
		}

		return false;
	}

    /**
     * Получить список городов из базы GEO IP
     *
     * @param string $zone
     * @return array
     */
    function getCityList($filter = false)
    {
    	global $db;

    	$where = false;
    	if (isset($filter['zone'])) {
    		$where[] = "zone='".$db->escape($filter['zone'])."'";
    	}
    	if (isset($filter['ids'])) {
    		$where[] = "city_id IN (".$db->escape($filter['ids'] ? $filter['ids'] : '-1').")";
    	}
    	if (isset($filter['id'])) {
    		$where[] = "city_id = ".intval($filter['id']);
    	}
    	if (isset($filter['ip'])) {
    	    $ip = sprintf("%u", ip2long($filter['ip']));
    		$where[] = $ip." BETWEEN first_long_ip AND last_long_ip";
    	}
    	$db->query("SELECT DISTINCT city_id, name, region, zone FROM counter_cities"
    	    .($where ? " WHERE ".implode(" AND ", $where) : "")
    	    ." ORDER BY name");
    	$ret = array();
    	while ($db->next_record()) {
    		$ret[$db->Record['city_id']] = array(
    		    'city_id' => $db->Record['city_id'],
    		    'name' => $db->Record['name'],
    		    'region' => $db->Record['region'],
    		    'zone' => $db->Record['zone'],
    		);
    	}

    	return $ret;
    }
}

function utf2ansii($var){
      static $table;
      if(!$table){
        $table = array(
                "\xD0\x81" => "\xA8", // Ё
                "\xD1\x91" => "\xB8", // ё
        );
      }
      if(is_array($var)){foreach($var as $_k => $_v){$ret[$_k] = utf2ansii($_v);}return $ret;}
       elseif(is_string($var)){
        $ret = preg_replace(
                '#([\xD0-\xD1])([\x80-\xBF])#se',
                'isset($table["$0"]) ? $table["$0"] : chr(ord("$2")+("$1" == "\xD0" ? 0x30 : 0x70))',
                $var
        );
        return $ret;
      }
      return $var;
}
?>