<?php

$db->query('SELECT * FROM '.$server.$lang.'_counter_tables');
if ($db->nf() > 0) {
    for ($i = 0; $i < $db->nf(); $i++) {
        $db->next_record();
		$table_name[]=$db->f('name');
	}
}

$date			= date('m_Y');
$list_tables	= (is_array($table_name)) ? implode(', ', $table_name) : '';
if (isset($list_tables) && $list_tables != '') {
	$list_tables_2	= $server.$lang.'_counter_'.str_replace(', ', ', '.$server.$lang.'_counter_', $list_tables);
}

if (!is_array($table_name) || !in_array($date, $table_name)) {
	$db->query("CREATE TABLE IF NOT EXISTS ".$server.$lang."_counter_".$date." (
					user_agent		enum('Another',
										 'IE 6.0',
										 'IE 5.5',
										 'IE 5.0',
										 'IE 4.0',
                                         'Opera 8.xx',
										 'Opera 7.xx',
										 'Opera 6.xx',
										 'Opera 5.xx',
										 'Netscape 7.x',
										 'Netscape 6.x',
										 'Netscape 4.x',
										 'Konqueror',
										 'Lynx',
										 'Galeon',
										 'Epiphany',
										 'Firefox',
										 'Mozilla')	NOT NULL,
					page_visit		varchar(255)	NOT NULL,
					ip_addr			bigint(20)  unsigned NOT NULL default 0,
					days			tinyint(3)	unsigned NOT NULL,
					mounths			tinyint(3)	unsigned NOT NULL,
					years			tinyint(3)	unsigned NOT NULL,
					the_time		tinyint(4)	NOT NULL,
					minuts			tinyint(4)	NOT NULL,
					os_user			enum('Windows 2003',
										 'Windows XP',
										 'Windows 2000',
										 'Windows NT',
										 'Windows 98',
										 'Windows 95',
										 'Linux',
										 'Unix',
										 'FreeBSD',
										 'IRIX',
										 'BEOS',
										 'Mac',
										 'OS/2',
										 'AIX',
										 'SunOS',
										 'Another')	NOT NULL,
					user_lang		char(2)		NOT NULL,
					visit_from		varchar(255)	NOT NULL,
					visit_from_site varchar(100)	NOT NULL,
					user_resolution	enum('640x480',
									 	 '800x600',
									 	 '960x600',
									 	 '1024x512',
									 	 '1024x768',
									 	 '1152x864',
                                     	 '1280x720',
                                     	 '1280x768',
                                     	 '1280x800',
									 	 '1280x960',
									 	 '1280x1024',
									 	 '1360x768',
									 	 '1600x900',
									 	 '1600x1024',
									 	 '1600x1200',
										 'Another')	NOT NULL,
					engine			enum('".implode("', '", array_keys($CONFIG['search_engine_name']))."') NULL,
					engine_str		varchar(255)	NOT NULL,
					module_id		tinyint(4)	NOT NULL default 0,
					sid    			bigint  NOT NULL default 0,
					utime  			bigint  NOT NULL default 0,
					zone            char(2),
					city            varchar(31)
					) ENGINE=MyISAM DEFAULT CHARSET=utf8");

    $db->query('INSERT INTO '.$server.$lang.'_counter_tables VALUES ("'.$date.'")');

    $db->query('DROP TABLE IF EXISTS '.$server.$lang.'_counter_all');

    if (isset($list_tables_2) && $list_tables_2 != '') {
    	$tables = $list_tables_2.', '.$server.$lang.'_counter_'.$date;
	} else {
    	$tables = $server.$lang.'_counter_'.$date;
	}
}
create_tbl_all($server,$lang);








function create_tbl_all($server,$lang){
	global $db,$CONFIG;
	$db->query("SHOW TABLES LIKE '{$server}{$lang}_counter_all'");if($db->nf()){return true;}
	$db->query("SHOW TABLES LIKE '{$server}{$lang}_counter\___\_____'");
	while($db->next_record()){$tbl[] = $db->f(0);}
	$db->query("CREATE TABLE {$server}{$lang}_counter_all (
					user_agent		enum('Another',
										 'IE 6.0',
										 'IE 5.5',
									 	 'IE 5.0',
									 	 'IE 4.0',
                                         'Opera 8.xx',
									 	 'Opera 7.xx',
										 'Opera 6.xx',
									 	 'Opera 5.xx',
									 	 'Netscape 7.x',
									 	 'Netscape 6.x',
									 	 'Netscape 4.x',
									 	 'Konqueror',
									 	 'Lynx',
									 	 'Galeon',
									 	 'Epiphany',
									 	 'Firefox',
									 	 'Mozilla')	NOT NULL,
					page_visit		varchar(255)	NOT NULL,
					ip_addr			bigint(20)  unsigned NOT NULL default 0,
					days			tinyint(3)	unsigned NOT NULL,
					mounths			tinyint(3)	unsigned NOT NULL,
					years			tinyint(3)	unsigned NOT NULL,
					the_time		tinyint(4)	NOT NULL,
					minuts			tinyint(4)	NOT NULL,
					os_user			enum('Windows 2003',
									 	 'Windows XP',
									 	 'Windows 2000',
									 	 'Windows NT',
									 	 'Windows 98',
									 	 'Windows 95',
									 	 'Linux',
									 	 'Unix',
									 	 'FreeBSD',
									 	 'IRIX',
									 	 'BEOS',
									 	 'Mac',
									 	 'OS/2',
									 	 'AIX',
									 	 'SunOS',
									 	 'Another')	NOT NULL,
					user_lang		char(2)		NOT NULL,
					visit_from		varchar(255)	NOT NULL,
					visit_from_site varchar(100)	NOT NULL,
					user_resolution	enum('640x480',
									 	 '800x600',
									 	 '960x600',
									 	 '1024x512',
									 	 '1024x768',
									 	 '1152x864',
                   	 '1280x720',
                   	 '1280x768',
                   	 '1280x800',
									 	 '1280x960',
									 	 '1280x1024',
									 	 '1360x768',
									 	 '1600x900',
									 	 '1600x1024',
									 	 '1600x1200',
									 	 'Another')	NOT NULL,
					engine			enum('".implode("', '", array_keys($CONFIG['search_engine_name']))."') NULL,
					engine_str		varchar(255)	NOT NULL,
					module_id		tinyint(4)	NOT NULL default 0,
					sid    			bigint  NOT NULL default 0,
					utime  			bigint  NOT NULL default 0,
					zone            char(2),
					city            varchar(31)
				) ENGINE=MERGE DEFAULT CHARSET=utf8 UNION=(".implode(',',$tbl).")");
}
?>