<?php // Vers 5.8.2 27.06.2012
/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
			  'counter_default_time'	=> array('type'		=> 'select',
												 'defval'	=>	array('0'	=> array(	'rus' => 'Час',
												 										'eng' => 'Hour'),
												 					  '1'	=> array(	'rus' => 'День',
												 										'eng' => 'Day'),
    									 							  '2'	=> array(	'rus' => 'Месяц',
												 										'eng' => 'Month'),
    									 							  '3'	=> array(	'rus' => 'Год',
												 										'eng' => 'Year'),
    									 							  '4'	=> array(	'rus' => 'Всё время',
												 										'eng' => 'All time'),
    									 							  ),
												 'descr'	=> array(	'rus' => 'Вывод статистики за время',
								 										'eng' => 'Display statistic at period'),
												 'access'	=> 'editable',
												 ),

			   'counter_max_rows'		=> array('type'		=> 'integer',
			   									 'defval'	=> '10',
			   									 'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу',
								 										'eng' => 'Maximum quantity of records on page'),
			   									 'access'	=> 'editable'
			   									 ),

			   'counter_max_countries' 	=> array('type'		=> 'integer',
			   									 'defval'	=> '10',
			   									 'descr'	=> array(	'rus' => 'Количество стран на диаграмме географии',
								 										'eng' => 'Country quantity on geo diagram'),
			   									 'access'	=> 'editable'
			   									 ),
			   );

$OPEN_NEW = array(
    'counter_file'				=> 'c.php',							// Файл счётчика
    'counter_alt'				=> '',								// Атрибут alt=""
    'counter_graph_type'		=> 'image/png', 					// Mime тип подложки
    'counter_font'              => RP.'mod/counter/ttf/ARIALN.TTF',                // Шрифт используеммый на графиках (папка /ttf/)
    'counter_banner' 			=> 'i/counter/counter-1.png',		// Подложка баннера
    'counter_banner_type'		=> 'image/png', 					// Mime тип подложки
    'counter_geo_city_file_path'    => RP.'mod/counter/.default/GeoIPCity.csv', // Путь к файлу с бызой соответствий Ip - городу
    'counter_variants_of_time'	=> array('rus' => array('0'	=> 'Час',			// Варианты возможных типов временных периодов,
                                      									'1'	=> 'День',
                                      									'2'	=> 'Месяц',
                                      									'3'	=> 'Год',
                                      									'4'	=> 'Всё время'),
                                      	 'eng' => array('0'	=> 'Hour',			// Варианты возможных типов временных периодов,
                                      									'1'	=> 'Day',
                                      									'2'	=> 'Month',
                                      									'3'	=> 'Year',
                                      									'4'	=> 'All time'),
    									                   ),
	'search_engine_name'		=> array('yan'	=> 'Яндекс',
    									 'goo'	=> 'Google',
    									 'ram'	=> 'Rambler',
    									 'apo'	=> 'Aport',
    									 'yah'	=> 'Yahoo',
    									 'msn'	=> 'MSN',
    									 'big'	=> 'bigmir)net',
    									 'sml'	=>  'Search.Mail.ru'
    									),
    'search_engine_url'			=> array('yan' => 'yandex.ru',
    									 'goo' => 'google.',
    									 'ram' => 'rambler.ru',
    									 'apo' => 'sm.aport.ru',
    									 'yah' => 'search.yahoo.com',
    									 'msn' => 'search.msn.com',
    									 'big' => 'search.bigmir.net',
    									 'sml' => 'search.mail.ru'
    									),
	'search_engine_var'			=> array('yan' => 'text',
    									 'goo' => 'q',
    									 'ram' => 'words',
    									 'apo' => 'r',
    									 'yah' => 'p',
    									 'msn' => 'q',
    									 'big' => 'q',
    									 'sml' => 'q'
    									),
	'search_engine_encoding'	=> array('yan' => 'windows-1251',
    									 'goo' => 'UTF-8',
    									 'ram' => 'windows-1251',
    									 'apo' => 'windows-1251',
    									 'yah' => 'UTF-8',
    									 'msn' => 'UTF-8',
    									 'big' => 'windows-1251',
    									 'sml' => 'windows-1251'
    									)
);

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'counter_default_time' => '2',
'counter_max_rows' => '50',
'counter_max_countries' => '7',
/* ABOCMS:END */
);
?>