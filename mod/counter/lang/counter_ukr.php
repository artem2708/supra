<?php
$_MSG = Array(
	"counter"					=>	"Статистика посещений",

	"Attendance"				=>	"Посещаемость",
	"Links"						=>	"Ссылки",
	"Sites"						=>	"Сайты",
	"Pages"						=>	"Страницы",
	"Visitors"					=>	"Посетители",
	"Geography"					=>	"География",
	"Search_engines"			=>	"Поисковые системы",

	"Controls"					=>	"Настройки",
	"Report_of"					=>	"Отчёт за",
	"from"						=>	"от",
	"No_data_of_this_period"	=>	"Нет данных за выбранный период.",
	"Attendance_content"		=>	"Посещаемость по контенту",
	"Module"					=>	"Модуль",
	"Visits"					=>	"Визиты",
	"Unique_visitors"			=>	"Уникальные посетители",
	"Module_ID"					=>	"ID модуля",
	"Current_day"				=>	"Текущий день",
	"Current_year"				=>	"Текущий год",
	"Last_hour"					=>	"Прошедший час",
	"Last_day"					=>	"Прошедший день",
	"Last_year"					=>	"Прошедший год",
	"Current_hour"				=>	"Текущий час",
	"Today"						=>	"Сегодня",
	"Yesterday"					=>	"Вчера",
	"Current_month"				=>	"Текущий месяц",
	"Last_month"				=>	"Прошедший месяц",
	"Total_time"				=>	"За всё время",
	"Total_visitors_num"		=>	"Всего посетителей",
	"Unique_visitors_num"		=>	"Уникальных посетителей",
	"By"						=>	"За",
	"Links_on_site"				=>	"Ссылки на сайт",
	"Link"						=>	"Ссылка",
	"Passage_num"				=>	"Переходов",
	"Linking_sites"				=>	"Ссылающиеся сайты",
	"Site"						=>	"Сайт",
	"Paths"						=>	"Пути",
	"Path"						=>	"Путь",
	"Q_ty"						=>	"Кол-во",
	"Entry_points"				=>	"Точки входа",
	"Page"						=>	"Страница",
	"Title"						=>	"Заголовок",
	"Entry_q_ty"				=>	"Кол-во входов",
	"Shows_q_ty"				=>	"Кол-во показов",
	"Last"						=>	"Последний",
	"Exit_points"				=>	"Точки выхода",
	"Exit_q_ty"					=>	"Кол-во выходов",
	"Single_loadings"			=>	"Единичные загрузки",
	"Time"						=>	"Время",
	"Site_review_depth"			=>	"Глубина просмотра сайта",
	"Review_depth"				=>	"Глубина просмотра",
	"Session_q_ty"				=>	"Кол-во сессий",
	"Last_entry"				=>	"Последний вход",
	"Last_exit"					=>	"Последний выход",
	"Attendance_statistics"		=>	"Статистика по посещаемости страниц",
	"Shows_num"					=>	"Просмотров",
	"Languages"					=>	"Языки",
	"Language"					=>	"Язык",
	"Q_ty"						=>	"Кол-во",
	"OS"						=>	"ОС",
	"Using_OS"					=>	"Используемые ОС",
	"Browsers"					=>	"Браузеры",
	"Browser"					=>	"Браузер",
	"Screen_sizes"				=>	"Разрешения экрана",
	"Address_IP"				=>	"IP адреса",
	"IP"						=>	"IP",
	"Geography_cities"			=>	"География посещений по городам",
	"City"						=>	"Город",
	"Geography_countries"		=>	"География посещений по странам",
	"Country"					=>	"Страна",
	"Passages"					=>	"Переходы",
	"Search_engine"				=>	"Поисковая система",
	"Request_string"			=>	"Строка запроса",
	"Download_report_in_CSV"	=>	"Выгрузить отчет в CSV формате",
	"Enter_period"				=>	"Укажите период",
	"Show"						=>	"Показать",
	"Reset"						=>	"Очистить",
	"Statistics"				=>	"Статистика",
	"by_countries"				=>	"по странам",
	"by_cities"					=>	"по городам",
	"All_visits_of"				=>	"Все визиты за",
	"Geo_visits_of"				=>	"География всех визитов за",
	"Unique_visits_of"			=>	"Уникальные визиты за",
	"Geo_unique_visits_of"		=>	"География уникальных визитов за",
	"Go_link"					=>	"Перейти по ссылке",
	"Pages"						=>	"Страницы",
	"Site_paths"				=>	"Пути по сайту",
	"No_data"					=>	"Нет данных",
	"Search_requests"			=>	"Поисковые запросы",
	"No_search_engine_passages"	=>	"Переходов с поисковых систем не зарегистрировано.",
	"Attendance_review"			=>	"Просмотр посещаемости",
	"by_content"				=>	"по контенту",
	"by_modules"				=>	"по модулям",
	"Select"					=>	"Выбрать",
	"Period"					=>	"Период",
	"All_visits_dynamics"		=>	"Динамика визитов всех посетителей",
	"All_visits_dynamics_by"	=>	"Динамика всех визитов за",
	"Unique_visits_dynamics"	=>	"Динамика визитов уникальных посетителей",
	"Unique_visits_dynamics_by"	=>	"Динамика уникальных визитов за",
	"from"						=>	"с",
	"to"						=>	"по",
	"Search_robots"				=>	"Поисковые роботы",
	"Search_engine_stat"		=>	"Статистика поисковой системы",
	"Fill_geoip"            	=>	"Наполнение базы GEO IP",
	"Select_csv_file"           =>	"Выберите csv файл с базой",
	"CSV_files_charset"         =>	"Кодировка csv файла",
	"CSV_files_fields_order"    =>	"Порядок следования полей в csv файле",
	"CSV_files_fields"          =>	"Начальный ip-адрес блока;Конечный ip-адрес блока;Зона(RU);id населенного пункта;Населенный пункт;Регион",
	"Fields_loaded"            	=>	"Загружено записей",
	"Data_not_loaded"           =>	"Данные не были загружены",
	"Load"                      =>	"Загрузить",
);
?>