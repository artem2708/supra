<?php
define('RP', realpath("../../").'/');

require_once('./config.php');
$counter_new = array_merge($NEW, $OPEN_NEW);

require_once(RP.'config.php');
$NEW	= array_merge($NEW, $counter_new);

$CONFIG = array_merge($CONFIG, $NEW);

require_once(RP.'inc/class.Main.php');

$db = new My_Sql();
$db->Database 	= $NEW['db_database'];
$db->Host 		= $NEW['db_host'];
$db->User 		= $NEW['db_user'];
$db->Password 	= $NEW['db_password'];

$site_config = Main::getSiteConfig();
$mult_lang   = @$site_config['multilanguage'] ? $site_config['multilanguage'] : $NEW['multilanguage'];
$def_lang    = @$site_config['language']      ? $site_config['language'] : $NEW['default_lang'];
$server      = @$site_config['pref']          ? $site_config['pref']."_" : Main::getServerName();

// ����������� �������� ������ ����� 
if ($mult_lang && $_REQUEST['lang']) {
	$lang	= My_Sql::escape($_REQUEST['lang']);	
} else {
	$lang	= $def_lang;	
} 
define('SITE_PREFIX', $server.$lang);

$graph			= new Graph($NEW, SITE_PREFIX, $_GET['type']);
$graph->getGraph();

/*
require_once('lang/ru_RU.php');
require_once('./config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/class.Main.php');
require_once('lib/class.Counter.php');
require_once('lib/class.AdminCounter.php');
*/
class Graph {

    var $color_0	= NULL;
    var $color_1	= NULL;
    var $color_2	= NULL;
    var $color_3	= NULL;
    var $color_4	= NULL;
    var $color_5	= NULL;
    var $color_6	= NULL;
    var $color_7	= NULL;

    var $graph		= NULL;
    var $lang		= NULL;
    var $type		= NULL;
    var $conf		= array();
	var $style		= array();
	var $tables		= array();
    var	$spad		= 50;												// ����������� ����� ������ �� �������

    var $value_for_hour			= array();
	var $value_for_day			= array();
	var $value_for_mounth		= array();
	var $value_for_year			= array();
	var $value_for_all_time		= array();

    var $old_value_for_hour		= array();
	var $old_value_for_day		= array();
	var $old_value_for_mounth	= array();
	var $old_value_for_year		= array();
	var $old_value_for_all_time	= array();

    var $step_x_for_hour		= NULL;
    var $step_x_for_day			= NULL;
    var $step_x_for_mounth		= NULL;
    var $step_x_for_year		= NULL;
    var $step_x_for_all_time	= NULL;

    var $step_y_for_hour		= NULL;
    var $step_y_for_day			= NULL;
    var $step_y_for_mounth		= NULL;
    var $step_y_for_year		= NULL;
    var $step_y_for_all_time	= NULL;

	function Graph($conf, $lang, $type) {
        global $db;
        $db->query('SELECT name FROM '.$lang.'_counter_tables');
        if ($db->nf() > 0) {
			for($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
				$this->tables[] = $db->f('name');
			}
        }

        $this->conf		= $conf;
        $this->lang		= $db->escape($lang);
        $this->type		= $type;
    	$this->graph	= ImageCreate(555, 445);
		$this->color_0	= ImageColorAllocate($this->graph, 255, 255, 255);
		$this->color_1	= ImageColorAllocate($this->graph, 0, 0, 0);
        $this->color_2	= ImageColorAllocate($this->graph, 153, 153, 153);
		$this->color_3	= ImageColorAllocate($this->graph, 162, 93, 221);
		$this->color_4	= ImageColorAllocate($this->graph, 220, 150, 255);
		$this->color_5	= ImageColorAllocate($this->graph, 16, 143, 224);
		$this->color_6	= ImageColorAllocate($this->graph, 0, 128, 0);
		$this->color_7	= ImageColorAllocate($this->graph, 0, 101, 182);

        $this->style = array($this->color_0, $this->color_0, $this->color_2);
        imagesetstyle($this->graph, $this->style);
	}

	function getGraphBackGround() {
   		imageline($this->graph, 35, 10, 35, 415, $this->color_1);
		imageline($this->graph, 35, 415, 535, 415, $this->color_1);
	}

    function getVectorX($time = NULL) {
        global $db;
		if (!isset($time) || $time == NULL) {
        	$time = $this->conf['counter_default_time'];
		}
       	switch ($time) {
			case 0:																			// ���
                $this->getVectorXForHour();
				break;

			case 1:																			// ����
                $this->getVectorXForDay();
				break;

			case 2:																			// �����
                $this->getVectorXForMounth();
				break;

			case 3:																			// ���
                $this->getVectorXForYear();
                break;

			case 4:																			// �� �����
        		$this->getVectorXForAllTime();
        	    break;
/*
			default:																		// ������
        		$this->getVectorXForPeriod($time);
*/        		
		}
	}

    function getVectorXForHour() {
		for($i = 0; $i < 12; $i++) {
            $x = (76+(41*$i));
        	imageline($this->graph, $x, 10, $x, 414, IMG_COLOR_STYLED);
            if ((($i+1)*5) < 10) {$minuts = '0'.(($i+1)*5);} else {$minuts = (($i+1)*5);}
        	ImageString($this->graph, 2, (73+(41*$i)), 420, $minuts, $this->color_2);
		}
        $this->step_x_for_hour = 491/$minuts;
		return TRUE;
	}

	function getVectorXForDay() {
		for ($i = 0; $i < 24; $i++) {
            $x = (55+(20*$i));
        	imageline($this->graph, $x, 10, $x, 414, IMG_COLOR_STYLED);
            if (($i+1) < 10) {$hours = '0'.($i+1);} else {$hours = ($i+1);}
            ImageString($this->graph, 2, (50+(20*$i)), 420, $hours, $this->color_2);
		}
		$this->step_x_for_day = ($x-16)/24;
		return TRUE;
	}

	function getVectorXForMounth() {
        $year	= date('Y');
        $mounth	= date('n');
		for ($i = 0; $i < 31; $i++) {
            $x = (51+(16*$i));
			if ($i > 27 && !checkdate($mounth, ($i+1), $year)) {break;}
            if (($i+1) < 10) {$days = '0'.($i+1);} else {$days = ($i+1);}
        	imageline($this->graph, $x, 10, $x, 414, IMG_COLOR_STYLED);
        	ImageString($this->graph, 2, (46+(16*$i)), 420, $days, $this->color_2);
		}
        $this->step_x_for_mounth = ($x-46)/$days;
		return TRUE;
	}

	function getVectorXForYear() {
   		for ($i = 0; $i < 12; $i++) {
            $x = (76+(41*$i));
        	imageline($this->graph, $x, 10, $x, 414, IMG_COLOR_STYLED);
            if (($i+1) < 10) {$mounths = '0'.($i+1);} else {$mounths = ($i+1);}
            ImageString($this->graph, 2, (72+(41*$i)), 420, $mounths, $this->color_2);
		}
		$this->step_x_for_year = ($x-76)/$mounths;
		return TRUE;
	}

	function getVectorXForAllTime() {
        global $db;$count= array();
		$db->query('SELECT DISTINCT years AS count FROM '.$this->lang.'_counter_all ORDER BY years');
		if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
			    $count[$i] = $db->f('count');
			}
		}
		if (1 == sizeof($count)) {
        	$this->getVectorXForYear();
            $this->step_x_for_all_time = $this->step_x_for_year;
		} elseif (1 < sizeof($count)) {
        	$step	= (int)(500/sizeof($count));
   			for ($i = 0; $i < sizeof($count); $i++) {
            	$x = (35+$step+($step*$i));
        		imageline($this->graph, (35+$step+($step*$i)), 10, (35+$step+($step*$i)), 414, IMG_COLOR_STYLED);
            	ImageString($this->graph, 2, (35+($step-13)+($step*$i)), 420, '200'.$count[$i], $this->color_2);
			}
            $this->step_x_for_all_time = ($x-35)/sizeof($count);
		}
		return FALSE;
	}
	
	function getVectorXForPeriod($time) {
        global $db;
		$db->query('SELECT DISTINCT years AS count FROM '.$this->lang.'_counter_all ORDER BY years');
		if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
			    $count[$i] = $db->f('count');
			}
		}
		if (1 == sizeof($count)) {
        	$this->getVectorXForYear();
            $this->step_x_for_all_time = $this->step_x_for_year;
		} elseif (1 < sizeof($count)) {
        	$step	= (int)(500/sizeof($count));
   			for ($i = 0; $i < sizeof($count); $i++) {
            	$x = (35+$step+($step*$i));
        		imageline($this->graph, (35+$step+($step*$i)), 10, (35+$step+($step*$i)), 414, IMG_COLOR_STYLED);
            	ImageString($this->graph, 2, (35+($step-13)+($step*$i)), 420, '200'.$count[$i], $this->color_2);
			}
            $this->step_x_for_all_time = ($x-35)/sizeof($count);
		}
		return FALSE;
	}

    function getVectorY($time = NULL) {
		if (!isset($time) || $time == NULL) {
        	$time = $this->conf['counter_default_time'];
		}
       	switch ($time) {
			case 0:																			// ���
                $this->getVectorYForHour();
				break;

			case 1:																			// ����
                $this->getVectorYForDay();
				break;

			case 2:																			// �����
                $this->getVectorYForMounth();
				break;

			case 3:																			// ���
                $this->getVectorYForYear();
                break;

			case 4:																			// �� �����
        		$this->getVectorYForAllTime();
        		break;
/*
			default:																			// �� �����
        		$this->getVectorYForPeriod($time);
*/	        		
		}	
	}

    function getVectorYForHour() {
        global $db;
		$hour	= date('G');
		$day	= date('j');
		$table	= date('m_Y');
        if (0 != $hour) {
            if ('people' == $this->type) {
				$db->query('SELECT minuts, COUNT(minuts) AS count
								FROM '.$this->lang.'_counter_'.$table.'
								WHERE the_time = '.($hour-1).' AND
							  		  days = '.$day.'
								GROUP BY minuts
								ORDER BY count DESC');
			} elseif ('uniq_people' == $this->type) {
				$db->query('SELECT minuts, COUNT(DISTINCT ip_addr) AS count
								FROM '.$this->lang.'_counter_'.$table.'
								WHERE the_time = '.($hour-1).' AND
							  		  days = '.$day.'
								GROUP BY minuts
								ORDER BY count DESC');
			}
        	if ($db->nf() > 0) {
            	for($i = 0; $i < $db->nf(); $i++) {
                	$db->next_record();
                	$this->old_value_for_hour[$i][] = $db->f('count');
                	$this->old_value_for_hour[$i][] = $db->f('minuts');
				}
        	}
		}
        if ('people' == $this->type) {
			$db->query('SELECT minuts, COUNT(minuts) AS count
							FROM '.$this->lang.'_counter_'.$table.'
							WHERE the_time = '.$hour.' AND
								  days = '.$day.'
							GROUP BY minuts
							ORDER BY count DESC');
		} elseif ('uniq_people' == $this->type) {
   			$db->query('SELECT minuts, COUNT(DISTINCT ip_addr) AS count
							FROM '.$this->lang.'_counter_'.$table.'
							WHERE the_time = '.$hour.' AND
								  days = '.$day.'
							GROUP BY minuts
							ORDER BY count DESC');
		}
        if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $this->value_for_hour[$i][] = $db->f('count');
                $this->value_for_hour[$i][] = $db->f('minuts');
			}
        }
		if (@$this->value_for_hour[0][0] > @$this->old_value_for_hour[0][0]) {
        	$count = $this->getNextIntValue(@$this->value_for_hour[0][0]);
		} else {
        	$count = $this->getNextIntValue(@$this->old_value_for_hour[0][0]);
		}
        @$this->step_y_for_hour = 400/$count;
		for($i = 0; $i < 10; $i++) {
        	imageline($this->graph, 36, (375-(40*$i)), 535, (375-(40*$i)), IMG_COLOR_STYLED);
        	ImageString($this->graph, 2, 8, (368-(40*$i)), (($count/10)+($i*($count/10))), $this->color_2);
		}
		return TRUE;
	}

	function getVectorYForDay() {
        global $db;
		$day	= date('j');
		$table	= date('m_Y');
        if (1 != $day) {
        	if ('people' == $this->type) {
				$db->query('SELECT the_time, COUNT(the_time) AS count
								FROM '.$this->lang.'_counter_'.$table.'
								WHERE days = '.($day-1).'
								GROUP BY the_time
								ORDER BY count DESC');
			} elseif ('uniq_people' == $this->type) {
   				$db->query('SELECT the_time, COUNT(DISTINCT ip_addr) AS count
								FROM '.$this->lang.'_counter_'.$table.'
								WHERE days = '.($day-1).'
								GROUP BY the_time
								ORDER BY count DESC');
			}
        	if ($db->nf() > 0) {
            	for($i = 0; $i < $db->nf(); $i++) {
                	$db->next_record();
                	$this->old_value_for_day[$i][] = $db->f('count');
                	$this->old_value_for_day[$i][] = $db->f('the_time');
				}
        	}
		}
        if ('people' == $this->type) {
			$db->query('SELECT the_time, COUNT(the_time) AS count
							FROM '.$this->lang.'_counter_'.$table.'
							WHERE days = '.$day.'
							GROUP BY the_time
							ORDER BY count DESC');
		} elseif ('uniq_people' == $this->type) {
   			$db->query('SELECT the_time, COUNT(DISTINCT ip_addr) AS count
							FROM '.$this->lang.'_counter_'.$table.'
							WHERE days = '.$day.'
							GROUP BY the_time
							ORDER BY count DESC');
		}
        if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $this->value_for_day[$i][] = $db->f('count');
                $this->value_for_day[$i][] = $db->f('the_time');
			}
        }
		if (@$this->value_for_day[0][0] > @$this->old_value_for_day[0][0]) {
        	$count = $this->getNextIntValue(@$this->value_for_day[0][0]);
		} else {
        	$count = $this->getNextIntValue(@$this->old_value_for_day[0][0]);
		}
        @$this->step_y_for_day = 400/$count;
        $y = 20;
		for($i = 0; $i < $y; $i++) {
        	imageline($this->graph, 36, (395-($y*$i)), 535, (395-($y*$i)), IMG_COLOR_STYLED);
            $val = (round((((($count/$y)+($i*($count/$y))))*10))/10);
        	ImageString($this->graph, 2, 3, (390-($y*$i)), $val, $this->color_2);
		}
		return TRUE;
	}

	function getVectorYForMounth() {
        global $db;
		$table		= date('m_Y');
        if (10 >= date('m')) {
        	$table_2	= '0'.(date('m')-1).'_'.date('Y');
        } else {
        	$table_2	= (date('m')-1).'_'.date('Y');
		}
        if (in_array(($table_2), $this->tables)) {
            if ('people' == $this->type) {
				$db->query('SELECT days, COUNT(days) AS count
								FROM '.$this->lang.'_counter_'.$table_2.'
								GROUP BY days
								ORDER BY count DESC');
			} elseif ('uniq_people' == $this->type) {
            	$db->query('SELECT days, COUNT(DISTINCT ip_addr) AS count
								FROM '.$this->lang.'_counter_'.$table_2.'
								GROUP BY days
								ORDER BY count DESC');
			}
        	if ($db->nf() > 0) {
            	for($i = 0; $i < $db->nf(); $i++) {
                	$db->next_record();
                	$this->old_value_for_mounth[$i][] = $db->f('count');
                	$this->old_value_for_mounth[$i][] = $db->f('days');
				}
        	}
		}
        if ('people' == $this->type) {
			$db->query('SELECT days, COUNT(days) AS count
							FROM '.$this->lang.'_counter_'.date('m_Y').'
							GROUP BY days
							ORDER BY count DESC');
		} elseif ('uniq_people' == $this->type) {
   			$db->query('SELECT days, COUNT(DISTINCT ip_addr) AS count
							FROM '.$this->lang.'_counter_'.date('m_Y').'
							GROUP BY days
							ORDER BY count DESC');
		}
        if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $this->value_for_mounth[$i][] = $db->f('count');
                $this->value_for_mounth[$i][] = $db->f('days');
			}
       }
	   if (@$this->value_for_mounth[0][0] > @$this->old_value_for_mounth[0][0]) {
        	$count = $this->getNextIntValue(@$this->value_for_mounth[0][0]);
	   } else {
        	$count = $this->getNextIntValue(@$this->old_value_for_mounth[0][0]);
	   }
       @$this->step_y_for_mounth = 400/$count;
	   for($i = 0; $i < 20; $i++) {
        	imageline($this->graph, 36, (395-(20*$i)), 535, (395-(20*$i)), IMG_COLOR_STYLED);
            $val = (round((((($count/20)+($i*($count/20))))*10))/10);
            ImageString($this->graph, 2, 3, (390-(20*$i)), $val, $this->color_2);
		}
		return TRUE;
	}

	function getVectorYForYear() {
        global $db;
        if (1 == 1) {
        	if ('people' == $this->type) {
				$db->query('SELECT mounths, COUNT(mounths) AS count
								FROM '.$this->lang.'_counter_all
                            	WHERE years = '.(date('y')-1).'
								GROUP BY mounths
								ORDER BY count DESC');
			} elseif ('uniq_people' == $this->type) {
   				$db->query('SELECT mounths, COUNT(DISTINCT ip_addr) AS count
								FROM '.$this->lang.'_counter_all
                            	WHERE years = '.(date('y')-1).'
								GROUP BY mounths
								ORDER BY count DESC');
			}
        	if ($db->nf() > 0) {
            	for($i = 0; $i < $db->nf(); $i++) {
                	$db->next_record();
                	$this->old_value_for_year[$i][] = $db->f('count');
                	$this->old_value_for_year[$i][] = $db->f('mounths');
				}
        	}
		}
        if ('people' == $this->type) {
			$db->query('SELECT mounths, COUNT(mounths) AS count
							FROM '.$this->lang.'_counter_all
							WHERE years = '.date('y').'
                        	GROUP BY mounths
                        	ORDER BY count DESC');
		} else if ('uniq_people' == $this->type) {
   			$db->query('SELECT mounths, COUNT(DISTINCT ip_addr) AS count
							FROM '.$this->lang.'_counter_all
							WHERE years = '.date('y').'
                        	GROUP BY mounths
                        	ORDER BY count DESC');
		}
        if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $this->value_for_year[$i][] = $db->f('count');
                $this->value_for_year[$i][] = $db->f('mounths');
			}
        }
		if (@$this->value_for_year[0][0] > @$this->old_value_for_year[0][0]) {
        	$count = $this->getNextIntValue(@$this->value_for_year[0][0]);
		} else {
        	$count = $this->getNextIntValue(@$this->old_value_for_year[0][0]);
		}
       	@$this->step_y_for_year = 400/$count;
		for($i = 0; $i < 10; $i++) {
       		imageline($this->graph, 36, (375-(40*$i)), 535, (375-(40*$i)), IMG_COLOR_STYLED);
        	$val = (round((((($count/10)+($i*($count/10))))*10))/10);
            ImageString($this->graph, 1, 3, (368-(40*$i)), $val, $this->color_2);
		}
		return TRUE;
	}

	function getVectorYForAllTime() {
        global $db;
        if ('people' == $this->type) {
			$db->query('SELECT years, COUNT(years) AS count
							FROM '.$this->lang.'_counter_all
							GROUP BY years
							ORDER BY count DESC');
		} elseif ('uniq_people' == $this->type) {
   			$db->query('SELECT years, COUNT(DISTINCT years) AS count
							FROM '.$this->lang.'_counter_all
							GROUP BY years
							ORDER BY count DESC');
		}
		if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $this->value_for_all_time[$i][] = $db->f('count');
                $this->value_for_all_time[$i][] = $db->f('years');
			}
		}
       	$count = $this->getNextIntValue(@$this->value_for_all_time[0][0]);
       	@$this->step_y_for_all_time = 400/$count;
		for($i = 0; $i < 10; $i++) {
       		imageline($this->graph, 36, (375-(40*$i)), 535, (375-(40*$i)), IMG_COLOR_STYLED);
           	$val = (round((((($count/10)+($i*($count/10))))*10))/10);
           	ImageString($this->graph, 1, 3, (368-(40*$i)), $val, $this->color_2);
		}
		return TRUE;
	}

    function getNextIntValue($count, $int = 0) {
    	$dy = 2;
        if ($count > 0) {
			if ($count > $dy) {
                if (0 == $int) {
					$int = $dy;
                } else {
					$int = ($int*$dy);
                }
                if ($count <= $int) {
					return $int;
                } else {
					return $this->getNextIntValue($count, $int);
				}
			} else {
            	return $dy;
			}
        }
    	return FALSE;
    }

    function getCurrentCurve($time) {
   		if (!isset($time) || $time == NULL) {
        	$time = $this->conf['counter_default_time'];
		}
       	switch ($time) {
			case 0:																			// ���
                $this->getCurrentHourCurve();
				break;

			case 1:																			// ����
                $this->getCurrentDayCurve();
				break;

			case 2:																			// �����
                $this->getCurrentMounthCurve();
				break;

			case 3:																			// ���
                $this->getCurrentYearCurve();
                break;

			case 4:																			// �� �����
        		$this->getCurrentAllTimeCurve();
		}
		return FALSE;
    }

    function getCurrentHourCurve() {
        global $db;
        if (@is_array($this->value_for_hour[0])) {
			for($i = 0; $i < sizeof($this->value_for_hour); $i++) {
                $arr[$this->value_for_hour[$i][1]] = $this->value_for_hour[$i][0];
			}
            ksort($arr);
			$count	 = sizeof($arr);
            $count_2 = 0;
        	$x_1 = 36;
            $x_2 = 36;
            $y_1 = 414;
            $y_2 = 414;
            for ($i = 0; $i < 60; $i++) {
                if (array_key_exists($i, $arr)) {
                    $count_2++;
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_hour*$i));
                    $y_1 = $y_2;
					$y_2 = 414 - $this->step_y_for_hour*$arr[$i];
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_4);
				} else {
                    if ($count_2 >= $count) {
						break;
                    }
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_hour*$i));
                    $y_1 = $y_2;
                    if (($y_2 + $this->step_y_for_hour*$this->spad) <= 414) {
                    	$y_2 = $y_2 + $this->step_y_for_hour*$this->spad;
					} else {
						$y_2 = 414;
					}
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_4);
				}
            }
        	return TRUE;
        }
    	return FALSE;
	}

   	function getCurrentDayCurve() {
        global $db;
        if (@is_array($this->value_for_day[0])) {
			for($i = 0; $i < sizeof($this->value_for_day); $i++) {
                $arr[$this->value_for_day[$i][1]] = $this->value_for_day[$i][0];
			}
            ksort($arr);
			$count	 = sizeof($arr);
            $count_2 = 0;
        	$x_1 = 36;
            $x_2 = 36;
            $y_1 = 414;
            $y_2 = 414;
            for ($i = 0; $i < 24; $i++) {
                if (array_key_exists($i, $arr)) {
                    $count_2++;
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_day*$i));
                    $y_1 = $y_2;
					$y_2 = 414 - $this->step_y_for_day*$arr[$i];
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_4);
				} else {
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_day*$i));
                    $y_1 = $y_2;
                    if (($y_2 + $this->step_y_for_day*$this->spad) <= 414) {
						$y_2 = $y_2 + $this->step_y_for_day*$this->spad;
					} else {
						$y_2 = 414;
					}
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_4);
				}
            }
        	return TRUE;
        }
    	return FALSE;
	}

   	function getCurrentMounthCurve() {
    	if (@is_array($this->value_for_mounth[0])) {
			for($i = 0; $i < sizeof($this->value_for_mounth); $i++) {
                $arr[$this->value_for_mounth[$i][1]] = $this->value_for_mounth[$i][0];
			}
            ksort($arr);
			$count	 = sizeof($arr);
            $count_2 = 0;
        	$x_1 = 36;
            $x_2 = 36;
            $y_1 = 414;
            $y_2 = 414;
            for ($i = 0; $i < 60; $i++) {
                if (array_key_exists($i, $arr)) {
                    $count_2++;
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_mounth*$i));
                    $y_1 = $y_2;
					$y_2 = 414 - $this->step_y_for_mounth*$arr[$i];
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_4);
				} else {
                    if ($count_2 >= $count) {
						break;
                    }
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_mounth*$i));
                    $y_1 = $y_2;
                    if (($y_2 + $this->step_y_for_mounth*$this->spad) <= 414) {
                    	$y_2 = $y_2 + $this->step_y_for_mounth*$this->spad;
					} else {
						$y_2 = 414;
					}
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_4);
				}
            }
        	return TRUE;
        }
		return FALSE;
	}

   	function getCurrentYearCurve() {
       	if (@is_array($this->value_for_year[0])) {
			for($i = 0; $i < sizeof($this->value_for_year); $i++) {
                $arr[$this->value_for_year[$i][1]] = $this->value_for_year[$i][0];
			}
            ksort($arr);
			$count	 = sizeof($arr);
            $count_2 = 0;
        	$x_1 = 36;
            $x_2 = 36;
            $y_1 = 414;
            $y_2 = 414;
            for ($i = 0; $i < 60; $i++) {
                if (array_key_exists($i, $arr)) {
                    $count_2++;
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_year*$i));
                    $y_1 = $y_2;
					$y_2 = 414 - $this->step_y_for_year*$arr[$i];
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_4);
				} else {
                    if ($count_2 >= $count) {
						break;
                    }
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_year*$i));
                    $y_1 = $y_2;
                    if (($y_2 + $this->step_y_for_year*$this->spad) <= 414) {
                    	$y_2 = $y_2 + $this->step_y_for_year*$this->spad;
					} else {
						$y_2 = 414;
					}
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_4);
				}
            }
        	return TRUE;
        }
    	return FALSE;
	}

   	function getCurrentAllTimeCurve() {
		if (@is_array($this->value_for_all_time[0])) {
			for($i = 0; $i < sizeof($this->value_for_all_time); $i++) {
                $arr[$this->value_for_all_time[$i][1]] = $this->value_for_all_time[$i][0];
			}
            ksort($arr);
            $i	 = 1;
        	$x_1 = 36;
            $x_2 = 36;
            $y_1 = 414;
            $y_2 = 414;
			foreach ($arr AS $k => $v) {
				    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_all_time*$i));
                    $y_1 = $y_2;
					$y_2 = 414 - $this->step_y_for_all_time*$v;
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_4);
            		$i++;
			}
        	return TRUE;
        }
		return FALSE;
	}

    function getOldCurve($time) {
   		if (!isset($time) || $time == NULL) {
        	$time = $this->conf['counter_default_time'];
		}
       	switch ($time) {
			case 0:																			// ���
                $this->getOldHourCurve();
				break;

			case 1:																			// ����
                $this->getOldDayCurve();
				break;

			case 2:																			// �����
                $this->getOldMounthCurve();
				break;

			case 3:																			// ���
                $this->getOldYearCurve();
                break;
		}
		return FALSE;
    }

    function getOldHourCurve() {
        global $db;
        if (@is_array($this->old_value_for_hour[0])) {
			for($i = 0; $i < sizeof($this->old_value_for_hour); $i++) {
                $arr[$this->old_value_for_hour[$i][1]] = $this->old_value_for_hour[$i][0];
			}
            ksort($arr);
			$count	 = sizeof($arr);
            $count_2 = 0;
        	$x_1 = 36;
            $x_2 = 36;
            $y_1 = 414;
            $y_2 = 414;
            for ($i = 0; $i < 60; $i++) {
                if (array_key_exists($i, $arr)) {
                    $count_2++;
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_hour*$i));
                    $y_1 = $y_2;
					$y_2 = 414 - $this->step_y_for_hour*$arr[$i];
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_5);
				} else {
                    if ($count_2 >= $count) {
						break;
                    }
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_hour*$i));
                    $y_1 = $y_2;
                    if (($y_2 + $this->step_y_for_hour*$this->spad) <= 414) {
                    	$y_2 = $y_2 + $this->step_y_for_hour*$this->spad;
					} else {
						$y_2 = 414;
					}
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_5);
				}
            }
        	return TRUE;
        }
		return FALSE;
	}

   	function getOldDayCurve() {
		if (@is_array($this->old_value_for_day[0])) {
			for($i = 0; $i < sizeof($this->old_value_for_day); $i++) {
                $arr[$this->old_value_for_day[$i][1]] = $this->old_value_for_day[$i][0];
			}
            ksort($arr);
			$count	 = sizeof($arr);
            $count_2 = 0;
        	$x_1 = 36;
            $x_2 = 36;
            $y_1 = 414;
            $y_2 = 414;
            for ($i = 0; $i < 60; $i++) {
                if (array_key_exists($i, $arr)) {
                    $count_2++;
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_day*$i));
                    $y_1 = $y_2;
					$y_2 = 414 - $this->step_y_for_day*$arr[$i];
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_5);
				} else {
                    if ($count_2 >= $count) {
						break;
                    }
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_day*$i));
                    $y_1 = $y_2;
                    if (($y_2 + $this->step_y_for_day*$this->spad) <= 414) {
                    	$y_2 = $y_2 + $this->step_y_for_day*$this->spad;
					} else {
						$y_2 = 414;
					}
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_5);
				}
            }
        	return TRUE;
        }
		return FALSE;
	}

   	function getOldMounthCurve() {
    	if (@is_array($this->old_value_for_mounth[0])) {
			for($i = 0; $i < sizeof($this->old_value_for_mounth); $i++) {
                $arr[$this->old_value_for_mounth[$i][1]] = $this->old_value_for_mounth[$i][0];
			}
            ksort($arr);
			$count	 = sizeof($arr);
            $count_2 = 0;
        	$x_1 = 36;
            $x_2 = 36;
            $y_1 = 414;
            $y_2 = 414;
            for ($i = 0; $i < 60; $i++) {
                if (array_key_exists($i, $arr)) {
                    $count_2++;
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_mounth*$i));
                    $y_1 = $y_2;
					$y_2 = 414 - $this->step_y_for_mounth*$arr[$i];
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_5);
				} else {
                    if ($count_2 >= $count) {
						break;
                    }
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_mounth*$i));
                    $y_1 = $y_2;
                    if (($y_2 + $this->step_y_for_mounth*$this->spad) <= 414) {
                    	$y_2 = $y_2 + $this->step_y_for_mounth*$this->spad;
					} else {
						$y_2 = 414;
					}
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_5);
				}
            }
        	return TRUE;
        }
    	return FALSE;
	}

   	function getOldYearCurve() {
         if (@is_array($this->old_value_for_year[0])) {
			for($i = 0; $i < sizeof($this->old_value_for_year); $i++) {
                $arr[$this->old_value_for_year[$i][1]] = $this->old_value_for_year[$i][0];
			}
            ksort($arr);
			$count	 = sizeof($arr);
            $count_2 = 0;
        	$x_1 = 36;
            $x_2 = 36;
            $y_1 = 414;
            $y_2 = 414;
            for ($i = 0; $i < 60; $i++) {
                if (array_key_exists($i, $arr)) {
                    $count_2++;
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_year*$i));
                    $y_1 = $y_2;
					$y_2 = 414 - $this->step_y_for_year*$arr[$i];
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_5);
				} else {
                    if ($count_2 >= $count) {
						break;
                    }
                    $x_1 = $x_2;
					$x_2 = (36+($this->step_x_for_year*$i));
                    $y_1 = $y_2;
                    if (($y_2 + $this->step_y_for_year*$this->spad) <= 414) {
                    	$y_2 = $y_2 + $this->step_y_for_year*$this->spad;
					} else {
						$y_2 = 414;
					}
               		imageline($this->graph, $x_1, $y_1, $x_2, $y_2, $this->color_5);
				}
            }
        	return TRUE;
        }
    	return FALSE;
	}

	function sendImageGraph() {
    	Header('Content-type: image/png');
		ImagePng($this->graph);
		ImageDestroy($this->graph);
	}

	function getGraph() {
		$this->getGraphBackGround();
		$this->getVectorX($_GET['time_type']);
        $this->getVectorY($_GET['time_type']);
        $this->getOldCurve($_GET['time_type']);
        $this->getCurrentCurve($_GET['time_type']);
		$this->sendImageGraph();
	}
}
?>