<?php
include $_SERVER["DOCUMENT_ROOT"] . "/common.php";
include $_SERVER["DOCUMENT_ROOT"] . "/mod/faq/config.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/captcha/lib.image.php";

$str = $CONFIG["faq_captcha_symbols"];

for($i = 0; $i < $CONFIG["faq_captcha_wordlength"]; $i++)
{
	$code[] = substr($str, rand(0, strlen($str) - 1), 1);
}

$_SESSION[$CONFIG["faq_captcha_secretword"]] = $text = implode("", $code);

$image_width = $CONFIG["faq_captcha_width"];
$image_height = $CONFIG["faq_captcha_height"];
$font_uri = $CONFIG["faq_captcha_fonts"];
$font_size = $CONFIG["faq_captcha_fontsize"];
$font_depth = $CONFIG["faq_captcha_fontdepth"];
$bgcolor = $CONFIG["faq_captcha_bgcolor"];
$color = $CONFIG["faq_captcha_color"];
$color2 = $CONFIG["faq_captcha_color2"];

$img = new CryptPng($image_width, $image_height);

if($img->create())
{
	$img->apply(new GradientEffect($bgcolor[0], $bgcolor[1], $bgcolor[2]));
	$img->apply(new GridEffect(2, $bgcolor[0], $bgcolor[1], $bgcolor[2]));
	$img->apply(new DotEffect());
	$t = new TextEffect($text, $font_size, $font_depth, $color, $color2);
	foreach($font_uri as $k => $v)
	{
		$t->addFont($v);
	}
	$img->apply($t);
	$img->apply(new DotEffect());
	$img->render();
}
?>

