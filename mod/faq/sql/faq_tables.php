<?php

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_faq` (
  `id` int(7) NOT NULL auto_increment,
  `category_id` smallint(5) unsigned NOT NULL default '0',
  `username` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `city` varchar(255) default NULL,
  `question` text,
  `date` datetime default NULL,
  `answer` longtext,
  `hits` bigint(20) unsigned NOT NULL default '0',
  `active` tinyint(1) NOT NULL default '0',
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_faq_categories` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `mail` varchar(50) default NULL,
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `express` tinyint(1) NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

?>