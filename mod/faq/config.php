<?php // Vers 5.8.3 24.07.2012
/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
			  'faq_order_by'		=> array('type'		=> 'select',
		   									 'defval'	=> array('id DESC'	=> array(	'rus' => 'В порядке обратном добавлению',
												 										'eng' => 'Reverce adding order'),
		   									 					 'id'		=> array(	'rus' => 'В порядке добавлению',
												 										'eng' => 'Adding order'),
		   									 					 ),
		   									 'descr'	=> array(	'rus' => 'Порядок вывода записей',
						 											'eng' => 'Order of record displaying'),
											 'access'	=> 'editable',
											 ),

			   'faq_max_rows'		=> array('type'		=> 'integer',
			   								 'defval'	=> '20',
			   								 'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу',
											 						'eng' => 'Maximum quantity of records on page'),
			   								 'access'	=> 'editable'
			   								 ),

			   'faq_date_format' 	=> array('type'		=> 'select',
		   									 'defval'	=> array('%d.%m.%Y %H:%i'	=> array(	'rus' => 'День.Месяц.Год Часы:Минуты',
											 										  			'eng' => 'Day.Month.Year Hours:Minutes'),
		   									 					 '%d.%m.%Y'			=> array(	'rus' => 'День.Месяц.Год',
											 													'eng' => 'Day.Month.Year'),
		   									 					 '%Y.%m.%d'			=> array(	'rus' => 'Год.Месяц.День',
											 													'eng' => 'Year.Month.Day'),
		   									 					 '%Y.%m.%d %H:%i'	=> array(	'rus' => 'Год.Месяц.День Часы:Минуты',
											 													'eng' => 'Year.Month.Day Hours:Minutes'),
		   									 					 ),
											 'descr'	=> array(	'rus' => 'Формат выводимой даты',
											 						'eng' => 'Format of deduced date'),
			   								 'access'	=> 'editable'
			   								 ),

			   'faq_captcha'		=> array('type'		=> 'checkbox',
			   								 'defval'	=> 'off',
			   								 'descr'	=> array(	'rus' => 'Добавить CAPTCHA-проверку',
											 						'eng' => 'Add CAPTCHA-verification'),
			   								 'access'	=> 'editable'
			   								 ),
               'faq_mail'		=> array('type'		=> 'string',
			   								 'defval'	=> '',
			   								 'descr'	=> array(	'rus' => 'E-mail для сообщения о добавлении вопроса',
											 						'eng' => 'E-mail for notifications on addition of a question'),
			   								 'access'	=> 'editable'
			   								 ),
			   'faq_count_hits' 	=> array('type'		=> 'checkbox',
		   									 'defval'	=> 'on',
		   									 'descr'	=> array(	'rus' => 'Учитывать статистику отображения контента',
											 						'eng' => 'To consider statistics of display of a content'),
		   									 'access'	=> 'editable'
		   									 ),
			   'faq_default_rights'	=> array('type'		=> 'string',
			   									 'defval'	=> '111111110001',
			   									 'descr'	=> array(	'rus' => 'Права для вопроса по-умолчанию (формат <img src="/i/admin/rights_format.gif" width=72 height=18 alt="формат прав">, где x - 0 или 1, каждая группа представляет набор прав: удаление, публикация, редактирование, чтение)',
 												 						'eng' => 'Faq default rights (format <img src="/i/admin/rights_format.gif" width=72 height=18 alt="rights format">, where x - 0 or 1, every group is rights set: delete, publication, edit, read)'),
			   									 'access'	=> 'editable'
			   									 ),
			   );

$CONFIG["faq_captcha_width"] = 110;
$CONFIG["faq_captcha_height"] = 32;
$CONFIG["faq_captcha_secretword"] = "FAQABOCMSwORd";
$CONFIG["faq_captcha_fontsize"] = 16;
$CONFIG["faq_captcha_fontdepth"] = 2;
$CONFIG["faq_captcha_symbols"] = "0123456789";
$CONFIG["faq_captcha_wordlength"] = 3;
$CONFIG["faq_captcha_fonts"] = array(
	$_SERVER["DOCUMENT_ROOT"] . "/inc/captcha/arialbd.ttf",
	$_SERVER["DOCUMENT_ROOT"] . "/inc/captcha/comicbd.ttf",
	$_SERVER["DOCUMENT_ROOT"] . "/inc/captcha/courbd.ttf",
	$_SERVER["DOCUMENT_ROOT"] . "/inc/captcha/timesbd.ttf",
);
$CONFIG["faq_captcha_bgcolor"] = array(98, 118, 144);
$CONFIG["faq_captcha_color"] = array(rand(190, 210), rand(200, 220), rand(30, 50));
$CONFIG["faq_captcha_color2"] = array(rand(150, 170), rand(50, 70), rand(140, 160));

$OPEN_NEW = array();

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'faq_order_by' => 'id DESC',
'faq_max_rows' => '10',
'faq_date_format' => '%d.%m.%Y',
'faq_captcha' => 'on',
'faq_mail' => 'olgaconst@tut.by',
'faq_count_hits' => 'on',
'faq_default_rights' => '111111110001',
/* ABOCMS:END */
);
?>