<?php //test new version

require_once(RP.'mod/faq/lib/class.faqDB.php');

class FaqPrototype extends Module {

	var $main_title;
	var $table_prefix;
	var $addition_to_path;
	var $module_name				= 'faq';
	var $admin_default_action		= 'showctg';
	var $tpl_path					= 'mod/faq/tpl/';
	var $block_module_actions		= array();
	var $block_main_module_actions	= array();

	// действие, выполняемое модулем по умолчанию
	var $default_action = "list";

	// Функция-конструктор
	// в зависимости от переданного $action выполняет различные действия
	function FaqPrototype($action = '', $transurl = '', $properties = array(), $prefix = NULL, $rewrite_mod_params = NULL, $adminurl = NULL, $noadmin = false) {
		global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang, $permissions, $request_type, $_RESULT;

		/* Get $action, $tpl_name, $permissions */
		extract( $this->init(array(
		        'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
		        'action' => $action, 'tpl_name' => $properties['tpl']
		)), EXTR_OVERWRITE);

		$this->block_module_actions	= array(
		        'list'	=> $this->_msg["Show_faq_list"],
		        'express'	=> $this->_msg["Show_faq_express"],
		        //										  'show'	=> $this->_msg["Show_faq"],
		);
		$this->block_main_module_actions = array(
		        'list'	=> $this->_msg["Show_faq_list"],
		        'express'	=> $this->_msg["Show_faq_express"],
		        //		                  'show'	=> $this->_msg["Show_faq"],
		);

		// выставим свойства, которые пользователь имеет возможность задать для блока с данным модулем
		// индекс свойства в массиве $properties соответствует номеру поля в БД
		// например, первый элемент соотв. полю property1 в БД
		// показывать категорию
		$category = intval($properties[1]);
		// показывать кол-во вопросов на одной странице
		$rows	= intval($properties[2]);
		if (!$rows) $rows = $CONFIG["faq_max_rows"];

		if ('only_create_object' == $action) {
		    return;
		}

		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}

		if (!self::is_admin()) {
//---------------------------------- обработка действий с сайта --------------------------------//
			switch ($action) {
// Вывод списка вопросов-ответов
			case "list":
			global $request_start, $request_category;
				$start		= ($request_start) ? $request_start : 1;
				$category	= ($request_category) ? $request_category : $category;
				$main->include_main_blocks($this->module_name."_list.html", "main");
				$tpl->assignInclude("post_faq_form", $tpl_path.$this->module_name."_post_form.html");
				$tpl->prepare();
				if($CONFIG["faq_captcha"] == "on")
				{
					$tpl->newBlock("block_captcha");
					$tpl->assign(
						array(
							'captcha_image_url'		=>	"/mod/faq/captcha/image.php?rand=" . rand(0, 100000),
							'captcha_maxlength'		=>	$CONFIG["faq_captcha_wordlength"],
							'captcha_rand'		=>	rand(1, 9999),
						)
					);
				}

				$pages_cnt = $this->show_questions('block_questions', $category, $start, $rows);
				$tpl->gotoBlock("_ROOT");
				$main->_show_nav_block($pages_cnt, null, "action=".$action."&category=".$category, $start);
				break;

// Вывод экспресс ответа
			case 'express':
				global $lang, $db;

				$main->include_main_blocks($this->module_name.'_express.html', 'main');
				$tpl->prepare();

				$tpl->newBlock("block_express");
				$tpl->assign(array(
				    'action' => $baseurl.'action=add&nocache',
				 ));
				$query = 'SELECT * FROM '.$this->table_prefix.'_faq_categories WHERE express="1"';
				$db->query($query);
				$count = $db->nf();
				for ($i=0;$i<$count;$i++) {
				    $db->next_record();
				    $value[$i] = $db->f('id');
				    $title[$i] = $db->f('title');
                }
                for ($i=0;$i<$count;$i++) {
                    $tpl->newBlock("block_questions");
                    $tpl->assign(array(
                        'value' => $value[$i],
                        'title' => $title[$i],
                    ));
                }
                if($CONFIG["faq_captcha"] == "on") {
					$tpl->newBlock("block_captcha");
					$tpl->assign(array(
					    'captcha_image_url' => "/mod/faq/captcha/image.php?rand=" . rand(0, 100000),
					    'captcha_maxlength' => $CONFIG["faq_captcha_wordlength"],
						'captcha_rand'		=>	rand(1, 9999),
					));
				}
				$this->addition_to_path = '';
				$this->main_title	= $PAGE['title'];
				break;

// Вывод вопроса и ответа к нему по id
			case 'show':
				global $request_id, $request_category, $PAGE, $lang;
				$id			= (int)$request_id;
				$category	= ($request_category) ? $request_category : $category;
				$main->include_main_blocks($this->module_name.'_question.html', 'main');
				$tpl->prepare();
				$this->show_question($id);
				if ($CONFIG['faq_count_hits']) {
					Main::addHits($this->table_prefix."_faq", $id);
				}
				$this->addition_to_path = '';
				$this->main_title	= $PAGE['title'];
				break;


// Добавление вопроса в базу данных
			case "add":
				global $request_username, $request_email, $request_captcha, $request_city, $request_question, $request_category;
				$category = $request_category ? intval($request_category) : $category;

				if($CONFIG["faq_captcha"] == "on" && (!$request_captcha || $request_captcha != $_SESSION[$CONFIG["faq_captcha_secretword"]]))
				{
					unset($_SESSION[$CONFIG["faq_captcha_secretword"]]);
					$blockError = "block_errorcode";
					$showForm = true;
				} else {
					#unset($_SESSION[$CONFIG["faq_captcha_secretword"]]);
					list($allow, $ctg_rights) = $this->test_item_rights("r", "id", $this->table_prefix."_faq_categories",
																		$category, false, 0, true);

					if ($allow) {
						if ($this->add_faq($category, $request_username, $request_email, $request_city, $request_question, '', $ctg_rights)) {
                            $this->sendAdmin($category, $request_username, $request_email, $request_city, $request_question);
							$blockError = "block_success";
							$showForm = false;
							@$cache->delete_cache_files();
						}
					}
				}
				$main->include_main_blocks($this->module_name . "_success.html", "main");
				if($showForm){
					$tpl->assignInclude("post_faq_form", $tpl_path.$this->module_name."_post_form.html");
				}
				$tpl->prepare();


				if($blockError) $tpl->newBlock($blockError);
				$tpl->gotoBlock("_ROOT");

				if($showForm){
					$tpl->assign('category', $category);
					$arr = array('username','city','email','question');
					foreach($arr as $_v){
						$assign[$_v.'_value'] = htmlspecialchars($_POST[$_v],ENT_QUOTES);
					}
					$tpl->assign($assign);
				}

				if($CONFIG["faq_captcha"] == "on")
				{
					$tpl->newBlock("block_captcha");
					$tpl->assign(
						array(
							'captcha_image_url'		=>	"/mod/faq/captcha/image.php?rand=" . rand(0, 100000),
							'captcha_maxlength'		=>	$CONFIG["faq_captcha_wordlength"],
							'captcha_rand'		=>	rand(1, 9999),
						)
					);
				}

				$this->main_title = $this->_msg["Add_message"];
				break;


// Default...
			default:
				if (is_object($tpl)) $tpl->prepare();
				return;
			}
//------------------------------- конец обработки действий с сайта -----------------------------//
		} else {
//------------------------------ обработка действий из админ части -----------------------------//
			if (!isset($tpl)) {
			    $main->message_die(iconv("WINDOWS-1251", "UTF-8", 'Не подключен класс для работы с шаблонами'));
			}

			switch ($action) {
		    case 'prp':
		    	if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step;
				$main->include_main_blocks('_properties.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Information"	=> $main->_msg["Information"],
					"MSG_Value"			=> $main->_msg["Value"],
					"MSG_Save"			=> $main->_msg["Save"],
					"MSG_Cancel"		=> $main->_msg["Cancel"],
				));
		        if (!$request_step || $request_step == 1) {
					$tpl->assign(array('form_action'	=> $baseurl,
									   'step'			=> 2,
									   'lang'			=> $lang,
									   'name'			=> $this->module_name,
									   'action'			=> 'prp'));
					$prp_html = $main->getModuleProperties($this->module_name);
		            $this->main_title = $this->_msg["Controls"];
				} elseif (2 == $request_step) {
		        	$main->setModuleProperties($this->module_name, $_POST);
					header('Location: '.$baseurl.'&action=prp&step=1');
				}
		    	break;

			case 'block_prp':
				global $request_sub_action, $request_block_id;
		    	$arr   = $this->getEditLink($request_sub_action, $request_block_id);
				$cont = 'var container = document.getElementById(\'target_span\');
		    		  	 container.innerHTML = "'.$this->getPropertyFields().'";
		    		  	 material_id		  = "'.$arr['material_id'].'";
		    		  	 material_url		  = "'.$arr['material_url'].'";
		    		  	 changeAction();
		    	      	 setURL("property1");';
		       	header('Content-Length: '.strlen($cont));
		    	echo $cont;
		    	exit;
		    	break;

			case 'list':
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_start, $request_category;
				$start		= ($request_start) ? $request_start : 1;
				$category	= ($request_category) ? $request_category : 1;
				$main->include_main_blocks_2($this->module_name."_list.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Activ" => $this->_msg["Activ"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Rights" => $main->_msg["Rights"],
					"MSG_Name" => $this->_msg["Name"],
					"MSG_Date" => $this->_msg["Date"],
					"MSG_Question" => $this->_msg["Question"],
					"MSG_Answer" => $this->_msg["Answer"],
					"MSG_Shows" => $this->_msg["Shows"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Select_action" => $this->_msg["Select_action"],
					"MSG_Activate" => $this->_msg["Activate"],
					"MSG_Suspend" => $this->_msg["Suspend"],
					"MSG_Delete" => $this->_msg["Delete"],
					"MSG_Execute" => $this->_msg["Execute"],
				));
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				$pages_cnt = $this->show_questions('block_question_list', $request_category, $request_start, "", "", $ctg_owner_id);
				$main->_show_nav_block($pages_cnt, null, "action=".$action."&category=".$category, $start);
				$tpl->assign(Array(
					"MSG_Pages" => $this->_msg["Pages"],
				));
				$this->main_title	= ($category_title) ? $category_title : "";
				break;

// Добавление вопроса
			case 'add':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step, $request_category, $request_username, $request_email,
						$request_city, $request_question, $request_txt_content, $request_answer,
						$request_block_id;
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(array(
						"MSG_Enter_question" => $this->_msg["Enter_question"],
						"MSG_Choose_category" => $this->_msg["Choose_category"],
						"MSG_Category" => $this->_msg["Category"],
						"MSG_Select_category" => $this->_msg["Select_category"],
						"MSG_User_name" => $this->_msg["User_name"],
						"MSG_Email" => $this->_msg["Email"],
						"MSG_City" => $this->_msg["City"],
						"MSG_Question" => $this->_msg["Question"],
						"MSG_required_fields" => $this->_msg["required_fields"],
						"MSG_Save" => $this->_msg["Save"],
						"MSG_Cancel" => $this->_msg["Cancel"],
                        "MSG_Answer_sender" => $this->_msg["Answer_sender"],
						'_ROOT.form_action'	=> $baseurl.'&action=add&step=2',
						'_ROOT.cancel_link'	=> $baseurl.'&action=showctg',
					));

					$nf = $this->show_categories('', true);
					if ($nf == 0) {
						header('Location: '.$baseurl.'&action=showctg&empty');
						exit;
					}
					$this->main_title	= $this->_msg["Add_question"];
				} else if ($request_step == 2) {
					list($allow, $ctg_rights) = $this->test_item_rights("e", "id", $this->table_prefix."_faq_categories",
																		$request_category, false, 0, true);
					if (!$allow) $main->message_access_denied($this->module_name, $action);

					$block_id = intval(substr($request_block_id, 10));
					$lid = $this->add_faq($request_category, $request_username, $request_email, $request_city, $request_question, $request_txt_content, $ctg_rights, $block_id);
					if ($lid) {
						@$cache->delete_cache_files();
						$lc->add_internal_links($this->module_name, $this->table_prefix.'_faq', 'answer', $lid, 'ссылка в вопрос-ответ', $request_answer);
						header('Location: '.$baseurl.'&action=list&category='.$request_category);
					} else {
						header('Location: '.$baseurl.'&action=showctg');
					}
					exit;
				}
				break;

// Редактирование вопроса и ответа
			case 'edit':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_faq", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					"MSG_Enter_question" => $this->_msg["Enter_question"],
					"MSG_Choose_category" => $this->_msg["Choose_category"],
					"MSG_Category" => $this->_msg["Category"],
					"MSG_Select_category" => $this->_msg["Select_category"],
					"MSG_User_name" => $this->_msg["User_name"],
					"MSG_Email" => $this->_msg["Email"],
					"MSG_City" => $this->_msg["City"],
					"MSG_Question" => $this->_msg["Question"],
					"MSG_required_fields" => $this->_msg["required_fields"],
                    "MSG_Answer_sender" => $this->_msg["Answer_sender"],
					"MSG_Save" => $this->_msg["Save"],
					"MSG_Cancel" => $this->_msg["Cancel"],
					'_ROOT.form_action'	=> $baseurl.'&action=update&id='.$request_id,
					'_ROOT.cancel_link'	=> $baseurl.'&action=list&category='.$request_category.'&start='.$request_start,
				));
				if (!$this->show_question($request_id, $request_start, 1)) {
					header('Location: '.$baseurl.'&action=showctg');
					exit;
				}
				$this->show_categories($request_category, true);
				$this->main_title	= $this->_msg["Edit_question"];
				break;

// Обновление faq
			case 'update':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_username, $request_email, $request_city, $request_start,
						$request_question, $request_txt_content, $request_id, $request_answer, $request_send_mess;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_faq", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);
				if (!$this->update_faq($request_category, $request_username, $request_email, $request_city, $request_question, $request_txt_content, $request_id)) {

                    if($request_send_mess && !empty($request_email)){
                       $this->sendAnswer($request_category, $request_username, $request_email, $request_city, $request_question, $request_txt_content, $request_id);
                    }
					@$cache->delete_cache_files();
					$lc->add_internal_links($this->module_name, $this->table_prefix.'_faq', 'answer', $request_id, 'ссылка в вопрос-ответ', $request_answer);
				}
				header('Location: '.$baseurl.'&action=list&category='.$request_category.'&start='.$request_start);
				exit;
				break;

// Удаляем faq
			case "del":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("d", "id", $this->table_prefix."_faq", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($this->delete_faq($request_id)) {
					@$cache->delete_cache_files();
					$lc->delete_internal_links($this->module_name, $this->table_prefix."_faq", $request_id);
				}
				header("Location: $baseurl&action=list&category=$request_category&start=$request_start");
				exit;
				break;

// Отключение публикации
			case "suspend":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("p", "id", $this->table_prefix."_faq", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($main->suspend($this->table_prefix."_faq", $request_id)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=list&category=$request_category&start=$request_start");
				exit;
				break;

// Включение публикации
			case "activate":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("p", "id", $this->table_prefix."_faq", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($main->activate($this->table_prefix."_faq", $request_id)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=list&category=$request_category&start=$request_start");
				exit;
				break;


// Удалить выбранные вопросы-ответы
			case "delete_checked":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_category, $request_start;
				if(is_array($request_check)) {
					list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("d", "id", $this->table_prefix."_faq", $v, false, $ctg_owner_id)) {
							if ($this->delete_faq($v)) {
								@$cache->delete_cache_files();
								$lc->delete_internal_links($this->module_name, $this->table_prefix."_faq", $v);
							}
						}
					}
				}
				header("Location: $baseurl&action=list&category=$request_category&start=$request_start");
				exit;
				break;

// Отключить публикацию выбранных вопросов-ответов
			case "suspend_checked":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_category, $request_start;
				if(is_array($request_check)) {
					list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $this->table_prefix."_faq", $v, false, $ctg_owner_id)) {
							if ($main->suspend($this->table_prefix."_faq", $v)) @$cache->delete_cache_files();
						}
					}
				}
				header("Location: $baseurl&action=list&category=$request_category&start=$request_start");
				exit;
				break;

// Выключить публикацию выбранных вопросов-ответов
			case "activate_checked":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_category, $request_start;
				if(is_array($request_check)) {
					list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $this->table_prefix."_faq", $v, false, $ctg_owner_id)) {
							if ($main->activate($this->table_prefix."_faq", $v)) @$cache->delete_cache_files();
						}
					}
				}
				header("Location: $baseurl&action=list&category=$request_category&start=$request_start");
				exit;
				break;


// Редактирование прав элемента галереи
			case 'editrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_faq", $request_id, true, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($this->table_prefix."_faq", "question", $request_id,
											"&id=".$request_id."&category=".$request_category."&start=".$request_start, $this->_msg["Image"])) {
					header('Location: '.$baseurl.'&action=list&category='.$request_category.'&start='.$request_start);
				}
				break;

// Сохранение прав элемента галереи
			case 'saverights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start, $request_rights;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_faq", $request_id, true, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($this->table_prefix."_faq", $request_id, $request_rights);
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
									"res"	=> 1,
									"id"	=> $request_id,
								);
				} else {
					header('Location: '.$baseurl.'&action=list&category='.$request_category.'&start='.$request_start);
				}
				exit;
				break;


// Показать категории вопросов
			case "showctg":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2($this->module_name."_categories.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					"MSG_Empty_field" => $this->_msg["Empty_field"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Rights" => $main->_msg["Rights"],
					"MSG_Category" => $this->_msg["Category"],
                    "MSG_Email" => $this->_msg["GroupEmail"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Create_new_category" => $this->_msg["Create_new_category"],
					"MSG_Create" => $this->_msg["Create"],
					"_ROOT.form_action"	=>	"$baseurl&action=addctg",
				));
				$res = $this->show_categories();
				if (!$res && isset($_REQUEST['empty'])) {
				    $tpl->newBlock('warning');
				    $tpl->assign('txt', 'Список категорий пуст.');
				}
				$this->main_title = $this->_msg["Faq_categories"];
				break;

// Добавить категорию вопросов
			case "addctg":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_title, $request_block_id;
				if ($this->add_category($request_title, intval(substr($request_block_id, 10)))) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showctg");
				exit;
				break;

// Вывод категории на экран для редактирования
			case "editctg":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_category;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_faq_categories", $request_category))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name."_edit_category.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					"MSG_Empty_field"	=> $this->_msg["Empty_field"],
					"MSG_Save"			=> $this->_msg["Save"],
					"MSG_Cancel"		=> $this->_msg["Cancel"],
					"MSG_GroupName"	    => $this->_msg["GroupName"],
					"MSG_GroupEmail"    => $this->_msg["GroupEmail"],
					"_ROOT.form_action"	=>	"$baseurl&action=updatectg",
					"_ROOT.cancel_link"	=>	"$baseurl&action=showctg",
				));
				$this->show_category($request_category);
				$this->main_title = $this->_msg["Edit_category"];
				break;

// Сделать update для категории
			case "updatectg":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_title, $request_mail;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_faq_categories", $request_category))
					$main->message_access_denied($this->module_name, $action);

				if ($this->update_category($request_category, $request_title, $request_mail)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showctg");
				exit;
				break;

// Удаляем категорию
			case "delctg":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_step, $request_category;
				if (!$this->test_item_rights("d", "id", $this->table_prefix."_faq_categories", $request_category))
					$main->message_access_denied($this->module_name, $action);

				// Удаление категории, первый шаг
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name."_del_category.html", $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(Array(
						"MSG_Deleting_category" => $this->_msg["Deleting_category"],
						"MSG_deletes_all_faqs" => $this->_msg["deletes_all_faqs"],
						"MSG_Confirm_delete_category" => $this->_msg["Confirm_delete_category"],
						"MSG_Delete" => $this->_msg["Delete"],
						"MSG_Cancel" => $this->_msg["Cancel"],
					));
					$tpl->assign(array(
						"_ROOT.form_action"	=>	"$baseurl&action=delctg&step=2&category=$request_category",
						"_ROOT.cancel_link"	=>	"$baseurl&action=showctg",
					));
					$this->show_category($request_category);
					$this->main_title = $this->_msg["Deleting_category"];
				// Удаление категории, шаг второй
				} else if ($request_step == 2) {
					if ($this->delete_category($request_category)) @$cache->delete_cache_files();
					header("Location: $baseurl&action=showctg");
					exit;
				}
				break;

// Редактирование прав категории
			case 'editctgrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_start;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_faq_categories", $request_category, true))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($this->table_prefix."_faq_categories", "title", $request_category,
											"&category=".$request_category, $this->_msg["Category"], "savectgrights")) {
					header('Location: '.$baseurl);
				}
				break;

// Сохранение прав категории
			case 'savectgrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_start, $request_rights;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_faq_categories", $request_category, true))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($this->table_prefix."_faq_categories", $request_category, $request_rights);
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
						"res" => 1,
						"category" => $request_category,
				);
				} else {
					header('Location: '.$baseurl);
				}
				exit;
				break;


// Редактирование свойств блока
			case 'properties':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $module_id, $request_field, $request_block_id, $title;
				$main->include_main_blocks('_block_properties.html', 'main');
				$tpl->prepare();

				if ($main->show_actions($request_field, '', $this->block_main_module_actions, $this->block_module_actions)) {
					$main->show_linked_pages($module_id, $request_field);
				}
				$tpl->assign(array( '_ROOT.title'		=> $title,
									'_ROOT.username'	=> $_SESSION['session_login'],
									'_ROOT.password'	=> $_SESSION['session_password'],
									'_ROOT.name'		=> $this->module_name,
									'_ROOT.lang'		=> $lang,
									'_ROOT.block_id'	=> $request_block_id,
									));
				$this->main_title = $this->_msg["Block_properties"];
				break;

// Редактирование свойств експресс ввода
			case 'editexpress':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_category, $db;
				$db->query('UPDATE '.$this->table_prefix.'_faq_categories
		        	SET express = IF (express, 0, 1) WHERE id = '.$request_category);

				header("Location: ". ($_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $baseurl."&action=assortment&id=".$request_category_id));
				break;


// Создание объекта для дальнейшего использования
			case 'only_create_object':
				break;

// Default...
			default:
				$main->include_main_blocks();
				$tpl->prepare();
			}
//--------------------------- конец обработки действий из админ части --------------------------//
		}
	}


	// функция для вывода списка вопросов
	function show_questions($blockname, $category, $start = "", $rows = "", $order_by = "", $owner_id = 0) {
		global $CONFIG, $main, $tpl, $baseurl, $db, $transurl;
		$admin = Module::is_admin();
		$category						= intval($category);
		if (!$category) return 0;
		$ctg_str						= ($category) ? "category_id = $category" : "";
		$start							= intval($start);
		$start							= ($start) ? $start : 1;
		$rows							= intval($rows);
		if (!$rows) 		$rows		= $CONFIG["faq_max_rows"];
		if (!$order_by)		$order_by	= $CONFIG["faq_order_by"];
		if (!$admin)		$act_str	= "AND active = 1";
		$tpl->gotoBlock("_ROOT");
		$tpl->assign(array(
			add_question_action	=>	"$baseurl&action=add&nocache",
			category			=>	$category,
			));

		$total_cnt = $this->get_list_with_rights(
								"f.*, DATE_FORMAT(f.date,'".$CONFIG["faq_date_format"]."') as date",
								$this->table_prefix."_faq f", "f",
								$ctg_str." ".$act_str, "", $order_by,
								$start, $rows,
								true, $owner_id);
		$pages_cnt = ceil($total_cnt/$rows);
		if ($db->nf()) {
			$actions = array(	"edit"		=> "edit",
								"editrights"=> "editrights",
								"activate"	=>	array("activate","suspend"),
								"delete"	=> "del",
								"confirm_delete"	=> $this->_msg["Confirm_delete_question"]
							);
			while($db->next_record()) {
				$id				= intval($db->f("id"));
				$category_id	= intval($db->f("category_id"));
				$adm_actions	= $this->get_actions_by_rights($baseurl."&id=".$id."&category=".$category_id."&start=".$start, $db->f("active"), $db->f("is_owner"), $db->f("user_rights"), $actions);
				#$question		= htmlspecialchars(stripslashes($main->tokenize($db->f("question"), 50)));
                $question		= htmlspecialchars(stripslashes($db->f("question")));
				$username		= htmlspecialchars(stripslashes($db->f("username")));
				$email			= stripslashes(str_replace("@", " [at] ", $db->f("email")));
				$email			= stripslashes(str_replace(".", " [dot] ", $email));
				$city			= $db->f("city") ? stripslashes($db->f("city")) : "-";
				$answer			= $db->f("answer");
				$tpl->newBlock($blockname);
				$tpl->assign(array(
					'faq_question'			=> $question,
					'faq_username'			=> $username,
					'faq_email'				=> $db->f("email") ? "<a href=\"mailto:".$email."\">".$email."</a>" : "-",
					'faq_city'				=> $city,
					'faq_date'				=> $db->f("date"),
					'faq_answer'			=> $answer,
					'faq_hits'				=> $db->f("hits"),
					'faq_link'				=> $baseurl."&action=show&category=".$category_id."&id=".$id,
					'faq_id'				=> $id,
					'faq_status'			=> $db->f("active") ? "<span style=\"color: green; font-weight: bold;\">" . $this->_msg["Publish"] . "</span>" : "<span style=\"color: red; font-weight: bold;\">" . $this->_msg["Not_publish"] . "</span>",
					'start'					=> $start,
					'faq_edit_action'		=>	$adm_actions["edit_action"],
					'faq_edit_rights_action'=>	$adm_actions["edit_rights_action"],
					'faq_del_action'		=>	$adm_actions["delete_action"],
					'change_status_action'	=>	$adm_actions["change_status_action"],
				));
				if ($adm_actions["rights"]["d"] || $adm_actions["rights"]["p"]) {
					$tpl->newBlock('block_check');
					$tpl->assign("faq_id", $id);
				}
			}
			return $pages_cnt;
		}
		return FALSE;
	}


	// функция для вывода вопроса и ответа к нему по id
	function show_question($id = null, $start = null, $admin = 0) {
		global $CONFIG, $main, $tpl, $baseurl, $db;
		$admin = Module::is_admin();
		$id = intval($id);
		if (!$id) return 0;

		$this->get_list_with_rights(
						"f.*, DATE_FORMAT(f.date,'".$CONFIG["faq_date_format"]."') as date",
						$this->table_prefix."_faq f", "f",
						"id = ".$id);

		if ($db->next_record()) {
			$question 	= htmlspecialchars(stripslashes($db->f("question")));
			$username 	= htmlspecialchars(stripslashes($db->f("username")));
			$email		= stripslashes(str_replace("@", " [at] ", $db->f("email")));
			$email		= stripslashes(str_replace(".", " [dot] ", $email));
			if (!$admin) {
				$city 		= $db->f("city") ? " (".htmlspecialchars(stripslashes($db->f("city"))).") ": "";
				$this->main_title = $main->tokenize($db->f("question"), 17);
				$tpl->newBlock('block_question');
				$tpl->assign(array(
					faq_question	=>	nl2br($question),
					faq_date		=>	$db->f("date"),
					faq_username	=>	$username,
					faq_email		=>	$email,
					faq_city		=>	$city,
					faq_answer		=>	nl2br(stripslashes($db->f("answer"))),
					faq_id			=>	intval($db->f("id")),
					start			=>	$start,
				));
			} else {
				$tpl->assign(array(
					'text'			=> nl2br($db->f("answer")),
					faq_question	=> htmlspecialchars($db->f("question")),
					faq_date		=> htmlspecialchars($db->f("date")),
					faq_username	=> htmlspecialchars($db->f("username")),
					faq_email		=> htmlspecialchars($db->f("email")),
					faq_city		=> htmlspecialchars($db->f("city")),
					faq_id			=> intval($db->f("id")),
					start			=> $start,
				));
			}
			$this->addition_to_path = $question;
			return TRUE;
		}
		return FALSE;
	}


	// функция для добавления faq в базу данных
	function add_faq($category, $username,  $email, $city, $question, $answer = "", $item_rights, $block_id = 0) {
		global $CONFIG, $db;
		if (!$category) return 0;

		$db->query("SELECT usr_group_id FROM ".$this->table_prefix."_faq_categories WHERE id=".$category);
		if (!$db->next_record()) return false;
		$owner_id = $_SESSION["siteuser"]["id"];
		$ctg_group_id = $db->f("usr_group_id");
		list($group_id, $rights) = $this->get_group_and_rights('faq_default_rights', $block_id, $ctg_group_id);
		if (empty($group_id)) $group_id = $ctg_group_id;
		$rights = $item_rights ? $item_rights : $rights;
		$time = false;
        if(strlen($_POST['date'])){        	$d = explode('.', $_POST['date']);
        	$time = $d[2].'-'.$d[1].'-'.$d[0].' 12:12:12';
        }
		$FD = new CFaqData_DB($this->table_prefix."_faq", "id");
		$FD->id				= 0;
		$FD->question		= addslashes($question);
		$FD->username		= $username ? addslashes($username) : "anonymous";
		$FD->email			= addslashes($email);
		$FD->city			= addslashes($city);
		$FD->answer			= addslashes($answer);
		$FD->category_id	= intval($category);
		$FD->date			= $time ? $time : date("Y-m-d H:i:s");
		$FD->active			= 0;
		$FD->owner_id		= $owner_id;
		$FD->usr_group_id	= $group_id;
		$FD->rights			= $rights;
		if (!$FD->store()) {
			echo "error!";
			return 0;
		} else {
			return 1;
		}
	}


	// вывести навигационную строчку, отображающую путь
	function show_nav_path($category = 0, $id = 0, $transurl = "?") {
		global $CONFIG, $tpl;
		$admin = Module::is_admin();
		$path = $this->get_nav_path(intval($category), intval($id), $transurl);
		$tpl->assign(array(
			"_ROOT.path"	=>	$path,
		));
		return 1;
	}


	// получить навигационную строчку, отображающую путь
	function get_nav_path($category, $id, $transurl = "?", $show_question = 0) {
		global $CONFIG, $main, $server, $lang;
		$admin = Module::is_admin();
		$separator = $CONFIG["nav_separator"];
		if ($category) {
			$FC = new CFaqCategories_DB($this->table_prefix."_faq_categories", "id");
			$FC->load($category);
			if ($FC->id) {
				$root = ($id) ? "$separator<a href=\"$transurl\">".$FC->title."</a>" : "$separator".$FC->title;
			}
		}
		if ($id) {
			$FD = new CFaqData_DB($this->table_prefix."_faq", "id");
			$FD->load($id);
			if ($FD->id) {
				if ($show_question != 0) $path = "$separator<span class=red>".$main->tokenize($FD->question, 17)."</span>";
			}
		}
		$path = $root . $path;
		return $path;
	}

	function getPropertyFields() {
		GLOBAL $request_site_target, $request_sub_action;
		if ('' != $request_sub_action) {

			$tpl = $this->getTmpProperties();

			switch ($request_sub_action) {
				case 'list':
					$tpl->newBlock('block_properties');
					$tpl->assign(Array(
						"MSG_Faq_category" => $this->_msg["Faq_category"],
						"MSG_Select" => $this->_msg["Select"],
						"MSG_Show_faq_number" => $this->_msg["Show_faq_number"],
					));
					$list = $this->getCategories('block_categories');
					abo_str_array_crop($list);
					$tpl->assign_array('block_categories', $list);
					$tpl->newBlock('block_page_edit');
					$tpl->assign(Array(
						"MSG_Goto_edit" => $this->_msg["Goto_edit"],
						"site_target" => $request_site_target,
					));
					break;

				default:
					$tpl->newBlock('block_properties_none');
					$tpl->assign(Array(
						"MSG_No" => $this->_msg["No"],
					));
					break;
			}
			return $this->getOutputContent($tpl->getOutputContent());
		}
		return FALSE;
	}

	function getEditLink($sub_action = NULL, $block_id = NULL) {
		GLOBAL $request_name, $db, $lang, $server;
		$block_id	= (int)$block_id;
		if ($sub_action && $block_id) {
			$db->query('SELECT	property1,
								property2,
								property3,
								property4,
								property5
							FROM '.$this->table_prefix.'_pages_blocks
							WHERE id = '.$block_id);
			if ($db->nf() > 0) {
				$db->next_record();
				$arr[]	= $db->f('property1');
				$arr[]	= $db->f('property2');
				$arr[]	= $db->f('property3');
				$arr[]	= $db->f('property4');
				$arr[]	= $db->f('property5');

				switch ($sub_action) {
					case 'list':
						$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=list&menu=false&category=';
						$link['material_id']	= $arr[0];
						break;
				}
				return $link;
			}

			switch ($sub_action) {
				case 'list':
					$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=list&menu=false&category=';
					$link['material_id']	= '';
					break;
			}
			return $link;
		}
		return FALSE;
	}

	// функция для редактирования faq в базе данных
	function update_faq($category, $username, $email, $city, $question, $answer, $id) {
		global $CONFIG, $server, $lang;
		$admin = Module::is_admin();
    	$id					= (int)$id;
		if (!$id) return 0;
		if (!$category) return 0;
		$time = false;
        if(strlen($_POST['date'])){
        	$d = explode('.', $_POST['date']);
        	$time = $d[2].'-'.$d[1].'-'.$d[0].' 12:12:12';
        }
		$FD = new CFaqData_DB($this->table_prefix."_faq", "id");
		$FD->id				= $id;
		$FD->question		= addslashes($question);
		$FD->username		= $username ? addslashes($username) : "anonymous";
		$FD->email			= addslashes($email);
		$FD->city			= addslashes($city);
		$FD->answer			= addslashes($answer);
		$FD->category_id	= intval($category);
		$FD->date			= $time ? $time : date("Y-m-d H:i:s");
		if (!$FD->store()) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


	// функция для удаления faq из базы данных
	function delete_faq($id) {
		global $CONFIG, $server, $lang;
		$admin = Module::is_admin();
		$id = (int)$id;
		$FD = new CFaqData_DB($this->table_prefix."_faq", "id");
		if (!$FD->delete($id)) {
			return FALSE;
		} else {
			return TRUE;
		}
	}


	function getCategories($selected_category = NULL) {
		GLOBAL $CONFIG, $baseurl, $server, $lang;
		$admin = Module::is_admin();
		$selected_category = intval($selected_category);
		$FC		= new CFaqCategories_DB($this->table_prefix."_faq_categories", "id");
		$temp	= $FC->listAll();
		if (count($temp) > 0) {
			foreach ($temp as $v) {
				$id			= (int)$v["id"];
				$title		= htmlspecialchars(stripslashes($v["title"]));
				$selected	= ($id == $selected_category) ? ' selected' : '';
				$arr[]		= array('category_title'		=> $title,
									'category_link'			=> $baseurl.'&action=list&category='.$id,
									'category_edit_link'	=> $baseurl.'&action=editctg&category='.$id,
									'category_del_link'		=> $baseurl.'&action=delctg&category='.$id,
									'category'				=> $id,
									'selected'				=> $selected,
								);
			}
			return $arr;
		}
		return FALSE;
	}

	function show_categories($selected_category = "", $only_editable = false) {
		global $CONFIG, $db, $tpl, $baseurl;
		$admin = Module::is_admin();
		$selected_category = intval($selected_category);
		$this->get_list_with_rights(
				"C.*",
				$this->table_prefix."_faq_categories AS C", "C",
				"", "", "C.id");
		$cnt = $db->nf();
		if ($cnt) {
			$actions = array(	"edit"		=> "editctg",
								"editrights"=> "editctgrights",
								"delete"	=> "delctg",
								"express"	=> "editexpress",
								"confirm_delete"	=> ""
							);
			while ($db->next_record()) {
				if ($only_editable && !($db->f("user_rights") & 2)) continue;

				$id				= intval($db->f("id"));
				$actions["express_title"]=	$db->f("express") ? $this->_msg["Confirm_unset_express"] : $this->_msg["Confirm_set_express"];
			  $actions["express_img"]	=	$db->f("express") ? "b_select.gif" : "b_select_off.gif";
				$adm_actions	= $this->get_actions_by_rights($baseurl."&category=".$id, 0, $db->f("is_owner"), $db->f("user_rights"), $actions);
				$title			= htmlspecialchars(stripslashes($db->f("title")));
        $mail			= htmlspecialchars(stripslashes($db->f("mail")));
				$selected = ($id == $selected_category) ? " selected" : "";
				$count = $this->getCountQuestions($id);
				$tpl->newBlock('block_categories');
				$tpl->assign(array(
					"MSG_Category_ID"	=> $this->_msg["Category_ID"],
					"MSG_is"			=> $this->_msg["is"],
          "category_title"		=>	$title,
					"category_mail"		=>	$mail,
					"category_link"		=>	"$baseurl&action=list&category=".$id,
					"category_edit_action" =>	$adm_actions["edit_action"],
					"category_edit_rights_action"	=>	$adm_actions["edit_rights_action"],
					"category_del_action"	=>	$adm_actions["delete_action"],
					"category"			=>	$id	,
					"express"			=>	$adm_actions["express_action"],
					"ques"			=>	$count[0],
					"noans"			=>	$count[1],
					"selected"			=>	$selected,
				));
			}
			return $cnt;
		}
		return FALSE;
	}

 //функция считает количество вопросов и не отвеченых
 	function getCountQuestions($id) {
	  global $db1;
	    $nonAnswer = 0;
	    $count = 0;
	    $query = "SELECT category_id, answer FROM ".SITE_PREFIX."_faq WHERE category_id=".$id;
      $db1->query($query);
      for ($i=0;$i<$db1->nf();$i++) {
        $db1->next_record();
        if($db1->f('answer') == '') $nonAnswer++; else $count++;
      }
    return $line = array($count,$nonAnswer);
  }

	// функция для вывода инофрмации о категории по id
	function show_category($id = "", $need_output = true) {
		global $CONFIG, $tpl, $baseurl, $server, $lang;
		$admin = Module::is_admin();
		$id = intval($id);
		if (!$id) return 0;
		$FC = new CFaqCategories_DB($this->table_prefix."_faq_categories", "id");
		$FC->load($id, true, true);
		if ($FC->id) {
			if ($need_output) {
				$tpl->assign(array(
					category		=>	(int)$FC->id,
					category_title	=>	htmlspecialchars(stripslashes($FC->title)),
                    category_mail	=>	htmlspecialchars(stripslashes($FC->mail)),
					category_link	=>	"$baseurl&action=list&category=".intval($FC->id),
				));
			}
			return array(htmlspecialchars(stripslashes($FC->title)), $FC->owner_id);
		}
		return FALSE;
	}


	// функция для добавления категории
	function add_category($title, $block_id = 0) {
		global $CONFIG, $db, $server, $lang;
		$title = $db->escape(trim($title));
		if (!$title) return FALSE;

		$owner_id = $_SESSION["siteuser"]["id"];
		list($group_id, $rights) = $this->get_group_and_rights('faq_default_rights', $block_id);

		$query = "INSERT INTO ".$this->table_prefix."_faq_categories (title,owner_id,usr_group_id,rights)
					VALUES ('$title',$owner_id,$group_id,$rights)";
		$db->query($query);
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}


	// функция для update категории по id
	function update_category($id, $title, $mail=null) {
		global $CONFIG, $server, $lang;
		$admin = Module::is_admin();
		$id = (int)$id;
		if (!$id) return FALSE;
		if (!$title) return FALSE;
		$FC = new CFaqCategories_DB($this->table_prefix."_faq_categories", "id");
		$FC->id		=	$id;
		$FC->title	=	addslashes($title);
        $FC->mail	=	addslashes($mail);
		if (!$FC->store()) {
			return FALSE;
		} else {
			return TRUE;
		}
	}


	// функция для удаления категории по id
	function delete_category($id = "") {
		global $CONFIG, $tpl, $server, $lang;
		$admin = Module::is_admin();
		$id = (int)$id;
		if (!$id) return FALSE;
		$FC = new CFaqCategories_DB($this->table_prefix."_faq_categories", "id");
		if (!$FC->DeleteByID($id)) {
			return FALSE;
		} else {
			$FD = new CFaqData_DB($this->table_prefix."_faq", "id");
			$FD->DeleteByCategory($id);
			return TRUE;
		}
		return FALSE;
	}
    function getMail($category=null){
        global $CONFIG;
        if(intval($category)){
            $FC = new CFaqCategories_DB($this->table_prefix."_faq_categories", "id");
		    $FC->load((int)$category, true, true);
            if(!empty($FC->mail)){$mail = $FC->mail;}

        }
        if(empty($mail)){$mail = $CONFIG['faq_mail'];}
        return ($FC) ? $FC : false;

    }
    function sendAdmin($category, $username, $email, $city, $question){
        global $CONFIG, $main, $server, $lang;
        if(false === ($FC = $this->getMail($category))) return false;
        if(empty($FC->mail)){$FC->mail = $CONFIG['faq_mail'];}
		$t = new AboTemplate('tpl/'.SITE_PREFIX.'/faq_mail.html');
		$t->prepare();

		$t->newBlock('block_question');

		$t->assign(array(
				'HOST' 		=> $_SERVER['HTTP_HOST'] ,
				'RZD' 		=> $FC->title,
				'username' 	=> $username ,
				'email' 	=> $email ,
				'city' 		=> $city ,
				'question' 	=> $question ,
				'date' 		=> date('d.m.Y H:i') ,
				'IP' 		=> $_SERVER['REMOTE_ADDR'] ,
		 ));
		 require_once(RP.'/inc/class.Mailer.php');
		 $mail = new Mailer();
         $mail->CharSet		= $CONFIG['email_charset'];
         $mail->ContentType	= 'text/html';
         $mail->From = $CONFIG['return_email'];
         $mail->FromName = $CONFIG['sitename_rus'];
         $mail->Mailer = 'mail';
         $mail->Subject = sprintf($this->_msg["Admin_mailSubject"], $FC->title);
         $mail->Body = $t->getOutputContent();
         $mail->AddAddress($FC->mail);
         $mail->Send();
         return true;
    }
    function sendAnswer($category, $username, $email, $city, $question, $txt_content, $id){
        global $CONFIG, $main, $server, $lang;
        if(false === ($FC = $this->getMail($category))) return false;
		$t = new AboTemplate('tpl/'.SITE_PREFIX.'/faq_mail.html');
		$t->prepare();
		$t->newBlock('block_answer');

		$t->assign(array(
				'HOST' 		=> $_SERVER['HTTP_HOST'] ,
				'RZD' 		=> $FC->title,
				'username' 	=> $username ,
				'email' 	=> $email ,
				'city' 		=> $city ,
				'question' 	=> $question ,
                'answer' 	=> $txt_content,
				'date' 		=> date('d.m.Y H:i') ,
				'IP' 		=> $_SERVER['REMOTE_ADDR'] ,
		 ));
		 require_once(RP.'/inc/class.Mailer.php');
		 $mail = new Mailer();
         $mail->CharSet		= $CONFIG['email_charset'];
         $mail->ContentType	= 'text/html';
         $mail->From = $FC->mail;
         $mail->FromName = $CONFIG['sitename_rus'];
         $mail->Mailer = 'mail';
         $mail->Subject = sprintf($this->_msg["User_mailSubject"], $_SERVER['HTTP_HOST']);
         $mail->Body = $t->getOutputContent();
         $mail->AddAddress($email, $username);
         $mail->Send();
         return true;
    }
}

?>