<?php

class CFaqData_DB extends TObject {
	var $id				= NULL;
	var $category_id	= NULL;
	var $username		= NULL;
	var $email			= NULL;
	var $city			= NULL;
	var $question		= NULL;
	var $date			= NULL;
	var $answer			= NULL;
	var $active			= NULL;
	var $owner_id		= NULL;
	var $usr_group_id	= NULL;
	var $rights			= NULL;

	function CFaqData_DB ($table, $PK) {
		$this->TObject($table, $PK);
	}

	function showQuestions($dateFormat, $ctg_str = null, $order_by = "id", $start_row, $rows) {
		$admin = Module::is_admin();
		if (!$admin) {
			$act_str = "AND active = 1";
		}
		$start_row = (int) $start_row;
		$rows = (int) $rows;
		$sql = "SELECT *, DATE_FORMAT(date,'".My_Sql::escape($dateFormat)."') as date FROM $this->_tbl WHERE ".My_Sql::escape($ctg_str)
		    ." {$act_str} ORDER BY ".My_Sql::escape($order_by)." LIMIT $start_row,$rows";
		return db_loadList($sql);
	}

	function loadByID($dateFormat, $id) {
	    $id = (int) $id;
		$sql = "SELECT *, DATE_FORMAT(date,'".$dateFormat."') as date FROM $this->_tbl WHERE id = $id";
		return db_loadObject($sql, $this);
	}

	function DeleteByCategory($id) {
		$id = (int) $id;
		$sql = "DELETE FROM $this->_tbl WHERE category_id = '$id'";
		db_exec($sql);
		if (db_affected_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}
}

class CFaqCategories_DB extends TObject {
	var $id				= NULL;
	var $title			= NULL;
	var $owner_id		= NULL;
	var $usr_group_id	= NULL;
	var $rights			= NULL;
	function CFaqCategories_DB ($table, $PK) {
		$this->TObject($table, $PK);
	}
	function ListAll () {
		$sql = "SELECT * FROM $this->_tbl ORDER BY id";
		return db_loadList($sql);
	}
	function DeleteByID($id) {
		$id = (int) $id;
		$sql = "DELETE FROM $this->_tbl WHERE id = '$id'";
		db_exec($sql);
		if (db_affected_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}

}

?>