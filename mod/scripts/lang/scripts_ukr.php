<?php
$_MSG = Array(
	'scripts'					=>	'Скрипты',
	'script_list'				=>	'Список скриптов',
	'add_script'				=>	'Добавить скрипт',
	
	'execute_script'			=>	'Выполнить скрипт',
	'script'					=>	'Скрипт',
	'block_properties'			=>	'Свойства блока',
	'select'					=>	'Выбрать...', 
	'No'						=>	'Нет', 
	'script_list'				=> 	'Список скриптов',
	'ed'						=>	'Ред.', 
	'edit'						=>	'Редактировать',
	'caption'					=>	'Название',
	'description'				=>	'Описание',
	'del'						=>	'Удл.', 
	'delete'					=>	'Удалить', 
	'empty_field'				=>	'Пустое поле!',
	'save'						=>	'Сохранить', 
	'cancel'					=>	'Отменить', 
	'script_content'			=>	'Содержимое',
);
?>