<?php
class ScriptsPrototype extends Module
{
	var $main_title;
	var $table_prefix;
	var $addition_to_path;
	var $module_name				= 'scripts';
	var $default_action				= 'show_scripts';
	var $admin_default_action		= 'show_scripts';
	var $tpl_path					= 'mod/scripts/tpl/';
	var $block_module_actions		= array();
	var $block_main_module_actions	= array();
	var $allow_img_typs = array('image/gif', 'image/jpeg', 'image/png', 'image/pjpeg', 'image/x-png');

	/**
	 * Конструктор класса для работы со скриптами.
	 *
	 * @param unknown_type $action
	 * @param unknown_type $transurl
	 * @param unknown_type $properties
	 * @param unknown_type $prefix
	 * @param unknown_type $rewrite_mod_params
	 * @param unknown_type $adminurl
	 * @param unknown_type $noadmin
	 * @return ScriptsPrototype
	 */
	function ScriptsPrototype($action = '', $transurl = '', $properties = array(), $prefix = NULL, $rewrite_mod_params = null, $adminurl = null, $noadmin = false) {
		// Задействуем глобальные переменные.
		global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang,	$permissions, $request_type, $_RESULT;
    // Устанавливаем принятые из клиентской части идентификаторы и свойства,
		// согласно настройкам в структуре сайта для данного модуля и страницы.
		$script_id = intval($properties[1]);
		
		/* Get $action, $tpl_name, $permissions */
		extract( $this->init(array(
		        'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
		        'action' => $action, 'tpl_name' => $properties['tpl']
		)), EXTR_OVERWRITE);

		// Права доступа из переменных сессии, массив $permissions используется для проверки прав доступа в модуле $name.
		$permissions['r'] = $_SESSION['permissions'][$this->table_prefix][$this->module_name]['r'];		// чтение
		$permissions['e'] = $_SESSION['permissions'][$this->table_prefix][$this->module_name]['e'];		// редактирование
		$permissions['p'] = $_SESSION['permissions'][$this->table_prefix][$this->module_name]['p'];		// публикация
		$permissions['d'] = $_SESSION['permissions'][$this->table_prefix][$this->module_name]['d'];		// удаление

		if ('only_create_object' == $action) {
		    return;
		}
		
		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}
		
		if (self::is_admin()) {
		    // Действия, которые пользователь имеет возможность задать для блока с данным модулем.
		    $this->block_module_actions = array('execute_script'	=> $this->_msg["execute_script"]);
		    $this->block_main_module_actions = array('execute_script'	=> $this->_msg["execute_script"]);
		    if ('only_create_object' == $action) {
		        return;
		    }
			/**
			 * АДМИНКА
			 *
			 *
			 */
			switch ($action) {
				case 'prp':
					if (!$permissions['e'])
					{
						$main->message_access_denied($this->module_name, $action);
					}

					global $request_step;

					$main->include_main_blocks('_properties.html', $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(Array(
						"MSG_Information"	=> $main->_msg["Information"],
						"MSG_Value"			=> $main->_msg["Value"],
						"MSG_Save"			=> $main->_msg["Save"],
						"MSG_Cancel"		=> $main->_msg["Cancel"],
					));


					if (!$request_step || $request_step == 1)
					{
						$tpl->assign(array('form_action'	=> $baseurl,
						'step'			=> 2,
						'lang'			=> $lang,
						'name'			=> $this->module_name,
						'action'		=> 'prp'));
						$prp_html = $main->getModuleProperties($this->module_name);
						$this->main_title = $this->_msg["Controls"];
					}
					elseif (2 == $request_step)
					{
						$main->setModuleProperties($this->module_name, $_POST);
						header('Location: '.$baseurl.'&action=prp&step=1');
					}

					break;

				case 'properties':
					if (!$permissions['e'])
					{
						$main->message_access_denied($this->module_name, $action);
					}

					global $module_id, $request_field, $request_block_id, $title;

					$main->include_main_blocks('_block_properties.html', 'main');
					$tpl->prepare();

					if ($main->show_actions($request_field, '', $this->block_main_module_actions,
					$this->block_module_actions))
					{
						$main->show_linked_pages($module_id, $request_field);
					}
					$tpl->assign(array(
						'_ROOT.title'		=> $title,
						'_ROOT.username'	=> $_SESSION['session_login'],
						'_ROOT.password'	=> $_SESSION['session_password'],
						'_ROOT.name'		=> $this->module_name,
						'_ROOT.lang'		=> $lang,
						'_ROOT.block_id'	=> $request_block_id
					));

					$this->main_title = $this->_msg["block_properties"];
					break;

				case 'block_prp':
					global $request_sub_action, $request_block_id;
					$arr = $this->getEditLink($request_sub_action, $request_block_id);
					$cont = 'var container = document.getElementById(\'target_span\');
			    			 container.innerHTML = "'.$this->getPropertyFields().'";
			    			 material_id		  = "'.$arr['material_id'].'";
			    		  	 material_url		  = "'.$arr['material_url'].'";
			    		  	 changeAction();
			    		  	 setURL("property1");';
					header('Content-Length: '.strlen($cont));
					echo $cont;
					exit();
					break;

				case 'show_scripts':
					if (!$permissions["r"])
					{
						$main->message_access_denied($this->module_name, $action);
					}

					$main->include_main_blocks_2($this->module_name."_show_scripts.html", $this->tpl_path);
					$tpl->prepare();

					$tpl->assign(array(
						'MSG_ed' => $this->_msg['ed'],
						'MSG_caption' => $this->_msg['caption'],
						'MSG_description' => $this->_msg['description'],
						'MSG_del' => $this->_msg['del'],
						'_ROOT.lang' => $lang,
						'_ROOT.name' => $this->module_name,
						'_ROOT.action' => 'show_scripts',
					));

					$this->ShowScriptList();

					$this->main_title = $this->_msg['script_list'];
					break;

				case 'edit_script':
					if (!$permissions['e'])
					{
						$main->message_access_denied($this->module_name, $action);
					}

					global $request_script_id;

					$script_id = intval($request_script_id);
					$main->include_main_blocks_2($this->module_name."_edit_script.html", $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(array(
						'MSG_empty_field' => $this->_msg['empty_field'],
						'MSG_save' => $this->_msg['save'],
						'MSG_cancel' => $this->_msg['cancel'],
						'_ROOT.form_action'	=> "$baseurl&action=update_script",
						'_ROOT.cancel_link'	=> "$baseurl&action=show_scripts",
					));

					$this->ShowScript($script_id);

					break;

				case 'update_script':
					global $request_script_id, $request_script_name, $request_script_description,
					$request_script_content;

					$this->AddUpdateScript($request_script_id, $request_script_name, $request_script_description,
					$request_script_content);

					header("Location: $baseurl&action=show_scripts");
					die();
					break;

				case 'delete_script':
					global $request_script_id;

					$this->DeleteScript($request_script_id);

					header("Location: $baseurl&action=show_scripts");
					die();
					break;

				default:
					$main->include_main_blocks();
					$tpl->prepare();
					break;
			}
		}	else {
			/**
			 * САЙТ.
			 *
			 *
			 */
		    if ('only_create_object' == $action) {
		        return;
		    }
			switch ($action)
			{
				case 'execute_script':

					$script_id = intval($script_id);

					if ($script_id)
					{
						$script = $this->GetScript($script_id);
						if ($script)
						{
							$path = $this->CreateScriptPath();
							if ($path)
							{
								$file_name = $path.$script_id.'.php';

								if (is_file($file_name))
								{
									ob_start();
									require($file_name);
									$content = ob_get_contents();
									ob_end_clean();

									// Глобальный флаг, указывающий на то, что не нужно обрабатывать шаблон.
									global $do_not_process_templates;
									if ($do_not_process_templates)
									{
										echo $content;
									}
									else
									{
										$main->include_main_blocks($this->module_name.'_content_output.html', 'main');
										$tpl->prepare();

										$tpl->newBlock('block_output_content');
										$tpl->assign(Array
										(
											"content" => $content,
										));
									}
								}
							}
						}
					}

					break;
			}
		}
	}

	/**
	 * Метод, отображающий список свойств в зависимости от выбранного действия.
	 * Далее данные передаются через AJAX в структуру сайта при настройке.
	 *
	 * @return mixed строка с результатом или false.
	 */
	function getPropertyFields()
	{
		global  $request_site_target, $request_sub_action, $request_block_id;
		if ($request_sub_action != '')
		{
			$tpl = $this->getTmpProperties();

			switch ($request_sub_action)
			{
				case 'execute_script':
					$tpl->newBlock('block_properties');
					$tpl->assign(Array(
						'MSG_script' => $this->_msg['script'],
						'MSG_select' => $this->_msg['select'],
					));

					$script_list = $this->GetScriptList();
					if ($script_list)
					{
						foreach ($script_list as $script)
						{
							$tpl->newBlock('block_script_list');
							$tpl->assign(Array
							(
								'script_id' => $script['id'],
								'script_name' => abo_str_crop($script['name'])
							));
						}
					}

				default:
					$tpl->newBlock('block_properties_none');
					$tpl->assign(Array(
						'MSG_No' => $this->_msg['No'],
					));
					break;
			}
			return $this->getOutputContent($tpl->getOutputContent());
		}
		return false;
	}

	/**
	 * Метод, который указывает систему, каким образом передавать запрос на страницу из клиентской части,
	 * в зависимости от выбранного действия для страницы.
	 *
	 * @param string $sub_action - название действия
	 * @param int $block_id - идентификатор блока
	 * @return mixed строка с резульататом, либо false.
	 */
	function getEditLink($sub_action = null, $block_id = null)
	{
		global $request_name, $db, $lang;
		$block_id = intval($block_id);
		if ($sub_action && $block_id)
		{
			$db->query('SELECT	property1,
								property2,
								property3,
								property4,
								property5
							FROM '.$this->table_prefix.'_pages_blocks
							WHERE id = '.$block_id);
			if ($db->nf() > 0)
			{
				$db->next_record();
				$arr[]	= $db->f('property1');
				$arr[]	= $db->f('property2');
				$arr[]	= $db->f('property3');
				$arr[]	= $db->f('property4');
				$arr[]	= $db->f('property5');

				switch ($sub_action)
				{
					case 'execute_script':
						$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=execute_script&menu=false&script_id=';
						$link['material_id']	= $arr[0];
						break;
				}
				return $link;
			}

			switch ($sub_action)
			{
				case 'execute_script':
					$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=execute_script&menu=false&script_id=';
					$link['material_id']	= '';
					break;
			}
			return $link;
		}

		return false;
	}

	/**
	 * Метод для получения списка всех скриптов.
	 *
	 * @return mixed массив с идентификатором, именем и описанием скрипта или false.
	 */
	function GetScriptList()
	{
		global $db;

		$sql = "SELECT * FROM {$this->table_prefix}_scripts";
		$result = $db->getArrayOfResult($sql);

		return $result;
	}

	/**
	 * Метод для получения информации о скрипте по идентификатору.
	 *
	 * @param int $script_id - идентификатор скрипта
	 * @return mixed массив с информацией о скрипте или false, в случае неудачи.
	 */
	function GetScript($script_id)
	{
		$script_id = intval($script_id);
		if (!$script_id)
		{
			return false;
		}

		global $db;

		$sql = "SELECT * FROM {$this->table_prefix}_scripts WHERE `id` = '$script_id'";
		$data = $db->getArrayOfResult($sql);

		if (!$data)
		{
			return false;
		}

		$result = $data[0];

		return $result;
	}

	/**
	 * Метод для отображения списка скриптов.
	 *
	 */
	function ShowScriptList()
	{
		global $tpl, $baseurl;

		// Получаем список скриптов.
		$script_list = $this->GetScriptList();

		if (!$script_list)
		{
			return false;
		}

		$actions = array(
			'edit' => 'edit_script',
			'delete' => 'delete_script',
		);

		foreach ($script_list as $script)
		{
			// Извлечем ссылки с картинками для действий.
			$adm_actions = $this->get_actions_by_rights($baseurl."&script_id=".intval($script['id']),
			1, 1, 0xFFFFFF, $actions);

			$tpl->newBlock('block_script_list');

			$params = array(
				'script_caption' =>	htmlspecialchars($script['name']),
				'script_description' =>	htmlspecialchars($script['description']),
				'edit_action' => $adm_actions['edit_action'],
				'delete_action' => $adm_actions['delete_action'],
			);
			$tpl->assign($params);
		}
	}

	/**
	 * Метод для отображение данных о скрипте.
	 *
	 * @param int $script_id - идентификатор скрипта
	 * @return bool результат выполнения функции.
	 */
	function ShowScript($script_id)
	{
		global $tpl, $CONFIG;

		$script_id = intval($script_id);

		$script = array();
		$script['content'] = '';
		if ($script_id)
		{
			// Редактирование. Извлечем данные.
			$script = $this->GetScript($script_id);
			if (!$script_id)
			{
				// Ошибка! В БД нет скрипта с таким id.
				return false;
			}

			$script['name'] = htmlspecialchars($script['name']);
			$script['description'] = htmlspecialchars($script['description']);

			// Считаем контент файла-скрипта.
			$path = $this->CreateScriptPath();
			if ($path)
			{
				// Загрузим данные.
				$file_name = $path.$script_id.'.php';

				if (is_file($file_name))
				{
					$file_handle = @fopen($file_name, 'r');
					if ($file_handle)
					{
						$script['content'] = @fread($file_handle, filesize($file_name));
						@fclose($file_handle);
					}
				}
			}
		}
		else
		{
			// Добавление.
			$script['name'] = '';
			$script['description'] = '';
		}

		// Отображение.
		$tpl->newBlock('block_script_data');
		$params = array(
			'MSG_caption' => $this->_msg['caption'],
			'script_caption' =>	htmlspecialchars($script['name']),
			'MSG_description' => $this->_msg['description'],
			'script_description' =>	htmlspecialchars($script['description']),
			'script_content' =>	htmlspecialchars($script['content']),
			'MSG_script_content' => $this->_msg['script_content'],
		);
		$tpl->assign($params);

		if ($script_id)
		{
			// Отображаем невидимый блок с идентификатором скрипта.
			$tpl->newBlock('block_hidden_script_id');
			$tpl->assign(array(
				"hidden_script_id" => $script_id
			));
		}
	}

	/**
	 * Метод для добавления/обновления скрипта.
	 *
	 * @param int $script_id - идентификатор скрипта, если 0 - будет создан новый
	 * @param string $name - название скрипта
	 * @param string $description - описание скрипта
	 * @param string $content - код скрипта
	 * @return mixed идентификатор добаленного/обновленного скрипта или false.
	 */
	function AddUpdateScript($script_id, $name, $description, $content)
	{
		global $db;

		$script_id = intval($script_id);
		$name = $db->escape(trim($name));
		$description = $db->escape(trim($description));
		$content = trim($content);

		if ($script_id)
		{
			// Обновление.
			$sql = "UPDATE ".$this->table_prefix."_scripts SET
				`name` = '$name',
				`description` = '$description'
				WHERE `id` = '$script_id'";
		}
		else
		{
			// Добавление.
			$sql = "INSERT INTO ".$this->table_prefix."_scripts SET
				`name` = '$name',
				`description` = '$description'";
		}

		$result = $db->query($sql);
		if (!$result)
		{
			return false;
		}

		// Получим идентификатор.
		if (!$script_id)
		{
			$script_id = $db->lid();
		}

		$path = $this->CreateScriptPath();
		if ($path)
		{
			// Сохраним данные.
			$file_name = $path.$script_id.'.php';

			$file_handle = @fopen($file_name, 'w+');
			if ($file_handle)
			{
				@fwrite($file_handle, $content);
				@fclose($file_handle);

				//@chmod($file_name, 0766);
			}
		}

		return $script_id;
	}

	/**
	 * Метод для удаление скрипта.
	 *
	 * @param int $script_id - идентификатор скрипта
	 * @return bool результат удаления.
	 */
	function DeleteScript($script_id)
	{
		$script_id = intval($script_id);

		if (!$script_id)
		{
			return false;
		}

		$sql = "DELETE FROM {$this->table_prefix}_scripts WHERE  `id` = '$script_id'";

		global $db;
		if ($db->query($sql))
		{
			// Удалим файл.
			$path = $this->CreateScriptPath();
			if ($path)
			{
				$file_name = $path.$script_id.'.php';

				if (is_file($file_name))
				{
					@unlink($file_name);
				}
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Проверяет, существует ли путь для хранения скриптов, если нет - создает и возвращает его значение.
	 *
	 * @return mixed путь к папке со скриптами или false.
	 */
	function CreateScriptPath()
	{
		global $CONFIG;

		$path = $CONFIG['files_upload_path'].'scripts/'.SITE_PREFIX;

		if (is_dir($path))
		{
			return $path.'/';
		}
		else
		{
			if (!is_dir($CONFIG['files_upload_path'].'scripts'))
			{
				if (!@mkdir($CONFIG['files_upload_path'].'scripts', 0777))
				{
					@chmod($CONFIG['files_upload_path'].'scripts', 0777);
					return false;
				}
			}

			if (!@mkdir($CONFIG['files_upload_path'].'scripts/'.SITE_PREFIX, 0777))
			{
				@chmod($CONFIG['files_upload_path'].'scripts/'.SITE_PREFIX, 0777);
				return false;
			}

			return $path.'/';
		}
	}
}
?>