<?php

class BackupPrototype extends Module {

	var $main_title;
	var $table_prefix;
	var $module_name				= 'backup';
	var $default_action				= '';
	var $admin_default_action		= 'list';
	var $tpl_path					= 'mod/backup/tpl/';
	var $cmpr_method 				= 1; // Метод сжатия. 0 - без сжатия, 1 - gz сжатие
	var $cmpr_level  				= 7;  // Степень сжатия - 0-9. 0 - без сжатия
	var $_msg						= array();

	function BackupPrototype($action = '') {
		global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang, $permissions;
		require_once(RP.'mod/'.$this->module_name.'/lang/'.$this->module_name.'_'.$CONFIG["admin_lang"].'.php');
		$this->_msg = $_MSG;
		$this->table_prefix = $server.$lang;
		$is_admin = preg_match('/admin\.php/', $_SERVER["SCRIPT_NAME"]);
		
		if ('only_create_object' == $action) {
		    return;
		}
		
		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}

		if (empty($is_admin)) {
//---------------------------------- обработка действий с сайта --------------------------------//
			if (empty($action)) $action = $this->default_action;
//------------------------------- конец обработки действий с сайта -----------------------------//
		} else {
//------------------------------ обработка действий из админ части -----------------------------//
			if (!isset($tpl)) $main->message_die(iconv("WINDOWS-1251", "UTF-8", 'Не подключен класс для работы с шаблонами'));

			
			if (empty($action)) $action = $this->admin_default_action;
			switch ($action) {
		    case 'prp':
		    	if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step;
				$main->include_main_blocks('_properties.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Information"	=> $main->_msg["Information"],
					"MSG_Value"			=> $main->_msg["Value"],
					"MSG_Save"			=> $main->_msg["Save"],
					"MSG_Cancel"		=> $main->_msg["Cancel"],
				));
		        if (!$request_step || $request_step == 1) {
					$tpl->assign(array('form_action'	=> $baseurl,
									   'step'			=> 2,
									   'lang'			=> $lang,
									   'name'			=> $this->module_name,
									   'action'			=> 'prp'));
					$prp_html = $main->getModuleProperties($this->module_name);
		            $this->main_title = $this->_msg["Controls"];
				} elseif (2 == $request_step) {
		        	$main->setModuleProperties($this->module_name, $_POST);
					header('Location: '.$baseurl.'&action=prp&step=1');
				}
		    	break;

    		case "list":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2($this->module_name."_list.html", $this->tpl_path);
				$tpl->prepare();
				$this->show_files_list($CONFIG["backup_files_path"]);
				$tpl->assign(array(
					"_ROOT.make_backup_action"	=>	"$baseurl&action=make",
					"_ROOT.upload_file_action"	=>	"$baseurl&action=upload",
				));
				$tpl->assign(Array(
					"_ROOT.MSG_Load_backup_file_from_hd" => $this->_msg["Load_backup_file_from_hd"],
					"_ROOT.MSG_Upload" => $this->_msg["Upload"],
					"files.MSG_Name" => $this->_msg["Name"],
					"files.MSG_Date" => $this->_msg["Date"],
					"files.MSG_Size" => $this->_msg["Size"],
					"files.MSG_Rec" => $this->_msg["Rec"],
					"files.MSG_Del" => $this->_msg["Del"],
				));
				$this->main_title = $this->_msg["Backup_files_list"];
				break;

// Создать резервную копию
    		case "create":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_step, $request_cmpr_method, $request_cmpr_level, $request_prefix, $request_tables;
				$main->include_main_blocks_2($this->module_name."_create.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					"_ROOT.make_backup_action"	=>	"$baseurl&action=create&step=2",
				));
				$tpl->assign(Array(
					"MSG_Tables_not_selected" => $this->_msg["Tables_not_selected"],
					"MSG_Gzip_method" => $this->_msg["Gzip_method"],
					"MSG_Without_gzip" => $this->_msg["Without_gzip"],
					"MSG_Gzip_level" => $this->_msg["Gzip_level"],
					"MSG_maximal" => $this->_msg["maximal"],
					"MSG_average" => $this->_msg["average"],
					"MSG_minimal" => $this->_msg["minimal"],
					"MSG_File_prefix" => $this->_msg["File_prefix"],
					"MSG_DB_backup_start" => $this->_msg["DB_backup_start"],
					"MSG_Table" => $this->_msg["Table"],
					"MSG_Filesize_Kb" => $this->_msg["Filesize_Kb"],
					"MSG_Rows" => $this->_msg["Rows"],
				));
				$tpl->assign_array('block_tables', $this->GetTabsInfo());
				if (2 == $request_step) {
					$this->SetCmpr($request_cmpr_method, $request_cmpr_level);
					$res = $this->CreateBackup($request_prefix, $request_tables);
					$tpl->newBlock('block_info');
					$tpl->assign('info', $this->_msg["Backup_size"] . ': '.$res["db_size"].' Mb<br />
					                      ' . $this->_msg["File_size"] . ' <b>'.$res["filename"].'</b>: '.$res["file_size"].' Mb<br />
					                      ' . $this->_msg["Tables_number"] . ': '.$res["tabs"].'<br />
					                      ' . $this->_msg["Rows_number"] . ': '.$res["records"]);

				}
				$this->main_title = $this->_msg["Create_backup_file"];
				break;


// Удалить файл
			case "delete":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_filename;
				$this->delete_file($CONFIG["backup_files_path"], $request_filename);
				header("Location: $baseurl&action=list");
				exit;
				break;


// Сделать восстановление БД из указанного файла
			case "restore":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_filename, $request_step;
				if ($request_filename) {
					// Восстановление БД, первый шаг
					if (!$request_step || $request_step == 1) {
						if ($request_filename && $lang && !strstr($request_filename, $lang)) $no_db_coincidence = $this->_msg["Attention_two_db"];
						$main->include_main_blocks_2($this->module_name."_restore_db.html", $this->tpl_path);
						$tpl->prepare();
						$tpl->assign(array(
							"_ROOT.no_db_coincidence"	=>	$no_db_coincidence,
							"_ROOT.form_action"	=>	"$baseurl&action=restore&step=2&filename=$request_filename",
							"_ROOT.file_download_link"	=>	"$baseurl&action=download&filename=$request_filename",
							"_ROOT.file_name"	=>	$request_filename,
							"_ROOT.file_time"	=>	date($CONFIG["backup_date_file_format"], filemtime($CONFIG["backup_files_path"] . $request_filename)),
						));
						$tpl->assign(Array(
							"MSG_Data_recovery_from_file" => $this->_msg["Data_recovery_from_file"],
							"MSG_restore_database_to" => $this->_msg["restore_database_to"],
							"MSG_backup_date" => $this->_msg["backup_date"],
							"MSG_All_data_lost" => $this->_msg["All_data_lost"],
							"MSG_Confirm_db_restore" => $this->_msg["Confirm_db_restore"],
							"MSG_Cancel" => $this->_msg["Cancel"],
							"MSG_Restore" => $this->_msg["Restore"],
						));
						$this->main_title = $this->_msg["DB_recovery"];
						break;
					// Восстановление категории, шаг второй
					} else if ($request_step == 2) {
						if ($this->Restore($request_filename)) {
							$cache->delete_all_cache_files();
						}
					}
				}
				header("Location: $baseurl&action=list");
				exit;
				break;


// Выгрузка резервного файла
			case "download":
				global $request_filename;
				$this->download_backup($CONFIG["backup_files_path"], $request_filename);
				exit;
				break;


// Закачка резервного файла с локальной машины
			case "upload":
				if ($_FILES['userfile']['name']) {
					$filename = $main->upload_file('userfile', 0, $CONFIG["backup_files_path"]);
				}
				header("Location: $baseurl&action=list");
				exit;
				break;

// Default...
			default:
				$main->include_main_blocks();
				$tpl->prepare();
			}
//--------------------------- конец обработки действий из админ части --------------------------//
		}
	}

	// функция для вывода списка файлов
	function show_files_list($path) {
		global $CONFIG, $tpl, $baseurl, $lang, $main;
		if (!is_dir($path)) return false;
		$files_in_dir = $main->files_list($CONFIG['backup_files_path'], 'files', 0);
		$i = 0;
		$tpl->newBlock('files');
		foreach($files_in_dir as $i=>$file) {
			if ($file != ".htaccess") {
				$tpl->newBlock('file_row');
				$size = sprintf ("%01.1f Kb", filesize($path.$file)/1024);
				$tpl->assign(array(
					'file_name'				=>	$file,
					'file_date'				=>	date($CONFIG["backup_date_file_format"], filemtime($path.$file)),
					'file_size'				=>	$size,
					'file_link'				=>	'/'.$path.$file,
					'file_download_link'		=>	$baseurl."&action=download&filename=$file",
					'file_del_link'			=>	$baseurl."&action=delete&filename=$file",
					'file_restore_db_link'	=>	$baseurl."&action=restore&filename=$file",
					"file_row.MSG_Confirm_delete_file" => $this->_msg["Confirm_delete_file"],
					"file_row.MSG_Restore" => $this->_msg["Restore"],
					"file_row.MSG_Delete" => $this->_msg["Delete"],
					'i'						=>	$i,
				));
			}
		}
	}


	// функция для удаления файла
	function delete_file($path, $filename) {
		$file = $path . $filename;
		if (file_exists($file)) {
			if (unlink($file)) return true;
		}
		return false;
	}


	// функция для загрузки резервного файла с сервера
	function download_backup($path, $filename) {
		global $CONFIG, $main;
		//Header("Content-type: application/zip");
		if (file_exists($path . $filename)) {
			header("Cache-control: private"); // fix for IE
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=$filename");
			readfile($path . $filename);
			return 1;
		} else {
			$main->message_die($filename . " - " . $this->_msg["This_file_doesnt_exist"]);
			return 0;
		}
	}

	/**
	 * Резервирование БД
	 * @param $tables array Список таблиц для резервирования.
	 *                      По умолчанию все таблицы
	 * @return boolean
	 */
	function CreateBackup($p_file_pref = "", $p_tables = array())
	{   
		global $CONFIG, $db;

		$is_safe_mode = ini_get('safe_mode') == '1' ? 1 : 0;
		if (!$is_safe_mode) set_time_limit(600);
		
		$p_file_pref = htmlspecialchars(trim($p_file_pref));

	    // Определение списка резервируемых таблиц
		$tables = array();
        $tbl_list = $this->GetTabsInfo($p_tables);
		$tabs = count($tbl_list);
		$tabinfo = array();
		$tabinfo[0] = 0;
		$info = '';
		foreach ($tbl_list as $key=>$tab) {
			$tables[] = $tab['table'];
			$tabinfo[0] += $tab['rows'];
			$tabinfo[$tab['table']] = $tab['rows'];
			$this->size += $tab['data_len'];
			$tabsize[$tab['table']] = 1 + round($CONFIG['backup_limit'] * 1048576 / ($tab['avg'] + 1));
			if($tab['rows']) {
				$info .= "|" . $tab['rows'];
			}
		}

		$show = 10 + $tabinfo[0] / 50;
		$info = $tabinfo[0] . $info;

		$name = $p_file_pref . SITE_PREFIX . date($CONFIG['backup_date_format_in_name']);
        $fp = $this->FileOpen($name, "w");
		$this->FileWrite($fp, "#ABOCMS|{$tabs}|" . date("Y.m.d H:i:s") ."|{$info}\n\n");

		$t=0;
		$db->query("SET SQL_QUOTE_SHOW_CREATE = 1");
        foreach ($tables AS $key=>$table) {
        	// Создание таблицы
        	$table_out = str_replace(SITE_PREFIX, '{site_prefix}', $table);
			$db->query("SHOW CREATE TABLE `{$table}`");
			$db->next_record();
			$tab = array_values($db->Record);
			$patt = array(
			            '/(default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP|DEFAULT CHARSET=\w+|character set \w+|collate \w+)/i',
			            '/'.SITE_PREFIX.'/'
			        );
			$repl = array('/*!40101 \\1 */', '{site_prefix}');
			$tab = preg_replace($patt, $repl, $db->f('Create Table'));
        	$this->FileWrite($fp, "DROP TABLE IF EXISTS `{$table_out}`;\n{$tab};\n\n");
        	// Опредеделяем типы столбцов
            $NumericColumn = $DefColumn =  array();
            $db->query("SHOW COLUMNS FROM `{$table}`");
            $field = 0;
            while($db->next_record()) {
            	$NumericColumn[$field] = preg_match("/^(\w*int|year)/", $db->f('Type')) ? 1 : 0;
            	$DefColumn[$field] = $db->f('Default');
            	$NullColumn[$field] = $db->f('Null');
            	$field++;
            }
			$fields = $field;
            $from = 0;
			$limit = $tabsize[$table];
			$limit2 = round($limit / 3);
			if ($tabinfo[$table] > 0) {
				$i = 0;
				$this->FileWrite($fp, "INSERT INTO `{$table_out}` VALUES");
   		        while(($db->query("SELECT * FROM `{$table}` LIMIT {$from}, {$limit}")) && $total = $db->nf()) {
   	    	    	while($db->next_record()) {
   	    	    		$row = $db->Record;
  		               	$i++;
    					$t++;
    					$vals = '';
						for($k = 0; $k < $fields; $k++) {
                    		if ($NumericColumn[$k]) {
                    		    $vals .= isset($row[$k]) ? $row[$k].", " : ($NullColumn[$k] ? "NULL" : $DefColumn[$k]).", ";
                    		} else {
                    			$vals .= isset($row[$k]) ? "'" . mysql_real_escape_string ($row[$k]) . "', " : ($NullColumn[$k] ? "NULL" : "'".mysql_real_escape_string ($DefColumn[$k])."'").", ";
	                   		}
						}
	   					$this->FileWrite($fp, ($i == 1 ? "" : ",") . "\n(" . substr($vals, 0, -2) . ")");
   	    	    	}
               		$db->free();
					if ($total < $limit) {
					   	break;
					}
    				$from += $limit;
            	}
				$this->FileWrite($fp, ";\n\n");
			}
		}
		$this->tabs = $tabs;
		$this->records = $tabinfo[0];
		$this->comp = $this->cmpr_method * 10 + $this->cmpr_level;

        $this->FileClose($fp);

        $ret = array(
        	'tabs' => $tabs,
        	'records' => $tabinfo[0],
        	'db_size' => round($this->size / 1048576, 2),
        	'file_size' => round(filesize(RP.$CONFIG['backup_files_path'].$this->filename) / 1048576, 2),
        	'filename' => $this->filename
        );

		return $ret;
	}

	/**
	 * Восстановить дамп
	 * @param $file string Имя файла
	 * @return boolean
	 */
	function Restore($file)
	{   global $CONFIG, $db;

		$is_safe_mode = ini_get('safe_mode') == '1' ? 1 : 0;
		if (!$is_safe_mode) set_time_limit(600);

		preg_match("/^(\d+)\.(\d+)\.(\d+)/", mysql_get_server_info(), $m);
		$mysql_ver = sprintf("%d%02d%02d", $m[1], $m[2], $m[3]);
		$ret = array();
		// Определение формата файла
		if (preg_match("/^(.+?)\.sql(\.(bz2|gz))?$/", $file, $matches)) {
			if (isset($matches[3]) && $matches[3] == 'bz2') {
			    $this->cmpr_method = 2;
			} elseif (isset($matches[2]) && $matches[3] == 'gz'){
				$this->cmpr_method = 1;
			} else{
				$this->cmpr_method = 0;
			}
			$this->cmpr_level = '';
			if (!file_exists(RP.$CONFIG['backup_files_path'] . $file)) {
				$ret = array('error' => $file . " - " . $this->_msg["File_not_found"]);
    		    return $ret;
    		}
			$file = $matches[1];
		} else {
			$ret = array('error' => $file . " - " . $this->_msg["Wrong_format_filename"]);
			return $ret;
		}

		$fp = $this->FileOpen($file, "r");
		$this->file_cache = $sql = $table = $insert = '';
        $is_skd = $query_len = $execute = $q =$t = $i = $aff_rows = 0;
		$limit = 300;
        $index = 4;
		$tabs = 0;
		$cache = '';
		$info = array();
		while(($str = $this->FileReadStr($fp)) !== false){
			if (empty($str) || preg_match("/^(#|--)/", $str)) {
				if (!$is_skd && preg_match("/^#ABOCMS\|/", $str)) {
				    $info = explode("|", $str);
					$is_skd = 1;
				}
        	    continue;
        	}
			$query_len += strlen($str);

			if (!$insert && preg_match("/^(INSERT INTO `?([^` ]+)`? .*?VALUES)(.*)$/i", $str, $m)) {
				if ($table != $m[2]) {
				    $table = $m[2];
					$tabs++;
					$i = 0;
				}
        	    $insert = str_replace('{site_prefix}', SITE_PREFIX, $m[1]) . ' ';
				$sql .= $m[3];
				$index++;
				$info[$index] = isset($info[$index]) ? $info[$index] : 0;
		       	    continue;
        	}
			$query_len += strlen($str);

			if (!$insert && preg_match("/^(INSERT INTO `?([^` ]+)`? .*?VALUES)(.*)$/i", $str, $m)) {
				if ($table != $m[2]) {
				    $table = $m[2];
					$tabs++;
					$i = 0;
				}
        	    $insert = str_replace('{site_prefix}', SITE_PREFIX, $m[1]) . ' ';
				$sql .= $m[3];
				$index++;
				$info[$index] = isset($info[$index]) ? $info[$index] : 0;
				$limit = round($info[$index] / 20);
				$limit = $limit < 300 ? 300 : $limit;
				if ($info[$index] > $limit){
					$cache = '';
				}
        	} else {
        		if (preg_match("/^CREATE TABLE/i", $str) || preg_match("/^DROP TABLE/i", $str) || preg_match("/UNION/i", $str) || preg_match("/TRUNCATE TABLE/i", $str)) {
        			$str = str_replace('{site_prefix}', SITE_PREFIX, $str);
        			//echo $str."<hr>";
        		}
        		$sql .= $str;
				if ($insert) {
				    $i++;
    				$t++;
				}
        	}

			if (!$insert && preg_match("/^CREATE TABLE (IF NOT EXISTS )?`?([^` ]+)`?/i", $str, $m) && $table != $m[2]){
				$table = $m[2];
				$insert = '';
				$tabs++;
				$i = 0;
			}
			if ($sql) {
			    if (preg_match("/;$/", $str)) {
            		$sql = rtrim($insert . $sql, ";");
					if (empty($insert)) {
						if ($mysql_ver < 40101) {
				    		$sql = preg_replace("/ENGINE\s?=/", "TYPE=", $sql);
						}
					}
            		$insert = '';
            	    $execute = 1;
            	}
            	if ($query_len >= 65536 && preg_match("/,$/", $str)) {
            		$sql = rtrim($insert . $sql, ",");
            	    $execute = 1;
            	}
    			if ($execute) {
            		$q++;
            		if (!$db->query($sql)) {
            			$ret = array('error' => $this->_msg["SQL_query_error"]);
            			return $ret;
            		}
					if (preg_match("/^insert/i", $sql)) {
            		    $aff_rows += mysql_affected_rows();
            		}
            		$sql = '';
            		$query_len = 0;
            		$execute = 0;
            	}
			}
		}
		$this->FileClose($fp);

		$ret = array(
			'query_cnt' => $q,
			'tabs_cnt' => $tabs,
			'row_cnt' => $aff_rows
		);
		return $ret;
		exit;
	}


	function FileOpen($name, $mode)
	{   global $CONFIG;

		if ($this->cmpr_method == 2) {
			$this->filename = "{$name}.sql.bz2";
		    return bzopen(RP.$CONFIG['backup_files_path'] . $this->filename, "{$mode}b{$this->cmpr_level}");
		}
		elseif ($this->cmpr_method == 1) {
			$this->filename = "{$name}.sql.gz";
		    return gzopen(RP.$CONFIG['backup_files_path'] . $this->filename, "{$mode}b{$this->cmpr_level}");
		}
		else{
			$this->filename = "{$name}.sql";
			return fopen(RP.$CONFIG['backup_files_path'] . $this->filename, "{$mode}b");
		}
	}

	function FileWrite($fp, $str)
	{
		if ($this->cmpr_method == 2) {
		    bzwrite($fp, $str);
		}
		elseif ($this->cmpr_method == 1) {
		    gzwrite($fp, $str);
		}
		else{
			fwrite($fp, $str);
		}
	}

	function FileRead($fp)
	{
		if ($this->cmpr_method == 2) {
		    return bzread($fp, 4096);
		}
		elseif ($this->cmpr_method == 1) {
		    return gzread($fp, 4096);
		}
		else{
			return fread($fp, 4096);
		}
	}

	function FileReadStr($fp)
	{
		$string = '';
		$this->file_cache = ltrim($this->file_cache);
		$pos = strpos($this->file_cache, "\n", 0);
		if ($pos < 1) {
			while (!$string && ($str = $this->FileRead($fp))){
    			$pos = strpos($str, "\n", 0);
    			if ($pos === false) {
    			    $this->file_cache .= $str;
    			}
    			else{
    				$string = $this->file_cache . substr($str, 0, $pos);
    				$this->file_cache = substr($str, $pos + 1);
    			}
    		}
			if (!$str) {
			    if ($this->file_cache) {
					$string = $this->file_cache;
					$this->file_cache = '';
				    return trim($string);
				}
			    return false;
			}
		}
		else {
  			$string = substr($this->file_cache, 0, $pos);
  			$this->file_cache = substr($this->file_cache, $pos + 1);
		}
		return trim($string);
	}

	function FileClose($fp)
	{   global $CONFIG;
		if ($this->cmpr_method == 2) {
		    bzclose($fp);
		} elseif ($this->cmpr_method == 1) {
		    gzclose($fp);
		} else {
			fclose($fp);
		}
		@chmod(RP.$CONFIG['backup_files_path'] . $this->filename, 0666);
	}

	/**
	 * Установить метод и степень сжатия
	 * @param int $method
	 * @param int $level
	 * @return mixed
	 */
	function SetCmpr($method, $level)
	{
		$this->cmpr_method = 0 == $method ? 0 : 1;
		$this->cmpr_level = !$this->cmpr_method ? 0 : ($level >= 0 && $level <= 9 ? $level : 0);
	}

	/**
	 * Получить информацию о таблицах
	 * @param array $p_tables Список таблиц о которых будет выдани информация
	 *                        Default - все
	 * @return array
	 */
	function GetTabsInfo($p_tables = array())
	{   global $CONFIG, $db;

		$ret = array();

		$db->query("SHOW TABLE STATUS");
		while($db->next_record()) {
			$name_f = $db->f('Name');
			if (empty($p_tables) || in_array($name_f, $p_tables)) {
				$except = 0;
        		foreach($CONFIG['backup_except_tables'] as $key=>$table) {
        			if (FALSE !== strpos($name_f, $table)) {
        				$except = 1;
        				break;
        			}
        		}
        		if (!$except) {
        			$ret[] = array(
        				'table' => $name_f,
        				'size' => number_format(($db->f('Data_length') + $db->f('Index_length'))/1024, 2, ".", " "),
        				'data_len' => $db->f('Data_length'),
        				'rows' => $db->f('Rows'),
        				'avg' => $db->f('Avg_row_length')
        			);
        		}
			}
		}

		return $ret;

	}
	
	function getTables($add_mask = false, $del_mask = false)
	{   global $db;
		static $tbls = false;
		
		$ret = false;
		if (!$tbls) {
		    $db->query("SHOW TABLES");
		    if (! $db->nf()) {
		        return false;
		    }
		    while ($db->next_record()) {
		    	$tbls[] = $db->Record[0];
		    }
		}
		if(is_array($tbls)){
		    if ($add_mask || $del_mask) {
		        foreach($tbls as $v){
    		        if ($add_mask && preg_match("~$add_mask~i", $v)) {
    		            if ( ($del_mask && !preg_match("~$del_mask~i", $v)) || !$del_mask) {
        		            $ret[] = $v;
        		        }
    		        } else if (!$add_mask && $del_mask && !preg_match("~$del_mask~i", $v)) {
    		            $ret[] = $v;
    		        }
    		    }
		    } else {
		        $ret = $tbls;
		    }
		}
		return $ret;
	}

}

?>