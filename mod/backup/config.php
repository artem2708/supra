<?php // Vers 5.8.2 27.06.2012
/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
			  'backup_files_path'			=> array(
			  									 'type'		=> 'string',
												 'defval'	=> 'admin/backup/',
												 'descr'	=> array(	'rus' => 'Путь к папке с резервными копиями базы данных',
												 						'eng' => 'Path to a folder with backup copies of a database'),
												 'access'	=> 'readonly',
												 ),

			  'backup_date_format_in_name'	=> array(
			  									 'type'		=> 'string',
			   									 'defval'	=> '_d.m.Y_G.i.s',
												 'descr'	=> array(	'rus' => 'Формат даты добавляемый в название backup файла',
												 						'eng' => 'Format of date added in the name backup a file'),
			   									 'access'	=> 'readonly'
			   									 ),

			   'backup_date_file_format' 	=> array(
			   									 'type'		=> 'select',
			   									 'defval'	=> array('d/m/Y G:i:s'	=> array(	'rus' => 'День/Месяц/Год Часы:Минуты:Секунды',
												 												'eng' => 'Day/Month/Year Hours:Minutes:Seconds'),
			   									 					 'd/m/Y'		=> array(	'rus' => 'День/Месяц/Год',
												 												'eng' => 'Day/Month/Year'),
			   									 					 'Y/m/d'		=> array(	'rus' => 'Год/Месяц/День',
												 												'eng' => 'Year/Month/Day'),
			   									 					 'Y/m/d G:i:s'	=> array(	'rus' => 'Год/Месяц/День Часы:Минуты:Секунды',
												 												'eng' => 'Year/Month/Day Hours:Minutes:Seconds'),
			   									 					 ),

			   									 'descr'	=> array(	'rus' => 'Формат выводимой даты создания файла',
												 						'eng' => 'Format of deduced date of creation of a file'),
			   									 'access'	=> 'editable'
			   									 ),
			   );

$OPEN_NEW = array(
	// если в названии таблицы встречаются слова из
	'backup_except_tables'	=> array('search', 'modules', 'counter_all'),
	// Путь к папке резервными копиями базы данных
	'backup_files_path'		=> 'admin/backup/',
	// Ограничение размера данных доставаемых за одно обращения к БД (в мегабайтах)
	// Нужно для ограничения количества памяти пожираемой сервером при дампе очень объемных таблиц
	'backup_limit'			=> 1
);

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'backup_files_path' => 'admin/backup/',
'backup_date_format_in_name' => '_d.m.Y_G.i.s',
'backup_date_file_format' => 'd/m/Y G:i:s',
/* ABOCMS:END */
);
?>