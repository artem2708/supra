<?php // Vers 5.8.2 27.06.2012

/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
			  'siteusers_order_by'		=> array('type'		=> 'select',
												 'defval'	=> array('id DESC'	=> array(	'rus' => 'в порядке обратном добавлению',
												 											'eng' => 'reverce adding order'),
												 					 'id'		=> array(	'rus' => 'в порядке добавления',
												 											'eng' => 'adding order'),
												 					 ),
												 'descr'	=> array(	'rus' => 'Порядок вывода записей',
												 						'eng' => 'Show records order'),
												 'access'	=> 'editable',
												 ),

			   'siteusers_max_rows'		=> array('type'		=> 'integer',
			   									 'defval'	=> '10',
			   									 'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу по умолчанию',
												 						'eng' => 'default maximum quantity of records on page'),
			   									 'access'	=> 'editable'
			   									 ),

			   'siteusers_recipient' 	=> array('type'		=> 'string',
			   									 'defval'	=> 'admin@cms.ru',
			   									 'descr'	=> array(	'rus' => 'E-mail получателя извещения о новом пользователе на сайте',
					 													'eng' => 'E-mail the addressee of the notice on a new site user'),
			   									 'access'	=> 'editable'
			   									 ),

	 			'siteusers_img_max_width'		=> array('type'		=> 'integer',
				   								 'defval'	=> '100',
				   								 'descr'	=> array(	'rus' => 'Максимальная ширина изображения пользователя',
					 													'eng' => 'Maximum width of user picture'),
				   								 'access'	=> 'editable'
			   									 ),

	 			'siteusers_img_max_height'		=> array('type'		=> 'integer',
			   									 'defval'	=> '100',
			   									 'descr'	=> array(	'rus' => 'Максимальная высота изображения пользователя',
					 													'eng' => 'Maximum height of user picture'),
			   									 'access'	=> 'editable'
			   									 ),

				'siteusers_img_max_size'		=> array('type'		=> 'integer',
			   									 'defval'	=> '10',
			   									 'descr'	=> array(	'rus' => 'Максимальный размер изображения пользователя(Кб)',
					 													'eng' => 'Maximum size of user picture(Kb)'),
			   									 'access'	=> 'editable'
			   									 ),

				'siteusers_captcha'				=> array('type'		=> 'checkbox',
			   									 'defval'	=> 'off',
			   									 'descr'	=> array(	'rus' => 'Добавить CAPTCHA-проверку',
					 													'eng' => 'Add CAPTCHA-verification'),
			   									 'access'	=> 'editable'
			   								 	 ),

				'siteusers_generate_XML'		=> array('type'		=> 'checkbox',
				   								 'defval'	=> '',
				   								 'descr'	=> array(	'rus' => 'Создавать XML выгрузки при добавлении/изменении пользователя',
					 													'eng' => 'To create XML unloadings at addition/change of the user'),
				   								 'access'	=> 'editable'
				   								 ),

				'siteusers_register_auto'		=> array('type'		=> 'checkbox',
				   								 'defval'	=> '',
				   								 'descr'	=> array(	'rus' => 'Регистрация через уведомление по почте',
					 													'eng' => 'Registration through e-mail notice'),
				   								 'access'	=> 'editable'
				   								 ),

				'siteusers_subscribe_group_id'	=> array('type'		=> 'integer',
			   									 'defval'	=> '15',
			   									 'descr'	=> array(	'rus' => 'Идентификатор группы рассылки (список через запятую или all_categs для подписки во все группы рассылок)',
					 													'eng' => 'The identifier of group of dispatch (the list through a comma or all_categs for a subscription in all groups of dispatches)'),
			   									 'access'	=> 'editable'
												),

				'siteusers_input_admin'			=> array('type'		=> 'checkbox',
			   									 'defval'	=> 'checked',
			   									 'descr'	=> array(	'rus' => 'Возможность входа без авторизации в административный раздел после авторизации на сайте',
												 						'eng' => 'Input in administrative section'),
			   									 'access'	=> 'editable'
												),

				'siteusers_show_panel'		=> array('type'		=> 'checkbox',
			   									 'defval'	=> 'checked',
			   									 'descr'	=> array(	'rus' => 'Отображать/неотображать административную панель',
												 						'eng' => 'Отображать/неотображать административную панель'),
			   									 'access'	=> 'editable'
												),

				'siteusers_reg_type'			=> array('type'		=> 'select',
												 'defval'	=> array('admin'		=> array(	'rus' => 'регистрация администратором',
												 												'eng' => 'administartor registration'),
												 					 'email_confirm'=> array(	'rus' => 'регистрация с подтвержением по e-mail',
												 												'eng' => 'registration e-mail confirmation'),
												 					 ),
												 'descr'	=> array(	'rus' => 'Тип регистрации пользователей на сайте',
												 						'eng' => 'User registration type on site'),
												 'access'	=> 'editable',
												 ),

				'siteusers_policy'			=> array('type'		=> 'select',
												 'defval'	=> array('box'	=> array(	'rus' => 'вся коробка',
												 										'eng' => 'all box'),
												 					 'site'	=> array(	'rus' => 'сайт',
												 										'eng' => 'site'),
												 					 ),
												 'descr'	=> array(	'rus' => 'Политика пользователей (сфера деятельности)',
												 						'eng' => 'Policy of users (field of activity)'),
												 'access'	=> 'editable',
												 ),
				'siteusers_group'			=> array('type'		=> 'select',
												 'defval'	=> (class_exists('Siteusers')) ? Siteusers::GetAllGroup() : array (),
												 'descr'	=> array(	'rus' => 'Группа пользователей по умолчанию',
												 						'eng' => 'Default group for users'),
												 'access'	=> 'editable',
												 ),
//				'siteusers_blog_max_rows'		=> array('type'		=> 'integer',
//			   									 'defval'	=> '10',
//												 'descr'	=> array(	'rus' => 'Максимальное количество записей блогов на страницу',
//												 						'eng' => 'Maximum blogs records quantity on page'),
//			   									 'access'	=> 'editable'
//			   									 ),
//
//				'siteusers_blog_msgs_max_rows'		=> array('type'		=> 'integer',
//			   									 'defval'	=> '10',
//												 'descr'	=> array(	'rus' => 'Максимальное количество комментариев блога на страницу',
//												 						'eng' => 'Maximum blogs comments quantity on page'),
//			   									 'access'	=> 'editable'
//			   									 ),
//
//	 			'siteusers_blog_img_max_size'		=> array('type'		=> 'integer',
//			   									 'defval'	=> '300',
//												 'descr'	=> array(	'rus' => 'Максимальный размер прикрепляемого к блогу изображения(Кб)',
//												 						'eng' => 'Maximum blog attachment picture size(Kb)'),
//			   									 'access'	=> 'editable'
//				   								 ),
//
//	 			'siteusers_blog_img_max_width'	=> array('type'		=> 'integer',
//				   								 'defval'	=> '400',
//												 'descr'	=> array(	'rus' => 'Сжимать по ширине прикреплённое к блогу изображения до(пикселей)',
//												 						'eng' => 'Resize width blog attachment picture to (pixels)'),
//				   								 'access'	=> 'editable'
//			   									 ),
//
//	 			'siteusers_blog_img_max_height'	=> array('type'		=> 'integer',
//			   									 'defval'	=> '400',
//												 'descr'	=> array(	'rus' => 'Сжимать по высоте прикреплённое к блогу изображения до(пикселей)',
//												 						'eng' => 'Resize height blog attachment picture to (pixels)'),
//			   									 'access'	=> 'editable'
//			   									 ),
//
//	 			'siteusers_blog_preview_max_width'	=> array('type'		=> 'integer',
//				   								 'defval'	=> '100',
//												 'descr'	=> array(	'rus' => 'Максимальная ширина предварительного изображения прикреплённого к блогу',
//												 						'eng' => 'Maximum blog preview picture width'),
//				   								 'access'	=> 'editable'
//			   									 ),
//
//	 			'siteusers_blog_preview_max_height'	=> array('type'		=> 'integer',
//			   									 'defval'	=> '100',
//												 'descr'	=> array(	'rus' => 'Максимальная высота предварительного изображения прикреплённого к блогу',
//												 						'eng' => 'Maximum blog preview picture height'),
//			   									 'access'	=> 'editable'
//			   									 ),

			   );

$CONFIG["siteusers_captcha_width"] = 110;
$CONFIG["siteusers_captcha_height"] = 32;
$CONFIG["siteusers_captcha_secretword"] = "SecRetABOCMSwORd";
$CONFIG["siteusers_captcha_fontsize"] = 16;
$CONFIG["siteusers_captcha_fontdepth"] = 2;
$CONFIG["siteusers_captcha_symbols"] = "0123456789";
$CONFIG["siteusers_captcha_wordlength"] = 5;
$CONFIG["siteusers_captcha_fonts"] = array(
	$_SERVER["DOCUMENT_ROOT"] . "/inc/captcha/arialbd.ttf",
	$_SERVER["DOCUMENT_ROOT"] . "/inc/captcha/comicbd.ttf",
	$_SERVER["DOCUMENT_ROOT"] . "/inc/captcha/courbd.ttf",
	$_SERVER["DOCUMENT_ROOT"] . "/inc/captcha/timesbd.ttf",
);
$CONFIG["siteusers_captcha_bgcolor"] = array(98, 118, 144);
$CONFIG["siteusers_captcha_color"]	= array(rand(190, 210), rand(200, 220), rand(30, 50));
$CONFIG["siteusers_captcha_color2"] = array(rand(150, 170), rand(50, 70), rand(140, 160));
$CONFIG['siteusers_export_path']	= 'admin/exchange/'.SITE_PREFIX.'/out/';		// путь для выгрузки данных
$CONFIG['siteusers_import_path']	= 'admin/exchange/'.SITE_PREFIX.'/in/';		// путь для загрузки данных

$OPEN_NEW = array (
	"siteusers_client_types"	=> array("1" => "Розничный покупатель",	// массив типов клиента
									 	 "2" => "Партнёр",
									 	 "3" => "Дилер",
									 	 "4" => "Дилер 2",
									 	 "5" => "Дилер 3"),
	"siteusers_default_client_type"		=> 1,	// тип пользователя по умолчанию
	"siteusers_default_admin_client_type"		=> 2,
	"siteusers_img_path"				=> $CONFIG['files_upload_path'].'siteusers/'.SITE_PREFIX.'/',	// Путь к картинкам новостей
	"siteusers_blog_img_path"				=> $CONFIG['files_upload_path'].'siteusers/'.SITE_PREFIX.'/',	// Путь к картинкам новостей
	"siteusers_max_login_len"			=> 20,	// Max длина логина
	"siteusers_min_login_len"			=> 4,	// Min длина логина
	"siteusers_max_pass_len"			=> 9,	// Max длина пароля
	"siteusers_min_pass_len"			=> 4,	// Min длина пароля
	"siteusers_prop_types"     			=> array(
	    1 => 'Строка',
	    2 => 'Текст',
	    3 => 'Флаг',
	    4 => 'Дата',
	    5 => 'Список',
	    6 => 'Выпадающий список',
	),
	"siteusers_reserv_prop_unames"     	=> array('email', 'username', 'active', 'is_new', 'client_type', 'uid'),
);

if(!is_dir(RP.'mod/blogs')){
    foreach($TYPES_NEW as $k => $v){if(false!==strpos($k,'siteusers_blog')){unset($TYPES_NEW[$k]);}}
}

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'siteusers_order_by' => 'id DESC',
'siteusers_max_rows' => '10',
'siteusers_recipient' => 'supra@supra.ru, timofeeva@deltael.ru, omutkova@deltael.ru',
'siteusers_img_max_width' => '500',
'siteusers_img_max_height' => '500',
'siteusers_img_max_size' => '5000',
'siteusers_captcha' => 'on',
'siteusers_generate_XML' => '',
'siteusers_register_auto' => 'on',
'siteusers_subscribe_group_id' => '15',
'siteusers_input_admin' => 'on',
'siteusers_show_panel' => 'on',
'siteusers_reg_type' => 'email_confirm',
'siteusers_policy' => 'box',
'siteusers_group' => '2',
/* ABOCMS:END */
);
?>