<?php
require_once(RP.'/inc/class.Mailer.php');
if(file_exists(RP.'mod/subscription/lib/class.Subscription.php')){
    require_once(RP.'mod/subscription/lib/class.Subscription.php');
}
require_once(RP.'/inc/class.TreeMenu.php');

class SiteusersPrototype extends Module {

    var $main_title;
    var $table_prefix;
    var $addition_to_path;
    var $require_authorization		= 1;
    var $is_authorized				= FALSE;
    var $module_name				= 'siteusers';
    var $default_action				= 'loginform';
    var $admin_default_action		= 'siteusers';
    var $tpl_path					= 'mod/siteusers/tpl/';
    var $block_module_actions		= array();
    var $block_main_module_actions	= array();
    var $allow_img_typs				= array('image/gif', 'image/jpeg', 'image/png', 'image/pjpeg', 'image/x-png');
    var $default_client_type = 1;
    var $data_charset				= 'utf-8'; // cp1251, koi8-r, iso-8859-1, utf-8
    var $onsite_min = 15;
    var $action_tpl = array(
        'loginform' => 'siteusers_loginform.html',
        'userinfo' => 'siteusers_userinfo.html',
        'usernav' => 'siteusers_usernav.html',
        'welcome' => 'siteusers_welcome.html',
        'registration' => 'siteusers_registration.html',
    );

    function SiteusersPrototype($action = "only_create_object", $transurl = "", $properties = array(), $prefix = NULL, $rewrite_mod_params = NULL) {
        global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $core, $server, $lang, $permissions;
        // проверить авторизован ли пользователь

        if ($_SESSION['siteuser']['is_authorized']) {
            $this->is_authorized = true;
        } else {
            $this->is_authorized = false;
        }

        /* Get $action, $tpl_name, $permissions */
        extract( $this->init(array(
            'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
            'action' => $action, 'tpl_name' => $properties['tpl']
        )), EXTR_OVERWRITE);

        $this->default_client_type = $CONFIG["siteusers_default_client_type"];

        if (!$rows) $rows = $CONFIG["siteusers_max_rows"];
        $start = 1;													// показывать, начиная со страницы

        $this->updateOnSite();

        if ('only_create_object' == $action) {
            return;
        }

        if ($this->actionExists($action)) {
            $this->doAction($action);
            return;
        }

        if (! self::is_admin()) {

//---------------------------------- обработка действий с сайта --------------------------------//
            if ($rewrite_mod_params && $CONFIG['rewrite_mod']) {
                $username = explode("/", $rewrite_mod_params);
                $username = $username[0];
                $user_id = $this->get_siteuser_by_siteusername($username, 'username');
                if ($user_id) {
                    $PAGE['current_user_id'] = $user_id;
                    $PAGE['current_username'] = $username;
                    $PAGE['process_params'] = true;
                }
                $this->addition_to_path = array($username => '');
            }
            if ('edit'==$action && $this->is_authorized) {
                $PAGE['current_user_id'] = $_SESSION['siteuser']['id'];
            }
            if (!isset($_REQUEST['action']) && $PAGE['current_user_id'] && 0==$PAGE['field']) {
                global $request_siteuser;
                $request_siteuser = $PAGE['current_username'];
                $action = 'userinfo';
                $tpl_name = $this->checkTplName($tpl_name, $action);
            }
            switch ($action) {
                // Регистрация на сайте
                case "registration":
                    global $request_step, $request_email, $request_captcha, $request_siteusername;
                    $result_message = "";
                    if($_SERVER['SERVER_NAME'] == "supra.ru")
                    {
                        $_REQUEST['groups']="dilers";
                        $_REQUEST['siteusername']=$request_email;
                        $request_siteusername=$request_email;

                    }
                    else
                    {
                        $_REQUEST['siteusername']=md5($request_email);
                        $request_siteusername=md5($request_email);
                    }
                    if ($this->is_authorized) {
                        header('Location: '.$transurl.'&action=edit');
                    }
                    // Добавление/обновление записи, первый шаг
                    $assign = array(
                        'action' => $action,
                        'baseurl' => $baseurl,
                        'transurl' => $transurl,
                    );
                    if (!$request_step || $request_step == 1) {
                        $main->include_main_blocks($this->module_name."_registration.html", "main");
                        $tpl->prepare();

                        $this->show_registration_form(true, 'block_form', $assign);
                        // Добавление/обновление записи, шаг второй
                    } else if ($request_step == 2) {
                        $main->include_main_blocks($this->module_name."_registration.html", "main");
                        $tpl->prepare();

                        if ($this->get_siteuser_by_siteusername($request_email)) {
                            $this->show_registration_form(true, 'block_form', $assign);
                            $tpl->newBlock('block_username_busy');
                        } elseif ($this->get_siteuser_by_siteusername($request_siteusername, 'username')) {

                            $this->show_registration_form(true, 'block_form', $assign);
                            $tpl->newBlock('block_username_busy_nick');
                        } else {

                            if($CONFIG["siteusers_captcha"] == "on" && (!$request_captcha || $request_captcha != $_SESSION[$CONFIG["siteusers_captcha_secretword"]]))
                            {

                                unset($_SESSION[$CONFIG["siteusers_captcha_secretword"]]);
                                $tpl->newBlock("block_errorcode");
                                $this->show_registration_form(true, 'block_form', $assign);
                            } else {

                                if ($this->add_siteuser()) {
                                    $tpl->newBlock('block_registration_ok_'.$CONFIG["siteusers_reg_type"]);
                                } else {
                                    $this->show_registration_form(true, 'block_form', $assign);
                                    $tpl->newBlock('block_input_false');
                                }
                            }
                        }
                    }

                    break;

                // Подтверждение регистрации
                case "confirm":
                    global $request_uid;
                    $main->include_main_blocks($this->module_name.'_confirm_message.html', 'main');
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Registation_confirmed" => $this->_msg["Registation_confirmed"],
                        "MSG_Fail_confirm_registration" => $this->_msg["Fail_confirm_registration"],
                    ));
                    if ($request_uid) {
                        $subscr = $this->confirm_registration($request_uid);
                        if ($subscr) {
                            $tpl->newBlock('block_confirm_1');
                        } else {
                            $tpl->newBlock('block_confirm_2');
                        }
                    }
                    break;

                // Смена пароля
                case "changepsw":
                    global $request_uid;
                    $main->include_main_blocks($this->module_name.'_changepsw.html', 'main');
                    $tpl->prepare();

                    if ($this->is_authorized) {

                        $show_form = 1;
                        $changed = 1;
                        if ($_POST['password'] && $_POST['new_password'] && $_POST['new_password'] ) {
                            $changed = $this->changePsw($_SESSION['siteuser']['id'], $_POST['password'], $_POST['new_password'], $_POST['new_password']);
                            if (1 == $changed) {
                                $tpl->newBlock('block_ok');
                                $show_form = 0;
                            }
                        }
                        if ($show_form) {
                            $tpl->newBlock('block_form');
                            $tpl->assign(array(
                                'baseurl' => $baseurl,
                                'action' => $action,
                            ));
                        }
                        if (1 != $changed) {
                            $tpl->newBlock('block_err');
                        }
                    } else {
                        $tpl->newBlock('block_access_deny');
                    }

                    break;

                // Редактирование личных данных
                case "edit":
                    global $request_step, $request_siteusername;
                    $result_message = "";
                    $assign = array(
                        'action' => $action,
                        'baseurl' => $baseurl,
                        'transurl' => $transurl,
                    );
                    // Обновление записи, первый шаг
                    if (!$request_step || $request_step == 1) {


                        $main->include_main_blocks($this->module_name."_edit.html", "main");
                        $tpl->prepare();
                        if ($this->is_authorized) {
                        $this->show_registration_form(false, 'block_form', $assign);
                        if($this->isDiler())
                        {
                            $tpl->assign(array('class'=>"dilerEdit"));
                        }
                        }
                        else $tpl->newBlock('block_access_deny');
                        // Обновление записи, шаг второй
                    } else if ($request_step == 2) {
                        $main->include_main_blocks($this->module_name."_edit.html", "main");
                        $tpl->prepare();

                        // если пользователь авторизован
                        if ($this->is_authorized) {
                            if ($this->update_siteuser($err)) {
                                $this->show_registration_form(false, 'block_form', $assign);
                                $tpl->newBlock('block_edit_ok');
                                if(!$this->isDiler())
                                {
                                    $this->updateUserAddress();
                                }

                            } else {
                                $this->show_registration_form(false, 'block_form', $assign);
                                $tpl->newBlock('block_input_false');
                            }

                        } else {
                            $tpl->newBlock('block_access_deny');
                        }
                    }
                    if ($this->is_authorized){
                        $allAddress=$this->getAllAddress($_SESSION["siteuser"]["id"]);
                        if(!empty($allAddress))
                        {
                            $attr="";
                            $tpl->newBlock("block_selectAddress");
                            $selectAddress='<select name="selectAddress" id="selectAddress"><option value="add">Добавить</option>';
                            $divHidden="";
                            foreach($allAddress as $val)
                            {
                                $name=$val['name']." ".$val['surname']." ".$val['lastname']." ".$val['phone']." ".$val['area']." ".$val['region']." ".$val['index']." ".$val['city']." ".$val['address'];
                                if($val['primary'])
                                {
                                    $attr="selected=selected";
                                    $ini=$val['id'];
                                }
                                $selectAddress=$selectAddress.'<option value="'.$val['id'].'" '.$attr.' >'.$name.'</option>';
                                $divHidden=$divHidden."<div id='select".$val['id']."' style='display:none'>
                                <span name='name'>".$val['name']."</span>
                                <span name='surname'>".$val['surname']."</span>
                                <span name='lastname'>".$val['lastname']."</span>
                                <span name='phone'>".$val['phone']."</span>
                                <span name='area'>".$val['area']."</span>
                                <span name='region'>".$val['region']."</span>
                                <span name='index'>".$val['index']."</span>
                                <span name='city'>".$val['city']."</span>
                                <span name='address'>".$val['address']."</span>
                                <span name='txtInfo'>".$val['txtInfo']."</span>
                                 </div>";
                                $attr="";
                            }
                            $data=$selectAddress.'</select><span id="delAddress" ini="'.$ini.'">Удалить данные</span>'.$divHidden;
                            $tpl->assign('selectAddress',$data);
                        }
                        else{
                            if(!$this->isDiler())
                            {
                                $tpl->newBlock("block_selectAddress");
                                $tpl->assign('selectAddress','<select name="selectAddress"><option value="add">Добавить</option></select>');
                            }

                        }
                    }
                    break;

                case "delAddress":
                    global $db,$reguest_id;
                    $reguest_id=json_decode($_REQUEST['id']);
                    $db->query("Delete from sup_rus_address WHERE id= {$reguest_id}");
                    $db->query("Delete from core_users_prop_val WHERE user_id= {$_SESSION['siteuser']['id']}");
                    echo  json_encode(true);exit;
                    break;


                // Авторизация на сайте
                case "loginform":
                    $main->include_main_blocks($tpl_name, "main");
                    $tpl->prepare();
                    $this->show_login_form($transurl);
                    break;

                // Авторизация на сайте для редактирования контента
                case "startadmin":
                    global $main, $db, $server, $lang, $request_user_login, $request_user_pwd;
                    if (empty($CONFIG['siteusers_confirm_login'])) {
                        $db->query("SELECT username, password FROM core_users u WHERE id=".$_SESSION["siteuser"]["id"]);
                        if ($db->nf()) {
                            $db->next_record();
                            $_SESSION["session_is_admin"] = true;
                            $main->fillPermissions($db->f("username"), $db->f("password"));
                        }
                    } elseif ($request_user_login == $_SESSION["siteuser"]["siteusername"]) {
                        $db->query("SELECT id FROM core_users u WHERE u.id=su.id"
                            ." AND username='".$request_user_login."' AND password='".$request_user_pwd."'");
                        if ($db->nf()) {
                            $_SESSION["session_is_admin"] = true;
                            $main->fillPermissions($request_user_login, $request_user_pwd);
                        }
                    }
                    header("Location: ".(@$_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $transurl."&action=welcome"));
                    break;

                // Окончание редактирования контента
                case "endadmin":
                    unset($_SESSION["session_is_admin"]);
                    unset($_SESSION["permissions"]);
                    header("Location: ".(@$_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $transurl."&action=welcome"));
                    break;

                // Авторизация на сайте
                case "login":
                    global $request_username, $request_password, $main;
//                    $request_username = str_replace(array("<", ">", "\"", "'", ".", "@"), "", $request_username);
//                    $request_username = str_replace('\\', '', $request_username);
//                    $request_password = str_replace(array("<", ">", "\"", "'", ".", "@"), "", $request_password);
//                    $request_password = str_replace('\\', '', $request_password);
                    $result = $this->login_siteuser($request_username, $request_password);
                    if ($result == 1) {
                        if($CONFIG['siteusers_input_admin'])
                            $main->fillPermissions($request_username, $request_password);
                        // после авторизации переадресовать на страницу приветствия
                        $back_link = $transurl."&action=welcome";
                        if ($_SERVER['HTTP_REFERER']
                            && false !== stristr($_SERVER['HTTP_REFERER'], 'action=deny')
                        ) {
                            $back_link = parse_url(htmlspecialchars($_SERVER['HTTP_REFERER']), PHP_URL_PATH);
                        } else if ($_SERVER['HTTP_REFERER']
                            && false === stristr($_SERVER['HTTP_REFERER'], 'action=confirm')
                            && false === stristr($_SERVER['HTTP_REFERER'], 'action=logout')
                            && false === stristr($_SERVER['HTTP_REFERER'], 'action=login')
                        ) {
                            $back_link = parse_url(htmlspecialchars($_SERVER['HTTP_REFERER']), PHP_URL_PATH);
                        }
                        header("Location: ".$back_link);
                    } else if ($result == -1) {
                        $result_message = $this->_msg["Now_access_denied"];
                    } else {
                        $result_message = $this->_msg["Wrong_login_password"];
                    }
                    $main->show_result_message($result_message);
                    break;

                // Закончить сессию
                case "logout":
                    $this->logout_siteuser();
                    header('Location: '.$transurl.'&action=logoutsuccess');
                    break;

                // Закончить сессию
                case "logoutsuccess":
                    $main->show_result_message($this->_msg["You_logged_out"]);
                    break;

                // Персональный раздел
                case "welcome":
                    if ($this->is_authorized) {
                        $main->include_main_blocks($tpl_name, "main");
                        $tpl->prepare();
                        $tpl->newBlock("block_welcome_siteuser");
                        $tpl->assign(array(
                            'forum_page_address'    => $core->formPageLink($CONFIG["forum_page_address"], $CONFIG["forum_page_address"], $lang),
                            'mba_page_address'	    => $core->formPageLink($CONFIG["mba_page_address"], $CONFIG["mba_page_address"], $lang),
                            'he_page_address'		=> $core->formPageLink($CONFIG["he_page_address"], $CONFIG["he_page_address"], $lang),
                            'group'				    => (is_array($_SESSION["siteuser"]["group"])) ? implode(",", $_SESSION["siteuser"]["group"]) : '',
                            'orders_history_link'	=> $core->formPageLink($CONFIG["shop_page_link"], $CONFIG["shop_page_address"], $lang).'&action=orders',
                            'shopping_cart_link'	=> $core->formPageLink($CONFIG["shop_page_link"], $CONFIG["shop_page_address"], $lang),
                            'edit_profile_link'	    => $transurl."&action=registration",
                        ));
                    } else $main->show_result_message($this->_msg["Info_log_in_or"] . " <a href=\"{$transurl}&action=registration\">" . $this->_msg["register"] . "</a> " . $this->_msg["on_site"] . ".");
                    break;

                // Показывать пользователей сайта
                case "show_siteusers":
                    global $request_start, $request_group, $request_word;
                    $main->include_main_blocks($this->module_name . ".html", "main");
                    $tpl->prepare();
                    $start = intval($request_start);
                    $group = intval($request_group);
                    $word = $request_word;
                    if (!$start) $start = 1;
                    $order_by = $CONFIG["siteusers_order_by"];
                    if ($group) $where = "AND group_id = $group";
                    if ($word) $where .= " AND SUBSTRING(username,1,1) = '$word'";

                    $this->show_siteusers_list('block_siteusers', $transurl, $group, $start, $rows, "", $word);
                    $main->_show_nav_string($CONFIG["siteusers_table"], $where, "", "&action=$action&group=$group&word=$word", $start, $rows, $order_by, "", 0);
                    break;

                // Напомнить пароль
                case "remind":
                    global $request_step, $request_email, $request_hash;
                    $result_message = "";
                    if (!$request_step || $request_step == 1) {
                        $main->include_main_blocks($this->module_name."_remind_password.html", "main");
                        $tpl->prepare();
                        $tpl->assign(array(
                            'remind_password_action'	=>	$transurl.'&action=remind&step=2'
                        ));
                        // Добавление/обновление записи, шаг второй
                    } else if ($request_step == 2) {
                        $result = $this->remind_password($request_email);
                        if ($result == 1) {
                            $result_message = $this->_msg["Password_sent"];
                        } else if ($result == -1) {
                            $result_message = $this->_msg["Email_send_failure"];
                        } else {
                            $result_message = $this->_msg["Login_not_found"];
                        }
                        $main->show_result_message($result_message);
                    } else if ($request_step == 3) {
                        $form="<form method='post'>
                                    <table>
                                    <tr>
                                    <td>Новый пароль</td>
                                    <td><input name='password1' type='text'></td>
                                    </tr>
                                    <tr>
                                    <td>Повторите пароль</td>
                                    <td><input name='password2' type='text'></td>
                                    </tr>
                                    <tr>
                                    <td></td>
                                    <td><input name='submit' type='submit' value='Изменить'></td>
                                    </tr>
                                    </table>
                                                                </form>";
                        if(isset($_REQUEST['password1']))
                        {
                            if($_REQUEST['password1']==$_REQUEST['password2'])
                            {
                                $result = $this->change_password($request_hash,$_REQUEST['password1']);
                                //$result = $this->change_password($_REQUEST['hash']);
                                if ($result == 1) {
                                    $result_message = $this->_msg["Password_change"];
                                } else if ($result == -1) {
                                    $result_message = $this->_msg["Email_send_failure"].$form;
                                } else {
                                    $result_message = $this->_msg["Login_not_found"].$form;
                                }
                                $main->show_result_message($result_message);
                            }
                            else{
                                $main->show_result_message("<h4>Пароли не совпадают</h4>".$form);
                            }

                        }
                        else
                        {
                            $main->show_result_message($form);
                        }


                    }
                    $this->main_title = $this->_msg["Restore_password"];
                    break;

                // Показать информацию о пользователе
                case "userinfo":
                    global $request_user_id, $request_siteuser;

                    $main->include_main_blocks($tpl_name, "main");
                    $tpl->prepare();

                    if ($this->is_authorized || 1) {		//Решили показывать инфо всем
                        $user_id = $request_user_id ? $request_user_id : $this->get_siteuser_by_siteusername($request_siteuser, 'username');
                        $this->showUserInfo($user_id, $transurl);
                        $this->main_title = $this->_msg["Personal_section"];
                    } else {
                        $tpl->newBlock('block_access_deny');
                        $tpl->assign('registration_link', $transurl.'&action=registration');
                    }
                    break;

                // Показать информацию о пользователе
                case "usernav":
                    global $request_user_id, $request_siteuser;

                    $main->include_main_blocks($tpl_name, "main");
                    $tpl->prepare();
                    $user_id = 0;
                    if ($request_user_id >= 1) {
                        $user_id = (int)$request_user_id;
                    } else if ($request_siteuser) {
                        $user_id = $this->get_siteuser_by_siteusername($request_siteuser, 'username');
                    } else if ($PAGE['current_user_id']) {
                        $user_id = $PAGE['current_user_id'];
                    }
                    if ($user_id) {
                        $this->showUserInfo($user_id, $transurl);
                        $this->main_title = $this->_msg["Personal_section"];
                    } else {
                        $tpl->newBlock('block_undefined');
                        $tpl->assign('registration_link', $transurl.'&action=registration');
                    }
                    break;

                /**
                 * Информация о пользователях
                 */
                case "usersinfo":
                    $main->include_main_blocks($this->module_name."_".$action.".html", "main");
                    $tpl->prepare();

                    $tpl->assign(array(
                        'users_count' => $this->getUsersCount(),
                        'onsite_users_count' => $this->getUsersCount(array('active' => 1, 'onsite' => 1)),
                    ));
                    $last_user = $this->getUsers(array('active' => 1, 'order_by' => 'reg_date', 'order_desc' => 1, 'limit' => 1));
                    if ($last_user) {
                        $tpl->newBlock('block_last_user');
                        $tpl->assign($last_user[0] +
                            array(
                                'link' => $this->getUserLink($last_user[0]['username'])
                            ));
                    }
                    break;
                /**
                 * Отправить сообщение
                 */
                case "sendmsg":
                    global $request_data, $request_ref;

                    $result_message = $this->_msg['Now_access_denied'];
                    if ($this->is_authorized) {
                        if ($this->addMsg($request_data['to'], $_SESSION["siteuser"]["id"], $request_data['title'], $request_data['body'])) {
                            $result_message = $this->_msg['Send_msg_ok'];
                            $send = 1;
                        } else {
                            $result_message = $this->_msg['Send_msg_error'];
                            $send = 0;
                        }
                    }
                    if ($GLOBALS['JsHttpRequest']) {
                        $GLOBALS['_RESULT'] = array('res' => $send);
                        exit;
                    } else {
                        Module::go_back($request_ref);
                        $main->show_result_message($result_message);
                    }
                    break;
                /**
                 * Показать сообщения
                 */
                case "msglist":
                    global $request_out;
                    $main->include_main_blocks($this->module_name."_msg_list.html", "main");
                    $tpl->prepare();
                    if (isset($request_out)) {
                        $this->showMsgList(false, '_out');
                    } else {
                        $this->showMsgList(true, '_in');
                    }
                    break;
                /**
                 * Показать сообщение
                 */
                case "showmsg":
                    global $request_id;
                    $main->include_main_blocks($this->module_name."_msg.html", "main");
                    $tpl->prepare();
                    $this->showMsg($request_id);
                    break;
                /**
                 * Удалить сообщение
                 */
                case "delmsg":
                    global $request_id, $request_out;
                    $this->delMsg($request_id);
                    header("Location: ".$baseurl."action=msglist".(isset($request_out) ? "&out" : ""));
                    break;
                /**
                 * Добавить в друзья
                 */
                case "addfriend":
                    global $request_siteuser;
                    if (! class_exists('BlogsPrototype')) {
                        require(RP.'mod/blogs/lib/class.BlogsPrototype.php');
                    }
                    $blog = new BlogsPrototype('only_create_object');
                    $user_id = $this->get_siteuser_by_siteusername($request_siteuser);
                    if ($user_id && $this->is_authorized && !$blog->is_friend($user_id)) {
                        $blog->add_friend($user_id);
                    }
                    header("Location: ".$this->getUserLink($request_siteuser));
                    break;
                /**
                 * Удалить из друзья
                 */
                case "delfriend":
                    global $request_siteuser;
                    if (! class_exists('BlogsPrototype')) {
                        require(RP.'mod/blogs/lib/class.BlogsPrototype.php');
                    }
                    $blog = new BlogsPrototype('only_create_object');
                    $user_id = $this->get_siteuser_by_siteusername($request_siteuser);
                    if ($user_id && $this->is_authorized) {
                        $blog->delete_friend($user_id);
                    }
                    header("Location: ".$this->getUserLink($request_siteuser));
                    break;

                case "reports":
                    if(!$this->isDiler())
                    {
                        header('Location: /');
                        exit;
                    }
                    $this->main_title="Отчёты";
                    $main->include_main_blocks($this->module_name."_reports.html", "main");
                    $tpl->prepare();

                    if(isset($_REQUEST['start_date']))
                    {
                        if($_FILES['file']['error'] > 0)
                        {
                            //в зависимости от номера ошибки выводим соответствующее сообщение
                            //UPLOAD_MAX_FILE_SIZE - значение установленное в php.ini
                            //MAX_FILE_SIZE значение указанное в html-форме загрузки файла
                            switch ($_FILES['file']['error'])
                            {
                                case 1: $err= 'Размер файла превышает допустимое значение UPLOAD_MAX_FILE_SIZE'; break;
                                case 2: $err= 'Размер файла превышает допустимое значение MAX_FILE_SIZE'; break;
                                case 3: $err= 'Не удалось загрузить часть файла'; break;
                                case 4: $err= 'Файл не был загружен'; break;
                                case 6: $err= 'Отсутствует временная папка.'; break;
                                case 7: $err= 'Не удалось записать файл на диск.'; break;
                                case 8: $err= 'PHP-расширение остановило загрузку файла.'; break;
                            }
                            $tpl->assign(array('message'=>$err));
                        }
                        else
                        {


                            $uploaddir = 'files/reports/';
                            $newFileName=$_SESSION['siteuser']['siteusername'].'_'.$_REQUEST['start_date'].'-'.$_REQUEST['end_date'].'.xls';
                            $uploadfile = $uploaddir.$newFileName;
                            if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile))
                            {
                                $this->addReport($newFileName);
                                $tpl->assign(array('message'=>'Отчёт успешно загружен.'));
                            }else
                            {
                                $tpl->assign(array('message'=>'Ошибка загрузки файла.'));
                            }
                        }


                    }
                    $reports=$this->getDilerReports($_SESSION['siteuser']['id']);
                    if($reports)
                        foreach($reports as $val)
                        {
                            $tpl->newBlock("reports_history");
                            $tpl->assign(array('date'=>$val['date'],
                                'link'=>$val['link']));
                        }
                    break;

                // Default...
                default:
                    if (!empty($is_admin)) {return ;}
                        if($_SERVER['SERVER_NAME'] != "supra.ru"){
                        $main->show_result_message($this->_msg["Info_log_in_or"] . " <a href=\"{$transurl}&action=registration\">" . $this->_msg["register"] . "</a> " . $this->_msg["on_site"] . ".");

                    }
                    else
                        {
                            $main->show_result_message($this->_msg["Info_log_in_or"] . " <a href=\"{$transurl}&action=registration&groups=dilers\">" . $this->_msg["register"] . "</a> " . $this->_msg["on_site"] . ".");

                        }
                    //				if (is_object($tpl)) $tpl->prepare();
                    return;
            }
            //------------------------------- конец обработки действий с сайта -----------------------------//
        } else {


            //------------------------------ обработка действий из админ части -----------------------------//
            $this->block_module_actions = array(
                'loginform'		=> $this->_msg["Show_login_form"],
                'userinfo'		=> $this->_msg["Show_curr_user_info"],
                'usernav'		=> $this->_msg["Show_curr_user_nav"],
                'registration'		=> "Регистрация",
            );
            var_dump($this->_msg["Show_curr_user_nav"]);
            $this->block_main_module_actions = array(
                'welcome'	=> $this->_msg["Users_personal_section"],
                'userinfo'		=> $this->_msg["Show_curr_user_info"],
                'usernav'		=> $this->_msg["Show_curr_user_nav"],
                'registration'		=> "Регистрация",
            );
            $this->client_types = array(1 => $this->_msg["Customer"], 2 => $this->_msg["Partner"], 3 => $this->_msg["Dealer"]);

            // тип пользователя, в зависимости от него разная цена
            if (!isset($tpl)) {
                $main->message_die('Не подключен класс для работы с шаблонами');
            }

            $this->client_types = $CONFIG['siteusers_client_types'];

            switch ($action) {
                case 'prp':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_step;
                    $main->include_main_blocks('_properties.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Info_siteusers"	=> $this->_msg["Info_siteusers"],
                        "MSG_Information"	=> $main->_msg["Information"],
                        "MSG_Value"			=> $main->_msg["Value"],
                        "MSG_Save"			=> $main->_msg["Save"],
                        "MSG_Cancel"		=> $main->_msg["Cancel"],
                    ));
                    if (!$request_step || $request_step == 1) {
                        $tpl->assign(array(
                            'form_action'	=> $baseurl,
                            'step'			=> 2,
                            'lang'			=> $lang,
                            'name'			=> $this->module_name,
                            'action'			=> 'prp'
                        ));
                        $prp_html = $main->getModuleProperties($this->module_name);
                        $this->main_title = $this->_msg['Settings'];
                    } elseif (2 == $request_step) {
                        $main->setModuleProperties($this->module_name, $_POST);
                        header('Location: '.$baseurl.'&action=prp&step=1');
                    }
                    break;

                case 'block_prp':
                    global $request_sub_action, $request_block_id;
                    $arr  = $this->getEditLink($request_sub_action, $request_block_id);
                    $cont = 'var container = document.getElementById(\'target_span\');
		    		  	 container.innerHTML	= "'.$this->getPropertyFields().'";
		    		  	 material_id			= "'.$arr['material_id'].'";
		    		  	 material_url			= "'.$arr['material_url'].'";
		    		  	 changeAction();
						//setURL("property1");';
                    header('Content-Length: '.strlen($cont));
                    echo $cont;
                    exit;
                    break;

                case 'roles':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
                    global $request_start; $start = (int) $request_start;
                    $main->include_main_blocks_2($this->module_name.'_roles.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Add_role" => $this->_msg["Add_role"],
                        "MSG_Ed" => $this->_msg["Ed"],
                        "MSG_Domain" => $this->_msg["Domain"],
                        "MSG_language" => $this->_msg["language"],
                        "MSG_Description" => $this->_msg["Description"],
                        "MSG_Del" => $this->_msg["Del"],
                        "MSG_Info_siteusers"	=> $this->_msg["Info_siteusers"],
                        "users_add_role" => $baseurl.'&action=editrole',
                    ));
                    $pages_cnt = $this->getRoles(null,true);
                    $arr = (array) $this->getRoles($request_start);
                    foreach($arr as $k => $v)
                    {
                        $tpl->newBlock("block_roles");
                        $tpl->assign(Array(
                            "MSG_Edit" => $this->_msg["Edit"],
                            "MSG_Delete" => $this->_msg["Delete"],
                        ));
                        $tpl->assign($v);
                    }
                    $main->_show_nav_block($pages_cnt, 'nav', "action=".$action, $start);
                    $tpl->assign(Array(
                        "MSG_Pages" => $this->_msg["Pages"],
                    ));
                    $tpl->assign(
                        array(
                            '_ROOT.onchange'	=>	$baseurl.'&action=roles',
                        )
                    );

                    $this->main_title = $this->_msg["Roles"];
                    break;

                case 'editrole':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_step, $request_id, $request_site_lang_role, $request_role_descr,
                           $request_r, $request_e, $request_p, $request_d;
                    if (!$request_step || $request_step == 1) {
                        $main->include_main_blocks_2($this->module_name.'_edit_role.html', $this->tpl_path);
                        $tpl->prepare();
                        $tpl->assign(Array(
                            "MSG_Choose_site_language" => $this->_msg["Choose_site_language"],
                            "MSG_Select_site_language" => $this->_msg["Select_site_language"],
                            "MSG_Role_description" => $this->_msg["Role_description"],
                            "MSG_Module_name" => $this->_msg["Module_name"],
                            "MSG_Re" => $this->_msg["Re"],
                            "MSG_Ed" => $this->_msg["Ed"],
                            "MSG_Activ" => $this->_msg["Activ"],
                            "MSG_Del" => $this->_msg["Del"],
                            "MSG_Select_all" => $this->_msg["Select_all"],
                            "MSG_required_fields" => $this->_msg["required_fields"],
                            "MSG_Info_role_relogin" => $this->_msg["Info_role_relogin"],
                            "MSG_Save" => $this->_msg["Save"],
                            "MSG_Cancel" => $this->_msg["Cancel"],
                            "MSG_Info_siteusers"	=> $this->_msg["Info_siteusers"],
                        ));
                        $tpl->assign(array(
                            '_ROOT.role_id'		=> $request_id,
                            '_ROOT.form_action'	=> $baseurl.'&action=editrole&step=2',
                            '_ROOT.cancel_link'	=> $baseurl.'&action=roles'
                        ));

                        $arr	= $this->getRole($request_id);
                        $arr_2	= $this->getRolePermissions($request_id);

                        $tpl->assign($arr);
                        $tpl->assign_array('block_role_sites_langs', $this->getSitesLangs($arr));
                        $tpl->assign_array('block_role_permissions', $arr_2);
                        $tpl->assign(array(	'_ROOT.i_start'	=> 0,
                            '_ROOT.i_end'	=> sizeof($arr_2),));

                        if ($request_id) {
                            $this->main_title	= $this->_msg["Edit_role"];
                        } else {
                            $this->main_title	= $this->_msg["Add_role"];
                        }
                    } else if ($request_step == 2) {

                        $lid = $this->setRole($request_id, $request_site_lang_role, $request_role_descr);

                        $role_id = $lid ? $lid : $request_id;

                        $this->setRolePermissions($role_id, $request_r, $request_e, $request_p, $request_d);

                        header('Location: '.$baseurl.'&action=roles');
                        exit;
                    }
                    break;

                case 'delrole':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_id;
                    $this->delRole($request_id);
                    header('Location: '.$baseurl.'&action=roles');
                    exit;
                    break;

                case "groups":
                    if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
                    $main->include_main_blocks_2($this->module_name.'_groups.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "add_group_link"	=> $baseurl.'&action=addgroup',
                        "MSG_Add_group"		=> $this->_msg["Add_group"],
                        "MSG_Ed"			=> $this->_msg["Ed"],
                        "MSG_Roles"			=> $this->_msg["Roles"],
                        "MSG_Group"			=> $this->_msg["Group"],
                        "MSG_Description"	=> $this->_msg["Description"],
                        "MSG_Del"			=> $this->_msg["Del"],
                    ));
                    if(!empty($this->_msg["info_siteusers"])){
                        $tpl->newblock('block_info_groups');
                        $tpl->assign('MSG_info_siteusers',$this->_msg["info_siteusers"]);
                    }
                    $this->show_groups('block_groups');
                    $this->main_title = $this->_msg["Users_groups"];
                    break;

                // Добавление новой группы
                case "addgroup":
                    if (!$permissions['e']) {
                        $main->message_access_denied($this->module_name, $action);
                    }

                    global $request_step, $request_group_name, $request_group_owner,
                           $request_group_description, $request_p, $request_role;

                    if (!$request_step || $request_step == 1) {
                        $main->include_main_blocks_2($this->module_name.'_edit_group.html', $this->tpl_path);
                        $tpl->prepare();
                        $tpl->assign(Array(
                            "MSG_Enter_group_name" => $this->_msg["Enter_group_name"],
                            "MSG_Group_name" => $this->_msg["Group_name"],
                            "MSG_Group_owner" => $this->_msg["Group_owner"],
                            "MSG_Description" => $this->_msg["Description"],
                            "MSG_Set_rights" => $this->_msg["Set_rights"],
                            "MSG_Mark_closed_pages" => $this->_msg["Mark_closed_pages"],
                            "MSG_required_fields" => $this->_msg["required_fields"],
                            "MSG_Info_relogin_change_rights" => $this->_msg["Info_relogin_change_rights"],
                            "MSG_Domain" =>  $this->_msg["Domain"],
                            "MSG_language" =>  $this->_msg["language"],
                            "MSG_Role_description" =>  $this->_msg["Role_description"],
                            "MSG_Switch_on_off" =>  $this->_msg["Switch_on_off"],
                            "MSG_Roles"			=> $this->_msg["Roles"],
                            "MSG_Save" => $this->_msg["Save"],
                            "MSG_Cancel" => $this->_msg["Cancel"],
                            "_ROOT.form_action"	=>	$baseurl.'&action=addgroup&step=2',
                            "_ROOT.cancel_link"	=>	$baseurl.'&action=groups',
                        ));
                        if (in_array(1, $_SESSION["siteuser"]["group_id"])) {
                            $this->show_siteusers('block_owner', $_SESSION["siteuser"]["id"]);
                        } else {
                            $tpl->newBlock('block_owner');
                            $tpl->assign(Array(
                                "name"		=> $_SESSION["siteuser"]["siteusername"],
                                "value"		=> $_SESSION["siteuser"]["id"],
                                "selected"	=> ' selected',
                            ));
                        }
                        $this->show_group_permissions();
                        $tpl->assign_array('block_group_roles', $this->getRolesForGroup());
                        $this->main_title = $this->_msg["Add_new_group"];
                    } else if ($request_step == 2) {
                        $lid = $this->add_group($request_group_name, $request_group_owner, $request_group_description);
                        if ($lid) {
                            $this->set_group_permissions($lid, $request_p);
                            $this->updateGroupRoles($lid, $request_role);
                        }
                        header('Location: '.$baseurl.'&action=groups');
                        exit;
                    }
                    break;

                // Редактирование группы
                case "editgroup":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id;
                    $main->include_main_blocks_2($this->module_name.'_edit_group.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Enter_group_name" => $this->_msg["Enter_group_name"],
                        "MSG_Group_name" => $this->_msg["Group_name"],
                        "MSG_Group_owner" => $this->_msg["Group_owner"],
                        "MSG_Description" => $this->_msg["Description"],
                        "MSG_Set_rights" => $this->_msg["Set_rights"],
                        "MSG_Mark_closed_pages" => $this->_msg["Mark_closed_pages"],
                        "MSG_required_fields" => $this->_msg["required_fields"],
                        "MSG_Info_relogin_change_rights" => $this->_msg["Info_relogin_change_rights"],
                        "MSG_Domain" =>  $this->_msg["Domain"],
                        "MSG_language" =>  $this->_msg["language"],
                        "MSG_Role_description" =>  $this->_msg["Role_description"],
                        "MSG_Switch_on_off" =>  $this->_msg["Switch_on_off"],
                        "MSG_Roles"			=> $this->_msg["Roles"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                        '_ROOT.form_action'	=>	$baseurl.'&action=updategroup&id='.$request_id,
                        '_ROOT.cancel_link'	=>	$baseurl.'&action=groups',
                    ));
                    $owner_id = $this->show_group($request_id);
                    if (in_array(1, $_SESSION["siteuser"]["group_id"])) {
                        $this->show_siteusers('block_owner', $owner_id);
                    } else {
                        $tpl->newBlock('block_owner');
                        $tpl->assign(Array(
                            "name"		=> $_SESSION["siteuser"]["siteusername"],
                            "value"		=> $_SESSION["siteuser"]["id"],
                            "selected"	=> ' selected',
                        ));
                    }
                    $this->show_group_permissions($request_id);
                    $tpl->assign_array('block_group_roles', $this->getRolesForGroup($request_id));
                    $this->main_title = $this->_msg["Edit_group"];
                    break;

                // Обновление информации о группе
                case "updategroup":
                    if (!$permissions["e"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }

                    global $request_group_name, $request_group_owner, $request_group_description,
                           $request_active, $request_id, $request_p, $request_role;
                    if ($this->update_group($request_group_name, $request_group_owner, $request_group_description, $request_active, $request_id)) {
                        $this->set_group_permissions($request_id, $request_p);
                        $this->updateGroupRoles($request_id, $request_role);
                    }
                    header('Location: '.$baseurl.'&action=groups');
                    exit;
                    break;

                // Обновление информации о ролях группы
                case "editgrouproles":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id;
                    $main->include_main_blocks_2($this->module_name.'_edit_group_roles.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Enter_group_name" => $this->_msg["Enter_group_name"],
                        "MSG_Group_name" => $this->_msg["Group_name"],
                        "MSG_Description" => $this->_msg["Description"],
                        "MSG_Domain" => $this->_msg["Domain"],
                        "MSG_language" => $this->_msg["language"],
                        "MSG_Role_description" => $this->_msg["Role_description"],
                        "MSG_Switch_on_off" => $this->_msg["Switch_on_off"],
                        "MSG_Activate_group" => $this->_msg["Activate_group"],
                        "MSG_required_fields" => $this->_msg["required_fields"],
                        "MSG_Info_group_relogin" => $this->_msg["Info_group_relogin"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                    ));
                    $tpl->assign(array(	'_ROOT.form_action'	=> $baseurl.'&action=updategrouproles&id='.$request_id,
                        '_ROOT.cancel_link'	=> $baseurl.'&action=groups',));

                    $tpl->assign($this->getGroup($request_id));
                    $tpl->assign_array('block_group_roles', $this->getRolesForGroup($request_id));
                    $this->main_title = $this->_msg["Edit_role_set"];
                    break;

                // Обновление группы
                case 'updategrouproles':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_role;
                    $this->updateGroupRoles($request_id, $request_role);
                    header('Location: '.$baseurl.'&action=groups');
                    exit;
                    break;

                // Удаляем группу пользователей из БД
                case "delgroup":
                    if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
                    global $request_step, $request_id;
                    // Удаление группы, первый шаг
                    if (!$request_step || $request_step == 1) {
                        $main->include_main_blocks_2($this->module_name.'_del_group.html', $this->tpl_path);
                        $tpl->prepare();
                        $tpl->assign(Array(
                            "MSG_Delete_users_group" => $this->_msg["Delete_users_group"],
                            "MSG_deletes_all_its_users" => $this->_msg["deletes_all_its_users"],
                            "MSG_Confirm_delete_this_group" => $this->_msg["Confirm_delete_this_group"],
                            "MSG_Cancel" => $this->_msg["Cancel"],
                            "MSG_Delete" => $this->_msg["Delete"],
                        ));
                        $tpl->assign(
                            array(
                                '_ROOT.form_action'	=>	$baseurl.'&action=delgroup&step=2&id='.$request_id,
                                '_ROOT.cancel_link'	=>	$baseurl.'&action=groups',
                            )
                        );
                        $this->show_group($request_id);
                        $this->main_title = $this->_msg["Delete_group"];
                        // Удаление группы, шаг второй
                    } else if ($request_step == 2) {
                        $this->delete_group($request_id);
                        header('Location: '.$baseurl.'&action=groups');
                        exit;
                    }
                    break;

                // Вывод списка пользователей
                case "siteusers":
                    if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
                    global $request_start, $request_group;
                    $start			= ($request_start) ? $request_start : 1;
                    $request_group	= (int)$request_group;
                    $where = '';
                    $href = 'action='.$action;
                    if ($request_group) {
                        $where = 'AND group_id='.$request_group;
                        $href .= '&group='.$request_group;
                    }
                    $main->include_main_blocks_2($this->module_name.'_list.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "add_user_link" => $baseurl.'&action=addsiteuser',
                        "MSG_Add_user" => $this->_msg["Add_user"],
                        "MSG_Show" => $this->_msg["Show"],
                        "MSG_All_users" => $this->_msg["All_users"],
                        "MSG_Access" => $this->_msg["Access"],
                        "MSG_Ed" => $this->_msg["Ed"],
                        "MSG_Email" => $this->_msg["Email"],
                        "MSG_Login" => $this->_msg["Login"],
                        "MSG_Group" => $this->_msg["Group"],
                        "MSG_Lastname_firstname" => $this->_msg["Lastname_firstname"],
                        "MSG_Reg_date" => $this->_msg["Reg_date"],
                        "MSG_Last_visit" => $this->_msg["Last_visit"],
                        "MSG_Del" => $this->_msg["Del"],
                        "MSG_Select_action" => $this->_msg["Select_action"],
                        "MSG_Activate_access" => $this->_msg["Activate_access"],
                        "MSG_Suspend_access" => $this->_msg["Suspend_access"],
                        "MSG_Delete" => $this->_msg["Delete"],
                        "MSG_Execute" => $this->_msg["Execute"],
                        'baseurl' => $baseurl,
                        'lang' => $lang,
                        'q' => $_REQUEST['q'] ? htmlspecialchars($_REQUEST['q']) : ''
                    ));
                    $this->show_groups('block_groups', array($request_group));
                    $filter = array(
                        'start' => $start,
                        'group' => $request_group,
                        'q' => $_REQUEST['q']
                    );
                    $pages_cnt = $this->show_siteusers_list_adm('block_siteusers_list', $filter);
                    // Функция для разбивки записей по страницам и вывода навигации по ним
                    // Имена полей и их назначение :
                    // ($table = таблица, $where = условие выборки, $blockname = имя блока,
                    // $action = добавка к урл, $start = текущая страница/начало выборки,
                    // $rows = кол-во строк, $order_by = порядок выборки, $separator = разделитель)
                    //				$main->_show_nav_string($server.$lang.'_siteusers', $where, '', $href, $start, $CONFIG['siteusers_max_rows'], $CONFIG['siteusers_order_by'], '');
                    // $pages = количество страниц,
                    // $blockname = название навигационного блока в шаблоне,
                    // $action = добавка к урл, $current = текущая страница/начало выборки,
                    // $current - номер текущей страницы
                    $main->_show_nav_block($pages_cnt, 'nav', "action=".$action."&group=".$request_group, $start);
                    $tpl->assign(Array(
                        "MSG_Pages" => $this->_msg["Pages"],
                    ));
                    $tpl->assign(
                        array(
                            '_ROOT.onchange'	=>	$baseurl.'&action=siteusers&group=',
                        )
                    );
                    $this->main_title = $this->_msg["siteusers"];
                    break;

                //вывод информации по дилерам
                case "diler":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    $main->include_main_blocks_2($this->module_name.'_dilers.html', $this->tpl_path);
                    global $reguest_id;
                    $user=$this->getUser($_REQUEST['id']);
                    $tpl->prepare();
                    $tpl->assign(Array("fio"=>$user['prop_list'][11]['value']));
                    $tpl->prepare();
                    $reports=$this->getDilerReports($_REQUEST['id']);

                    foreach($reports as $val)
                    {
                        $tpl->newBlock('rep_block');
                        $link="/files/reports/".$val['link'];
                        $tpl->assign(array('link'=>$link,
                            "name"=>$val['link'],
                            'dateReports'=>$val['date']));
                    }
                    $update= $this->getUserUpdates($_REQUEST['id']);

                    foreach($update as $val)
                    {
                        $tpl->newBlock('upd_block');
                        $data= json_decode($val['data']);
                        $res="";
                        foreach($data as $key=>$val1)
                        {
                            $res=$res.$key.":".$val1."|";
                        }
                        $tpl->assign(array('value'=>$res,
                            'dateUpdates'=>$val['date']));
                    }
                    break;
                // Добавление нового пользователя
                case "addsiteuser":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_step, $request_err, $request_group;

                    // Добавление новой записи, первый шаг
                    if (!$request_step || $request_step == 1) {
                        $main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path);
                        $tpl->prepare();

                        if (isset($request_err)) {
                            $tpl->newBlock('block_error_msg');
                            $tpl->assign('error_msg', $this->_msg["Fail_add_user"]);
                        }

                        $this->show_registration_form(true);
                        $tpl->gotoBlock("block_form");
                        $tpl->assign(Array(
                            "MSG_Enter_lastname" => $this->_msg["Enter_lastname"],
                            "MSG_Enter_firstname" => $this->_msg["Enter_firstname"],
                            "MSG_Enter_company" => $this->_msg["Enter_company"],
                            "MSG_Enter_post" => $this->_msg["Enter_post"],
                            "MSG_Enter_phone" => $this->_msg["Enter_phone"],
                            "MSG_Choose_users_group" => $this->_msg["Choose_users_group"],
                            "MSG_Choose_user_type" => $this->_msg["Choose_user_type"],
                            "MSG_Users_group" => $this->_msg["Users_group"],
                            "MSG_Select_group" => $this->_msg["Select_group"],
                            "MSG_User_type" => $this->_msg["User_type"],
                            "MSG_Select_type" => $this->_msg["Select_type"],
                            "MSG_required_fields" => $this->_msg["required_fields"],
                            "MSG_Max_height_width_size" => $this->_msg["Max_height_width_size"],
                            "MSG_Choose_users_group" => $this->_msg["Choose_users_group"],
                            "Groups" => $this->_msg["Groups"],
                            "MSG_Save" => $this->_msg["Save"],
                            "MSG_Cancel" => $this->_msg["Cancel"],
                        ));

                        $this->main_title = $this->_msg["Add_new_user"];
                        // Добавление новой записи, шаг второй
                    } else if ($request_step == 2) {

                        if ($this->add_siteuser()) {
                            header('Location: '.$baseurl.'&action=siteusers&group='.$request_group);
                        } else {
                            header('Location: '.$baseurl.'&action=addsiteuser&err');
                        }
                        exit;
                    }
                    break;


                // Вывод на экран и редактирование информации о пользователе
                case "editsiteuser":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $db1, $request_id;
                    $main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path);
                    $tpl->prepare();

                    $user = $this->getUser($request_id);
                    if (!$user) {
                        $tpl->newBlock('block_error_msg');
                        $tpl->assign('error_msg', $this->_msg["User_with_id"] . ' = \''.$request_id.'\' ' . $this->_msg["doesnt_exist"] . '');
                        $group_id[] = 0;
                    }
                    $this->show_registration_form();
                    $tpl->gotoBlock("block_form");
                    $tpl->assign(Array(
                        "MSG_Enter_lastname" => $this->_msg["Enter_lastname"],
                        "MSG_Enter_firstname" => $this->_msg["Enter_firstname"],
                        "MSG_Enter_company" => $this->_msg["Enter_company"],
                        "MSG_Enter_post" => $this->_msg["Enter_post"],
                        "MSG_Enter_phone" => $this->_msg["Enter_phone"],
                        "MSG_Choose_users_group" => $this->_msg["Choose_users_group"],
                        "MSG_Choose_user_type" => $this->_msg["Choose_user_type"],
                        "MSG_Users_group" => $this->_msg["Users_group"],
                        "MSG_Select_group" => $this->_msg["Select_group"],
                        "MSG_User_type" => $this->_msg["User_type"],
                        "MSG_Select_type" => $this->_msg["Select_type"],
                        "MSG_required_fields" => $this->_msg["required_fields"],
                        "MSG_Max_height_width_size" => $this->_msg["Max_height_width_size"],
                        "Groups" => $this->_msg["Groups"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                    ));
                    $this->show_groups('block_groups', $user['groups']);
                    $tpl->assign_array('block_user_groups', $this->getUserGroups($request_id));

                    $this->main_title = $this->_msg["Edit_user"].($user ? " &laquo;".$user['siteusername']."&raquo;" : '');
                    break;


                // Обновление информации о пользователе в БД
                case "updatesiteuser":
                    if (!$permissions["e"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $request_oldgroup, $request_group, $request_start, $request_id;

                    $group = ($request_oldgroup == 0) ? 0 : $request_group;
                    if ($request_id) {
                        $this->update_siteuser($err);
                    } else {
                        $this->add_siteuser();
                    }
                    header('Location: '.$baseurl.'&action=siteusers&start='.$request_start.'&group='.$group);
                    exit;
                    break;


                // Удаляем пользователя из БД
                case "delsiteuser":
                    if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_start, $request_group;
                    $this->delete_siteuser($request_id);
                    header('Location: '.$baseurl.'&action=siteusers&start='.$request_start.'&group='.$request_group);
                    exit;
                    break;

                // Отключение доступа
                case "suspend":
                    if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_start, $request_group;
                    if ($request_id == $_SESSION['session_siteuser_id']) $main->message_die('Вы не можете отключить доступ сами себе');
                    if ($main->suspend('core_users', $request_id)) {
                        $this->informUser($request_id);
                    }
                    header('Location: '.$baseurl.'&action=siteusers&start='.$request_start.'&group='.$request_group);
                    exit;
                    break;

                // Включение доступа
                case "activate":
                    if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_start, $request_group;
                    if ($main->activate('core_users', $request_id)) {
                        $this->informUser($request_id);
                    }
                    header('Location: '.$baseurl.'&action=siteusers&start='.$request_start.'&group='.$request_group);
                    exit;
                    break;


                // Удалить выбранных пользователей
                case "delete_checked":
                    if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
                    global $request_check, $request_start, $request_group;
                    if(is_array($request_check)) {
                        foreach($request_check as $k => $v) {
                            $this->delete_siteuser($v);
                        }
                    }
                    header('Location: '.$baseurl.'&action=siteusers&start='.$request_start.'&group='.$request_group);
                    exit;
                    break;

                // Отключить доступ выбранных пользователей
                case "suspend_checked":
                    if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
                    global $request_check, $request_start, $request_group;
                    if(is_array($request_check)) {
                        foreach($request_check as $k => $v) {
                            if ($v != $_SESSION['session_siteuser_id'])  {
                                if ($main->suspend('core_users', $v)) {
                                    $this->informUser($v);
                                }
                            }
                        }
                    }
                    header('Location: '.$baseurl.'&action=siteusers&start='.$request_start.'&group='.$request_group);
                    exit;
                    break;

                // Включить доступ выбранных пользователей
                case "activate_checked":
                    if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
                    global $request_check, $request_start, $request_group;
                    if(is_array($request_check)) {
                        foreach($request_check as $k => $v) {
                            if ($main->activate('core_users', $v)) {
                                $this->informUser($v);
                            }
                        }
                    }
                    header('Location: '.$baseurl.'&action=siteusers&start='.$request_start.'&group='.$request_group);
                    exit;
                    break;

                // Выгрузка/загрузка XML
                case 'imp-exp':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
                    $main->include_main_blocks_2($this->module_name.'_import_export.html', $this->tpl_path);
                    $tpl->prepare();

                    if ($_SESSION["info_block_name"]) {
                        $tpl->newBlock('block_info_box');
                        $tpl->newBlock($_SESSION["info_block_name"]);
                        unset($_SESSION["info_block_name"]);
                    }
                    $tpl->assign(Array(
                        "MSG_Info_export_success" => $this->_msg["Info_export_success"],
                        "MSG_No_new_info_for_export" => $this->_msg["No_new_info_for_export"],
                        "MSG_Info_import_success" => $this->_msg["Info_import_success"],
                    ));

                    //$tpl->newBlock('block_upload_xml');
                    //$tpl->assign(array(	'upload_xml_url'	=> $baseurl.'&action=uploadxml',));
                    // $tpl->assign(Array(
                    //	"MSG_Upload_XML_file" => $this->_msg["Upload_XML_file"],
                    //	"MSG_Upload" => $this->_msg["Upload"],
                    //));

                    $tpl->newBlock('block_get_xml');
                    $tpl->assign(Array(
                        "MSG_Download_XML_file" => $this->_msg["Download_XML_file"],
                        "MSG_Download" => $this->_msg["Download"],
                    ));
                    $tpl->assign(array(	'upload_xml_url'	=> $baseurl.'&action=getxml',));

                    $this->main_title	= $this->_msg["Import_Export_info"];
                    break;

                case 'getxml':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);

                    if ($this->do_export_XML()) {
                        $_SESSION["info_block_name"] = "block_info_download_success";
                    } else {
                        $_SESSION["info_block_name"] = "block_no_info_to_download";
                    }

                    header('Location: '.$baseurl.'&action=imp-exp');
                    break;

                /*		    case 'uploadxml':
                if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                if ($_FILES['xml_user_file']['tmp_name'] && ($dom = domxml_open_file($_FILES['xml_user_file']['tmp_name']))) {
                $this->doImportXMLData($dom);
                }
                $_SESSION["info_block_name"] = "block_info_upload_success";

                header('Location: '.$baseurl.'&action=imp-exp');
                break;*/

                // Изменить свои настройки
                case "changepwd":
                    global $request_step, $request_password, $request_group;
                    if (!$request_step || $request_step == 1) {
                        $main->include_main_blocks_2($this->module_name."_changepwd.html", $this->tpl_path);
                        $tpl->prepare();
                        $tpl->assign(Array(
                            "MSG_Enter_password" => $this->_msg["Enter_password"],
                            "MSG_Confirm_password" => $this->_msg["Confirm_password"],
                            "MSG_Passwords_differ" => $this->_msg["Passwords_differ"],
                            "MSG_New_password" => $this->_msg["New_password"],
                            "MSG_Confirm_new_password" => $this->_msg["Confirm_new_password"],
                            "MSG_required_fields" => $this->_msg["required_fields"],
                            "MSG_Save" => $this->_msg["Save"],
                            "MSG_Cancel" => $this->_msg["Cancel"],
                            "_ROOT.form_action"	=>	"$baseurl&action=changepwd&step=2",
                        ));
                        $this->main_title = $this->_msg["Change_password"];
                    } else if ($request_step == 2) {
                        $this->update_password($request_password, $_SESSION["siteuser"]["id"]);
                        header("Location: $baseurl&action=groups&group=$request_group");
                        exit;
                    }
                    break;

                case "prop_list":
                    if (!$permissions["d"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    $main->include_main_blocks_2("{$this->module_name}_{$action}.html", $this->tpl_path);
                    $tpl->assignInclude("prop_set", $this->tpl_path.$this->module_name."_prop_set.html");
                    $tpl->prepare();

                    $assign = array(
                        "baseurl" => $baseurl,
                        "action" => $action,
                        "MSG_Add" => $this->_msg["Add"],
                        "MSG_Activ" => $this->_msg["Activ"],
                        "MSG_Order" => $this->_msg["Order"],
                        "MSG_Del" => $this->_msg["Del"],
                        "MSG_Delete" => $this->_msg["Delete"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                        "MSG_Select" => $this->_msg["Select"],
                        "MSG_Group" => $this->_msg["Group"],
                        "MSG_Uname" => $this->_msg["Uname"],
                        "MSG_Type" => $this->_msg["Type"],
                        "MSG_Title" => $this->_msg["Title"],
                        "MSG_Required" => $this->_msg["Required"],
                        "MSG_Public" => $this->_msg["Public"],
                        "MSG_Public_short" => $this->_msg["Public_short"],
                        "MSG_Reg_show" => $this->_msg["Reg_show"],
                        "MSG_Reg_show_short" => $this->_msg["Reg_show_short"],
                        "MSG_Mandatory" => $this->_msg["Mandatory"],
                        "MSG_All_groups" => $this->_msg["All_groups"],
                        "MSG_Access" => $this->_msg["Access"],
                        "MSG_Ed" => $this->_msg["Ed"],
                        "MSG_Add_prop" => $this->_msg["Add_prop"],
                        "MSG_Move_down" => $this->_msg["Move_down"],
                        "MSG_Move_up" => $this->_msg["Move_up"]
                    );

                    $this->showPropForm(0, $assign);

                    $this->showPropList($this->getPropList(), $assign);

                    $tpl->gotoBlock('prop_list');
                    Module::show_select('group_list2', 0, '', $this->getGroups());
                    $this->main_title = $this->_msg["Props"];
                    break;

                case "prop_set_all":
                    if (!$permissions["e"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }

                    if (! empty($_POST)) {
                        global $db;
                        foreach ($_POST['id'] as $id) {
                            $db->set('core_users_prop',
                                array(
                                    'required' => $_POST['required'][$id] ? 1 : 0,
                                    'public' => $_POST['public'][$id] ? 1 : 0,
                                    'reg_show' => $_POST['reg_show'][$id] ? 1 : 0,
                                    'group' => intval($_POST['group'][$id])
                                ),
                                array('id' => intval($id))
                            );
                        }

                    }
                    Module::go_back($baseurl.'&action=prop_list');

                case "prop_set":
                    if (!$permissions["e"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }

                    if (! empty($_POST) && $_POST['uname']) {
                        $this->setProp($_POST);
                        Module::go_back($baseurl.'&action=prop_list');
                    }

                    $main->include_main_blocks_2("{$this->module_name}_{$action}.html", $this->tpl_path);
                    $tpl->prepare();

                    $assign = array(
                        "baseurl" => $baseurl,
                        "action" => $action,
                        "MSG_Add" => $this->_msg["Add"],
                        "MSG_Del" => $this->_msg["Del"],
                        "MSG_Delete" => $this->_msg["Delete"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                        "MSG_Select" => $this->_msg["Select"],
                        "MSG_Group" => $this->_msg["Group"],
                        "MSG_Uname" => $this->_msg["Uname"],
                        "MSG_Type" => $this->_msg["Type"],
                        "MSG_Title" => $this->_msg["Title"],
                        "MSG_Required" => $this->_msg["Required"],
                        "MSG_Public" => $this->_msg["Public"],
                        "MSG_Public_short" => $this->_msg["Public_short"],
                        "MSG_Reg_show" => $this->_msg["Reg_show"],
                        "MSG_Reg_show_short" => $this->_msg["Reg_show_short"],
                        "MSG_Mandatory" => $this->_msg["Mandatory"],
                        "MSG_All_groups" => $this->_msg["All_groups"],
                        "MSG_Access" => $this->_msg["Access"],
                        "MSG_Ed" => $this->_msg["Ed"],
                        "MSG_Add_prop" => $this->_msg["Add_prop"]
                    );

                    $this->showPropForm($_REQUEST['id'], $assign);

                    $tpl->newBlock('edit_info');

                    $this->main_title = $this->_msg["Edit_prop"];
                    break;

                case "prop_del":
                    if (!$permissions["d"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    $this->delProp($_REQUEST['id']);
                    Module::go_back($baseurl.'&action=prop_list');
                    break;

                // Отключение доступа
                case "prop_suspend":
                    if (!$permissions["p"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $request_id;
                    $main->suspend('core_users_prop', $request_id);
                    Module::go_back();
                    exit;
                    break;

                // Включение доступа
                case "prop_activate":
                    if (!$permissions["p"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $request_id;
                    $main->activate('core_users_prop', $request_id);
                    Module::go_back();
                    exit;
                    break;

                case "prop_up":
                    if (!$permissions["e"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $request_id, $request_shift, $request_rank;
                    $request_shift = $request_shift > 1 ? (int)$request_shift : 1;
                    $main->change_order('up', 'core_users_prop', 'rank', $request_rank, $request_shift);
                    Module::go_back();
                    exit;
                    break;

                case "prop_down":
                    if (!$permissions["e"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $request_id, $request_shift, $request_rank;
                    $request_shift = $request_shift > 1 ? (int)$request_shift : 1;
                    $main->change_order('down', 'core_users_prop', 'rank', $request_rank, $request_shift);
                    Module::go_back();
                    exit;
                    break;

                case 'properties':
                    if (!$permissions['e']) $main->message_access_denied($siteusres->module_name, $action);
                    global $module_id, $request_field, $title;
                    $main->include_main_blocks('_block_properties.html', 'main');
                    $tpl->prepare();

                    if ($main->show_actions($request_field, '', $this->block_main_module_actions, $this->block_module_actions)) {
                        $main->show_linked_pages($module_id, $request_field);
                    }

                    $tpl->assign(array(	'_ROOT.title'		=> $title,
                        '_ROOT.username'	=> $_SESSION['session_login'],
                        '_ROOT.password'	=> $_SESSION['session_password'],
                        '_ROOT.name'		=> $this->module_name,
                        '_ROOT.lang'		=> $lang,
                    ));
                    $this->main_title	= $this->_msg["Block_properties"];
                    break;

                // Default...
                default:
                    $main->include_main_blocks();
                    $tpl->prepare();
            }
            //--------------------------- конец обработки действий из админ части --------------------------//
        }
    }


    /**
     * Добавить пользователя
     *
     * @return mixed FALSE or user_id
     */
    function add_siteuser()
    {   global $CONFIG, $db, $core, $lang;

        if ( FALSE !== strpos($_SERVER['PHP_SELF'], "admin.php") ) {
            $admin = 1;
        }

        $_REQUEST["siteusername"]     = trim($_REQUEST["siteusername"]);
        $_REQUEST["password"]         = trim($_REQUEST["password"]);
        $_REQUEST["confirm_password"] = trim($_REQUEST["confirm_password"]);
        $_REQUEST["email"] = trim($_REQUEST["email"]);
        if ( 1 > $this->isValidPassword($_REQUEST["password"], $_REQUEST["confirm_password"]) //1 > $this->isValidLogin($_REQUEST["siteusername"]) ||
            || ! $this->isValidEmail($_REQUEST["email"])
            || $this->get_siteuser_by_siteusername($_REQUEST["email"])
            || $this->get_siteuser_by_siteusername($_REQUEST["siteusername"], 'username')
        ) {
            return FALSE;
        }

        $groups = array();
        $client_type = 0;
        if ($admin) {
            $is_new = 0;
            $active = 1;
            $client_type = intval($_REQUEST["type"]);
            $grp_list = explode(",", $_REQUEST["group"]);
            foreach ($grp_list as $key => $val) {
                if ($this->groupExist($val)) {
                    $groups[] = $val;
                }
            }
            if (empty($groups)) {
                return FALSE;
            }
        } else {
            $is_new = 1;
            $active = 0;
            if(isset($_REQUEST['groups']))
            {
                if($_REQUEST['groups']=="dilers")
                $groups[] = 3;
            }
            else{
                $groups[] = $CONFIG['siteusers_group'] ? $CONFIG['siteusers_group'] : $this->getFirstGroup();
            }
            if ( ! $groups[0] ) {
                return FALSE;
            }
        }
        if (! $client_type) {
            $client_type = $this->default_client_type;
        }
        $uid = md5(uniqid(rand(), true));

        $ret = $db->set('core_users', array(
            'username' => $_REQUEST["siteusername"],
            'password' => md5($_REQUEST["password"]),
            'email' => $_REQUEST["email"],
            'site_id' => $_SESSION['session_cur_site_id'],
            'lang_id' => $_SESSION['session_cur_lang_id'],
            'reg_date' => new DbExp('NOW()'),
            'active' => $active,
            'is_new' => $is_new,
            'client_type' => $client_type,
            'uid' => $uid

        ));
        $user_id = 0;
        if ($ret) {
            $user_id = $db->lid();
            $db->query("INSERT INTO core_users_groups (user_id, group_id) VALUES (".$user_id.","
                .implode("), (".$user_id.",", $groups).")");
            $this->setUserProp($user_id, $_POST['prop']);
            $this->uploadFile($user_id, 'img');
        }

        if ($CONFIG["siteusers_register_auto"] == "on") {
            // Письмо пользователю с просьбой о подтверждении регистрации
            $notify_blocks = array();
            if ($CONFIG["siteusers_reg_type"] == 'email_confirm') {
                $confirmation_link = $core->formPageLink($CONFIG['siteusers_page_link'], $CONFIG['siteusers_page_address'], $lang, 1);
                $notify_blocks['confirm'] = array(
                    'link' => 'http://'.$_SERVER['SERVER_NAME'].$confirmation_link.'&action=confirm&uid='.$uid.'&nocash'
                );
            }


            $this->notifyUser(
                $user_id,
                $_REQUEST["email"],
                array($CONFIG['return_email'], $CONFIG['sitename_rus']),
                $this->_msg["New_user_register_on_site"].' '.$CONFIG["sitename_rus"].' ('.$_SERVER['SERVER_NAME'].')',
                'tpl/'.SITE_PREFIX.'/siteusers_mail_registration.html',
                array(
                    'web_address' => $_SERVER['SERVER_NAME'],
                    'sitename_rus'	=> $_SERVER['SERVER_NAME'],
                    'date' => date("d/m/Y G:i:s"),
                    'password' => $_REQUEST["password"],
                ),
                $notify_blocks
            );
        }

        if (!$admin) {
            if($_REQUEST['groups']=="dilers")
            {

                $this->notifyUser(
                    $user_id,
                    $CONFIG['siteusers_recipient'],
                    $_REQUEST["email"],
                    'Вы подали заявку на регистрацию в качестве дилера SUPRA',
                    'tpl/admin/siteusers_mail_delivers_registration.html',
                    array(
                        'date' => date("d/m/Y G:i:s"),
                        'fio' => $_POST['prop'][11]
                    )
                );
            }
            else{
                $this->notifyUser(
                    $user_id,
                    $CONFIG['siteusers_recipient'],
                    $_REQUEST["email"],
                    $this->_msg["New_user_register_on_site"].' '.$CONFIG["sitename_rus"].' ('.$_SERVER['SERVER_NAME'].')',
                    'tpl/'.SITE_PREFIX.'/siteusers_mail_adm_registration.html',
                    array(
                        'web_address' => $_SERVER['SERVER_NAME'],
                        'sitename_rus'	=> $_SERVER['SERVER_NAME'],
                        'date' => date("d/m/Y G:i:s"),
                    )
                );
            }
        }

        return $user_id;
    }


    // функция для подтверждения регистрации
    function confirm_registration($uid)
    {
        GLOBAL $CONFIG, $db, $server, $lang;

        $uid = $db->escape(strip_tags(substr($uid, 0, 50)));
        $db->query('SELECT active FROM core_users WHERE uid="'.$uid.'"');
        if (!$db->nf()) return FALSE;
        $db->next_record();

        if (0 == $db->Record['active']) {
            $db->query('UPDATE core_users SET active = 1 WHERE uid="'.$uid.'"');
        }

        return TRUE;
    }


    // функция для обновления информации о пользователе в базе данных
    function update_siteuser(&$err)
    {   global $CONFIG, $db;

        $ret = true;
        if ( FALSE !== strpos($_SERVER['PHP_SELF'], "admin.php") ) {
            $admin = 1;
        }

        $user_id = $admin ? intval($_REQUEST["id"]) : $_SESSION["siteuser"]["id"];
        $user = $this->getUser($user_id);
        if (! $user) {
            $err = "Пользователь не найден";
            return '';
        }

        $set = array();

        if ($_REQUEST['siteusername'] && $_REQUEST['siteusername'] != $user['username']
            && (1 > $this->isValidLogin($_REQUEST["siteusername"]) || $this->get_siteuser_by_siteusername($_REQUEST["siteusername"], 'username'))
        ) {
            $err = "Не допустимый ник пользователя";
            return FALSE;
        } else if($_REQUEST['siteusername']) {
            $set['username'] = $_REQUEST['siteusername'];
        }

        if ($_REQUEST['email'] && $_REQUEST['email'] != $user['email']
            && (! $this->isValidEmail($_REQUEST["email"]) || $this->get_siteuser_by_siteusername($_REQUEST["email"], 'email'))
        ) {
            $err = "Не допустимый e-mail пользователя";
            return FALSE;
        } else if ($_REQUEST['email']) {
            $set['email'] = $_REQUEST['email'];
        }

        if ($_REQUEST["password"] && $_REQUEST["password"] && md5($_REQUEST["password"])!=$user['password']
            && (1 > $this->isValidPassword($_REQUEST["password"], $_REQUEST["confirm_password"]))
        ) {
            $err = "Не допустимый пароль пользователя";
            return false;
        } else if ($_REQUEST["password"]) {
            $set['password'] = md5($_REQUEST['password']);
        }

        $groups = array();

        if ($admin) {
            $set['client_type'] = intval($_REQUEST["type"]);
            $grp_list = explode(",", $_REQUEST["group"]);
            foreach ($grp_list as $key => $val) {
                if ($this->groupExist($val)) {
                    $groups[] = $val;
                }
            }
            if (empty($groups)) {
                $err = "Не определена группа пользователя";
                return FALSE;
            }
        } else {
            if (!$this->is_authorized) {
                $err = "Пользователь не авторизован";
                return FALSE;
            }
        }
        if (empty($set) || $ret = $db->set('core_users', $set, array('id' => $user_id))) {
            if ($groups) {
                $db->del('core_users_groups', array('user_id' => $user_id));
                $db->query("INSERT INTO core_users_groups (user_id, group_id) VALUES (".$user_id.","
                    .implode("), (".$user_id.",", $groups).")");
            }
            $this->setUserProp($user_id, $_POST['prop']);
            $this->uploadFile($user_id, 'img', $user['img'], $_POST['del_img']);
        }
        if($this->isDiler())
        {
            /*  $db->set('core_users_prop_val', array(
                  'user_id' => $user_id,
                  'prop_id' => 11,
                  'value' => $_POST['prop']['2']." ".$_POST['prop']['1']
              ));*/
            $arrR=$db->getArrayOfResult("SELECT p.type,p.title, v.value
                                FROM `core_users_prop_val` AS v
                                RIGHT JOIN `core_users_prop` AS p ON p.id = v.prop_id
                                WHERE v.user_id ={$user_id}");
            foreach($arrR as $key=>$value)
            {
                if($value['type']==6)
                {
                    $prop=$this->getProp(19);
                    $resUp[$prop['title']]=$prop['list_val'][$_POST['prop'][19]];
                }
                elseif($value['type']==5)
                {
                    $prop=$this->getProp(20);
                    $j="";
                    foreach($_POST['prop'][20] as $key=>$val)
                    {
                        if(!$j) $j=$prop['list_val'][$key];
                        else $j=$j.",".$prop['list_val'][$key];
                    }

                    $resUp[$prop['title']]=$j;
                }
                else
                {
                    $resUp[$value['title']]=$value['value'];
                }

            }
            $arrUpd=array('user_id'=>$user_id,
                'date'=>date("d-m-y h:i:s"),
                'data'=>json_encode($resUp));
            $db->set('sup_rus_dilers_history', $arrUpd);
        }

        return $ret;
    }


    // функция для авторизации пользователя
    function login_siteuser($siteusername, $password) {
        global $CONFIG, $db, $server, $lang,$db2;
        if (!$siteusername || !$password){
            return FALSE;
        }

        $siteusername	= $db->escape(strip_tags(substr($siteusername, 0, 255)));
        $password		= $db->escape(strip_tags(substr($password, 0, 255)));
        $query = "SELECT u.*, ug.group_id, g.name as group_name"
            ." FROM core_users u JOIN core_users_groups ug JOIN core_groups g"
            ." WHERE u.id=ug.user_id AND ug.group_id=g.id "
            ." AND u.email ='".$siteusername."' AND u.password = md5('".$password."')";

        $db->query($query);

        // установить сессию и ее переменные
        if ($db->nf() > 0) {
            $db->next_record();
            if ($db->f("active") != 1) return -1;

            $_SESSION["siteuser"]["is_authorized"]	= true;
            $_SESSION["siteuser"]["id"]				= $db->f("id");
            $_SESSION["siteuser"]["siteusername"]	= $db->f("username");
            $_SESSION["siteuser"]["client_type"]	= $db->f("client_type");
            $_SESSION["siteuser"]["last_visit"]		= $db->f("last_visit");
            $_SESSION["siteuser"]["email"]			= $db->f("email");
            $_SESSION["siteuser"]["group_id"][]		= $db->f("group_id");
            $_SESSION["siteuser"]["group_name"][]	= $db->f("group_name");
            while ($db->next_record()) {
                $_SESSION["siteuser"]["group_id"][]		= $db->f("group_id");
                $_SESSION["siteuser"]["group_name"][]	= $db->f("group_name");
            }
            if(!$this->isLoginDiler())
            {
                unset($_SESSION["siteuser"]);
                return FALSE;
            }
            else
            {
                $ip=$_SERVER['REMOTE_ADDR'];
                $arrC=$db2->getArrayOfResult("SELECT ip
                                FROM `sup_rus_countir`
                                GROUP BY ip");
                if(is_array($arrC))foreach($arrC as $key=>$val)
                {
                    $arr[]=$val['ip'];
                }
                if(!in_array($ip,$arr))
                {
                    $db2->set('sup_rus_countir',array("ip"=>$ip));
                }
            }
            $now = date("Y-m-d H:i:s") ;
            $query = "UPDATE core_users SET last_visit=NOW(), onsite=1 WHERE id = ".$_SESSION["siteuser"]["id"];
            $db->query($query);
            $_SESSION["siteuser"]["last_visit"]	= $now;
            //ini_set('session.gc_maxlifetime','2678400');
            //ini_set('session.cookie_lifetime', 2678400);
            //session_set_cookie_params(2678400);
            $_cook = preg_replace('~[,; \\t\\r\\n\\013\\014]+~i','',$CONFIG['sitename'].'-siteuser-siteusername');
            setcookie($_cook, $_SESSION["siteuser"]["siteusername"], time() + 2678400, "/");
            return TRUE;
        }

        return FALSE;
    }


    // функция для корректного завершения сессии пользователя
    function logout_siteuser()
    {
        global $CONFIG, $db, $server, $lang;

        $id = (int)$_SESSION["siteuser"]["id"];
        if ($this->is_authorized && $id) {
            unset($_SESSION["siteuser"]);
            unset($_SESSION["session_is_admin"]);
            unset($_SESSION["permissions"]);
            unset($_COOKIE[$CONFIG['sitename'].'-siteuser-siteusername']);
            // можно делать update даты последнего посещения при выходе пользователя
            $db->query('UPDATE core_users SET last_visit = NOW(), onsite=0 WHERE id = '.$id);
            return TRUE;
        }
        return FALSE;
    }


    // функция вывода формы для регистрации
    function show_registration_form($is_add = false, $block_name = 'block_form', $param = array())
    {
        global $CONFIG, $db, $db1, $tpl, $baseurl, $lang, $request_start;

        $username_list = $email_list = array();
        $users = $this->getUsers();
        if (! empty($users)) {
            foreach ($users as $val) {
                $username_list[] = $val['username'];
                $email_list[] = $val['email'];
            }
        }
        foreach ($param as $k => $v) {
            $param[$k] = htmlspecialchars($v);
        }

        if ( FALSE !== strpos($_SERVER['PHP_SELF'], "admin.php") ) {
            $admin = 1;
        }

        $tpl->newBlock($block_name);

        $user = false;
        if ($this->is_authorized && !$admin) {
            $form_heading = $this->_msg["Your_personal_info"];
            $user = $this->getUser($_SESSION["siteuser"]["id"], true);
            $form_action = $baseurl."&action=edit&nocash";
            $client_type = $user['client_type'];
            $tpl->newBlock('block_edit_title');
        } else {
            if ($admin) {
                if ($_REQUEST['id']) {
                    $form_heading	= $this->_msg["Edit_user_info"];
                    $form_action =  $baseurl."&action=updatesiteuser&start=".intval($request_start);
                    $user = $this->getUser($_REQUEST['id']);
                    $client_type = $user['client_type'];
                } else {
                    $form_heading	= $this->_msg["Add_new_user"];
                    $form_action =  $baseurl."&action=addsiteuser";
                    $client_type = 0;
                }
            } else {
                $form_heading	= $this->_msg["New_user_register_on_site"];
                if($_REQUEST['groups'])
                {
                    $form_action =  $baseurl."&action=registration&nocash&groups=".$_REQUEST['groups'];
                }
                else{
                    $form_action =  $baseurl."&action=registration&nocash";
                }
                $client_type = 0;
            }
            $tpl->newBlock('block_registration_title');
        }

        $tpl->gotoBlock($block_name);
        $this->addition_to_path = $form_heading;
        $tpl->assign(array(
                'form_heading'		=>	$form_heading,
                'form_action'		=>	$form_action,
                'cancel_link'		=>	$baseurl.'&action=siteusers',

                'img_max_width'		=>	$CONFIG['siteusers_img_max_width'],
                'img_max_height'	=>	$CONFIG['siteusers_img_max_height'],
                'img_max_size'		=>	$CONFIG['siteusers_img_max_size'],

                'siteuser_id'		=>	$user ? $user['id'] : 0,
                'username'  		=>	$user ? $user['username'] : ($_POST['siteusername'] ? htmlspecialchars($_POST['siteusername']) : ''),
                'email'     		=>	$user ? $user['email'] : ($_POST['email'] ? htmlspecialchars($_POST['email']) : ''),
                'groups'            =>  ! empty($user['groups']) ? implode(',', $user['groups']) : 3,
                'max_login_len'		=>	$CONFIG['siteusers_max_login_len'],
                'min_login_len'		=>	$CONFIG['siteusers_min_login_len'],
                'max_pass_len'		=>	$CONFIG['siteusers_max_pass_len'],
                'min_pass_len'		=>	$CONFIG['siteusers_min_pass_len'],

                'json_username_list' => json_encode($username_list),
                'json_email_list' => json_encode($email_list),

                "MSG_Enter_login" => $this->_msg["Enter_login"],
                "MSG_Enter_password" => $this->_msg["Enter_password"],
                "MSG_Confirm_password" => $this->_msg["Confirm_password"],
                "MSG_Passwords_differ" => $this->_msg["Passwords_differ"],
                "MSG_Enter_email" => $this->_msg["Enter_email"],
                "MSG_Enter_valid_email" => $this->_msg["Enter_valid_email"],
                "MSG_Login"			=> $this->_msg["Login"],
                "MSG_not_less_than"	=> $this->_msg["not_less_than"],
                "MSG_and_not_more_than" => $this->_msg["and_not_more_than"],
                "MSG_symbols"		=> $this->_msg["symbols"],
                "MSG_Password"		=> $this->_msg["Password"],
                "MSG_Email"			=> $this->_msg["Email"],
                "MSG_Image"			=> $this->_msg["Image"]
            ) + $param);


        if ($is_add) {
            $this->show_groups('block_groups');
            $tpl->newBlock('block_add_siteuser_js');
            $tpl->assign(Array(
                "MSG_Enter_login" => $this->_msg["Enter_login"],
                "MSG_Enter_password" => $this->_msg["Enter_password"],
                "MSG_Confirm_password" => $this->_msg["Confirm_password"],
                "MSG_Passwords_differ" => $this->_msg["Passwords_differ"],
                "MSG_Enter_email" => $this->_msg["Enter_email"],
                "MSG_Enter_valid_email" => $this->_msg["Enter_valid_email"],
                'max_login_len'		=>	$CONFIG['siteusers_max_login_len'],
                'min_login_len'		=>	$CONFIG['siteusers_min_login_len'],
                'max_pass_len'		=>	$CONFIG['siteusers_max_pass_len'],
                'min_pass_len'		=>	$CONFIG['siteusers_min_pass_len'],
            ));
            $tpl->newBlock('block_add_siteuser_fields');
            $tpl->assign(Array(
                "MSG_Login"			=> $this->_msg["Login"],
                "MSG_not_less_than"	=> $this->_msg["not_less_than"],
                "MSG_and_not_more_than" => $this->_msg["and_not_more_than"],
                "MSG_symbols"		=> $this->_msg["symbols"],
                "MSG_Password"		=> $this->_msg["Password"],
                "MSG_Confirm_password" => $this->_msg["Confirm_password"],
                "MSG_Email"			=> $this->_msg["Email"],
                'max_login_len'		=>	$CONFIG['siteusers_max_login_len'],
                'min_login_len'		=>	$CONFIG['siteusers_min_login_len'],
                'max_pass_len'		=>	$CONFIG['siteusers_max_pass_len'],
                'min_pass_len'		=>	$CONFIG['siteusers_min_pass_len'],
                'siteusername'		=>	htmlspecialchars($user ? $user['siteusername'] : @$_REQUEST['siteusername']),
                'email'				=>	htmlspecialchars($user ? $user['email'] : @$_REQUEST['email']),
                'password'			=>	$user ? $user['password'] : '',
                'confirm_password'	=>	$user ? $user['password'] : '',
            ));
            $tpl->gotoBlock($block_name);
        } else {
            if ($user['img']) {
                $tpl->newBlock('block_img_exist');
                $tpl->assign(array(
                    'path' => "{$CONFIG['siteusers_img_path']}{$user['img']}",
                    "MSG_Delete" => $this->_msg["Delete"],
                ));
            }
            $tpl->gotoBlock($block_name);
        }

        $this->showTypes($client_type);
        $this->showUserPropList($user ? $user['id'] : 0, array('is_registration' => $is_add ? true : false) );

        if($CONFIG["siteusers_captcha"] == "on") {
            $tpl->newBlock("block_captcha");
            $tpl->assign(array(
                'captcha_image_url'		=>	"/mod/siteusers/captcha/image.php?rand=" . rand(0, 100000),
                'captcha_maxlength'		=>	$CONFIG["siteusers_captcha_wordlength"],
            ));
        }

        $this->main_title = $form_heading;
    }


    // функция вывода формы для авторизации
    function show_login_form($transurl)
    {
        global $CONFIG, $tpl, $baseurl, $core, $server, $lang, $db, $db1;
        if (!$transurl) $transurl = $baseurl;
        $shop_page = $core->formPageLink($CONFIG["shop_page_link"], $CONFIG["shop_page_address"], $lang, 1);
        $friends_page = $core->formPageLink($CONFIG["friends_page_link"], $CONFIG["friends_page_address"], $lang, 1);

        if ($_REQUEST['action'] == 'logout') {$this->is_authorized = FALSE;}

        // если пользователь авторизован
        if ($this->is_authorized) {
            $siteusername	= $_SESSION["siteuser"]["siteusername"];
            $last_visit		= $_SESSION["siteuser"]["last_visit_format"];
            $group			= $_SESSION["siteuser"]["group_id"];

            $tpl->newBlock("block_authorized_siteuser");
            $tpl->assign(array(
                'siteusername'		=> $siteusername,
                'last_visit'			=> $last_visit,
                'type_text'			=> $type_text,
                'group_name'			=> implode(",", $_SESSION["siteuser"]["group_name"]),
                'group'				=> implode(",", $_SESSION["siteuser"]["group_id"]),
                'edit_profile_link'	=> $transurl."&action=edit",
                'change_psw_link'	=> $transurl."&action=changepsw",
                'orders_history_link'	=> $shop_page."&action=orders",
                'shopping_cart_link'	=> $shop_page,
                'blogs_link'			=> $core->formPageLink($CONFIG["blogs_page_link"], $CONFIG["blogs_page_address"], $lang)
                    .($CONFIG["rewrite_mod"] ? $siteusername.'/' : '&action=showblogs&siteuser='.$siteusername),
                'logout_link'			=> $transurl."&action=logout",
                'friends_link'          => $friends_page,
                'msg_link'  			=> $transurl."&action=msglist",
                'gallery_link'          => $core->formPageLink($CONFIG["gallery_page_link"], $CONFIG["gallery_page_address"], $lang, 1),
                'resume_link'           => $core->formPageLink($CONFIG["resume_page_link"], $CONFIG["resume_page_address"], $lang, 1),
                'vacancy_link'          => $core->formPageLink($CONFIG["vacancy_page_link"], $CONFIG["vacancy_page_address"], $lang, 1),
                'support_link'          => $core->formPageLink($CONFIG["support_page_link"], $CONFIG["support_page_address"], $lang, 0)
            ));
            if ($_SESSION["session_is_admin"]) {
                $tpl->newBlock("block_site_administration");
                if($_COOKIE['edit_content']==1){
                    $tpl->newBlock("block_administration_off");
                }
                else {
                    $tpl->newBlock("block_administration_on");
                }
            } else {
                $tpl->newBlock("block_site_administration".($CONFIG['siteusers_confirm_login'] ? '' : '_without_confirm'));
                $tpl->assign("site_administration_link", $transurl."&action=startadmin");
            }
        } else {
            $tpl->newBlock("block_login_form");
            $tpl->assign(array(
                siteusername			=>	$_COOKIE["{$CONFIG["sitename"]}-siteuser-siteusername"],
                form_login_action		=>	$transurl."&action=login",
                registration_link		=>	$transurl."&action=registration",
                remind_password_link	=>	$transurl."&action=remind",
            ));
        }
        return TRUE;
    }

    // функция получении информации о пользователе через id
    function get_siteuser($id)
    {
        global $CONFIG, $db1, $server, $lang;

        $id = intval($id);
        if (!$id) return FALSE;

        $where = ($CONFIG['siteusers_policy'] == 'site')
            ? ' s.alias = "'.substr($server, 0 , -1).'" AND u.site_id = s.id AND'
            : '';

        $query = "SELECT u.*, u.id as su_id, u.username as siteusername, ug.group_id,"
            ." GROUP_CONCAT(DISTINCT ug.group_id SEPARATOR ',') AS groups"
            ." FROM core_users u LEFT JOIN core_users_groups ug ON ug.user_id = u.id"
            ." WHERE u.id = ".$id
            .($CONFIG['siteusers_policy'] == 'site' ? " AND u.site_id=".$_SESSION['session_cur_site_id'] : '')
            ." GROUP BY u.id";
        $db1->query($query);

        return ($db1->nf() > 0) ? TRUE : FALSE;
    }

    // функция получении информации о пользователе через id с сайта первоначальной регистрации
    function get_siteuser_defaults($id)
    {
        global $CONFIG, $db1, $server, $lang;

        $id = intval($id);
        if (!$id) return FALSE;

        $query = "SELECT s.alias, sl.language
    					FROM core_users u, sites s, sites_languages sl
    					WHERE u.id = ".$id." AND u.site_id = s.id AND u.language_id = sl.id";
        $db1->query($query);
        if(!$db->nf()) return false;
        $db1->next_record();
        $query = "SELECT su.*, su.id as su_id, u.id, u.username as siteusername
    					FROM core_users u
    					LEFT JOIN core_users su ON su.id = u.id
    					WHERE u.id = ".$id;
        $db1->query($query);

        return ($db1->nf() > 0) ? TRUE : FALSE;
    }

    // функция получении информации о пользователе через siteusername
    function get_siteuser_by_siteusername($siteusername, $field='email')
    {
        global $CONFIG, $db, $server, $lang;

        $siteusername = $db->escape(strip_tags(substr($siteusername, 0, 255)));

        $db->query("SELECT * FROM core_users WHERE ".$db->escape($field)." = '".$siteusername."'");
        if ($db->nf() == 0) {
            return FALSE;
        }
        $db->next_record();

        return $db->f('id');
    }

    // функция напоминания пароля
    function remind_password($email)
    {
        global $CONFIG, $db, $core, $lang;

        if ($this->get_siteuser_by_siteusername($email)) {
            $subject = $this->_msg["Restore_password_on_site"] . " {$CONFIG["sitename_rus"]} ({$_SERVER['SERVER_NAME']})";
            $arr = $db->Record;
            $uid = md5(time().rand(0, 999999));
            $db->query("update core_users set uid='".$uid."' where id=".$arr['id']);
            $arr['hash'] = $uid;
            $arr['uid'] = $uid;
            $sus = array (
                'site'	=>	trim($_SERVER['SERVER_NAME'],'/'),
                'url'	=>	trim($core->formPageLink($CONFIG["siteusers_page_link"], $CONFIG["siteusers_page_address"], $lang),'/'),
                'date'	=>	date("d/m/Y G:i:s")
            );
            $body = $this->getMail('zapros', array_merge($arr,$sus));

            $mail = new Mailer();
            $mail->CharSet		= $CONFIG['email_charset'];
            $mail->ContentType	= "text/html"; //$CONFIG['email_type'];
            $mail->From			= $CONFIG['return_email'];
            $mail->FromName		= $CONFIG['sitename_rus'];
            $mail->Mailer		= 'mail';
            $mail->AddAddress($db->f("email"));
            $mail->Subject		= $subject;
            $mail->Body			= $body;
            return $mail->Send() ? 1 : -1;
        } else {
            return 0;
        }
    }

    function change_password($hash,$pass=false)
    {
        global $CONFIG, $db, $core, $lang;

        $ret = 0;
        if ($this->get_siteuser_by_siteusername($hash,'uid')) {
            if(!$db->f("email")) return -1;
            $subject = $this->_msg["Restore_password_on_site"] . " {$CONFIG["sitename_rus"]} ({$_SERVER['SERVER_NAME']})";
            $arr = $db->Record;
            if($pass === false)
            {
                $pass=uniqid('');
            }
            $sus = array (
                'site'	=>	trim($_SERVER['SERVER_NAME'],'/'),
                'url'	=>	trim($core->formPageLink($CONFIG["siteusers_page_link"], $CONFIG["siteusers_page_address"], $lang),'/'),
                'date'	=>	date("d/m/Y G:i:s"),
                'newpass'	=> $pass,
                'mailU'=>$db->f("email")
            );
            $body = $this->getMail('newpass', array_merge($arr,$sus));
            if($body){$db->query("update core_users set password=md5('{$sus['newpass']}') where id='{$arr['id']}'");}

            if ($db->f("email")) {
                $mail = new Mailer();
                $mail->CharSet		= $CONFIG['email_charset'];
                $mail->ContentType	= "text/html"; //$CONFIG['email_type'];
                $mail->From			= $CONFIG['return_email'];
                $mail->FromName		= $CONFIG['sitename_rus'];
                $mail->Mailer		= 'mail';
                $mail->AddAddress($db->f("email"));
                $mail->Subject		= $subject;
                $mail->Body			= $body;
                $mail->Send();
                $ret = 1;
            } else {
                $ret = -1;
            }
        }

        return $ret;
    }

    function getMail($n_tpl, $arr){
        if(is_array($arr)){ extract ($arr, EXTR_SKIP); }
        switch ($n_tpl) {
            case "zapros":
                $ret = $date."<br>\n Было активировано восстановление пароля, для смены пароля перейдите по ссылке "
                    ."<a href='http://{$site}/{$url}?action=remind&step=3&hash={$hash}'>"
                    ."http://{$site}/{$url}?action=remind&step=3&hash={$hash}</a>";
                break;
            case "newpass":
                $ret = $date."<br>\n Было активировано изменения пароля.<br>\n"
                    ."логин: {$mailU}<br>\n"
                    ."новый пароль: {$newpass}<br>\n"
                    ."<a href='http://{$site}'>http://{$site}</a>";
                break;
        }
        return ($ret) ? $ret : false;
    }
    // функция вывода списка пользователей
    function show_siteusers_list($blockname, $transurl, $group = "", $start = 1, $rows = "", $order_by = "", $word = "")
    {
        global $CONFIG, $db, $tpl, $baseurl, $server, $lang;

        // Вывод списка заглавных букв ников
        $query = "SELECT SUBSTRING(username,1,1) AS surname_letter FROM core_users GROUP BY surname_letter ORDER BY surname_letter";
        $db->query($query);
        if ($db->nf() > 0) {
            while($db->next_record()) {
                $surname_letter = $db->f('surname_letter');
                if($surname_letter < 'А') {
                    $alphapet_breaker = "<br>";
                } else {
                    $alphapet_breaker = "";
                }
                $tpl->newBlock("block_siteusers_surname_letters");
                $tpl->assign(array(
                    'siteusers_surname_letter'	=> $surname_letter,
                    'siteusers_alphapet_breaker'	=> $alphapet_breaker,
                ));
            }
        }

        if ("" == $order_by) $order_by = $CONFIG["siteusers_order_by"];

        $rows		= (int)$rows;
        $start		= (int)$start;
        $start_row	= ($start - 1) * $rows;
        $group_str	= ($group > 0) ? "group = ".intval($group)." AND" : "";
        $word_str	= ($word) ? "SUBSTRING(username,1,1) = '".$db->escape($word)."' AND" : "";

        $query = "SELECT * FROM core_users WHERE "
            .$group_str." ".$word_str." active = 1 ORDER BY ".$order_by
            ." LIMIT ".$start_row.",".$rows;

        $db->query($query);
        if ($db->nf() > 0) {
            while($db->next_record()) {
                $siteuser_group = $db->f("group");
                switch ($siteuser_group) {
                    case "1":
                        $siteusers_image = '<img src="/i/i_shar_r.gif" width=9 height=11 border=0 align="left" vspace="2">';
                        break;
                    case "2":
                        $siteusers_image = '<img src="/i/i_shar_bl.gif" width=9 height=11 border=0 align="left" vspace="2">';
                        break;
                    case "3":
                        $siteusers_image = '<img src="/i/i_shar_gr.gif" width=9 height=11 border=0 align="left" vspace="2">';
                        break;
                }

                $tpl->newBlock($blockname);
                $tpl->assign(array(
                    'siteusers_siteusername'	=> $db->f("siteusername"),
                    'siteusers_image'			=> $siteusers_image,
                    'siteusers_id'			=> $db->f('id'),
                ));
            }

            $tpl->gotoBlock("_ROOT");
            $tpl->assign(array(
                'all_knowledges_link'	=> $transurl,
                'cols'				=> $rows
            ));
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Закачать файл изображения пользователя.
     *
     * @param int    $user_id    ID поьзователя
     * @param string $field_name Название поля в БД и форме
     * @param string $old_file   Имя старого файла. Если указано то файл с таким именем будет удалён.
     * @return boolean
     */
    function uploadFile($user_id, $field_name, $old_file = '', $del_file = false)
    {   global $db, $server, $lang, $CONFIG;

        if ($del_file && $old_file) {
            if ( file_exists(RP.$CONFIG['siteusers_img_path'].$old_file)) {
                @unlink(RP.$CONFIG['siteusers_img_path'].$old_file);
            }
            $old_file = '';
            $db->set('core_users', array('img' => ''), array('id' => $user_id));
        }
        if (@$_FILES[$field_name]['name']) {
            if (preg_match('~\.(phtml|php|php3|php4|php5|shtml|cgi|exe|pl|asp|aspx|htaccess|htgroup|htpasswd)$~i', $_FILES[$field_name]['name'])) {
                Main::message_die("<span class=\"title\">" . $this->_msg["Disallowed_extension"] . " " . $_FILES[$field_name]['name'] . "!</span>");
                return FALSE;
            }

            $up_img = $_FILES[$field_name];
            if (is_uploaded_file($up_img['tmp_name'])) {
                /**
                 * Является ли файл изображением
                 */
                if (in_array($up_img['type'], $this->allow_img_typs)) {
                    // Проверка ограничений на макс. размер файла
                    if (filesize($up_img['tmp_name']) < $CONFIG['siteusers_img_max_size']*1024) {
                        // Проверка ограничений на макс. высоту и ширину
                        $img_prp = getimagesize($up_img['tmp_name']);


                            $typs = array(1 => 'gif', 2 => 'jpg', 3 => 'png');
                            $img_name = $user_id.".".$typs[$img_prp[2]];
                            if ($old_file && file_exists(RP.$CONFIG['siteusers_img_path'].$old_file)) {
                                @unlink($CONFIG['siteusers_img_path'].$old_file);
                            }
                            Main::checkPath(RP.$CONFIG['siteusers_img_path']);
                            if ( move_uploaded_file($up_img['tmp_name'], RP.$CONFIG['siteusers_img_path'].$img_name) ) {
                                if ($img_prp[0] < $CONFIG['siteusers_img_max_width'] && $img_prp[1] < $CONFIG['siteusers_img_max_height']) {
                                    $this->resize(RP.$CONFIG['siteusers_img_path'].$img_name,RP.$CONFIG['siteusers_img_path'].$img_name,182,213);
                                }
                                else{
                                    $this->crop(RP.$CONFIG['siteusers_img_path'].$img_name,RP.$CONFIG['siteusers_img_path'].$img_name);
                                    $this->resize(RP.$CONFIG['siteusers_img_path'].$img_name,RP.$CONFIG['siteusers_img_path'].$img_name,182,213);
                                }
                                @chmod (RP.$CONFIG['siteusers_img_path'].$img_name, 0666);
                                $query = $db->query('UPDATE core_users SET '.$field_name.'=\''.$img_name.'\' WHERE id='.$user_id);
                                return TRUE;
                            }

                    }
                }
            }
        }


        return FALSE;
    }

    /**
     * Существует ли группа
     *
     * @param int $id
     * @return boolean
     */
    function groupExist($id)
    {
        global $db;

        $id = (int) $id;
        if ($id < 1) {
            return FALSE;
        }
        $db->query('SELECT id FROM core_groups WHERE id='.$id);

        return $db->nf() > 0 ? TRUE : FALSE;
    }

    /**
     * Получить первую группу
     *
     * @return mixed
     */
    function getFirstGroup()
    {   global $db, $server, $lang;

        $query = 'SELECT id FROM core_groups ORDER BY id ASC LIMIT 1';
        $db->query($query);
        if ($db->nf() > 0) {
            $db->next_record();
            return $db->f('id');
        }

        return FALSE;
    }

    /**
     * Проверить логин на допустинмость
     *
     * @param string $login
     * @return int -1 - слишком короткий
     *             -2 - слишком длинный
     *             -3 - содержит не допустимый символ
     *              0 - ок
     */
    function isValidLogin($login)
    {
        global $CONFIG;

        $len = strlen($login);

        if ($len < $CONFIG['siteusers_min_login_len']) {
            return -1;
        }

        if ($len > $CONFIG['siteusers_max_login_len']) {
            if(isset($_REQUEST['groups']))
            {
                // if($_REQUEST['groups']!='dilers') return -2;
            }
            else{
                return -2;
            }

        }

        if (preg_match("/\W/", $login)) {
            if(isset($_REQUEST['groups']))
            {
                if($_REQUEST['groups']!='dilers') return -3;
            }
            else{
                return -3;
            }

        }

        return 1;
    }

    /**
     * Проверить пароля на допустинмость
     *
     * @param string $password
     * @return int -1 - слишком короткий
     *             -2 - слишком длинный
     *             -3 - содержит не допустимый символ
     *             -4 - не совпадают пароли
     *              0 - ок
     */
    function isValidPassword($password, $confirm_password = '')
    {
        global $CONFIG;

        $len = strlen($password);

        if ($len < $CONFIG['siteusers_min_pass_len']) {
            return -1;
        }

        if ($len > $CONFIG['siteusers_max_pass_len']) {
            return -2;
        }

        if (preg_match("/\W/", $password)) {
            return -3;
        }

        if ('' !== $confirm_password && $confirm_password !== $password) {
            return -4;
        }

        return 1;
    }

    /**
     * Проверить email на допустинмость
     *
     * @param string $email
     * @return boolean FALSE - email допустип, TRUE - email не допустип
     */
    function isValidEmail($email)
    {
        if (preg_match( "/^[\w0-9][-\w]*(\.[\w0-9][-\w]*)*@[\w0-9][-\w]*(\.[\w0-9][-\w]*)*\.\w{2,4}$/", $email)) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Вывода списка типов пользователей
     *
     * @param int $type
     * @return FALSE
     */
    function showTypes($type = 0)
    {   global $CONFIG, $tpl;

        $arr = '';
        foreach($CONFIG['siteusers_client_types'] AS $key => $val) {
            if (isset($CONFIG["catalog_show_price_{$key}"]) && $CONFIG["catalog_show_price_{$key}"]) {
                if (($type == $key) && $type != '') {
                    $arr .= '<option value="'.$key.'" selected>'.$val."\n";
                } else {
                    $arr .= '<option value="'.$key.'">'.$val."\n";
                }
            }
        }
        if ($arr) {
            $tpl->assign(array('client_type' => $arr));
        }

        return FALSE;
    }

    function showUserInfo($user_id, $transurl)
    {
        global $tpl, $CONFIG, $PAGE;

        $_transurl = $transurl.(false === strpos($transurl, "?") ? "?" : "&");
        $user = $this->getUser($user_id);
        if (!$user) {
            $tpl->newBlock('block_not_found');
            return FALSE;
        }

        $user_link =  $this->getUserLink($user["siteusername"]);
        $tpl->newBlock('block_user_info');
        $tpl->assign(array(
            "user_id" => $user_id,
            "siteusername" => $user["siteusername"],
            "email" => $user['email'],
            "transurl" => $_transurl,
            "referer" => htmlspecialchars($_SERVER['HTTP_REFERER']),
            "user_link" => $user_link,
            "css_class_active" => (strstr($user_link, $_REQUEST['link']) ? 'selected' : ''),
            'img_max_width'		=>	$CONFIG['siteusers_img_max_width'],
            'img_max_height'	=>	$CONFIG['siteusers_img_max_height'],
            'img_max_size'		=>	$CONFIG['siteusers_img_max_size'],
        ));

        if ($_SESSION["siteuser"]["id"] == $user_id) {
            $tpl->newBlock('block_edit');
            $tpl->assign(array(
                'baseurl' => $_transurl,
                'action' => $_transurl."action=edit",
                'user_id' => $user_id,
            ));
        }

        $this->showUserPropList($user ? $user['id'] : 0, array('public' => 1, 'with_value' => 1) );
        if ($user['img'] && file_exists(RP.$CONFIG['siteusers_img_path'].$user['img'])) {
            $tpl->newBlock('block_user_img');
            $tpl->assign(array(
                'path' => '/'.$CONFIG['siteusers_img_path'].$user['img'],
            ));
        } else {
            $tpl->newBlock('block_user_no_img');
        }

        if ($this->is_authorized && $_SESSION["siteuser"]["id"] != $user_id) {
            $tpl->newBlock('block_send_msg');
            $tpl->assign(array(
                'baseurl' => $_transurl,
                'action' => $_transurl."action=sendmsg",
                'user_id' => $user_id,
            ));
        }

        if (file_exists(RP.'mod/gallery/lib/class.GalleryPrototype.php')) {
            require_once(RP.'mod/gallery/lib/class.GalleryPrototype.php');
            $o = new GalleryPrototype('only_create_object');
            $res = $o->getCategories(array('owner' => $user_id));
            if ($res) {
                $tpl->newBlock('block_gallery');
                $tpl->assign(array(
                    'link' => Core::formPageLink($CONFIG["gallery_page_link"], $CONFIG["gallery_page_address"], $lang, 1)."action=userlist&user_id=".$user_id,
                    'css_class_active' => 'gallery' == $PAGE['main_module_name'] && $PAGE['current_user_id'] == $user_id
                            ? 'selected' : ''
                ));
            } elseif ($user_id == $_SESSION['siteuser']['id']) {
                $tpl->newBlock('block_gallery');
                $tpl->assign(array(
                    'link' => Core::formPageLink($CONFIG["gallery_page_link"], $CONFIG["gallery_page_address"], $lang, 1)."action=userctgrlist",
                    'css_class_active' => 'gallery' == $PAGE['main_module_name'] && $PAGE['current_user_id'] == $user_id
                            ? 'selected' : ''
                ));
            }
        }

        if (file_exists(RP.'mod/job/lib/class.JobPrototype.php')) {
            if (! class_exists('JobPrototype')) {
                require(RP.'mod/job/lib/class.JobPrototype.php');
            }
            $o = new JobPrototype('only_create_object');
            $res = $o->GetUserResume($user_id);
            if ($res) {
                $tpl->newBlock('block_resume');
                $tpl->assign(array(
                    'link' => Core::formPageLink($CONFIG["resume_page_link"], $CONFIG["resume_page_address"], $lang, 1)."action=resume&user_id=".$user_id,
                    'css_class_active' => 'job' == $PAGE['main_module_name'] && $PAGE['current_user_id'] == $user_id
                        && in_array($PAGE['action'], array('resume', 'userresume'))
                            ? 'selected' : ''
                ));
            } elseif ($user_id == $_SESSION['siteuser']['id']) {
                $tpl->newBlock('block_resume');
                $tpl->assign(array(
                    'link' => Core::formPageLink($CONFIG["resume_page_link"], $CONFIG["resume_page_address"], $lang, 1)."action=userresume",
                    'css_class_active' => 'job' == $PAGE['main_module_name'] && $PAGE['current_user_id'] == $user_id
                        && in_array($PAGE['action'], array('userresume'))
                            ? 'selected' : ''
                ));
            }
            $res = $o->hasUserVacncy($user_id);
            if ($res) {
                $tpl->newBlock('block_vacancy');
                $tpl->assign(array(
                    'link' => Core::formPageLink($CONFIG["vacancy_page_link"], $CONFIG["vacancy_page_address"], $lang, 1)."action=usrvcncsshw&user_id=".$user_id,
                    'css_class_active' => 'job' == $PAGE['main_module_name'] && $PAGE['current_user_id'] == $user_id
                        && in_array($PAGE['action'], array('uservacancies', 'usrvcncsshw', 'vacancy')) ? 'selected' : ''
                ));
            } elseif ($user_id == $_SESSION['siteuser']['id']) {
                $tpl->newBlock('block_vacancy');
                $tpl->assign(array(
                    'link' => Core::formPageLink($CONFIG["vacancy_page_link"], $CONFIG["vacancy_page_address"], $lang, 1)."action=uservacancy",
                    'css_class_active' => 'job' == $PAGE['main_module_name'] && $PAGE['current_user_id'] == $user_id
                        && in_array($PAGE['action'], array('uservacancy', 'uservacancies'))
                            ? 'selected' : ''
                ));
            }
        }

        if (file_exists(RP.'mod/friends/lib/class.FriendsPrototype.php')) {
            if (! class_exists('FriendsPrototype')) {
                require(RP.'mod/friends/lib/class.FriendsPrototype.php');
            }
            $o = new FriendsPrototype('only_create_object');
            $res = $o->hasUserForm($user_id);
            if ($res) {
                $tpl->newBlock('block_friends');
                $tpl->assign(array(
                    'link' => Core::formPageLink($CONFIG["friends_page_link"], $CONFIG["friends_page_address"], $lang, 1)."action=form&user_id=".$user_id,
                    'css_class_active' => 'friends' == $PAGE['main_module_name'] && $PAGE['current_user_id'] == $user_id
                            ? 'selected' : ''
                ));
            } elseif ($user_id == $_SESSION['siteuser']['id']) {
                $tpl->newBlock('block_friends');
                $tpl->assign(array(
                    'link' => Core::formPageLink($CONFIG["friends_page_link"], $CONFIG["friends_page_address"], $lang, 1)."action=userform",
                    'css_class_active' => 'friends' == $PAGE['main_module_name'] && $PAGE['current_user_id'] == $user_id
                            ? 'selected' : ''
                ));
            }
        }

        if (file_exists(RP.'mod/blogs/lib/class.BlogsPrototype.php')) {
            if (! class_exists('BlogsPrototype')) {
                require(RP.'mod/blogs/lib/class.BlogsPrototype.php');
            }
            $l = Core::formPageLink($CONFIG["blogs_page_link"], $CONFIG["blogs_page_address"], $lang, 1);
            $o = new BlogsPrototype('only_create_object');
            $l = $o->get_transurl($l, $user["siteusername"]);
            $res = $o->has_user_blog($user_id);
            if ($res || $user_id == $_SESSION['siteuser']['id']) {
                $tpl->newBlock('block_blog');
                $tpl->assign(array(
                    'link' => $l."action=showblogs",
                    'css_class_active' => 'blogs' == $PAGE['main_module_name'] && $PAGE['current_user_id'] == $user_id
                        && in_array($PAGE['action'], array('showblogs', 'showblog', 'addblog')) ? 'selected' : ''
                ));
            }
            $res = $o->has_friends($user_id);
            if ($res || $user_id == $_SESSION['siteuser']['id']) {
                $tpl->newBlock('block_blog_friends');
                $tpl->assign(array(
                    'link' => $l."action=friends&siteuser=".$user["siteusername"],
                    'css_class_active' => 'blogs' == $PAGE['main_module_name'] && $PAGE['current_user_id'] == $user_id
                        && in_array($PAGE['action'], array('friends', '')) ? 'selected' : ''
                ));
            }
            $res = $o->is_friend($user_id);
            if ($_SESSION["siteuser"]["id"] && $user_id != $_SESSION["siteuser"]["id"] && !$res) {
                $tpl->newBlock('block_add_firend');
                $tpl->assign(array(
                    'link' => $l."action=addfriend&siteuser=".$user["siteusername"],
                ));
            }
        }
    }

    function do_export_XML($user_id = 0)
    {
        global $CONFIG, $lang;

        $doc = domxml_new_doc("1.0");
        $root = $doc->add_root("users");
        $root->set_attribute("date", gmdate('d.m.Y H:i:s').date("O"));
        $root->set_attribute("lang", $lang);
        if ($this->get_users_XML($doc, $root, $user_id)) {
            $out = $doc->dump_mem(TRUE, 'UTF-8');

            $file	= RP.$CONFIG['siteusers_export_path'].'users_'.gmdate('Ymd_His').substr(date("O"), 0, 3).'.xml';
            $handle = fopen($file, 'w');
            fwrite($handle, $out);
            fclose($handle);
            return true;
        } else {
            return false;
        }
    }

    function get_users_XML(&$doc, &$parent, $user_id)
    {
        global $db, $CONFIG;

        $query = "SELECT ".
            " u.id, u.client_type, u.username, u.password, u.email, u.active, DATE_FORMAT(u.last_visit,'%Y.%m.%d %H:%i:%s') AS last_visit, ".
            " u.is_new, GROUP_CONCAT(DISTINCT ug.group_id SEPARATOR ';') AS groups".
            " FROM core_users u ".
            " left join core_users_groups AS ug on (u.id=ug.user_id) ".
            " GROUP BY u.id ORDER BY u.id ";

        $db->query($query);
        if ($db->nf()) {
            $groups_list = $this->getGroups();
            while ($db->next_record()) {
                $user = $doc->create_element("user");
                $user->set_attribute("id", $db->f('id'));
                $user->set_attribute("type", $db->f('client_type'));
                $user = $parent->append_child($user);
                $groups = explode(";", $db->f('groups'));
                if (! empty($groups)) {
                    foreach ($groups as $key => $val) {
                        $group = $user->new_child("group", $this->conv_str($groups_list[$val]));
                        $group->set_attribute("id", $val);
                    }
                }
                //if ($db->f('password')) $group = $user->new_child("password", $this->conv_str($db->f('password')));
                if ($db->f('username')) $el = $user->new_child("username", $this->conv_str($db->f('username')));
                if ($db->f('email')) $el = $user->new_child("email", $this->conv_str($db->f('email')));
                //if ($db->f('active')) $group = $user->new_child("active", $this->conv_str($db->f('active')));
                if ($db->f('last_visit')) $el = $user->new_child("last_visit", $this->conv_str($db->f('last_visit')));
                //if ($db->f('is_new')) $group = $user->new_child("is_new", $this->conv_str($db->f('is_new')));
            }
            return true;
        } else {
            return false;
        }
    }

    function getPropertyFields() {
        GLOBAL $request_sub_action;
        if ('' != $request_sub_action) {

            $tpl = $this->getTmpProperties();

            switch ($request_sub_action) {
                default:
                    $tpl->newBlock('block_properties_none');
                    $tpl->assign(Array(
                        "MSG_No" => $this->_msg["No"],
                    ));
                    break;
            }
            return $this->getOutputContent($tpl->getOutputContent());
        }
        return FALSE;
    }

    function getEditLink($sub_action = NULL, $block_id = NULL) {
        GLOBAL $request_name, $db, $lang, $server;
        $block_id	= (int)$block_id;
        if ($sub_action && $block_id) {
            $db->query('SELECT	property1,
								property2,
								property3,
								property4,
								property5
							FROM '.$this->table_prefix.'_pages_blocks
							WHERE id = '.$block_id);
            if ($db->nf() > 0) {
                $db->next_record();
                $arr[]	= $db->f('property1');
                $arr[]	= $db->f('property2');
                $arr[]	= $db->f('property3');
                $arr[]	= $db->f('property4');
                $arr[]	= $db->f('property5');

                switch ($sub_action) {
                    default:
                        $link['material_url']	= '';
                        $link['material_id']	= '';
                        break;
                }
                return $link;
            }

            switch ($sub_action) {
                default:
                    $link['material_url']	= '';
                    $link['material_id']	= '';
                    break;
            }
            return $link;
        }
        return FALSE;
    }

    // Функция для вывода списка пользователей
    function show_siteusers_list_adm($blockname, $filter = array(), $retArr = false)
    {
        global $CONFIG, $db, $tpl, $baseurl, $server, $lang;

        $start = $filter['start']>1 ? (int)$filter['start'] : 1;
        $rows = $filter['row']>1 ? (int)$filter['row'] : $CONFIG['siteusers_max_rows'];
        $start_row = ($start - 1) * $rows;
        $order_by = $filter['order'] ? $filter['order'] : $CONFIG['siteusers_order_by'];
        $group = (int)$filter['group'];
        $where = ($group) ? 'ug.group_id = '.$group.' AND ug.user_id = u.id AND' : '';
        $where .= ($CONFIG['siteusers_policy'] == 'site')
            ? ' u.site_id = '.$_SESSION['session_cur_site_id'].' AND'
            : '';
        if ($filter['q']) {
            $q = str_replace(array('_', '%'), array('\_', '\%'), My_Sql::escape($filter['q']));
            $where .= "(u.username LIKE '%{$q}%' OR u.email LIKE '%{$q}%') AND ";
        }
        $group_table = ($group) ? ', core_users_groups ug' : '';

        $query = 'SELECT u.id FROM core_users u'.$group_table.' WHERE '.$where.' 1';

        $db->query($query);
        $users_cnt = $db->nf();
        $pages_cnt = ceil($users_cnt / $rows);
        for($i=0; $i < $start_row; $i++) $db->next_record();
        $users_list = array();
        for(; $i < min($start_row + $rows, $users_cnt); $i++) {
            $db->next_record();
            $users_list[] = $db->f('id');
        }
        $users_list = implode(',', $users_list);

        $query = 'SELECT u.*,
						 u.id as siteuserid,
						 u.username as siteusername,
						 GROUP_CONCAT(DISTINCT ug.group_id SEPARATOR ";") AS groups
					FROM core_users u
						 LEFT JOIN core_users_groups ug ON ug.user_id = u.id
					WHERE u.id IN ('.(($users_list)?$users_list:0).')
					GROUP BY u.id ORDER BY '.$order_by;
        $db->query($query);

        if($retArr && $db->nf() > 0){
            while($db->next_record(1)){$ret[] = $db->Record;}
            return $ret;
        }

        if ($db->nf() > 0) {
            $groups_list = $this->getGroups();
            while($db->next_record()) {
                // вывод на экран списка
                $id = $db->f('siteuserid');
                $tpl->newBlock($blockname);
                $tpl->newBlock('block_siteuser');
                $groups = explode(";", $db->f('groups'));
                $email=$db->f('email');
                if (! empty($groups)) {
                    foreach ($groups as $key => $val) {
                        if($val ==3 && !isset($l))
                        {
                            $email="<a href='/admin.php?lang=rus&name=siteusers&action=diler&id=".$db->f('id')."'>".$email."</a>";
                            $l=1;
                        }
                    }
                    unset($l);
                }

                $tpl->assign(Array(
                    "MSG_Edit"			=> $this->_msg["Edit"],
                    "MSG_Confirm_delete_user" => $this->_msg["Confirm_delete_user"],
                    "MSG_Delete"		=> $this->_msg["Delete"],
                    'username'		=>	$db->f('siteusername'),
                    'email'		=>	$email,
                    'siteuser_edit_link'	=>	$baseurl.'&action=editsiteuser&id='.$id.'&start='.$start.'&group='.$group,
                    'siteuser_del_link'	=>	$baseurl.'&action=delsiteuser&id='.$id.'&start='.$start.'&group='.$group,
                    'change_status_link'	=>	$baseurl.'&action='.(($db->f('active')) ? 'suspend' : 'activate').'&id='.$id.'&start='.$start.'&group='.$group,
                    'action_text'			=>	($db->f('active')) ? $this->_msg["Suspend_access"] : $this->_msg["Activate_access"],
                    'action_img'			=>	($db->f('active')) ? 'ico_unlock.gif' : 'ico_lock.gif',
                    'siteuser_id'			=>	$id,
                    'last_visit'			=>	$db->f('last_visit'),
                    'reg_date'			=>	$db->f('reg_date')
                ));


                if (! empty($groups)) {
                    foreach ($groups as $key => $val) {
                        $tpl->newBlock('block_user_group');
                        $tpl->assign(array(
                            'link' => $baseurl."&action=siteusers&group=".$val,
                            'group' => $groups_list[$val],
                        ));
                    }
                }
            }

            return $pages_cnt;
        }
    }

    // функция для удаления пользователя из базы данных
    function delete_siteuser($id)
    {
        global $CONFIG, $db, $db1, $server, $lang;

        $id		= (int)$id;

        if (!$this->get_siteuser($id)) {
            return FALSE;
        }
        $db1->next_record();
        $user = $db1->Record;
        $db->del('core_users_prop_val', array('user_id' => $id));
        $db->del('core_users_prop_val_txt', array('user_id' => $id));
        $db->del('core_users_groups', array('user_id' => $id));
        $db->del('core_users', array('id' => $id));
        $db->query('DELETE FROM core_users_msg WHERE `from` = '.$id.' OR `to`='.$id);
        if (file_exists(RP."/mod/blogs")) {
            $db->query('DELETE FROM '.$this->table_prefix.'_blogs WHERE author_id = '.$id);
        }
        /* Удаление анкеты пользователя */
        if (file_exists(RP."/mod/friends")) {
            if (! class_exists('FriendsPrototype')) {
                include(RP."/mod/friends/lib/class.FriendsPrototype.php");
            }
            $friends = new FriendsPrototype('only_create_object');
            $form = $friends->GetUserForm($id);
            if ($form) {
                $friends->DelForm($form['id']);
            }
        }
        /* Удаление вакансии и резюме */
        if (file_exists(RP."/mod/job")) {
            if (! class_exists('JobPrototype')) {
                include(RP."/mod/job/lib/class.JobPrototype.php");
            }
            $job = new JobPrototype('only_create_object');
            $resume = $job->GetUserResume($id);
            if ($resume) {
                $job->DelResume($resume['id']);
            }
            $vacanc_list = $job->GetVacancyList(1, 1000, array('owner_id' => $id));
            if (! empty($vacanc_list)) {
                foreach ($vacanc_list as $key => $val) {
                    $job->DelVacancy($val['id']);
                }
            }
        }

        return ($db->affected_rows() > 0) ? 1 : 0;
    }

    // функция для вывода списка имеющихся групп пользователей
    function show_groups($blockname, $selected_group = array())
    {
        global $CONFIG, $db, $tpl, $baseurl;

        $query = 'SELECT * FROM core_groups ORDER BY id';
        $db->query($query);
        if ($db->nf() > 0) {
            $i = 0;
            while($db->next_record() && ++$i) {
                // вывод на экран списка
                $id			= $db->f('id');
                $selected	= (in_array($id, $selected_group)) ? ' selected' : '';
                $tpl->newBlock($blockname);
                $tpl->assign(array(
                    'group_id'			=>	$id,
                    'group_name'		=>	$db->f('name'),
                    'group_description'	=>	$db->f('description'),
                    'group_link'		=>	$baseurl.'&action=siteusers&group='.$id,
                    'group_edit_link'	=>	$baseurl.'&action=editgroup&id='.$id,
                    'group_edit_role_link'  =>	$baseurl.'&action=editgrouproles&id='.$id,
                    'selected'			=>	$selected,
                    "MSG_Edit"			=>	$this->_msg["Edit"],
                    "MSG_Edit_role_set"	=>	$this->_msg["Edit_role_set"],
                ));
                if ($i > 1) {
                    // для групп с ид больше 2 выводим иконку для удаления
                    $tpl->newBlock("block_delete_groups");
                    $tpl->assign(array(
                        'group_del_link' =>	$baseurl.'&action=delgroup&id='.$id,
                        "MSG_Delete" =>	$this->_msg["Delete"],
                    ));
                }
            }
            return $db->nf();
        }
        return FALSE;
    }

    // функция для вывода группы и ее прав доступа по id
    function show_group($id = '')
    {
        global $CONFIG, $main, $db, $tpl, $baseurl;

        $id = (int)$id;
        if (!$id) return FALSE;
        $query = 'SELECT * FROM core_groups WHERE id = '.$id;
        $db->query($query);

        if ($db->nf() > 0) {
            $db->next_record();
            $name			= htmlspecialchars($db->f('name'));
            $description	= htmlspecialchars($db->f('description'));
            $checked		= ($db->f('active')) ? 'checked' : '';

            //вывод на экран
            $tpl->assign(array(
                'group_name'			=>	$name,
                'group_description'	=>	$description,
                'group_id'			=>	$db->f('id'),
                'group_link'			=>	$baseurl.'&action=siteusers&group='.$db->f('id'),
                'checked'				=>	$checked,
            ));
            return $db->f('owner_id');
        }
    }

    // функция для добавления группы пользователей в базу данных
    function add_group($group_name, $group_owner, $group_description)
    {
        global $CONFIG, $db, $server, $lang;

        if (!$group_name) return FALSE;
        $group_name			= $db->escape($group_name);
        $group_description	= $db->escape($group_description);
        $query = 'INSERT INTO core_groups SET name = "'.$group_name.'", description = "'.$group_description.'",'
            .' owner_id = '.(in_array(1, $_SESSION["siteuser"]["group_id"]) ? $group_owner : $_SESSION["siteuser"]["group_id"]);
        $db->query($query);
        return ($db->affected_rows() > 0) ? $db->lid() : 0;
    }


    // функция для обновления информации о группе
    function update_group($group_name, $group_owner, $group_description, $active, $id)
    {
        global $CONFIG, $db, $server, $lang;

        $id		= (int)$id;
        $active = (int)$active;
        if (!$id || !$group_name || !$this->groupExist($id)) {
            return FALSE;
        }
        $group_name			= $db->escape($group_name);
        $group_description	= $db->escape($group_description);
        $query = 'UPDATE core_groups SET name = "'.$group_name.'", description = "'.$group_description.'"'
            .(in_array(1, $_SESSION["siteuser"]["group_id"]) ? ', owner_id = '.intval($group_owner) : '')
            .' WHERE id = '.$id;
        $db->query($query);

        return TRUE;
    }


    // функция для удаления группы пользователей из базы данных
    function delete_group($id)
    {
        global $CONFIG, $db, $server, $lang;

        $id = (int)$id;
        if (!$id || $id < 2 || !$this->groupExist($id)) {
            return FALSE;
        }
        $db->query('DELETE FROM core_groups WHERE id = '.$id);
        if ($db->affected_rows() > 0) {
            $db->query('DELETE FROM core_users_groups WHERE group_id = '.$id);
            $this->delete_group_permissions($id);
            $this->deleteGroupRoles($id);
            return TRUE;
        }
        return FALSE;
    }


    // функция для обновления пароля
    function update_password($password, $id)
    {
        global $CONFIG, $db;

        $id = (int)$id;
        if (!$id || !$password) return FALSE;
        $password	= addslashes(trim($password));
        $db->query('UPDATE core_users SET password = md5("'.$password.'") WHERE id = '.$id);
        return TRUE;
    }

    // функция для вывода прав доступа группы
    function show_group_permissions($id = NULL) {
        global $CONFIG, $db, $tpl, $core;
        $this->get_bound_pages_in_tree($id);
        $this->get_unbound_pages_in_tree($id);
        return TRUE;
    }

    // получение дерева сайта для редактирования
    function get_bound_pages_in_tree($group_id)
    {
        global $CONFIG, $core, $db, $db1, $tpl, $baseurl, $CONFIG, $server, $lang;

        $home_page = (isset($lang) && $lang == "eng") ? "Home" : $this->_msg["Mainpage"];
        $home_link = $core->formPageLink("", "", $lang);
        $site_id = (int) $_SESSION['session_cur_site_id'];
        $lang_id = (int) $_SESSION['session_cur_lang_id'];

        $icon	= '';
        $menu 	= new TreeMenu();
        $text	= '<span id=b_page_0><a href="'.$home_link.'" target=_blank title="' . $this->_msg["see_page_on_site"] . '"><img src="'.$CONFIG['admin_img_path'].'/ico_ishome.gif" border=0 align=middle></a>&nbsp;<span onClick="selectParentPage(0);" style="cursor: hand;">'.$home_page.'</span></span>';
        $array	= array(
            'text'			=> $text,
            'icon'			=> $icon,
            'isDynamic'		=> TRUE,
            'ensureVisible'	=> TRUE,
        );
        $node[0] = new TreeNode($array);
        $menu->addItem($node[0]);
        $db->query('SELECT P.id, P.parent_id, P.title, P.address, P.link, P.active, P.permission_type,'
            .($group_id ? ' GP.show' : '0 AS `show`')
            .' FROM '.$this->table_prefix.'_pages AS P'
            .($group_id
                ? ' LEFT JOIN core_group_permissions AS GP ON (P.id=GP.page_id AND GP.group_id = '.$group_id.' AND GP.site_id='.$site_id.' AND lang_id='.$lang_id.')'
                : ''
            )
            .' WHERE !ISNULL(parent_id) AND is_default != 1 ORDER BY page_rank DESC');
        if ($db->nf() > 0) {
            while($db->next_record()) {
                $id			= $db->f('id');
                $pid		= $db->f('parent_id');
                $title		= str_replace(array("\n", "\r"), array(" ", "2&nbsp;"), $db->f('title'));
                $title		= addslashes($title);
                $page_link	= $core->formPageLink($db->f('link'), $db->f('address'), $lang);
                $page_checked = $db->f('show') ? ' checked' : '';
                $img = ($db->f('active')) ? 'ico_page.gif' : 'ico_page_noactive.gif';
                if ((1 == $db->f('permission_type')) || (3 == $db->f('permission_type'))) {
                    $text = '<span style="padding: 0 2px; margin-top: 0px; margin-bottom: 0px;" id=unb_page_'.$id.'><input style="padding: 0 2px; margin-top: 0px; margin-bottom: 0px;" type="checkbox" name="p['.$id.']" value="1" class="checkb"'.$page_checked.'><a href="'.$page_link.'" target="_blank"><img src="/'.$CONFIG['admin_img_path'].$img.'" border=0 align=middle></a>&nbsp;'.$title.'</span>';
                } else {
                    $text = '<span id=unb_page_'.$id.'><a href="'.$page_link.'" target="_blank"><img src="/'.$CONFIG['admin_img_path'].'ico_page_off.gif" border=0 align=middle></a>&nbsp;<font color="#959595">'.$title.'</font></span>';
                }
                $array	= array(
                    'text'			=> $text,
                    'icon'			=> $icon,
                    'isDynamic'		=> TRUE,
                    'ensureVisible'	=> TRUE,
                );

                $node[$id]		= new TreeNode($array);
                $arr[$id]		= $array;
                $child_nodes   .= "if (is_object(\$node[$pid])) \$node[$pid]->addItem(\$node[$id]);";
            }
            eval($child_nodes);
        }

        $treeMenu = &new TreeMenu_DHTML(
            $menu,
            array(
                'images'				=> '/'.$CONFIG['tree_img_path'],
                'defaultClass'		=> 'treeMenuDefault',
                'noTopLevelImages'	=>	FALSE,
            )
        );
        $bound_output = $treeMenu->toHTML();
        $tpl->assign(array(
            '_ROOT.bound_pages_tree'	=>	$bound_output,
        ));
        return TRUE;
    }

    // функция для получения списка непривязанных страниц
    function get_unbound_pages_in_tree($group_id)
    {
        global $CONFIG, $core, $db, $db1, $tpl, $baseurl, $CONFIG, $server, $lang;

        $icon	= '';
        $site_id = (int) $_SESSION['session_cur_site_id'];
        $lang_id = (int) $_SESSION['session_cur_lang_id'];
        $menu	= new TreeMenu();
        $text	= '<span><img src="/'.$CONFIG['admin_img_path'].'ico_tree.gif" border=0 align=middle>&nbsp;<span style="cursor: hand;">' . $this->_msg["Unbound_pages"] . '</span></span>';
        $array = array(
            'text'			=> $text,
            'icon'			=> $icon,
            'isDynamic'		=> TRUE,
            'ensureVisible'	=> TRUE,
        );
        $node[0] = new TreeNode($array);
        $menu->addItem($node[0]);
        $db->query('SELECT P.id, P.parent_id, P.title, P.address, P.link, P.active, P.permission_type,'
            .($group_id ? ' GP.show' : '0 AS `show`')
            .' FROM '.$this->table_prefix.'_pages AS P'
            .($group_id
                ? ' LEFT JOIN core_group_permissions AS GP ON (P.id=GP.page_id AND GP.group_id = '.$group_id.' AND GP.site_id='.$site_id.' AND lang_id='.$lang_id.')'
                : ''
            )
            .' WHERE ISNULL(parent_id) AND is_default != 1 AND (permission_type = 1 OR permission_type = 3)'
            .' ORDER BY page_rank DESC');

        if ($db->nf() > 0) {
            while($db->next_record()) {
                $id			= $db->f('id');
                $pid		= 0; // так как parent на самом деле NULL для непривязанных страниц
                $title		= str_replace(array("\n", "\r"), array(" ", "2&nbsp;"), $db->f('title'));
                $title		= addslashes($title);
                $page_link	= $core->formPageLink($db->f('link'), $db->f('address'), $lang);
                $page_checked = $db->f('show') ? ' checked' : '';
                $img = ($db->f('active')) ? 'ico_page.gif' : 'ico_page_noactive.gif';
                $text = '<span id=unb_page_'.$id.'><input type="checkbox" name="p['.$id.']" value="1" class="checkb"'.$page_checked.'><img src="/'.$CONFIG['admin_img_path'].$img.'" border=0 align=middle>&nbsp;'.$title.'</span>';

                $array	= array(
                    'text'			=> $text,
                    'icon'			=> $icon,
                    'isDynamic'		=> TRUE,
                    'ensureVisible'	=> TRUE,
                );

                $node[$id]		= new TreeNode($array);
                $arr[$id]		= $array;
                $child_nodes   .= "if (is_object(\$node[$pid])) \$node[$pid]->addItem(\$node[$id]);";
            }
            eval($child_nodes);
        }
        $treeMenu = &new TreeMenu_DHTML(
            $menu,
            array(
                'images'			=> '/'.$CONFIG['tree_img_path'],
                'defaultClass'		=> 'treeMenuDefault',
                'noTopLevelImages'	=>	FALSE
            )
        );
        $unbound_output = $treeMenu->toHTML();
        $tpl->assign(array(
            '_ROOT.unbound_pages_tree'	=>	$unbound_output,
        ));
        return TRUE;
    }

    // функция для обновления информации о правах группы
    function set_group_permissions($group, $p = array())
    {
        global $db;

        $group = (int)$group;
        $site = (int) $_SESSION['session_cur_site_id'];
        $lang = (int) $_SESSION['session_cur_lang_id'];
        if (!$this->groupExist(group) && $site < 1 && $lang < 1) {
            return FALSE;
        }
        $insrt = array();
        $arr = $this->get_pages_ids();
        foreach ($arr as $idx => $pg) {
            $insrt[] = ' ('.$group.', '.intval($pg).', '.$site.', '.$lang.', '.($p[$pg] ? 1 : 0).')';
        }
        if (! empty($insrt)) {
            $db->query('REPLACE INTO core_group_permissions (`group_id`, `page_id`, `site_id`, `lang_id`, `show`)'
                .' VALUES '.implode(", ", $insrt));
        }
        return TRUE;
    }


    // функция для удаления прав группы из базы данных
    function delete_group_permissions($id)
    {
        global $CONFIG, $db;

        $id = (int)$id;
        if (!$id || $id == 1) {
            return FALSE;
        }
        $query = 'DELETE FROM core_group_permissions WHERE group_id = '.$id;
        $db->query($query);

        return TRUE;
    }


    // функция для получения списка pages
    function get_pages_ids() {
        global $CONFIG, $db, $server, $lang;
        $query = 'SELECT * FROM '.$this->table_prefix.'_pages ORDER BY id';
        $db->query($query);
        if ($db->nf() > 0) {
            while($db->next_record()) {
                $arr[] = $db->f('id');
            }
            return $arr;
        }
        return array();
    }

    // функция для наследования прав доступа
    function permissions_inheritance() {
    }

    /**
     * Информировать пользователя об изменении права доступа к сайту
     *
     * @param int $id
     * @return boolean
     */
    function informUser($id)
    {   global $CONFIG, $main, $db, $server, $lang, $tpl;

        $id = (int)$id;
        if ($id < 1) {
            return FALSE;
        }

        $db->query('SELECT email, active FROM core_users WHERE id='.$id);
        if ($db->nf() > 0) {
            while($db->next_record()) {
                $email  = $db->f('email');
                $active = $db->f('active');
            }
        } else {
            return FALSE;
        }

        if (!$email) {
            return FALSE;
        }

        $block_name = $active ? 'block_activate' : 'block_suspend';

        $main->include_main_blocks_2($this->module_name.'_inform_user.html', $this->tpl_path, 'main');
        $tpl->prepare();
        $tpl->newBlock($block_name);
        $tpl->assign(Array(
            "MSG_Access_activated" => $this->_msg["Access_activated"],
            "MSG_Use_support_service" => $this->_msg["Use_support_service"],
            "MSG_Info_welcome" => $this->_msg["Info_welcome"],
            "MSG_Access_suspended" => $this->_msg["Access_suspended"],
            "MSG_Use_support_service" => $this->_msg["Use_support_service"],
        ));
        $tpl->assign(
            array('site_name' => $CONFIG['sitename_rus'],
                'site_address' => $CONFIG['web_address'],
                'recipient' => $CONFIG['return_email']
            )
        );
        $body = $tpl->getOutputContent();
        $subj = $this->_msg["Message_from"] . " '".$CONFIG['sitename_rus']."' (".$CONFIG['web_address'].")";
        $mail = new Mailer();
        $mail->CharSet		= $CONFIG['email_charset'];
        $mail->ContentType	= "text/html"; //$CONFIG['email_type'];
        $mail->From			= $CONFIG['return_email'];
        $mail->FromName		= $CONFIG['sitename_rus'];
        $mail->Mailer		= 'mail';
        $mail->AddAddress($email);
        $mail->Subject		= $subj;
        $mail->Body			= $body;
        $mail->Send();

        return TRUE;
    }

    function getRoles($start = NULL, $pages = null)
    {
        GLOBAL $CONFIG, $db, $baseurl;

        if($pages){
            $db->query('SELECT count(*) FROM core_roles a,sites b,sites_languages c WHERE a.site_id = b.id AND a.lang_id = c.id');
            $db->next_record();
            return ceil($db->Record[0]/$CONFIG['siteusers_max_rows']);
        }
        $start = (int)$start;
        if($start<1) $start = 1;
        $b = intval(($start-1)*$CONFIG['siteusers_max_rows']);
        $db->query('SELECT a.*, b.title site_title, c.language site_lang
							FROM core_roles a,
								 sites b,
								 sites_languages c
							WHERE a.site_id = b.id AND
								  a.lang_id = c.id
							ORDER BY a.id
							LIMIT '.$b.','.$CONFIG['siteusers_max_rows']);
        if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]['id']				= $db->f('id');
                $arr[$i]['site_id']			= $db->f('site_id');
                $arr[$i]['lang_id']			= $db->f('lang_id');
                $arr[$i]['site_title']		= $db->f('site_title');
                $arr[$i]['site_lang']		= $db->f('site_lang');
                $arr[$i]['role_descr']		= $db->f('descr');
                $arr[$i]['role_edit_link']	= $baseurl.'&action=editrole&id='.$db->f('id');
                $arr[$i]['role_del_link']	= $baseurl.'&action=delrole&id='.$db->f('id');

                for ($h = 0, $j = sizeof($CONFIG['site_langs']); $h < $j; $h++) {
                    if ($CONFIG['site_langs'][$h]['value'] == $arr[$i]['site_lang']) {
                        $arr[$i]['role_site_lang']	= strtoupper($arr[$i]['site_title']).' ('.$CONFIG['site_langs'][$h]['name'].')';
                    }
                }
            }
        }
        return $arr;
    }

    function getRole($id = NULL)
    {
        GLOBAL $db;

        $id = ($id);
        if ($id) {
            $db->query('SELECT * FROM core_roles WHERE id = '.$id);
            if ($db->nf() > 0) {
                $db->next_record();
                $arr['role_descr']	= $db->f('descr');
                $arr['site_id']	= $db->f('site_id');
                $arr['lang_id']	= $db->f('lang_id');
            }
            return $arr;
        }
        return FALSE;

    }

    function setRole($id, $site_lang_role, $descr)
    {
        GLOBAL $CONFIG, $db;

        $id			= (int)$id;
        $sites		= explode('/', $site_lang_role);
        $site_id	= (int)$sites[0];
        $lang_id	= (int)$sites[1];
        $descr		= $db->escape($descr);

        if ($site_id && $lang_id) {
            if ($id) {
                $db->query('UPDATE core_roles SET site_id = '.$site_id.', lang_id = '.$lang_id.', '
                    .'descr = "'.$descr.'" WHERE id = '.$id);
            } else {
                $db->query('INSERT INTO core_roles (site_id, lang_id, descr) VALUES '
                    .'('.$site_id.', '.$lang_id.', "'.$descr.'")');
            }
            return ($db->affected_rows() > 0) ? $db->lid() : FALSE;
        }
        return FALSE;
    }

    function delRole($id)
    {
        GLOBAL $db;

        $id = (int)$id;
        if ($id) {
            $db->query('DELETE FROM core_roles WHERE id = '.$id);
            $db->query('DELETE FROM core_roles_permissions WHERE role_id = '.$id);
            $db->query('DELETE FROM core_roles_groups WHERE role_id = '.$id);
            return TRUE;
        }
        return FALSE;
    }

    function getRolePermissions($id) {
        GLOBAL $db;
        $id = (int)$id;
        if ($id) {
            $db->query('SELECT *, a.id mod_id, a.title title FROM modules a '
                .'LEFT JOIN core_roles_permissions b ON b.module_id = a.id AND b.role_id = '.$id
                .' WHERE a.parent_id>0 ORDER BY b.id');
        } else {
            $db->query('SELECT id mod_id, title  FROM modules WHERE parent_id>0 ORDER BY id');
        }

        if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();

                $r_checked = ($db->f('r')) ? 'checked' : '';
                $e_checked = ($db->f('e')) ? 'checked' : '';
                $p_checked = ($db->f('p')) ? 'checked' : '';
                $d_checked = ($db->f('d')) ? 'checked' : '';

                $arr[] = array(	'mod_title'	=> $db->f('title'),
                    'mod_id'	=> $db->f('mod_id'),
                    'i_id'		=> $i,
                    'r_checked'	=> $r_checked,
                    'e_checked'	=> $e_checked,
                    'p_checked'	=> $p_checked,
                    'd_checked'	=> $d_checked,
                );
            }

            return $arr;
        }
        return FALSE;
    }

    function setRolePermissions($role_id, $r = array(), $e = array(), $p = array(), $d = array()) {
        GLOBAL $CONFIG, $db;

        $role_id = (int)$role_id;

        if ($role_id) {
            $db->query('DELETE FROM core_roles_permissions WHERE role_id = '.$role_id);

            $arr = $this->getModules();

            foreach ($arr as $idx => $mod) {
                $read	= ($r[$mod]) ? 1 : 0;
                $edit	= ($e[$mod]) ? 1 : 0;
                $public	= ($p[$mod]) ? 1 : 0;
                $delete	= ($d[$mod]) ? 1 : 0;

                $db->query('INSERT INTO core_roles_permissions (role_id, module_id, r, e, p, d) '
                    .'VALUES ('.$role_id.', '.$mod.', '.$read.', '.$edit.', '.$public.', '.$delete.')');
            }
            return TRUE;
        }
        return FALSE;
    }

    function getSitesLangs($site_lang_id = array())
    {
        GLOBAL $CONFIG, $db;

        $db->query('SELECT	a.id site_id, a.title site_title, a.descr site_descr,
							b.id lang_id, b.title lang_title, b.language lang
						FROM sites a, sites_languages b
						WHERE a.id = b.category_id
						ORDER BY a.title');
        if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]['site_id']			= $db->f('site_id');
                $arr[$i]['site_title']		= strtoupper($db->f('site_title'));
                $arr[$i]['site_descr']		= $db->f('site_descr');
                $arr[$i]['lang_id']			= $db->f('lang_id');
                $arr[$i]['lang']			= $db->f('lang');
                $arr[$i]['site_id_lang_id']	= $db->f('site_id').'/'.$db->f('lang_id');

                for ($h = 0, $j = sizeof($CONFIG['site_langs']); $h < $j; $h++) {
                    if ($CONFIG['site_langs'][$h]['value'] == $arr[$i]['lang']) {
                        $arr[$i]['lang_title']		= $CONFIG['site_langs'][$h]['name'];
                    }
                }

                if ($arr[$i]['site_id'] == $site_lang_id['site_id'] && $arr[$i]['lang_id'] == $site_lang_id['lang_id']) {
                    $arr[$i]['selected'] = 'selected';
                }
            }
            return $arr;
        }
        return FALSE;
    }

    // функция для получения списка модулей
    function getModules() {
        GLOBAL $CONFIG, $db;

        $db->query('SELECT * FROM modules ORDER BY id');

        if ($db->nf() > 0) {
            while($db->next_record()) {
                $arr[] = $db->f('id');
            }
            return $arr;
        }
        return FALSE;
    }

    // получение группы и ее прав доступа по id
    function getGroup($id = NULL) {
        GLOBAL $CONFIG, $db, $baseurl, $server, $lang;

        $id = (int)$id;

        if ($id) {
            $db->query('SELECT * FROM core_groups WHERE id = '.$id);

            if ($db->nf() > 0) {
                $db->next_record();

                $arr	= array('group_name'		=> htmlspecialchars($db->f('name')),
                    'group_description'	=> htmlspecialchars($db->f('description')),
                    'group_id'			=> $db->f('id'),
                    'group_link'		=> $baseurl.'&action=users&group='.$db->f('id'),
                    'checked'			=> $db->f('active') ? 'checked' : '',
                );
                return $arr;
            }
        }
        return FALSE;
    }

    // получение ролей группы
    function getRolesForGroup($id = NULL) {
        GLOBAL $CONFIG, $db, $baseurl, $server, $lang;

        $id = (int)$id;

        if ($id) {
            $QUERY =
                'SELECT R.id id, R.descr role_descr, RG.role_id role_check, S.title site, L.title lang '.
                ' FROM core_roles R '.
                ' LEFT JOIN sites S ON (S.id = R.site_id) '.
                ' LEFT JOIN sites_languages L ON (L.id = R.lang_id)'.
                ' LEFT JOIN core_roles_groups RG ON (R.id = RG.role_id AND RG.group_id = '.$id.') '.
                ' ORDER BY R.id';
        } else {
            $QUERY =
                'SELECT R.id id, R.descr role_descr, S.title site, L.title lang '.
                ' FROM core_roles R '.
                ' LEFT JOIN sites S ON (S.id = R.site_id) '.
                ' LEFT JOIN sites_languages L ON (L.id = R.lang_id)'.
                ' ORDER BY R.id';
        }
        $db->query($QUERY);

        if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]	= array('role_id'		=> $db->f('id'),
                    'role_site'		=> strtoupper($db->f('site')),
                    'role_lang'		=> $db->f('lang'),
                    'role_descr'	=> $db->f('role_descr'),
                    'role_checked'	=> $db->f('role_check') ? 'checked' : '',
                    'role_link'		=> $baseurl.'&action=editrole&id='.$db->f('id'),
                );
            }
            return $arr;
        }
        return FALSE;
    }

    // добавдение в БД ролей группы
    function addGroupRoles($group_id, $roles)
    {
        GLOBAL $CONFIG, $db, $server, $lang;

        $group_id = (int)$group_id;
        if ($this->groupExist($group_id)) {
            if (is_array($roles) && !empty($roles)) {
                $db->query('INSERT INTO core_roles_groups (group_id, role_id)'
                    .' VALUES ('.$group_id.', '. implode('), ('.$group_id.', ', $roles) .')');
            }
            return TRUE;
        }
        return FALSE;
    }

    // функция для обновления информации о правах группы
    function updateGroupRoles($group_id, $roles)
    {
        GLOBAL $CONFIG, $db;

        $group_id = (int)$group_id;

        if ($this->groupExist($group_id)) {
            $db->query('DELETE FROM core_roles_groups WHERE group_id = '.$group_id);
            if (is_array($roles) && !empty($roles)) {
                $db->query('INSERT INTO core_roles_groups (group_id, role_id)'
                    .' VALUES ('.$group_id.', '. implode('), ('.$group_id.', ', $roles) .')');
            }
            return TRUE;
        }
        return FALSE;
    }

    function GetAllGroup(){
        $gr = call_user_func(array('Siteusers', 'get_users_groups'));
        $ret[0]['rus'] = $ret[0]['eng'] = 'Отсутствует';
        foreach($gr['groups'] as $k=>$v){
            $ret[$k] = array('rus'=>$v, 'eng'=>$v);
        }
        return $ret;
    }
    // функция для удаления информации о правах группы
    function deleteGroupRoles($group_id)
    {
        GLOBAL $CONFIG, $db, $server, $lang;

        $group_id = (int)$group_id;

        if ($group_id) {
            $db->query('DELETE FROM core_roles_groups WHERE group_id = '.$group_id);
            return TRUE;
        }
        return FALSE;
    }

    function my_iconv($charset_from, $charset_to, $str){
        if ($ret = iconv($charset_from, $charset_to, $str)) {
            return $ret;
        }
        $str_arr = explode(" ", $str);
        for ($i=0; $i < sizeof($str_arr); $i++) {
            $str_arr[$i] = iconv($charset_from, $charset_to, $str_arr[$i]);
        }
        return implode(" ", $str_arr);
    }

    function conv_str($str){
        switch ($this->data_charset) {
            case 'cp1251':
                return $this->my_iconv("WINDOWS-1251", "UTF-8", $str);
            case 'koi8-r':
                return $this->my_iconv("KOI8-R", "UTF-8", $str);
            case 'iso-8859-1':
                return $this->my_iconv("ISO-8859-1", "UTF-8", $str);
            default:
                return $str;
        }
    }

    function getUserGroups($user_id)
    {
        global $db;

        return $db->getArrayOfResult("SELECT G.* FROM core_groups AS G, core_users_groups AS UG"
            ." WHERE UG.user_id=".intval($user_id)." AND G.id=UG.group_id");
    }

    /**
     * Получить список групп
     *
     * @return unknown
     */
    function getGroups()
    {   static $ret = false;
        global $db1;

        if (!$ret) {
            $db1->query('SELECT id, name FROM core_groups');
            while ($db1->next_record()) {
                $ret[$db1->f('id')] = $db1->f('name');
            }
        }

        return $ret;
    }

    /**
     * Существуют ли пользователи
     * Вызов existUser(id1, id2, ... idN) или existUser(array(id1, id2, ... idN))
     * @return boolean
     */
    function existUser()
    {
        global $db;

        $args = func_get_args();
        switch (count($args)) {
            case 0:
                return FALSE;

            case 1:
                if (is_array($args[0])) {
                    $args = array_map("intval", $args[0]);
                } else {
                    $args = array(intval($args[0]));
                }
                break;

            default:
                $args = array_map("intval", $args);
                break;
        }

        $db->query("SELECT COUNT(*) AS cnt FROM core_users WHERE id IN (".implode(", ", $args).")");
        $db->next_record();

        return $db->f('cnt') == count($args) ? TRUE : FALSE;
    }


    /**
     * Добавить сообщение
     *
     * @param int $to
     * @param int $from
     * @param string $title
     * @param string $body
     * @return boleean
     */
    function addMsg($to, $from, $title, $body)
    {
        global $db;

        $to = (int) $to;
        $from = (int) $from;
        $title = trim($db->escape(str_replace(array("<", ">"), array("&lt", "&gt;"), $title)));
        if (! $title) {
            $title = "...";
        }
        $body = trim($db->escape(str_replace(array("<", ">"), array("&lt", "&gt;"), $body)));

        if (! $this->existUser($to, $from) || ! $body) {
            return FALSE;
        }

        $db->query("INSERT INTO core_users_msg (`to`, `from`, `title`, `body`, `date`)
		    VALUES (".$to.", ".$from.", '".$title."', '".$body."', NOW())");

        return $db->affected_rows() ? true : false;
    }

    /**
     * Получить список сообщений пользователя
     *
     * @param int $user_id
     * @param boolean $type TRUE - входяште, FALSE - исходящие, иначе все
     * @param boolean $only_new
     */
    function getMsgList($user_id, $type, $only_new)
    {
        global $db;

        $user_id = (int) $user_id;
        if (! $this->existUser($user_id)) {
            return FALSE;
        }
        $where = FALSE === $type
            ? "`from`=".$user_id." AND !del_from"
            : (TRUE === $type ? "`to`=".$user_id." AND !del_to" : "(`to`=".$user_id." OR `from`=".$user_id.")");
        $where .= $only_new ? " AND `new`" : "";

        return $db->getArrayOfResult("SELECT M.id, `to`, `from`, `title`, M.new, M.date,".
            " F.id AS from_id, F.username AS from_username, T.id AS to_id, T.username AS to_username".
            " FROM core_users_msg AS M, core_users AS F, core_users AS T".
            " WHERE M.from=F.id AND M.to=T.id AND ".$where." ORDER BY `date` DESC");
    }

    /**
     * Кол-во сообщений
     *
     * @param int $user_id
     * @param boolean $type TRUE - входяште, FALSE - исходящие, иначе все
     * @param boolean $only_new
     */
    function getMsgCnt($user_id, $type, $only_new)
    {
        global $db;

        $user_id = (int) $user_id;
        if (! $this->existUser($user_id)) {
            return FALSE;
        }
        $where = FALSE === $type
            ? "`from`=".$user_id
            : (TRUE === $type ? "`to`=".$user_id : "`to`=".$user_id." OR `from`=".$user_id);
        $where .= $only_new ? " AND `new`" : "";

        $db->query("SELECT COUNT(*) AS cnt FROM core_users_msg WHERE ".$where);
        $db->next_record();

        return $db->f('cnt');
    }

    /**
     * Получить сообщение
     *
     * @param int $id
     * @return mixed
     */
    function getMsg($id)
    {
        global $db;

        $db->query("SELECT M.*, F.id AS from_id, F.username AS from_username
		    FROM core_users_msg AS M, core_users AS F
		    WHERE M.from=F.id AND M.id=".intval($id));
        if (! $db->nf()) {
            return FALSE;
        }
        $db->next_record();

        return $db->Record;
    }

    /**
     * Пометить сообщение как прочитанное
     *
     * @param int $id
     * @return boolean
     */
    function readMsg($id)
    {
        global $db;

        $db->query("UPDATE core_users_msg SET new=0 WHERE id=".intval($id));

        return $db->affected_rows() ? true : false;
    }

    function showMsgList($type, $suff= '')
    {
        global $tpl, $transurl, $CONFIG, $core;

        if (! $this->is_authorized) {
            return FALSE;
        }
        $tpl->newBlock('block_nav'.$suff);
        $tpl->assign('baseurl', $transurl);
        $list = $this->getMsgList($_SESSION["siteuser"]["id"], $type, false);
        if (empty($list)) {
            $tpl->newBlock('block_no_msg'.$suff);
            return FALSE;
        }
        $tpl->newBlock('block_msg_list'.$suff);
        if ($list) {
            foreach ($list as $key => $val) {
                $tpl->newBlock('block_msg'.$suff);
                $val['link'] = $transurl."?action=showmsg&id=".$val['id'];
                $val['css_class_new'] = $val['new'] ? 'new' : '';
                $val['from_user_link'] = $this->getUserLink($val['from_username']);
                $val['to_user_link'] = $this->getUserLink($val['to_username']);
                $val['del_link'] = $transurl."?action=delmsg&id=".$val['id'];
                $tpl->assign($val);
            }
        }
        return TRUE;
    }

    function showMsg($id)
    {
        global $tpl, $transurl, $CONFIG, $core;

        if (! $this->is_authorized) {
            return FALSE;
        }
        $msg = $this->getMsg($id);
        if (! count($msg)) {
            $tpl->newBlock('block_no_msg');
            return FALSE;
        }
        $tpl->newBlock('block_msg');
        $msg['user_link'] = $this->getUserLink($msg['from_username']);
        $msg['del_link'] = $transurl."?action=delmsg&id=".$msg['id'];
        $msg['msg_list_link'] = $transurl."?action=msglist";
        $msg['send_msg_action'] = $transurl."?action=sendmsg";
        $tpl->assign($msg);
        $this->readMsg($id);

        return TRUE;
    }

    function delMsg($id)
    {
        global $db;

        $msg = $this->getMsg($id);
        if (!$_SESSION['siteuser']['id'] || !$msg
            || ($_SESSION['siteuser']['id'] != $msg['from'] && $_SESSION['siteuser']['id'] != $msg['to'])
        ) {
            return false;
        }
        if ( ($_SESSION['siteuser']['id'] == $msg['from'] && $msg['del_to'])
            || ($_SESSION['siteuser']['id'] == $msg['to'] && $msg['del_from'])
            || $msg['from'] == $msg['to']
        ) {
            $db->query("DELETE FROM core_users_msg WHERE id=".intval($id));
        } else {
            $db->query("UPDATE core_users_msg SET "
                .($_SESSION['siteuser']['id'] == $msg['to'] ? "del_to=1" : "del_from=1")
                ." WHERE id=".intval($id));
        }

        return $db->affected_rows() ? true : false;
    }

    function getUsersCount($filter = array('active' => 1))
    {
        global $db;

        $where = array();
        if (isset($filter['active'])) {
            $where[] = 'active='.($filter['active']) ? 1 : 0;
        }
        if (isset($filter['onsite'])) {
            $where[] = 'onsite=1';
        }
        $db->query("SELECT COUNT(*) FROM core_users".(! empty($where) ? ' WHERE '.implode(' AND ', $where) : ''));
        $db->next_record();

        return $db->Record[0];
    }

    function getUserLink($username)
    {
        global $CONFIG, $lang;

        return Core::formPageLink($CONFIG['siteusers_page_link'], $CONFIG['siteusers_page_address'], $lang, 0)
        .( $CONFIG['rewrite_mod'] ? $username : '&username='.$username );
    }

    function getUsers($filter = array('active'))
    {
        global $db;

        $where = array();
        $limit = $oreder = '';
        if (isset($filter['active'])) {
            $where[] = 'active='.($filter['active']) ? 1 : 0;
        }
        if (isset($filter['order_by'])) {
            $oreder = ' ORDER BY '.$db->escape($filter['order_by']) . (isset($filter['order_desc']) ? ' DESC' : '');
        }
        if (isset($filter['limit'])) {
            $limit = ' LIMIT '.intval($filter['limit']);
        }
        return $db->getArrayOfresult("SELECT * FROM core_users "
            .(!empty($where) ? " WHERE ".implode(" AND ", $where) : "").$oreder.$limit);
    }

    function getUserName($id)
    {
        global $db;

        $db->query("SELECT username FROM core_users WHERE id=".intval($id));
        return $db->next_record() ? $db->Record[0] : false;
    }

    function generatePassword()
    {
        $arr = array(
            'a','b','d','e','f','g','h','j','k','m','n','p','r','s','t','u','v','x','y','z',
            'A','B','D','E','F','G','H','J','K','M','N','P','R','S','T','U','V','X','Y','Z',
            '2','3','4','5','6','7','8','9',
        );
        $pass = "";
        $c = count($arr) - 1;
        for($i = 0; $i < $this->passwoer_length; $i++) {
            $pass .= $arr[rand(0, $c)];
        }

        return $pass;
    }

    function updateOnSite()
    {
        global $db;
        static $id_update = false;
        if (! $id_update) {
            $id_update = true;
            if ($this->is_authorized) {
                $db->query('UPDATE core_users SET last_visit=NOW() WHERE id='.$_SESSION["siteuser"]["id"]);
            }
            $db->query('UPDATE core_users SET onsite=0 WHERE last_visit<DATE_SUB(NOW(), INTERVAL '.$this->onsite_min.' MINUTE)');
        }
    }

    function changePsw($user_id, $psw, $new_psw, $conf_psw)
    {   global $db;

        $ret = 0;
        $user_id = (int)$user_id;
        $find = $db->fetchOne('core_users', array('clause' => array(
            'id' => $user_id,
            'password ' => md5($psw),
        )));
        if ($find) {
            $ret = $this->isValidPassword($new_psw, $conf_psw);
            if (1 == $ret) {
                $db->set(
                    'core_users',
                    array('password ' => md5($new_psw)),
                    array('id' => $user_id)
                );
            }
        }

        return $ret;
    }

    function showPropForm($id = 0, $assign_param = array())
    {   global $tpl, $CONFIG;

        $prop = false;
        if ($id) {
            $prop = $this->getProp($id);
            if ($prop) {
                $prop['required_checked'] = $prop['required'] ? 'checked' : '';
                $prop['reg_show_checked'] = $prop['reg_show'] ? 'checked' : '';
                $prop['public_checked'] = $prop['public'] ? 'checked' : '';
                $assign_param = $assign_param + $prop;
            }
        }

        $assign_param['css_display_list_val'] = $prop && 5 == $prop['type'] || 6 == $prop['type']
            ? 'block' : 'none';

        $tpl->assign($assign_param);

        Module::show_select('prop_type_list', $prop ? $prop['type'] : 0, '', $CONFIG['siteusers_prop_types']);
        Module::show_select('prop_group_list', $prop ? $prop['group'] : 0, '', $this->getGroups());

        if ($prop && 5 == $prop['type'] || 6 == $prop['type'] && !empty($prop['list_val'])) {
            foreach ($prop['list_val'] as $key => $val) {
                $tpl->newBlock('val_list_item');
                $tpl->assign(array(
                        'id' => $key,
                        'title' => $val,
                    ) + $assign_param);
            }
        }
    }

    function delProp($id)
    {   global $db;

        return $db->del("core_users_prop", array('id' => $id));
    }

    function getProp($id)
    {   global $db, $CONFIG;

        $ret = $db->fetchOne("core_users_prop", array('clause' => array(
            'id' => $id
        )));
        if ($ret && 5 == $ret['type'] || 6 == $ret['type']) {
            $ret['list_val'] = unserialize($ret['meta']);
        }

        return $ret;
    }

    function getPropList($filter = false)
    {   global $db, $CONFIG;
        static $_prop_list = array();

        if (empty($_prop_list)) {
            $db->query("SELECT * FROM core_users_prop ORDER BY rank");
            while ($db->next()) {
                $_prop_list[$db->Record['id']] = $db->Record;
                if (5 == $db->Record['type'] || 6 == $db->Record['type']) {
                    $_prop_list[$db->Record['id']]['list_val'] = unserialize($db->Record['meta']);
                }
            }
        }
        $ret = $_prop_list;

        if ($filter) {
            foreach ($ret as $key => $val) {
                if ( (isset($filter['active']) && $val['active']!=$filter['active'] )
                    || (isset($filter['public']) && $val['public']!=$filter['public'] )
                    || (isset($filter['reg_show']) && $val['reg_show']!=$filter['reg_show'] )
                    || (isset($filter['required']) && $val['required']!=$filter['required'] )
                    || (isset($filter['groups']) && $val['group'] && !in_array($val['group'], $filter['groups']) )
                ) {
                    unset($ret[$key]);
                }
            }
        }

        return $ret;
    }

    function setProp($data)
    {   global $db, $CONFIG;

        $ret = $prop = false;
        $data['uname'] = trim($data['uname']);
        $reserv_unames = $this->getReservUnames();
        if ($data['id']) {
            $prop = $this->getProp($data['id']);
            if ($prop) {
                if ($data['uname'] <> $prop['uname'] && in_array($data['uname'], $reserv_unames)) {
                    $data['uname'] = false;
                }
            } else {
                $data['uname'] = false;
            }
        } else if(in_array($data['uname'], $reserv_unames) ) {
            $data['uname'] = false;
        }
        if ($data['title'] && $data['uname']) {
            $set = array(
                'title' => $data['title'],
                'uname' => $data['uname'],
                'type' => (int)$data['type'],
                'group' => (int)$data['group'],
                'required' => $data['required'] ? 1 : 0,
                'public' => $data['public'] ? 1 : 0,
                'reg_show' => $data['reg_show'] ? 1 : 0,
                'meta' => ''
            );
            if (5 == $data['type'] || 6 == $data['type'] && !empty($data['list_val'])) {
                $meta = array();
                foreach ($data['list_val'] as $k => $v) {
                    $v = trim($v);
                    if ($v !== '') {
                        $meta[$k] = $v;
                    }
                }
                if (! empty($meta))  {
                    $set['meta'] = serialize($meta);
                }
            }
            if ($prop) {
                $ret = $db->set('core_users_prop', $set, array('id' => $data['id']));
            } else {
                $db->query("SELECT MAX(rank) AS max_rank FROM core_users_prop");
                $max_rank = $db->next() ? $db->Record['max_rank']+1 : 1;
                $set['rank'] = $max_rank;
                $ret = $db->set('core_users_prop', $set);
            }
        }

        return $ret;
    }


    function getReservUnames()
    {   global $db, $CONFIG;

        $ret = $db->fetchColumn('core_users_prop', 'uname');

        return $ret ? array_merge($ret, $CONFIG['siteusers_reserv_prop_unames']) : $CONFIG['siteusers_reserv_prop_unames'];
    }

    function showPropList($list, $param = array())
    {   global $tpl;

        if (empty($list)) {
            $tpl->newBlock('prop_list_empty');
            $tpl->assign($param);
        } else {
            $actions = array("activate"	=>	array("prop_activate","prop_suspend"));
            $group_list = $this->getGroups();
            $tpl->newBlock('prop_list');
            $tpl->assign($param);
            $i = 0; $c = count($list);
            foreach ($list as $val) {
                $i++;
                $tpl->newBlock('prop_list_item');

                $val['required_checked'] = $val['required'] ? 'checked' : '';
                $val['reg_show_checked'] = $val['reg_show'] ? 'checked' : '';
                $val['public_checked'] = $val['public'] ? 'checked' : '';
                $val['img_up'] = 1 == $i ? 'ico_ligrup.gif' : 'ico_listup.gif';
                $val['img_down'] = $c == $i ? 'ico_ligrdw.gif' : 'ico_listdw.gif';
                $val['is_first'] = 1 == $i ? 1 : 0;
                $val['is_last'] = $c == $i ? 1 : 0;
                $val[''] = $val[''] ? 'checked' : '';


                $tpl->assign(array_merge($val, $param));

                $adm_actions = $this->get_actions_by_rights($param['baseurl'].'&id='.$val['id'], $val["active"], 1, 4095, $actions);
                $tpl->assign($adm_actions);

                Module::show_select('group_list', $val['group'], '', $group_list);
            }
        }
    }

    function showUserPropList($user_id, $param = array(), $assign = array())
    {   global $tpl, $CONFIG;

        $user = $user_id ? $this->getUser($user_id) : false;
        $filter = array('active' => 1);
        if ($param['is_registration']) {
            $filter['reg_show'] = 1;
        }
        if ($param['public']) {
            $filter['public'] = 1;
        }
        if ($user) {
            $filter['groups'] = $user['groups'];
        } else {
            if($_REQUEST['groups']=="dilers")
            {
                $filter['groups'] = array(3);
            }else{
                $filter['groups'] = array($CONFIG['siteusers_group']);
            }

        }
        $prop_list = $this->getPropList($filter);
        if (empty($prop_list)) {
            $tpl->newBlock('prop_list_empty');
            $tpl->assign($assign);
        } else {
            $tpl->newBlock('prop_list');
            $tpl->assign($assign);
            $i = 0; $c = count($list);
            $textBlock2="";
            $textBlockReg="";
            foreach ($prop_list as $val) {
                if (!$param['with_value'] || ($param['with_value'] && $user && $user['prop_list'][$val['id']]['value'])) {
                    $i++;
                    if($this->isDiler())
                    {
                        if($i==5)
                        {
                            $textBlock2='</div></div><div class="lklklk1"><div class="blu_line">О компании:</div>';
                        }

                    }
                    if(isset($_REQUEST['groups'])) if($_REQUEST['groups']=="dilers")
                        if($i==5)
                    {
                        $textBlockReg='</tbody></table><div class="blu_line">О компании:</div><table class="reg_diller"><tbody>';
                    }
                        $tpl->newBlock('prop_list_item');
                    $_assign = array_merge($val, $assign);
                    $_assign['css_required_class'] = $val['required'] ? 'required' : '';
                    $_assign['value'] = $user && $user['prop_list'][$val['id']]['value']
                        ? $user['prop_list'][$val['id']]['value']
                        : ($_POST['prop'][$val['id']] ? htmlspecialchars($_POST['prop'][$val['id']]) : '');
                    if($this->isDiler())$_assign['textBlock2']=$textBlock2;
                    if(isset($_REQUEST['groups'])) if($_REQUEST['groups']=="dilers")$_assign['textBlockReg']=$textBlockReg;
                    switch ($val['type']) {
                        // 1 => Строка
                        case 1:
                            $tpl->assign($_assign);
                            $tpl->newBlock('prop_list_item_string');
                            $tpl->assign($_assign);
                            break;
                        // 2 => Текст
                        case 2:
                            $tpl->assign($_assign);
                            $tpl->newBlock('prop_list_item_text');
                            $tpl->assign($_assign);
                            break;
                        // 3 => Флаг
                        case 3:
                            $_assign['value'] = $_assign['value'] ? 'Да' : 'Нет';
                            $tpl->assign($_assign);
                            $tpl->newBlock('prop_list_item_flag');
                            $_assign['checked'] = $_assign['value'] ? 'checked' : '';
                            $tpl->assign($_assign);
                            break;
                        // 4 => Дата
                        case 4:
                            $tpl->assign($_assign);
                            $tpl->newBlock('prop_list_item_date');
                            $tpl->assign($_assign);
                            break;
                        // 5 => Список
                        case 5:
                            $tpl->newBlock('prop_list_item_set');
                            $tpl->assign($_assign);
                            $_assign['value'] = array();
                            if (!empty($val['list_val'])) {
                                foreach ($val['list_val'] as $k => $v) {
                                    $selected = ( $user && $user['prop_list'][$val['id']]['value'][$k] )
                                        || ( !$user && $_POST['prop'][$val['id']][$k] );
                                    $tpl->newBlock('prop_list_item_set_item');
                                    $tpl->assign(array(
                                        'prop_id' => $val['id'],
                                        'id' => $k,
                                        'name' => $v,
                                        'selected' => $selected ? 'selected' : '',
                                        'checked' => $selected ? 'checked' : '',
                                    ));
                                    if ($selected) {
                                        $_assign['value'][] = $v;
                                    }
                                }
                            }
                            $_assign['value'] = implode(', ', $_assign['value']);
                            $tpl->gotoBlock('prop_list_item');
                            $tpl->assign($_assign);
                            break;
                        // 6 => Выпадающий список
                        case 6:
                            $_assign['value'] = $val['list_val'][$_assign['value']] ? $val['list_val'][$_assign['value']] : '';
                            $tpl->assign($_assign);
                            $tpl->newBlock('prop_list_item_select');
                            $tpl->assign($_assign);
                            if (!empty($val['list_val'])) {
                                foreach ($val['list_val'] as $k => $v) {
                                    $selected = ( $user && $user['prop_list'][$val['id']]['value'] == $k  )
                                        || ( !$user && $_POST['prop'][$val['id']] == $k );
                                    $tpl->newBlock('prop_list_item_select_item');
                                    $tpl->assign(array(
                                        'prop_id' => $val['id'],
                                        'id' => $k,
                                        'value' => $k,
                                        'name' => $v,
                                        'selected' => $selected ? 'selected' : '',
                                        'checked' => $selected ? 'checked' : '',
                                    ));
                                }
                            }
                            break;
                    }
                    $textBlock2="";
                    $textBlockReg="";
                }
            }
        }
    }

    function checkUserProp($prop)
    {
        $ret = true;
        $prop_list = $this->getPropList(array('active' => 1, 'required' => 1));
        if ($prop_list) {
            foreach ($prop_list as $val) {
                if (!isset($prop[$val['id']]) || '' === $prop[$val['id']]) {
                    $ret = $val;
                }
            }
        }

        return $ret;
    }

    function setUserProp($user_id, $prop)
    {   global $db, $CONFIG;

        $ret = true;
        $db->del('core_users_prop_val', array('user_id' => $user_id));
        $db->del('core_users_prop_val_txt', array('user_id' => $user_id));
        $prop_list = $this->getPropList(array('active' => 1));
        if ($prop_list) {
            foreach ($prop_list as $val) {
                if (isset($prop[$val['id']]) && '' !== $prop[$val['id']]) {
                    $tbl = false;
                    $prop_val = $prop[$val['id']];
                    switch ($val['type']) {
                        // Строка
                        case 1:
                            $tbl = 'core_users_prop_val';
                            break;
                        // Текст
                        case 2:
                            $tbl = 'core_users_prop_val_txt';
                            break;
                        // Флаг
                        case 3:
                            $tbl = 'core_users_prop_val';
                            $prop_val = $prop_val ? 1 : 0;
                            break;
                        // Дата
                        case 4:
                            $tbl = 'core_users_prop_val';
                            break;
                        // Список
                        case 5:
                            $tbl = 'core_users_prop_val';
                            $prop_val = serialize($prop_val);
                            break;
                        // Выпадающий список
                        case 6:
                            $tbl = 'core_users_prop_val';
                            $prop_val = serialize($prop_val);
                            break;

                        default:
                            break;
                    }
                    if ($tbl) {
                        $db->set($tbl, array(
                            'user_id' => $user_id,
                            'prop_id' => $val['id'],
                            'value' => $prop_val
                        ));
                    }

                }
            }
        }

        return $ret;
    }

    function getUser($id, $clear = false)
    {   global $db, $CONFIG;
        static $ret = array();

        $id = (int)$id;
        if ($clear && $ret[$id]) {
            unset($ret[$id]);
        }

        if (! $ret[$id]) {
            //$ret = $db->fetchOne('core_users', array('clause' => array('id' => $id)));
            $db->query("SELECT u.*, u.id as su_id, u.username as siteusername,"
                ." GROUP_CONCAT(DISTINCT ug.group_id SEPARATOR ',') AS groups"
                ." FROM core_users u LEFT JOIN core_users_groups ug ON ug.user_id = u.id"
                ." WHERE u.id = ".intval($id)." GROUP BY u.id");
            if ($db->next()) {
                $ret[$id] = $db->Record;
                $ret[$id]['groups'] = explode(',', $ret[$id]['groups']);
                $ret[$id]['prop_list'] = array();
                $prop_list = $this->getPropList(array('active' => 1));
                if ($prop_list) {
                    $str_val = $db->fetchAll('core_users_prop_val', array('clause' => array(
                        'user_id' => $ret[$id]['id']
                    )), 'prop_id');
                    $txt_val = $db->fetchAll('core_users_prop_val_txt', array('clause' => array(
                        'user_id' => $ret[$id]['id']
                    )), 'prop_id');
                    foreach ($prop_list as $prop) {
                        if (isset($str_val[$prop['id']])) {
                            $ret[$id]['prop_list'][$prop['id']] = $prop;
                            if (5 == $prop['type'] || 6 == $prop['type']) {
                                $ret[$id]['prop_list'][$prop['id']]['value'] = unserialize($str_val[$prop['id']]['value']);
                            } else if (3 == $prop['type']) {
                                $ret[$id]['prop_list'][$prop['id']]['value'] = $str_val[$prop['id']]['value'] ? 1 : 0;
                            } else {
                                $ret[$id]['prop_list'][$prop['id']]['value'] = $str_val[$prop['id']]['value'];
                            }
                        } else if (isset($txt_val[$prop['id']])) {
                            $ret[$id]['prop_list'][$prop['id']] = $prop;
                            $ret[$id]['prop_list'][$prop['id']]['value'] = $txt_val[$prop['id']]['value'];

                        }
                    }
                }
            }
        }

        return $ret[$id];
    }

    /**
     * Enter description here...
     *
     * @param int $user_id
     * @param string $email_to
     * @param string od array $email_from - 'zx@zx.zx' OR array('zx@zx.zx', 'From Name')
     * @param string $subject
     * @param string $tpl_path
     * @param array $param
     * @param array $notify_blocks
     * @return boolean
     */
    function notifyUser($user_id, $email_to, $email_from, $subject, $tpl_path, $param = array(), $notify_blocks = array())
    {   global $CONFIG;

        $ret = false;

        $user = $this->getUser($user_id);
        if ($user) {
            $tpl = new AboTemplate(RP.$tpl_path);
            $tpl->prepare();
            $tpl->assign($param + $user);
            if ($user['prop_list']) {
                $tpl->newBlock('prop_list');
                foreach ($user['prop_list'] as $prop) {
                    $tpl->newBlock('prop_list_item');
                    if(in_array($prop['type'],array(5,6))){
$vk_val='';
if(is_array($prop['value'])){
foreach($prop['value'] as $vk){
$vk_val=='' ? $vk_val=$prop['list_val'][$vk] : $vk_val.=', '.$prop['list_val'][$vk];
}
}
$tpl->assign(array(
                        'title' => $prop['title'],
                        'value' => is_array($prop['value']) ? $vk_val : $prop['list_val'][$prop['value']],
                    ));
}else
                    $tpl->assign(array(
                        'title' => $prop['title'],
                        'value' => is_array($prop['value']) ? implode(',', $prop['value']) : $prop['value'],
                    ));
                }
            }
            if (! empty($notify_blocks)) {
                foreach ($notify_blocks as $key => $val) {
                    $tpl->newBlock($key);
                    $tpl->assign($val);
                }
            }
            $body = $tpl->getOutputContent();
            $mail = new Mailer();
            $mail->CharSet		= $CONFIG['email_charset'];
            $mail->ContentType	= "text/html"; //$CONFIG['email_type'];
            if (is_array($email_from)) {
                $mail->From = $email_from[0];
                $mail->FromName = $email_from[1];
            } else {
                $mail->From = $email_from;
            }
            $mail->Mailer		= 'mail';
            $mail->AddAddress($email_to);
            $mail->Subject		= $subject;
            $mail->Body			= $body;
            $ret = $mail->Send();
        }
        return $ret;
    }

    function getAllAddress($user_id)
    {
        global $db;
        $q="Select * from sup_rus_address as a where a.user_id= {$user_id} Order by a.primary DESC";
        return $db->getArrayOfResult($q);
    }

    function updateUserAddress()
    {
        global $db,$_REQUEST;
        foreach($_REQUEST['prop'] as $key=>$val)
        {
            $prop=$this->getProp($key);
            $res[$prop['uname']]=$val;

        }
        if($_REQUEST['selectAddress'] =="add")
        {
            $strK="";
            $strV="";
            foreach($res as $key=>$val)
            {
                $strK = $strK ? $strK.", `".$key."`" : '`'.$key.'`';
                $strV = $strV ? $strV.",'".$val."'" : "'".$val."'";
            }
            $q1="UPDATE sup_rus_address SET `primary`=0 WHERE user_id={$_SESSION['siteuser']['id']} AND `primary`=1";
            $db->query($q1);
            $q="Insert Into sup_rus_address ({$strK},`primary`,user_id) values ({$strV},1,{$_SESSION['siteuser']['id']})";
            $db->query($q);
        }
        else{
            if($_REQUEST['selectAddress'])
            {
                $strU="";
                foreach($res as $key=>$val)
                {
                    $strU = $strU ? $strU.", `".$key."`= '".$val."'" : '`'.$key."`= '".$val."'";
                }
                $strU=$strU.",`primary` = 1";
                $db->query("UPDATE sup_rus_address SET `primary`=0 WHERE user_id={$_SESSION['siteuser']['id']} AND `primary`=1");
                $db->query("UPDATE sup_rus_address SET {$strU} WHERE id={$_REQUEST['selectAddress']}");
            }

        }
    }

    function isDiler()
    {
        if(!isset($_SESSION['siteuser'])) return false;
        foreach($_SESSION['siteuser'][group_id] as $val)
        {

            if($val == 3)
            {
                return true;
            }
        }
        return false;
    }

    function addReport($link)
    {
        global $db;
        $arr=array('user_id'=>$_SESSION["siteuser"]["id"],
            'date'=>date('d-m-y h:i:s'),
            'link'=>$link);
        $db->set('sup_rus_dilers_reports',$arr);
    }

    function getDilerReports($id)
    {
        global $db;

        return $db->getArrayOfResult("Select *  From sup_rus_dilers_reports Where user_id={$id}");
    }

    function getUserUpdates($id)
    {
        global $db;

        return $db->getArrayOfResult("Select *  From sup_rus_dilers_history Where user_id={$id}");
    }

    function isLoginDiler()
    {
        $diler=false;
        foreach($_SESSION['siteuser'][group_id] as $val)
        {
            if($val == 3)
            {
                $diler= true;
            }
        }
        if($diler)
        {
            if($_SERVER['SERVER_NAME'] != "supra.ru") return false;
        }
        else{
            if($_SERVER['SERVER_NAME'] == "supra.ru") return false;
        }
        return true;

    }

    function crop($file_input, $file_output, $crop = 'square',$percent = false) {
        list($w_i, $h_i, $type) = getimagesize($file_input);
        if (!$w_i || !$h_i) {
            return; 'Невозможно получить длину и ширину изображения';

        }
        $types = array('','gif','jpeg','png');
        $ext = $types[$type];
        if ($ext) {
            $func = 'imagecreatefrom'.$ext;
            $img = $func($file_input);
        } else {
            return; 'Некорректный формат файла';

        }
        if ($crop == 'square') {
            $min = $w_i;
            if ($w_i > $h_i) $min = $h_i;
            $w_o = $h_o = $min;
        } else {
            list($x_o, $y_o, $w_o, $h_o) = $crop;
            if ($percent) {
                $w_o *= $w_i / 100;
                $h_o *= $h_i / 100;
                $x_o *= $w_i / 100;
                $y_o *= $h_i / 100;
            }
            if ($w_o < 0) $w_o += $w_i;
            $w_o -= $x_o;
            if ($h_o < 0) $h_o += $h_i;
            $h_o -= $y_o;
        }
        $img_o = imagecreatetruecolor($w_o, $h_o);
        imagecopy($img_o, $img, 0, 0, $x_o, $y_o, $w_o, $h_o);
        if ($type == 2) {
            return imagejpeg($img_o,$file_output,100);
        } else {
            $func = 'image'.$ext;
            return $func($img_o,$file_output);
        }
    }

    function resize($file_input, $file_output, $w_o, $h_o, $percent = false) {
        list($w_i, $h_i, $type) = getimagesize($file_input);
        if (!$w_i || !$h_i) {
            return; 'Невозможно получить длину и ширину изображения';

        }
        $types = array('','gif','jpeg','png');
        $ext = $types[$type];
        if ($ext) {
            $func = 'imagecreatefrom'.$ext;
            $img = $func($file_input);
        } else {
            return; 'Некорректный формат файла';

        }
        if ($percent) {
            $w_o *= $w_i / 100;
            $h_o *= $h_i / 100;
        }
        if (!$h_o) $h_o = $w_o/($w_i/$h_i);
        if (!$w_o) $w_o = $h_o/($h_i/$w_i);

        $img_o = imagecreatetruecolor($w_o, $h_o);
        imagecopyresampled($img_o, $img, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i);
        if ($type == 2) {
            return imagejpeg($img_o,$file_output,100);
        } else {
            $func = 'image'.$ext;
            return $func($img_o,$file_output);
        }
    }

}

?>
