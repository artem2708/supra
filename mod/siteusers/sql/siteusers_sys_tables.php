<?php

$MOD_TABLES[] = "CREATE TABLE `core_group_permissions` (
  `group_id` smallint(5) unsigned NOT NULL default '0',
  `page_id` smallint(5) unsigned NOT NULL default '0',
  `site_id` int(10) unsigned NOT NULL default '0',
  `lang_id` int(10) unsigned NOT NULL default '0',
  `show` tinyint(4) default '0',
  PRIMARY KEY  (`group_id`,`page_id`,`site_id`,`lang_id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `core_groups` (
  `id` smallint(6) NOT NULL auto_increment,
  `owner_id` int(10) unsigned NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `description` text,
  `active` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `core_roles` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(10) unsigned NOT NULL default '0',
  `lang_id` int(10) unsigned NOT NULL default '0',
  `descr` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `core_roles_groups` (
  `role_id` int(11) unsigned NOT NULL default '0',
  `group_id` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`role_id`,`group_id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `core_roles_permissions` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `role_id` int(10) unsigned NOT NULL default '0',
  `module_id` int(10) unsigned NOT NULL default '0',
  `r` tinyint(4) NOT NULL default '0',
  `e` tinyint(4) NOT NULL default '0',
  `p` tinyint(4) NOT NULL default '0',
  `d` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `core_users` (
  `id` smallint(11) NOT NULL auto_increment,
  `site_id` int(10) unsigned NOT NULL default '0',
  `lang_id` int(10) unsigned NOT NULL default '0',
  `username` varchar(15) NOT NULL default '',
  `password` varchar(32) NOT NULL default '',
  `email` varchar(63) default NULL,
  `client_type` tinyint(1) NOT NULL default '1',
  `active` tinyint(1) unsigned NOT NULL default '0',
  `last_visit` datetime default NULL,
  `is_new` tinyint(1) unsigned NOT NULL default '1',
  `uid` varchar(50) default NULL,
  `confirmation` tinyint(1) unsigned default NULL,
  `onsite` tinyint(1) unsigned NOT NULL default '0',
  `reg_date` datetime default NULL,
  `img` varchar(31) default NULL,
  PRIMARY KEY  (`id`),
  KEY `username` (`password`,`site_id`,`lang_id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `core_users_groups` (
  `user_id` int(10) unsigned NOT NULL default '0',
  `group_id` int(10) unsigned NOT NULL default '0',
  UNIQUE KEY `unique_combination` (`user_id`,`group_id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `core_users_msg` (
    `id` int(10) unsigned NOT NULL auto_increment,
    `to` smallint(5) unsigned NOT NULL default '0',
    `from` smallint(5) unsigned NOT NULL default '0',
    `title` varchar(255) NOT NULL default '',
    `body` text,
    `date` datetime default NULL,
    `new` tinyint(1) unsigned NOT NULL default '1',
    `del_to` tinyint(1) unsigned NOT NULL default '0',
    `del_from` tinyint(1) unsigned NOT NULL default '0',
    PRIMARY KEY  (`id`),
    KEY `to` (`to`,`from`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE IF NOT EXISTS `core_users_prop` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `uname` VARCHAR(15) NOT NULL DEFAULT '',
  `group` INT UNSIGNED NOT NULL DEFAULT 0,
  `rank` INT NOT NULL DEFAULT 0,
  `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `title` VARCHAR(63) NOT NULL DEFAULT '',
  `required` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `public` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `reg_show` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `type` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `meta` TEXT NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY (`uname`),
  KEY (`group`),
  KEY (`rank`),
  KEY (`active`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE IF NOT EXISTS `core_users_prop_val` (
  `user_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `prop_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `value` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`, `prop_id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE IF NOT EXISTS `core_users_prop_val_txt` (
  `user_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `prop_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `value` TEXT NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`, `prop_id`)
) ENGINE=MyISAM";

?>