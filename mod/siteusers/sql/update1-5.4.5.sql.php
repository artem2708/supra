<?php
$sql = array(
    "CREATE TABLE IF NOT EXISTS `core_users_prop` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `uname` VARCHAR(15) NOT NULL DEFAULT '',
  `group` INT UNSIGNED NOT NULL DEFAULT 0,
  `rank` INT NOT NULL DEFAULT 0,
  `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `title` VARCHAR(63) NOT NULL DEFAULT '',
  `required` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `public` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `reg_show` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `type` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `meta` TEXT NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY (`uname`),
  KEY (`group`),
  KEY (`rank`),
  KEY (`active`)
) ENGINE=MyISAM",

    "CREATE TABLE IF NOT EXISTS `core_users_prop_val` (
  `user_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `prop_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `value` VARCHAR(63) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`, `prop_id`)
) ENGINE=MyISAM",

    "CREATE TABLE IF NOT EXISTS `core_users_prop_val_txt` (
  `user_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `prop_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `txt` TEXT NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`, `prop_id`)
) ENGINE=MyISAM"
);
?>