<?php

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_banners` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `category_id` int(11) default NULL,
  `name` varchar(255) NOT NULL default '',
  `text` text,
  `type` tinyint(1) default NULL,
  `filename` varchar(255) default NULL,
  `imagename` varchar(255) default NULL,
  `url` varchar(255) default NULL,
  `showing` bigint(20) unsigned NOT NULL default '0',
  `clicking` bigint(20) unsigned default '0',
  `max_showing` bigint(20) unsigned NOT NULL default '0',
  `show_from` date default NULL,
  `show_to` date default NULL,
  `active` tinyint(3) unsigned NOT NULL default '0',
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',

    PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_banners_categories` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',

    PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_banners_cities` (
  `banner_id` int(10) unsigned NOT NULL default '0',
  `city_id` int(10) unsigned NOT NULL default '0',
  UNIQUE (`banner_id`, `city_id`)
) ENGINE=MyISAM";
?>