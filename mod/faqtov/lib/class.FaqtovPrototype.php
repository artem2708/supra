<?php

///require_once(RP.'mod/banners/lib/class.faqtovDB.php');

class FaqtovPrototype extends Module {
    var $module_name = "faqtov";
    var $table_prefix;
    var $main_title;											// переопределяет title страницы, если модуль главный
    var $addition_to_path;										// добавляет к пути страницы строку, если модуль главный
    var $already_displayed = array();							// баннеры, уже показанные на этой странице
    var $_msg						= array();
    var $block_module_actions		= array();
    var $block_main_module_actions	= array();
    var $default_action 			= "showall";				// действие, выполняемое модулем по умолчанию
    var $admin_default_action		= 'showctg';
    var $tpl_path					= 'mod/faqtov/tpl/';
    var $banner_types				= array();
    var $action_tpl = array(
        'banners' => 'banners_banners.html',
    );
    var $is_shop;
    function FaqtovPrototype($action = "", $transurl = "", $properties = array(), $prefix = "", $rewrite_mod_params = NULL, $adminurl = NULL, $noadmin = false) {
        global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang,	$permissions, $request_type, $_RESULT;

        /* Get $action, $tpl_name, $permissions */
        extract( $this->init(array(
            'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
            'action' => $action, 'tpl_name' => $properties['tpl']
        )), EXTR_OVERWRITE);
        $this->is_shop = isShop();
        // выставим свойства, которые пользователь имеет возможность задать для блока с данным модулем
        // индекс свойства в массиве $properties соответствует номеру поля в БД
        // например, первый элемент соотв. полю property1 в БД
        $category_id	= (int)$properties[1];										// показывать баннеры такой то категории
        $new_line		= (int)$properties[2];										// перенос строки после баннера

        if ('only_create_object' == $action) {
            return;
        }

        if ($this->actionExists($action)) {
            $this->doAction($action);
            return;
        }

        if (! self::is_admin()) {

//---------------------------------- обработка действий с сайта --------------------------------//		
            switch ($action) {
                case "showall":
                    global $tpl,$db,$db2;
                    $main->include_main_blocks($this->module_name."_showall.html", "main");
                    $tpl->prepare();
                    $db->query("SELECT * FROM ".$server.$lang."_faqtov_faq WHERE id!= 0 AND active = 1");
                    $tpl->newBlock("accordion");
                    $i = 0;
                    if($db->nf()!=0) {
                        while ($db->next_record()) {
                            $db2->query('SELECT * FROM ' . $server . $lang . "_faqtov_faqtov WHERE cat_id=" . $db->f('id') . " AND active = 1");
                            if($db2->nf()!=0) {
                                $tpl->newBlock("js_script");
                                $tpl->assign("count", $i);
                                $tpl->newBlock("accordion_lvl1");
                                $tpl->assign(Array(
                                    "title" => $db->f('title'),
                                    "count" => $i
                                ));


                                $i++;
                            }
                            while ($db2->next_record()) {
                                $tpl->newBlock("accordion_lvl2");
                                $tpl->assign("title", $db2->f('title'));
                                $tpl->assign("faq_text", $db2->f('faq'));
                            }

                        }
                    }
                    break;


                default:
                    if (is_object($tpl)) $tpl->prepare();
                    return;
            }
//------------------------------- конец обработки действий с сайта -----------------------------//
        } else {

            // действия, которые пользователь имеет возможность задать для блока с данным модулем
            $this->block_module_actions	= array(
                'showall' => "Показать все описния",
            );
            $this->block_main_module_actions	=  array(
                'showall' => "Показать все описния",
            );

            if (!isset($tpl)) {
                $main->message_die(iconv("WINDOWS-1251", "UTF-8", 'Не подключен класс для работы с шаблонами'));
            }

            if (empty($action)) $action = $this->admin_default_action;
            switch ($action) {

                case 'showctg':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$server,$lang,$tpl,$request_cat_id;
                    $main->include_main_blocks_2($this->module_name."_cat.html", $this->tpl_path);
                    $tpl->prepare();
                    if(!$request_cat_id) {
                        $request_cat_id = 0;
                        $db->query("SELECT * FROM " . $server . $lang . "_faqtov_faq WHERE parent_id = ".$request_cat_id." AND id!=0");
                        $tpl->newBlock('cat_tov');
                        if($db->nf()!=0) {
                            while ($db->next_record()) {
                                $tpl->newBlock('faqtov_cat');
                                $tpl->assign(Array(
                                    "id" => $db->f('id'),
                                    "title" => $db->f('title'),
                                    "ico" => $db->f("active") == 0 ? "ico_swon" : "ico_swof"
                                ));
                            }
                        }
                    }
                    else
                    {

                        $db->query("SELECT * FROM " . $server . $lang . "_faqtov_faq WHERE parent_id=".$request_cat_id);
                        if($db->nf()!=0) {
                            $tpl->newBlock('cat_tov');
                            while ($db->next_record()) {
                                $tpl->newBlock('faqtov_cat');
                                $tpl->assign(Array(
                                    "id" => $db->f('id'),
                                    "title" => $db->f('title'),
                                    "ico" => $db->f("active") == 0 ? "ico_swon" : "ico_swof"
                                ));
                            }
                        }

                        $tpl->newBlock('cat_cat');
                        $db->query("SELECT * FROM " . $server . $lang . "_faqtov_faqtov WHERE cat_id=".$request_cat_id);
                        while ($db->next_record()) {
                            $tpl->newBlock('faqtov');
                            $tpl->assign(Array(
                                "id" => $db->f('id'),
                                "title" => $db->f('title'),
                                "ico" => $db->f("active") == 0 ? "ico_swon" : "ico_swof"
                            ));
                        }
                    }
                    break;

                case 'delfaq':
                    global $db,$server,$lang,$request_id;
                    $db->query("DELETE FROM ".$server.$lang."_faqtov_faqtov WHERE id=".$request_id);
                    $db->query("DELETE FROM ".$server.$lang."_faqtov_ids WHERE id_faq=".$request_id);
                    Module::go_back();

                    break;


                case 'delcat':
                    global $db,$server,$lang,$request_id;
                    $db->query("DELETE FROM ".$server.$lang."_faqtov_faq WHERE id=".$request_id);
                    $db->query("DELETE FROM ".$server.$lang."_faqtov_faq WHERE parent_id=".$request_id);
                    Module::go_back();
                    break;


                case 'publ':
                    global $db,$db2,$server,$lang,$request_id;
                    $db->query("UPDATE ".$server.$lang."_faqtov_faq SET active = if (active ,0,1)  WHERE id=".$request_id);
                    Module::go_back();
                    break;

                case 'publfaq':
                    global $db,$db2,$server,$lang,$request_id;
                    $db->query("UPDATE ".$server.$lang."_faqtov_faqtov SET active = if (active ,0,1)  WHERE id=".$request_id);
                    Module::go_back();
                    break;



                case 'addnewfaq';
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$db2,$tpl,$server,$lang,$request_cat_id,$request_title,$request_FAQ,$request_tovcats,$request_cat_ids,$request_prod_ids;
                    //var_dump($request_cat_id,$request_title,$request_FAQ,$request_tovcats,$request_cat_ids,$request_prod_ids);

                    $db->query("INSERT INTO ".$server.$lang."_faqtov_faqtov (faq,title,cat_id,active) VALUES('{$request_FAQ}','{$request_title}','{$request_cat_id}','0')");
                    $last_id_faq = $db->lid();
                    if($request_tovcats == 1)
                    {
                        foreach($request_cat_ids as $row)
                        {
                            $db2->query("INSERT INTO ".$server.$lang."_faqtov_ids (id_faq,id_tov,id_cat) VALUES('{$last_id_faq}','0','{$row}')");

                        }
                    }
                    elseif($request_tovcats == 0)
                    {
                        foreach($request_prod_ids as $row)
                        {
                            $db2->query("INSERT INTO ".$server.$lang."_faqtov_ids (id_faq,id_tov,id_cat) VALUES('{$last_id_faq}','{$row}','0')");
                        }
                    }
                    header("Location: /admin.php?name=faqtov");


                    break;

                case 'addfaq':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$db2,$tpl,$server,$lang;
                    $main->include_main_blocks_2($this->module_name."_add.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->newBlock('add_new');
                    $tpl->assign("action","/admin.php?name=faqtov&action=addnewfaq");
                    $db->query("SELECT * FROM ".$server.$lang."_faqtov_faq WHERE id!=0");
                    while($db->next_record())
                    {
                        //var_dump(12);
                        $tpl->newBlock('cat');
                        $tpl->assign(Array(
                            "name" => $db->f('title'),
                            "id"    => $db->f('id')
                        ));
                    }

                    $db->query("SELECT * FROM ".$server.$lang."_catalog_categories");
                    while($db->next_record())
                    {
                        $tpl->newBlock("cat_list");
                        $tpl->assign(Array(
                            "id_cat"  	=> $db->f("id"),
                            "name_cat"	=> $db->f("title")
                        ));

                        $db2->query("SELECT * FROM ".$server.$lang."_catalog_products WHERE category_id=".$db->f('id'));
                        if($db2->nf()!=0) {
                            $tpl->newBlock("prod_cat");
                            $tpl->assign(Array(
                                "id_tov" => $db->f("id"),
                                "name_tov" => $db->f("title")
                            ));
                            $db2->query("SELECT * FROM " . $server . $lang . "_catalog_products WHERE category_id=" . $db->f('id'));
                            while ($db2->next_record()) {
                                $tpl->newBlock("prod_list");
                                $tpl->assign(Array(
                                    "id_tov" => $db2->f("id"),
                                    "name_tov" => $db2->f("product_title")
                                ));
                            }
                        }
                    }




                    break;


                case 'editfaq':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$db2,$db3,$tpl,$server,$lang,$request_id;
                    $main->include_main_blocks_2($this->module_name."_editfaqt.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->newBlock('edit_old');
                    $tpl->assign("action","/admin.php?name=faqtov&action=editfaqnow");
                    $db->query("SELECT * FROM ".$server.$lang."_faqtov_faqtov WHERE id = ".$request_id);
                    $db->next_record();
                    $tpl->assign("id_faq",$request_id);
                    $db2->query("SELECT * FROM ".$server.$lang."_faqtov_faq WHERE id!= 0 ");
                    while($db2->next_record())
                    {
                        $tpl->newBlock('cat');
                        $tpl->assign(Array(
                            "name" => $db2->f('title'),
                            "id"    => $db2->f('id'),
                            "selected" => $db2->f("id") == $db->f('cat_id')? "selected":""

                        ));
                    }

                    $db3->query("SELECT * FROM ".$server.$lang."_faqtov_ids WHERE id_faq =".$request_id);
                    $db3->next_record();
                    $tpl->gotoBlock("edit_old");
                    $tpl->assign("name_faq",$db->f('title'));
                    $tpl->assign("faq_text",$db->f('faq'));
                    $tpl->assign("check",$db3->f('id_tov')!=0?"checked":"");
                    $tpl->assign("check2",$db3->f('id_cat')!=0?"checked":"");
                    $tpl->assign("non_or_not1",$db3->f('id_tov')!=0?"display: block;":"display:none;");
                    $tpl->assign("non_or_not2",$db3->f('id_cat')!=0?"display: block;":"display:none;");





                    $db->query("SELECT * FROM ".$server.$lang."_catalog_categories");
                    while($db->next_record())
                    {
                        $tpl->newBlock("cat_list");
                        $tpl->assign(Array(
                            "id_cat"  	=> $db->f("id"),
                            "name_cat"	=> $db->f("title")
                        ));
                        $db3->query("SELECT * FROM ".$server.$lang."_faqtov_ids WHERE id_faq = $request_id AND id_cat = ".$db->f('id'));
                        $db3->next_record();

                        $tpl->assign("selected",$db->f('id') == $db3->f('id_cat')?"selected":"");

                        $db2->query("SELECT * FROM ".$server.$lang."_catalog_products WHERE category_id=".$db->f('id'));
                        if($db2->nf()!=0) {
                            $tpl->newBlock("prod_cat");
                            $tpl->assign(Array(
                                "id_tov" => $db->f("id"),
                                "name_tov" => $db->f("title")
                            ));
                            $db2->query("SELECT * FROM " . $server . $lang . "_catalog_products WHERE category_id=" . $db->f('id'));
                            while ($db2->next_record()) {

                                $tpl->newBlock("prod_list");
                                $tpl->assign(Array(
                                    "id_tov" => $db2->f("id"),
                                    "name_tov" => $db2->f("product_title")
                                ));
                                $db3->query("SELECT * FROM ".$server.$lang."_faqtov_ids WHERE id_faq = $request_id AND id_tov = ".$db2->f('id'));
                                $db3->next_record();

                                    $tpl->assign("selected",$db2->f('id') == $db3->f('id_tov')?"selected":"");

                            }
                        }
                    }


                    break;


                case "editfaqnow":
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$db2,$tpl,$server,$lang,$request_cat_id,$request_title,$request_FAQ,$request_tovcats,$request_cat_ids,$request_prod_ids,$request_id;
                    $db->query("UPDATE ".$server.$lang."_faqtov_faqtov SET faq='{$request_FAQ}',title='{$request_title}',cat_id='{$request_cat_id}',active='0' WHERE id=".$request_id);
                    $db->query("DELETE FROM ".$server.$lang."_faqtov_ids WHERE id_faq =".$request_id);
                    if($request_tovcats == 1)
                    {
                        foreach($request_cat_ids as $row)
                        {
                            $db2->query("INSERT INTO ".$server.$lang."_faqtov_ids (id_faq,id_tov,id_cat) VALUES('{$request_id}','0','{$row}')");

                        }
                    }
                    elseif($request_tovcats == 0)
                    {
                        foreach($request_prod_ids as $row)
                        {
                            $db2->query("INSERT INTO ".$server.$lang."_faqtov_ids (id_faq,id_tov,id_cat) VALUES('{$request_id}','{$row}','0')");
                        }
                    }
                    header("Location: /admin.php?name=faqtov");


                    break;


                case 'editcat':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$db2,$tpl,$server,$lang,$request_id;
                    $main->include_main_blocks_2($this->module_name."_editcat.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assignGlobal("action","/admin.php?name=faqtov&action=updatecategory");
                    $db->query("SELECT * FROM ".$server.$lang."_faqtov_faq WHERE id = ".$request_id);
                    $db->next_record();
                    $db2->query("SELECT * FROM ".$server.$lang."_faqtov_faq WHERE id!=$request_id");
                    while($db2->next_record())
                    {
                        $tpl->newBlock('cat_list');
                        $tpl->assign(Array(
                            "cat_name" => $db2->f('title'),
                            "id_c"    => $db2->f('id'),
                            "selected" => $db2->f('id') == $db->f('parent_id')?"selected":""
                        ));
                    }
                    $tpl->assignGlobal("namecat",$db->f('title'));
                    $tpl->assignGlobal("id_cat",$request_id);
                    break;



                case "addcategory":
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$tpl,$server,$lang,$request_category,$request_name_cat;
                    //var_dump("INSERT INTO ".$server.$lang."_faqtov_faq (title,active,parent_id) VALUES('$request_name_cat',1,$request_category)");
                    $db->query("INSERT INTO ".$server.$lang."_faqtov_faq (title,active,parent_id) VALUES('$request_name_cat',1,$request_category)");
                    header("Location: /admin.php?name=faqtov");
                    break;

                case "updatecategory":
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$tpl,$server,$lang,$request_category,$request_name_cat,$request_id_cat;
                    $db->query("UPDATE ".$server.$lang."_faqtov_faq SET title='{$request_name_cat}',parent_id=$request_category WHERE id=".$request_id_cat);
                    header("Location: /admin.php?name=faqtov");
                    break;


                case "addcat":
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$tpl,$server,$lang;
                    $main->include_main_blocks_2($this->module_name."_addcat.html", $this->tpl_path);
                    $tpl->prepare();
                    $db->query("SELECT * FROM ".$server.$lang."_faqtov_faq");
                    while($db->next_record())
                    {
                        $tpl->newBlock("cat_list");
                        $tpl->assign(Array(
                            "id_cat" 	=> $db->f('id'),
                            "cat_name"	=> $db->f('title')
                        ));
                    }
                    $tpl->assignGlobal("action","/admin.php?name=".$this->module_name."&action=addcategory");
                    break;

                case 'prp':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_step;
                    $main->include_main_blocks('_properties.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Information"	=> $main->_msg["Information"],
                        "MSG_Value"			=> $main->_msg["Value"],
                        "MSG_Save"			=> $main->_msg["Save"],
                        "MSG_Cancel"		=> $main->_msg["Cancel"],
                    ));
                    if (!$request_step || $request_step == 1) {
                        $tpl->assign(array('form_action'	=> $baseurl,
                            'step'			=> 2,
                            'lang'			=> $lang,
                            'name'			=> $this->module_name,
                            'action'			=> 'prp'));
                        $prp_html = $main->getModuleProperties($this->module_name);
                        $this->main_title = $this->_msg["Controls"];
                    } elseif (2 == $request_step) {
                        $main->setModuleProperties($this->module_name, $_POST);
                        header('Location: '.$baseurl.'&action=prp&step=1');
                    }
                    break;

                case 'block_prp':
                    global $request_sub_action, $request_block_id;
                    $arr  = $this->getEditLink($request_sub_action, $request_block_id);
                    $cont = 'var container = document.getElementById(\'target_span\');
		    		  	 container.innerHTML = "'.$this->getPropertyFields().'";
		    		  	 material_id		  = "'.$arr['material_id'].'";
		    		  	 material_url		  = "'.$arr['material_url'].'";
		    		  	 changeAction();
		    	      	 setURL("property1");';
                    header('Content-Length: '.strlen($cont));
                    echo $cont;
                    exit;
                    break;

// Редактирование свойств (параметров) блока
//-------------------------------------------------
                case "properties":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $module_id, $request_field, $request_block_id, $title;
                    $main->include_main_blocks("_block_properties.html", "main");
                    $tpl->prepare();
                    $main->show_actions($request_field, "", $this->block_main_module_actions, $this->block_module_actions);
                    //$tpl->assign("_ROOT.title", $title);
                    $tpl->assign(array('_ROOT.title'		=> $title,
                        '_ROOT.username'	=> $_SESSION['session_login'],
                        '_ROOT.password'	=> $_SESSION['session_password'],
                        '_ROOT.name'		=> $this->module_name,
                        '_ROOT.lang'		=> $lang,
                        '_ROOT.block_id'	=> $request_block_id,
                    ));
                    $this->main_title = $this->_msg["Block_properties"];
                    break;






// Создание объекта для дальнейшего использования
                case 'only_create_object':
                    break;

// Default...
//-----------------------------------------------------------------------------
                default:
                    $main->include_main_blocks();
                    $tpl->prepare();
            }
//--------------------------- конец обработки действий из админ части --------------------------//
        }
    }



    function getPropertyFields() {
        GLOBAL $request_sub_action, $db;
        if ('' != $request_sub_action) {
            $tpl = $this->getTmpProperties();
            switch ($request_sub_action) {
                case 'showbanner':
                    $tpl->newBlock('block_properties');
                    $tpl->assign(Array(
                        "MSG_Showing_banner_category" => $this->_msg["Showing_banner_category"],
                        "MSG_All_categories" => $this->_msg["All_categories"],
                        "MSG_Block_division" => $this->_msg["Block_division"],
                        "MSG_no" => $this->_msg["no"],
                        "MSG_yes" => $this->_msg["yes"],
                    ));
                    $ctgr_list = $db->fetchAll("{$this->table_prefix}_banners_categories");
                    abo_str_array_crop($ctgr_list);
                    $tpl->assignList('category_', $ctgr_list);
                    //$this->show_categories('block_categories');
                    break;
                case 'banners':
                    $tpl->newBlock('block_properties');
                    $tpl->assign(Array(
                        "MSG_Showing_banner_category" => $this->_msg["Showing_banner_category"],
                        "MSG_All_categories" => $this->_msg["All_categories"],
                        "MSG_Block_division" => $this->_msg["Block_division"],
                        "MSG_no" => $this->_msg["no"],
                        "MSG_yes" => $this->_msg["yes"],
                    ));
                    $ctgr_list = $db->fetchAll("{$this->table_prefix}_banners_categories");
                    abo_str_array_crop($ctgr_list);
                    $tpl->assignList('category_', $ctgr_list);
                    $tpl->newBlock('block_delay');
                    break;
                default:
                    $tpl->newBlock('block_properties_none');
            }
            return $this->getOutputContent($tpl->getOutputContent());
        }
        return FALSE;
    }



}