<?php // Vers 5.8.2 27.06.2012
/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
			  'faqtov_order_by'		=> array('type'		=> 'select',
												 'defval'	=> array('id DESC'	=> array(	'rus' => 'в порядке обратном добавлению',
												 											'eng' => 'reverce adding order'),
												 					 'id'		=> array(	'rus' => 'в порядке добавления',
												 											'eng' => 'adding order'),
												 					 ),
												 'descr'	=> array(	'rus' => 'Порядок вывода записей',
												 						'eng' => 'Show records order'),
												 'access'	=> 'editable',
												 ),

			   'faqtov_max_rows'		=> array('type'		=> 'integer',
			   									 'defval'	=> '10',
			   									 'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу',
												 						'eng' => 'Maximum quantity of records on page'),
			   									 'access'	=> 'editable'
			   									 ),

			   'faqtov_max_size' 		=> array('type'		=> 'integer',
			   									 'defval'	=> '0',
			   									 'descr'	=> array(	'rus' => 'Максимальный размер закачиваемой баннера в байтах (0 - размер не ограничен)',
												 						'eng' => 'Maximum size of uploaded banner in bytes (0 - size not limited)'),
			   									 'access'	=> 'editable'
			   									 ),
			   'faqtov_default_rights'	=> array('type'		=> 'string',
			   									 'defval'	=> '111111110001',
			   									 'descr'	=> array(	'rus' => 'Права для баннера по-умолчанию (формат <img src="/i/admin/rights_format.gif" width=72 height=18 alt="формат прав">, где x - 0 или 1, каждая группа представляет набор прав: удаление, публикация, редактирование, чтение)',
 												 						'eng' => 'Banner default rights (format <img src="/i/admin/rights_format.gif" width=72 height=18 alt="rights format">, where x - 0 or 1, every group is rights set: delete, publication, edit, read)'),
			   									 'access'	=> 'editable'
			   									 ),
			   );

$OPEN_NEW = array (
'faqtov_path' => 'files/images/bs/',
);

$NEW = array(
/* ABOCMS:START */
'faqtov_order_by' => 'id DESC',
'faqtov_max_rows' => '10',
'faqtov_max_size' => '0',
'faqtov_default_rights' => '111111110001',
/* ABOCMS:END */
);
?>