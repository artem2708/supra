<?php

$MOD_TABLES[] = "CREATE TABLE `sites_languages` (
					`id` int(10) unsigned NOT NULL auto_increment,
          `category_id` int(10) unsigned NOT NULL default '0',
          `title` char(255) NOT NULL default '',
          `language` char(255) NOT NULL default '',
          `is_fill` tinyint(1) unsigned NOT NULL default '0',
          `is_default` tinyint(1) unsigned NOT NULL default '0',
          PRIMARY KEY  (`id`),
          UNIQUE KEY `language` (`category_id`,`language`)
					) ENGINE=MyISAM AUTO_INCREMENT=1";

$MOD_TABLES[] = "CREATE TABLE `sites` (
					`id` int(10) unsigned NOT NULL auto_increment,
          `title` varchar(255) NOT NULL default '',
          `alias` varchar(31) NOT NULL default '',
          `descr` varchar(255) NOT NULL default '',
          `sitename` varchar(255) NOT NULL default '',
          `sitename_rus` varchar(255) NOT NULL default '',
          `multilanguage` tinyint(1) unsigned NOT NULL default '0',
          `contact_email` varchar(31) NOT NULL default '',
          `return_email` varchar(31) NOT NULL default '',
          `Vers` varchar(50) default NULL,
          `LU` int(12) unsigned default NULL,
          `LKEY` varchar(32) default NULL,
          PRIMARY KEY  (`id`),
          UNIQUE KEY `title` (`title`),
          UNIQUE KEY `alias` (`alias`)
					) ENGINE=MyISAM AUTO_INCREMENT=1";

$MOD_TABLES[] = "CREATE TABLE `sites_alias` (
					`id` int(10) unsigned NOT NULL auto_increment,
          `site_id` int(10) unsigned NOT NULL default '0',
          `alias` char(255) NOT NULL default '',
          PRIMARY KEY  (`id`)
					) ENGINE=MyISAM AUTO_INCREMENT=1";

$MOD_TABLES[] = "CREATE TABLE `sites_modules` (
					`site_id` int(10) unsigned NOT NULL auto_increment,
          `module_id` int(10) unsigned NOT NULL default '0',
          `active` tinyint(1) unsigned NOT NULL default '1',
          UNIQUE KEY `site_id` (`site_id`,`module_id`)
					) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `cron` (
					`id` int(10) unsigned NOT NULL auto_increment,
          `minute` tinyint(3) unsigned default NULL,
          `hour` tinyint(3) unsigned default NULL,
          `day` tinyint(3) unsigned default NULL,
          `month` tinyint(3) unsigned default NULL,
          `interval` int(10) unsigned default NULL,
          `title` varchar(255) NOT NULL default '',
          `command` varchar(255) NOT NULL default '',
          `active` tinyint(1) unsigned NOT NULL default '1',
          `last` datetime default NULL,
          PRIMARY KEY  (`id`)
					) ENGINE=MyISAM";
?>