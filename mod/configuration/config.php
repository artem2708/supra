<?php // Vers 5.8.2 27.06.2012
/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
	'configuration_admin_order_by'		=> array('type'		=> 'select',
												 'defval'	=> array('id DESC'	=> array(	'rus' => 'в порядке обратном добавлению',
												 											'eng' => 'reverce adding order'),
												 					 'id'		=> array(	'rus' => 'в порядке добавления',
												 											'eng' => 'adding order'),
												 					 ),
												 'descr'	=> array(	'rus' => 'Порядок вывода записей в адм.интерфейсе',
												 						'eng' => 'Show records order'),
												 'access'	=> 'editable',
												 ),

	'configuration_max_rows'			=> array('type'		=> 'integer',
		   									 	 'defval'	=> '20',
		   									 	 'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу',
												 						'eng' => 'Maximum quantity of records on page'),
		   									 	 'access'	=> 'editable'
		   									 	 ),
			   );

$OPEN_NEW = array(
	'configuration_site_files'			=> array(0	=> 'admin/files/{site}_internal_links.txt',
												),

	'configuration_site_dirs'			=> array(0	=> 'cache/',
												 1	=> 'i/admin/tpl/',
												 2	=> 'tpl/',
												 3	=> 'files/bs/',
												 4	=> 'files/catalog/',
												 5	=> 'files/downloads/categories/',
												 6	=> 'files/downloads/files/',
												 7	=> 'files/downloads/images/',
												 8	=> 'files/gallery/',
												 9	=> 'files/news/',
												 10	=> 'files/pages/',
												 11	=> 'files/pricelist/',
												 12	=> 'files/subscription/',
												 13	=> 'files/siteusers/',
												 14	=> 'admin/exchange/',
												 15	=> 'files/maps/',
												 16	=> 'files/support/',
												 17	=> 'files/blogs/',
												 18	=> 'files/documents/',
	                                             19	=> 'files/tags/',
												),

	'configuration_common_dirs'			=> array(0	=> 'files/friends/',
	                                       ),
);

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'configuration_admin_order_by' => 'id DESC',
'configuration_max_rows' => '10',
/* ABOCMS:END */
);
?>