<?php
class ConfigurationPrototype extends Module {
	var $version 				= '5.0.1';
	var $main_title;
	var $table_prefix;
	var $module_name			= 'configuration';
	var $default_action			= '';
	var $admin_default_action	= 'ADM_prp';
	var $tpl_path				= 'mod/configuration/tpl/';
	var $default_count_row		= 20;
	var $_msg = array();
	var $_vers = array(
			'Vers' => array('val'=>'varchar(50)'),
			'LU'   => array('val'=>'int(12) unsigned','create'=>' int(12) unsigned default null'),
		  'LKEY' => array('val'=>'varchar(32)')
		);
  var $mod = array(
		        'article',
		        'backup',
		        'banners',
		        'blogs',
		        'catalog',
		        'configuration',
		        'counter',
		        'documents',
		        'faq',
		        'forms',
		        'forum',
		        'friends',
		        'gallery',
		        'glossary',
		        'job',
		        'maps',
		        'news',
		        'pages',
		        'poll',
		        'pricelists',
		        'scripts',
		        'search',
		        'shop',
		        'sitelinks',
		        'siteusers',
		        'subscription',
		        'support');
	function ConfigurationPrototype($action = null){
		global $server, $lang, $main, $tpl;
		$this->table_prefix = $server.$lang;
		if($action!='only_create_object') $this->setLang();
		$action = $this->setAction($action);
		if (!isset($tpl)){
		  $main->message_die('Не подключен класс для работы с шаблонами');
		}
        call_user_func(array(&$this, $action));
	}
	function ADM_only_create_object(){
		return true;
	}
	function ADM_loaded(){
		global $db, $request_id,$request_lid, $request_type, $CONFIG;
		$ID = (int) $request_id; $LID = (int) $request_lid; $TYPE = ($request_type=='base') ? 'base' : 'tmpl';
		$db->query(
			"select concat_ws('_',s.alias, sl.language) pref FROM sites s ".
			" INNER JOIN sites_languages sl ON s.id=sl.category_id where s.id={$ID} AND sl.id={$LID}"
		);
		if(!$db->nf()) return false;
		$db->next_record();
		$_pref = $db->f('pref');
		$file_name = sprintf('%s(%s)_%s',$_pref,$TYPE, date('d.m.Y'));
		require_once(RP.'inc/class.archive.php');
		$tmpfname = tempnam(TRUE, "FOO");
		$arch = new gzip_file($tmpfname);
		$header[] = 'Content-Type: application/force-download';
		switch ($TYPE){
			case 'base':				
				$backup_mod = Module::load_mod('backup', 'only_create_object');
				$backup_mod->SetCmpr(1, 5);
				$res = $backup_mod->CreateBackup('', $backup_mod->getTables('^'.$_pref, '^'.$_pref.'_counter_all'));
				$header[] = 'Content-Disposition: attachment; filename="'.$res['filename'].'"';
				$tmpfname = RP.$CONFIG['backup_files_path'].$res['filename'];
				break;
			case 'tmpl':
				$header[] = 'Content-Disposition: attachment; filename="'.$file_name.'.tgz"';
    			$options = array(
    				'basedir' 		=> RP.'/tpl/'.$_pref,
    				'comment' 		=> "backup templates ({$_pref}); date: (".date('d/m/Y H:i').") ",
    				'storepaths' 	=> 0,
    				'overwrite' 	=> 1
    			);
    			$arch->set_options($options);
    			foreach(glob(RP.'/tpl/'.$_pref.'/*') as $v){$arch->add_files($v);}
    			$arch->create_archive();
				break;
		}
		if(file_exists($tmpfname)){
			$header[] = 'Content-Length:'.filesize($tmpfname);
			if(is_array($header)){foreach($header as $h){@header($h);}}
			readfile($tmpfname);
			unlink($tmpfname);
		}
		if($mysql_file && file_exists($mysql_file)) @unlink ($mysql_file);
		exit;
	}
	function ADM_prp(){
		global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions, $request_step, $TYPES_NEW;
		if (!$request_step || $request_step == 1) {
 			if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
 			$main->include_main_blocks('_properties.html', $this->tpl_path);
 			$tpl->prepare();
 			$tpl->assign(array(
				"MSG_Information"	=> $main->_msg["Information"],
				"MSG_Value"		=> $main->_msg["Value"],
				"MSG_Save"		=> $main->_msg["Save"],
				"MSG_Cancel"	=> $main->_msg["Cancel"],
				'form_action'	=> $baseurl,
				'step'			=> 2,
				'lang'			=> $lang,
				'name'			=> $this->module_name,
				'action'			=> 'prp'));
			$prp_html = $main->getModuleProperties();
			$this->main_title = $this->_msg["Controls"];
		}
		 elseif (2 == $request_step) {
			if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
			$main->include_main_blocks('_properties.html', $this->tpl_path);
			$tpl->prepare();
			$main->setModuleProperties(NULL, $_POST, $TYPES_NEW);
			header('Location: '.$baseurl.'&action=prp&step=1');
			exit;
		}
	}
	function ADM_sites(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
		global $request_start, $request_action;
		$main->include_main_blocks_2($this->module_name.'_list.html', $this->tpl_path);
		$tpl->prepare();
		$tpl->assign(Array(
			"MSG_Add_site" => $this->_msg["Add_site"],
		));
		$start	= ($request_start) ? $request_start	: 1;
		$tpl->assign('add_site', $baseurl.'&action=add');
		$tpl->newBlock('block_categories');
		$tpl->assign(Array(
			"MSG_Ed" => $this->_msg["Ed"],
			"MSG_Domain" => $this->_msg["Domain"],
			"MSG_Name" => $this->_msg["Name"],
			"MSG_Description" => $this->_msg["Description"],
			"MSG_Modules" => $this->_msg["Modules"],
			"MSG_Ad" => $this->_msg["Ad"],
			"MSG_Del" => $this->_msg["Del"],
		));
		$arr = $this->getSites($start);

		if ('' != $arr[0]['id']) {

           	$tpl->assign_array('block_category', $arr);
		}
        $category = (isset($category)) ? (int) $category : 0;
		$main->_show_nav_string('sites',
								'', '',
								'action='.$request_action,
								$start,
								$CONFIG['configuration_max_rows'],
								$CONFIG['configuration_admin_order_by'], '');
		$tpl->assign(Array(
			"MSG_Pages" => $this->_msg["Pages"],
		));

		$this->main_title = $this->_msg["Sites"];
	}

	function ADM_menu(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions, $db, $db1;
		if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
		global $request_start;
		$main->include_main_blocks_2($this->module_name.'_menu_list.html', $this->tpl_path);
		$tpl->prepare();
		
		$tpl->newBlock('block_edit_menu');
		$tpl->assign(Array(
		        "group_action" 		=> $baseurl."&action=gr_act&parent=1",
		        "MSG_Title" => $this->_msg["Name"],
		        "MSG_Rank" => $this->_msg["Rank"],
		        "baseurl" => $baseurl
		));
		
		$db->query('SELECT * FROM modules WHERE parent_id="0" ORDER BY rank ASC');
		if ($db->nf()>0) {
		    while ($db->next_record()) {		        
		        $sub_menu = array();
		        $db1->query('SELECT * FROM modules WHERE parent_id='.$db->f('id').' ORDER BY rank ASC');
		        if($db1->nf()>0) {
		            while($db1->next_record()) {
		                $sub_menu[] = array('id' => $db1->f('id'), 'title' => $db1->f('title'), 'rank' => $db1->f('rank'));
		            }
		        }
		        $menu[] = array('id' => $db->f('id'), 'title' => $db->f('title'), 'rank' => $db1->f('rank'), 'sub_menu' => $sub_menu);
		    }
		}
		
		$cnt0 = count($menu);
		for ($i=0; $i<$cnt0; $i++) {
		    $tpl->newBlock('block_menu_fields');
		    $tpl->assign(array(
		        "MSG_Move_up" => $this->_msg['Move_up'],
		        "MSG_Move_down" => $this->_msg['Move_down'],
		        "id" => $menu[$i]['id'],
		        "title_menu" => $menu[$i]['title'],
		        'cat_up_link' => !$i ? '#' : $baseurl."&action=upcat&id=".$menu[$i]['id']."&next=".$menu[$i-1]['id']."&rank=".$menu[$i]['rank'],
		        'cat_down_link' => $i == $cnt0-1 ? '#' : $baseurl."&action=downcat&id=".$menu[$i]['id']."&rew=".$menu[$i+1]['id']."&rank=".$menu[$i]['rank'],
		        'img_down' => $i == $cnt0-1 ? "ico_ligrdw.gif" : "ico_listdw.gif",
		        'img_up' => !$i ? "ico_ligrup.gif" : "ico_listup.gif",
		    ));
		    $tpl->newBlock('block_menu_level_0');
		    $tpl->assign(array(
		        "title_menu" => $menu[$i]['title'],
		    ));
		    $tpl->newBlock('block_parent');
		    $tpl->assign(array(
		        "title_menu" => $menu[$i]['title'],
		        "id" => $menu[$i]['id'],
		    ));
		    $cnt1 = count($menu[$i]['sub_menu']);
		    for ($j=0; $j<$cnt1; $j++) {
		        $tpl->newBlock('block_menu_level_1');
		        $tpl->assign(array(
		            "MSG_Move_up" => $this->_msg['Move_up'],
		            "MSG_Move_down" => $this->_msg['Move_down'],
		            "id" => $menu[$i]['sub_menu'][$j]['id'],
		            "title_menu" => $menu[$i]['sub_menu'][$j]['title'],
		            'cat_up_link' => !$j ? '#' : $baseurl."&action=upcat&id=".$menu[$i]['sub_menu'][$j]['id']."&next=".$menu[$i]['sub_menu'][$j-1]['id']."&rank=".$menu[$i]['sub_menu'][$j]['rank'],
		            'cat_down_link' => $j == $cnt1-1 ? '#' : $baseurl."&action=downcat&id=".$menu[$i]['sub_menu'][$j]['id']."&rew=".$menu[$i]['sub_menu'][$j+1]['id']."&rank=".$menu[$i]['sub_menu'][$j]['rank'],		            
		            'img_up' => !$j ? "ico_ligrup.gif" : "ico_listup.gif",
		            'img_down' => $j == $cnt1-1 ? "ico_ligrdw.gif" : "ico_listdw.gif",
		        ));
		    }
		}

		$this->main_title = $this->_msg["Menu"];
	}

	function ADM_upcat() {
        global $request_id, $request_next, $request_rank, $db, $baseurl;
        $request_id = (int)$request_id;
        $request_next = (int)$request_next;
        $rank_up = $request_rank-1;
        $rank_dw = $request_rank+1;
        $db->query('UPDATE modules set rank='.$rank_up.' WHERE id='.$request_id);
        $db->query('UPDATE modules set rank='.$rank_dw.' WHERE id='.$request_next);
        header('Location: '.$baseurl.'&action=menu');
	}

	function ADM_downcat() {
        global $request_id, $request_rew, $request_rank, $db, $baseurl;
        $request_id = (int)$request_id;
        $request_rew = (int)$request_rew;
        $rank_up = $request_rank+1;
        $rank_dw = $request_rank-1;
        $db->query('UPDATE modules set rank='.$rank_up.' WHERE id='.$request_id);
        $db->query('UPDATE modules set rank='.$rank_dw.' WHERE id='.$request_rew);
        header('Location: '.$baseurl.'&action=menu');
	}

	function ADM_changeparent() {
        global $request_id, $request_parent, $db, $baseurl;
        $request_parent = (int)$request_parent;
        $request_id = array_map('intval', $request_id);
        $db->query('SELECT * FROM modules WHERE parent_id=0 AND id='.$request_parent);
        if ($db->nf()>0 && !empty($request_id)) {
            $db->query('UPDATE modules SET parent_id='.$request_parent.' WHERE id IN ('
                .implode(',', $request_id).')');
        }
        header('Location: '.$baseurl.'&action=menu');
	}

	function ADM_add(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
		global $request_step, $request_title, $request_alias, $request_descr, $request_aliases,
				$request_sitename, $request_sitename_rus, $request_multilanguage,
				$request_contact_email, $request_return_email;

		$main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path);
		$tpl->prepare();
		$tpl->assign(Array(
			'title' => $request_title,
			'alias' => $request_alias,
			'descr' => $request_descr,
			'aliases' => $request_aliases,
			'sitename' => $request_sitename,
			'sitename_rus' => $request_sitename_rus,
			'multilanguage' => $request_multilanguage,
			'contact_email' => $request_contact_email,
			'return_email' => $request_return_email,
			"MSG_Enter_domain" => $this->_msg["Enter_domain"],
			"MSG_Enter_domain_prefix" => $this->_msg["Enter_domain_prefix"],
			"MSG_Enter_site_name" => $this->_msg["Enter_site_name"],
			"MSG_Enter_valid_contact_email" => $this->_msg["Enter_valid_contact_email"],
			"MSG_Enter_valid_feedback_email" => $this->_msg["Enter_valid_feedback_email"],
			"MSG_Domain_without_http" => $this->_msg["Domain_without_http"],
			"MSG_Prefix_for_tables_folders" => $this->_msg["Prefix_for_tables_folders"],
			"MSG_Aliases_with_breaklines" => $this->_msg["Aliases_with_breaklines"],
			"MSG_Site_name" => $this->_msg["Site_name"],
			"MSG_Site_name_for_mails" => $this->_msg["Site_name_for_mails"],
			"MSG_Multilanguage_support" => $this->_msg["Multilanguage_support"],
			"MSG_Contact_email_on_site" => $this->_msg["Contact_email_on_site"],
			"MSG_Feedback_email_on_mail" => $this->_msg["Feedback_email_on_mail"],
			"MSG_Description" => $this->_msg["Description"],
			"MSG_required_fields" => $this->_msg["required_fields"],
			"MSG_Save" => $this->_msg["Save"],
			"MSG_Cancel" => $this->_msg["Cancel"],
		));

		$tpl->assign(
			array(
				'form_action'	=>	$baseurl.'&action=add&step=2',
				'cancel_link'	=>	$baseurl.'&action=sites',
			)
		);
		$this->main_title = $this->_msg["Add_site"];

		if ($request_step == 2) {
		  	$res = $this->addSite($request_title,
		  						$request_alias,
		  						$request_descr,
		  						explode("\n", $request_aliases),
		  						$request_sitename,
		  						$request_sitename_rus,
		  						$request_multilanguage,
		  						$request_contact_email,
		  						$request_return_email);
		  	switch ($res) {
		  		case 0:
		  			$tpl->newBlock('block_err');
		  			$tpl->assign("MSG_Error", $this->_msg["Error"]);
		  			break;
				case -1:
					$tpl->newBlock('block_err_obligatory');
					$tpl->assign("MSG_Not_all_required_fields", $this->_msg["Not_all_required_fields"]);
					break;

				case -2:
					$tpl->newBlock('block_err_domain');
					$tpl->assign("MSG_Domain_or_alias_exists", $this->_msg["Domain_or_alias_exists"]);
					break;

				case -3:
					$tpl->newBlock('block_err_pref');
					$tpl->assign("MSG_Prefix_reserved", $this->_msg["Prefix_reserved"]);
					break;

				default:
				    header('Location: '.$baseurl.'&action=sites');
			        exit;
					break;
			}
		}
	}
	function ADM_edit(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
		global $request_step, $request_id, $request_err, $request_title, $request_alias,
				$request_descr, $request_aliases, $request_sitename, $request_sitename_rus,
				$request_multilanguage, $request_contact_email, $request_return_email;
		if (!$request_step || $request_step == 1) {
			$main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path);
			$tpl->prepare();
			$arr = $this->getSite($request_id);
			$tpl->assign($arr[0]);
			$tpl->assign(array(
				'_ROOT.form_action'	=>	$baseurl.'&action=edit&step=2',
				'_ROOT.cancel_link'	=>	$baseurl.'&action=sites',
				"MSG_Enter_domain" => $this->_msg["Enter_domain"],
				"MSG_Enter_domain_prefix" => $this->_msg["Enter_domain_prefix"],
				"MSG_Enter_site_name" => $this->_msg["Enter_site_name"],
				"MSG_Enter_valid_contact_email" => $this->_msg["Enter_valid_contact_email"],
				"MSG_Enter_valid_feedback_email" => $this->_msg["Enter_valid_feedback_email"],
				"MSG_Domain_without_http" => $this->_msg["Domain_without_http"],
				"MSG_Prefix_for_tables_folders" => $this->_msg["Prefix_for_tables_folders"],
				"MSG_Aliases_with_breaklines" => $this->_msg["Aliases_with_breaklines"],
				"MSG_Site_name" => $this->_msg["Site_name"],
				"MSG_Site_name_for_mails" => $this->_msg["Site_name_for_mails"],
				"MSG_Multilanguage_support" => $this->_msg["Multilanguage_support"],
				"MSG_Contact_email_on_site" => $this->_msg["Contact_email_on_site"],
				"MSG_Feedback_email_on_mail" => $this->_msg["Feedback_email_on_mail"],
				"MSG_Description" => $this->_msg["Description"],
				"MSG_required_fields" => $this->_msg["required_fields"],
				"MSG_Save" => $this->_msg["Save"],
				"MSG_Cancel" => $this->_msg["Cancel"],
			));
			$err = isset($request_err) ? $request_err : 1;
			switch ($err) {
				case 0:
					$tpl->newBlock('block_err');
					$tpl->assign("MSG_Error", $this->_msg["Error"]);
					break;

			 	case -1:
			 		$tpl->newBlock('block_err_obligatory');
			 		$tpl->assign("MSG_Not_all_required_fields", $this->_msg["Not_all_required_fields"]);
			 		break;

			 	case -2:
			 		$tpl->newBlock('block_err_domain');
			 		$tpl->assign("MSG_Domain_or_alias_exists", $this->_msg["Domain_or_alias_exists"]);
			 		break;

			 	case -3:
			 		$tpl->newBlock('block_err_pref');
			 		$tpl->assign("MSG_Prefix_reserved", $this->_msg["Prefix_reserved"]);
			 		break;
			 }
			$tpl->assign(Array(
				"MSG_Not_all_required_fields" => $this->_msg["Not_all_required_fields"],
				"MSG_Domain_or_alias_exists" => $this->_msg["Domain_or_alias_exists"],
				"MSG_Prefix_reserved" => $this->_msg["Prefix_reserved"],
				"MSG_Error" => $this->_msg["Error"],
			));
			$this->main_title = $this->_msg["Edit_site"];
		} else if ($request_step == 2) {
			$res = $this->updateSite($request_id,
                               $request_title,
                               $request_alias,
                               $request_descr,
                               explode("\n", $request_aliases),
                               $request_sitename,
                               $request_sitename_rus,
                               $request_multilanguage,
                               $request_contact_email,
                               $request_return_email);
			//exit;
		   	if ($res < 1) {
		  		header('Location: '.$baseurl.'&action=edit&id='.$request_id.'&err='.$res);
		  		exit;
		  	}
			header('Location: '.$baseurl.'&action=sites');
			exit;
		}
	}
	function ADM_delete(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['d']) $main->message_access_denied($this->module_name, $action);
		global $request_id;
		$this->removeSite($request_id);
		header('Location: '.$baseurl.'&action=sites');
	}
	function ADM_mod(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
		global $request_id, $request_mod;
		$main->include_main_blocks_2($this->module_name.'_modules.html', $this->tpl_path);
		$tpl->prepare();
		$tpl->assign(Array(
			"MSG_Module" => $this->_msg["Module"],
			"MSG_Save" => $this->_msg["Save"],
		));

		if (!$site = $this->getSite($request_id)) {
		    header('Location: '.$baseurl.'&action=sites');
		}

		if (isset($request_mod) && is_array($request_mod)) {
			$this->setSiteModules($request_id, $request_mod);
		}
		$tpl->assign(
			array(
				'action' =>	$baseurl."&action=mod&id=".$request_id,
			)
		);
		$tpl->assign_array('block_module', $this->getSiteModules($request_id));

		$this->main_title = $this->_msg["Site_modules"] . ' \''.$site[0]['title'].' '.$site[0]['sitename'].'\'';
	}
	function ADM_active(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['p']) $main->message_access_denied($this->module_name, $action);
		global $request_id;
		$this->activeSite($request_id);
		header('Location: '.$baseurl.'&action=sites');
	}
	function ADM_suspend(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['p']) $main->message_access_denied($this->module_name, $action);
		global $request_id;
		$this->suspendSite($request_id);
		header('Location: '.$baseurl.'&action=sites');
	}
	function ADM_langs(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
		global $request_start, $request_id;
		$main->include_main_blocks_2($this->module_name.'_langs_list.html', $this->tpl_path);
		$tpl->prepare();
		$tpl->assign(Array(
			"MSG_Add_language" => $this->_msg["Add_language"],
		));
		$start	= ($request_start) ? $request_start	: 1;

        $arr	= $this->getSite($request_id);

		$langs	= $this->getLangs($request_id);
		$tpl->assign('add_lang', $baseurl.'&action=addlang&id='.$request_id);
		$tpl->newBlock('block_categories');
		$tpl->assign(Array(
			"MSG_Ed" => $this->_msg["Ed"],
			"MSG_Fill" => $this->_msg["Fill"],
			"MSG_Language" => $this->_msg["Language"],
			"MSG_Description" => $this->_msg["Description"],
			"MSG_Default" => $this->_msg["Default"],
			"MSG_Del" => $this->_msg["Del"],
		));
		if ('' != $langs[0]['id']) {
		    foreach ($langs as $key => $val) {
		    $tpl->newBlock('block_category');
				$tpl->assign(Array(
					"MSG_Edit" => $this->_msg["Edit"],
					"MSG_All_data_will_lost" => $this->_msg["All_data_will_lost"],
					"MSG_Language_default" => $this->_msg["Language_default"],
					"MSG_Do_language_default" => $this->_msg["Do_language_default"],
					"MSG_Confirm_delete_site" => $this->_msg["Confirm_delete_site"],
					"MSG_Delete" => $this->_msg["Delete"],
				));
		        $tpl->assign($val);
		        if ($val['is_default']) {
		        	$tpl->newBlock('block_default_lang');
		        } else {
		            $tpl->newBlock('block_not_default_lang');
		            $tpl->assign(Array(
		              'default_lang_link' => $baseurl.'&action=set_default&id='.$request_id.'&lid='.$val['id'],
		              "MSG_Do_language_default" => $this->_msg["Do_language_default"]
		              ));
		        }
		    }
        	//$tpl->assign_array('block_category', $langs);
		}
		$tpl->assign(Array(
			"nav.MSG_Pages" => $this->_msg["Pages"],
		));
		$this->main_title = $this->_msg["Site_languages"] . ' '.$arr[0]['title'];
	}
	function ADM_addlang(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
		global $request_step, $request_id, $request_site_lang, $request_descr,
				$request_siteuser, $request_siteuserinfo;
		$main->include_main_blocks_2($this->module_name.'_lang_edit.html', $this->tpl_path);
		$tpl->prepare();
		$tpl->assign(Array(
			"MSG_Language" => $this->_msg["Language"],
			"MSG_Description" => $this->_msg["Description"],
			"MSG_required_fields" => $this->_msg["required_fields"],
			"MSG_Save" => $this->_msg["Save"],
			"MSG_Cancel" => $this->_msg["Cancel"],
		));
		$arr	= $this->getSite($request_id);
		$select = $main->getSelectTag($CONFIG['site_langs'], 0);
		$select = $this->parseLangs($request_id, NULL, $select);
		$tpl->assign(array(
				'id'			=> $request_id,
				'form_action'	=> $baseurl.'&action=addlang&step=2',
				'cancel_link'	=> $baseurl.'&action=langs',
			));
		$tpl->assign_array('block_langs', $select);
		$tpl->newBlock('block_add_siteuser_js');
		$tpl->assign(array(
				'MSG_Enter_login'		=> $this->_msg["Enter_login"],
				'MSG_Enter_password'	=> $this->_msg["Enter_password"],
				'MSG_Confirm_password'	=> $this->_msg["Confirm_password"],
				'MSG_Passwords_differ'	=> $this->_msg["Passwords_differ"],
				'MSG_Enter_email'		=> $this->_msg["Enter_email"],
				'MSG_Enter_valid_email'	=> $this->_msg["Enter_valid_email"],
			));
		$tpl->newBlock('block_admin_siteuser');
		$tpl->assign(array(
				'siteusername'			=> $request_siteuserinfo['siteusername'],
				'password'				=> $request_siteuserinfo['password'],
				'confirm_password'		=> $request_siteuserinfo['confirm_password'],
				'email'					=> $request_siteuserinfo['email'],
				'MSG_Fill_next_form'	=> $this->_msg["Fill_next_form"],
				'MSG_Login'				=> $this->_msg["Login"],
				'MSG_Password'			=> $this->_msg["Password"],
				'MSG_Confirm_password'	=> $this->_msg["Confirm_password"],
				'MSG_Email'				=> $this->_msg["Email"],
				"MSG_not_less_than"		=> $this->_msg["not_less_than"],
				"MSG_and_not_more_than" => $this->_msg["and_not_more_than"],
				"MSG_symbols"			=> $this->_msg["symbols"],
				'max_login_len'			=> $CONFIG['siteusers_max_login_len'],
				'min_login_len'			=> $CONFIG['siteusers_min_login_len'],
				'max_pass_len'			=> $CONFIG['siteusers_max_pass_len'],
				'min_pass_len'			=> $CONFIG['siteusers_min_pass_len'],
			));
		$users = $this->getUsers(isset($request_siteuser) ? $request_siteuser : $_SESSION["siteuser"]["id"],
								$arr[0]["alias"]);
		if ($users) {
			$tpl->newBlock('block_siteuser_select');
			$tpl->assign(array(
					'MSG_Select_admin_user'	=> $this->_msg["Select_admin_user"],
					'MSG_new_user'			=> $this->_msg["new_user"],
					'MSG_OR'				=> $this->_msg["OR"],
				));
			$tpl->assign_array('block_user', $users);
		} else {
			$tpl->newBlock('block_siteuser_no_select');
		}
		$this->main_title = $this->_msg["Add_site_language"] . ' '.$arr[0]['title'];
		if ($request_step == 2) {
			$res = 0;
			if ($this->testUser($request_siteuser)) {
				$lang_id = $this->addLang($request_id, $request_site_lang, $request_descr);
				header('Location: '.$baseurl.'&action=langs&id='.$request_id);
				exit;
			}
			switch ($res) {
				case -1:
					$tpl->newBlock('block_err_siteuser');
					$tpl->assign("MSG_Error_siteuser", $this->_msg["Error_siteuser"]);
					break;

				case -2:
					$tpl->newBlock('block_siteuser_exist');
					$tpl->assign("MSG_Siteuser_exist", $this->_msg["Siteuser_exist"]);
					break;
			}
		}
	}
	function ADM_editlang(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
		global $request_step, $request_id, $request_lid, $request_site_lang, $request_descr;
		if (!$request_step || $request_step == 1) {
			$main->include_main_blocks_2($this->module_name.'_lang_edit.html', $this->tpl_path);
			$tpl->prepare();
			$tpl->assign(Array(
				"MSG_Language" => $this->_msg["Language"],
				"MSG_Description" => $this->_msg["Description"],
				"MSG_required_fields" => $this->_msg["required_fields"],
				"MSG_Save" => $this->_msg["Save"],
				"MSG_Cancel" => $this->_msg["Cancel"],
			));
			$arr		= $this->getSite($request_id);
			$site_lang	= $this->getLang($request_lid);
			$tpl->assign($site_lang[0]);
			$tpl->assign(
				array(
					'id'			=> $request_id,
					'form_action'	=> $baseurl.'&action=editlang&step=2',
					'cancel_link'	=> $baseurl.'&action=langs',
				)
			);
            $select = $this->getSelectTag($request_lid);
			$select = $this->parseLangs($request_id, $request_lid, $select);
			$tpl->assign_array('block_langs', $select);
			$this->main_title = $this->_msg["Edit_site_language"] . ' '.$arr[0]['title'];
		} else if ($request_step == 2) {
			$this->updateLang($request_lid, $request_site_lang, $request_descr);
			header('Location: '.$baseurl.'&action=langs&id='.$request_id);
			exit;
		}
	}
	function ADM_dellang(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['d']) $main->message_access_denied($this->module_name, $action);
		global $request_lid, $request_id;
		$this->removeLang($request_lid);
		header('Location: '.$baseurl.'&action=langs&id='.$request_id);
	}
	function ADM_demofill(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
		global $request_lid, $request_id;
		$this->fillByDemoContent($request_lid);
		header('Location: '.$baseurl.'&action=langs&id='.$request_id);
	}
	function ADM_democlear(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
		global $request_lid, $request_id;
		$this->clearByDemoContent($request_lid);
		header('Location: '.$baseurl.'&action=langs&id='.$request_id);
	}
	function ADM_set_default(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
		global $request_lid, $request_id;

		if ($this->setDefaultLang($request_id, $request_lid)) {
		    header('Location: '.$baseurl.'&action=langs&id='.$request_id);
		} else {
		    header('Location: '.$baseurl.'&action=langs&id='.$request_id."&err=not_set_def_lang");
		}

	}
	function ADM_log(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
		global $request_clear, $request_date, $request_cnt, $request_start, $request_filtr;

		if (isset($request_clear)) {
			$main->include_main_blocks_2($this->module_name.'_log_clear.html', $this->tpl_path);
		    $tpl->prepare();
			$tpl->assign(Array(
				"MSG_Enter_date" => $this->_msg["Enter_date"],
				"MSG_Confirm_delete_records_before" => $this->_msg["Confirm_delete_records_before"],
				"MSG_Total_records" => $this->_msg["Total_records"],
				"MSG_Delete_records_before" => $this->_msg["Delete_records_before"],
				"MSG_Delete" => $this->_msg["Delete"],
				"MSG_Cancel" => $this->_msg["Cancel"],
			));

		    if (isset($request_date)) {
		        $tpl->newBlock('block_del');
				$tpl->assign(Array(
					"MSG_Deleted_records" => $this->_msg["Deleted_records"],
				));
		        $tpl->assign('del_records', $this->delLogRecords($request_date));
		        $tpl->gotoBlock('_ROOT');
		    }
		    $tpl->assign(array('lang' => $lang,
    		                   'baseurl' => $baseurl,
    		                   'log_amount' => $this->getLogRecordAnount()
    		                   ));
		    $this->main_title = $this->_msg["Clear_logs"];
		}

		$main->include_main_blocks_2($this->module_name.'_log.html', $this->tpl_path);
		$tpl->prepare();
		$tpl->assign(Array(
			"MSG_Enter_filter_parameter" => $this->_msg["Enter_filter_parameter"],
			"MSG_Delete_old_records" => $this->_msg["Delete_old_records"],
			"MSG_Filter" => $this->_msg["Filter"],
			"MSG_User" => $this->_msg["User"],
			"MSG_Choose" => $this->_msg["Choose"],
			"MSG_Module" => $this->_msg["Module"],
			"MSG_Choose" => $this->_msg["Choose"],
			"MSG_Action" => $this->_msg["Action"],
			"MSG_Date_from" => $this->_msg["Date_from"],
			"MSG_to" => $this->_msg["to"],
			"MSG_Clear_period" => $this->_msg["Clear_period"],
			"MSG_show_upto" => $this->_msg["show_upto"],
			"MSG_records" => $this->_msg["records"],
			"MSG_Reset" => $this->_msg["Reset"],
			"MSG_ID" => $this->_msg["ID"],
			"MSG_Time" => $this->_msg["Time"],
			"MSG_IP" => $this->_msg["IP"],
			"MSG_Request_URL" => $this->_msg["Request_URL"],
		));

		$cnt = $request_cnt > 1 ? (int) $request_cnt : $this->default_count_row;
		$start = $request_start > 1 ? (int)$request_start : 1;
		$tpl->assign(array('lang' => $lang,
		                   'start' => $start,
		                   'date_from_val' => !$request_filtr['from'] && !$request_filtr['to'] ? date("d-m-Y"): $request_filtr['from'],
		                   'date_to_val' => !$request_filtr['to'] ? date("d-m-Y"): $request_filtr['to'],
		                   'baseurl' => $baseurl,
		                   'cnt_val' => $cnt
		                   ));
		Main::show_list($this->getLogModules(), 'block_mod', $request_filtr['mod']);
		Main::show_list($this->getLogActions(), 'block_action', $request_filtr['action']);
		Main::show_list($this->getLogUsers(), 'block_user', $request_filtr['user']);

		$log = $this->getLog($request_filtr, $cnt, $request_start);
		if (count($log) > 0) {
		    $amount = $log['amount'];
		    unset($log['amount']);
		    $tpl->assign_array('block_log_row', $log);
		    Main::_show_nav_block(ceil($amount/$cnt), 'nav', ConfigurationPrototype::getLogRequestStr(), $start);
		}

		$period_title = " " . $this->_msg["for_this_day"];
		if ($request_filtr['from'] || $request_filtr['to']) {
			$period_title = $request_filtr['from'] ? " ".$this->_msg["from"]." ".$request_filtr['from'] : "";
			$period_title .= $request_filtr['to'] ? " ".$this->_msg["to"]." ".$request_filtr['to'] : "";
		}
		$tpl->assign(Array(
			"nav.MSG_Pages" => $this->_msg["Pages"],
		));
		$this->main_title = $this->_msg["Users_actions"] . ''.$period_title;
	}
	function ADM_cron(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);

		$main->include_main_blocks_2($this->module_name.'_cron.html', $this->tpl_path);
		$tpl->prepare();
		$tpl->assign(Array(
			"MSG_Add" => $this->_msg["Add"],
			"MSG_Suspend" => $this->_msg["Suspend"],
			"MSG_Activate" => $this->_msg["Activate"],
			"MSG_Cron_information" => $this->_msg["Cron_information"],
		));

		$this->showCronTab();

		$this->main_title = $this->_msg["Scheduler"];
	}
	function ADM_new_cmd(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
		global $request_id;

		$main->include_main_blocks_2($this->module_name.'_cron_edit.html', $this->tpl_path);
		$tpl->prepare();
		$tpl->assign(Array(
			"MSG_Execute_script" => $this->_msg["Execute_script"],
			"MSG_Name" => $this->_msg["Name"],
			"MSG_with_period" => $this->_msg["with_period"],
			"MSG_min" => $this->_msg["min"],
			"MSG_or" => $this->_msg["or"],
			"MSG_enter_time" => $this->_msg["enter_time"],
			"MSG_Minute" => $this->_msg["Minute"],
			"MSG_Hour" => $this->_msg["Hour"],
			"MSG_Day" => $this->_msg["Day"],
			"MSG_Month" => $this->_msg["Month"],
			"MSG_Any_f" => $this->_msg["Any_f"],
			"MSG_Any_m" => $this->_msg["Any_m"],
			"MSG_Save" => $this->_msg["Save"],
			"MSG_Cancel" => $this->_msg["Cancel"],
			"MSG_Not_entered_execute_cmd" => $this->_msg["Not_entered_execute_cmd"],
			"MSG_Not_entered_name" => $this->_msg["Not_entered_name"],
			"MSG_Interval_must_be_greater" => $this->_msg["Interval_must_be_greater"],
		));

		if (!$this->showCronForm($request_id)) {
		    header('Location: '.$baseurl."&action=cron");
		    exit;
		}

		$this->main_title = $request_id ? $this->_msg["Edit"] : $this->_msg["New_task"];
	}
	function ADM_edit_cmd(){return $this->ADM_new_cmd();}
	function ADM_add_cmd(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
		global $request_minute, $request_hour, $request_day, $request_month,
				$request_interval, $request_command, $request_title;

		$this->addCronCommand(
		    $request_minute,
		    $request_hour,
		    $request_day,
		    $request_month,
		    $request_interval,
		    $request_command,
		    $request_title);

		header('Location: '.$baseurl."&action=cron");
		exit;
	}
	function ADM_update_cmd(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
		global $request_id, $request_minute, $request_hour, $request_day, $request_month,
				$request_interval, $request_command, $request_title;

		$this->setCronCommand(
		    $request_id,
		    array (
		        'time' => array (
		            'm' => $request_minute,
		            'h' => $request_hour,
		            'd' => $request_day,
		            'mn' => $request_month,
		            'i' => $request_interval
		        ),
		        'cmd' => $request_command,
		        'title' => $request_title
		    )
		);

		header('Location: '.$baseurl."&action=cron");
		exit;
	}
	function ADM_del_cmd(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
		global $request_id;

		$this->delCronCommand($request_id);

		header('Location: '.$baseurl."&action=cron");
		exit;
	}
	function ADM_suspend_cmd(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
	    if (!$permissions["e"]) $main->message_access_denied($sitelinks->module_name, $action);
		global $request_id, $request_JsHttpRequest;
	    if ($this->setCronCommand($request_id, array('active' => 0))
	        && isset($request_JsHttpRequest)
	    ) {
			$_RESULT = array(
				"cmd_id"	=> $request_id,
				"active"	=> 0,
			);
	    }
	    exit;
	}
	function ADM_activate_cmd(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
	    if (!$permissions["e"]) $main->message_access_denied($sitelinks->module_name, $action);
		global $request_id, $request_JsHttpRequest;
	    if ($this->setCronCommand($request_id, array('active' => 1))
	        && isset($request_JsHttpRequest)
	    ) {
			$_RESULT = array(
				"cmd_id"	=> $request_id,
				"active"	=> 1,
			);
	    }
	    exit;
	}
	function ADM_show_info(){
		global $main,  $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $permissions;
		if (!$permissions["e"]) $main->message_access_denied($sitelinks->module_name, $action);
		$main->include_main_blocks_2($this->module_name.'_updates.html', $this->tpl_path);
		$tpl->prepare();
		$tpl->AssignGlobal('baseurl' , $baseurl);
		$tpl->newBlock('MAIN');
		$tpl->assign(Array(
			"MSG_Module" 		=> $this->_msg["Module"],
			"MSG_LocalVersion" 	=> $this->_msg["LocalVersion"],
			"MSG_ServerVersion" => $this->_msg["ServerVersion"],
			"MSG_Backup" 		=> $this->_msg["Backup"],
			"MSG_Update" 		=> $this->_msg["Update"],
			"MSG_CheckUpdate" 	=> $this->_msg["CheckUpdate"],
			"MSG_UpdateInvalid" => ($GLOBALS['ErrKEY']) ? $this->_msg["UpdateInvalid"] : '',
			"MSG_UpdateButton" 	=> ($GLOBALS['ErrKEY']) ? 'disabled' : '',
		));

		$mdls = $this->checkUpdater ();

		if(file_exists(RP.'updates2.php')){require_once(RP.'updates2.php');}
			 else {echo "error_msg('Файл проверки обновлений отсутствует');";}
		if(class_exists('Updates')){
			$u = new Updates;
			$u->PARAMS = $CONFIG;
			$vers = $u->getLocalVersion();
		}
		  else {echo "error_msg('Ошибка файла проверки обновлений');";}
		if(is_array($vers)){
			$BUCKUPS = $this->getBackups();
			foreach($mdls as $k => $v){
				$tmp = array (
					'name' => $v['name'],
					'title'=> $v['title'],
				);

				if(isset($vers[$k])){
					$tmp['version'] = ($vers[$k]['Vers']) ? $vers[$k]['Vers'] : null;
					$tmp['Date'] = ($vers[$k]['Date']) ? date('d.m.Y', $vers[$k]['Date']): null;
				}
				if($tmp['version'] && $tmp['Date']){
					$tmp['Current'] = sprintf('%s / %s', $tmp['version'] , $tmp['Date']);
				}
				 elseif(!$tmp['version'] && !$tmp['Date']){
				 	$tmp['Current'] = '???';
				}
				 else {
					$tmp['Current'] = ($tmp['version']) ? $tmp['version'] : $tmp['Date'];
				}
				if(isset($BUCKUPS[$k])){
					$tmp['Backup'] = date('d.m.Y H:i', $BUCKUPS[$k]);
				}
				$mdl[] = $tmp; unset($tmp);
			}
		}

		if(is_array($mdl)){
			$tpl->assign_array('block_module', array_values($mdl));
		}
	}

	function ADM_backup()
	{   global $main,  $tpl, $tpl_path, $CONFIG, $permissions, $checkKey, $request_script, $request_mod;

		if (!$permissions["e"]) $main->message_access_denied($sitelinks->module_name, $action);
		if($request_script && isset($_SESSION['UPDATES'])){
			if(file_exists(RP.'updates2.php')) {
			    require_once(RP.'updates2.php');
			} else {
			    echo "error_msg('Файл востановления модулей отсутствует');";
			}
			if(class_exists('Updates')) {
			    $u = new Updates;
			    $u->BackupRestore();
			} else {
			    echo "error_msg('Ошибка файла востановления модулей');";
			}
			exit;
		}
        $tpl = new AboTemplate($this->tpl_path.$this->module_name.'_check_popup.html');
		$tpl->prepare();
		$tpl->newBlock('ALL');
		$tpl->assign ('title','Восстановление резервной копии:');

		$_SESSION['UPDATES'] = array (
				'db_host' 		=> $CONFIG['db_host'],
				'db_database' 	=> $CONFIG['db_database'],
				'db_user' 		=> $CONFIG['db_user'],
				'db_password' 	=> $CONFIG['db_password'],
				'L_KEY' 		=> $CONFIG['L_KEY'],
				'logged_error'	=> $CONFIG['error_report'],
				'mod'			=> $request_mod,
		);
	}

	function ADM_check_updates()
	{   global $main,  $tpl, $tpl_path, $CONFIG, $permissions, $checkKey, $request_script, $db;

		if (!$permissions["e"]) $main->message_access_denied($sitelinks->module_name, $action);
		if($request_script && isset($_SESSION['UPDATES'])){
			if (file_exists(RP.'updates2.php')) {
			    require_once(RP.'updates2.php');
			} else {
			    echo "error_msg('Файл проверки обновлений отсутствует');";
			}
			if (class_exists('Updates')) {
			    $u = new Updates;
			    $u->checkUpdate();
			} else {
			    echo "error_msg('Ошибка файла проверки обновлений');";
			}
			exit;
		}

		$host = $_SERVER['HTTP_HOST'];
		if(strpos($host,':')!==false) {
		    list($host,)=explode(':',$host);
		}
		$_h = explode('.', $host);
		$_sub = array ('www', 'abocms', 'site');
		foreach ($_h as $_k => $_v) {
		    if(in_array($_v, $_sub) && sizeof($_h)>2) {
		        unset($_h[$_k]);
		    } else {
		        break;
		    }
		}
		$host = implode('.', $_h);
		$db->query(
			"select s.id, s.title, s.LKEY as l_key from sites s left join sites_alias sa ON (s.id=sa.site_id) ".
			" where s.title='{$host}' OR sa.alias='{$host}' LIMIT 1 "
		);
		$db->next_record();
		$date = $main->getDateUpdate('domain',
			array ('host' 	=> $db->Record["title"],
				   'id'		=> $db->Record["id"],
				   'l_key'	=> $db->Record["l_key"],
			)
		);
		$tpl = new AboTemplate($this->tpl_path.$this->module_name.'_check_popup.html');
		$tpl->prepare();
		$dates = explode('-', $date);
		if(time() <= strtotime($date)) {
      		$tpl->newBlock('ALL');
      		$tpl->assign ('title', 'Проверка обновлений:');
      		$URI = parse_url($CONFIG['server_updates']);
      		if(!$URI['host']) {
      		    $tpl->assign(array('error'=>'Сервер обновлений неопределен'));
      		} else {
      			$_SESSION['UPDATES'] = array (
      		 		'server_updates'	 => $URI['host'],
      				'db_host' 		=> $CONFIG['db_host'],
      				'db_database' 	=> $CONFIG['db_database'],
      				'db_user' 		=> $CONFIG['db_user'],
      				'db_password' 	=> $CONFIG['db_password'],
      				'L_KEY' 		=> $CONFIG['L_KEY'],
      				'logged_error'	=> $CONFIG['error_report'],
      	   		);
      		}
		} else {
		  $tpl->newBlock('ERROR_UPDATE');
		}
	}
	function ADM_get_updates(){
		global $main,  $tpl, $tpl_path, $CONFIG, $permissions, $checkKey, $request_script, $request_mod;
		if (!$permissions["e"]) $main->message_access_denied($sitelinks->module_name, $action);
		if ($request_script && isset($_SESSION['UPDATES'])) {
			if (file_exists(RP.'updates2.php')) {
			    require_once(RP.'updates2.php');
			} else {
			    echo "error_msg('Файл получения обновлений отсутствует');";
			}
			if (class_exists('Updates')) {
			    $u = new Updates;
			    $u->getUpdate();
			} else {
			    echo "error_msg('Ошибка файла получения обновлений');";
			}
			exit;
		}
        $tpl = new AboTemplate($this->tpl_path.$this->module_name.'_check_popup.html');
		$tpl->prepare();
		$tpl->newBlock('ALL');
		$tpl->assign ('title', 'Получение обновлений:');

        $URI = parse_url($CONFIG['server_updates']);
		if(!$URI['host']){$tpl->assign(array('error'=>'Сервер обновлений неопределен'));}
		 else {
			$_SESSION['UPDATES'] = array (
		 		'server_updates'	 => $URI['host'],
				'db_host' 		=> $CONFIG['db_host'],
				'db_database' 	=> $CONFIG['db_database'],
				'db_user' 		=> $CONFIG['db_user'],
				'db_password' 	=> $CONFIG['db_password'],
				'L_KEY' 		=> $CONFIG['L_KEY'],
				'logged_error'	=> $CONFIG['error_report'],
				'modules'		=> $request_mod,
	   		);
		}

	}
	function getBackups(){
		$dir = getcwd();
		@chdir(RP.'admin/backup/sysbackup');
		$f_lst = glob("*_*-*.tgz");
		if (! empty($f_lst)) {
		    foreach ($f_lst as $filename) {
    			preg_match('~(\w+)_(\d{1,2})(\d{1,2})(\d{2,4})-(\d{2})(\d{2})\.tgz~i', $filename, $m);
    			$date = mktime($m[5],$m[6], 0, $m[3], $m[2], $m[4]);
    			if (!isset($RET[($m[1])]) || $RET[($m[1])]< $date ) {
    			    $RET[($m[1])]= $date;
    			}
    		}
		}

		@chdir($dir);
		return isset($RET) && is_array($RET) ? $RET : false;
	}
	function setLang(){
		global $CONFIG, $lang;
		$is_admin = preg_match('/admin\.php/', $_SERVER["SCRIPT_NAME"]);
		if ($is_admin) $filter = $CONFIG["admin_lang"]; else $filter = $lang;
		include(RP.'mod/'.$this->module_name.'/lang/'.$this->module_name.'_'.$filter.'.php');
		$this->_msg = $_MSG;
	}
	function setAction($action=null){
		$is_admin = preg_match('/admin\.php/', $_SERVER["SCRIPT_NAME"]);
		if($is_admin){$ret = method_exists($this, 'ADM_'.$action) ? 'ADM_'.$action :$this->admin_default_action;}
		 else {$ret = method_exists($this, 'USR_'.$action) ? 'USR_'.$action : $this->default_action;}
		if($ret) return $ret;
	}
//старые функции перенесены один в один
	function getSites($start = NULL) {
		GLOBAL $CONFIG, $db, $baseurl;
		$start	= (int)$start;
		if ($start) {
			$arr = $db->getArrayOfResult('SELECT *
											FROM sites
											ORDER BY '.$CONFIG['configuration_admin_order_by'].'
											LIMIT '.($start - 1) * $CONFIG['configuration_max_rows'].', '.$CONFIG['configuration_max_rows']);
			for ($i = 0; $i < sizeof($arr); $i++) {
				$arr[$i]['edit_link']		= $baseurl.'&action=edit&id='.$arr[$i]['id'];
				$arr[$i]['del_link']		= $baseurl.'&action=delete&id='.$arr[$i]['id'];
				$arr[$i]['lang_link']		= $baseurl.'&action=addlang&id='.$arr[$i]['id'];
				$arr[$i]['langs_link']		= $baseurl.'&action=langs&id='.$arr[$i]['id'];
				$arr[$i]['mod_link']		= $baseurl.'&action=mod&id='.$arr[$i]['id'];
				$arr[$i]['MSG_Site_ID']		= $this->_msg['Site_ID'];
				$arr[$i]['MSG_is']			= $this->_msg['is'];
				$arr[$i]['MSG_Edit_site']	= $this->_msg['Edit_site'];
				$arr[$i]['MSG_Edit_modules_list'] = $this->_msg['Edit_modules_list'];
				$arr[$i]['MSG_Add_language']= $this->_msg['Add_language'];
				$arr[$i]['MSG_Confirm_delete_filled_site'] = $this->_msg['Confirm_delete_filled_site'];
			}
			return $arr;
		}
		return FALSE;
	}

	function getSite($id) {
		GLOBAL $CONFIG, $db, $baseurl;
		$id	= (int)$id;
		if ($id) {
			$arr = $db->getArrayOfResult('SELECT * FROM sites WHERE id = '.$id);
			if (is_array($arr)) {
			    $arr[0]['multilanguage_checked'] = $arr[0]['multilanguage'] ? 'checked' : '';
				$aliases = Configuration::getSiteAliases($id);
				if (count($aliases)) {
					$arr[0]['aliases'] = implode("\n", $aliases);
				}
			}
			return $arr;
		}
		return FALSE;
	}

	/**
	 * Добавить сайт
	 *
	 * @param string  $title   Домен
	 * @param string  $alias   Префикс
	 * @param string  $descr   Описание
	 * @param array   $aliases Псевдонимы
	 * @param string  $sitename    Название сайта
	 * @param string  $sitename_rus    Название сайта на русском языке (используется для вставки в письма)
	 * @param boolean $multilanguage   Многоязыковая поддержка
	 * @param string  $contact_email   Контактный emai
	 * @param string  $return_email    Обратный email
	 * @return int
	 */
	function addSite($title, $alias, $descr, $aliases, $sitename, $sitename_rus, $multilanguage, $contact_email, $return_email)
	{   GLOBAL $CONFIG, $db;

		$title            = Main::parseSiteName($db->escape($title));
		$alias            = Main::parseSiteName($db->escape($alias));
		$descr            = $db->escape($descr);
		$sitename         = $db->escape($sitename);
		$sitename_rus     = $db->escape($sitename_rus);
		$multilanguage    = $multilanguage ? 1 : 0;
		$contact_email    = $db->escape($contact_email);
		$return_email     = $db->escape($return_email);

		if ('' == $title || '' == $alias || '' == $sitename) {
		    return -1; /* Пустые обязательные свойства */
		}

		$all_domains = Configuration::getAllDomains();
		if (in_array($title, $all_domains)) {
			return -2; /* Уже есть такое доменное имя или псевдоним */
		}

		$db->query('SELECT id FROM sites WHERE alias = "'.$alias.'"');
		if ($db->nf()) {
			return -3; /* Префикс зарезервирован */
		}

	    $query = "INSERT INTO sites (";
	    $query .= "title, alias, descr, sitename, sitename_rus,";
	    $query .= " multilanguage, contact_email, return_email)";
		$query .= " VALUES (";
		$query .= "'".$title."', '".$alias."', '".$descr."', '".$sitename."', '".$sitename_rus."',";
		$query .= "'".$multilanguage."', '".$contact_email."', '".$return_email."')";
		$db->query($query);
		if ($db->affected_rows() > 0) {
			$this->setAliases($db->lid(), $aliases);
			$this->setSiteModules($db->lid(), array());
			return 1;
		}

		return 0;
	}

	/**
	 * Обновить информацию о сайте
	 *
	 * @param int     $id  ID
	 * @param string  $title   Домен
	 * @param string  $alias   Префикс
	 * @param string  $descr   Описание
	 * @param array   $aliases Псевдонимы
	 * @param string  $sitename    Название сайта
	 * @param string  $sitename_rus    Название сайта на русском языке (используется для вставки в письма)
	 * @param boolean $multilanguage   Многоязыковая поддержка
	 * @param string  $contact_email   Контактный emai
	 * @param string  $return_email    Обратный email
	 * @return unknown
	 */
	function updateSite($id, $title, $alias, $descr, $aliases, $sitename, $sitename_rus, $multilanguage, $contact_email, $return_email)
	{   GLOBAL $CONFIG, $db, $main;

		$id               = (int)$id;
		$title            = Main::parseSiteName($db->escape($title));
		$alias            = Main::parseSiteName($db->escape($alias));
		$descr            = $db->escape($descr);
		$sitename         = $db->escape($sitename);
		$sitename_rus     = $db->escape($sitename_rus);
		$multilanguage    = $multilanguage ? 1 : 0;
		$contact_email    = $db->escape($contact_email);
		$return_email     = $db->escape($return_email);

		if ($id < 1 || '' == $title || '' == $alias || '' == $sitename) {
		    return -1; /* Пустые отбязательные свойства */
		}

		$all_domains = Configuration::getAllDomains($id);
		if (in_array($title, $all_domains)) {
			return -2; /* Уже есть такое доменное имя или псевдоним */
		}

		$db->query('SELECT id FROM sites WHERE alias = "'.$alias.'" AND id!='.$id);
		if ($db->nf()) {
			return -3; /* Префикс зарезервирован */
		}

		$db->query('SELECT * FROM sites WHERE id = '.$id);
		if (!$db->nf()) {
		    return 0;
		}

		$db->next_record();
		/* Если поменялся префикс */
		if ($db->f('alias') != $alias) {
			$domen	= str_replace(array('.', '-'), array('_', '_'), $db->f('alias'));
			$db->query('SELECT * FROM sites_languages WHERE category_id = '.$id);
			if ($db->nf() > 0) {
				for ($i = 0; $i < $db->nf(); $i++) {
					$db->next_record();
					$arr[$i]['site']		= $domen.'_'.$db->f('language');
					$arr[$i]['domen']		= str_replace(array('.', '-'), array('_', '_'), $alias);
					$arr[$i]['new_lang']	= $db->f('language');
				}
               	for ($i = 0; $i < sizeof($arr); $i++) {
               		$tables	= $this->getTablesByPrefix($arr[$i]['site']);
					$this->renameTables($arr[$i], $tables);
					$this->renameDirsAndFiles($arr[$i]);
				}
			}
		}

		$db->query('UPDATE sites
						SET title = "'.$title.'",
							alias = "'.$alias.'",
	 						descr = "'.$descr.'",
	 						sitename = "'.$sitename.'",
	 						sitename_rus = "'.$sitename_rus.'",
	 						multilanguage = "'.$multilanguage.'",
	 						contact_email = "'.$contact_email.'",
	 						return_email = "'.$return_email.'"
						WHERE id = '.$id);
		$ret = $db->affected_rows();
		$this->setAliases($id, $aliases, $id);
		if ($ret > 0) {
			return TRUE;
		}

    	return FALSE;
	}

	function removeSite($id = NULL) {
   		GLOBAL $CONFIG, $db, $db1;
		$id		= (int)$id;
		if ($id) {
			$site	= $this->getSite($id);
			$prefix	= str_replace('.', '_', $site[0]['title']);
			$arr	= $this->getLangs($id);

			for ($i = 0; $i < sizeof($arr); $i++) {
				$arr_prefix[$i] = $prefix.'_'.$arr[$i]['language'];
			}

			$tables = $db->table_names();

			for ($i = 0; $i < sizeof($tables); $i++) {
				for ($j = 0; $j < sizeof($arr_prefix); $j++) {
					$tbl = NULL;
					$tbl = strpos($tables[$i]['table_name'], $arr_prefix[$j]);
					if (is_numeric($tbl)) {
						$arr_tbl[] = $tables[$i]['table_name'];
					}
				}
			}

            if (is_array($arr_tbl) && sizeof($arr_tbl) > 0) {
            	for ($i = 0; $i < sizeof($arr_tbl); $i++) {
            		$db->query('DROP TABLE '.$arr_tbl[$i]);
            	}
            }

            $db->query('SELECT * FROM sites_languages WHERE category_id = '.$id);
			if ($db->nf() > 0) {
				for ($i = 0; $i < $db->nf(); $i++) {
                	$db->next_record();
					$langs[]	= $db->f('id');
				}
				for ($i = 0; $i < sizeof($langs); $i++) {
					$this->removeLang($langs[$i], FALSE);
				}
			}
			$db->query('DELETE FROM core_roles_permissions WHERE role_id IN (SELECT id FROM core_roles WHERE site_id='.$id.')');
			$db->query('DELETE FROM core_roles WHERE site_id='.$id);
			$db1->query('UPDATE core_users SET site_id=0, lang_id=0 WHERE site_id = '.$id);			
			$db->query('DELETE FROM sites WHERE id = '.$id);
			if ($db->affected_rows() > 0) {
				$this->delSiteAliases($id);
				$this->delSiteModules($id);
				return TRUE;
			}
		}
		return FALSE;
	}

	function getTablesByPrefix($prefix = NULL) {
		GLOBAL $db;
		if ($prefix) {
			$tables = $db->table_names();
			for ($i = 0; $i < sizeof($tables); $i++) {
				$tbl = NULL;
				$tbl = strpos($tables[$i]['table_name'], $prefix);
				if (is_numeric($tbl)) {
					$arr_tbl[] = $tables[$i]['table_name'];
				}
			}
			return $arr_tbl;
		}
		return FALSE;
	}

	function getDomenByLangID($lid) {
		GLOBAL $CONFIG, $db;
		$lid = (int)$lid;
		if ($lid) {
			$db->query('SELECT a.id as lang_id, a.language, b.id as site_id, b.alias as title
							FROM sites_languages AS a,
								 sites AS b
							WHERE a.category_id = b.id AND
								  a.id = '.$lid);
			if ($db->nf() > 0) {
				$db->next_record();
				$arr['domen']	= str_replace('.', '_', $db->f('title'));
				$arr['domen']	= str_replace('-', '_', $arr['domen']);
				$arr['site']	= $arr['domen'].'_'.$db->f('language');
				$arr['site_id']	= $db->f('site_id');
				$arr['lang_id']	= $db->f('lang_id');
				$arr['lang']	= $db->f('language');
				return $arr;
			}
		}
		return FALSE;
	}

	function renameTables($arr = NULL, $arr_tbl = NULL) {
		GLOBAL $db;
		if (is_array($arr_tbl) && sizeof($arr_tbl) > 0 && $arr['site'] != '') {
			$string = '';
       		for ($i = 0; $i < sizeof($arr_tbl); $i++) {
       			$new_name = $arr['domen'].'_'.$arr['new_lang'].str_replace($arr['site'], '', $arr_tbl[$i]);
       			$string .= $new_name == $arr_tbl[$i] ? '' : $arr_tbl[$i].' TO '.$new_name.', ';
			}

			$string = substr($string, 0, -2);
			if ($string) {
			    $db->query('RENAME TABLE '.$string);
			    return TRUE;
			}
		}

		return FALSE;
	}

	function renameDirsAndFiles($arr = NULL) {
		if ($this->renameSiteDirs($arr) && $this->renameSiteFiles($arr)) {
			return TRUE;
		}
		return FALSE;
	}

	function createSiteDirs($site) {
		GLOBAL $CONFIG;
		if ('' != $site) {
			for ($i = 0; $i < sizeof($CONFIG['configuration_site_dirs']); $i++) {
				if (!file_exists(RP.$CONFIG['configuration_site_dirs'][$i].$site)) {
					@mkdir(RP.$CONFIG['configuration_site_dirs'][$i].$site, 0755);
				} else {
					@chmod(RP.$CONFIG['configuration_site_dirs'][$i].$site, 0755);
				}
			}
			for ($i = 0; $i < sizeof($CONFIG['configuration_common_dirs']); $i++) {
				if (!file_exists(RP.$CONFIG['configuration_common_dirs'][$i])) {
					@mkdir(RP.$CONFIG['configuration_common_dirs'][$i], 0755);
				} else {
					@chmod(RP.$CONFIG['configuration_common_dirs'][$i], 0755);
				}
			}
			return TRUE;
		}
		return FALSE;
	}

	function createSiteFiles($site) {
		GLOBAL $CONFIG;
		if ('' != $site) {
			for ($i = 0; $i < sizeof($CONFIG['configuration_site_files']); $i++) {
				$file	= RP.str_replace('{site}', $site, $CONFIG['configuration_site_files'][$i]);
				if (!file_exists($file)) {
					fopen($file, 'w');
				}
			}
			return TRUE;
		}
		return FALSE;
	}
	function checkUpdater (){
		GLOBAL $CONFIG,$db;
		if(!$db->Link_ID){return false;}
		$sql = "SHOW COLUMNS FROM modules";
		$res = mysql_query($sql);
		if(!$res){return false;}
		while($tmp = mysql_fetch_assoc($res)){
			$_CHECK[($tmp['Field'])] = $tmp['Type'];
		}
		@mysql_free_result($res);

		if(!is_array($_CHECK) || !is_array($this->_vers)){return false;}
		foreach($this->_vers as $_k => $_v){
			if(!isset($_CHECK[$_k]) || $_CHECK[$_k]!=$_v['val']){$_change_needs = true;$_change_ok = $this->UpdTbl ($_CHECK);break;}
		}
		if(isset($_change_needs) && !$_change_ok){return false;}
		$sql = "select name, title from modules where parent_id>0 order by id ";
		$res = mysql_query($sql);
		if(!$res){return false;}
		$_mdl['_kernel'] = array(
				'name' 		=> '_kernel',
				'title' 	=> 'Ядро системы',
		);
		while($tmp = mysql_fetch_assoc($res)){
			$_mdl[($tmp['name'])] = $tmp;
		}
		@mysql_free_result($res);
		return $_mdl;
	}
	function UpdTbl ($arr){
		GLOBAL $CONFIG,$db;
		if(!$db->Link_ID){return false;}
		$sql = "alter table modules ";
		foreach($this->_vers as $_k => $_v){
			if(!$arr[$_k]){
				$sql_mrg[] = ($_v['create']) ? " ADD `{$_k}` {$_v['create']}"  : " ADD `{$_k}` {$_v['val']}";
			}
			  else if($arr[$_k] != $_v['val']){
			  	$sql_mrg[] = ($_v['create']) ? " MODIFY `{$_k}` {$_v['create']} ": " MODIFY `{$_k}` {$_v['val']} ";
			}
		}
		if(!is_array($sql_mrg)){return false;}

		$sql .= implode(' , ',$sql_mrg);
		$res = mysql_query($sql);
		return $res;
	}
	function renameSiteDirs($arr) {
		GLOBAL $CONFIG;
		if ('' != $arr['site'] && '' != $arr['domen'] && '' != $arr['new_lang'] && $arr['site'] != $arr['domen'].'_'.$arr['new_lang']) {
			$this->removeSiteDirs($arr['domen'].'_'.$arr['new_lang']);
			for ($i = 0, $size = sizeof($CONFIG['configuration_site_dirs']); $i < $size; $i++) {
				if (file_exists(RP.$CONFIG['configuration_site_dirs'][$i].$arr['site'])) {
					rename(RP.$CONFIG['configuration_site_dirs'][$i].$arr['site'], RP.$CONFIG['configuration_site_dirs'][$i].$arr['domen'].'_'.$arr['new_lang']);
				}
			}
			return TRUE;
		}
		return FALSE;
	}

	function renameSiteFiles($arr) {
		GLOBAL $CONFIG;
		if ('' != $arr['site'] && '' != $arr['domen'] && '' != $arr['new_lang'] && $arr['site'] != $arr['domen'].'_'.$arr['new_lang']) {
			for ($i = 0, $files_count = sizeof($CONFIG['configuration_site_files']); $i < $files_count; $i++) {
				$f_1 = RP.str_replace('{site}', $arr['site'], $CONFIG['configuration_site_files'][$i]);
				$f_2 = RP.str_replace('{site}', $arr['domen'].'_'.$arr['new_lang'], $CONFIG['configuration_site_files'][$i]);
				if (file_exists($f_1)) {
					if (file_exists($f_2)) {
						@unlink($f_2);
					}
					@rename($f_1, $f_2);
				}
			}
			return TRUE;
		}
		return FALSE;
	}

	function deleteAllFiles($site) {
		GLOBAL $CONFIG, $main;
		if ('' != $site) {
           	$files	= $main->files_list($site, 'files');
           	if (is_array($files)) {
           		for ($j = 0, $files_count = sizeof($files); $j < $files_count; $j++) {
           			@unlink($site.'/'.$files[$j]);
           		}
           	}
			$dirs	= $main->getDirList($site);
			for ($i = 0, $dirs_count = sizeof($dirs); $i < $dirs_count; $i++) {
				$this->deleteAllFiles($site.'/'.$dirs[$i]);
			}
			return TRUE;
		}
		return FALSE;
	}

	function deleteAllSubDirs($site, $ret_dirs = NULL) {
		GLOBAL $CONFIG, $main;
		if ('' != $site) {
			$dirs	= $main->getDirList($site);
           	if (is_array($dirs)) {
           		for ($j = 0, $dirs_count = sizeof($dirs); $j < $dirs_count; $j++) {
					$ret_dirs[]	= $site.'/'.$dirs[$j];
					$ret_dirs	= $this->deleteAllSubDirs($site.'/'.$dirs[$j], $ret_dirs);
           		}
           	}
			return $ret_dirs;
		}
		return FALSE;
	}

	function removeSiteDirs($site) {
		GLOBAL $CONFIG;
		if ('' != $site) {
			for ($i = 0, $dirs_count = sizeof($CONFIG['configuration_site_dirs']); $i < $dirs_count; $i++) {
				if (file_exists(RP.$CONFIG['configuration_site_dirs'][$i].$site)) {
					$this->deleteAllFiles(RP.$CONFIG['configuration_site_dirs'][$i].$site);
					$dirs = $this->deleteAllSubDirs(RP.$CONFIG['configuration_site_dirs'][$i].$site);
					if (is_array($dirs)) {
						for ($j = sizeof($dirs); $j >= 0; $j--) {
							if (file_exists($dirs[$j])) {
                        		@rmdir($dirs[$j]);
							}
						}
					}
					@rmdir(RP.$CONFIG['configuration_site_dirs'][$i].$site);
				}
			}
			return TRUE;
		}
		return FALSE;
	}

	function removeSiteFiles($site) {
		GLOBAL $CONFIG;
		if ('' != $site) {
			for ($i = 0; $i < sizeof($CONFIG['configuration_site_files']); $i++) {
				$file	= RP.str_replace('{site}', $site, $CONFIG['configuration_site_files'][$i]);
				if (file_exists($file)) {
					@unlink($file);
				}
			}
			return TRUE;
		}
		return FALSE;
	}

	function changeSite($lid, $type = NULL, $new_lang = NULL) {
		GLOBAL $CONFIG, $db, $main;
		$lid		= (int)$lid;
		$new_lang	= $db->escape($new_lang);
		if ($lid) {
			$arr				= $this->getDomenByLangID($lid);
			$arr['new_lang']	= $new_lang;
			switch ($type) {
				case 'add':
					$tables[] = $main->getArrayOfTables($arr['site'], '');
					for ($i = 0; $i < sizeof($tables); $i++) {
						$main->createTables($tables[$i]);
					}
					$this->createSiteDirs($arr['site']);
					$this->createSiteFiles($arr['site']);
					$this->createSiteAdmin($arr['site'], $arr['site_id'], $lid);
					break;

				case 'update':
					$arr_tbl = $this->getTablesByPrefix($arr['site']);
					$this->renameTables($arr, $arr_tbl);
					$this->renameDirsAndFiles($arr);
					break;

				case 'remove':
					$this->removeSiteDirs($arr['site']);
					$this->removeSiteFiles($arr['site']);
					$arr_tbl = $this->getTablesByPrefix($arr['site']);
		            if (is_array($arr_tbl) && sizeof($arr_tbl) > 0) {
       					for ($i = 0; $i < sizeof($arr_tbl); $i++) {
       						$db->query('DROP TABLE '.$arr_tbl[$i]);
       					}
       				}
					break;
			}
		}
		return FALSE;
	}

	function parseLangs($id = NULL, $lid = NULL, $arr = NULL) {
		GLOBAL $db;
		$id		= (int)$id;
		$lid	= (int)$lid;
		if (is_array($arr) && sizeof($arr) > 0 && $id) {
			if ($lid) {
				$db->query('SELECT language
								FROM sites_languages
								WHERE category_id	 = '.$id.' AND
									  id			!= '.$lid);
			} else {
   				$db->query('SELECT language
								FROM sites_languages
								WHERE category_id	 = '.$id);
			}
			if ($db->nf() > 0) {
				for ($i = 0; $i < $db->nf(); $i++) {
					$db->next_record();
					$langs[] = $db->f('language');
				}

				for ($i = 0; $i < sizeof($langs); $i++) {
					for ($j = 0; $j < sizeof($arr); $j++) {
						if ($arr[$j]['value'] == $langs[$i]) {
							unset($arr[$j]);
						}
					}
				}
            	foreach($arr as $k => $v) {
					if ($v) {
						$prp[] = $v;
					}
				}
            	return $prp;
			}
			return $arr;
		}
		return $arr;
	}

	function getLangs($id)
	{   GLOBAL $CONFIG, $db, $baseurl;

		$id	= (int)$id;
		if ($id) {
			$arr = $db->getArrayOfResult('SELECT * FROM sites_languages WHERE category_id = '.$id);
			for ($i = 0; is_array($arr) && $i < sizeof($arr); $i++) {
				$arr[$i]['edit_link']			= $baseurl.'&action=editlang&id='.$id.'&lid='.$arr[$i]['id'];
				$arr[$i]['del_link']			= $baseurl.'&action=dellang&id='.$id.'&lid='.$arr[$i]['id'];
				$arr[$i]['demo_fill_link']		= $arr[$i]['is_fill'] ? $baseurl.'&action=democlear&id='.$id.'&lid='.$arr[$i]['id'] : $baseurl.'&action=demofill&id='.$id.'&lid='.$arr[$i]['id'];
				$arr[$i]['demo_fill_icon']		= $arr[$i]['is_fill'] ? 'ico_swof.gif' : 'ico_swon.gif';
				$arr[$i]['demo_fill_message']	= $arr[$i]['is_fill'] ? 'удалить содержимое сайта' : 'наполнить демонстрационным содержимым';

				$arr[$i]['MSG_Delete_site']	= $this->_msg['Delete_site'];
                $arr[$i]['MSG_Download_f'] 	= $this->_msg["Download_f"];
				$arr[$i]['MSG_Download_b'] 	= $this->_msg["Download_b"];
				$arr[$i]['link_download_f']	= $baseurl.'&action=loaded&type=tmpl&id='.$id.'&lid='.$arr[$i]['id'];
				$arr[$i]['link_download_b']	= $baseurl.'&action=loaded&type=base&id='.$id.'&lid='.$arr[$i]['id'];



				for ($j = 0; $j < sizeof($CONFIG['site_langs']); $j++) {
					if ($CONFIG['site_langs'][$j]['value'] == $arr[$i]['language']) {
						$arr[$i]['name_language'] = $CONFIG['site_langs'][$j]['name'];
						break;
					}
				}
			}
			return $arr;
		}
		return FALSE;
	}

	function getLang($lid) {
		GLOBAL $CONFIG, $db, $baseurl;
		$lid	= (int)$lid;
		if ($lid) {
			$arr = $db->getArrayOfResult('SELECT * FROM sites_languages WHERE id = '.$lid);
			for ($i = 0; $i < sizeof($arr); $i++) {
				$arr[$i]['lid']	= $arr[$i]['id'];
				$arr[$i]['id']	= $arr[$i]['category_id'];
				$arr[$i]['lang']	= $arr[$i]['language'];
			}
			return $arr;
		}
		return FALSE;
	}

	function addLang($categ_id, $site_lang, $descr) {
		GLOBAL $CONFIG, $db;
		for($i=0;$i<count($this->mod);$i++) {
      		$file_tmp = 'mod/' . $this->mod[$i] . '/lang/' . $this->mod[$i] . '_rus.php';
      		$file_new = 'mod/' . $this->mod[$i] . '/lang/' . $this->mod[$i] . '_' . $site_lang . '.php';
      		if (!file_exists(RP.$file_new)) {
      		    copy($file_tmp, $file_new);
      		}
		}
		$categ_id	= (int)$categ_id;
		$site_lang	= $db->escape($site_lang);
		$descr		= $db->escape($descr);
		if ($categ_id && $site_lang) {
		    $langs = $this->getLangs($categ_id);
		    $is_def = is_array($langs) && count($langs) ? 0 : 1;

			$db->query('INSERT INTO sites_languages (category_id, title, language, is_default)
							VALUES ('.$categ_id.', "'.$descr.'", "'.$site_lang.'", '.$is_def.')');
			if ($db->affected_rows() > 0) {
			    $lang_id = $db->lid();
				$this->changeSite($lang_id, 'add');

				return $lang_id;
			}
		}
		return FALSE;
	}

	function updateLang($id, $site_lang, $descr) {
		GLOBAL $CONFIG, $db;
		$id			= (int)$id;
		$site_lang	= $db->escape($site_lang);
		$descr		= $db->escape($descr);
		if ($id && $site_lang) {
			$this->changeSite($id, 'update', $site_lang);
			$db->query('UPDATE sites_languages
							SET	title		= "'.$descr.'",
								language	= "'.$site_lang.'"
							WHERE id = '.$id);
			if ($db->affected_rows() > 0) {
				return $db->lid();
			}
		}
		return FALSE;
	}

	function getSelectTag($lid = NULL) {
		GLOBAL $CONFIG, $db, $main;
		$lid = (int)$lid;
		if ($lid) {
			$db->query('SELECT language
							FROM sites_languages
							WHERE id = '.$lid);
			if ($db->nf() > 0) {
				$db->next_record();
				$lang = $db->f('language');
				for ($i = 0; $i < sizeof($CONFIG['site_langs']); $i++) {
					if ($lang == $CONFIG['site_langs'][$i]['value']) {
						$arr = $main->getSelectTag($CONFIG['site_langs'], $i);
						return $arr;
					}
				}
			}
		}
		return FALSE;
	}

	function fillByDemoContent($lid) {
		GLOBAL $CONFIG, $db;
		$lid = (int)$lid;
		if ($lid) {
			$db->query('UPDATE sites_languages
							SET is_fill = 1
							WHERE id = '.$lid);
			if ($db->affected_rows() > 0) {
			    $arr = $this->insertDemoContent($lid);
			    $this->copyDefaultTemplates($arr['site']);
			}
			return TRUE;
		}
		return FALSE;
	}

	function clearByDemoContent($lid) {
		GLOBAL $CONFIG, $db;
		$lid = (int)$lid;
		if ($lid) {
			$db->query('UPDATE sites_languages
							SET is_fill = 0
							WHERE id = '.$lid);
			if ($db->affected_rows() > 0) {
			    $arr = $this->clearDemoContent($lid);
            	$this->delDefaultTemplates($arr['site']);
			}
			return TRUE;
		}
		return FALSE;
	}

	function insertDemoContent($lid = NULL) {
		GLOBAL $CONFIG, $db, $main;
		$lid = (int)$lid;
		if ($lid) {
        	$arr	= $this->getDomenByLangID($lid);
			$tables	= $this->getTablesByPrefix($arr['site']);
			for ($i = 0; $i < sizeof($tables); $i++) {
				if (preg_match('/^'.$arr['site'].'_counter_[a\d]/', $tables[$i]))
					$db->query('DROP TABLE '.$tables[$i]);
				else
					$db->query('TRUNCATE TABLE '.$tables[$i]);
			}
			//$db->query('SET NAMES utf8');
			$main->makeDemoSite($arr['site'], $arr['site_id'], $arr['lang_id']);
			return $arr;
		}
		return FALSE;
	}

	function copyDefaultFiles($site, $dir = NULL) {
		if (file_exists($dir.'.default') && $site != '') {
			$arr = Main::files_list($dir.'.default', 'all');
			for ($h = 0, $k = sizeof($arr); $h < $k; $h++) {
				if (is_dir($dir.'.default/'.$arr[$h])) {
					if (!file_exists($dir.$site.'/'.$arr[$h])) {
						@mkdir($dir.$site.'/'.$arr[$h], 0755);
					}
					$arr_2	= Main::files_list($dir.'.default'.'/'.$arr[$h], 'files');
                	for ($i = 0, $j = sizeof($arr_2); $i < $j; $i++) {
                    	if ('.' != substr($arr_2[$i], 0, 1)) {
                    		@copy($dir.'.default'.'/'.$arr[$h].'/'.$arr_2[$i], $dir.$site.'/'.$arr[$h].'/'.$arr_2[$i]);
						}
					}
				} else if ('.' != substr($arr[$h], 0, 1)) {
					@copy($dir.'.default/'.$arr[$h], $dir.$site.'/'.$arr[$h]);
				}
			}
			return TRUE;
		}
		return FALSE;
	}

	function copyDefaultTemplates($site) {
		GLOBAL $CONFIG;
		if ('' != $site) {
	    	$modules = Main::getDirList(RP.'mod', 'directories');
	    	if (is_array($modules) && sizeof($modules)) {
	    		for ($k = 0, $h = sizeof($modules); $k < $h; $k++) {
	    			$files	= Main::files_list(RP.'mod/'.$modules[$k].'/.default/tpl', 'files');
					if (is_array($files) && sizeof($files)) {
						for ($i = 0, $j = sizeof($files); $i < $j; $i++) {
            				@copy(RP.'mod/'.$modules[$k].'/.default/tpl/'.$files[$i], RP.'tpl/'.$site.'/'.$files[$i]);
						}
					}
	    		}
	    	}

			for ($i = 0, $j = sizeof($CONFIG['configuration_site_dirs']); $i < $j; $i++) {
            	Configuration::copyDefaultFiles($site, RP.$CONFIG['configuration_site_dirs'][$i]);
			}

			@unlink(RP.'admin/files/'.$site.'_internal_links.txt');
			@copy(RP.'admin/files/.default_internal_links.txt', RP.'admin/files/'.$site.'_internal_links.txt');
			return TRUE;
		}
		return FALSE;
	}

	function clearDemoContent($lid = NULL) {
		GLOBAL $CONFIG, $db;
		$lid = (int)$lid;
		if ($lid) {
        	$arr	= $this->getDomenByLangID($lid);
			$tables	= $this->getTablesByPrefix($arr['site']);
			for ($i = 0; $i < sizeof($tables); $i++) {
				if (preg_match('/^'.$arr['site'].'_counter_[a\d]/', $tables[$i]))
					$db->query('DROP TABLE '.$tables[$i]);
				else
					$db->query('TRUNCATE TABLE '.$tables[$i]);
			}
			return $arr;
		}
		return FALSE;
	}


	function delDefaultFiles($dir = NULL) {
		if (file_exists($dir) && $dir != '') {
			$arr = Main::files_list($dir, 'all');
			for ($h = 0, $k = sizeof($arr); $h < $k; $h++) {
				if (is_dir($dir.'/'.$arr[$h])) {
					Configuration::delDefaultFiles($dir.'/'.$arr[$h]);
					if (file_exists($dir.'/'.$arr[$h])) {
						@rmdir($dir.'/'.$arr[$h]);
					}
				} else {
					@unlink($dir.'/'.$arr[$h]);
				}
			}
			return TRUE;
		}
		return FALSE;
	}


	function delDefaultTemplates($site) {
		GLOBAL $CONFIG, $main;
		if ('' != $site) {
			for ($i = 0, $j = sizeof($CONFIG['configuration_site_dirs']); $i < $j; $i++) {
            	Configuration::delDefaultFiles(RP.$CONFIG['configuration_site_dirs'][$i].$site);
			}
			fopen(RP.'admin/files/'.$site.'_internal_links.txt', 'w');
			return TRUE;
		}
		return FALSE;
	}

	function removeLang($lid = NULL, $delete = TRUE) {
   		GLOBAL $CONFIG, $db;
		$lid	= (int)$lid;
		if ($lid) {
			$this->clearByDemoContent($lid);
			$this->changeSite($lid, 'remove');
			if ($delete) {
			  $lang = $this->getLang($lid);
				$db->query('DELETE FROM core_roles_permissions WHERE role_id IN (
							SELECT id FROM core_roles WHERE site_id='.$lang[0]['id'].' AND lang_id='.$lid.')');
				$db->query('DELETE FROM core_roles WHERE site_id='.$lang[0]['id'].' AND lang_id='.$lid);
				$db->query('DELETE FROM sites_languages WHERE id = '.$lid);
				$ret = $db->affected_rows();
				$langs = $this->getLangs($lang[0]['id']);
				if (is_array($langs)) {
					if (1 == count($langs)) {
					  $this->setDefaultLang($lang[0]['id'], $langs[0]['id']);
					}
				}

				return $ret;
			}
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Установить псевдонимы сайта
	 *
	 * @param int $site_id
	 * @param mixed $aliases - Псевдомин либо массив псевдонимов
	 * @return mixed
	 */
	function setAliases($site_id, $aliases, $ignor_alias_site_id = 0)
	{   GLOBAL $db;

		$site_id = (int) $site_id;
		$site = $this->getSite($site_id);
		if (!isset($site[0]['id'])) {
			return FALSE;
		}

		$all_domains = Configuration::getAllDomains(0, $ignor_alias_site_id);

		$ins = '';
		$insrt_alias = array();
		if (!is_array($aliases)) {
			$alias = Main::parseSiteName(trim(strtolower($aliases)));
			if (!$alias) {
				return FALSE;
			}
			$alias = My_Sql::escape($alias);
			$ins = in_array($alias, $all_domains) ? "" : "(".$site_id.", '".$alias."'), ";
		} else {
			foreach ($aliases as $k=>$alias) {
				$alias = Main::parseSiteName(trim(strtolower($alias)));
				if ($alias && !in_array($alias, $insrt_alias) && !in_array($alias, $all_domains)) {
					$insrt_alias[] = $alias;
					$alias = My_Sql::escape($alias);
					$ins .= "(".$site_id.", '".$alias."'), ";
				}
			}
		}

		$ins = substr($ins, 0, -2);
		if (!$ins) {
			return FALSE;
		}

		$this->delSiteAliases($site_id);
		$db->query("INSERT INTO sites_alias (site_id, alias) VALUES ".$ins);

		return $db->affected_rows();
	}

	/**
	 * Удалить  псевдонимы сайта
	 *
	 * @param int $site_id
	 * @return mixed
	 */
	function delSiteAliases($site_id)
	{   GLOBAL $db;

		if (!$site_id = (int) $site_id) {
			return FALSE;
		}

		$db->query("DELETE FROM sites_alias WHERE site_id = ".$site_id);

		return $db->affected_rows();
	}

	/**
	 * Получить псевдонимы сайта
	 *
	 * @param int $site_id
	 * @return mixed
	 */
	function getSiteAliases($site_id)
	{   GLOBAL $db;

		$site_id = (int) $site_id;
		if ($site_id < 1) {
			return FALSE;
		}

		$ret = array();

		$db->query("SELECT alias FROM sites_alias WHERE site_id = ".$site_id);
		while ($db->next_record()) {
			$ret[] = $db->f('alias');
		}

		return $ret;
	}

	function setDefaultLang($site_id, $lang_id)
	{   GLOBAL $db;

	    $site_id = (int) $site_id;
	    $lang_id = (int) $lang_id;
		if ($site_id < 1 || $lang_id < 1
		   || !$lang = $this->getLang($lang_id) || $site_id != $lang[0]['id']
		) {
			return FALSE;
		}
		$query = "UPDATE sites_languages SET is_default=0 WHERE category_id=".$site_id;
		$db->query($query);
		$query = "UPDATE sites_languages SET is_default=1 WHERE id=".$lang_id;
		$db->query($query);

		return TRUE;
	}

	/**
	 * Получить все доменные имена и псевдонимы зарегистрированные в системе.
	 * Если указан $ignor_site_id - кроме домена с id = $ignor_site_id
	 * Если указан $ignor_alias_site_id - кроме псевдонимов домена с id = $ignor_alias_site_id
	 *
	 * @param int $ignor_site_id
	 * @param int $ignor_alias_site_id
	 * @return array
	 */
	function getAllDomains($ignor_site_id = 0, $ignor_alias_site_id = 0)
	{   GLOBAL $db;

	    $ignor_site_id = (int)$ignor_site_id;
	    $ignor_alias_site_id = (int)$ignor_alias_site_id;
	    $ret = array();

	    $where = $ignor_site_id ? " WHERE id != " . $ignor_site_id : "";
	    $query = "SELECT title FROM sites".$where;
	    $db->query($query);
	    while ($db->next_record()) {
	        $ret[] = $db->f('title');
	    }

	    $where = $ignor_alias_site_id ? " WHERE site_id != " . $ignor_alias_site_id : "";
	    $query = "SELECT alias FROM sites_alias".$where;
	    $db->query($query);
	    while ($db->next_record()) {
	        $ret[] = $db->f('alias');
	    }

	    return $ret;
	}

	/**
	 * Получит список всех зарегистрированных модулей в журнале
	 *
	 * @return array
	 */
	function getLogModules()
	{   GLOBAL $db;

	    $ret = array();
	    $query = "SELECT DISTINCT `mod`, title FROM ".$this->table_prefix."_configuration_log";
	    $query .= " LEFT JOIN modules ON `mod`=`name` ORDER BY title";
	    $db->query($query);
	    while ($db->next_record()) {
	        $mod = $db->f('mod');
	        $title =  $db->f('title');
	        $ret[$mod] = $title ? $title : $mod;
	    }

	    return $ret;
	}



	/**
	 * Получит список всех действий зарегистрированных журнале
	 *
	 * @return array
	 */
	function getLogActions()
	{   GLOBAL $db;

	    $ret = array();
	    $query = "SELECT DISTINCT action FROM ".$this->table_prefix."_configuration_log ORDER BY action";
	    $db->query($query);
	    while ($db->next_record()) {
	        $action = $db->f('action');
	        $action = $action ? $action : "n\a";
	        $ret[$action] = $this->getActionTitle($action);
	    }

	    return $ret;
	}

	/**
	 * Попытка распознать название действия
	 *
	 * @return array
	 */
	function getActionTitle($action)
	{
		$title = $action ? $action : "n\a";
		$title .= FALSE !== strpos($title, 'edit') ? ' - ' . $this->_msg["Edit"] : '';
		$title .= FALSE !== strpos($title, 'update') ? ' - ' . $this->_msg["Update"] : '';
		$title .= FALSE !== strpos($title, 'add') ? ' - ' . $this->_msg["Add"] : '';
		$title .= FALSE !== strpos($title, 'del') ? ' - ' . $this->_msg["Delete"] : '';
		$title .= FALSE !== strpos($title, 'list') ? ' - ' . $this->_msg["List"] : '';
		$title .= FALSE !== strpos($title, 'prp') ? ' - ' . $this->_msg["Controls"] : '';
		$title .= FALSE !== strpos($title, 'suspend') ? ' - ' . $this->_msg["Suspend"] : '';
		$title .= FALSE !== strpos($title, 'activate') ? ' - ' . $this->_msg["Activate"] : '';
		$title .= FALSE !== strpos($title, 'search') ? ' - ' . $this->_msg["Seek"] : '';

		return $title;
	}

	/**
	 * Получит список всех поьзователей зарегистрированных в журнале
	 *
	 * @return array
	 */
	function getLogUsers()
	{   GLOBAL $db;

	    $ret = array();
	    $query = "SELECT DISTINCT `user_id`, username FROM ".$this->table_prefix."_configuration_log";
	    $query .= " LEFT JOIN core_users AS U ON `user_id`=U.id ORDER BY username";
	    $db->query($query);
	    while ($db->next_record()) {
	        $usename = $db->f('username');
	        $id = $db->f('user_id');
	        $ret[$id] = $usename ? $usename : "ID - ".$id;
	    }

	    return $ret;
	}

	/**
	 * Получить записи из журнала
	 *
	 * @param array $filtr Фильтр - array(mod, action, user, from, to)
	 * @param int $cnt_row
	 * @param int $start
	 * @return array
	 */
	function getLog($filtr, $cnt_row, $start = 1)
	{   GLOBAL $db, $lang;

	    $cnt_row = $cnt_row > 1 ? (int)$cnt_row : $this->default_count_row;
	    $start = $start > 1 ? (int)$start : 1;
	    $ret = array();

	    $where = "1".(@$filtr['mod'] && "n\a" != $filtr['mod'] ? " AND `mod`='".My_Sql::escape($filtr['mod'])."'" : "");
	    $where .= @$filtr['action'] && "n\a" != $filtr['action'] ? " AND action='".My_Sql::escape($filtr['action'])."'" : "";
	    $where .= "n\a" == $filtr['action'] ? " AND action=''" : "";
	    $where .= @$filtr['user'] && "n\a" != $filtr['user'] ? " AND user_id='".My_Sql::escape($filtr['user'])."'" : "";

	    $from = @$filtr['from'] ? strtotime(str_replace(".", "-", $filtr['from'])) : -1;
        $to = @$filtr['to'] ? strtotime(str_replace(".", "-", $filtr['to'])) : -1;
        $to = -1 == $to ? $to : $to + 60*60*24;
        if (-1 != $from && -1 != $to) {
            if ($from > $to) {
                $t = $from;
                $from = $to;
                $to = $t;
            }
        	$where .= " AND utime BETWEEN ".$from." AND ".$to;
        } elseif (-1 != $from) {
        	$where .= " AND utime >= ".$from;
        } elseif (-1 != $to) {
        	$where .= " AND utime <= ".$to;
        }
	    $where = "1" !== $where ? $where : "utime > ".strtotime(date("Y-m-d"));

	    $query = "SELECT COUNT(*) AS amount FROM ".$this->table_prefix."_configuration_log WHERE ".$where;
	    //echo $query;
	    $db->query($query);
	    $db->next_record();
	    $amount = $db->f('amount');
	    if (0 == $amount) {
	    	return array();
	    }
	    $ret['amount'] = $amount;

	    $query = "SELECT `mod`, title, action, item_id, user_id, username, IP,";
	    $query .= " request, FROM_UNIXTIME(utime, '%d-%m-%Y %H:%i:%s') AS utime";
	    $query .= " FROM ".$this->table_prefix."_configuration_log";
	    $query .= " LEFT JOIN modules ON `mod`=`name`";
	    $query .= " LEFT JOIN core_users AS U ON `user_id`=U.id";
	    $query .= " WHERE ".$where;
	    $query .= " ORDER BY utime DESC";
	    $query .= " LIMIT ".($start-1)*$cnt_row.",".$cnt_row;
	    //echo $query;
	    $db->query($query);
	    while ($db->next_record()) {
	        $mod = $db->f('mod');
	        $title =  $db->f('title');
	        $usename = $db->f('username');
	        $id = $db->f('user_id');
	        $action = $this->getActionTitle($db->f('action'));
	        $ret[] = array('user' => $usename ? $usename : "ID - ".$id,
	                       'user_id' => $id,
	                       'module' => $title ? $title : $mod,
	                       'mod_uname' => $mod,
	                       'action' => $action,
	                       'item_id' => $db->f('item_id'),
	                       'time' => $db->f('utime'),
	                       'ip_adress' => $db->f('IP'),
	                       'request' => $db->f('request'),
	                       'lang' => $lang);
	    }

	    return $ret;
	}

	function getLogRequestStr()
	{
		$ret = "action=log";

		$ret .= (isset($_REQUEST['filtr']['mod'])) ? "&filtr[mod]=".$_REQUEST['filtr']['mod'] : "";
		$ret .= (isset($_REQUEST['filtr']['action'])) ? "&filtr[action]=".$_REQUEST['filtr']['action'] : "";
		$ret .= (isset($_REQUEST['filtr']['user'])) ? "&filtr[user]=".$_REQUEST['filtr']['user'] : "";
		$ret .= (isset($_REQUEST['filtr']['from'])) ? "&filtr[from]=".$_REQUEST['filtr']['from'] : "";
		$ret .= (isset($_REQUEST['filtr']['to'])) ? "&filtr[to]=".$_REQUEST['filtr']['to'] : "";
		$ret .= (isset($_REQUEST['cnt'])) ? "&cnt=".$_REQUEST['cnt'] : "";

		return $ret;
	}


	/**
	 * Получить кол-во записей в журнале
	 *
	 * @return int
	 */
	function getLogRecordAnount()
	{   GLOBAL $db;

	    $query = "SELECT COUNT(*) AS amount FROM ".$this->table_prefix."_configuration_log";
	    //echo $query;
	    $db->query($query);
	    $db->next_record();

	    return $db->f('amount');
	}

	/**
	 * Удалить из журнала записи позднее $date
	 *
	 * @param string $date 'YYYY-MM-DD'
	 * @return int Кол-во удалённых записей
	 */
	function delLogRecords($date)
	{   GLOBAL $db;

	    $date = strtotime(str_replace(".", "-", $date));
	    if (-1 == $date) {
	    	return FALSE;
	    }

	    $query = "DELETE FROM ".$this->table_prefix."_configuration_log WHERE utime < ".$date;
	    //echo $query;
	    $db->query($query);

	    return $db->affected_rows();
	}

	function getSiteModules($site_id)
	{   GLOBAL $db;

		if (!$site = Configuration::getSite($site_id)) {
		    return FALSE;
		}
		$ret = array();
		$query = "SELECT id, title, name, active FROM modules";
		$query .= " LEFT JOIN sites_modules ON id=module_id AND site_id=".$site[0]['id'];
		$query .= " WHERE parent_id > '0' ORDER BY title";
		$db->query($query);
		while ($db->next_record()) {
		    $active = $db->f('active');
			$ret[] = array('id' => $db->f('id'),
			               'title' => $db->f('title'),
			               'name' => $db->f('name'),
			               'active' => $active,
			               'checked' => $active ? 'checked' : '');
		}

		return $ret;
	}

	function getSiteModulesByPref($site_pref)
	{   GLOBAL $db;

		$site_pref = My_Sql::escape($site_pref);
		$ret = array();
		$query = "SELECT modules.* active FROM sites, modules";
		$query .= " LEFT JOIN sites_modules ON id=module_id AND site_id=".$site[0]['id'];
		$query .= " WHERE alias='".$site_pref."' AND sites.id = site_id";
		$query .= " ".($activ ? " AND activ" : "")." ORDER BY title";
		$db->query($query);
		while ($db->next_record()) {
		    $active = $db->f('active');
			$ret[] = array('id' => $db->f('id'),
			               'title' => $db->f('title'),
			               'name' => $db->f('name'),
			               'active' => $active,
			               'checked' => $active ? 'checked' : '');
		}

		return $ret;
	}

	function setSiteModules($site_id, $mod_list)
	{   GLOBAL $db;

	    $site_id = (int) $site_id;
		if (!$site = Configuration::getSite($site_id)) {
		    return FALSE;
		}

		Configuration::delSiteModules($site_id);
		$mods = Configuration::getModules(-1);
		$val = "";
		foreach ($mods as $k => $v) {
		    $val .= "(".$site_id.", ".$v.", ".(in_array($v, $mod_list) || 0 == count($mod_list) ? 1 : 0)."), ";
		}

		if ('' == $val) {
			return FALSE;
		}
		$val = substr($val, 0 , -2);

		$query = "INSERT INTO sites_modules (site_id, module_id, active) VALUES ".$val;
		$db->query($query);

		return $db->affected_rows();
	}

	function delSiteModules($site_id)
	{   GLOBAL $db;

	    $site_id = (int) $site_id;
	    $db->query("DELETE FROM sites_modules WHERE site_id=".$site_id);

	    return $db->affected_rows();
	}

	function getModules($admin_only = 0) {
	  GLOBAL $db;

	  $db->query('SELECT * FROM modules');

		if ($db->nf() > 0) {
			while($db->next_record()) {
				$arr[] = $db->f('id');
			}
			return $arr;
		}
		return FALSE;
	}

	function testUser($siteuser_id) {
		global $CONFIG, $db, $server, $lang;
		if ($siteuser_id) {
			return $this->getUsers($siteuser_id);
		}
	}

	function getUsers($user_id, $site_alias = '') {
		global $CONFIG, $db, $server, $lang;
		$db->query('SELECT id, username FROM core_users ORDER BY username');
		if (!$db->nf()) return FALSE;
		$arr = array();

		while ($db->next_record()) {
			$uid = $db->f('id');
			$arr[] = array(
						"value"			=> $uid,
						"name"			=> $db->f('username'),
						"option_select"	=> ($user_id == $uid) ? ' selected' : '',
					);
		}
		return $arr;
	}

	function createSiteAdmin($site, $site_id, $lang_id) {
		global $CONFIG, $db, $request_siteuser, $request_siteuserinfo;

		/**
		 * Создаём роль для администратора сайта
		 */
		$db->query('INSERT INTO core_roles SET site_id='.$site_id.', lang_id='.$lang_id.',descr="Admin"');
		if (!$db->affected_rows()) return FALSE;
	    $role_id = $db->lid();

		$modules = $this->getModules();
		for ($i=0; $i < sizeof($modules); $i++) {
			$db->query('INSERT INTO core_roles_permissions
						SET role_id='.$role_id.', module_id='.$modules[$i].', r=1, e=1, p=1, d=1');
		}
		/**
		 * Создаём группы для сайта
		 */
		$db->query('INSERT INTO core_groups SET name="Admin '.$site.'", description="Full admin rights"');
		if (!$db->affected_rows()) return FALSE;
		$adm_group_id = $db->lid();
		$db->query('INSERT INTO core_groups SET name="Guest '.$site.'", description="Guest group"');
		if (!$db->affected_rows()) return FALSE;
		$gst_group_id = $db->lid();
		/**
		 * Cвязываем роль администратора и группу
		 */
		$db->query('INSERT INTO core_roles_groups SET role_id='.$role_id.', group_id='.$adm_group_id);

		$uid = 0;
		if ($request_siteuser) {
			$uid = $request_siteuser;
		} else {
			$db->query('INSERT INTO core_users
						SET username = "'.$request_siteuserinfo["siteusername"].'",
							password = md5("'.$request_siteuserinfo["password"].'"),
							email = "'.$request_siteuserinfo["email"].'",
							client_type='.intval($CONFIG["siteusers_default_admin_client_type"]).',
							active=1,
							site_id = '.$site_id.',
							lang_id = '.$lang_id);
			$uid = $db->lid();
		}
		$db->query('INSERT core_users_groups SET user_id='.$uid.', group_id='.$adm_group_id);
		$db->query('INSERT core_users_groups SET user_id='.$uid.', group_id='.$gst_group_id);
		if (!$db->affected_rows()) return FALSE;

		return TRUE;
	}

	function validCronTime($m, $h, $d, $mn, $i)
	{
	    if ("" !== $m && ($m < 0 || $m > 59)) {
	    	return FALSE;
	    }
	    if ("" !== $h && ($h < 0 || $h > 23)) {
	    	return FALSE;
	    }
	    if ("" !== $d && ($d < 1 || $d > 31)) {
	    	return FALSE;
	    }
	    if ("" !== $mn && ($mn < 1 || $mn > 12)) {
	    	return FALSE;
	    }
	    if ("" !== $i && $i < 1) {
	    	return FALSE;
	    }

	    return TRUE;
	}

	function addCronCommand($m, $h, $d, $mn, $i, $cmd, $title)
	{   global $db;

	    if (!$this->validCronTime($m, $h, $d, $mn, $i)) {
	    	return FALSE;
	    }

	    if (empty($cmd) || empty($title)) {
	    	return FALSE;
	    }

	    $query = "INSERT INTO cron (minute, hour, day, month, `interval`, command, title)";
	    $query .= " VALUES (";
	    $query .= "" === $m ? 'NULL, ' : (int) $m.", ";
	    $query .= "" === $h ? 'NULL, ' : (int) $h.", ";
	    $query .= "" === $d ? 'NULL, ' : (int) $d.", ";
	    $query .= "" === $mn ? 'NULL, ' : (int) $mn.", ";
	    $query .= "" === $i ? 'NULL, ' : (int) $i.", ";
	    $query .= "'".My_sql::escape($cmd)."', '".My_sql::escape($title)."')";

	    return $db->query($query) ? $db->lid() : FALSE;
	}

	function getCronCommand($id)
	{   global $db;

	    if (!$db->query('SELECT * FROM cron WHERE id='.(int) $id)) {
	    	return FALSE;
	    }

	    if ($db->next_record()) {
	    	return array(
	    	    'id' => $db->f('id'),
	    	    'minute' => $db->f('minute'),
	    	    'hour' => $db->f('hour'),
	    	    'day' => $db->f('day'),
	    	    'month' => $db->f('month'),
	    	    'interval' => $db->f('interval'),
	    	    'command' => $db->f('command'),
	    	    'title' => $db->f('title'),
	    	    'active' => $db->f('active'),
	    	    'last' => $db->f('last')
	    	);
	    }

	    return FALSE;
	}

	function delCronCommand($id)
	{   global $db;

	    return $db->query("DELETE FROM cron WHERE id=".(int) $id);
	}

	function setCronCommand($id, $param)
	{   global $db;

	    if (!$db->query('SELECT id FROM cron WHERE id='.(int) $id)) {
	    	return FALSE;
	    }

	    $set = array();

	    if (isset($param['time'])) {
	        if (!$this->validCronTime($param['time']['m'],
	                                  $param['time']['h'],
	                                  $param['time']['d'],
	                                  $param['time']['mn'],
	                                  $param['time']['i'])
	        ) {
	        	return FALSE;
	        }
	    	$set[] = "minute=".("" === $param['time']['m'] ? "NULL" : (int) $param['time']['m']);
	    	$set[] = "hour=".("" === $param['time']['h'] ? "NULL" : (int) $param['time']['h']);
	    	$set[] = "day=".("" === $param['time']['d'] ? "NULL" : (int) $param['time']['d']);
	    	$set[] = "month=".("" === $param['time']['mn'] ? "NULL" : (int) $param['time']['mn']);
	    	$set[] = "`interval`=".("" === $param['time']['i'] ? "NULL" : (int) $param['time']['i']);
	    }

	    if (isset($param['cmd'])) {
	    	if (empty($param['cmd'])) {
	    		return FALSE;
	    	}
	    	$set[] = "command='".My_sql::escape($param['cmd'])."'";
	    }

	    if (isset($param['title'])) {
	    	if (empty($param['title'])) {
	    		return FALSE;
	    	}
	    	$set[] = "title='".My_sql::escape($param['title'])."'";
	    }

	    if (isset($param['active'])) {
	    	$set[] = "active=".($param['active'] ? 1 : 0);
	    }

	    if (isset($param['last'])) {
	    	$set[] = "last=NOW()";
	    }

		return count($set) > 0
		    ? $db->query("UPDATE cron SET ".implode(", ", $set)." WHERE id=".intval($id))
		    : FALSE;

	}

	function getCronTab()
	{   global $db;

	    $db->query('SELECT * FROM cron');
	    $ret = array();
	    while ($db->next_record()) {
	    	$ret[] = array(
	    	    'id' => $db->f('id'),
	    	    'minute' => $db->f('minute'),
	    	    'hour' => $db->f('hour'),
	    	    'day' => $db->f('day'),
	    	    'month' => $db->f('month'),
	    	    'interval' => $db->f('interval'),
	    	    'command' => $db->f('command'),
	    	    'title' => $db->f('title'),
	    	    'active' => $db->f('active'),
	    	    'last' => $db->f('last')
	    	);
	    }

	    return $ret;
	}

	function showCronTab()
	{   global $tpl, $baseurl;

	    $tpl->assign(
	        array(
	            'form_action' => $baseurl,
	            'document_root' => $_SERVER['DOCUMENT_ROOT'],
	            'http_host' => $_SERVER['HTTP_HOST'],
	            'new_link' => $baseurl."&action=new_cmd"

	        )
	    );

	    $crontab = $this->getCronTab();
	    if (count($crontab)) {
	    	$tpl->newBlock('block_cmds');
			$tpl->assign(Array(
				"MSG_Activ" => $this->_msg["Activ"],
				"MSG_Ed" => $this->_msg["Ed"],
				"MSG_Minute" => $this->_msg["Minute"],
				"MSG_Hour" => $this->_msg["Hour"],
				"MSG_Day" => $this->_msg["Day"],
				"MSG_Month" => $this->_msg["Month"],
				"MSG_Interval_min" => $this->_msg["Interval_min"],
				"MSG_Name" => $this->_msg["Name"],
				"MSG_Execute" => $this->_msg["Execute"],
				"MSG_Last_start" => $this->_msg["Last_start"],
				"MSG_Del" => $this->_msg["Del"],
			));
	    	foreach ($crontab as $k => $val) {
	    		$tpl->newBlock('block_cmd');
				$tpl->assign(Array(
					"MSG_Edit" => $this->_msg["Edit"],
					"MSG_Confirm_delete_record" => $this->_msg["Confirm_delete_record"],
					"MSG_Delete" => $this->_msg["Delete"],
				));
	    		$tpl->assign(
	    		    array(
	    		        'id' => $val['id'],
        	    	    'minute' => $val['minute'],
        	    	    'hour' => $val['hour'],
        	    	    'day' => $val['day'],
        	    	    'month' => $val['month'],
        	    	    'interval' => $val['interval'],
        	    	    'command' => $val['command'],
        	    	    'title' => $val['title'],
        	    	    'active' => $val['active'],
        	    	    'last' => $val['last'],
        	    	    'del_link' => $baseurl."&action=del_cmd&id=".$val['id'],
        	    	    'edit_link' => $baseurl."&action=edit_cmd&id=".$val['id'],
        	    	    'status_img' => $val['active'] ? 'ico_swof.gif' : 'ico_swon.gif',
        	    	    'status_text' => $val['active'] ? $this->_msg["Suspend"] : $this->_msg["Activate"],
        	        )
	    		);
	    	}
	    }
	}

	function showCronForm($id)
	{   global $tpl, $baseurl;

		if (!$id) {
			$assign = array(
    	            'form_action' => $baseurl,
    	            'action' => 'add_cmd'
    	    );
		} else {
		    $assign = $this->getCronCommand($id);
    		if (!is_array($assign)) {
    			return FALSE;
    		}
    		$assign['form_action'] = $baseurl;
    		$assign['action'] = 'update_cmd';
		}

		$tpl->assign($assign);

		for ($i = 0; $i <= 59; $i++) {
		    $tpl->newBlock('block_minutes');
		    $tpl->assign(
		        array(
		            'elm_idx' => $i,
		            'elm_val' => $i,
		            'selected' => isset($assign['minute']) && $i == $assign['minute'] ? 'selected' : ''
		        )
		    );
		}

		for ($i = 0; $i <= 23; $i++) {
		    $tpl->newBlock('block_hours');
		    $tpl->assign(
		        array(
		            'elm_idx' => $i,
		            'elm_val' => $i,
		            'selected' => isset($assign['hour']) && $i == $assign['hour'] ? 'selected' : ''
		        )
		    );
		}

		for ($i = 1; $i <= 31; $i++) {
		    $tpl->newBlock('block_days');
		    $tpl->assign(
		        array(
		            'elm_idx' => $i,
		            'elm_val' => $i,
		            'selected' => $i == $assign['day'] ? 'selected' : ''
		        )
		    );
		}

		for ($i = 1; $i <= 12; $i++) {
		    $tpl->newBlock('block_months');
		    $tpl->assign(
		        array(
		            'elm_idx' => $i,
		            'elm_val' => $i,
		            'selected' => $i == $assign['month'] ? 'selected' : ''
		        )
		    );
		}

		return TRUE;
	}
}
?>