<?
require_once('../../common.php');
require_once('lib/class.Shop.php');

$main->get_rates();

$shop = new Shop('no_action');		

$LMI_MODE = $CONFIG['shop_WM_activ'] ? 0 : 1; /** �����: 0 - ��������; 1 - ��������. **/

/*
$fp = fopen("log.txt",  "a");
$str = "\n\n==============================================\n";
foreach ($_POST as $key => $val) {
	$str .= $key."-".$val."\n";
}
fwrite($fp, $str);
fclose($fp);
*/

if ( isset($_POST['LMI_PREREQUEST']) && $_POST['LMI_PREREQUEST'] == 1) { # Prerequest
	
	if ( isset($_POST['LMI_PAYMENT_NO']) ) {		
		/** ���������� �� ����� **/
		if ( !$order = $shop->getOrder($_POST['LMI_PAYMENT_NO']) ) {
			//fclose($fp);
			exit;
		}		
		/** ������� �� ����� **/
		if ($order['status'] > 3) {
			//fclose($fp);
			exit;
		}
		$wm_amount = $order['cost'] * $_SESSION['catalog_rates'][$order['currency_id'] . '/' . $CONFIG['shop_WM_currency']];
		$wm_amount = number_format($wm_amount, 2, ".", "");
		if ($_POST['LMI_PAYEE_PURSE'] == $CONFIG['shop_WM_purse'] && $_POST['LMI_PAYMENT_AMOUNT'] == $wm_amount) { 							
			$status_info = "WMId ���������� - ".$_POST['LMI_PAYER_WM']."\n";	
			if ($shop->setOrderStatus($_POST['LMI_PAYMENT_NO'], 0, $status_info)) {
				echo 'YES';
			} 
		}            
	}
	
} else { #  Payment notification
	
    if ( isset($_POST['LMI_PAYMENT_NO']) ) {  
    	/** ���������� �� ����� **/
		if ( !$order = $shop->getOrder($_POST['LMI_PAYMENT_NO']) ) {
			//fclose($fp);
			exit;
		}
		/** ������� �� ����� **/
		if ($order['status'] > 3) {
			//fclose($fp);
			exit;
		}
		$wm_amount = $order['cost'] * $_SESSION['catalog_rates'][$order['currency_id'] . '/' . $CONFIG['shop_WM_currency']];
		$wm_amount = number_format($wm_amount, 2, ".", "");
		
		# Create check string
    	$chkstring = $CONFIG['shop_WM_purse'].$wm_amount.$order['order_id'].$_POST['LMI_MODE'];
    	$chkstring .= $_POST['LMI_SYS_INVS_NO'].$_POST['LMI_SYS_TRANS_NO'].$_POST['LMI_SYS_TRANS_DATE'];
    	$chkstring .= $CONFIG['shop_WM_secret_key'].$_POST['LMI_PAYER_PURSE'].$_POST['LMI_PAYER_WM'];
    	$md5sum = strtoupper(md5($chkstring));
		$hash_check = ($_POST['LMI_HASH'] == $md5sum);
		//fwrite($fp, "chkstring-".$chkstring."\n md5-".$md5sum."\n hash_check-".$hash_check."\n");
	    	  
	    if ($_POST['LMI_PAYEE_PURSE'] == $CONFIG['shop_WM_purse']
	    	&& $_POST['LMI_PAYMENT_AMOUNT'] == $wm_amount
			&& $_POST['LMI_MODE'] == $LMI_MODE
		    && $hash_check
		) {  
		    $status_info = "WMId ���������� - ".$_POST['LMI_PAYER_WM']."\n";	
		    $status_info .= "����� - ".$wm_amount." (".$CONFIG['shop_WM_purse'].")\n";	
		    $status_info .= "� ����� - ".$_POST['LMI_SYS_INVS_NO']."\n";
		    $status_info .= "� ������� - ".$_POST['LMI_SYS_TRANS_NO']."\n";
		    $status_info .= "����� ������� - ".$_POST['LMI_SYS_TRANS_DATE']."\n";
		    $status_info .= $_POST['LMI_PAYMER_NUMBER'] ? "����� ���� Paymer.com ��� ����� ��-����� - ".$_POST['LMI_PAYMER_NUMBER']."\n" : "";
		    $status_info .= $_POST['LMI_PAYMER_EMAIL'] ? "Email. Paymer.com ��� ��-������ - ".$_POST['LMI_PAYMER_EMAIL']."\n" : "";
		    $status_info .= $_POST['LMI_TELEPAT_PHONENUMBER'] ? "����� ��������. �������.�� - ".$_POST['LMI_TELEPAT_PHONENUMBER']."\n" : "";
		    $status_info .= $_POST['LMI_TELEPAT_ORDERID'] ? "����� ������� � �������.�� - ".$_POST['LMI_TELEPAT_ORDERID']."\n" : "";
		    $status_info .= $_POST['LMI_PAYMENT_CREDITDAYS'] ? "���� ������������ � ���� - ".$_POST['LMI_PAYMENT_CREDITDAYS']."\n" : "";
		    
			$shop->setOrderStatus($_POST['LMI_PAYMENT_NO'], 4, $status_info);
		}	
    }
}

//fclose($fp);
?>