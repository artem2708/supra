INSERT INTO `{site_prefix}_shop_delivery` (`id`, `title`, `dscr`, `cost`) VALUES
(3, 'Самовывоз', 'Самовывоз', 0.00),
(4, 'Курьером', 'В пределах МКАДа (300 руб.)', 300.00),
(5, 'По России', 'Доставка по всей России (1000 руб.)', 1000.00);

INSERT INTO `{site_prefix}_shop_order_status` (`id`, `title`, `payable`) VALUES
(5, 'Оплачен', 0),
(4, 'Согласован', 1),
(6, 'Отгружен', 0);