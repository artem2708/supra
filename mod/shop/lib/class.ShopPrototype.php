<?php
$NEW = false;
require_once(RP . "/mod/shop/wm_config.php");
require_once(RP . "/mod/siteusers/lib/class.SiteusersPrototype.php");
if ($NEW) {
    $CONFIG = array_merge($CONFIG, $NEW);
    unset($NEW);
}

$NEW = false;
require_once(RP . "/mod/shop/assist_config.php");
if ($NEW) {
    $CONFIG = array_merge($CONFIG, $NEW);
    unset($NEW);
}
function to_str(&$value)
{
    if (is_array($value) || is_object($value)) {
        return "";
    }
    $value = strval($value);
    return $value;
}

function to_int(&$value)
{
    if (is_int($value)) {
        return $value;
    }
    $internal = intval($value);
    if (strval($internal) != $value) {
        return 0;
    }
    return $internal;
}

function to_float(&$value)
{
    return floatval($value);
}

function to_bool(&$value)
{
    if (is_bool($value)) {
        return $value;
    }
    if ($value == 1) {
        return true;
    }
    return false;
}

function to_array(&$value)
{
    if (is_array($value)) {
        return $value;
    }
    return array();
}

class ShopPrototype extends Module
{

    var $main_title;
    var $table_prefix;
    var $addition_to_path;
    var $module_name = 'shop';
    var $default_action = 'show_users_discounts';
    var $admin_default_action = 'orders';
    var $tpl_path = 'mod/shop/tpl/';
    var $block_module_actions = array();
    var $block_main_module_actions = array();
    var $client_types = array();
    var $skidka_types = array(1 => 'Скидка', 2 => 'Комплект', 3 => 'Комплект-подарок');
    var $data_charset = 'utf-8'; // cp1251, koi8-r, iso-8859-1, utf-8
    var $allow_img_typs = array('image/gif', 'image/jpeg', 'image/png', 'image/pjpeg', 'image/x-png');
    var $is_get_gp = false;
    var $gp = array();
    var $EditorOrder = array(6, 5, 4, 2);

    function ShopPrototype($action = '', $transurl = '?', $properties = array(), $prefix = NULL, $rewrite_mod_params = NULL)
    {
        global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $core, $server, $lang, $permissions;

        /* Get $action, $tpl_name, $permissions */
        extract($this->init(array(
            'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
            'action' => $action, 'tpl_name' => $properties['tpl']
        )), EXTR_OVERWRITE);

        $this->currencies = $CONFIG['catalog_currencies'];
        $this->client_types = $CONFIG['siteusers_client_types'];

        if ('only_create_object' == $action) {
            return;
        }

        if ($this->actionExists($action)) {
            $this->doAction($action);
            return;
        }

        if (!self::is_admin()) {
//---------------------------------- обработка действий с сайта --------------------------------//

            switch ($action) {

                case "remove_f":


                    global $db;
                    $arr = $db->getArrayOfResult("Select filename,id From  sup_rus_catalog_files ");
                    $allProd = $db->getArrayOfResult("Select id From sup_rus_catalog_products");
                    $new = array();
                    foreach ($allProd as $l) {
                        array_push($new, $l['id']);
                    }
                    foreach ($arr as $val) {
                        $explode = explode('_', $val['filename']);
                        $k = false;
                        $filename = "";
                        $label = false;
                        foreach ($explode as $v) {
                            if ($k) {
                                $filename ? $filename = $filename . "_" . $v : $filename = $filename . $v;
                            } else {
                                if (strlen($v) < 5 && is_numeric($v)) {
                                    $v = (int)$v;

                                    if ($v && in_array($v, $new)) {
                                        $label = true;
                                    }
                                }

                            }
                            $k = true;
                        }
                        $id = $val['id'];

                        if ($label) {
                            //var_dump($filename,"-------",$val['filename']);
                            $db->query("UPDATE sup_rus_catalog_files SET filename='{$filename}' WHERE id={$id}");
                        }
                        //   rename (RP."/files/catalog/sup_rus/files/".$val['filename'],RP."/files/catalog/sup_rus/files/".$filename);
                    }
                    break;
                case 'get_all_category':
                    $root_cats = $this->getCategories(1);
                    $add = '&nbsp;&nbsp;&nbsp;';
                    $res = '<select name="category" onchange="loadProds(this.value, 4, this)" id="prod_id">
                <option value="0">выбрать</option>';
                    foreach ($root_cats as $root) {
                        $sep = '';
                        $res = $res . "<option value=" . $root['id'] . ">" . $sep . $root['title'] . "</option>";
                        //$this->showCat($root, $sep);
                        $sep1 = $add . $sep;
                        $cats1 = $this->getCategories($root['id']);
                        if (!sizeof($cats1)) {
                            //$products = $this->getProducts($root['id']);
                            //$this->showProducts($products, $sep1);
                        } else {
                            foreach ($cats1 as $cat1) {
                                $res = $res . "<option value=" . $cat1['id'] . ">" . $sep1 . $cat1['title'] . "</option>";
                                //$this->showCat($cat1, $sep1);
                                $sep2 = $add . $sep1;
                                $cats2 = $this->getCategories($cat1['id']);
                                if (!sizeof($cats2)) {
                                    //$products = $this->getProducts($cat1['id']);
                                    //$this->showProducts($products, $sep2);
                                } else {
                                    foreach ($cats2 as $cat2) {
                                        //$this->showCat($cat2, $sep2);
                                        $res = $res . "<option value=" . $cat2['id'] . ">" . $sep2 . $cat2['title'] . "</option>";
                                        $sep3 = $add . $sep2;
                                        $cats3 = $this->getCategories($cat2['id']);
                                        if (!sizeof($cats3)) {
                                            //$products = $this->getProducts($cat2['id']);
                                            //$this->showProducts($products, $sep3);
                                        } else {
                                            foreach ($cats3 as $cat3) {
                                                // $this->showCat($cat3, $sep3);
                                                $res = $res . "<option value=" . $cat3['id'] . ">" . $sep3 . $cat3['title'] . "</option>";
                                                $sep4 = $add . $sep3;
                                                $cats4 = $this->getCategories($cat3['id']);
                                                if (!sizeof($cats4)) {
                                                    //$products = $this->getProducts($cat3['id']);
                                                    //$this->showProducts($products, $sep4);
                                                } else {
                                                    foreach ($cats4 as $cat4) {
                                                        //$this->showCat($cat4, $sep4);
                                                        $res = $res . "<option value=" . $cat4['id'] . ">" . $sep4 . $cat4['title'] . "</option>";
                                                        //$sep = '  '.$sep;
                                                        //$cats3 = $this->getCategories($parent_id);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }


                            }
                        }

                    }
                    $res = $res . '</select>';
                    $json['content'] = $res;
                    echo json_encode($json);
                    exit;
                    break;

                case 'getprods':
                    global $request_cat_id;

                    $json['content'] = '<select style="margin:3px" name="product" onchange="selectProd(this.value,this)">';
                    $prods = $this->getProducts($request_cat_id);
                    foreach ($prods as $prod) {
                        $json['content'] = $json['content'] . '<option value="' . $prod['id'] . '">' . $prod['title'] . '</option>';
                    }
                    $json['content'] = $json['content'] . '</select>';
                    echo json_encode($json);
                    exit;
                    break;
                case 'getPoductValue':
                    global $request_id;
                    $prods['link'] = $this->getProductLink($request_id);
                    $prods['title'] = $this->getProductTitle($request_id);
                    echo json_encode($prods);
                    exit;
                    break;

                // Показать корзину
                case "showcart":
                    global $request_id, $request_start, $request_parent, $lang;

                    $main->include_main_blocks($this->module_name . '_cart.html', 'main');
                    $tpl->prepare();
                    $this->showCart();
                    //$this->showDeliveryList($baseurl, $_SESSION['order']['delivery_id']);
                    $this->main_title = $this->_msg["Cart"];
                    break;

                case "actions":
                    $main->include_main_blocks($this->module_name . '_actions.html', 'main');
                    $tpl->prepare();

                    $actions = $this->getCurrentActions();

                    foreach ($actions as $action) {
                        $tpl->newBlock('action');
                        $link = $baseurl . '&action=action&id=' . $action['id'];
                        $tpl->assign('link', $link);
                        $tpl->assign('title', $action['title']);
                        $tpl->assign('preview', $action['preview']);
                        $_action = $this->getAction($action['id'], true);
                        if (!$_action['skidki']) {
                            $tpl->assign('img_preview', $action['img']);
                        }
                        if ($action['img']) {
                            $tpl->newBlock('img');
                            $tpl->assign('link', $link);
                            $tpl->assign('img', $action['img']);
                        }

                        $this->showActionSkidki($_action);
                    }
                    $this->main_title = "Акции";
                    break;

                case "action":
                    $main->include_main_blocks($this->module_name . '_action.html', 'main');
                    $tpl->prepare();
                    $id = (int)$_GET['id'];
                    $_action = $this->getAction($id, true);
                    $tpl->assign('text', $_action['text']);
                    $_action['skidki'] ? $tpl->assign('text_type_first', $_action['text']) : $tpl->assign('text_type_second', $_action['text']);
                    $tpl->assign('preview', $_action['preview']);
                    if ($_action['img']) {
                        $tpl->newBlock('img');
                        $tpl->assign('img', $_action['img']);
                    }
                    $this->showActionSkidki($_action);
                    $this->main_title = $_action['title'];
                    $this->addition_to_path[$_action['title']] = '';//$_action['title'];
                    $PAGE['alt_title'] = $_action['title'];

                    break;

// Добавить в корзину
                case "addtocart":
                    global $request_id, $request_p, $request_qty;

                    if ($this->add_to_cart($request_id, $request_qty, $request_p)) {
                        $this->recalculate_cart();
                        $url = $core->formPageLink($CONFIG['catalog_page_link'], $CONFIG['catalog_page_address'], $lang);
                        $url .= (FALSE == strpos($url, "?") ? "?" : "&");
                        if ($GLOBALS['JsHttpRequest']) {
                            $GLOBALS['_RESULT'] = array(
                                'cost' => $_SESSION["order"]["cost"],
                                'total' => $_SESSION["order"]["total"],
                                'currency' => $CONFIG['catalog_currencies'][$CONFIG['shop_order_concurency']]
                            );
                            exit;
                        }
                        //echo 'end';
                        //exit;
                        //Module::go_back();
                        header('Location: '.$baseurl.'/shop');//$_SERVER['HTTP_HOST'].$url.'action=shwprd&id='.$request_id
                        exit;
                    } else {
                        if ($GLOBALS['JsHttpRequest']) {
                            $GLOBALS['_RESULT'] = false;
                            exit;
                        }
                        $main->show_result_message($this->_msg["Selected_good_absent"]);
                    }
                    break;
// Добавить в корзину комплект
                case "addtocart2":
                    global $request_id;

                    foreach($request_id as $id){
                        $k = $this->add_to_cart($id, 1);
                    }

                    if($k){
                        $this->recalculate_cart();
                        $url = $core->formPageLink($CONFIG['catalog_page_link'], $CONFIG['catalog_page_address'], $lang);
                        $url .= (FALSE == strpos($url, "?") ? "?" : "&");
                        if ($GLOBALS['JsHttpRequest']) {
                            $GLOBALS['_RESULT'] = array(
                                'cost' => $_SESSION["order"]["cost"],
                                'total' => $_SESSION["order"]["total"],
                                'currency' => $CONFIG['catalog_currencies'][$CONFIG['shop_order_concurency']]
                            );
                            exit;
                        }
                        //echo 'end';
                        //exit;
                        //Module::go_back();
                        header('Location: '.$baseurl.'/shop');//$_SERVER['HTTP_HOST'].$url.'action=shwprd&id='.$request_id
                        exit;
                    } else {
                    if ($GLOBALS['JsHttpRequest']) {
                        $GLOBALS['_RESULT'] = false;
                        exit;
                    }
                    $main->show_result_message($this->_msg["Selected_good_absent"]);
                }

                    break;
// Пересчитать корзину
                case "recal":
                    global $request_prod_id, $request_delivery;
                    $this->recalculate_cart($request_prod_id, $request_delivery);
                    Module::go_back();
                    header('Location: ' . $baseurl . '&action=showcart');
                    break;

// Удалить элемент
                case "delete":
                    global $request_id, $request_key, $lang;
                    if ($this->delete_item_from_cart($request_id, $request_key)) {
                        $this->recalculate_cart();
                    }
                    header('Location: ' . $baseurl);
                    exit;
                    break;

// Очистить корзину
                case "del_all":
                    global $request_id, $request_start, $request_parent, $lang;
                    if ($this->delete_item_from_cart()) {
                        $this->recalculate_cart();
                    }
                    header('Location: ' . $transurl . '&action=showcart&id=' . $request_id . '&parent=' . $request_parent);
                    exit;
                    break;

// Повторить заказ пользователя
                case "repeat":
                    global $request_id, $lang;
                    $result = $this->repeat_order($request_id);
                    if ($result) {
                        $this->recalculate_cart();
                        header('Location: ' . $transurl . '&action=showcart');
                    } else {
                        $main->show_result_message($this->_msg["Cannot_repeat_order"]);
                    }
                    break;
//Получить способы оплаты и инфо по доставке
                case "getDelivery":
                    global $request_id, $request_city;
                    $_SESSION['order']['delivery_id'] = $request_id;
                    $el = $this->showPayTypes('block_pay_types', true);
                    if (strlen($_REQUEST['city']) > 1 && $request_id == 7) {
                        $d = $this->getDelivery($request_id);
                        isset($_SESSION['orderinfo']['bonus']) ? $bonus = $_SESSION['orderinfo']['bonus'] : $bonus = 0;
                        $cost_del = 1;
                        $d['cost'] = 121234;
                        $d['cost'] ? $cost = $d['cost'] - $bonus : 0;

                        $this->recalculate_cart(false, $request_id);
                    } else {
                        $d = $this->getDelivery($request_id);
                        isset($_SESSION['orderinfo']['bonus']) ? $bonus = $_SESSION['orderinfo']['bonus'] : $bonus = 0;
                        $cost_del = $d['cost'];
                        $d['cost'] ? $cost = $d['cost'] - $bonus : 0;

                        $this->recalculate_cart(false, $request_id);
                    }
                    $res = array('pays' => $el,
                        'd_cost' => $cost,
                        'p_cost' => $cost_del,
                        'bonus' => $bonus,
                        'd_title' => $d['title'],
                    );
                    echo json_encode($res);
                    exit;
                    break;

// Оформить заказ
                case "processorder":
                    global $request_index, $request_id, $request_start, $request_parent, $request_step, $lang,
                           $request_surname, $request_name, $request_otch, $request_email, $request_phone, $request_country, $request_region,
                           $request_city, $request_zip_code, $request_address, $request_payment_type, $request_organization,
                           $request_inn, $request_rs, $request_ks, $request_bank, $request_bik, $request_additional,
                           $request_prod_id, $request_delivery, $request_notify, $request_bonus, $api_ems_date, $request_promo;

                    $request_customer = $request_surname . " " . $request_name . " " . $request_otch;

                    $main->include_main_blocks($this->module_name . '_process_order.html', 'main');
                    $tpl->prepare();
                    if (isset($request_prod_id) && is_array($request_prod_id)) {
                        $request_delivery = 3;//для того что-бы доставка была равна 0 (самовывоз) хард
                        $discounts = $this->recalculate_cart($request_prod_id, $request_delivery);
                    }


                    if (!$request_step || $request_step == 1) {
                        if (is_array($_SESSION['order'])) {
                            $tpl->newBlock('block_process_order');
                            $tpl->newBlock('address');
                            $tpl->gotoBlock('block_process_order');
                            if (!$_SESSION['siteuser']['is_authorized']) {
                                $tpl->newBlock('no_auth_user');
                                $tpl->assign(array(
                                    'email' => $_SESSION['orderinfo']['email'] ? $_SESSION['orderinfo']['email'] : '',
                                    'newuser_checked' => isset($_SESSION['orderinfo']) && !$_SESSION['orderinfo']['reguser'] ? 'checked' : '',
                                    'reguser_checked' => $_SESSION['orderinfo']['reguser'] ? 'checked' : '',
                                    'step_cnt' => 4,
                                ));
                                $tpl->gotoBlock('block_process_order');
                            }
                            if ($_SESSION['siteuser']['is_authorized']) {

                                $userAddress = $this->getUserAddress($_SESSION['siteuser']['id']);
                                foreach ($userAddress as $val) {
                                    $tpl->gotoBlock('address');
                                    $tpl->assign(array(
                                        "area" => $val['area'],
                                        "city" => $val['city'],
                                        "index" => $val['index'],
                                        "region" => $val['region'],
                                        "address" => $val['address'],
                                    ));
                                    $tpl->gotoBlock('block_process_order');
                                    //var_dump($val);
                                    $_SESSION['siteuser']['name'] = $val["name"];
                                    $_SESSION['siteuser']['phone'] = $val["phone"];
                                    $_SESSION['siteuser']['surname'] = $val["surname"];
                                    $_SESSION['siteuser']['lastname'] = $val["lastname"];
                                    $_SESSION['siteuser']['additional'] = $val["txtInfo"];
                                    $tpl->assign(array(
                                        'additional', $val["txtInfo"],
                                        'customer', $val["lastname"],
                                        'name', $val["name"],
                                        'surname', $val["surname"],
                                        'phone', $val["phone"],
                                        'email', $_SESSION['siteuser']['email']
                                    ));
                                }

                            }
                            $assign = array(
                                'transurl' => $transurl,
                                'process_order_action' => $transurl . '&action=processorder',
                                'customer' => $_SESSION['orderinfo']['fio'] ? $_SESSION['orderinfo']['fio']
                                    : ($_SESSION['siteuser']['is_authorized'] ? "{$_SESSION['siteuser']['lastname']} {$_SESSION['siteuser']['surname']} {$_SESSION['siteuser']['name']}" : ''),
                                'phone' => $_SESSION['orderinfo']['phone'] ? $_SESSION['orderinfo']['phone'] : $_SESSION['siteuser']['phone'],
                                'address' => $_SESSION['orderinfo']['address'] ? $_SESSION['orderinfo']['address'] : $_SESSION['siteuser']['address'],
                                'info' => $_SESSION['orderinfo']['info'],
                                'organization' => $_SESSION['orderinfo']['organization'] ? $_SESSION['orderinfo']['organization'] : $_SESSION['siteuser']['company'],
                                'inn' => $_SESSION['orderinfo']['inn'] ? $_SESSION['orderinfo']['fio'] : $_SESSION['siteuser']['inn'],
                                'rs' => $_SESSION['orderinfo']['rs'] ? $_SESSION['orderinfo']['rs'] : $_SESSION['siteuser']['rs'],
                                'ks' => $_SESSION['orderinfo']['ks'] ? $_SESSION['orderinfo']['ks'] : $_SESSION['siteuser']['ks'],
                                'bank' => $_SESSION['orderinfo']['bank'] ? $_SESSION['orderinfo']['bank'] : $_SESSION['siteuser']['bank'],
                                'bik' => $_SESSION['orderinfo']['bik'] ? $_SESSION['orderinfo']['bik'] : $_SESSION['siteuser']['bik'],
                                'css_display_step0' => $_SESSION['siteuser']['is_authorized'] ? 'none' : 'auto',
                                'css_display_step1' => $_SESSION['siteuser']['is_authorized'] ? 'auto' : 'none',
                                'css_class_disabled_step1' => $_SESSION['siteuser']['is_authorized'] ? '' : 'disabled',
                                'css_display_yur' => $_SESSION['orderinfo']['yur'] ? 'block' : 'none',
                                'step_cnt' => 4,
                                'step_12' => 2,
                                'step_23' => 3,
                                'step_34' => 4,
                                'additional' => $_SESSION['orderinfo']['additional'] ? $_SESSION['orderinfo']['additional'] : $_SESSION['siteuser']['additional'],
                                'email' => $_SESSION['orderinfo']['email'] ? $_SESSION['orderinfo']['email'] : $_SESSION['siteuser']['email'],
                                'surname' => $_SESSION['orderinfo']['surname'] ? $_SESSION['orderinfo']['surname'] : $_SESSION['siteuser']['surname'],
                                'name' => $_SESSION['orderinfo']['name'] ? $_SESSION['orderinfo']['name'] : $_SESSION['siteuser']['name'],
                                'lastname' => $_SESSION['orderinfo']['lastname'] ? $_SESSION['orderinfo']['lastname'] : $_SESSION['siteuser']['lastname'],
                            );


                            if ($_SESSION['siteuser']['is_authorized']) {
                                $assign['step_cnt'] = 3;
                                $assign['step_12'] = 1;
                                $assign['step_23'] = 2;
                                $assign['step_34'] = 3;
                            }
                            $tpl->assign($assign);
                            if ($_SESSION['siteuser']['is_authorized']) {
                                $odjS = new SiteusersPrototype();
                                $allAddress = $odjS->getAllAddress($_SESSION["siteuser"]["id"]);
                                $balance = $this->getUserBonusBalance($_SESSION['siteuser']['id']);
                                if (($balance >= (int)$request_bonus) && (int)$request_bonus > 0) $_SESSION['orderinfo']['bonus'] = (int)$request_bonus;
                                else unset($_SESSION['orderinfo']['bonus']);
                                $promoTrue = false;
                                if($request_promo){
	                                $CDBT				=	new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
							    $discountTotalP = 0;
	                                foreach( $discounts as $category => $discount){
		                                $parent = array('0'=>$category);
		                                while( $parent[0]!=1 ){
			                                 $discountCategory = $parent[0];
			                                 $parent = $CDBT->getParent($parent[0]);
			                                };
			                                $discountTotalP += $this->getDiscountC($request_promo, $discountCategory, $discount);
		                                
	                                }

	                                $_SESSION['orderinfo']['bonus'] = ceil($discountTotalP/10) * 10;
	                                $_SESSION['orderinfo']['promo_code'] = $request_promo;
	                                $discountTotalP ? $promoTrue=$request_promo : $promoTrue=false;
                                }
                                if (!empty($allAddress)) {
                                    $attr = "";
                                    $tpl->newBlock("block_selectAddress");
                                    $selectAddress = '<select name="selectAddress" id="selectAddress">';
                                    $divHidden = "";
                                    foreach ($allAddress as $val) {
                                        $name = $val['name'] . " " . $val['surname'] . " " . $val['lastname'] . " " . $val['phone'] . " " . $val['area'] . " " . $val['region'] . " " . $val['index'] . " " . $val['city'] . " " . $val['address'];
                                        if ($val['primary']) {
                                            $attr = "selected=selected";
                                            $ini = $val['id'];
                                        }
                                        $selectAddress = $selectAddress . '<option value="' . $val['id'] . '" ' . $attr . ' >' . $name . '</option>';
                                        $divHidden = $divHidden . "<div id='select" . $val['id'] . "' style='display:none'>
                            <span name='name'>" . $val['name'] . "</span>
                            <span name='surname'>" . $val['surname'] . "</span>
                            <span name='lastname'>" . $val['lastname'] . "</span>
                            <span name='phone'>" . $val['phone'] . "</span>
                            <span name='area'>" . $val['area'] . "</span>
                            <span name='region'>" . $val['region'] . "</span>
                            <span name='index'>" . $val['index'] . "</span>
                            <span name='city'>" . $val['city'] . "</span>
                            <span name='address'>" . $val['address'] . "</span>
                            <span name='txtInfo'>" . $val['txtInfo'] . "</span>
                             </div>";
                                        $attr = "";
                                    }
                                    $data = $selectAddress . '</select>' . $divHidden;
                                    $tpl->assign('selectAddress', $data);
                                }
                                $this->main_title = $this->_msg["Process_order_for_user"] . ' ' . $_SESSION['siteuser']['surname'] . ' ' . $_SESSION['siteuser']['firstname'];


                            } else {
                                $this->main_title = $this->_msg["Process_order"];
                                $promoTrue = false;
                                if($request_promo){
	                                $CDBT				=	new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
							    $discountTotalP = 0;
	                                foreach( $discounts as $category => $discount){
		                                $parent = array('0'=>$category);
		                                while( $parent[0]!=1 ){
			                                 $discountCategory = $parent[0];
			                                 $parent = $CDBT->getParent($parent[0]);
			                                };
			                                $discountTotalP += $this->getDiscountC($request_promo, $discountCategory, $discount);
		                                
	                                }

	                                $_SESSION['orderinfo']['bonus'] = ceil($discountTotalP/10) * 10;
	                                $_SESSION['orderinfo']['promo_code'] = $request_promo;
	                                $discountTotalP ? $promoTrue=$request_promo : $promoTrue=false;
                                }
                                
                            }
                            if ($CONFIG['shop_pay_type_cash']) {
                                $tpl->newBlock('pay_type_cash');
                            }
                            if ($CONFIG['shop_pay_type_sbr']) {
                                $tpl->newBlock('pay_type_sbr');
                            }
                            if ($CONFIG['shop_pay_type_non_cash']) {
                                $tpl->newBlock('pay_type_non_cash');
                            }
                            if ($CONFIG['shop_WM_activ']) {
                                $tpl->newBlock('pay_type_wm');
                                /* Если оплата WebMoney - учитываем скидку/надбавку */
                                if ($CONFIG['shop_WM_discount']) {
                                    $tpl->newBlock('block_webmoney_discount');
                                    $tpl->assign('webmoney_discount', ($CONFIG['shop_WM_discount'] > 0 ? "+" : "") . $CONFIG['shop_WM_discount']);
                                }
                            }
                            if ($CONFIG['shop_assist_activ']) {
                                $tpl->newBlock('pay_type_assist');
                                /* Если оплата ASSIST - учитываем скидку/надбавку */
                                if ($CONFIG['shop_assist_discount']) {
                                    $tpl->newBlock('block_assist_discount');
                                    $tpl->assign('assist_discount', ($CONFIG['shop_assist_discount'] > 0 ? "+" : "") . $CONFIG['shop_assist_discount']);
                                }
                            }
                            $this->showCart($promoTrue);
                            //$this->showDelivery($_SESSION['order']['delivery_id']);
                            //
                                            if (isset($_SESSION['orderinfo']['bonus'])) {
                    $tpl->newBlock('bonus_value');
                    $tpl->assign(array('bonus' => $_SESSION['orderinfo']['bonus'], 'message' => $promoTrue ? 'Общая скидка по промокоду "'.$request_promo.'"': 'Погашается баллов', 'currency' => $promoTrue ? 'руб.' : ''));
                }
                            if ($_SESSION['siteuser']['siteusername'] == "Admin") {
                                // для отображения способов оплаты  $this->showPayTypes();
                            }
                            $this->showPayTypes();
                            isset($_SESSION['order']['delivery_id']) ? $del_id = $_SESSION['order']['delivery_id'] : $del_id = 1;;
                            $this->showDeliveryList($baseurl, $del_id);


                        } else {
                            $tpl->newBlock('block_no_order');
                        }
                        // Оформление заказа, шаг второй
                    } else if ($request_step == 2) {
                        if (!$request_payment_type) $request_payment_type = 2;
                        $order_id = $this->process_order($request_customer,
                            $request_email,
                            $request_phone,
                            $request_supratown,
                            $request_address,
                            $request_additional,
                            $request_payment_type
                        );
                        if ($order_id) {
                            $order = $this->getOrder($order_id);
                            //$order['']
                            $tpl->newBlock('block_order');
                            $order['disc_stlye'] = (int)$order['discount'] ? '' : 'style="display:none"';
                            if ($_SESSION['id'] == "2")
                                $order['order_payLink'] = $this->getRoboPay($order_id, "Заказ №" . $order_id . "на сайте suprasop.ru ", $order['total_sum']);
                            $tpl->assign($order);

                            /* Если оплата WebMoney - учитываем скидку/надбавку */
                            if (4 == $order['pay_type_id'] && $CONFIG['shop_WM_discount']) {
                                $tpl->newBlock('webmoney_discount');
                                $tpl->assign('webmoney_discount', ($CONFIG['shop_WM_discount'] > 0 ? "+" : "") . $CONFIG['shop_WM_discount']);
                            }

                            /* Если оплата ASSIST - учитываем скидку/надбавку */
                            if (5 == $order['pay_type_id'] && $CONFIG['shop_assist_discount']) {
                                $tpl->newBlock('assist_discount');
                                $tpl->assign('assist_discount', ($CONFIG['shop_assist_discount'] > 0 ? "+" : "") . $CONFIG['shop_assist_discount']);
                            }

                            /** Способ оплаты **/
                            switch ($order['pay_type_id']) {
                                /** квитанция Сбербанк **/
                                case 2:
                                    $tpl->newBlock('block_sbr');
                                    $compn_info = $this->getCompanyInfo();
                                    $compn_info['sbr_ticket_link'] = $baseurl . ('&' == substr($baseurl, -1) ? '' : '&') . "action=sbr_ticket&id=" . $order_id;
                                    $compn_info['number'] = $order_id;
                                    $tpl->assign($compn_info);
                                    break;

                                /** банковский перевод **/
                                case 3:
                                    $tpl->newBlock('block_bank');
                                    $compn_info = $this->getCompanyInfo();
                                    $compn_info['number'] = $order_id;
                                    $tpl->assign($compn_info);
                                    break;

                                /** Webmoney **/
                                case 4:
                                    if (
                                        $order['cost'] > 0
                                        && $CONFIG['shop_WM_purse']
                                        && in_array($CONFIG['shop_WM_currency'], array('RUR', 'USD', 'EUR'))
                                        && $CONFIG['shop_WM_activ']
                                    ) {
                                        /* Сумма заказа в валюте кошелька webmoney*/
                                        $wm_amount = ShopPrototype::exchange($order['cost'], $order['currency_id'], $CONFIG['shop_WM_currency']);
                                        $wm_amount = number_format($wm_amount, "2", ".", "");
                                        $tpl->newBlock('block_webmoney');
                                        $tpl->assign(
                                            array(
                                                'wm_amount' => $wm_amount,
                                                'wm_payment_no' => $order['order_id'],
                                                'wm_payee_purse' => $CONFIG['shop_WM_purse'],
                                                'order_sum' => $order['cost'],
                                                'wm_success_url' => $CONFIG['shop_WM_success_url'],
                                                'wm_fail_url' => $CONFIG['shop_WM_fail_url'],
                                                'wm_result_url' => "http://" . $_SERVER['HTTP_HOST'] . $CONFIG['shop_WM_result_url'],
                                            )
                                        );
                                    }
                                    break;

                                /** ASSIST **/
                                case 5:
                                    if (
                                        $order['cost'] > 0
                                        && $CONFIG['shop_assist_idp']
                                        && in_array($CONFIG['shop_assist_currency'], array('RUR', 'USD', 'EUR'))
                                        && $CONFIG['shop_assist_activ']
                                    ) {
                                        /* Сумма заказа в валюте ASSIST */
                                        $assist_amount = ShopPrototype::exchange($order['cost'], $order['currency_id'], $CONFIG['shop_assist_currency']);
                                        number_format($wm_amount, "2", ".", "");
                                        $tpl->newBlock('block_assist');
                                        $tpl->assign(
                                            array(
                                                'assist_amount' => $assist_amount,
                                                'assist_order_id' => $order['order_id'],
                                                'assist_shop_id' => $CONFIG['shop_assist_idp'],
                                                'assist_currency' => $CONFIG['shop_assist_currency'],
                                                'assist_success_url' => "http://" . $_SERVER['HTTP_HOST'] . $CONFIG['shop_assist_result_url'],
                                                'assist_fail_url' => $CONFIG['shop_assist_fail_url'],
                                                'order_sum' => $order['cost'],
                                            )
                                        );
                                    }
                                    break;

                                case 6:
                                    if (
                                        $order['total_sum'] > 0
                                        && $CONFIG['shop_chronopay_activ']
                                    ) {
                                        $amount = $order['total_sum'];
                                        $tpl->newBlock('block_chronopay');
                                        $tpl->assign(
                                            array(
                                                'product_id' => $CONFIG['shop_chronopay_product_id'],
                                                'product_price' => $amount,
                                                'order_id' => $order['order_id'],
                                                'sign' => md5($CONFIG['shop_chronopay_product_id'] . "-" . $amount . "-" . $order['order_id'] . "-" . $CONFIG['shop_chronopay_sharedSec']),///
                                                'host' => "http://" . $_SERVER['HTTP_HOST'],
                                            )
                                        );
                                    }
                                    break;

                                case 7:


// Инициализация параметров для формы платежа
// Параметры могут извлекаться из БД или из других хранилищ данных,
// либо содержаться внутри кода
                                    if (
                                        $order['total_sum'] > 0
                                        //&& $CONFIG['shop_uniteller_activ']
                                    ) {
                                        $this->uniteller($order);
                                    }
                                    break;
                            }

                            if ($_SESSION['siteuser']['is_authorized']) {
                                $tpl->newBlock('block_is_auth');
                                $link = Core::formPageLink($CONFIG['shop_page_link'], $CONFIG['shop_page_address'], $lang, 1);
                                $link .= ('&' == substr($link, -1) ? '' : '&') . "action=orders";
                                $tpl->assign('history_link', $link);
                            }
                            if ($order['delivery_id']) {
                                $dlvr = $this->getDelivery($order['delivery_id']);
                                $tpl->newBlock('block_order_delivery');
                                $tpl->assign(array(
                                    'delivery_cost' => $order['delivery_id'] == 6 ? 'от 300' : $order['delivery_cost'],
                                    'currency' => $CONFIG['catalog_currencies'][$CONFIG['shop_order_concurency']],
                                    'title' => $dlvr['title'],
                                    'dscr' => $dlvr['dscr'],
                                    'eql' => $order['total_sum'],
                                    'text_transp' => $order['delivery_id'] == 6 ? 'оплата по факту получения уточняйте у менеджера' : '',
                                    'delivery_date' => $order['delivery_id'] == 5 ? '<p><b>Ориентировочный срок доставки</b>: ' . $_POST['api_ems_date'] . ' с  даты оплаты.</p><br>' : ''
                                ));
                            }
                        } else {
                            $tpl->newBlock('block_order_error');
                        }

                        $this->main_title = $this->_msg['Set_order'];
                        // $_SESSION['order']="";
                    }
                    $this->addition_to_path = $this->_msg['Set_order'];


                    break;

                case "chonopaycallback":
                    if ($_REQUEST['transaction_type'] = "Purchase") {
                        $order = $this->getOrder($_REQUEST['order_id']);
                        /* if($order['total_sum']!= $_REQUEST['total']){
                            echo "err";exit;
                        }*/
                        if (md5($CONFIG['shop_chronopay_sharedSec'] . $_REQUEST['customer_id'] . $_REQUEST['transaction_id'] . $_REQUEST['transaction_type'] . $_REQUEST['total']) == $_REQUEST['sign'])//
                        {
                            $this->setOrderStatus($_REQUEST['order_id'], 5, true);//статус оплачен $_REQUEST['order_id']
                            $this->setOrderNotes($_REQUEST['order_id'], "Оплачен системой Chronopay");//$_REQUEST['order_id']
                            echo '200 OK';
                            exit;

                        }
                        return false;

                    }
                    return false;
                    exit;

                    break;
// Список заказов авторизованного пользователя
                case "orders":
                    global $request_start, $lang;
                    if ($_SESSION['siteuser']['id']) {
                        $main->include_main_blocks($this->module_name . '_orders.html', 'main');
                        $tpl->prepare();
                        $start = ($request_start) ? $request_start : 1;
                        $orders = $this->getAllOrders($_SESSION['siteuser']['id']);
                        $tpl->assign(array("bonus_balance" => $this->getUserBonusBalance($_SESSION['siteuser']['id'])));
                        foreach ($orders as $order) {
                            $tpl->newBlock('block_order');
                            $tpl->assign($order);
                            if ($order['status'] == 0 && $order['pay_type'] == 7) {
                                $this->uniteller($order);
                            }
                        }

                    } else {
                        $main->show_result_message($this->_msg["Info_orders_history"]);
                    }
                    $this->main_title = $this->_msg["Orders_history"];
                    break;

// Показать информацию о заказе для авторизованного пользователя
                case 'vieworder':
                    global $request_id, $request_start, $request_order_id, $lang;
                    $order = $this->getOrder($request_id);
                    if ($order && ($_SESSION['siteuser']['is_authorized'] || session_id() == $order['session'])) {
                        $main->include_main_blocks($this->module_name . '_order.html', 'main');
                        $tpl->prepare();
                        $this->show_order($transurl, $request_id);
                        //$tpl->assign('repeat_order', $baseurl.'&action=repeat&id='.$request_id);
                        //$tpl->assign_array('block_item', $this->\derItems($request_id));
                        //$this->main_title = 'История заказов';
                    } else {
                        $register_page = $core->formPageLink($CONFIG['register_page_link'], $CONFIG['register_page_address'], $lang);
                        header('Location: ' . $register_page . '?action=loginform');
                    }
                    break;


// Квитанция для Сбербанка
                case 'sbr_ticket':
                    global $request_id;
                    $main->include_main_blocks($this->module_name . '_sbr_ticket.html', 'main');
                    $tpl->prepare();
                    $order = $this->getOrder($request_id);
                    if ($order && ($_SESSION['siteuser']['is_authorized'] || session_id() == $order['session'])) {
                        $tpl->newBlock('block_sbr');
                        $tpl->assign(array_merge($this->getCompanyInfo(), $order));
                    } else {
                        $tpl->newBlock('block_no_access');
                    }
                    break;

// Квитанция для Сбербанка
                case 'ordering':
                    global $request_id;
                    $main->include_main_blocks($this->module_name . '_ordering.html', 'main');
                    $tpl->prepare();
                    $order = $this->getOrder($request_id);
                    if ($_SESSION['siteuser']['is_authorized'] && $order) {
                        $tpl->newBlock('block_ordering');
                        $tpl->assign(array_merge($this->getCompanyInfo(), $order));
                    } else {
                        $tpl->newBlock('block_no_access');
                    }
                    break;

                case "login":
                    if ($GLOBALS['JsHttpRequest']) {
                        $user = Module::load_mod('siteusers', 'only_create_object');
                        $res = $user->login_siteuser($_REQUEST['email'], $_REQUEST['pass']);
                        $GLOBALS['_RESULT'] = true === $res
                            ? array(
                                'id' => $_SESSION["siteuser"]["id"],
                                'siteusername' => $_SESSION["siteuser"]["siteusername"],
                                'email' => $_SESSION["siteuser"]["email"],
                            )
                            : false;
                    }
                    exit;


                case "successPay":
                    $main->include_main_blocks($this->module_name . '_robokassa.html', 'main');
                    $tpl->prepare();
                    // as a part of SuccessURL script
                    // your registration data
                    $mrh_pass1 = "securepass1";
                    // merchant pass1 here
                    // HTTP parameters:
                    $out_summ = $_REQUEST["OutSum"];
                    $inv_id = $_REQUEST["InvId"];
                    $crc = $_REQUEST["SignatureValue"];
                    $crc = strtoupper($crc);
                    // force uppercase
                    // build own CRC
                    $my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass1"));
                    if (strtoupper($my_crc) != strtoupper($crc)) {
                        $tpl->assign(array("message" => "Спасибо за оплату товара."));
                    }
                    $_SESSION['robopay'] = $my_crc;
                    // you can check here, that resultURL was called // (for better security)
                    // OK, payment proceeds
                    break;

                case "failPay":

                    $main->include_main_blocks($this->module_name . '_robokassa.html', 'main');
                    $tpl->prepare();
                    $tpl->assign(array("message" => "Ошибка при проведении платежа."));
                    break;

                case "resultPay":
                    // as a part of ResultURL script
                    // your registration data
                    $mrh_pass2 = "sgg8Ff52A62"; // merchant pass2 here
                    // HTTP parameters:
                    $out_summ = $_REQUEST["OutSum"];
                    $inv_id = $_REQUEST["InvId"];
                    $crc = $_REQUEST["SignatureValue"];
                    // HTTP parameters:$out_summ, $inv_id, $crc
                    $crc = strtoupper($crc);
                    // force uppercase
                    // build own CRC
                    $my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass2"));
                    if (strtoupper($my_crc) != strtoupper($crc)) {
                        echo "Ошибка при проведении платежа.Плохой перевод.";
                        exit();
                    } else {
                        $this->setOrderStatus($inv_id, 5, true);
                        echo "Успешная оплата.";
                    }
                    // print OK signature
                    // perform some action (change order state to paid)
                    break;

                case "resultPayUniteller":

                    // Пришел callback с параметрами Order_ID, Status, Signature
                    if (isset($_GET['Order_ID'])) {
                        header('Location: /uniteller_success');
                        exit;
                    }
                    if (count($_POST) && isset($_POST["Order_ID"]) && isset($_POST["Status"])
                        && isset($_POST["Signature"])
                    ) {
                        // проверка подписи
                        if ($this->checkSignature($_POST["Order_ID"], $_POST["Status"], $_POST["Signature"])) {
							$order = $this->getOrder($_POST["Order_ID"]);
							if($order['status'] == 15){
								$this->setOrderStatus($_POST["Order_ID"], 5, true);
							}
							else{
								$this->setOrderStatus($_POST["Order_ID"], 14, true);
							}
                            

                        } else {
                            header('Location: /uniteller_error');
                            exit;
                        }
                    } else {
                        header('Location: /uniteller_error');
                        exit;
                    }
                    break;

// Default...
                default:
                    if (is_object($tpl)) $tpl->prepare();
                    return;
            }

//------------------------------- конец обработки действий с сайта -----------------------------//
        } else {
//------------------------------ обработка действий из админ части -----------------------------//
            $this->block_module_actions = array();
            $this->block_main_module_actions = array('showcart' => $this->_msg["Order_and_cart"], 'actions' => 'Показывать текущие акции');

            if (!isset($tpl)) {
                $main->message_die('Не подключен класс для работы с шаблонами');
            }

            switch ($action) {
                case "confirmPayUniteller":
                    global $request_order_id;
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    $data=$this->getDataUniteller($request_order_id);
                    $buildNumber = $data['BillNumber'];
                    $buildNumber = preg_replace('%[^0-9]%', '', $buildNumber);
                    $shop_id ='00007729';
                    $Login = '2393';
                    $Password = 'GuLfR2liwdJbxGfUqV4z1egBk4noMGo90yh5JkNQuSGkvBMNzlc2cbWlnfbzayKT27LgCI5SCTyIkzko';
                    $sPostFields =
                        "Billnumber=" . $buildNumber . "&Login=" . $Login . "&Password=" . $Password . "&Shop_ID=".$shop_id;
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "https://wpay.uniteller.ru/confirm/");
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_VERBOSE, 0);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $sPostFields);
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                    curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
                    $curl_response = curl_exec($ch);
                    $curl_error = curl_error($ch);
                    $data = array(); // результат для возврата

                    if ($curl_error) {
                        var_dump($curl_error);exit;
                    } else {
                        $arr = explode(";", $curl_response);
                
                        if(in_array('ErrorCode',$arr) ){
                            echo "<pre>"; var_dump($arr);exit;
                        }else{
                            $this->setOrderStatus($request_order_id, 15, true);
                            header("Location: /admin.php?lang=rus&name=shop&action=orders");
                            exit;
                        }
                    }
                    break;
                case 'discountup':
                    global $request_start;
                    $pageUrl = "";
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_shift, $request_id;
                    $this->upDiscount($request_id, $request_shift);
                    if ($request_start) $pageUrl = '&start=' . $request_start;
                    header("Location: $baseurl&action=skidki$pageUrl");
                    exit;
                    break;
                case 'discountdown':
                    global $request_start;
                    $pageUrl = "";
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_shift, $request_id;
                    if ($request_start) $pageUrl = '&start=' . $request_start;
                    $this->downDiscount($request_id, $request_shift);
                    header("Location: $baseurl&action=skidki$pageUrl");
                    exit;

                    break;

                case 'addorder':
                    /* if(!in_array($_SESSION['siteuser']['id'],$this->EditorOrder))
                     {
                         $permissions['e']=false;
                     }
                     global $request;

                     if (!$permissions['e'])
                     {
                         $main->message_access_denied($siteusers->module_name, $action);
                     }*/
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    $main->include_main_blocks_2($this->module_name . '_add_order.html', $this->tpl_path);
                    $tpl->prepare();
                    //$this->show_order($transurl, $request_id);
                    $type_p = "";
                    $i = 1;
                    $deliverys = $this->getDeliveryList();
                    $deliveryList = "";
                    foreach ($deliverys as $delivery) {
                        $deliveryList = $deliveryList . "<option value='" . $delivery['id'] . "' >" . $delivery['title'] . "</option>";
                    }
                    foreach ($CONFIG['order_pay_types'] as $types) {

                        $type_p = $type_p . "<option value='" . $i . "'>" . $types . "</option>";
                        $i++;
                    }
                    $search_data = $this->getSearchForTitleData();
                    $search_data = json_encode($search_data);
                    $tpl->assign(array(
                        'pay_types' => $type_p,
                        'deliveryList' => $deliveryList,
                        'address' => '<div>Область/Край:<input id="transp" type="text" name="area" value=""></div>
                            <div>Регион:<input id="transp" type="text" name="region" value=""></div>
                            <div>Почтовый индекс:<input id="transp" type="text" name="index" value=""></div>
                            <div>Город:<input id="transp" type="text" name="city" value=""></div>
                            <div>Адрес:<input id="transp" type="text" name="address" value=""></div>',
                        'action' => 'save_order',
                        'act' => "Добавить",
                        'search_data' => $search_data
                    ));
                    $root_cats = $this->getCategories(1);
                    $add = '&nbsp;&nbsp;&nbsp;';
                    foreach ($root_cats as $root) {
                        $sep = '';
                        $this->showCat($root, $sep);
                        $sep1 = $add . $sep;
                        $cats1 = $this->getCategories($root['id']);
                        if (!sizeof($cats1)) {
                            //$products = $this->getProducts($root['id']);
                            //$this->showProducts($products, $sep1);
                        } else {
                            foreach ($cats1 as $cat1) {
                                $this->showCat($cat1, $sep1);
                                $sep2 = $add . $sep1;
                                $cats2 = $this->getCategories($cat1['id']);
                                if (!sizeof($cats2)) {
                                    //$products = $this->getProducts($cat1['id']);
                                    //$this->showProducts($products, $sep2);
                                } else {
                                    foreach ($cats2 as $cat2) {
                                        $this->showCat($cat2, $sep2);
                                        $sep3 = $add . $sep2;
                                        $cats3 = $this->getCategories($cat2['id']);
                                        if (!sizeof($cats3)) {
                                            //$products = $this->getProducts($cat2['id']);
                                            //$this->showProducts($products, $sep3);
                                        } else {
                                            foreach ($cats3 as $cat3) {
                                                $this->showCat($cat3, $sep3);
                                                $sep4 = $add . $sep3;
                                                $cats4 = $this->getCategories($cat3['id']);
                                                if (!sizeof($cats4)) {
                                                    //$products = $this->getProducts($cat3['id']);
                                                    //$this->showProducts($products, $sep4);
                                                } else {
                                                    foreach ($cats4 as $cat4) {
                                                        $this->showCat($cat4, $sep4);
                                                        //$sep = '  '.$sep;
                                                        //$cats3 = $this->getCategories($parent_id);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }


                            }
                        }

                    }

                    //$tpl->assign('repeat_order', $baseurl.'&action=repeat&id='.$request_id);
                    //$tpl->assign_array('block_item', $this->\derItems($request_id));
                    //$this->main_title = 'История заказов';
                    break;

                case 'save_order':
                    /*if(!in_array($_SESSION['siteuser']['id'],$this->EditorOrder))
                    {
                        $permissions['e']=false;
                    }
                    if (!$permissions['e'])
                    {
                        $main->message_access_denied($siteusers->module_name, $action);
                    }*/
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    //очищаем корзину
                    if ($this->delete_item_from_cart()) {
                        $this->recalculate_cart();
                    }
                    global $CONFIG, $PAGE, $db, $server, $lang, $request_person, $request_phone, $request_email, $request_delivery,
                           $request_address, $request_Jur_person, $request_payment_type, $request_additional, $request_prod_id, $request_product_id, $request_city, $request_transp, $request_discount, $request_discountType;
                    $products = array();
                    foreach ($request_product_id as $prod_id) {
                        if ($this->add_to_cart($prod_id, (int)$_REQUEST[$prod_id], Null)) {
                            $this->recalculate_cart();
                            if ($GLOBALS['JsHttpRequest']) {
                                $GLOBALS['_RESULT'] = array(
                                    'cost' => $_SESSION["order"]["cost"],
                                    'total' => $_SESSION["order"]["total"],
                                    'currency' => $CONFIG['catalog_currencies'][$CONFIG['shop_order_concurency']]
                                );
                            }
                        }
                        $productsId[] = $prod_id;

                        if (in_array($prod_id, $productsId)) {
                            $products[$prod_id][0] = $products[$prod_id][0] + (int)$_REQUEST[$prod_id];
                        } else {
                            $products[$prod_id][] = (int)$_REQUEST[$prod_id];
                        }

                    }
                    if ($request_discountType == "percent") {
                        $request_discount = $_SESSION['order']['sum'] * $request_discount / 100;
                    }
                    $_SESSION['order']['sum'] = $_SESSION['order']['sum'] + (float)$request_transp - (float)$request_discount;
                    $main->include_main_blocks($this->module_name . '_process_order.html', 'main');
                    $tpl->prepare();
                    if (isset($products) && is_array($products)) {
                        $this->recalculate_cart($products, $request_delivery);
                    }
                    $request_supratown = $request_city;
                    $request_customer = $request_person;
                    $order_id = $this->process_order($request_customer,
                        $request_email,
                        $request_phone,
                        $request_supratown,
                        $request_address,
                        $request_additional,
                        $request_payment_type
                    );
                    if ($order_id) {
                        $request_transp = (float)$request_transp;
                        $request_discount = (float)$request_discount;
                        $this->updateTransServ($order_id, $request_transp);
                        $this->updateDiscountEdit($order_id, $request_discount);
                        $order = $this->getOrder($order_id);
                        //$order['']
                        $tpl->newBlock('block_order');
                        $order['disc_stlye'] = (int)$order['discount'] ? '' : 'style="display:none"';
                        $tpl->assign($order);

                        /* Если оплата WebMoney - учитываем скидку/надбавку */
                        if (4 == $order['pay_type_id'] && $CONFIG['shop_WM_discount']) {
                            $tpl->newBlock('webmoney_discount');
                            $tpl->assign('webmoney_discount', ($CONFIG['shop_WM_discount'] > 0 ? "+" : "") . $CONFIG['shop_WM_discount']);
                        }

                        /* Если оплата ASSIST - учитываем скидку/надбавку */
                        if (5 == $order['pay_type_id'] && $CONFIG['shop_assist_discount']) {
                            $tpl->newBlock('assist_discount');
                            $tpl->assign('assist_discount', ($CONFIG['shop_assist_discount'] > 0 ? "+" : "") . $CONFIG['shop_assist_discount']);
                        }

                        /** Способ оплаты **/
//                        switch ($order['pay_type_id']) {
//                            /** квитанция Сбербанк **/
//                            case 2:
//                                $tpl->newBlock('block_sbr');
//                                $compn_info = $this->getCompanyInfo();
//                                $compn_info['sbr_ticket_link'] = $baseurl.('&' == substr($baseurl, -1) ? '' : '&')."action=sbr_ticket&id=".$order_id;
//                                $compn_info['number'] = $order_id;
//                                $tpl->assign($compn_info);
//                                break;
//
//                            /** банковский перевод **/
//                            case 3:
//                                $tpl->newBlock('block_bank');
//                                $compn_info = $this->getCompanyInfo();
//                                $compn_info['number'] = $order_id;
//                                $tpl->assign($compn_info);
//                                break;
//
//                            /** Webmoney **/
//                            case 4:
//                                if (
//                                    $order['cost'] > 0
//                                    && $CONFIG['shop_WM_purse']
//                                    && in_array($CONFIG['shop_WM_currency'], array('RUR', 'USD', 'EUR'))
//                                    && $CONFIG['shop_WM_activ']
//                                ) {
//                                    /* Сумма заказа в валюте кошелька webmoney*/
//                                    $wm_amount = ShopPrototype::exchange($order['cost'], $order['currency_id'], $CONFIG['shop_WM_currency']);
//                                    $wm_amount = number_format($wm_amount, "2", ".", "");
//                                    $tpl->newBlock('block_webmoney');
//                                    $tpl->assign(
//                                        array(
//                                            'wm_amount'	=> $wm_amount,
//                                            'wm_payment_no'	=> $order['order_id'],
//                                            'wm_payee_purse'	=> $CONFIG['shop_WM_purse'] ,
//                                            'order_sum' => $order['cost'],
//                                            'wm_success_url'	=> $CONFIG['shop_WM_success_url'],
//                                            'wm_fail_url'		=> $CONFIG['shop_WM_fail_url'],
//                                            'wm_result_url'		=> "http://".$_SERVER['HTTP_HOST'].$CONFIG['shop_WM_result_url'],
//                                        )
//                                    );
//                                }
//                                break;
//
//                            /** ASSIST **/
//                            case 5:
//                                if (
//                                    $order['cost'] > 0
//                                    && $CONFIG['shop_assist_idp']
//                                    && in_array($CONFIG['shop_assist_currency'], array('RUR', 'USD', 'EUR'))
//                                    && $CONFIG['shop_assist_activ']
//                                ) {
//                                    /* Сумма заказа в валюте ASSIST */
//                                    $assist_amount = ShopPrototype::exchange($order['cost'], $order['currency_id'], $CONFIG['shop_assist_currency']);
//                                    number_format($wm_amount, "2", ".", "");
//                                    $tpl->newBlock('block_assist');
//                                    $tpl->assign(
//                                        array(
//                                            'assist_amount'			=> $assist_amount,
//                                            'assist_order_id'		=> $order['order_id'],
//                                            'assist_shop_id'		=> $CONFIG['shop_assist_idp'],
//                                            'assist_currency'		=> $CONFIG['shop_assist_currency'],
//                                            'assist_success_url'	=> "http://".$_SERVER['HTTP_HOST'].$CONFIG['shop_assist_result_url'],
//                                            'assist_fail_url'		=> $CONFIG['shop_assist_fail_url'],
//                                            'order_sum'				=> $order['cost'],
//                                        )
//                                    );
//                                }
//                                break;
//                        }

                        if ($_SESSION['siteuser']['is_authorized']) {
                            $tpl->newBlock('block_is_auth');
                            $link = Core::formPageLink($CONFIG['shop_page_link'], $CONFIG['shop_page_address'], $lang, 1);
                            $link .= ('&' == substr($link, -1) ? '' : '&') . "action=orders";
                            $tpl->assign('history_link', $link);
                        }
                        if ($order['delivery_cost'] > 0) {
                            $dlvr = $this->getDelivery($order['delivery_id']);
                            $tpl->newBlock('block_order_delivery');
                            $tpl->assign(array(
                                'delivery_cost' => $order['delivery_cost'],
                                'currency' => $CONFIG['catalog_currencies'][$CONFIG['shop_order_concurency']],
                                'title' => $dlvr['title'],
                                'dscr' => $dlvr['dscr'],
                                'eql' => $order['total_sum'],
                                'delivery_date' => '<i>Ориентировочный срок доставки</i>:  от 7 до 10 дней с  даты оплаты.<br>)'
                            ));
                        }
                    } else {
                        $tpl->newBlock('block_order_error');
                    }

                    $this->main_title = $this->_msg['Set_order'];

                    $_SESSION['order'] = "";
                    $this->addition_to_path = $this->_msg['Set_order'];
                    header('Location: ' . $baseurl . '&message="Заказ №' . $order_id . ' успешно создан"');
                    exit;

                    break;

                case 'editorder':
                    global $request_id;
                    /*if(!in_array($_SESSION['siteuser']['id'],$this->EditorOrder))
                    {
                        $permissions['e']=false;
                    }
                    if (!$permissions['e'])
                    {
                        $main->message_access_denied($siteusers->module_name, $action);
                    }*/
                    $search_data = $this->getSearchForTitleData();
                    $search_data = json_encode($search_data);
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    $main->include_main_blocks_2($this->module_name . '_add_order.html', $this->tpl_path);
                    $tpl->prepare();
                    //$this->show_order($transurl, $request_id);
                    $type_p = "";
                    $i = 1;
                    $deliverys = $this->getDeliveryList();
                    $deliveryList = "";
                    $order = $this->getOrder($request_id);
                    foreach ($deliverys as $delivery) {
                        if ($order['delivery_id'] == $delivery['id']) {
                            $deliveryList = $deliveryList . "<option value='" . $delivery['id'] . "' selected='selected'>" . $delivery['title'] . "</option>";
                        } else {
                            $deliveryList = $deliveryList . "<option value='" . $delivery['id'] . "' >" . $delivery['title'] . "</option>";
                        }

                    }
                    foreach ($CONFIG['order_pay_types'] as $key => $val) {
                        if ($order['pay_type_id'] == $key) {
                            $type_p = $type_p . "<option value='" . $key . "' selected='selected'>" . $val . "</option>";
                        } else {
                            $type_p = $type_p . "<option value='" . $key . "'>" . $val . "</option>";
                        }

                    }
                    $itemsInfo = $this->excelPrint($order);
                    $tpl->assign(array(
                        'pay_types' => $type_p,
                        'discount' => (float)$order['discount'],
                        'discountPercent' => $order['discount'] * 100 / $order['sum'],
                        'deliveryList' => $deliveryList,
                        'email' => $order['email'],
                        'person' => $order['customer'],
                        'phone' => $order['phone'],
                        'transp' => $order['transp'],
                        'address' => '<textarea rows="4" cols="80" name="address">' . strip_tags($order['address']) . '</textarea>',
                        'jur_person' => strip_tags($order['jpinfo']),
                        'additional' => strip_tags($order['addinfo']),
                        'items' => $itemsInfo,
                        'act' => "Редактировать",
                        'action' => 'update_order&id=' . $request_id,
                        'search_data' => $search_data

                    ));
                    $tpl->newBlock('order_excel');
                    $tpl->assign(array('order_id' => $order['order_id']));
                    $root_cats = $this->getCategories(1);
                    $add = '&nbsp;&nbsp;&nbsp;';
                    foreach ($root_cats as $root) {
                        $sep = '';
                        $this->showCat($root, $sep);
                        $sep1 = $add . $sep;
                        $cats1 = $this->getCategories($root['id']);
                        if (!sizeof($cats1)) {
                            //$products = $this->getProducts($root['id']);
                            //$this->showProducts($products, $sep1);
                        } else {
                            foreach ($cats1 as $cat1) {
                                $this->showCat($cat1, $sep1);
                                $sep2 = $add . $sep1;
                                $cats2 = $this->getCategories($cat1['id']);
                                if (!sizeof($cats2)) {
                                    //$products = $this->getProducts($cat1['id']);
                                    //$this->showProducts($products, $sep2);
                                } else {
                                    foreach ($cats2 as $cat2) {
                                        $this->showCat($cat2, $sep2);
                                        $sep3 = $add . $sep2;
                                        $cats3 = $this->getCategories($cat2['id']);
                                        if (!sizeof($cats3)) {
                                            //$products = $this->getProducts($cat2['id']);
                                            //$this->showProducts($products, $sep3);
                                        } else {
                                            foreach ($cats3 as $cat3) {
                                                $this->showCat($cat3, $sep3);
                                                $sep4 = $add . $sep3;
                                                $cats4 = $this->getCategories($cat3['id']);
                                                if (!sizeof($cats4)) {
                                                    //$products = $this->getProducts($cat3['id']);
                                                    //$this->showProducts($products, $sep4);
                                                } else {
                                                    foreach ($cats4 as $cat4) {
                                                        $this->showCat($cat4, $sep4);
                                                        //$sep = '  '.$sep;
                                                        //$cats3 = $this->getCategories($parent_id);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }


                            }
                        }

                    }
                    if ($order['siteuser_id']) {
                        $tpl->newBlock('user_info_block');
                        $tpl->prepare();
                        $allOrders = $this->getAllOrders($order['siteuser_id']);
                        $tpl->assign_array('block_order', $allOrders);
                        $tpl->newBlock('user_comment');
                        $tpl->assign('user_id', $order['siteuser_id']);
                        $tpl->prepare();
                        $allReview = $this->getUserReview($order['siteuser_id']);
                        $tpl->assign_array('block_review', $allReview);

                    }
                    //$tpl->assign('repeat_order', $baseurl.'&action=repeat&id='.$request_id);
                    //$tpl->assign_array('block_item', $this->\derItems($request_id));
                    //$this->main_title = 'История заказов';
                    break;

                case 'addReview':

                    /* if(!in_array($_SESSION['siteuser']['id'],$this->EditorOrder))
                     {
                         $permissions['e']=false;
                     }
                     if (!$permissions['e'])
                     {
                         $main->message_access_denied($siteusers->module_name, $action);
                     }*/
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $reguest_userId, $request_message, $db;
                    $data = array("date" => date("y-m-d h:i:s", time()),
                        "manager_id" => $_SESSION['siteuser']['id'],
                        "user_id" => $_REQUEST['userId'],
                        "message" => $request_message);
                    $db->set('core_users_review', $data);

                    break;
                case 'update_order':
                    /*if(!in_array($_SESSION['siteuser']['id'],$this->EditorOrder))
                    {
                        $permissions['e']=false;
                    }
                    if (!$permissions['e'])
                    {
                        $main->message_access_denied($siteusers->module_name, $action);
                    }*/
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $CONFIG, $PAGE, $db, $server, $lang, $request_id, $request_person, $request_phone, $request_email, $request_delivery, $request_sendData, $request_sendKvit,
                           $request_address, $request_Jur_person, $request_payment_type, $request_additional, $request_prod_id, $request_product_id, $request_start, $request_transp, $request_discount, $request_discountType;
                    $request_transp = (float)$request_transp;
                    $request_discount = (float)$request_discount;
                    /*$itemsOld=$this->getOrderItems($request_id);
                    foreach($itemsOld as $it)
                    {
                        $idOld[]=$it['id'];
                        $cnt[$it['id']]=$it['item_qty'];
                        $idItem[$it['item_id']]=$it['id'];
                    }
                    $i=0;
                    $ids="";
                    foreach($request_product_id as $id)
                    {
                        $idExp=explode(":",$id);
                        $id=$idExp[1];
                        if(in_array($id,$idOld))
                        {
                            if($cnt[$idExp[1]] != (int)$_REQUEST[$idExp[1]])
                            {
                                $db->query('UPDATE '.$server.$lang.'_shop_order_items SET  item_qty = '.(int)$_REQUEST[$idExp[1]].' WHERE id='.$idExp[1]);
                            }
                            $ids = $ids ? $ids.",".$idExp[1] : $idExp[1];
                           // unset($request_product_id[$i]);

                        }

                        $i++;
                    }*/
                    //var_dump($ids);
                    //exit;

                    $products = array();
                    if ($this->delete_item_from_cart()) {
                        $this->recalculate_cart();
                    }

                    foreach ($request_product_id as $prod_id) {
                        $idExp = explode(":", $prod_id);
                        $lab = false;
                        if ($idExp[1]) {
                            $label = true;
                            $prod_id = $idExp[0];
                            $count = (int)$_REQUEST[$idExp[1]];
                            $gifts = $this->getGiftToProd($prod_id);
                            $giftIdV = $db->getArrayOfResult("SELECT gift_id,kupon FROM sup_rus_shop_order_items WHERE id=" . $idExp[1]);
                            $i = 0;
                            foreach ($gifts[0] as $val) {
                                if ($val['id'] == $giftIdV[0]['gift_id']) {

                                    switch ($i) {
                                        case 0 :
                                            $_REQUEST['choose'] = 1;
                                            break;
                                        case 1 :
                                            $_REQUEST['choose'] = 9;
                                            break;
                                        default :

                                            $i = $i + 2;
                                            $_REQUEST['choose'] = "9." . "$i";


                                            break;
                                    }
                                    $lab = true;
                                } elseif ($giftIdV[0]['kupon']) {
                                    $lab = true;
                                    $_REQUEST['choose'] = 2;
                                }

                                $i++;
                            }
                            if (!$lab) $_REQUEST['choose'] = 1;
                            //var_dump($_REQUEST['choose']);
                        } else {
                            $_REQUEST['choose'] = 1;
                            $count = (int)$_REQUEST[$prod_id];
                        }
                        //exit;
                        if ($prod_id) {
                            if ($this->add_to_cart($prod_id, $count, Null)) {
                                $this->recalculate_cart();
                                if ($GLOBALS['JsHttpRequest']) {
                                    $GLOBALS['_RESULT'] = array(
                                        'cost' => $_SESSION["order"]["cost"],
                                        'total' => $_SESSION["order"]["total"],
                                        'currency' => $CONFIG['catalog_currencies'][$CONFIG['shop_order_concurency']]
                                    );
                                }
                            }
                            $productsId[] = $prod_id;
                            if (!$label) {
                                if (in_array($prod_id, $productsId)) {
                                    $products[$prod_id][0] = $products[$prod_id][0] + (int)$count;
                                } else {
                                    $products[$prod_id][] = (int)$count;
                                }
                            }

                        }
                        $label = false;
                    }
                    // $itemsOld=$this->getOrderItems($request_id);
                    $w = "";
                    // if($ids) $w=' AND id NOT IN ('.$ids.')';
                    $dlvr = $this->getDelivery($request_delivery);
                    $db->query('DELETE FROM ' . $server . $lang . '_shop_order_items WHERE order_id = ' . $request_id . $w);//

                    $this->add_order_items($request_id);//,$ids
                    /* foreach($itemsOld as $val)
                     {
                         if(stripos($ids,$val['id']))
                         {
                             $arr=array("gift_id"=>$val['gift_id'],
                                         "gift_title"=>$val['gift_title'],
                                         "kupon"=>$val['kupon'],
                                         "double_product"=>$val['double_product'],
                                         "double_title"=>$val['double_title'],
                             );
                             $db->query("UPDATE ".$server.$lang."_shop_order_items SET  gift_id = ".$val['gift_id']."
                                         ,gift_title = ".$val['gift_title']."
                                         kupon = ".$val['kupon']."
                                         double_product = ".$val['double_product']." WHERE id=".$idExp[1]);
                         }
                     }*/
                    //var_dump($products);exit;
                    if (isset($products) && is_array($products)) {
                        //  $this->recalculate_cart($products, $request_delivery); //xz lishnij bil
                    }
                    if ($request_discountType == "percent") {
                        $request_discount = $_SESSION['order']['sum'] * $request_discount / 100;
                    }
                    $_SESSION['order']['discount'] = $_SESSION['order']['discount'] + $request_discount;
                    $sumT = $_SESSION['order']['sum'] + $dlvr['cost'] + $request_transp - $_SESSION['order']['discount'];
                    $sum = $_SESSION['order']['sum'];
                    $costD = $dlvr['cost'];
                    $discount = $_SESSION['order']['discount'];
                    $editor = $_SESSION['siteuser']['siteusername'];
                    $db->query("Update {$this->table_prefix}_shop_orders SET customer='{$request_person}',phone='{$request_phone}',email='{$request_email}',
                    delivery_id={$request_delivery},address='{$request_address}',jpinfo='{$request_Jur_person}',pay_type={$request_payment_type},
                    notes='{$request_additional}',transportation_services={$request_transp},total_sum={$sumT},sum={$sum},delivery_cost='{$costD}',discount={$discount},editor='{$editor}'
                    WHERE id={$request_id}");

                    if ($request_email) {

                        $tpl = new TemplatePower(RP . "tpl/" . $server . $lang . "/" . $this->module_name . "_mail_update.html", T_BYFILE);
                        $tpl->prepare();

                        $tpl->assign(Array(
                            "order_id" => $request_id,
                            "site_name" => strpos($CONFIG["sitename_rus"], ".ru") ? $CONFIG["sitename_rus"] : $CONFIG["sitename_rus"] . ".ru",
                            "MSG_Date" => $this->_msg["Date"],
                            "MSG_Customer" => $this->_msg["Customer"],
                            "MSG_Type" => $this->_msg["Type"],
                            "MSG_Phone" => $this->_msg["Phone"],
                            "MSG_Email" => $this->_msg["Email"],
                            "MSG_Address" => $this->_msg["Address"],
                            "MSG_Jur_person" => $this->_msg["Jur_person"],
                            "MSG_Payment" => $this->_msg["Payment"],
                            "MSG_Adding_info" => $this->_msg["Adding_info"],
                            "MSG_Ordered_products_list" => $this->_msg["Ordered_products_list"],
                            "MSG_Product_ID" => $this->_msg["Product_ID"],
                            "MSG_Name" => $this->_msg["Name"],
                            "MSG_Code" => $this->_msg["Code"],
                            "MSG_Price" => $this->_msg["Price"],
                            "MSG_Inner_currency" => $this->_msg["Inner_currency"],
                            "MSG_Qty" => $this->_msg["Qty"],
                            "MSG_Sum" => $this->_msg["Sum"],
                            "MSG_Order_sum_with_discount" => $this->_msg["Order_sum_with_discount"],
                            "MSG_Order_sum" => $this->_msg["Order_sum"],
                            "MSG_Discount_increase" => $this->_msg["Discount_increase"],
                            "MSG_Total_order_sum" => $this->_msg["Total_order_sum"],
                            "MSG_Webmoney_percent_included" => $this->_msg["Webmoney_percent_included"],
                            "MSG_ASSIST_percent_included" => $this->_msg["ASSIST_percent_included"],
                            "MSG_Order_status" => $this->_msg["Order_status"],
                            "MSG_Save" => $this->_msg["Save"],
                            "MSG_Cancel" => $this->_msg["Cancel"],
                            "MSG_Notes" => $this->_msg["Notes"],
                            "MSG_Print_order" => $this->_msg["Print_order"],
                            "MSG_Delivery" => $this->_msg["Delivery"],
                            "MSG_Transport" => 'Транспортные услуги',
                            "editComment" => $_REQUEST['editComment']
                        ));
                        require_once(RP . '/inc/class.Mailer.php');
                        $mail = new Mailer();
                        if ((strripos($request_address, "Москва") !== false) || (strripos($request_address, "москва") !== false) || (strripos($request_address, "МОСКВА") !== false)) {

                            //$mail->AddEmbeddedImage(RP.'i/', 'cirlogo.jpg', 'cirlogo', 'base64', 'image/jpeg');
                            $tpl1 = new TemplatePower(RP . '_shop_waybill.html', T_BYFILE);
                            $items = $this->getOrderItems($request_id);
                            $orederInfo = $this->getOrder($request_id);
                            $i = 1;
                            $tableData = "";
                            foreach ($items as $item) {
                                $tableData = $tableData . "<tr><td>$i</td><td>" . $item['item_name'] . "</td><td>" . $item['item_price'] . "</td><td>" . $item['item_qty'] . "</td><td>шт.</td><td>" . $item['item_price'] . "</td></tr>";
                                $i++;
                            }
                            $tableData = $tableData . "<tr><td>" . $i . "</td><td>Доставка</td><td>" . $dlvr['cost'] . "</td><td>1</td><td>шт.</td><td>" . $dlvr['cost'] . "</td></tr>";
                            $i++;
                            $tableData = $tableData . "<tr><td>" . $i . "</td><td>Транспортные услуги</td><td>" . $orederInfo['transp'] . "</td><td>1</td><td>шт.</td><td>" . $orederInfo['transp'] . "</td></tr>";
                            $i++;
                            $tableData = $tableData . "<tr><td>" . $i . "</td><td>Скидка</td><td>" . $orederInfo['discount'] . "</td><td>1</td><td>шт.</td><td>" . $orederInfo['discount'] . "</td></tr>";
                            $count = $i;
                            $i++;
                            $k = $i + 3;
                            for ($i; $i < $k; $i++) {
                                $tableData = $tableData . "<tr><td>$i</td><td></td><td></td><td></td><td></td><td></td></tr>";
                            }
                            $date = $this->rusdate(time());
                            $tpl1->prepare();
                            $tpl1->assign(array(
                                "order_id" => $request_id,
                                "date" => iconv('utf-8', 'windows-1251', $date),
                                "table_data" => iconv('utf-8', 'windows-1251', $tableData),
                                "item_count" => $count,
                                "summ" => iconv('utf-8', 'windows-1251', $orederInfo['total_sum']),
                                "summ_p" => iconv('utf-8', 'windows-1251', $orederInfo['cost_rur_in_words'])

                            ));

                        } else {
                            $tpl1 = new TemplatePower(RP . 'tpl/' . $server . $lang . '/_kvit_template.html', T_BYFILE);
                            $tpl1->prepare();
                            $tpl1->assign(array(
                                'name' => iconv('utf-8', 'windows-1251', $request_person),
                                'num' => $request_id,
                                'company' => iconv('utf-8', 'windows-1251', $CONFIG['shop_company']),
                                'inn' => iconv('utf-8', 'windows-1251', $CONFIG['shop_inn']),
                                'rs' => iconv('utf-8', 'windows-1251', $CONFIG['shop_sett_account']),
                                'bank' => iconv('utf-8', 'windows-1251', $CONFIG['shop_bank']),
                                'bik' => iconv('utf-8', 'windows-1251', $CONFIG['shop_bik']),
                                'corr' => iconv('utf-8', 'windows-1251', $CONFIG['shop_corr_account']),

                                'address' => iconv('utf-8', 'windows-1251', $request_address),
                                'day' => date('d'),
                                'month' => iconv('utf-8', 'windows-1251', $this->rusdate(time(), '%MONTH%')),
                                'year' => date('Y'),
                                'total' => (double)$sumT,

                            ));
                        };
                        $f = fopen(RP . "kvitancia.htm", "w+");
                        fwrite($f, $tpl1->getOutputContent());
                        fclose($f);
                        $send = true;
                        $this->show_order($baseurl, $request_id);
                        $subject = "Изменение заказа №{$request_id} на сайте {$CONFIG['sitename_rus']}";
                        $mail->CharSet = $CONFIG['email_charset'];
                        $mail->ContentType = "text/html"; //$CONFIG['email_type'];
                        $mail->From = "Suprashop";// $CONFIG['shop_order_recipient'];
                        $mail->Mailer = 'mail';
                        $mail->AddAddress($request_email);
                        $mail->Subject = $subject;
                        $mail->Body = $request_sendData ? $_REQUEST['editComment'] : $tpl->getOutputContent();
                        if ($request_payment_type != 6 && !$request_sendKvit) $mail->AddAttachment(RP . 'kvitancia.htm');
                        if ($request_sendKvit && $request_sendData) $send = false;
                        if ($send) $mail->Send();

                    }
                    $_SESSION['order'] = "";
                    header('Location: ' . $baseurl . "&start=" . $request_start . "&message='Заказ №" . $request_id . " обновлён'");
                    exit;
                    break;

                case 'mergeOrders':

                    /*if(!in_array($_SESSION['siteuser']['id'],$this->EditorOrder))
                    {
                        $permissions['e']=false;
                    }
                    if (!$permissions['e'])
                    {
                        $main->message_access_denied($siteusers->module_name, $action);
                    }*/
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $CONFIG, $PAGE, $db, $server, $lang, $request_orders;

                    if (!strlen($request_orders)) {
                        header('Location: ' . $baseurl);
                        exit;
                    }
                    $orders = explode(':', $request_orders);
                    $totalSum = 0;
                    $sum = 0;
                    $ordersStr = "";

                    for ($i = 0; $i < count($orders); $i++) {
                        if (!$ordersStr) {
                            $ordersStr = $orders[$i];
                        } else {
                            $ordersStr = $ordersStr . ",$orders[$i]";
                        }

                        //$db->query("UPDATE {$this->table_prefix}_shop_orders SET id=max(id)+1,status=0 WHERE id={$orders[$i]}")
                        if ($i != 0) {


                            $items = $this->getOrderItems($orders[$i]);
                            if ($items) {
                                foreach ($items as $item) {
                                    $db->query("UPDATE {$this->table_prefix}_shop_order_items SET order_id={$orders[0]} WHERE order_id={$orders[$i]} AND item_id={$item['item_id']}");
                                }
                            }

                            $db->query("SELECT (total_sum - delivery_cost - transportation_services) AS summa, `sum` FROM {$this->table_prefix}_shop_orders WHERE id={$orders[$i]}");
                            $db->next_record();
                            $totalSum = (float)$totalSum + (float)$db->f("summa");
                            $sum = (float)$sum + (float)$db->f("sum");
                            $db->query("DELETE FROM {$this->table_prefix}_shop_orders Where id={$orders[$i]}");
                        }

                    }
                    $db->query("UPDATE {$this->table_prefix}_shop_orders SET status=0,`sum`=`sum`+{$sum},total_sum=total_sum+{$totalSum} WHERE id={$orders[0]}");

                    $order = $this->getOrder($orders[0]);

                    require_once(RP . '/inc/class.Mailer.php');

                    $mail = new Mailer();
                    if ((strripos($order['address'], "Москва") !== false) || (strripos($order['address'], "москва") !== false) || (strripos($order['address'], "МОСКВА") !== false)) {

                        //$mail->AddEmbeddedImage(RP.'i/', 'cirlogo.jpg', 'cirlogo', 'base64', 'image/jpeg');
                        $tpl1 = new TemplatePower(RP . '_shop_waybill.html', T_BYFILE);
                        $items = $this->getOrderItems($orders[0]);
                        $i = 1;
                        $tableData = "";
                        foreach ($items as $item) {
                            $tableData = $tableData . "<tr><td>$i</td><td>" . $item['item_name'] . "</td><td>" . $item['item_price'] . "</td><td>" . $item['item_qty'] . "</td><td>шт.</td><td>" . $item['item_price'] . "</td></tr>";
                            $i++;
                        }
                        $tableData = $tableData . "<tr><td>" . $i . "</td><td>Доставка</td><td>" . $dlvr['cost'] . "</td><td>1</td><td>шт.</td><td>" . $dlvr['cost'] . "</td></tr>";
                        $i++;
                        $tableData = $tableData . "<tr><td>" . $i . "</td><td>Транспортные услуги</td><td>" . $order['transp'] . "</td><td>1</td><td>шт.</td><td>" . $order['transp'] . "</td></tr>";
                        $i++;
                        $tableData = $tableData . "<tr><td>" . $i . "</td><td>Скидка</td><td>" . $order['discount'] . "</td><td>1</td><td>шт.</td><td>" . $order['discount'] . "</td></tr>";
                        $count = $i;
                        $i++;
                        $k = $i + 3;
                        for ($i; $i < $k; $i++) {
                            $tableData = $tableData . "<tr><td>$i</td><td></td><td></td><td></td><td></td><td></td></tr>";
                        }
                        $date = $this->rusdate(time());
                        $tpl1->prepare();
                        $tpl1->assign(array(
                            "order_id" => iconv('utf-8', 'windows-1251', $orders[0]),
                            "date" => iconv('utf-8', 'windows-1251', $date),
                            "table_data" => iconv('utf-8', 'windows-1251', $tableData),
                            "item_count" => iconv('utf-8', 'windows-1251', $count),
                            "summ" => iconv('utf-8', 'windows-1251', $order['total_sum']),
                            "summ_p" => iconv('utf-8', 'windows-1251', $order['cost_rur_in_words'])

                        ));

                    } else {
                        $tpl1 = new TemplatePower(RP . 'tpl/' . $server . $lang . '/_kvit_template.html', T_BYFILE);
                        $tpl1->prepare();
                        $tpl1->assign(array(
                            'name' => iconv('utf-8', 'windows-1251', $order['customer']),
                            'num' => $order['order_id'],
                            'company' => iconv('utf-8', 'windows-1251', $CONFIG['shop_company']),
                            'inn' => iconv('utf-8', 'windows-1251', $CONFIG['shop_inn']),
                            'rs' => iconv('utf-8', 'windows-1251', $CONFIG['shop_sett_account']),
                            'bank' => iconv('utf-8', 'windows-1251', $CONFIG['shop_bank']),
                            'bik' => iconv('utf-8', 'windows-1251', $CONFIG['shop_bik']),
                            'corr' => iconv('utf-8', 'windows-1251', $CONFIG['shop_corr_account']),

                            'address' => iconv('utf-8', 'windows-1251', $order['address']),
                            'day' => iconv('utf-8', 'windows-1251', date('d')),
                            'month' => iconv('utf-8', 'windows-1251', $this->rusdate(time(), '%MONTH%')),
                            'year' => iconv('utf-8', 'windows-1251', date('Y')),
                            'total' => iconv('utf-8', 'windows-1251', $order['total_sum']),

                        ));
                    }
                    $f = fopen(RP . "kvitancia.htm", "w+");
                    fwrite($f, $tpl1->getOutputContent());
                    fclose($f);

                    $this->show_order($baseurl, $order['order_id']);
                    $subject = "Объединение заказов №{$ordersStr} на сайте {$CONFIG['sitename_rus']}";

                    $mail->CharSet = $CONFIG['email_charset'];
                    $mail->ContentType = "text/html"; //$CONFIG['email_type'];
                    $mail->From = $CONFIG['shop_order_recipient'];
                    $mail->Mailer = 'mail';
                    $mail->AddAddress($order['email']);
                    $mail->Subject = $subject;
                    $mail->Body = "Ваши заказы объеденены в один.";
                    $mail->AddAttachment(RP . 'kvitancia.htm');
                    $mail->Send();
                    header('Location: ' . $baseurl . '&action=orders&message="Заказы ' . $ordersStr . ' объеденены"');
                    exit;
                    break;
                // Накопительные скидки.
                case 'show_users_discounts':
                    if (!$permissions['e']) {
                        $main->message_access_denied($siteusers->module_name, $action);
                    }

                    $main->include_main_blocks_2($this->module_name . "_show_users_discounts.html", $this->tpl_path);
                    $tpl->prepare();

                    $tpl->assign(array(
                        '_ROOT.lang' => $lang,
                        '_ROOT.name' => $this->module_name,
                    ));

                    $tpl->newBlock('block_shop_discounts');
                    $tpl->assign(Array(
                        "MSG_Enter_discount_name" => 'Введите наименование скидки!',
                        "MSG_Enter_discount_criterion" => 'Введите критерий скидки!',
                        "MSG_Activ" => 'Публ.',
                        "MSG_Ed" => 'Ред.',
                        "MSG_Groups" => 'Групп.',
                        "MSG_Discount" => 'Скидка',
                        "MSG_From" => 'От',
                        "MSG_To" => 'До',
                        "MSG_DiscountValue" => 'Величина',
                        "MSG_Type" => 'Тип',
                        "MSG_Del" => 'Удл.',
                        "MSG_Period" => 'Период (мес.)',
                        "add_link" => $baseurl . '&action=edit_discount&discount_id=0'
                    ));

                    $raw_rows = to_array($this->GetUsersDiscounts(false));

                    // Преобразуем значения.
                    $rows = array();
                    foreach ($raw_rows as $row) {
                        $row['is_percent'] = to_int($row['is_percent']) ? '%' : 'абс.';
                        $row['img_stat'] = to_int($row['active']) ? 'ico_swof.gif' : 'ico_swon.gif';
                        $row['act_stat'] = to_int($row['active']) ? 'Оключить публикацию' : 'Включить публикацию';
                        $row['set_activity_link'] = $baseurl . '&action=update_discount_activity&discount_id=' . to_int($row['id']) . '&set_active=' . (to_int($row['active']) ? '0' : '1');
                        $row['edit_users_groups_hint'] = 'Группы пользователей';
                        $row['show_groups_link'] = $baseurl . '&action=show_users_groups&discount_id=' . to_int($row['id']);
                        $row['edit_discount_link'] = $baseurl . '&action=edit_discount&discount_id=' . to_int($row['id']);
                        $row['delete_discount_link'] = $baseurl . '&action=delete_discount&discount_id=' . to_int($row['id']);

                        $rows[] = $row;
                    }

                    reset($rows);
                    foreach ($rows as $row) {
                        $tpl->newBlock("block_discounts_list");
                        $tpl->assign(Array(
                            "MSG_Edit" => 'Редактировать',
                            "MSG_Confirm_delete" => 'Вы действительно хотите удалить?',
                            "MSG_Delete" => 'Удалить',
                        ));

                        //print_r($row);
                        $tpl->assign($row);
                    }

                    $this->main_title = 'Накопительные скидки';
                    break;

                case 'show_users_groups':
                    if (!$permissions['e']) {
                        $main->message_access_denied($siteusers->module_name, $action);
                    }

                    global $request_discount_id;

                    $request_discount_id = to_int($request_discount_id);
                    $discount_row = $this->GetUsersDiscounts($request_discount_id);
                    $discount_row = to_array($discount_row[0]);

                    $main->include_main_blocks_2($this->module_name . "_show_users_groups.html", $this->tpl_path);
                    $tpl->prepare();

                    $this->main_title = 'Группы, на которые распространяется скидка &laquo;' .
                        htmlspecialchars(to_str($discount_row['name'])) . '&raquo;';

                    $tpl->newBlock('block_users_groups');
                    $tpl->assign(array(
                            'discount_id' => $request_discount_id,
                            'form_action' => $baseurl . '&action=update_users_groups'
                        )
                    );

                    $raw_rows = $this->GetGroupsWithDiscounts($request_discount_id);

                    // Преобразуем значения.
                    $rows = array();
                    foreach ($raw_rows as $row) {
                        $row['group_id'] = to_int($row['id']);
                        $row['selected'] = to_int($row['users_discound_id']) ? 'checked' : '';
                        $rows[] = $row;
                    }

                    //print_r($rows);

                    reset($rows);
                    foreach ($rows as $row) {
                        $tpl->newBlock("block_group_list");
                        $tpl->assign($row);
                    }
                    //print_r($raw_rows);

                    break;

                case 'update_users_groups':
                    global $request_discount_id;

                    $request_discount_id = to_int($request_discount_id);

                    // Очистим связь с группами данной скидки.
                    $this->ClearGroupsOfDiscount($request_discount_id);

                    // Список групп.
                    $rows = $this->GetGroupsWithDiscounts($request_discount_id);
                    $rows = to_array($rows);

                    foreach ($rows as $row) {
                        if (isset($_REQUEST['discount_group_' . to_int($row['id'])])) {
                            $this->InsertDiscountForGroup($request_discount_id, to_int($row['id']));
                        }
                    }

                    header('Location: ' . $baseurl . '&action=show_users_discounts');
                    break;

                case 'edit_discount':
                    global $request_discount_id;

                    $request_discount_id = to_int($request_discount_id);

                    if ($request_discount_id) {
                        // Редактирование скидки.
                        $this->main_title = 'Редактирование скидки';
                    } else {
                        // Создание новой скидки.
                        $this->main_title = 'Создание новой скидки';
                    }

                    $discount_row = $this->GetUsersDiscounts($request_discount_id);
                    $discount_row = to_array($discount_row[0]);

                    if (count($discount_row) > 0) {
                        $discount_row['is_percent'] = to_int($discount_row['is_percent']) ? 'checked' : '';
                    }

                    $main->include_main_blocks_2($this->module_name . "_edit_users_discount.html", $this->tpl_path);
                    $tpl->prepare();

                    $tpl->newBlock('block_edit_discount');
                    $tpl->assign(array(
                            'discount_id' => $request_discount_id,
                            'form_action' => $baseurl . '&action=update_discount',
                            'cancel_link' => $baseurl . '&action=show_users_discounts',
                        )
                    );

                    $tpl->assign($discount_row);
                    break;

                case 'update_discount':
                    global $request_discount_id, $request_discount_name, $request_discount_min,
                           $request_discount_max, $request_discount_interval, $request_discount_value;

                    $request_discount_id = to_int($request_discount_id);
                    $params = array();
                    $params['id'] = $request_discount_id;
                    $params['name'] = to_str($request_discount_name);
                    $params['min'] = to_float($request_discount_min);
                    $params['max'] = to_float($request_discount_max);
                    $params['value'] = to_float($request_discount_value);
                    $params['interval'] = to_int($request_discount_interval);
                    $params['is_percent'] = isset($_REQUEST['discount_is_percent']) ? 1 : 0;

                    $this->InsertDiscount($params);
                    header('Location: ' . $baseurl . '&action=show_users_discounts');
                    break;

                case 'update_discount_activity':
                    global $request_discount_id, $request_set_active;

                    $request_discount_id = to_int($request_discount_id);
                    $request_set_active = to_int($request_set_active);
                    if ($request_discount_id) {
                        $params = array();
                        $params['id'] = $request_discount_id;
                        $params['active'] = $request_set_active;
                        $this->InsertDiscount($params);
                    }

                    header('Location: ' . $baseurl . '&action=show_users_discounts');
                    break;

                case 'delete_discount':
                    global $request_discount_id;

                    $request_discount_id = to_int($request_discount_id);
                    if ($request_discount_id) {
                        $this->DeleteDiscount($request_discount_id);
                    }

                    header('Location: ' . $baseurl . '&action=show_users_discounts');
                    break;

                case 'show_stat':
                    global $request_period, $request_group;

                    $request_period = to_int($request_period);
                    $main->include_main_blocks_2($this->module_name . "_show_stat.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign('lang', $lang);
                    /*
					$tpl->newBlock('block_sales_graph');

					$block_params = array(
					    'MSG_Users_group' => $this->_msg['Users_group'],
					    'MSG_All_groups' => $this->_msg['All_groups'],
					    'MSG_Period' => $this->_msg['Period'],
					    'MSG_From2' => $this->_msg['From2'],
					    'MSG_To2' => $this->_msg['To2'],
					    'MSG_Show' => $this->_msg['Show'],
					    'MSG_All' => $this->_msg['All'],
					    'MSG_No_auth_user' => $this->_msg['No_auth_user'],
					    'graph_path' => '/mod/shop/stat_graph.php?lang='.$lang
					);

					$tpl->assign($block_params);
					
					$mod_user = Module::load_mod('siteusers', 'only_create_object');
					if ($mod_user) {
					    $mod_user->show_groups('block_groups');
					}
					*/
                    $this->main_title = $this->_msg['stat'];
                    break;

                case 'statparam':
                    echo $this->getStatParamXml();
                    exit;

                case 'statdata':
                    if ((!isset($_REQUEST['from']) && !isset($_REQUEST['to']))
                        || (!$_REQUEST['from'] && !$_REQUEST['to'])
                    ) {
                        $period = array(
                            'from' => date('d.m.Y', time() - (30 * 24 * 3600)),
                            'to' => date('d.m.Y')
                        );
                    } else {
                        $period = array(
                            'from' => $_REQUEST['from'],
                            'to' => $_REQUEST['to']
                        );
                    }
                    $ret = $this->GetOrderStat(
                        $period,
                        isset($_REQUEST['usertype']) ? $_REQUEST['usertype'] : -1,
                        isset($_REQUEST['status']) ? $_REQUEST['status'] : -1,
                        isset($_REQUEST['manager']) ? $_REQUEST['manager'] : -1);
                    echo "<result>\n";
                    if ($ret) {
                        foreach ($ret as $val) {
                            echo "<data><date>{$val['date']}</date><cnt>{$val['cnt']}</cnt><sum>{$val['sum']}</sum><cur>{$CONFIG['catalog_currencies'][$CONFIG['shop_order_concurency']]}</cur></data>\n";
                        }
                    }
                    echo "</result>\n";
                    exit;

                case 'prp':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_step;
                    $main->include_main_blocks('_properties.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Information" => $main->_msg["Information"],
                        "MSG_Value" => $main->_msg["Value"],
                        "MSG_Save" => $main->_msg["Save"],
                        "MSG_Cancel" => $main->_msg["Cancel"],
                    ));
                    if (!$request_step || $request_step == 1) {
                        $tpl->assign(array('form_action' => $baseurl,
                            'step' => 2,
                            'lang' => $lang,
                            'name' => $this->module_name,
                            'action' => 'prp'));
                        $prp_html = $main->getModuleProperties($this->module_name);
                        $this->main_title = $this->_msg["Controls"];
                    } elseif (2 == $request_step) {
                        $main->setModuleProperties($this->module_name, $_POST);
                        header('Location: ' . $baseurl . '&action=prp&step=1');
                    }
                    break;

                case 'block_prp':
                    global $request_sub_action, $request_block_id;
                    $arr = $this->getEditLink($request_sub_action, $request_block_id);
                    $cont = 'var container = document.getElementById(\'target_span\');
		    		  	 container.innerHTML = "' . $this->getPropertyFields() . '";
		    		  	 material_id		  = "' . $arr['material_id'] . '";
		    		  	 material_url		  = "' . $arr['material_url'] . '";
		    		  	 changeAction();
		    	      	 setURL("property1");';
                    header('Content-Length: ' . strlen($cont));
                    echo $cont;
                    exit;
                    break;

                case 'dsnt':
                    if (!$permissions['e']) $main->message_access_denied($siteusers->module_name, $action);
                    global $request_step, $request_start, $request_discount, $request_min, $request_max,
                           $request_discount_value, $request_is_percent, $request_id, $request_from_date, $request_to_date;
                    $main->include_main_blocks_2($this->module_name . "_discounts.html", $this->tpl_path);
                    $tpl->prepare();
                    if (!@$request_step || $request_step == 1) {
                        $tpl->newBlock('block_shop_discounts');
                        $tpl->assign(Array(
                            "MSG_Enter_discount_name" => $this->_msg["Enter_discount_name"],
                            "MSG_Enter_discount_criterion" => $this->_msg["Enter_discount_criterion"],
                            "MSG_Activ" => $this->_msg["Activ"],
                            "MSG_Ed" => $this->_msg["Ed"],
                            "MSG_Discount" => $this->_msg["Discount"],
                            "MSG_From" => $this->_msg["From"],
                            "MSG_To" => $this->_msg["To"],
                            "MSG_Magnitude" => $this->_msg["Magnitude"],
                            "MSG_Type" => $this->_msg["Type"],
                            "MSG_Del" => $this->_msg["Del"],
                            "MSG_From2" => $this->_msg["From2"],
                            "MSG_To2" => $this->_msg["To2"],
                        ));
                        $discounts = $this->getDiscountsAdm($baseurl, $request_start);
                        $discounts = is_array($discounts) ? $discounts : array();
                        foreach ($discounts as $k => $v) {
                            $tpl->newBlock("block_discounts_list");
                            $tpl->assign(Array(
                                "MSG_Edit" => $this->_msg["Edit"],
                                "MSG_Confirm_delete" => $this->_msg["Confirm_delete"],
                                "MSG_Delete" => $this->_msg["Delete"],
                            ));
                            $tpl->assign($v);
                        }
                        $tpl->newBlock('block_shop_discount_add');
                        $tpl->assign(Array(
                            "MSG_Sum" => $this->_msg["Sum"],
                            "MSG_Discount" => $this->_msg["Discount"],
                            "MSG_From" => $this->_msg["From"],
                            "MSG_To" => $this->_msg["To"],
                            "MSG_Magnitude" => $this->_msg["Magnitude"],
                            "MSG_Info_from_to" => $this->_msg["Info_from_to"],
                            "MSG_see" => $this->_msg["see"],
                            "MSG_module_controls" => $this->_msg["module_controls"],
                            "MSG_Period" => $this->_msg["Period"],
                            "MSG_From2" => $this->_msg["From2"],
                            "MSG_To2" => $this->_msg["To2"],
                        ));
                        $tpl->assign(array(
                            'action_title' => $this->_msg["Add_discount"],
                            'action_text' => $this->_msg["Add"],
                            'action_url' => $baseurl,
                            'name' => 'shop',
                            'action' => 'dsnt',
                            'step' => '2',
                        ));
                        $main->_show_nav_string($server . $lang . '_shop_discounts', '', '', 'action=' . $action, $request_start, $CONFIG['shop_max_rows'], $CONFIG['shop_order_by'], '');
                        $tpl->assign(Array(
                            "MSG_Pages" => $this->_msg["Pages"],
                        ));
                    } elseif ($request_step == 2) {
                        $this->addDiscounts($request_discount, $request_min, $request_max, $request_discount_value, $request_is_percent, $request_from_date, $request_to_date);
                        header('Location: ' . $baseurl . '&action=dsnt&step=1');
                    } elseif ($request_step == 3) {
                        $tpl->newBlock('block_shop_discounts');
                        $tpl->assign(Array(
                            "MSG_Enter_discount_name" => $this->_msg["Enter_discount_name"],
                            "MSG_Enter_discount_criterion" => $this->_msg["Enter_discount_criterion"],
                            "MSG_Activ" => $this->_msg["Activ"],
                            "MSG_Ed" => $this->_msg["Ed"],
                            "MSG_Discount" => $this->_msg["Discount"],
                            "MSG_From" => $this->_msg["From"],
                            "MSG_To" => $this->_msg["To"],
                            "MSG_Magnitude" => $this->_msg["Magnitude"],
                            "MSG_Type" => $this->_msg["Type"],
                            "MSG_Del" => $this->_msg["Del"],
                            "MSG_Period" => $this->_msg["Period"],
                            "MSG_From2" => $this->_msg["From2"],
                            "MSG_To2" => $this->_msg["To2"],
                        ));
                        $discounts = $this->getDiscountsAdm($baseurl, $request_start);
                        foreach ($discounts as $k => $v) {
                            $tpl->newBlock("block_discounts_list");
                            $tpl->assign(Array(
                                "MSG_Edit" => $this->_msg["Edit"],
                                "MSG_Confirm_delete" => $this->_msg["Confirm_delete"],
                                "MSG_Delete" => $this->_msg["Delete"],
                            ));
                            $tpl->assign($v);
                        }
                        $tpl->newBlock('block_shop_discount_add');
                        $tpl->assign(Array(
                            "MSG_Sum" => $this->_msg["Sum"],
                            "MSG_Discount" => $this->_msg["Discount"],
                            "MSG_From" => $this->_msg["From"],
                            "MSG_To" => $this->_msg["To"],
                            "MSG_Magnitude" => $this->_msg["Magnitude"],
                            "MSG_Info_from_to" => $this->_msg["Info_from_to"],
                            "MSG_see" => $this->_msg["see"],
                            "MSG_module_controls" => $this->_msg["module_controls"],
                            "MSG_Period" => $this->_msg["Period"],
                            "MSG_From2" => $this->_msg["From2"],
                            "MSG_To2" => $this->_msg["To2"],
                        ));
                        $tpl->assign(array(
                            'action_title' => 'Изменить скидку',
                            'action_text' => 'Изменить',
                            'action_url' => $baseurl,
                            'name' => 'shop',
                            'action' => 'dsnt',
                            'step' => '4',
                        ));
                        $tpl->assign($this->getDiscount($request_id));
                        $main->_show_nav_string($server . $lang . '_shop_discounts', '', '', 'action=' . $action, $request_start, $CONFIG['shop_max_rows'], $CONFIG['shop_order_by'], '');
                    } elseif ($request_step == 4) {
                        if (!$request_start) $request_start = 1;
                        $this->updateDiscount($request_id, $request_discount, $request_min, $request_max, $request_discount_value, $request_is_percent, $request_from_date, $request_to_date);
                        header('Location: ' . $baseurl . '&action=dsnt&step=1&start=' . $request_start);
                    }
                    $this->main_title = $this->_msg["Sum_discounts"];
                    break;

// Включение скидки
                case 'dsnt_a':
                    if (!$permissions['e']) $main->message_access_denied($siteusers->module_name, $action);
                    global $request_start, $request_id;
                    if (!$request_start) $request_start = 1;
                    $this->setDiscountActive($request_id);
                    header('Location: ' . $baseurl . '&action=dsnt&step=1&start=' . $request_start);
                    break;

// Отключение скидки
                case 'dsnt_d':
                    if (!$permissions['e']) $main->message_access_denied($siteusers->module_name, $action);
                    global $request_start, $request_id;
                    if (!$request_start) $request_start = 1;
                    $this->setDiscountUnActive($request_id);
                    header('Location: ' . $baseurl . '&action=dsnt&step=1&start=' . $request_start);
                    break;

// Удаление скидки
                case 'dsnt_del':
                    if (!$permissions['d']) $main->message_access_denied($siteusers->module_name, $action);
                    global $request_id;
                    $this->delDiscount($request_id);
                    header('Location: ' . $baseurl . '&action=dsnt&step=1');
                    break;

// Список налогов
                case 'taxes':
                    if (!$permissions['e']) $main->message_access_denied($siteusers->module_name, $action);
                    global $request_step, $request_start, $request_tax, $request_percent, $request_rank, $request_id;
                    $main->include_main_blocks_2($this->module_name . "_taxes.html", $this->tpl_path);
                    $tpl->prepare();
                    if (!@$request_step || $request_step == 1) {
                        $tpl->newBlock('block_shop_taxes');
                        $tpl->assign(Array(
                            "MSG_Enter_tax_name" => $this->_msg["Enter_tax_name"],
                            "MSG_Enter_tax_value" => $this->_msg["Enter_tax_value"],
                            "MSG_Enter_tax_rank" => $this->_msg["Enter_tax_rank"],
                            "MSG_Activ" => $this->_msg["Activ"],
                            "MSG_Ed" => $this->_msg["Ed"],
                            "MSG_Tax" => $this->_msg["Tax"],
                            "MSG_Magnitude" => $this->_msg["Magnitude"],
                            "MSG_Rank" => $this->_msg["Rank"],
                            "MSG_Del" => $this->_msg["Del"],
                        ));
                        $taxes = $this->getTaxesAdm($baseurl, $request_start);
                        if (!is_array($taxes)) {
                            $taxes = array();
                        }
                        foreach ($taxes as $k => $v) {
                            $tpl->newBlock("block_tax_list");
                            $tpl->assign(Array(
                                "MSG_Edit" => $this->_msg["Edit"],
                                "MSG_Confirm_delete" => $this->_msg["Confirm_delete"],
                                "MSG_Delete" => $this->_msg["Delete"],
                            ));
                            $tpl->assign($v);
                        }
                        $tpl->newBlock('block_shop_tax_add');
                        $tpl->assign(Array(
                            "MSG_Name" => $this->_msg["Name"],
                            "MSG_Rank" => $this->_msg["Rank"],
                            "MSG_Create" => $this->_msg["Create"],
                        ));
                        $tpl->assign(array(
                            'action_title' => $this->_msg["Add_tax"],
                            'action_text' => $this->_msg["Add"],
                            'action_url' => $baseurl,
                            'name' => 'shop',
                            'action' => 'taxes',
                            'step' => '2',
                        ));
                        $main->_show_nav_string($server . $lang . '_shop_taxes', '', '', 'action=' . $action, $request_start, $CONFIG['shop_max_rows'], $CONFIG['shop_order_by'], '');
                        $tpl->assign(Array(
                            "MSG_Pages" => $this->_msg["Pages"],
                        ));
                    } elseif ($request_step == 2) {
                        $this->addTax($request_tax, $request_percent, $request_rank);
                        header('Location: ' . $baseurl . '&action=taxes&step=1');
                    } elseif ($request_step == 3) {
                        $tpl->newBlock('block_shop_taxes');
                        $tpl->assign(Array(
                            "MSG_Enter_tax_name" => $this->_msg["Enter_tax_name"],
                            "MSG_Enter_tax_value" => $this->_msg["Enter_tax_value"],
                            "MSG_Enter_tax_rank" => $this->_msg["Enter_tax_rank"],
                            "MSG_Activ" => $this->_msg["Activ"],
                            "MSG_Ed" => $this->_msg["Ed"],
                            "MSG_Tax" => $this->_msg["Tax"],
                            "MSG_Magnitude" => $this->_msg["Magnitude"],
                            "MSG_Rank" => $this->_msg["Rank"],
                            "MSG_Del" => $this->_msg["Del"],
                        ));
                        $taxes = $this->getTaxesAdm($baseurl, $request_start);
                        foreach ($taxes as $k => $v) {
                            $tpl->newBlock("block_tax_list");
                            $tpl->assign(Array(
                                "MSG_Edit" => $this->_msg["Edit"],
                                "MSG_Confirm_delete" => $this->_msg["Confirm_delete"],
                                "MSG_Delete" => $this->_msg["Delete"],
                            ));
                            $tpl->assign($v);
                        }
                        $tpl->newBlock('block_shop_tax_add');
                        $tpl->assign(Array(
                            "MSG_Name" => $this->_msg["Name"],
                            "MSG_Rank" => $this->_msg["Rank"],
                            "MSG_Create" => $this->_msg["Create"],
                        ));
//					$arr = $this->getTax($request_id);
                        $tpl->assign(array(
                            'action_title' => $this->_msg["Change_tax"],
                            'action_text' => $this->_msg["Change"],
                            'action_url' => $baseurl,
                            'name' => 'shop',
                            'action' => 'taxes',
                            'step' => '4',
                        ));
                        $tpl->assign($this->getTax($request_id));
                        $main->_show_nav_string($server . $lang . '_shop_taxes', '', '', 'action=' . $action, $request_start, $CONFIG['shop_max_rows'], $CONFIG['shop_order_by'], '');
                    } elseif ($request_step == 4) {
                        if (!$request_start) $request_start = 1;
                        $this->updateTax($request_id, $request_tax, $request_percent, $request_rank);
                        header('Location: ' . $baseurl . '&action=taxes&step=1&start=' . $request_start);
                    }
                    $this->main_title = $this->_msg["Taxes"];
                    break;

// Включение налога
                case 'taxes_act':
                    if (!$permissions['e']) $main->message_access_denied($siteusers->module_name, $action);
                    global $request_start, $request_id;
                    if (!$request_start) $request_start = 1;
                    $this->setTaxActive($request_id);
                    header('Location: ' . $baseurl . '&action=taxes&step=1&start=' . $request_start);
                    break;

// Отключение налога
                case 'taxes_deac':
                    if (!$permissions['e']) $main->message_access_denied($siteusers->module_name, $action);
                    global $request_start, $request_id;
                    if (!$request_start) $request_start = 1;
                    $this->setTaxUnActive($request_id);
                    header('Location: ' . $baseurl . '&action=taxes&step=1&start=' . $request_start);
                    break;

// Удаление налога
                case 'taxes_del':
                    if (!$permissions['d']) $main->message_access_denied($siteusers->module_name, $action);
                    global $request_id;
                    $this->delTax($request_id);
                    header('Location: ' . $baseurl . '&action=taxes&step=1');
                    break;

                case 'getprods':
                    $cat_id = $_POST['cat'];
                    switch ($_POST['type']) {
                        case 0 :
                            $name = "product_id";
                            break;
                        case 1 :
                            $name = "product_id1[]";
                            break;
                        case 2 :
                            $name = "product_id2[]";
                            break;
                        case 3 :
                            $name = "gift_id";
                            break;
                        case 4 :
                            $name = "product_id[]";
                            break;
                        case 5 :
                            $name = "gift_id_if[]";
                            break;
                    }
                    if ($_POST['type'] == "checkbox") {
                        $json['content'] = '<br><div class="checkDiv">';
                        $prods = $this->getProducts($cat_id);
                        foreach ($prods as $prod) {
                            $json['content'] = $json['content'] . $prod['title'] . '<input type="checkbox" class="checkV" name="items[]" value="' . $prod['id'] . '"><br>';
                        }
                        $json['content'] = $json['content'] . '</div>';
                        echo json_encode($json);
                        exit;
                    } else {
                        $json['content'] = '<select style="margin:3px" name="' . $name . '" onchange="selectProd(this.value,this)">';
                        $prods = $this->getProducts($cat_id);
                        foreach ($prods as $prod) {
                            $json['content'] = $json['content'] . '<option value="' . $prod['id'] . '">' . $prod['title'] . '</option>';
                        }
                        $json['content'] = $json['content'] . '</select>';
                        echo json_encode($json);
                        exit;

                    }
                    break;

                case 'add_skidka':
                    global $db;
                    $title = $db->escape($_POST['title']);
                    $type = (int)$_POST['type'];
                    $percent = 0;
                    $percent_p = 0;
                    if ($type == 1) $percent = (int)$_POST['percent'];
                    $price = 0;
                    if ($type == 2) $price = (int)$_POST['percent1'];

                    if ($type == 3) $price = (int)$_POST['price'];
                    if ($type == 3){
                        $percent = (int)$_POST['price_p'];
                        $percent_p = $percent ? 1 : 0;
                    }
                    $rank = $this->getMaxRank() + 1;
                    $query = "INSERT INTO " . $server . $lang . "_skidki (type,percent,percent_p,price,title,rank) VALUES ($type, $percent,$percent_p, $price, '$title',$rank)";
                    if (!$result = mysql_query($query)) die(mysql_error());
                    $id = mysql_insert_id();

                    if ($type == 1) {
                        $prod_id = (int)$_POST['product_id'];
                        if ($prod_id) {
                            $check = 0;
                            $old_price = $_POST['old_price_value'] ? $_POST['old_price_value'] : 0;
                            if ($_POST['old_price_check']) {
                                $query = "Update " . $server . $lang . "_catalog_products Set price_1=$old_price Where id=" . $prod_id;
                                mysql_query($query);
                                $check = 1;
                            };
                            $query = "INSERT INTO " . $server . $lang . "_skidki_prods (skidka_id, prod_id,old_price_check,old_price_value) VALUES ($id, $prod_id,$check,$old_price)";
                            if (!$result = mysql_query($query)) die(mysql_error());
                        }
                    } elseif ($type == 2) {
                        foreach ($_POST['product_id1'] as $idx) {
                            $prod_id = (int)$idx;
                            if ($prod_id) {
                                $query = "INSERT INTO " . $server . $lang . "_skidki_prods (skidka_id, prod_id) VALUES ($id, $prod_id)";
                                if (!$result = mysql_query($query)) die(mysql_error());
                            }
                        }
                    } elseif ($type == 3) {
                        $o=0;
                        foreach ($_POST['product_id2'] as $idx) {
                            $prod_id = (int)$idx;
                            $check=0;
                            $old_price='';
                            if($o ==0 && ++$o){
                                $check = 0;
                                $old_price = $_POST['old_price_value_p'] ? $_POST['old_price_value_p'] : 0;
                                if ($_POST['old_price_check_p'] && $old_price) {
                                    $query = "Update " . $server . $lang . "_catalog_products Set price_1=$old_price Where id=" . $prod_id;
                                    mysql_query($query);
                                    $check = 1;
                                };
                            }

                            if ($prod_id) {
                                $query = "INSERT INTO " . $server . $lang . "_skidki_prods (skidka_id, prod_id,old_price_check,old_price_value) VALUES ($id, $prod_id,$check,$old_price)";
                                if (!$result = mysql_query($query)) die(mysql_error());
                            }
                        }
                        $prod_id = (int)$_POST['gift_id'];
                        if ($prod_id) {
                            $query = "INSERT INTO " . $server . $lang . "_skidki_prods (skidka_id, prod_id, is_gift) VALUES ($id, $prod_id, 1)";
                            if (!$result = mysql_query($query)) die(mysql_error());
                        }
                        $prod_id_if = (int)$_POST['gift_id_if'];

                        if ($prod_id_if && !$price) {

                            $query = "INSERT INTO " . $server . $lang . "_skidki_prods (skidka_id, prod_id, is_gift, is_if) VALUES ($id, $prod_id_if, 1, 1)";
                            if (!$result = mysql_query($query)) die(mysql_error());
                        }

                        //if(isset)


                    }
                    header('Location: ' . $baseurl . '&action=skidki');
                    exit;

                    break;
                case 'update_skidka':
                    global $db;
                    $title = $db->escape($_POST['title']);
                    $type = (int)$_POST['type'];
                    $id = (int)$_POST['id'];
                    // $old_id = $_POST['old_id'];
                    // $old_prod_id = $_POST['old_prod_id'];
                    $percent = 0;
                    $percent_p = 0;
                    if ($type == 1) $percent = (int)$_POST['percent'];
                    $price = 0;
                    if ($type == 2) $price = (int)$_POST['percent1'];

                    if ($type == 3) $price = (int)$_POST['price'];

                    if ($type == 3){
                        $percent = (int)$_POST['price_p'];
                        $percent_p = $percent ? 1 : 0;
                    }

                    $query = "UPDATE " . $server . $lang . "_skidki SET type=$type,percent=$percent,price=$price,title='$title', percent_p = $percent_p WHERE id=$id";
                    if (!$result = mysql_query($query)) die(mysql_error());
                    if ($type == 1) {
                        $prod_id = (int)$_POST['product_id'];
                        if ($prod_id) {
                            $check = 0;
                            $old_price = $_POST['old_price_value'];

                            if ($_POST['old_price_check']) {
                                $query = "Update " . $server . $lang . "_catalog_products Set price_1=$old_price Where id=" . $prod_id;
                                mysql_query($query);
                                $check = 1;
                            };
                            $query = "Delete FROM " . $server . $lang . "_skidki_prods WHERE skidka_id=$id";
                            if (!$result = mysql_query($query)) die(mysql_error());
                            $query = "INSERT INTO " . $server . $lang . "_skidki_prods (skidka_id, prod_id,old_price_check,old_price_value) VALUES ($id, $prod_id,$check,$old_price)";
                            if (!$result = mysql_query($query)) die(mysql_error());
                        }
                    } elseif ($type == 2) {
                        $query = "Delete FROM " . $server . $lang . "_skidki_prods WHERE skidka_id=$id";
                        if (!$result = mysql_query($query)) die(mysql_error());
                        foreach ($_POST['product_id1'] as $idx) {
                            $prod_id = (int)$idx;
                            if ($prod_id) {
                                $query = "INSERT INTO " . $server . $lang . "_skidki_prods (skidka_id, prod_id) VALUES ($id, $prod_id)";
                                if (!$result = mysql_query($query)) die(mysql_error());
                            }
                        }
                    } elseif ($type == 3) {
                        $query = "Delete FROM " . $server . $lang . "_skidki_prods WHERE skidka_id=$id";
                        if (!$result = mysql_query($query)) die(mysql_error());
                        $o=0;
                        foreach ($_POST['product_id2'] as $idx) {
                            $prod_id = (int)$idx;

                            $check=0;
                            $old_price='';
                            if($o ==0 && ++$o){
                                $check = 0;
                                $old_price = $_POST['old_price_value_p'] ? $_POST['old_price_value_p'] : 0;
                                if ($_POST['old_price_check_p'] && $old_price) {
                                    $query = "Update " . $server . $lang . "_catalog_products Set price_1=$old_price Where id=" . $prod_id;
                                    mysql_query($query);
                                    $check = 1;
                                };
                            }

                            if ($prod_id) {
                                $query = "INSERT INTO " . $server . $lang . "_skidki_prods (skidka_id, prod_id,old_price_check,old_price_value) VALUES ($id, $prod_id,$check,$old_price)";
                                if (!$result = mysql_query($query)) die(mysql_error());
                            }
                        }
                        $prod_id = (int)$_POST['gift_id'];
                        if ($prod_id) {
                            $comment = $_POST['gift_comment'];
                            $query = "INSERT INTO " . $server . $lang . "_skidki_prods (skidka_id, prod_id, is_gift, comment) VALUES ($id, $prod_id, 1, '$comment')";
                            if (!$result = mysql_query($query)) die(mysql_error());
                        }
                        $i = 0;
                        foreach ($_POST['gift_id_if'] as $prod_id_if) {
                            $prod_id_if = (int)$prod_id_if;

                            if ($prod_id_if && !$price) {

                                $query = "INSERT INTO " . $server . $lang . "_skidki_prods (skidka_id, prod_id, is_gift, is_if, comment) VALUES ($id, $prod_id_if, 1, 1, '" . $_POST['gift_if_comment'][$i] . "')";
                                if (!$result = mysql_query($query)) die(mysql_error());
                            }
                            $i++;
                        }
                    }
                    header('Location: ' . $baseurl . '&action=skidki');
                    exit;
                    break;

                case 'showskidka':
                    $main->include_main_blocks_2($this->module_name . "_skidka.html", $this->tpl_path);
                    $tpl->prepare();
                    $skidka = $this->getSkidka($_GET['id']);
                    $tpl->assign('type_title', $this->skidka_types[$skidka['type']]);
                    $tpl->newBlock('type' . $skidka['type']);
                    $tpl->assign('pp', $skidka['price']);
                    $tpl->assign('price', $skidka['price']);
                    $tpl->assign('percent', $skidka['percent']);
                    foreach ($skidka['prods'] as $prod) {
                        if ($prod['is_gift']) $tpl->newBlock('gift');
                        else $tpl->newBlock('prod' . $skidka['type']);
                        $tpl->assign($prod);
                    }

                    $this->main_title = 'Скидка «' . $skidka['title'] . '»';
                    break;
                case 'actions':
                    $main->include_main_blocks_2($this->module_name . "_actions.html", $this->tpl_path);
                    $tpl->prepare();
                    $start = (int)$_GET['start'];
                    if ($start < 1) $start = 1;
                    $num = 20;
                    $st = ($start - 1) * $num;

                    $query = 'SELECT * FROM ' . $server . $lang . '_actions ORDER BY id DESC LIMIT ' . $st . ',' . $num;

                    if (!$result = mysql_query($query)) die(mysql_error());

                    while ($row = mysql_fetch_assoc($result)) {
                        $tpl->newBlock('action');
                        $tpl->assign(array(
                            'id' => $row['id'],
                            'title' => $row['title'],
                            'edit_link' => $baseurl . '&action=editaction&id=' . $row['id'],
                            'type_title' => $this->skidka_types[$row['type']],
                            'del_link' => $baseurl . '&action=delaction&id=' . $row['id']
                        ));
                    }

                    $main->_show_nav_string($server . $lang . "_actions", "", "", "action=" . $action, $start, $num, "id DESC", "", 1, 0);

                    $this->main_title = 'Акции';
                    break;
                case 'add_action':
                    $main->include_main_blocks_2($this->module_name . "_action.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign('action', $baseurl . '&action=addaction');
                    $query = 'SELECT * FROM ' . $server . $lang . '_skidki ORDER BY id DESC';
                    if (!$result = mysql_query($query)) die(mysql_error());

                    while ($row = mysql_fetch_assoc($result)) {
                        $tpl->newBlock('skidka');
                        $tpl->assign(array(
                            'id' => $row['id'],
                            'title' => $row['title'],

                        ));
                    }
                    $this->main_title = 'Создание акции';
                    break;

                case 'editaction':
                    $main->include_main_blocks_2($this->module_name . "_action.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign('action', $baseurl . '&action=updateaction');

                    $_action = $this->getAction($_GET['id']);
                    $tpl->assign('id', $_action['id']);
                    $tpl->assign(array(
                        'title' => $_action['title'],
                        'text' => $_action['text'],
                        'preview' => $_action['preview'],
                        'img' => $_action['img'],
                        'date_from' => $_action['time_from'] ? date('d.m.Y', $_action['time_from']) : '',
                        'date_to' => $_action['time_to'] ? date('d.m.Y', $_action['time_to']) : '',
                    ));
                    if ($_action['img']) {
                        $tpl->newBlock('img');
                        $tpl->assign('img', $_action['img']);
                    }

                    $query = 'SELECT * FROM ' . $server . $lang . '_skidki ORDER BY id DESC';
                    if (!$result = mysql_query($query)) die(mysql_error());

                    while ($row = mysql_fetch_assoc($result)) {
                        $tpl->newBlock('skidka');
                        $tpl->assign(array(
                            'id' => $row['id'],
                            'title' => $row['title'],
                            'checked' => in_array($row['id'], $_action['skidki']) ? 'checked' : ''
                        ));
                    }
                    $this->main_title = 'Редактирование акции';
                    break;
                case 'updateaction':
                    global $db;
                    $id = (int)$_POST['id'];
                    $title = $db->escape($_POST['title']);
                    $text = $db->escape($_POST['text']);
                    $img = $db->escape($_POST['img']);
                    $preview = $db->escape($_POST['preview']);
                    $arr = explode('.', $_POST['date']);
                    $time_from = strtotime($arr[2] . '-' . $arr[1] . '-' . $arr[0] . ' 00:00:01');
                    $arr = explode('.', $_POST['date_to']);
                    $time_to = strtotime($arr[2] . '-' . $arr[1] . '-' . $arr[0] . ' 00:00:01');
                    $query = "UPDATE " . $server . $lang . "_actions SET title = '$title',text = '$text',img = '$img',time_from = '$time_from', time_to='$time_to', preview='$preview' WHERE id=" . $id;
                    if (!mysql_query($query)) die(mysql_error());
                    $query = "DELETE FROM " . $server . $lang . "_actions_skidki WHERE action_id=" . $id;
                    if (!mysql_query($query)) die(mysql_error());
                    foreach ($_POST['skidka'] as $idx) {
                        $idx = (int)$idx;
                        if ($idx) {
                            $query = "INSERT INTO " . $server . $lang . "_actions_skidki (action_id, skidka_id) VALUES ($id, $idx)";
                            if (!mysql_query($query)) die(mysql_error());
                        }
                    }
                    header('Location: ' . $baseurl . '&action=actions');
                    exit;
                    break;

                case 'addaction':
                    global $db;
                    $title = $db->escape($_POST['title']);
                    $text = $db->escape($_POST['text']);
                    $img = $db->escape($_POST['img']);
                    $arr = explode('.', $_POST['date']);
                    $preview = $db->escape($_POST['preview']);
                    $time_from = strtotime($arr[2] . '-' . $arr[1] . '-' . $arr[0] . ' 00:00:01');
                    $arr = explode('.', $_POST['date_to']);
                    $time_to = strtotime($arr[2] . '-' . $arr[1] . '-' . $arr[0] . ' 00:00:01');
                    $query = "INSERT INTO " . $server . $lang . "_actions (title,text,img,time_from,time_to,preview) VALUES ('$title', '$text', '$img', '$time_from', '$time_to','$preview')";
                    if (!mysql_query($query)) die($query . ' # ' . mysql_error());
                    $id = mysql_insert_id();
                    foreach ($_POST['skidka'] as $idx) {
                        $idx = (int)$idx;
                        if ($idx) {
                            $query = "INSERT INTO " . $server . $lang . "_actions_skidki (action_id, skidka_id) VALUES ($id, $idx)";
                            if (!mysql_query($query)) die($query . ' # ' . mysql_error());
                        }
                    }
                    header('Location: ' . $baseurl . '&action=actions');
                    exit;
                    break;

                case 'delaction':
                    $id = (int)$_GET['id'];
                    $query = 'DELETE FROM ' . $server . $lang . '_actions WHERE id=' . $id;
                    if (!$result = mysql_query($query)) die(mysql_error());
                    $query = 'DELETE FROM ' . $server . $lang . '_actions_skidki WHERE action_id=' . $id;
                    if (!$result = mysql_query($query)) die(mysql_error());
                    header('Location: ' . $baseurl . '&action=actions');
                    exit;
                    break;

                case "skidki":
                    if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
                    global $request_start, $request_clrfilter, $baseurl;
                    $start = ($request_start) ? $request_start : 1;
                    $main->include_main_blocks_2($this->module_name . "_skidki.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "lang" => $lang,
                        "start" => $start,
                        "MSG_Customer" => $this->_msg["Customer"],

                    ));

                    $root_cats = $this->getCategories(1);
                    $add = '&nbsp;&nbsp;&nbsp;';
                    foreach ($root_cats as $root) {
                        $sep = '';
                        $this->showCat($root, $sep);
                        $sep1 = $add . $sep;
                        $cats1 = $this->getCategories($root['id']);
                        if (!sizeof($cats1)) {
                            //$products = $this->getProducts($root['id']);
                            //$this->showProducts($products, $sep1);
                        } else {
                            foreach ($cats1 as $cat1) {
                                $this->showCat($cat1, $sep1);
                                $sep2 = $add . $sep1;
                                $cats2 = $this->getCategories($cat1['id']);
                                if (!sizeof($cats2)) {
                                    //$products = $this->getProducts($cat1['id']);
                                    //$this->showProducts($products, $sep2);
                                } else {
                                    foreach ($cats2 as $cat2) {
                                        $this->showCat($cat2, $sep2);
                                        $sep3 = $add . $sep2;
                                        $cats3 = $this->getCategories($cat2['id']);
                                        if (!sizeof($cats3)) {
                                            //$products = $this->getProducts($cat2['id']);
                                            //$this->showProducts($products, $sep3);
                                        } else {
                                            foreach ($cats3 as $cat3) {
                                                $this->showCat($cat3, $sep3);
                                                $sep4 = $add . $sep3;
                                                $cats4 = $this->getCategories($cat3['id']);
                                                if (!sizeof($cats4)) {
                                                    //$products = $this->getProducts($cat3['id']);
                                                    //$this->showProducts($products, $sep4);
                                                } else {
                                                    foreach ($cats4 as $cat4) {
                                                        $this->showCat($cat4, $sep4);
                                                        //$sep = '  '.$sep;
                                                        //$cats3 = $this->getCategories($parent_id);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }


                            }
                        }

                    }
                    $start = (int)$_GET['start'];
                    if ($start < 1) $start = 1;
                    $num = 20;
                    $st = ($start - 1) * $num;

                    $query = 'SELECT * FROM ' . $server . $lang . '_skidki ORDER BY rank DESC LIMIT ' . $st . ',' . $num;
                    if (!$result = mysql_query($query)) die(mysql_error());
                    $labelFirst = false;
                    $isFirst = 0;
                    while ($row = mysql_fetch_assoc($result)) {
                        if (!$labelFirst && ($start == 0 || $start == 1)) {
                            $maxRank = $row['rank'];
                            $labelFirst = true;
                            $isFirst = 1;
                        }
                        if ($row['rank'] == $this->getMinRank()) {
                            $isLast = 1;
                            $img_up = "ico_listup.gif";
                            $img_down = "ico_ligrdw.gif";

                        } else {
                            $isLast = 0;
                            $img_up = "ico_listup.gif";
                            $img_down = "ico_listdw.gif";
                            if ($isFirst && ($start == 0 || $start == 1)) $img_up = "ico_ligrup.gif"; //$img_down = "ico_listdw.gif";
                        }


                        $tpl->newBlock('skidka');
                        $tpl->assign(array(
                            'id' => $row['id'],
                            'title' => $row['title'],
                            'link' => $baseurl . '&action=showskidka&id=' . $row['id'],
                            'type_title' => $this->skidka_types[$row['type']],
                            'rank' => $row['rank'],
                            'is_first' => $isFirst,
                            'is_last' => $isLast,
                            'img_up' => $img_up,
                            'type' => $row["type"],
                            'img_dwn' => $img_down,
                            "discount_up_link" => "$baseurl&action=discountup&id=" . $row['id'] . "&rank=" . $row["rank"],
                            "discount_down_link" => "$baseurl&action=discountdown&id=" . $row['id'] . "&rank=" . $row["rank"],
                            'del_link' => $baseurl . '&action=delskidka&id=' . $row['id'],
                            'edit_link' => $baseurl . '&action=edit_discount&id=' . $row['id']
                        ));
                        $isFirst = 0;

                    }

                    $main->_show_nav_string($server . $lang . "_skidki", "", "", "action=" . $action, $start, $num, "id DESC", "", 1, 0);

                    $this->main_title = 'Скидки';
                    break;

                case 'getEditData':
                    global $db;
                    $id = $_POST['id'];
                    $db->query("SELECT s.prod_id,s.is_gift,s.is_if,s.old_price_value,s.old_price_check,s.comment,p.category_id,sk.type, sk.percent, sk.price,p.price_1 FROM " . $server . $lang . "_skidki_prods AS s
                                LEFT JOIN " . $server . $lang . "_catalog_products AS p ON s.prod_id= p.id
                                LEFT JOIN " . $server . $lang . "_skidki AS sk ON sk.id = s.skidka_id
                                WHERE s.skidka_id={$id}
                                ");
                    $j = 0;
                    $is_if_count = 0;
                    for ($i = 0; $i < $db->nf(); $i++) {
                        $db->next_record();
                        //$json['content'] = $json['content'].'<select style="margin:3px" name="'.$name.'">';
                        $prods = $this->getProducts($db->f('category_id'));
                        $json['percent'][] = $db->f('percent');
                        $json['price'][] = $db->f('price');
                        $json['price_1'][] = $db->f('price_1');
                        $json['old_price_value'][] = $db->f('old_price_value');
                        $json['old_price_check'][] = $db->f('old_price_check');
                        if ($db->f('is_gift')) {
                            if (!$db->f('is_if')) {
                                $labelGift = true;
                                $json['gift_cat'] = $db->f('category_id');
                                $json['gift'] = '';//<select onchange="loadProds(this.value, 3, this)" name="gift">
                                foreach ($prods as $prod) {
                                    if ($db->f('prod_id') == $prod['id']) {
                                        $json['gift'] = $json['gift'] . '<option selected="selected" value="' . $prod['id'] . '">' . $prod['title'] . '</option>';
                                    } else {
                                        $json['gift'] = $json['gift'] . '<option value="' . $prod['id'] . '">' . $prod['title'] . '</option>';
                                    }

                                }
                                $json['gift_comment'] = $db->f('comment');
                            } else {
                                $labelGiftIf = true;
                                $json['gift_cat_if'][] = "";
                                $json['gift_if'][] = "";
                                $json['gift_cat_if'][$is_if_count] = $db->f('category_id');
                                $json['gift_if'][$is_if_count] = '';//<select onchange="loadProds(this.value, 3, this)" name="gift">
                                foreach ($prods as $prod) {
                                    if ($db->f('prod_id') == $prod['id']) {
                                        $json['gift_if'][$is_if_count] = $json['gift_if'][$is_if_count] . '<option selected="selected" value="' . $prod['id'] . '">' . $prod['title'] . '</option>';
                                    } else {
                                        $json['gift_if'][$is_if_count] = $json['gift_if'][$is_if_count] . '<option value="' . $prod['id'] . '">' . $prod['title'] . '</option>';
                                    }


                                }
                                $json['gift_if_comment'][$is_if_count] = $db->f('comment');
                                $is_if_count++;
                            }

                            //$json['content'] = $json['content'].'</select>';

                        } else {
                            $json['category'][] = $db->f('category_id');
                            foreach ($prods as $prod) {
                                if ($db->f('prod_id') == $prod['id']) {
                                    $json['content'][$j] = $json['content'][$j] . '<option selected="selected" value="' . $prod['id'] . '">' . $prod['title'] . '</option>';
                                } else {
                                    $json['content'][$j] = $json['content'][$j] . '<option value="' . $prod['id'] . '">' . $prod['title'] . '</option>';
                                }

                            }
                            $j++;
                            //$json['content'] = $json['content'].'</select>';
                        }


                    }
                    $json['count'] = $db->nf();
                    if (isset($labelGift)) $json['count'] = $json['count'] - 1;
                    if (isset($labelGiftIf)) $json['count'] = $json['count'] - $is_if_count;
                    echo json_encode($json);
                    exit;
                    break;

                case 'delskidka':
                    $id = (int)$_GET['id'];
                    $query = 'DELETE FROM ' . $server . $lang . '_skidki WHERE id=' . $id;
                    if (!$result = mysql_query($query)) die(mysql_error());
                    $query = 'DELETE FROM ' . $server . $lang . '_skidki_prods WHERE skidka_id=' . $id;
                    if (!$result = mysql_query($query)) die(mysql_error());
                    header('Location: ' . $baseurl . '&action=skidki');
                    exit;
                    break;

// Список заказов
                case "orders":
                    if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
                    global $request_start, $request_clrfilter, $db, $request_search, $request_message;
                    $start = ($request_start) ? $request_start : 1;
                    $main->include_main_blocks_2($this->module_name . "_orders.html", $this->tpl_path);
                    $tpl->prepare();
                    if ($request_search) $request_message = "Результат поиска заказа по запросу '" . $request_search . "'";
                    $tpl->assign(Array(
                        "MSG_ID" => $this->_msg["ID"],
                        "MSG_Date" => $this->_msg["Date"],
                        "MSG_Customer" => $this->_msg["Customer"],
                        "MSG_Email" => $this->_msg["Email"],
                        "MSG_Sum" => $this->_msg["Sum"],
                        "MSG_Discount" => $this->_msg["Discount"],
                        "MSG_Currency" => $this->_msg["Currency"],
                        "MSG_Order_status" => $this->_msg["Order_status"],
                        "MSG_Orders_status" => $this->_msg["Orders_status"],
                        "MSG_Responsible" => $this->_msg["Responsible"],
                        "MSG_Del" => $this->_msg["Del"],
                        "baseurl" => $baseurl,
                        "message" => $request_message,
                    ));
                    if (isset($request_clrfilter)) {
                        unset($_SESSION['shop_fltr']);
                    }
                    $this->show_orders($baseurl, $start);
                    // Функция для разбивки записей по страницам и вывода навигации по ним
                    // Имена полей и их назначение :
                    // ($table = таблица, $where = условие выборки, $blockname = имя блока,
                    // $action = добавка к урл, $start = текущая страница/начало выборки,
                    // $rows = кол-во строк, $order_by = порядок выборки, $separator = разделитель)
                    $where = $this->getFilterCondition();
                    $where = $where ? "AND " . $where : "";

                    $query = "SELECT order_id FROM " . $this->table_prefix . "_shop_yandex_orders";
                    $y_ids = $db->getArrayOfResult($query);
                    foreach ($y_ids as $i => $v) {
                        $y_ids[$i] = $v['order_id'];
                    }

                    $y_ids = implode(", ", $y_ids);

                    $y_temp = " AND ";

                    $where .= " AND O.id NOT IN (" . $y_ids . ") ";
                    if ($request_search) $where .= " AND O.id LIKE '$request_search' ";
                    $main->_show_nav_string($server . $lang . "_shop_orders AS O", $where, "", "action=" . $action . $this->getFilterHref(), $start, $CONFIG["shop_max_rows"], "id DESC", "", 1, 0);
                    $tpl->assign(Array(
                        "MSG_Pages" => $this->_msg["Pages"],
                    ));
                    $this->main_title = $this->_msg["Orders_list"];
                    break;

// Список заказов
                case "yandex_orders":
                    if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
                    global $request_start, $request_clrfilter, $db;
                    $start = ($request_start) ? $request_start : 1;
                    $main->include_main_blocks_2($this->module_name . "_orders.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_ID" => $this->_msg["ID"],
                        "MSG_Date" => $this->_msg["Date"],
                        "MSG_Customer" => $this->_msg["Customer"],
                        "MSG_Email" => $this->_msg["Email"],
                        "MSG_Sum" => $this->_msg["Sum"],
                        "MSG_Discount" => $this->_msg["Discount"],
                        "MSG_Currency" => $this->_msg["Currency"],
                        "MSG_Order_status" => $this->_msg["Order_status"],
                        "MSG_Orders_status" => $this->_msg["Orders_status"],
                        "MSG_Responsible" => $this->_msg["Responsible"],
                        "MSG_Del" => $this->_msg["Del"],
                        "baseurl" => $baseurl,
                    ));
                    if (isset($request_clrfilter)) {
                        unset($_SESSION['shop_fltr']);
                    }
                    $this->show_orders($baseurl, $start, "", "", "", true);
                    // Функция для разбивки записей по страницам и вывода навигации по ним
                    // Имена полей и их назначение :
                    // ($table = таблица, $where = условие выборки, $blockname = имя блока,
                    // $action = добавка к урл, $start = текущая страница/начало выборки,
                    // $rows = кол-во строк, $order_by = порядок выборки, $separator = разделитель)
                    $where = $this->getFilterCondition();
                    $where = $where ? "AND " . $where : "";

                    $query = "SELECT order_id FROM " . $this->table_prefix . "_shop_yandex_orders";
                    $y_ids = $db->getArrayOfResult($query);
                    foreach ($y_ids as $i => $v) {
                        $y_ids[$i] = $v['order_id'];
                    }

                    $y_ids = implode(", ", $y_ids);

                    $y_temp = " AND ";

                    //if($is_yandex){
                    $where .= " AND O.id IN (" . $y_ids . ") ";
                    /*}else{
					$yandex_filter = $y_temp."O.id not IN (".$y_ids.") ";
				}*/

                    $main->_show_nav_string($server . $lang . "_shop_orders AS O", $where, "", "action=" . $action . $this->getFilterHref(), $start, $CONFIG["shop_max_rows"], "id DESC", "", 1, 0);
                    $tpl->assign(Array(
                        "MSG_Pages" => $this->_msg["Pages"],
                    ));
                    $this->main_title = $this->_msg["Orders_list"];
                    break;

// Показать историю заказов пользователя
                case "history":
                    if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
                    global $request_start, $request_id;
                    $start = ($request_start) ? $request_start : 1;
                    $id = (int)$request_id;
                    $main->include_main_blocks_2($this->module_name . "_orders.html", $this->tpl_path);
                    $tpl->prepare();
                    $this->show_orders($baseurl, $start, "", "", $id);
                    $main->_show_nav_string($server . $lang . "_shop_orders", " AND siteuser_id = " . $id, "", "action=" . $action . "&id=" . $id, $start, $CONFIG["shop_max_rows"], "id DESC", "", 1, 0);
                    $this->main_title = $this->_msg["User_orders_list"];
                    break;

// Удалить заказ
                case "delorder":
                    if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
                    global $request_start, $request_id;
                    $this->delete_order($request_id);
                    header("Location: $baseurl&action=orders&start=$request_start");
                    exit;
                    break;

// Показать информацию о заказе
                case "vieworder":
                    if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
                    global $request_start, $request_id;
                    $main->include_main_blocks_2($this->module_name . "_order.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Date" => $this->_msg["Date"],
                        "MSG_Customer" => $this->_msg["Customer"],
                        "MSG_Type" => $this->_msg["Type"],
                        "MSG_Phone" => $this->_msg["Phone"],
                        "MSG_Email" => $this->_msg["Email"],
                        "MSG_Address" => $this->_msg["Address"],
                        "MSG_Jur_person" => $this->_msg["Jur_person"],
                        "MSG_Payment" => $this->_msg["Payment"],
                        "MSG_Adding_info" => $this->_msg["Adding_info"],
                        "MSG_Ordered_products_list" => $this->_msg["Ordered_products_list"],
                        "MSG_Product_ID" => $this->_msg["Product_ID"],
                        "MSG_Name" => $this->_msg["Name"],
                        "MSG_Code" => $this->_msg["Code"],
                        "MSG_Price" => $this->_msg["Price"],
                        "MSG_Inner_currency" => $this->_msg["Inner_currency"],
                        "MSG_Qty" => $this->_msg["Qty"],
                        "MSG_Sum" => $this->_msg["Sum"],
                        "MSG_Order_sum_with_discount" => $this->_msg["Order_sum_with_discount"],
                        "MSG_Order_sum" => $this->_msg["Order_sum"],
                        "MSG_Discount_increase" => $this->_msg["Discount_increase"],
                        "MSG_Total_order_sum" => $this->_msg["Total_order_sum"],
                        "MSG_Webmoney_percent_included" => $this->_msg["Webmoney_percent_included"],
                        "MSG_ASSIST_percent_included" => $this->_msg["ASSIST_percent_included"],
                        "MSG_Order_status" => $this->_msg["Order_status"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                        "MSG_Notes" => $this->_msg["Notes"],
                        "MSG_Print_order" => $this->_msg["Print_order"],
                        "MSG_Delivery" => $this->_msg["Delivery"],
                        "MSG_Transport" => 'Транспортные услуги',
                        "print_link" => $baseurl . "&action=print&id=" . $request_id . "&menu=false"
                    ));
                    $this->show_order($baseurl, $request_id, $request_start);
                    $order = $this->getOrder($request_id);
                    $this->excelPrint($order);
                    break;


                case "print":
                    if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
                    global $request_start, $request_id;
                    $main->include_main_blocks_2($this->module_name . "_print.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Date" => $this->_msg["Date"],
                        "MSG_Customer" => $this->_msg["Customer"],
                        "MSG_Type" => $this->_msg["Type"],
                        "MSG_Phone" => $this->_msg["Phone"],
                        "MSG_Email" => $this->_msg["Email"],
                        "MSG_Address" => $this->_msg["Address"],
                        "MSG_Jur_person" => $this->_msg["Jur_person"],
                        "MSG_Payment" => $this->_msg["Payment"],
                        "MSG_Adding_info" => $this->_msg["Adding_info"],
                        "MSG_Ordered_products_list" => $this->_msg["Ordered_products_list"],
                        "MSG_Product_ID" => $this->_msg["Product_ID"],
                        "MSG_Name" => $this->_msg["Name"],
                        "MSG_Code" => $this->_msg["Code"],
                        "MSG_Price" => $this->_msg["Price"],
                        "MSG_Inner_currency" => $this->_msg["Inner_currency"],
                        "MSG_Qty" => $this->_msg["Qty"],
                        "MSG_Sum" => $this->_msg["Sum"],
                        "MSG_Order_sum_with_discount" => $this->_msg["Order_sum_with_discount"],
                        "MSG_Discount_increase" => $this->_msg["Discount_increase"],
                        "MSG_Total_order_sum" => $this->_msg["Total_order_sum"],
                        "MSG_Webmoney_percent_included" => $this->_msg["Webmoney_percent_included"],
                        "MSG_ASSIST_percent_included" => $this->_msg["ASSIST_percent_included"],
                        "MSG_Order_status" => $this->_msg["Order_status"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                        "MSG_Notes" => $this->_msg["Notes"],
                        "MSG_Transp" => "Транспортные услуги",
                        "MSG_Delivery" => "Доставка",
                    ));
                    $this->show_order($baseurl, $request_id, $request_start);
                    break;

// Обновить статус заказа
                case "changestatus":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_start, $request_status, $request_id, $request_notes;
                    $this->setOrderStatus($request_id, $request_status);
                    $this->setOrderNotes($request_id, $request_notes);
                    header('Location: ' . $baseurl . '&action=orders&start=' . $request_start);
                    exit;
                    break;

// Настройки платёжных систем
                case "pay_sys":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_ok, $request_err;
                    $main->include_main_blocks_2($this->module_name . "_pay_sys.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Payment_system_controls" => $this->_msg["Payment_system_controls"],
                    ));

                    if (isset($request_ok)) {
                        $tpl->newBlock('block_ok');
                        $tpl->assign(Array(
                            "MSG_Data_saved" => $this->_msg["Data_saved"],
                        ));
                    } elseif (isset($request_err)) {
                        $tpl->newBlock('block_err');
                        $tpl->assign(Array(
                            "MSG_Write_file_error" => $this->_msg["Write_file_error"],
                        ));
                    }

                    $tpl->newBlock('block_pay_sys_conf');
                    $tpl->assign(Array(
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                    ));
                    $tpl->assign('form_action', $baseurl . '&action=set_pay_conf');
                    $this->showWmConf();
                    $this->showAssistConf();

                    break;

// Настройки платёжных систем
                case "set_pay_conf":
                    if (!$permissions["e"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }

                    header('Location: ' . $baseurl . '&action=pay_sys&' . (!$this->saveWmConf() || !$this->saveAssistConf() ? 'err' : 'ok'));

                    break;

// Выгрузка/загрузка XML
                case 'imp-exp':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
                    $main->include_main_blocks_2($this->module_name . '_import_export.html', $this->tpl_path);
                    $tpl->prepare();

                    if ($_SESSION["info_block_name"]) {
                        $tpl->newBlock('block_info_box');
                        $tpl->newBlock($_SESSION["info_block_name"]);
                        unset($_SESSION["info_block_name"]);
                        $tpl->assign(Array(
                            "MSG_Info_export_success" => $this->_msg["Info_export_success"],
                            "MSG_No_new_export_info" => $this->_msg["No_new_export_info"],
                            "MSG_Info_import_success" => $this->_msg["Info_import_success"],
                            "file" => "/" . $_SESSION['import_file'],
                        ));
                        unset($_SESSION['import_file']);
                    }

                    $tpl->newBlock('block_upload_xml');
                    $tpl->assign(Array(
                        "MSG_Upload_XML_file" => $this->_msg["Upload_XML_file"],
                        "MSG_Upload" => $this->_msg["Upload"],
                    ));
                    $tpl->assign(array('upload_xml_url' => $baseurl . '&action=uploadxml',));

                    $tpl->newBlock('block_get_xml');
                    $tpl->assign(Array(
                        "MSG_Download_XML_file" => $this->_msg["Download_XML_file"],
                        "MSG_Download" => $this->_msg["Download"],
                        "MSG_All_orders" => $this->_msg["All_orders"],
                    ));
                    $tpl->assign(array('upload_xml_url' => $baseurl . '&action=getxml',));

                    $this->main_title = $this->_msg["Import_Export_info"];
                    break;

                case 'getxml':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
                    if (($file = $this->do_export_XML())) {
                        $_SESSION["info_block_name"] = "block_info_download_success";
                        $_SESSION["import_file"] = $file;
                    } else {
                        $_SESSION["info_block_name"] = "block_no_info_to_download";
                    }

                    header('Location: ' . $baseurl . '&action=imp-exp');
                    break;

                case 'uploadxml':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    if ($_FILES['xml_user_file']['tmp_name'] && ($dom = domxml_open_file($_FILES['xml_user_file']['tmp_name']))) {
                        $this->doImportXMLData($dom);
                    }
                    $_SESSION["info_block_name"] = "block_info_upload_success";

                    header('Location: ' . $baseurl . '&action=imp-exp');
                    break;

                case 'delivery':
                    global $request_id;
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);

                    $main->include_main_blocks_2("{$this->module_name}_{$action}.html", $this->tpl_path);
                    $tpl->prepare();

                    $tpl->assign(array(
                        "MSG_Delivery" => $this->_msg["Delivery"],
                        "MSG_Title" => $this->_msg["Title"],
                        "MSG_Delivery_cost" => $this->_msg["Delivery_cost"],
                        "MSG_description" => $this->_msg["description"],
                        "MSG_Cost" => $this->_msg["Cost"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                        "MSG_Add" => $this->_msg["Add"],
                        "MSG_Ed" => $this->_msg["Ed"],
                        "MSG_Del" => $this->_msg["Del"],
                        "currency" => $CONFIG['shop_order_concurency'],
                        "baseurl" => $baseurl
                    ));

                    $show_list = $this->showDeliveryList($baseurl);
                    $tpl->gotoBlock('_ROOT');
                    if ($request_id && ($dlvr = $this->getDelivery($request_id))) {
                        $tpl->assign($dlvr);
                    }
                    $tpl->assign(array(
                        "css_form_dispay" => !$show_list || $dlvr ? '' : 'none',
                        "css_list_dispay" => $show_list && !$dlvr ? '' : 'none',
                    ));
                    $this->main_title = $this->_msg['Delivery'];
                    break;

                case 'setdelivery':
                    if (!$permissions["e"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $request_data;
                    $this->setDelivery($request_data);
                    header("Location: " . $baseurl . "&action=delivery");
                    exit;

                case 'deldelivery':
                    if (!$permissions["d"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $request_id;
                    $this->delDelivery($request_id);
                    header("Location: " . $baseurl . "&action=delivery");
                    exit;

                case 'status':
                    global $request_id;
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);

                    $main->include_main_blocks_2("{$this->module_name}_{$action}.html", $this->tpl_path);
                    $tpl->prepare();

                    $tpl->assign(array(
                        "MSG_Delivery" => $this->_msg["Delivery"],
                        "MSG_Title" => $this->_msg["Title"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                        "MSG_Add" => $this->_msg["Add"],
                        "MSG_Ed" => $this->_msg["Ed"],
                        "MSG_Del" => $this->_msg["Del"],
                        "MSG_Payable" => $this->_msg["Payable"],
                        "baseurl" => $baseurl
                    ));

                    $show_list = $this->showOrdStatusList($baseurl, false);
                    $tpl->gotoBlock('_ROOT');
                    if ($request_id && ($status = $this->getOrdStatus($request_id))) {
                        $status['payable_checked'] = $status['payable'] ? 'checked' : '';
                        $tpl->assign($status);
                    }
                    $tpl->assign(array(
                        "css_form_dispay" => !$show_list || $status ? '' : 'none',
                        "css_list_dispay" => $show_list && !$status ? '' : 'none',
                    ));
                    $this->main_title = $this->_msg['Orders_status'];
                    break;

                case 'setstatus':
                    if (!$permissions["e"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $request_data;
                    $this->setOrdStatus($request_data);
                    header("Location: " . $baseurl . "&action=status");
                    exit;

                case 'delstatus':
                    if (!$permissions["d"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $request_id;
                    $this->delOrdStatus($request_id);
                    header("Location: " . $baseurl . "&action=status");
                    exit;

// Редактирование свойств (параметров) блока
                case 'properties':
                    if (!$permissions['e']) {
                        $main->message_access_denied($this->module_name, $action);
                        exit;
                    }
                    global $module_id, $request_field, $request_block_id, $title;
                    $main->include_main_blocks('_block_properties.html', 'main');
                    $tpl->prepare();

                    if ($main->show_actions($request_field, '', $this->block_main_module_actions, $this->block_module_actions)) {
                        $main->show_linked_pages($module_id, $request_field);
                    }
                    $tpl->assign(array('_ROOT.title' => $title,
                        '_ROOT.username' => $_SESSION['session_login'],
                        '_ROOT.password' => $_SESSION['session_password'],
                        '_ROOT.name' => $this->module_name,
                        '_ROOT.lang' => $lang,
                        '_ROOT.block_id' => $request_block_id,
                    ));
                    $this->main_title = $this->_msg["Block_properties"];
                    break;

                case 'updateprodfiles':
                    if (!$permissions['r']) {
                        $main->message_access_denied($this->module_name, $action);
                    }

                    if (!$_FILES['filename']['name']) {
                        $this->updateprodfiles($_POST['oldFilename'], $_POST['oldFilename'], $_POST['namef']);
                    } elseif ($_POST['oldFilename'] != $_FILES['filename']['name']) {

                        $img_name = time() . "_" . $_FILES['filename']['name'];
                        $img_name = iconv('UTF-8', 'WINDOWS-1251//IGNORE', $img_name);
                        $uploadfile = RP . "/files/catalog/sup_rus/files/" . $img_name;
                        //var_dump($_FILES['filename']['tmp_name'],$uploadfile);exit;
                        if (move_uploaded_file($_FILES['filename']['tmp_name'], $uploadfile)) {
                            $img_name = iconv('WINDOWS-1251', 'UTF-8//IGNORE', $img_name);
                            // @chmod (RP."/files/catalog/sup_rus/files/".$_POST['oldFilename'], 7777);
                            $this->updateprodfiles($_POST['oldFilename'], $img_name, $_POST['namef']);
                            $_POST['oldFilename'] = iconv('UTF-8', 'WINDOWS-1251//IGNORE', $_POST['oldFilename']);
                            @unlink(RP . "/files/catalog/sup_rus/files/" . $_POST['oldFilename']);
                        }

                    }
                    header('Location: /admin.php?lang=rus&name=catalog&action=addprodfiles');
                    break;

                case 'deleteprodfiles':
                    if (!$permissions['r']) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    $this->deleteprodfiles($_POST['filename']);
                    echo $_REQUEST['filename'];
                    exit;
                    break;

// Default...
                default:
                    $main->include_main_blocks();
                    $tpl->prepare();
            }
//--------------------------- конец обработки действий из админ части --------------------------//
        }
    }

    function getRank($id)
    {
        global $db;
        //$db->query("Select rank From {$this->table_prefix}_skidki Where id={$id}");
        $arr = $db->getArrayOfResult("Select rank From {$this->table_prefix}_skidki Where id={$id}");
        if (isset($arr[0]['rank'])) return $arr[0]['rank'];
    }

    function getMaxRank()
    {
        global $db;
        $arr = $db->getArrayOfResult("Select MAX(rank) as rank From {$this->table_prefix}_skidki");
        if (isset($arr[0]['rank'])) return $arr[0]['rank'];
    }

    function getMinRank()
    {
        global $db;
        $arr = $db->getArrayOfResult("Select MIN(rank) as rank From {$this->table_prefix}_skidki");
        if (isset($arr[0]['rank'])) return $arr[0]['rank'];
    }

    /**
     * @param $id
     * @param $rank
     * @param $shift
     * @return bool
     */
    function upDiscount($id, $shift)
    {
        global $db, $main, $cache;
        $id = (int)$id;
        $rank = (int)$this->getRank($id);
        $shift = (int)$shift;
        if ($rank) {
            if ($main->change_order('down', $this->table_prefix . '_skidki', "rank", $rank, $shift))
                @$cache->delete_cache_files();
            /*
        $value=$rank+$shift;
        if(($rank+$shift) > $this->getMaxRank()) $value=$this->getMaxRank();
        for($i=$rank+1;$i > $value;$i++)
        {
            $db->query("UPDATE {$this->table_prefix}_skidki SET rank=rank-1 WHERE rank={$i}");
        }
        if($db->query("UPDATE {$this->table_prefix}_skidki SET rank={$value} WHERE id={$id}"))
        {
            return true;
        }
        else{
            return false;
        }*/
        }

    }

    /**
     * @param $id
     * @param $rank
     * @param $shift
     * @return bool
     */
    function downDiscount($id, $shift)
    {
        global $db, $cache, $main;
        $id = (int)$id;
        $rank = (int)$this->getRank($id);
        $shift = (int)$shift;
        if ($rank) {

            if ($main->change_order('up', $this->table_prefix . '_skidki', "rank", $rank, $shift))
                @$cache->delete_cache_files();
            /*
            $value=$rank-$shift;
            if(($rank-$shift) < 0 ) $value=0;
            for($i=$rank-1;$i > $value;$i--)
            {
                $db->query("UPDATE {$this->table_prefix}_skidki SET rank=rank+1 WHERE rank={$i}");
            }
            if($db->query("UPDATE {$this->table_prefix}_skidki SET rank={$value} WHERE id={$id}"))
            {
                return true;
            }
            else{
                return false;
            }*/
        }

    }

    function showActionSkidki($_action)
    {
        global $tpl;
        require_once(RP . "mod/catalog/lib/class.CatalogPrototype.php");
        $cat = new CatalogPrototype('only_create_object');

        foreach ($_action['skidki'] as $skidka_id) {
            $skidka = $this->getSkidka($skidka_id);
            if ($skidka) {
                $tpl->newBlock('skidka');
                $tpl->assign('title', $skidka['title']);
                $tpl->newBlock('type' . $skidka['type']);
                $tpl->assign("linkT", '/catalog/' . $skidka['prods'][0]['category_id'] . "/" . $skidka['prods'][0]['id'] . ".html");
                $tpl->assign('title', $skidka['title']);
                $tpl->assign($skidka);
                $is_gift = false;
                $i = 1;
                foreach ($skidka['prods'] as $prod) {
                    if ($prod['is_if']) {
                        $tpl->newBlock('gift_if');
                        if ($i & 1 == 1) {

                            $prod['class'] = "gift_type3_kupon_gift";

                        } else {
                            $prod['class'] = "gift_type3_gift2";
                            if (!isset($skidka['prods'][$i + 2])) {
                                $prod['class'] = "gift_type2_gift2";
                            }

                        }
                        $i++;

                        $tpl->assign($prod);


                        continue;
                    }
                    if ($prod['is_gift']) {
                        $tpl->newBlock('gift');
                        $is_gift = true;
                    } else {
                        $tpl->newBlock('prod' . $skidka['type']);

                        $tpl->assign(array(
                                "linkT" => '/catalog/' . $prod['category_id'] . "/" . $prod['id'] . ".html",
                                "prod_id" => $prod['id'],
                            )

                        );



                    }

                    $tpl->assign('title', $skidka['title']);
                    $tpl->assign($prod);
                    $tpl->assign('new_price', $skidka['new_price']);

                    $tpl->newBlock('prodS' . $skidka['type']);
                    $tpl->assign(array(
                            "prod_id" => $prod['id'],
                        )

                    );
                    if ($prod['path']) {
                        if ($prod['is_gift']) $tpl->newBlock('gift_img');
                        else $tpl->newBlock('img' . $skidka['type']);
                        $tpl->assign($prod);

                    }
                    if (!$prod['is_gift']) {
                        $this->getActionProductGifts($prod['id']);
                        //     $tpl->newBlock('buy');
                        //     $tpl->assign(array('prodId'=>$prod['id']));
                        //      $cat->showGift($prod['id'],1);
                    }
                }
                if ($skidka['type'] == 3 && $skidka['price']) {
                    if ($is_gift) $tpl->newBlock('kupon_gift');
                    else $tpl->newBlock('kupon');
                    $tpl->assign('price', $skidka['price']);

                }elseif($skidka['type'] == 3 && $skidka['percent']){
                    $tpl->newBlock('percent_gift');
                    $tpl->assign('price', $skidka['percent']);
                }

            }
        }
    }

    function getGiftToProd($id)
    {
        if (!$this->is_get_gp) {
            $this->gp = $this->getGiftsToProds();
            $this->is_get_gp = true;
        }

        foreach ($this->gp as $p) {
            /*if(sizeof($p) == 1 && $p[0][0]['id'] == $id)
                return array($p[0][0], $p[1]);
            elseif(sizeof($p) == 2 && ($p[0][0]['id'] == $id || $p[0][1]['id'] == $id))
                return array(array($p[0][0], $p[0][1]), $p[1]);*/
            if (sizeof($p['gifts']) == 0 && $p[0][0]['id'] == $id)
                return array($p[0][0], $p[1]);
            elseif (sizeof($p['gifts']) == 1 && ($p[0][0]['id'] == $id || $p[0][1]['id'] == $id))
                return array(array($p[0][0], $p['gifts'][0]), $p['gifts'][0]);
            elseif (sizeof($p['gifts']) >= 2 && ($p[0][0]['id'] == $id || $p[0][1]['id'] == $id || $p[0][2]['id'] == $id)) {
                $arr = array(array($p[0]), $p['gifts'][0]);
                $i = 0;
                foreach ($p['gifts'] as $val) {
                    if (!$i) {
                        $i++;
                        continue;
                    }
                    $arr[0][] = $val;
                }
                return $arr;
            }
        }
        return 0;


    }

    function getGiftToProdEl($id)
    {
        if (!$this->is_get_gp) {
            $this->gp = $this->getGiftsToProds();
            $this->is_get_gp = true;
        }
        foreach ($this->gp as $p) {
            if (sizeof($p['gifts']) == 0 && $p[0][0]['id'] == $id) {
                return array($p[0], $p['gifts'][0]);
            } elseif (sizeof($p['gifts']) == 1 && ($p[0][0]['id'] == $id || $p[0][1]['id'] == $id))
                return array(array($p[0], $p['gifts'][0]), $p['gifts'][0]);
            elseif (sizeof($p['gifts']) >= 2 && ($p[0][0]['id'] == $id || $p[0][1]['id'] == $id || $p[0][2]['id'] == $id)) {
                $arr = array(array($p[0]), $p['gifts'][0]);
                $i = 0;
                foreach ($p['gifts'] as $val) {
                    if (!$i) {
                        $i++;
                        //  continue;
                    }
                    $arr[0][] = $val;
                }
                return $arr;
            }

        }
        return 0;
    }

    function showGift($id, $choose)
    {
        global $server, $lang, $tpl, $db;
        $ret = $this->getGiftToProd($id);
        if ($ret && $choose != 2 && $ret[1]) {
            if ($choose == 3) {
                $tpl->newBlock('double_cart');
                if ($ret[0][0]['id'] == $id) {
                    $tpl->assign('title', $ret[0][1]['product_title']);
                    $tpl->assign('link', $ret[0][1]['link']);
                } else {
                    $tpl->assign('title', $ret[0][0]['product_title']);
                    $tpl->assign('link', $ret[0][0]['link']);
                }
            }
            $gift = $ret[1];
            if (floor($choose) == 9) {
                $el = $choose - floor($choose);
                $el = $el * 10;
                if ($el) $el = $el - 2;
                if ($el) $gift = $ret[0][$el];
                else $gift = $ret[0][1];
            }
            $link = '/catalog/' . ($gift['calias'] ? $gift['calias'] : $gift['cid']) . '/' . ($gift['alias'] ? $gift['alias'] : $gift['id']) . '.html';
            $tpl->newBlock('gift');
            $tpl->assign('title', $gift['product_title']);
            $tpl->assign('link', $link);
            if ($gift['image_middle']) {
                $tpl->newBlock('gift_img');
                $tpl->assign('src', '/files/catalog/' . $server . $lang . '/' . $gift['image_middle']);
            }
        } elseif ($ret && $choose == 2 && $ret[0][0]['kupon']) {
            $tpl->newBlock('kupon');
            $tpl->assign('kupon_price', $ret[0][0]['kupon']);
        }

        $ar = $db->getArrayOfResult("SELECT   c.product_title,c.category_id,p1.prod_id FROM sup_rus_skidki_prods as p
                                LEFT JOIN sup_rus_skidki_prods  AS p1 ON p1.skidka_id = p.skidka_id
                                LEFT JOIN sup_rus_catalog_products AS c ON c.id = p1.prod_id
                                 JOIN sup_rus_actions_skidki AS a
                                ON  p.skidka_id = a.skidka_id
                                WHERE p.prod_id={$id} AND p1.is_gift=0 AND p1.is_if=0");
        if (!empty($ar)) {

            $tpl->newBlock('products_gift');
            foreach ($ar as $val) {
                $tpl->newBlock('product_inform');
                $tpl->assign(array('link' => "/catalog/" . $val['category_id'] . "/" . $val['prod_id'] . ".html",
                    'title' => $val['product_title']));
            }
        }
    }

    function getGiftsToProds()
    {
        $actions = $this->getCurrentActions();
        //var_dump($actions);
        $prods = array();
        foreach ($actions as $action) {
            $act = $this->getAction($action['id']);
            //var_dump($act);
            foreach ($act['skidki'] as $sid) {
                $skidka = $this->getSkidka($sid);
                //var_dump($skidka);
                if ($skidka['type'] == 3) {
                    $gift = NULL;
                    $gift_if = NULL;
                    $prds = array();
                    $ic = false;
                    $r = array();
                    foreach ($skidka['prods'] as $prod) {
                        if ($prod['is_gift'] && !$prod['is_if']) $gift = $prod;
                        elseif ($prod['is_if']) $gift_if[] = $prod;
                        else $prds[] = $prod;

                    }
                    if ($gift) {
                        $r[] = $gift;
                        $ic = true;
                    }
                    if ($gift_if) {
                        //foreach
                        $r = array_merge($r, $gift_if);
                        //$r[]=$gift_if;
                        $ic = true;
                    }
                    if ($ic) $prods[] = array($prds, 'gifts' => $r);
                    else $prods[] = array($prds, 'gifts' => null);


                }
            }
        }
        return $prods;
    }

    function getCurrentActions()
    {
        global $server, $lang;
        $time = time();
        $actions = array();
        $query = 'SELECT * FROM ' . $server . $lang . "_actions WHERE time_from<=$time AND time_to>=$time ORDER BY id DESC ";
        if (!$result = mysql_query($query)) die(mysql_error());
        while ($rw = mysql_fetch_assoc($result)) $actions[] = $rw;
        return $actions;
    }

    function getAction($id, $current = false)
    {
        global $server, $lang;
        $id = (int)$id;
        $time = time();
        if ($current) $whr = " time_from<=$time AND time_to>=$time AND ";
        else $whr = '';

        $query = 'SELECT * FROM ' . $server . $lang . '_actions WHERE ' . $whr . ' id=' . $id;
        if (!$result = mysql_query($query)) die(mysql_error());
        $row = mysql_fetch_assoc($result);
        if (!$row) return false;

        $row['skidki'] = array();

        $query = 'SELECT sp.*,skid.rank FROM ' . $server . $lang . '_actions_skidki as sp left join ' . $server . $lang . '_skidki as skid ON skid.id =sp.skidka_id
         		   WHERE sp.action_id=' . $id . ' ORDER BY skid.rank DESC';
        /*'SELECT sp.* FROM '.$server.$lang.'_actions_skidki as sp
        WHERE sp.action_id='.$id;*/

        if (!$result = mysql_query($query)) die(mysql_error());
        while ($rw = mysql_fetch_assoc($result)) $row['skidki'][] = $rw['skidka_id'];
        return $row;
    }

    function getSkidka($id)
    {
        global $server, $lang;
        $id = (int)$id;
        $query = 'SELECT * FROM ' . $server . $lang . '_skidki WHERE id=' . $id;
        if (!$result = mysql_query($query)) die(mysql_error());
        $row = mysql_fetch_assoc($result);
        if (!$row) return false;

        $row['prods'] = array();
        $ctlg = Module::load_mod('catalog', 'only_create_object');
        $query = 'SELECT p.*, sp.is_gift,sp.is_if,sp.comment,c.alias as calias,c.title as cat_title,c.alias as calias, c.id as cid,p.product_inf as inf FROM ' . $server . $lang . '_skidki_prods as sp
         		  INNER JOIN ' . $server . $lang . '_catalog_products as p ON (sp.prod_id=p.id)
				  INNER JOIN ' . $server . $lang . '_catalog_categories as c ON (c.id=p.category_id)	
				  WHERE sp.skidka_id=' . $id;
        if (!$result = mysql_query($query)) die(mysql_error());
        $price = 0;
        while ($rw = mysql_fetch_assoc($result)) {
            $rw['link'] = $ctlg->getProdLink('/catalog/', $rw['alias'] ? $rw['alias'] : $rw['id'], $rw['id'], $rw['calias'] ? $rw['calias'] : $rw['cid']);
            $rw['cat_link'] = $ctlg->getCtgrLink('/catalog/', $rw['calias'] ? $rw['calias'] : $rw['cid'], $rw['cid']);
            if ($rw['image_middle']) $rw['path'] = '/files/catalog/' . $server . $lang . '/' . $rw['image_middle'];
            $rw['price'] = (int)$rw['price_1'];

            $rw['kupon'] = $row['price'];
            $rw['percent'] = $row['percent'];

            $row['prods'][] = $rw;
            if (!$rw['is_gift']) $price += (int)$rw['price_1'];
        }
        $row['total'] = $price;
        if ($row['type'] == 1) {
            $row['new_price'] = (int)((100 - $row['percent']) * $price / 100);
            $row['res'] = $price - $row['new_price'];
            $row['prod_id'] = $row['prods'][0]['id'];
        } elseif ($row['type'] == 2) {
            $row['res'] = $price - $row['price'];
            $row['res_v'] = $row['price'];
            $row['price_v'] = $price - $row['price'];
        }
        return $row;
    }

    function getNewProductPrice($id, $choose = false)
    {
        global $server, $lang;
        $time = time();

        $query = 'SELECT sk.percent,sk.percent_p, p.price_1 FROM ' . $server . $lang . '_skidki as sk
		          INNER JOIN ' . $server . $lang . '_actions as a 
				  INNER JOIN ' . $server . $lang . '_actions_skidki as ask ON (sk.id=ask.skidka_id AND a.id=ask.action_id)
				  INNER JOIN ' . $server . $lang . '_skidki_prods as sp ON (sp.skidka_id=sk.id)
				  INNER JOIN ' . $server . $lang . '_catalog_products as p ON (sp.prod_id=p.id)
				  WHERE p.id=' . $id . ' AND a.time_from<=' . $time . ' AND a.time_to>=' . $time . '
				  GROUP BY sk.id ORDER BY a.id DESC LIMIT 1';

        if (!$result = mysql_query($query)) die(mysql_error());
        if (!$row = mysql_fetch_assoc($result)) return false;
        return $row['percent_p'] && !$choose ? (int)$row['price_1'] :(int)((100 - $row['percent']) * $row['price_1'] / 100);
    }

    function showCat($cat, $sep)
    {

        global $tpl;
        $tpl->newBlock('prod');
        $tpl->assign('id', $cat['id']);
        $tpl->assign('title', $sep . $cat['title']);
        $tpl->newBlock('prod1');
        $tpl->assign('id', $cat['id']);
        $tpl->assign('title', $sep . $cat['title']);
        $tpl->newBlock('prod2');
        $tpl->assign('id', $cat['id']);
        $tpl->assign('title', $sep . $cat['title']);
        $tpl->newBlock('gift');
        $tpl->assign('id', $cat['id']);
        $tpl->assign('title', $sep . $cat['title']);
        $tpl->newBlock('gift_if');
        $tpl->assign('id', $cat['id']);
        $tpl->assign('title', $sep . $cat['title']);
    }

    function showProducts($products, $sep)
    {
        global $tpl;
        foreach ($products as $cat) {
            $tpl->newBlock('prod');
            $tpl->assign('id', $cat['id']);
            $tpl->assign('title', $sep . '- ' . $cat['title']);
            $tpl->newBlock('prod1');
            $tpl->assign('id', $cat['id']);
            $tpl->assign('title', $sep . '- ' . $cat['title']);
            $tpl->newBlock('prod2');
            $tpl->assign('id', $cat['id']);
            $tpl->assign('title', $sep . '- ' . $cat['title']);
            $tpl->newBlock('gift');
            $tpl->assign('id', $cat['id']);
            $tpl->assign('title', $sep . '- ' . $cat['title']);
            $tpl->newBlock('gift_if');
            $tpl->assign('id', $cat['id']);
            $tpl->assign('title', $sep . '- ' . $cat['title']);
        }
    }

    function getCategories($parent_id)
    {
        global $server, $lang;
        $query = 'SELECT id,title FROM ' . $server . $lang . '_catalog_categories WHERE parent_id=' . (int)$parent_id . ' ORDER BY rank';
        $cats = array();
        if (!$result = mysql_query($query)) die(mysql_error());
        while ($row = mysql_fetch_assoc($result)) $cats[] = $row;
        return $cats;
    }

    function getProducts($category_id)
    {
        global $server, $lang;
        //return array();
        $query = 'SELECT id,product_title as title FROM ' . $server . $lang . '_catalog_products WHERE category_id=' . (int)$category_id . ' ORDER BY rank';
        $prods = array();
        if (!$result = mysql_query($query)) die(mysql_error());
        while ($row = mysql_fetch_assoc($result)) $prods[] = $row;
        return $prods;
    }


    function getTaxes($sum)
    {
        global $CONFIG, $db, $server, $lang;
        if ($sum) {
            $db->query('SELECT rank, sum(percent) value
							FROM ' . $this->table_prefix . '_shop_taxes
							WHERE active = 1
							GROUP BY rank
							ORDER BY rank');
            if ($db->nf() > 0) {
                for ($i = 0; $i < $db->nf(); $i++) {
                    $db->next_record();
                    $arr[$i]['rank'] = $db->f('rank');
                    $arr[$i]['value'] = $db->f('value');
                }
                return $arr;
            }
        }
        return FALSE;
    }

    function getSimpleDiscount($sum, $currency)
    {
        global $CONFIG;

        $sum = ShopPrototype::exchange($sum, $currency, $CONFIG['shop_order_concurency']);
        $arr = $this->getDiscounts($sum);
        $discount = 0;
        $ret = false;
        if (is_array($arr)) {
            $cost = (float)$sum;
            $sum_discount = 0;
            for ($i = 0; $i < sizeof($arr); $i++) {
                if ($arr[$i]['discount_is_percent'] == 1) {
                    $sum_discount = (($arr[$i]['discount_value']) / 100) * $cost;
                } else if ($arr[$i]['discount_is_percent'] == 0) {
                    $sum_discount = $arr[$i]['discount_value'];
                }
                if ($sum_discount > $discount) {
                    $discount = $sum_discount;
                    $ret = $arr[$i];
                    $ret['discount_sum'] = number_format(
                        ShopPrototype::exchange($discount, $CONFIG['shop_order_concurency'], $currency),
                        2, ".", "");
                }
            }
        }
        return $ret;
    }

    function getDiscounts($sum)
    {
        global $CONFIG, $db, $server, $lang;
        $sum = (float)$sum;
        if ($sum) {
            $db->query("SELECT * FROM {$this->table_prefix}_shop_discounts WHERE {$sum} "
                . "BETWEEN min AND max AND IF(from_date AND from_date IS NOT NULL, NOW()>=from_date, 1) "
                . "AND IF(to_date AND to_date IS NOT NULL, NOW()<DATE_ADD(to_date, INTERVAL 1 DAY), 1) "
                . "AND active ORDER BY name");
            if ($db->nf() > 0) {
                for ($i = 0; $i < $db->nf(); $i++) {
                    $db->next_record();
                    $arr[$i]['discount_id'] = $db->f('id');
                    $arr[$i]['discount_name'] = $db->f('name');
                    $arr[$i]['discount_value'] = $db->f('value');
                    $arr[$i]['discount_min'] = $db->f('min');
                    $arr[$i]['discount_max'] = $db->f('max');
                    $arr[$i]['discount_is_percent'] = $db->f('is_percent');
                }
                return $arr;
            }
        }
        return FALSE;
    }

    /**
     * Получить товары заказа
     *
     * @param unknown_type $order_id
     * @return unknown
     */
    function getOrderItems($order_id = NULL)
    {
        global $CONFIG, $db, $server, $lang, $baseurl, $core;
        $order_id = (int)$order_id;
        $catalog_page = $core->formPageLink($CONFIG['catalog_page_link'], $CONFIG['catalog_page_address'], $lang);
        if ($order_id) {
            $arrR = $db->getArrayOfResult('SELECT * FROM ' . $this->table_prefix . '_shop_order_items WHERE order_id = ' . $order_id);
            if (!empty($arrR)) return $arrR;

        }
        return FALSE;
    }

    function getOrder($order_id)
    {
        global $CONFIG, $db, $server, $lang, $baseurl, $core;

        $order_id = (int)$order_id;

        $db->query('SELECT *, DATE_FORMAT(date, \'' . $CONFIG['shop_date_format']
            . '\') AS order_date, S.title AS order_status FROM ' . $this->table_prefix . '_shop_orders O LEFT JOIN '
            . $this->table_prefix . '_shop_order_status S ON status=S.id WHERE O.id = ' . $order_id);

        include_once(RP . "/inc/func.num2str.php");

        if ($db->nf() > 0) {
            $db->next_record();
            $pay_type = $db->f('pay_type');
            $sum = Main::process_price($db->f('sum'));
            $cost = Main::process_price($db->f('total_sum'));
            $discount = Main::process_price($db->f('discount'));
            $discount = ($discount > 0 ? "+" : "") . $discount;
            $cur = $db->f('currency');
            $arr['order_id'] = $order_id;
            $arr['number'] = $order_id;
            $arr['customer'] = $db->f('customer');
            $arr['delivery_id'] = $db->f('delivery_id');
            $arr['delivery_cost'] = Main::process_price($db->f('delivery_cost'));
            $arr['sum'] = (int)$sum;
            $arr['sum_rur'] = (int)Main::process_price(ShopPrototype::exchange($sum, $cur, "RUR"));
            $arr['discount'] = $discount;
            $arr['total_sum'] = $cost;
            $arr['cost'] = $cost;
            $arr['cost_rur'] = Main::process_price(ShopPrototype::exchange($cost, $cur, "RUR"));
            $arr['cost_rur_in_words'] = num2str($arr['cost_rur']);
            $arr['currency'] = $CONFIG['catalog_currencies'][$cur];
            $arr['currency_id'] = $cur;
            $arr['status'] = $db->f('status');
            $arr['order_status'] = $this->getStatusTitle($db->f('order_status'), $db->f('status'));
            $arr['pay_type_id'] = $pay_type;
            $arr['pay_type'] = $CONFIG['order_pay_types'][$pay_type];
            $arr['date'] = $db->f('order_date');
            $arr['nds'] = $CONFIG['shop_nds'];
            $arr['nds_cost'] = number_format($arr['cost_rur'] * $CONFIG['shop_nds'] / 100, "2", ".", "");
            $arr['sum_cost'] = number_format($arr['sum_rur'] * $CONFIG['shop_nds'] / 100, "2", ".", "");
            $arr['address'] = nl2br($db->f("address"));
            $arr['phone'] = $db->f("phone");
            $arr['organization'] = $db->f("organization") ? $db->f("organization") : "";
            $arr['jpinfo'] = nl2br($db->f("jpinfo"));
            $arr['addinfo'] = nl2br($db->f("addinfo"));
            $arr['session'] = nl2br($db->f("session"));
            $arr['notify'] = $db->f("notify");
            $arr['email'] = $db->f("email");
            $arr['transp'] = $db->f("transportation_services");
            $arr['siteuser_id'] = $db->f("siteuser_id");

            return $arr;
        }
        return FALSE;
    }


    function getAllOrders($siteuser_id, $order = NULL)
    {
        global $CONFIG, $db, $db2, $server, $lang, $baseurl;
        $siteuser_id = (int)$siteuser_id;
        if ($siteuser_id) {
            $db->query('SELECT O.id, customer, DATE_FORMAT(date,"' . $CONFIG['shop_date_format'] . '") date, status, sum, currency, pay_type, total_sum, discount, total_sum, S.title AS order_status, payable
							FROM ' . $this->table_prefix . '_shop_orders O LEFT JOIN  ' . $this->table_prefix . '_shop_order_status S ON status=S.id
							WHERE siteuser_id = ' . $siteuser_id . '
							ORDER BY date DESC');
            if ($db->nf() > 0) {
                for ($i = 0; $i < $db->nf(); $i++) {
                    $db->next_record();
                    $oder_id = $db->f('id');
                    $discount = Main::process_price($db->f('discount'));
                    $discount = ($discount > 0 ? "+" : "") . $discount;
                    $arr[$i]['order_id'] = $db->f('id');
                    $arr[$i]['pay_type'] = $db->f('pay_type');
                    $arr[$i]['status'] = $db->f('status');
                    $arr[$i]['total_sum'] = $db->f('total_sum');
                    $arr[$i]['order_customer'] = $db->f('customer');
                    $arr[$i]['order_organization'] = $db->f('organization');
                    $arr[$i]['order_date'] = $db->f('date');
                    $arr[$i]['order_status'] = $this->getStatusTitle($db->f('order_status'), $db->f('status'));
                    $arr[$i]['order_payable'] = $db->f('payable');
                    $arr[$i]['order_sum'] = Main::process_price($db->f('sum'));
                    $arr[$i]['order_discount'] = $discount;
                    $arr[$i]['order_cost'] = Main::process_price($db->f('total_sum'));
                    $arr[$i]['order_currency'] = $CONFIG['catalog_currencies'][$db->f('currency')];
                    $arr[$i]['order_link'] = $baseurl . '&action=vieworder&id=' . $db->f('id');
                    $array = $db2->getArrayOfResult("Select bonus,status From core_users_bonus_data WHERE user_id={$siteuser_id} AND order_id={$oder_id}");
                    if (is_array($array)) {
                        $arr[$i]['bonus'] = "";
                        $l = "";
                        foreach ($array as $key => $val) {
                            switch ($val['status']) {
                                case 0 :
                                    $arr[$i]['bonus'] .= $l . "Ожидает зачисления (" . $val['bonus'] . ")";
                                    break;
                                case 1 :
                                    $arr[$i]['bonus'] .= $l . "+ " . $val['bonus'];
                                    break;
                                case 2 :
                                    $arr[$i]['bonus'] .= $l . "- " . $val['bonus'];
                                    break;
                                case 3 :
                                    $arr[$i]['bonus'] .= $l . "Заморожено(" . $val['bonus'] . ")";
                                    break;
                                case 4 :
                                    $arr[$i]['bonus'] .= $l . "Отменено(" . $val['bonus'] . ")";
                                    break;
                                case 5 :
                                    $arr[$i]['bonus'] .= $l . "Возвращено(" . $val['bonus'] . ")";
                                    break;
                            }
                            $l = ",";
                        }

                    }
                }
                return $arr;
            }
        }
        return FALSE;
    }

    // функция для вывода списка заказов
    function show_orders($transurl, $start = "", $rows = "", $order_by = "", $siteuser_id = "", $is_yandex = false)
    {
        global $CONFIG, $main, $db, $tpl, $server, $lang, $request_search;

        $admin = Module::is_admin();

        $start = $start > 1 ? (int)$start : 1;
        $rows = !$rows || $rows < 1 ? $CONFIG["shop_max_rows"] : (int)$rows;
        $start_row = ($start - 1) * $rows;
        if (!$order_by) {
            $order_by = "id DESC";
        }

        $query = "SELECT order_id FROM " . $this->table_prefix . "_shop_yandex_orders";
        $y_ids = $db->getArrayOfResult($query);
        foreach ($y_ids as $i => $v) {
            $y_ids[$i] = $v['order_id'];
        }
        $y_ids = implode(", ", $y_ids);
        $filter = $this->getFilterCondition();
        if ($filter)
            $y_temp = " AND ";
        else
            $y_temp = " WHERE ";

        if ($is_yandex) {
            $yandex_filter = $y_temp . "O.id IN (" . $y_ids . ") ";
        } else {
            $yandex_filter = $y_temp . "O.id not IN (" . $y_ids . ") ";
        }
        //var_dump($yandex_filter);

        if ($request_search) $search = " AND O.id LIKE '$request_search' ";
        if ($siteuser_id) {
            $query = "SELECT O.*, DATE_FORMAT(date,'" . $CONFIG["shop_date_format"] . "') as date, S.title AS order_status
						FROM " . $this->table_prefix . "_shop_orders O LEFT JOIN " . $this->table_prefix . "_shop_order_status S ON status=S.id
						WHERE siteuser_id = $siteuser_id 
						ORDER BY $order_by
						LIMIT $start_row,$rows";
        } else {
            if (!$admin) {
                return FALSE;
            }
            $this->showFilter();;
            $filter = $filter ? " WHERE " . $filter : "";
            $query = "SELECT O.*, DATE_FORMAT(date,'{$CONFIG["shop_date_format"]}') as date, "
                . "S.title AS order_status, U.username AS manager_username "
                . "FROM {$this->table_prefix}_shop_orders O LEFT JOIN "
                . $this->table_prefix . "_shop_order_status S ON O.status=S.id LEFT JOIN "
                . "core_users U ON O.manager_id=U.id {$filter} " .
                $yandex_filter . $search .
                "ORDER BY O.{$order_by} LIMIT {$start_row},{$rows}";
        }
        $db->query($query);
        if ($db->nf() > 0) {
            $tpl->newBlock('block_basket');
            while ($db->next_record()) {
                if ($db->f("email")) {
                    $email = "<a href=\"mailto:" . $db->f("email") . "\">" . $db->f("email") . "</a>";
                } else {
                    $email = "-";
                }
                $sum = $main->process_price($db->f("sum"));
                $cost = $main->process_price($db->f("total_sum"));
                $discount = $main->process_price($db->f("discount"));
                $discount = ($discount > 0 ? "+" : "") . $discount;
                if (!$sum) $sum = "-";

                $user_id = $db->f("siteuser_id");
                $customer = $user_id
                    ? "<a href=\"/admin.php?lang=" . $lang . "&name=siteusers&action=editsiteuser&id=" . $user_id . "\">" . $db->f("customer") . "</a>"
                    : $db->f("customer");

                $order_status = $this->getStatusTitle($db->f('order_status'), $db->f('status'));

                $tpl->newBlock('block_order');
                $tpl->assign(Array(
                    "MSG_Confirm_delete_record" => $this->_msg["Confirm_delete_record"],
                    "MSG_Delete" => $this->_msg["Delete"],
                ));
                $id = $db->f("id");
                $tpl->assign(
                    array(
                        'order_id' => $id,
                        'order_customer' => $customer,
                        'order_status' => $db->f('status')==14 ? "<a href='/admin.php?lang=rus&name=shop&action=confirmPayUniteller&order_id=".$id."'>Подтвердить платеж</a>":$order_status,
                        'order_address' => nl2br($db->f("address")),
                        'order_phone' => $db->f("phone"),
                        'order_organization' => ($db->f("organization")) ? " (" . $db->f("organization") . ")" : "",
                        'order_jpinfo' => nl2br($db->f("jpinfo")),
                        'order_addinfo' => nl2br($db->f("addinfo")),
                        'order_email' => $email,
                        'order_sum' => $sum,
                        'order_cost' => $cost,
                        'order_discount' => $discount,
                        'order_currency' => $CONFIG['catalog_currencies'][$db->f("currency")],
                        'order_date' => $db->f("date"),
                        'order_link' => $transurl . "&action=vieworder&id=" . $id . "&start=" . $start,
                        'edit_link' => $transurl . "&action=editorder&id=" . $id . "&start=" . $start,
                        'order_del_link' => $transurl . "&action=delorder&id=" . $id . "&start=" . $start,
                        'start' => $start,
                        'tr_color' => $tr_color,
                        'manager_username' => $db->f("manager_username"),
                        'manager_id' => $db->f("manager_id"),
                        'editor' => $db->f("editor"),
                        'lang' => $lang,
                        'transportation_services' => $db->f("transportation_services"),
                        'delivery_cost' => $db->f("delivery_cost"),
                    )
                );

            }
            return TRUE;
        }
        return FALSE;
    }


    // вывести информацию о заказе по его id
    function show_order($transurl, $id = "", $start = "")
    {
        $id = (int)$id;
        if (!$id) return FALSE;
        global $CONFIG, $main, $db, $tpl, $baseurl, $core, $server, $lang;

        $admin = Module::is_admin();

        $status_list = $this->getOrdStatusList();
        if ($admin) {
            $query = "SELECT O.*, DATE_FORMAT(O.date,'" . $CONFIG["shop_date_format"]
                . "') as date, COUNT(H.id) AS status_cnt, payable, S.title AS order_status"
                . " FROM " . $this->table_prefix . "_shop_orders AS O LEFT JOIN " . $this->table_prefix . "_shop_order_history AS H"
                . " ON O.id=H.order_id LEFT JOIN " . $this->table_prefix . "_shop_order_status S ON status=S.id"
                . " WHERE O.id = $id GROUP BY H.order_id";
            $db->query($query);
        } else if ($_SESSION["siteuser"]["id"]) {
            $query = "SELECT O.*, DATE_FORMAT(date,'" . $CONFIG["shop_date_format"] . "') as date, payable, S.title AS order_status
						FROM " . $this->table_prefix . "_shop_orders O LEFT JOIN " . $this->table_prefix . "_shop_order_status S ON status=S.id
						WHERE O.id = $id AND siteuser_id = {$_SESSION["siteuser"]["id"]}";
            $db->query($query);
        } else {
            return FALSE;
        }
        if ($db->nf() > 0) {
            $db->next_record();
            if ($db->f("email")) {
                $email = "<a href=\"mailto:" . $db->f("email") . "\">" . $db->f("email") . "</a>";
            } else {
                $email = "-";
            }
            $sum = $main->process_price($db->f("sum"));
            $cost = $main->process_price($db->f("total_sum"));
            $discount = $main->process_price($db->f("discount"));
            $dlvr_cost = $main->process_price($db->f("delivery_cost"));
            $dlvr_id = $db->f("delivery_id");
            if (!$sum) $sum = "-";

            $orders_history_link = "";
            $customer = $db->f("customer");
            if ($db->f("siteuser_id") && $admin) {
                $customer = $this->_msg["Registered_user"] . "<br><a href=\"/admin.php?lang={$lang}&name=siteusers&action=editsiteuser&id=" . $db->f("siteuser_id") . "\">" . $db->f("customer") . "</a>";
                $orders_history_link = "<br><a href=\"$transurl&action=history&id=" . $db->f("siteuser_id") . "\">" . $this->_msg["Show_user_orders"] . "</a>";
            }

            $status = $db->f("status");
            $order_status = 0 == $status ? $this->_msg['Default_status']
                : ($db->f('order_status') ? $db->f('order_status') : $this->_msg['undef']);

            $shopping_cart_page = $core->formPageLink($CONFIG["shop_page_link"], $CONFIG["shop_page_address"], $lang);

            $cur = $db->f('currency');
            $status_action = $admin ? $transurl . '&action=changestatus&start=' . $start : "";
            $pay_type = $db->f("pay_type");
            $sum_rur = $sum * $_SESSION['catalog_rates'][$cur . '/RUR'];
            $cost_rur = $cost * $_SESSION['catalog_rates'][$cur . '/RUR'];
            //вывод на экран
            $bonus = $this->getOrderBonus($id);
            $l = "";
            $bonus = $bonus * $CONFIG['shop_bonus_cost'];
            if ($bonus) {
                if ((float)$discount == (float)$bonus) {
                    $l = " бонус";
                } else $l = "(из них " . $bonus . " руб. бонус)";
            }
            $tpl->assign(
                array(
                    'order_id' => $id,
                    'order_status' => $order_status,
                    'order_customer' => $customer,
                    'order_customer_type' => $db->f('type'),
                    'order_address' => nl2br($db->f("address")),
                    'order_phone' => $db->f("phone"),
                    'order_email' => $email,
                    'order_organization' => ($db->f("organization")) ? " (" . $db->f("organization") . ")" : "",
                    'order_jpinfo' => nl2br($db->f("jpinfo")),
                    'order_addinfo' => nl2br($db->f("addinfo")),
                    'order_sum' => $sum,
                    'order_sum_rur' => number_format($sum_rur, "2", ".", ""),
                    'order_cost' => $cost,
                    'order_cost_rur' => number_format($cost_rur, "2", ".", ""),
                    'order_discount' => $discount > 0 ? "+" . $discount . $l : $discount . $l,
                    'order_currency' => $CONFIG['catalog_currencies'][$cur],
                    'order_delivery' => $dlvr_cost,
                    'transport_service' => $db->f("transportation_services"),
                    'delivery_currency' => $CONFIG['catalog_currencies'][$CONFIG['shop_order_concurency']],
                    'currency_id' => $cur,
                    'order_date' => $db->f("date"),
                    'order_info' => $db->f("info"),
                    'order_notes' => $db->f("notes"),
                    'order_payable' => $db->f("payable"),
                    'order_pay_type' => $CONFIG['order_pay_types'][$pay_type],
                    'repeat_order_link' => $shopping_cart_page . '?action=repeat&id=' . $id,
                    'change_status_action' => $status_action,
                    'orders_history_link' => $orders_history_link,
                    'ordering_link' => $transurl . "&action=ordering&id=" . $id,
                )
            );

            /* Если оплата WebMoney - учитываем скидку/надбавку */
            if (4 == $pay_type && $CONFIG['shop_WM_discount']) {
                $tpl->newBlock('block_webmoney_discount');
                $tpl->assign(array(
                    'webmoney_discount' => ($CONFIG['shop_WM_discount'] > 0 ? "+" : "") . $CONFIG['shop_WM_discount'],
                    'MSG_Webmoney_percent_included' => $this->_msg['Webmoney_percent_included']
                ));
            }
            /* Если оплата ASSIST - учитываем скидку/надбавку */
            if (5 == $pay_type && $CONFIG['shop_assist_discount']) {
                $tpl->newBlock('block_assist_discount');
                $tpl->assign(array(
                    'assist_discount' => ($CONFIG['shop_assist_discount'] > 0 ? "+" : "") . $CONFIG['shop_assist_discount'],
                    'MSG_ASSIST_percent_included' => $this->_msg['ASSIST_percent_included']
                ));
            }

            $this->main_title = $this->_msg["Order"] . " №: " . $id . " (" . $order_status . ")";
            $status_hist = $admin && $db->f("status_cnt") ? TRUE : FALSE;
            $_SESSION["order"]["currency"] = $cur;

            /** Если не оплачено **/
            if (0 == $status || $db->f("payable")) {
                /** Способ оплаты **/
                switch ($pay_type) {
                    /** квитанция Сбербанк **/
                    case 2:
                        $tpl->newBlock('block_sbr');
                        $compn_info = $this->getCompanyInfo();
                        $compn_info['sbr_ticket_link'] = $baseurl . ('&' == substr($baseurl, -1) ? '' : '&') . "action=sbr_ticket&id=" . $id;
                        $compn_info['number'] = $id;
                        $tpl->assign($compn_info);
                        break;

                    /** банковский перевод **/
                    case 3:
                        $tpl->newBlock('block_bank');
                        $compn_info = $this->getCompanyInfo();
                        $compn_info['number'] = $id;
                        $tpl->assign($compn_info);
                        break;

                    /** Webmoney **/
                    case 4:
                        if (
                            $cost > 0
                            && $CONFIG['shop_WM_purse']
                            && in_array($CONFIG['shop_WM_currency'], array('RUR', 'USD', 'EUR'))
                        ) {
                            /* Сумма заказа в валюте кошелька webmoney*/
                            $wm_amount = $cost * $_SESSION['catalog_rates'][$cur . '/' . $CONFIG['shop_WM_currency']];
                            $tpl->newBlock('block_webmoney');
                            $tpl->assign(
                                array(
                                    'wm_amount' => number_format($wm_amount, 2, ".", ""),
                                    'wm_payment_no' => $id,
                                    'wm_payee_purse' => $CONFIG['shop_WM_purse'],
                                    'order_sum' => $cost,
                                    'wm_success_url' => $CONFIG['shop_WM_success_url'],
                                    'wm_fail_url' => $CONFIG['shop_WM_fail_url'],
                                    'wm_result_url' => "http://" . $_SERVER['HTTP_HOST'] . $CONFIG['shop_WM_result_url'],
                                )
                            );
                        }
                        break;

                    /** ASSIST **/
                    case 5:
                        if (
                            $cost > 0
                            && $CONFIG['shop_assist_idp']
                            && in_array($CONFIG['shop_assist_currency'], array('RUR', 'USD', 'EUR'))
                        ) {
                            /* Сумма заказа в валюте кошелька ASSIST */
                            $assist_amount = $cost * $_SESSION['catalog_rates'][$cur . '/' . $CONFIG['shop_assist_currency']];
                            $tpl->newBlock('block_assist');
                            $tpl->assign(
                                array(
                                    'assist_shop_id' => $CONFIG['shop_assist_idp'],
                                    'assist_order_id' => $id,
                                    'assist_amount' => $assist_amount,
                                    'assist_success_url' => "http://" . $_SERVER['HTTP_HOST'] . $CONFIG['shop_assist_result_url'],
                                    'assist_fail_url' => $CONFIG['shop_assist_fail_url'],
                                    'assist_currency' => $CONFIG['shop_assist_currency'],
                                )
                            );
                        }
                        break;
                }
            }

            if ($dlvr_id) {
                $dlvr = $this->getDelivery($dlvr_id);
                $tpl->newBlock('block_order_delivery');
                $tpl->assign(array(
                    'delivery_cost' => $dlvr_cost,
                    'currency' => $CONFIG['catalog_currencies'][$CONFIG['shop_order_concurency']],
                    'title' => $dlvr ? $dlvr['title'] : '',
                    'dscr' => $dlvr ? $dlvr['dscr'] : '',
                ));
            }

            if ($admin) {
                foreach ($status_list as $val) {
                    $tpl->newBlock('block_order_status');
                    $val['selected'] = $val['id'] == $status ? 'selected' : '';
                    $tpl->assign($val);
                }
            }

            $this->show_items($transurl, $id, $cur);

            if ($status_hist) {
                $tpl->newBlock('block_status_history');
                $tpl->assign(Array(
                    "MSG_Status_changes_history" => $this->_msg["Status_changes_history"],
                    "MSG_Time" => $this->_msg["Time"],
                    "MSG_Old_stat" => $this->_msg["Old_stat"],
                    "MSG_New_stat" => $this->_msg["New_stat"],
                    "MSG_Info" => $this->_msg["Info"],
                    "MSG_Responsible" => $this->_msg["Responsible"],
                ));
                $query = "SELECT H.*, DATE_FORMAT(H.date,'" . $CONFIG["shop_date_format"] . "') as date, username FROM "
                    . $this->table_prefix . "_shop_order_history H LEFT JOIN core_users U ON manager_id=U.id  WHERE order_id=" . $id
                    . " ORDER BY date";
                $db->query($query);
                while ($db->next_record()) {
                    $tpl->newBlock('block_status_history_row');
                    $tpl->assign(
                        array(
                            'date' => $db->f('date'),
                            'old_status' => $status_list[$db->f('old_status')] ? $status_list[$db->f('old_status')]['title'] : $this->_msg['undef'],
                            'new_status' => $status_list[$db->f('new_status')] ? $status_list[$db->f('new_status')]['title'] : $this->_msg['undef'],
                            'info' => nl2br($db->f('info')),
                            'responsible' => $db->f('username') ? $db->f('username') : 'n\a'
                        )
                    );
                }
            }

            return TRUE;
        }
        return FALSE;
    }


    // вывести информацию о заказанных товарах по id заказа
    function show_items($transurl, $id = "", $order_currency = "")
    {
        $la_disc = true;
        $id = intval($id);
        if (!$id) return 0;
        global $CONFIG, $main, $db, $tpl, $baseurl, $core, $server, $lang;
        $admin = Module::is_admin();

        $query = "SELECT * FROM " . $this->table_prefix . "_shop_order_items WHERE order_id = $id ORDER BY id";
        $db->query($query);

        if ($db->nf() > 0) {
            while ($db->next_record()) {

                $cost = $db->f("item_cost");
                $cost = $main->process_price($cost);
                if (!$cost) $cost = "-";

                $price = $main->process_price($db->f("item_price"));
                if (!$price) $price = "-";

                $catalog_page = $core->formPageLink($CONFIG["catalog_page_link"], $CONFIG["catalog_page_address"], $lang);
                $item_link = ($admin) ? "/admin.php?name=catalog&action=editproduct&id=" . $db->f("item_id")
                    : $catalog_page . "?action=shwprd&id=" . $db->f("item_id");

                $gift_link = ($admin) ? "/admin.php?name=catalog&action=editproduct&id=" . $db->f("gift_id")
                    : $catalog_page . "?action=shwprd&id=" . $db->f("gift_id");
                //вывод на экран
                $info = unserialize($db->f("info"));
                $tpl->newBlock("block_item");
                $tpl->assign(array(
                    'item_id' => $db->f("item_id"),
                    'item_name' => $db->f("item_name"),
                    'item_link' => $item_link,
                    'gift_title' => $db->f("gift_title"),
                    'gift_link' => $gift_link,
                    'item_code' => $db->f("item_code"),
                    'item_currency' => $CONFIG['catalog_currencies'][$db->f("item_currency")],
                    'item_price' => $price,
                    'item_rate' => $db->f("item_rate"),
                    'item_qty' => $db->f("item_qty"),
                    'item_info' => $info['dscr'],
                    'item_cost' => $cost,
                    'order_currency' => $CONFIG['catalog_currencies'][$order_currency]
                ));

                if ($db->f("gift_id")) {
                    $tpl->newBlock('gift');
                    $tpl->assign('gift_title', $db->f("gift_title"));
                    $tpl->assign('gift_link', $gift_link);
                }

                if ($db->f("kupon")) {
                    $tpl->newBlock('kupon');
                    $tpl->assign('price', $db->f("kupon"));
                    //$tpl->assign('gift_link', $gift_link);
                }
                if ($db->f("percent")) {
                    $tpl->newBlock('discount');
                    $tpl->assign('price', $db->f("percent"));
                    if($la_disc){
                        $tpl->newBlock('block_discount_info');
                        $tpl->assign('title', 'с вычетом акционной скидки');
                        $la_disc = false;
                    }

                    //$tpl->assign('gift_link', $gift_link);
                }

                if ($db->f("double_product")) {
                    $tpl->newBlock('double_gift');
                    $double_link = ($admin) ? "/admin.php?name=catalog&action=editproduct&id=" . $db->f("double_product") : $catalog_page . "?action=shwprd&id=" . $db->f("double_product");
                    $tpl->assign('title', $db->f("double_title"));
                    $tpl->assign('link', $double_link);
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    function getBasketProds()
    {
        if (is_array($_SESSION["order"]["items"]) && sizeof($_SESSION["order"]["items"]) > 0) {
            foreach ($_SESSION["order"]["items"] as $item_idx => $item_array) {
                $arr[] = $item_array;
            }
            return $arr;
        }
        return FALSE;
    }

    function getProdsForShowInBasket($item)
    {
        GLOBAL $CONFIG, $baseurl, $core;
        $sizeOfItem = sizeof($item);
        if (is_array($item) && $sizeOfItem > 0) {
            $catalog_page = $core->formPageLink($CONFIG['catalog_page_link'], $CONFIG['catalog_page_address'], $lang);
            for ($i = 0; $i < $sizeOfItem; $i++) {
                if (3 == $_SESSION['siteuser']['client_type']) {
                    $price = Main::process_price($item[$i]['price_3']);
                } elseif (2 == $_SESSION['siteuser']['client_type']) {
                    $price = Main::process_price($item[$i]['price_2']);
                } else {
                    $price = Main::process_price($item[$i]['price_1']);
                }
                $sum = number_format($price * $item[$i]['quantity'], 2, ".", "");
                $arr[$i]['item_id'] = $item[$i]['id'];
                $arr[$i]['item_name'] = $item[$i]['name'];
                $arr[$i]['item_currency'] = $CONFIG['catalog_currencies'][$item[$i]['currency']];
                $arr[$i]['item_cost'] = $sum;
                $arr[$i]['item_sum'] = $sum;
                $arr[$i]['item_price'] = $price;
                $arr[$i]['item_price_1'] = (float)($item[$i]['price_1']);
                $arr[$i]['item_price_2'] = (float)($item[$i]['price_2']);
                $arr[$i]['item_price_3'] = (float)($item[$i]['price_3']);
                $arr[$i]['item_availability'] = $item[$i]['availability'];
                $arr[$i]['item_quantity'] = $item[$i]['quantity'];
                $arr[$i]['item_razm'] = $item[$i]['razm'];
                $arr[$i]['item_link'] = $catalog_page . '&action=shwprd&id=' . $item[$i]['id'];
                $arr[$i]['item_del_link'] = $baseurl . '&action=delete&id=' . $item[$i]['id'] . '&razm=' . $item[$i]['razm'];
            }
            return $arr;
        }
        return FALSE;
    }

    // Функция для вывода корзины покупок
    function showCart($promo = false)
    {
        global $CONFIG, $tpl, $lang, $transurl, $baseurl;
        //var_dump($_SESSION['order']);
        if (is_array($_SESSION['order']['items']) && sizeof($_SESSION['order']['items']) > 0) {
            $tpl->newBlock('block_cart');
            $ctlg = Module::load_mod('catalog', 'only_create_object');
            $catalog_page = Core::formPageLink($CONFIG['catalog_page_link'], $CONFIG['catalog_page_address'], $lang);
            if ($_SESSION["siteuser"]["id"]) {
                if (isset($_SESSION['orderinfo']['bonus'])) {
                    $balance = $this->getUserBonusBalance($_SESSION["siteuser"]["id"]);
                    if ($balance >= $_SESSION['orderinfo']['bonus']) {
                        $bonusV = $CONFIG['shop_bonus_cost'] * $_SESSION['orderinfo']['bonus'];
                    } else {
                        $bonusV = "";
                    }

                } else {
                    $bonusV = "";
                }
            } else {
                $bonusV = "";
            }
            
            if($promo){
	                     $bonusV = $_SESSION['orderinfo']['bonus'];
	                     }
                                
            if ($bonusV) {
                $costV = (int)$_SESSION['order']['cost'] - $bonusV;
            }
            $arrayAs = array(
                'basket_recal' => $baseurl . '&action=recal',
                'baseurl' => $baseurl,
                'del_all' => $baseurl . '&action=del_all',
                'discount_all' => $_SESSION['order']['discount'],
                'currencie' => $CONFIG['catalog_currencies'][$CONFIG['shop_order_concurency']],

                'process_order' => $baseurl . '&action=processorder'
            );
            if ($bonusV) {
                $arrayAs['item_all'] = $costV . ' ' . $CONFIG['catalog_currencies'][$CONFIG['shop_order_concurency']];
                $arrayAs['item_all_old'] = (int)$_SESSION['order']['cost'] . ' ' . $CONFIG['catalog_currencies'][$CONFIG['shop_order_concurency']];
            } else {
                $arrayAs['item_all'] = (int)$_SESSION['order']['cost'] . ' ' . $CONFIG['catalog_currencies'][$CONFIG['shop_order_concurency']];
            }
            $tpl->assign($arrayAs);
            if ($_SESSION['order']['discount']) {
                if ($_SESSION['order']['discount_is_simple']) {
                    $d = $this->getDiscount($_SESSION['order']['discount_id']);
                    if ($d) {
                        $tpl->newBlock('block_simple_discount');
                        $tpl->assign(array(
                            'id' => $_SESSION['order']['discount_id'],
                            'name' => $d['discount'],
                            'sum' => $_SESSION['order']['discount'],
                            'currencie' => $arr['currencie']));
                    }
                } else {
                    $d = $this->GetUsersDiscounts($_SESSION['order']['discount_id']);
                    if ($d && 1 == count($d)) {
                        $tpl->newBlock('block_accumulative_discount');
                        $tpl->assign(array(
                            'id' => $_SESSION['order']['discount_id'],
                            'name' => $d[0]['name'],
                            'sum' => $_SESSION['order']['discount'],
                            'currencie' => $arr['currencie']));
                    }
                }
            }
            $prod_list = $this->getCartProdList();
            $ord_prop_list = $ctlg->getOrderPropList();
            $weight = 0;
            $dimensions = array();
            foreach ($_SESSION["order"]["items"] as $key => $val) {
                foreach ($val as $k => $v) {
                    $weight += (int)$prod_list[$key]['weight'] * $val[0]['cnt'];
                    for( $h_f = 0; $h_f < $val[0]['cnt']; $h_f++){
                        $a = explode('x',$prod_list[$key]['dimensions'],3);
                        if(isset($a[2])){

                            $dimensions[] = array( 'weight' => (int)$prod_list[$key]['weight']/1000, 'w' => $a[0], 'l' => $a[1], 'h' => $a[2]);
                        }
                        else{
                            $dimensions[] = array( 'weight' => (int)$prod_list[$key]['weight']/1000, 'w' => 30, 'l' => 30, 'h' => 30);
                        }
                    }
                    $prod_list[$key]['key'] = $v['key'];
                    $prod_list[$key]['link'] = CatalogPrototype::getProdLink(
                        $catalog_page,
                        $prod_list[$key]['prod_alias'],
                        $prod_list[$key]['id'],
                        $prod_list[$key]['ctgr_alias']);
                    $prod_list[$key]['del_link'] = $baseurl . '&action=delete&id=' . $prod_list[$key]['id']
                        . ($prod_list[$key]['key'] ? "&key={$prod_list[$key]['key']}" : "");
                    $prod_list[$key]['cnt'] = $v['cnt'];
                    if ($v['choose'] == 3) {
                        $act_info = $this->getGiftToProd($prod_list[$key]['id']);
                        $prod_list[$key]['cost'] = number_format(($act_info[0][0]['price_1'] + $act_info[0][1]['price_1']) * $v['cnt'], 2, ".", "");
                        $prod_list[$key]['price'] = number_format($act_info[0][0]['price_1'] + $act_info[0][1]['price_1'], 2, ".", "");
                        //var_dump($prod_list[$key]['cost']);
                    }elseif ($v['choose'] == 5){
                        $prod_list[$key]['cost'] = number_format($prod_list[$key]['new_price_p'] * $v['cnt'], 2, ".", "");
                        $prod_list[$key]['price'] = number_format($prod_list[$key]['new_price_p'], 2, ".", "");
                    } elseif ($v['choose'] == 4){
                        $prod_list[$key]['cost'] = number_format($prod_list[$key]['price_1'] * $v['cnt'], 2, ".", "");
                        $prod_list[$key]['price'] = number_format($prod_list[$key]['price_1'], 2, ".", "");
                    } else {
                        $prod_list[$key]['cost'] = number_format($prod_list[$key]['price'] * $v['cnt'], 2, ".", "");
                        $prod_list[$key]['price'] = number_format($prod_list[$key]['price'], 2, ".", "");
                    }

                    $tpl->newBlock('block_cart_item');
                    $prod_list[$key]['price'] = (int)$prod_list[$key]['price'];

                    $tpl->assign($prod_list[$key]);
                    if ($prod_list[$key]['image'] && file_exists(RP . $CONFIG['catalog_img_path'] . $prod_list[$key]['image'])) {
                        $tpl->newBlock('block_cart_item_img');
                        $prod_list[$key]['img_path'] = "/" . $CONFIG['catalog_img_path'] . $prod_list[$key]['image'];
                    } else {
                        $tpl->newBlock('block_cart_item_no_img');
                    }
                    $tpl->assign($prod_list[$key]);
                    if ($v['choose'] && $v['choose'] != 5) {
                        $this->showGift($prod_list[$key]['id'], $v['choose']);
                    }
                    if (!empty($v['prop'])) {
                        $tpl->newBlock('block_cart_item_prop_list');
                        foreach ($v['prop'] as $pk => $pv) {
                            $tpl->newBlock('block_cart_item_prop');
                            $tpl->assign(array(
                                'title' => $ord_prop_list[$pk]['title'],
                                'name' => $ord_prop_list[$pk]['name'],
                                'val' => $ord_prop_list[$pk]['val'][$pv],
                                'prop_id' => $pk,
                                'val_id' => $pv,
                            ));
                        }
                    }

                    if ($prod_list[$key]['bonus']) {
                        $tpl->newBlock('bonus');
                        $tpl->assign('bonus', $prod_list[$key]['bonus'] * $v['cnt']);
                    }

                }
            }
            if ($_SESSION["siteuser"]["id"]) {
                $b = $this->getUserBonusBalance($_SESSION["siteuser"]["id"]);
                if ($b) {
                    $tpl->newBlock('bonus_balance');
                    $tpl->assign('bonus', $b);
                    
                }

            }
            $tpl->newBlock('discount_block');
            $tpl->newBlock('weight_b');
            $tpl->assign('weight', $weight / 1000);
            if(isset($_SESSION["siteuser"]) && $_SESSION["siteuser"]["id"] == '2'){
                $tpl->newBlock('dimensions_block');
                $tpl->assign('date' , date('Y-m-d'));
                foreach($dimensions as $count=>$d){
                    $tpl->newBlock('dimensions');
                    $tpl->assign(array_merge($d,array('c'=>$count)));
                }
            }

        } else {
            $tpl->newBlock('block_cart_empty');
        }
    }


    // Функция для повторения заказа
    function repeat_order($id)
    {
        global $CONFIG, $main, $db, $db2, $core, $server, $lang;

        $id = (int)$id;
        if (!$id) return FALSE;
        if (!$_SESSION["siteuser"]["id"]) return FALSE;
        $no_product_arr = array();

        $query = "SELECT * FROM " . $this->table_prefix . "_shop_orders WHERE id = $id AND siteuser_id = {$_SESSION["siteuser"]["id"]}";
        $db->query($query);

        if (!($db->nf() > 0)) return FALSE;

        $result = 1;
        $db->next_record();
        $id = $db->f("id");

        $query = "SELECT * FROM " . $this->table_prefix . "_shop_order_items WHERE order_id = $id";
        $db2->query($query);

        if ($db2->nf() > 0) {
            while ($db2->next_record()) {
                $info = unserialize($db2->f("info"));
                if (!$this->add_to_cart($db2->f("item_id"), $db2->f("item_qty"), $info['prop'])) {
                    $no_product_arr[] = $db2->f("item_name");
                }
            }

            // сформировать предупреждение о том, что товара/товаров нет в каталоге
            $c = count($no_product_arr);
            if ($c > 0) {
                $str = implode(", ", $no_product_arr);
                if ($c == 1) $result_message = $this->_msg["Now_good_absent"] . " $str.";
                else $result_message = $this->_msg["Now_goods_absent"] . ": $str.";
                if ($db2->nf() > 1) $result_message .= " " . $this->_msg["Another_goods_added_to_cart"];
                $result = -1;
                $_SESSION["order"]["result_message"] = $result_message;
            }
        }

        return $result;
    }


    // Функция для добавления товара в корзину покупок
    function add_to_cart($id = '', $cnt = '', $order_prop = false)
    {
        global $CONFIG, $main, $db, $server, $lang;
        $id = (int)$id;
        $ctlg = Module::load_mod('catalog', 'only_create_object');
        if (!$id || !$ctlg) {
            return FALSE;
        }
        $prod = $ctlg->getProduct($id);
        if (!$prod) {
            return false;
        }
        $cnt = $cnt > 1 ? (int)$cnt : 1;

        if (!isset($_SESSION["order"]["items"])) {
            $_SESSION["order"]["items"] = array();
            $_SESSION["order"]["sum"] = 0;
        }
        if (is_array($order_prop)) {
            foreach ($order_prop as $key => $val) {
                if (!$val) {
                    unset($order_prop[$key]);
                }
            }
        }
        /**
         * $_SESSION["order"]["items"] = array(
         *     product_id => array(
         *         0 => array(
         *             'prop' => array(prod_id => prod_val_id),
         *             'key' => key_for_poperties_values,
         *             'cnt' => count_of_products,
         *         ),
         *         ...
         *     ),
         *     ...
         * );
         *
         */

        $choose = (float)$_REQUEST['choose'];

        $prop_key = $this->getPropKey($choose);

        //var_dump($prop_key);
        if (in_array($id, array_keys($_SESSION["order"]["items"]))) {
            $find = false;
            foreach ($_SESSION["order"]["items"][$id] as $key => $val) {
                if ($val['key'] == $prop_key) {
                    $find = true;
                    $_SESSION["order"]["items"][$id][$key]['cnt'] += $cnt;
                    if ($choose) {
                        //$_SESSION["order"]["items"][$id][$key]['choose'] = $choose;
                        $choose = false;
                    }
                }
            }
            if (!$find) {
                $_SESSION["order"]["items"][$id][] = array(
                    'prop' => $order_prop,
                    'key' => $prop_key,
                    'choose' => $choose,
                    'cnt' => $cnt
                );
            }
        } else {
            $_SESSION["order"]["items"][$id][] = array(
                'prop' => $order_prop,
                'key' => $prop_key,
                'choose' => $choose,
                'cnt' => $cnt
            );
        }

        return TRUE;
    }

    // Функция для удаления одного товара из корзины покупок
    function delete_item_from_cart($id = '', $cart_key = '')
    {
        $ret = false;
        if ($id && isset($_SESSION['order']['items'][$id])) {
            while (!$ret && list($key, $val) = each($_SESSION['order']['items'][$id])) {
                if (($cart_key && $cart_key == $val['key'])
                    || (!$cart_key && !$val['key'])
                ) {
                    unset($_SESSION['order']['items'][$id][$key]);
                    $ret = true;
                    if (0 == count($_SESSION['order']['items'][$id])) {
                        unset($_SESSION['order']['items'][$id]);
                    }
                }
            }
            if (0 == count($_SESSION['order']['items'])) {
                unset($_SESSION['order']);
            }
        } else if (0 == func_num_args()) {
            unset($_SESSION['order']);
        }
        return $ret;
    }


    /**
     * Enter description here...
     *
     * @param unknown_type $customer
     * @param unknown_type $email
     * @param unknown_type $phone
     * @param unknown_type $country
     * @param unknown_type $region
     * @param unknown_type $city
     * @param unknown_type $zip_code
     * @param unknown_type $address
     * @param unknown_type $payment_type
     * @param unknown_type $organization
     * @param unknown_type $inn
     * @param unknown_type $rs
     * @param unknown_type $ks
     * @param unknown_type $bank
     * @param unknown_type $bik
     * @param unknown_type $additional
     * @return unknown
     */
    function process_order($customer, $email, $phone, $city, $address, $additional, $payment_type)
    {
        global $CONFIG, $main, $lang, $db;
        //$email = $_SESSION['siteuser']['id'] ? $_SESSION['siteuser']['email'] : $email;
        if ($customer && $email && is_array($_SESSION['order'])) {

            $user_id = (int)$_SESSION['siteuser']['id'];
            $user_type = $_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : 1;
            $sum = (int)$_SESSION['order']['sum'];
            $custumer = strip_tags($customer);
            $email = strip_tags($email);
            $phone = strip_tags($phone);
            //$country		= strip_tags($country);
            //$region			= strip_tags($region);
            $city = strip_tags($city);
            //$zip_code		= strip_tags($zip_code);
            $address = strip_tags($address);
            $payment_type = strip_tags($payment_type);
            //$organization	= strip_tags($organization);
            //$inn			= strip_tags($inn);
            //$rs				= strip_tags($rs);
            //$ks				= strip_tags($ks);
            //$bank			= strip_tags($bank);
            //$bik			= strip_tags($bik);
            $adds = strip_tags($additional);

            if ($organization) {
                $_SESSION['siteuser']['company'] = $organization;
            }

//            /* Если оплата WebMoney - учитываем скидку/надбавку */
//            if (4 == $payment_type && $CONFIG['shop_WM_discount']) {
//                //$_SESSION['order']['sum'] = $_SESSION['order']['sum'] + $_SESSION['order']['sum']*$CONFIG['shop_WM_discount']/100;
//
//                /* Учитываем скидку/надбавку WebMoney */
//                $_SESSION['order']['discount'] += $_SESSION['order']['cost']*$CONFIG['shop_WM_discount']/100;
//                $_SESSION['order']['cost']		= $_SESSION['order']['sum'] + $_SESSION['order']['discount'];
//            }
//
//            /* Если оплата ASSIST - учитываем скидку/надбавку */
//            if (5 == $payment_type && $CONFIG['shop_assist_discount']) {
//                /* Учитываем скидку/надбавку WebMoney */
//                $_SESSION['order']['discount'] += $_SESSION['order']['cost']*$CONFIG['shop_assist_discount']/100;
//                $_SESSION['order']['cost']		= $_SESSION['order']['sum'] + $_SESSION['order']['discount'];
//            }

            $_SESSION['order']['sum'] = number_format($_SESSION['order']['sum'], 2, ".", "");
            $_SESSION['order']['discount'] = number_format($_SESSION['order']['discount'], 2, ".", "");
            $_SESSION['order']['cost'] = number_format($_SESSION['order']['cost'], 2, ".", "");

            $dlvr = $this->getDelivery($_SESSION['order']['delivery_id']);

            $__n = $main->_msg["NO"];
            $addr = $custumer ? $this->_msg["FIO"] . ': ' . $custumer . "\n" : $this->_msg["FIO"] . ': ' . $__n . "\n";
            $addr .= $phone ? $this->_msg["Phone"] . ': ' . $phone . "\n" : $this->_msg["Phone"] . ': ' . $__n . "\n";
            //$addr .= $zip_code			? $this->_msg["Zip_code"] . ': '.$zip_code."\n"		: $this->_msg["Zip_code"] .': '.$__n."\n";
            //$addr .= $region				? $this->_msg["Region"] . ': '.$region."\n"			: $this->_msg["Region"] .': '.$__n."\n";
            $addr .= "Область/Край: " . $_REQUEST['area'] . " \n";
            $addr .= "Регион: " . $_REQUEST['region'] . " \n";
            $addr .= "Почтовый индекс: " . $_REQUEST['index'] . " \n";
            $addr .= "Город: " . $_REQUEST['city'] . " \n";
            $addr .= $address ? $this->_msg["Address"] . ': ' . $address . "\n" : $this->_msg["Address"] . ': ' . $__n . "\n";
            $addr .= $email ? $this->_msg["Email"] . ': ' . $email . "\n" : $this->_msg["Email"] . ': ' . $__n . "\n";
            $addr .= $payment_type ? $this->_msg["Payment_type"] . ': ' . $CONFIG['order_pay_types'][$payment_type] . "\n" : $this->_msg["Payment_type"] . ': ' . $__n . "\n";
            //$addr .= "\n";
            //$addr .= $organization	? $this->_msg["Organization"] . ': '.$organization."\n"	: $this->_msg["Organization"] .': '.$__n."\n";
            //$addr .= $inn						? $this->_msg["INN_KPP"] . ': '.$inn."\n"				: $this->_msg["INN_KPP"] .': '.$__n."\n";
            //$addr .= $rs						? $this->_msg["RS"] . ': '.$rs."\n"						: $this->_msg["RS"] .': '.$__n."\n";
            //$addr .= $ks						? $this->_msg["KS"] . ': '.$ks."\n"						: $this->_msg["KS"] .': '.$__n."\n";
            //$addr .= $bank					? $this->_msg["Bank"] . ': '.$bank."\n"					: $this->_msg["Bank"] . ': '.$__n."\n";
            //$addr .= $bik						? $this->_msg["BIK"] . ': '.$bik."\n"					: $this->_msg["BIK"] .': '.$__n."\n";
            $addr .= $adds ? $this->_msg["Adding_info"] . ': ' . $adds . "\n" : "";

            //	$addr	.= "\n{$this->_msg["Order"]}:\n";

            //	$org	= $organization			? $this->_msg["Organization"] . ': '.$organization."\n"	: $this->_msg["Organization"] .': '.$__n."\n";
            //	$org	.= $inn						? $this->_msg["INN_KPP"] . ': '.$inn."\n"				: $this->_msg["INN_KPP"] .': '.$__n."\n";
            //	$org	.= $rs						? $this->_msg["RS"] . ': '.$rs."\n"						: $this->_msg["RS"] .': '.$__n."\n";
            //	$org	.= $ks						? $this->_msg["KS"] . ': '.$ks."\n"						: $this->_msg["KS"] .': '.$__n."\n";
            //	$org	.= $bank					? $this->_msg["Bank"] . ': '.$bank."\n"					: $this->_msg["Bank"] . ': '.$__n."\n";
            //	$org	.= $bik						? $this->_msg["BIK"] . ': '.$bik."\n"					: $this->_msg["BIK"] .': '.$__n."\n";

            //@$adr	.= $zip_code				? $this->_msg["Zip_code"] . ': '.$zip_code."\n"	: $this->_msg["Zip_code"] .': '.$__n."\n";
            //@$adr	.= $region					? $this->_msg["Region"] . ': '.$region."\n"		: $this->_msg["Region"] .': '.$__n."\n";
            //@$adr	.= $city					? $this->_msg["City"] . ': '.$city."\n"			: $this->_msg["City"] .': '.$__n."\n";

            $adr .= "Область/Край: " . $_REQUEST['area'] . " \n";
            $adr .= "Регион: " . $_REQUEST['region'] . " \n";
            $adr .= "Почтовый индекс: " . $_REQUEST['index'] . " \n";
            $adr .= "Город: " . $_REQUEST['city'] . " \n";

            $adr .= $address ? $this->_msg["Address"] . ': ' . $address . "\n" : $this->_msg["Address"] . ': ' . $__n . "\n";

            $ctlg = Module::load_mod('catalog', 'only_create_object');
            $prod_list = $this->getCartProdList();
            $ord_prop_list = $ctlg->getOrderPropList();
            foreach ($_SESSION["order"]["items"] as $prod_id => $val) {
                foreach ($val as $k => $v) {
                    $prod .= $this->_msg["Product_ID"] . ': ' . $prod_id . "\n";
                    $prod .= $this->_msg["Product_name"] . ': ' . $prod_list[$prod_id]['title'] . "\n";
                    $ret = $this->getGiftToProd($prod_id);
                    if ($ret) {
                        $gift = $ret[1];
                        if (floor($v['choose']) == 9) {
                            $el = $v['choose'] - floor($v['choose']);
                            $el = $el * 10;
                            if ($el) $el = $el - 2;
                            if ($el) $gift = $ret[0][$el];
                            else $gift = $ret[0][1];
                        }
                        if ($v['choose'] == 2 && $ret[0][0]['kupon']) $prod .= ' +Купон на ' . $ret[0][0]['kupon'] . "р\n";
                        elseif ($v['choose'] != 3 && $v['choose'] != 0 && $gift) $prod .= ' +Подарок : ' . $gift['product_title'] . "\n";
                        elseif ($v['choose'] == 3 && $gift) {
                            if ($ret[0][0]['id'] == $prod_id)
                                $double_gift = $ret[0][1]['product_title'];
                            else
                                $double_gift = $ret[0][0]['product_title'];
                            $prod .= '+ ' . $double_gift . "\n";
                            $prod .= '+Подарок : ' . $gift['product_title'] . "\n";
                            $prod_list[$prod_id]['price'] = number_format($ret[0][0]['price_1'] + $ret[0][1]['price_1'], 2, ".", "");
                        } elseif ($v['choose'] == 9 && $gift) {
                            if ($ret[0][0]['id'] == $prod_id)
                                $double_gift = $ret[0][1]['product_title'];
                            else
                                $double_gift = $ret[0][0]['product_title'];
                            $prod .= '+ ' . $double_gift . "\n";
                            $prod .= '+Подарок : ' . $gift['product_title'] . "\n";
                            $prod_list[$prod_id]['price'] = number_format($ret[0][0]['price_1'] + $ret[0][1]['price_1'], 2, ".", "");
                        }

                    }
                    //$prod .= $prod_list[$prod_id]['currency'] ? $this->_msg["Currency"] . ': '.$CONFIG['catalog_currencies'][$prod_list[$prod_id]['currency']]."\n" : '';

                    $prod .= $this->_msg["Price"] . ': ' . ((int)$prod_list[$prod_id]['price']) . $CONFIG['catalog_currencies'][$prod_list[$prod_id]['currency']] . "\n";
                    $prod .= $prod_list[$prod_id]['availability'] ? $this->_msg["Presence"] . ': ' . $prod_list[$prod_id]['availability'] . "\n" : '';
                    $prod .= $this->_msg["Quantity"] . ': ' . $v['cnt'] . "\n";
                    if (!empty($v['prop'])) {
                        $prod .= $this->_msg["Params"] . ': ';
                        foreach ($v['prop'] as $pk => $pv) {
                            $prod .= $ord_prop_list[$pk]['title'] . ' - ' . $ord_prop_list[$pk]['val'][$pv] . '; ';
                        }
                        $prod .= "\n";
                    }
                    $prod .= "\n";

                }
            }
            $addr .= "\nЗаказ:\n" . $prod;
            $addr .= $this->_msg["Order_sum"] . ': ' . (int)$_SESSION['order']['sum'] . ' ' . $CONFIG['catalog_currencies'][$_SESSION['order']['currency']] . "\n";
            $addr .= $this->_msg["Discount"] . ': ' . (int)$_SESSION['order']['discount'] . ' ' . $CONFIG['catalog_currencies'][$_SESSION['order']['currency']] . "\n";
            //$order['delivery_id']==6 ? 'оплата по факту получения уточняйте у менеджера' : '',
            $del_cost = $_SESSION['order']['delivery_id'] == 6 ? 'оплата по факту получения от 300 руб. уточняйте у менеджера' : (int)$dlvr['cost'] . ' ' . $CONFIG['catalog_currencies'][$_SESSION['order']['currency']];
            $addr .= $dlvr ? $this->_msg["Delivery"] . ': ' . $del_cost . ', ' . $dlvr['title'] . "\n" : '';
            $c_sum = 0;
            if (isset($_POST['api_ems']) ) {
                if ($_POST['api_ems']) {
                    $delivApi = (float)$_POST['api_ems'];
                    if ($_POST['api_ems'] == "moscov") {
                        $delivApi = 0;
                    }
                    $c_sum += $delivApi;

                }
            }

            $c_sum += (int)$_SESSION['order']['cost'];

            $addr .= $this->_msg["Total_order_sum"] . ': ' . $c_sum . ' ' . $CONFIG['catalog_currencies'][$_SESSION['order']['currency']] . "\n";

//            /* Если оплата WebMoney - учитываем скидку/надбавку */
//            if (4 == $payment_type && $CONFIG['shop_WM_discount']) {
//                $addr .= $this->_msg["Webmoney_percent"] . ': '.$CONFIG['shop_WM_discount']." %\n";
//            }
//            /* Если оплата ASSIST - учитываем скидку/надбавку */
//            if (5 == $payment_type && $CONFIG['shop_assist_discount']) {
//                $addr .= $this->_msg["ASSIST_percent"] . ': '.$CONFIG['shop_assist_discount']." %\n";
//            }
            $order_id = $this->add_order($adr, $custumer, $email, $phone, $city, $adds, $payment_type);
            $subject = "{$this->_msg["Order"]} №{$order_id}. {$this->_msg['From']} {$custumer} {$this->_msg['on_site']} {$CONFIG['sitename_rus']}";
            $addr = "{$this->_msg["Order"]} №{$order_id}\n" . $addr;

            require_once(RP . '/inc/class.Mailer.php');
            $mail = new Mailer();
            //echo $CONFIG['shop_order_recipient']; die();
            if ($payment_type == 2) { //тип квитанция на оплату
                if ((strripos($_REQUEST['city'], "Москва") !== false) || (strripos($_REQUEST['city'], "москва") !== false) || (strripos($_REQUEST['region'], "МОСКВА") !== false)) {

//                 //$mail->AddEmbeddedImage(RP.'i/', 'cirlogo.jpg', 'cirlogo', 'base64', 'image/jpeg');
//                 $tpl1 = new TemplatePower(RP.'_shop_waybill.html', T_BYFILE);
//                 $items=$this->getOrderItems($order_id);
//                 $orederInfo=$this->getOrder($order_id);
//                 $i=1;
//                 $tableData="";
//                 foreach($items as $item)
//                 {
//                     $tableData=$tableData."<tr><td>$i</td><td>".$item['item_name']."</td><td>".$item['item_price']."</td><td>".$item['item_qty']."</td><td>шт.</td><td>".$item['item_price']."</td></tr>";
//                     $i++;
//                 }
//                 $tableData=$tableData."<tr><td>".$i."</td><td>Доставка</td><td>".$dlvr['cost']."</td><td>1</td><td>шт.</td><td>".$dlvr['cost']."</td></tr>";
//                 $i++;
//                 $tableData=$tableData."<tr><td>".$i."</td><td>Транспортные услуги</td><td>".$orederInfo['transp']."</td><td>1</td><td>шт.</td><td>".$orederInfo['transp']."</td></tr>";
//                 $count=$i;
//                 $i++;
//                 $k=$i+3;
//                 for($i;$i<$k;$i++)
//                 {
//                     $tableData=$tableData."<tr><td>$i</td><td></td><td></td><td></td><td>шт.</td><td></td></tr>";
//                 }
//                 $date=$this->rusdate(time());
//                 $tpl1->prepare();
//                 $tpl1->assign(array(
//                     "order_id"=>$order_id,
//                     "date" =>iconv('utf-8','windows-1251',$date),
//                     "table_data"=>iconv('utf-8','windows-1251',$tableData),
//                     "item_count"=>$count,
//                     "summ"=>iconv('utf-8','windows-1251',$c_sum),//$_SESSION['order']['sum']),
//                     "summ_p"=>iconv('utf-8','windows-1251',$orederInfo['cost_rur_in_words'])
//
//                 ));
                    $label_moscow = true;
                } else {
                    $tpl1 = new TemplatePower(RP . 'tpl/sup_' . $server . $lang . '/_kvit_template.html', T_BYFILE);//'tpl/sup_ было
                    $tpl1->prepare();
                    $tpl1->assign(array(
                        'name' => iconv('utf-8', 'windows-1251', $customer),
                        'num' => $order_id,
                        'company' => iconv('utf-8', 'windows-1251', $CONFIG['shop_company']),
                        'inn' => iconv('utf-8', 'windows-1251', $CONFIG['shop_inn']),
                        'rs' => iconv('utf-8', 'windows-1251', $CONFIG['shop_sett_account']),
                        'bank' => iconv('utf-8', 'windows-1251', $CONFIG['shop_bank']),
                        'bik' => iconv('utf-8', 'windows-1251', $CONFIG['shop_bik']),
                        'corr' => iconv('utf-8', 'windows-1251', $CONFIG['shop_corr_account']),

                        'address' => iconv('utf-8', 'windows-1251', $address),
                        'day' => date('d'),
                        'month' => iconv('utf-8', 'windows-1251', $this->rusdate(time(), '%MONTH%')),
                        'year' => date('Y'),
                        'total' => (double)$c_sum,//$_SESSION['order']['sum'],

                    ));
                    $f = fopen(RP . "kvitancia.htm", "w+");
                    fwrite($f, $tpl1->getOutputContent());
                    fclose($f);
                }

            }

            $mail->CharSet = $CONFIG['email_charset'];
            $mail->ContentType = "text/html"; //$CONFIG['email_type'];
            $mail->From = $CONFIG['shop_order_recipient'];// ---------$email;
            $mail->Mailer = 'mail';
            $mail->AddAddress($CONFIG['shop_order_recipient']);///$CONFIG['shop_order_recipient']
            $mail->Subject = $subject;
            $mail->Body = nl2br($addr . "\n Если у вас возникнут вопросы по вашему заказу, вы можете связаться с нашими менеджерами по телефонам:  7 (800) 775-49-37 или написать нам на электронную почту shop@supra.ru");
            $mail->Send();
            $mail->ClearAddresses();
            //$mail = new Mailer();
            $addr = $this->_msg["Order_confirmed"] . "\n\n" . $addr;
//            $mail->CharSet		= $CONFIG['email_charset'];
//            $mail->ContentType	= "text/html"; //$CONFIG['email_type'];
//            $mail->Mailer		= 'mail';
//            $mail->From			= $CONFIG['shop_order_recipient'];
//            $mail->FromName		= $CONFIG['sitename_rus'];
            $mail->AddAddress($email);
//            $mail->Subject		= $subject;
            $mail->Body = nl2br($addr . "\n Если у вас возникнут вопросы по вашему заказу, вы можете связаться с нашими менеджерами по телефонам:  7 (800) 775-49-37 или написать нам на электронную почту shop@supra.ru");
            if ($payment_type == 2 && !isset($label_moscow)) $mail->AddAttachment(RP . 'kvitancia.htm');
            $mail->Send();

            $this->sendSms($phone, $order_id, 'new', true);
            unset($_SESSION['order']);

            return $order_id;
        }
        return FALSE;
    }

    function getAddress($data)
    {
        if ($data) {
            if ($data['index']) {
                $address .= $data['index'];
                $_SESSION['siteuser']['index'] = $data['index'];
            } else {
                $_SESSION['siteuser']['index'] = '';
            }
            if ($data['obl_kray']) {
                if ($address) {
                    $address .= ', ';
                }
                $address .= $data['obl_kray'];
                $_SESSION['siteuser']['obl_kray'] = $data['obl_kray'];
            } else {
                $_SESSION['siteuser']['obl_kray'] = '';
            }
            if ($data['region']) {
                if ($address) {
                    $address .= ', ';
                }
                $address .= $data['region'];
                $_SESSION['siteuser']['region'] = $data['region'];
            } else {
                $_SESSION['siteuser']['region'];
            }
            if ($data['naspunkt']) {
                if ($address) {
                    $address .= ', ';
                }
                $address .= $data['naspunkt'];
                $_SESSION['siteuser']['naspunkt'] = $data['naspunkt'];
            } else {
                $_SESSION['siteuser']['naspunkt'];
            }
            if ($data['address']) {
                if ($address) {
                    $address .= ', ';
                }
                $address .= $data['address'];
                $_SESSION['siteuser']['address'] = $data['address'];
            } else {
                $_SESSION['siteuser']['address'];
            }
            return $address;
        }
        return FALSE;
    }

    // Функция для добавления заказа в БД
    function add_order($adr, $custumer, $email, $phone, $city, $adds, $payment_type = '')
        //$address, $org = '', $adds = '', $custumer = '', $email = '', $phone = '', $payment_type = '', $notify = 0)
    {
        global $CONFIG, $PAGE, $db, $server, $lang;

        $user_id = (int)$_SESSION['siteuser']['id'];
//        $db->query('SELECT * FROM core_users WHERE id = '.$user_id);
//        if ($db->nf() > 0) {
//            for ($i = 0; $i < $db->nf(); $i++) {
//                $db->next_record();
//                $user[$i]['id']				= $db->f('id');
//                $user[$i]['group_id ']		= $db->f('group_id');
//                $user[$i]['client_type']	= $db->f('client_type ');
//                $user[$i]['siteusername']	= $db->f('username');
//                $user[$i]['password']		= $db->f('password');
//                $user[$i]['surname']		= $db->f('surname');
//                $user[$i]['firstname']		= $db->f('firstname');
//                $user[$i]['company']		= $db->f('company');
//                $user[$i]['activity']		= $db->f('activity');
//                $user[$i]['post']			= $db->f('post');
//                $user[$i]['phone']			= $db->f('phone');
//                $user[$i]['email']			= $db->f('email');
//                $user[$i]['last_visit']		= $db->f('last_visit');
//                $user[$i]['is_new']			= $db->f('is_new');
//                $user[$i]['active']			= $db->f('active');
//                $user[$i]['get_subscr']		= $db->f('get_subscr');
//            }
//        }
        $sum = (double)$_SESSION['order']['sum'];
        $cost = (double)$_SESSION['order']['cost'];

        $discount = (double)$_SESSION['order']['discount'];
        if (isset($_SESSION['orderinfo']['bonus'])) {
            $bonusV = $CONFIG['shop_bonus_cost'] * $_SESSION['orderinfo']['bonus'];
            $discount = $discount + $bonusV;
            $cost = $cost - $bonusV;

        }


        $org = (isset ($org)) ? $db->escape($org) : '';
        $adds = (isset ($adds)) ? $db->escape($adds) : '';
        $currency = $db->escape($_SESSION['order']['currency']);
        $address = $db->escape($adr);
        $organization = !empty($_SESSION['siteuser']['company']) ? $db->escape($_SESSION['siteuser']['company']) : '';
        $custumer = $custumer ? $custumer : $user[0]['surname'] . ' ' . $user[0]['firstname'];
        $email = $email ? $email : $user[0]['email'];
        $phone = $phone ? $phone : $user[0]['phone'];
        $payment_type = (isset($payment_type) && $payment_type > 1) ? (int)$payment_type : $CONFIG['order_default_pay_type'];
        $notify = $notify ? 1 : 0;

//        $user_id = intval($user[0]['id']);
        $dlvr_id = $dlvr_cost = 0;
        if ($_SESSION['order']['delivery_id'] && ($dlvr = $this->getDelivery($_SESSION['order']['delivery_id']))) {
            $dlvr_id = $dlvr['id'];
            $dlvr_cost = (float)$dlvr['cost'];
            if (isset($_POST['api_ems'])) {
                if ($_POST['api_ems']) {
                    $delivApi = (float)$_POST['api_ems'];
                    if ($_POST['api_ems'] == "moscov") {
                        $delivApi = 0;
                    }
                    $cost += $delivApi;
                }
            }
        }
        $user_id = isset($user_id) ? $user_id : 0;
        if (!$user_id) {
            $user_id = $this->getIssetUser($email, $phone);
        }
        $sql = " INSERT INTO {$this->table_prefix}_shop_orders
					(
					 customer,
					 `type`,
					 siteuser_id,
					 delivery_id, 
					 delivery_cost, 
					 phone, 
					 address,
					 email, 
					 organization, 
					 jpinfo, 
					 `date`, 
					 addinfo, 
					 `sum`,
					 discount,
					 total_sum,
					 currency, 
					 pay_type, 
					 `session`,
					 promo_code,
					 `notify`
					  )
					 
		             VALUES
					 (
					   '" . $custumer . "', 
					   '" . $CONFIG['siteusers_client_types'][$_SESSION['siteuser']['client_type']] . "',
		               '" . $user_id . "', 
					   '" . $dlvr_id . "',
					   '" . $dlvr_cost . "', 
					   '" . $phone . "' ,
					   '" . $address . "',
			           '" . $email . "',
					   '" . $organization . "', 
					   '" . $org . "', 
					   NOW(),
					   '" . $adds . "',
					   '" . $sum . "' ,
					   '" . $discount . "',
			           '" . $cost . "', 
					   '" . $currency . "',
					   '" . $payment_type . "',
					   '" . ($user_id ? '' : session_id()) . "',
					   '" . ($_SESSION['orderinfo']['promo_code'] ? $_SESSION['orderinfo']['promo_code'] : '') . "',
                       '" . $notify . "')";
                       
        // echo $sql; die();

        $db->query($sql);
        if ($db->affected_rows() > 0) {
            $lid = $db->lid();
            $this->add_order_items($lid);
//            //БОНУС
//            if($user_id)
//            {
//                $bonus=0;
//                $prod_list = $this->getCartProdList();
//                foreach ($_SESSION["order"]["items"] as $key => $val) {
//                    foreach ($val as $k => $v) {
//
//                        $bonus+=$prod_list[$key]['bonus']*$v['cnt'];
//                    }
//                }
//                    if($bonus > 0)
//                    $db->set("core_users_bonus_data",array("user_id"=>$user_id,"order_id"=>$lid,"bonus"=>$bonus,"status"=>0));
//
//                if(isset($_SESSION['orderinfo']['bonus']))
//                {
//                    if($_SESSION['orderinfo']['bonus'] > 0)
//                    $db->set("core_users_bonus_data",array("user_id"=>$user_id,"order_id"=>$lid,"bonus"=>$_SESSION['orderinfo']['bonus'],"status"=>3));
//                }
//            }
            $adr = $db->getArrayOfResult('Select mail,`active` FROM `sup_rus_news_users` Where  `mail`="' . $email . '"');
            if (empty($adr)) {
                $db->query("INSERT INTO `sup_rus_news_users` (`mail`, `news_id`, `active`) VALUES ( '{$email}', '1', '1');");
            }
            return $lid;
        } else {
            return FALSE;
        }
    }


    // Функция для добавления заказанных товаров в БД
    function add_order_items($id = 0)
    {
        global $CONFIG, $db, $server, $lang;

        $id = (int)$id;
        if (!$id) {
            return FALSE;
        }

        $ctlg = Module::load_mod('catalog', 'only_create_object');
        $prod_list = $this->getCartProdList();
        $ord_prop_list = $ctlg->getOrderPropList();
        $ins = array();
        $discount_price =0;
        foreach ($_SESSION["order"]["items"] as $prod_id => $val) {
            //if(stripos($ids,$prod_id) && $ids)continue;
            foreach ($val as $k => $v) {
                $percent = 0;
                $inf = '';
                if (!empty($v['prop'])) {
                    foreach ($v['prop'] as $pk => $pv) {
                        $inf .= $ord_prop_list[$pk]['title'] . ' - ' . $ord_prop_list[$pk]['val'][$pv] . '; ';
                    }
                    $inf = serialize(array('dscr' => $inf, 'prop' => $v['prop']));
                }


                $title = mysql_real_escape_string($prod_list[$prod_id]['title']);
                $code = mysql_real_escape_string($prod_list[$prod_id]['code']);

                $gift_id = 0;
                $double_gift = 0;
                $double_title = '';
                $gift_title = '';
                $kupon = 0;
                $ret = $this->getGiftToProd($prod_id);
                if ($ret) {

                    $gift = $ret[1];
                    if (floor($v['choose']) == 9) {
                        $el = $v['choose'] - floor($v['choose']);
                        $el = $el * 10;
                        if ($el) $el = $el - 2;
                        if ($el) $gift = $ret[0][$el];
                        else $gift = $ret[0][1];
                    }
                    if($v['choose'] == 5){
                        $percent = $gift['percent'];
                        $discount_price += number_format(($prod_list[$prod_id]['price_1'] - $prod_list[$prod_id]['new_price_p'])*$v['cnt'], 2, ".", "");
                    }
                    elseif ($gift && $v['choose'] != 2 && $v['choose'] != 0) {

                        $gift_id = $gift['id'];
                        $gift_title = mysql_real_escape_string($gift['product_title']);

                    } elseif ($v['choose'] == 2 && $ret[0][0]['kupon']) {
                        $kupon = $ret[0][0]['kupon'];
                    } elseif (floor($v['choose']) == 9) {
                        $gift_id = $gift['id'];
                        $gift_title = mysql_real_escape_string($gift['product_title']);
                    }

                    if ($v['choose'] == 3) {

                        $act_info = $this->getGiftToProd($prod_id);
                        $prod_list[$prod_id]['cost'] = number_format(($act_info[0][0]['price_1'] + $act_info[0][1]['price_1']) * $v['cnt'], 2, ".", "");
                        $prod_list[$prod_id]['price'] = number_format($act_info[0][0]['price_1'] + $act_info[0][1]['price_1'], 2, ".", "");

                        if ($act_info[0][0]['id'] == $prod_id) {
                            $double_gift = $ret[0][1]['id'];
                            $double_title = $ret[0][1]['product_title'];
                        } else {
                            $double_gift = $ret[0][0]['id'];
                            $double_title = $ret[0][0]['product_title'];
                        }
                    }

                }
                $price = $prod_list[$prod_id]['price'];

                $rate = $_SESSION['catalog_rates'][$CONFIG['shop_order_concurency'] . '/' . $prod_list[$prod_id]['currency']];
                $sum = $this->exchange($price * $v['cnt'], $prod_list[$prod_id]['currency'], $CONFIG['shop_order_concurency']);
                $ins[] = "({$id}, {$prod_id}, '{$title}', '{$code}', '{$price}', "
                    . "'{$prod_list[$prod_id]['currency']}', '{$rate}', "
                    . "'{$v['cnt']}', '{$sum}', '{$inf}' ,'{$gift_id}', '{$gift_title}', '{$kupon}', {$double_gift}, '{$double_title}', '{$percent}')";

            }
        }
        if (!empty($ins)) {
            $db->query("INSERT INTO {$this->table_prefix}_shop_order_items "
                . "(order_id, item_id, item_name, item_code, item_price, "
                . "item_currency, item_rate, item_qty, item_cost, info, gift_id, gift_title, kupon, double_product, double_title, percent) VALUES "
                . implode(", ", $ins));

            //$this->do_export_XML();
        }
        if($discount_price){
            $db->query("Update {$this->table_prefix}_shop_orders SET discount = discount + ".$discount_price."  WHERE id=".$id);
        }

        return TRUE;
    }


    // обработка цены
    function get_item_price($qty, $item_array = array())
    {
        // для Сеула:
        if ($_SESSION["siteuser"]["is_authorized"]) {
            $price = $item_array["price{$_SESSION["siteuser"]["client_type"]}"];
        } else {
            $price = $item_array["price1"];
        }
        return preg_replace("/[^0-9]/", "", $price);
    }


    // Функция для пересчета корзины покупок
    function recalculate_cart($prods = array(), $delivery = 0)
    {
        global $CONFIG;
		$elementsDisc = array();
        $prod_list = $this->getCartProdList();
        $cods = $this->getAllActionMainProducts();
        //var_dump($CONFIG['shop_order_concurency']);
        if (!empty($prod_list)) {
            $keys = array();
            $komplekti = $this->getAllActionKomplektProducts();
            foreach ($_SESSION["order"]["items"] as $key => $val) {
                if (!array_key_exists($key, $prod_list)) {
                    unset($_SESSION["order"]["items"][$key]);
                } else {
                    foreach ($val as $k => $v) {
                        if (isset($prods[$key])) {
                            $cart_key = $v['key'] ? $v['key'] : 0;
                            if ($prods[$key][$cart_key] <= 0) {
                                $this->delete_item_from_cart($key, $cart_key);
                                $v['cnt'] = 0;
                            } else {
                                $v['cnt'] = $_SESSION["order"]["items"][$key][$k]['cnt'] = (int)$prods[$key][$cart_key];
                            }
                        }
                        if ($v['cnt'] > 0) {

                            if ($v['choose'] == 3) {
                                $act_info = $this->getGiftToProd($prod_list[$key]['id']);
                                $prod_list[$key]['cost'] = number_format(($act_info[0][0]['price_1'] + $act_info[0][1]['price_1']) * $v['cnt'], 2, ".", "");
                                $prod_list[$key]['price'] = number_format($act_info[0][0]['price_1'] + $act_info[0][1]['price_1'], 2, ".", "");

                            } elseif($v['choose'] == 4){
                                $prod_list[$key]['price'] = $prod_list[$key]['price_1'];
                            }
                            elseif($v['choose'] == 5){
                                $new_sum = $this->getNewProductPrice($prod_list[$key]['id'],true);
                                if ($new_sum) $prod_list[$key]['price'] = $new_sum;
                            }
                            else{
                                $new_sum = $this->getNewProductPrice($prod_list[$key]['id']);
                                if ($new_sum) $prod_list[$key]['price'] = $new_sum;
                            }

                            $prod_sum = ShopPrototype::exchange($v['cnt'] * $prod_list[$key]['price'], $prod_list[$key]['currency'], $CONFIG['shop_order_concurency']);
                            
							if(!in_array($prod_list[$key]['id'], $cods)){
								$elementsDisc[$prod_list[$key]['category_id']] = $prod_sum;
							}
                            $sum += $prod_sum;
                            $total += $v['cnt'];
                        }
                    }
                    $keys[] = $key;
                }
            }
            $_SESSION['order']['sum'] = number_format(round($sum), 2, ".", ""); // Полная стоимость
            $accmlt_dscnt = $this->getAccumulativeDiscount($_SESSION['order']['sum'], $CONFIG['shop_order_concurency']); //Накопительная скидка
            $simple_dscnt = $this->getSimpleDiscount($_SESSION['order']['sum'], $CONFIG['shop_order_concurency']);
            $_SESSION['order']['discount'] = 0;
            $_SESSION['order']['discount_id'] = 0;
            $_SESSION['order']['discount_is_simple'] = 0;
            if ($simple_dscnt && $simple_dscnt['discount_sum'] >= $accmlt_dscnt['discount_sum']) {
                $_SESSION['order']['discount'] = $simple_dscnt['discount_sum'];
                $_SESSION['order']['discount_id'] = $simple_dscnt['discount_id'];
                $_SESSION['order']['discount_is_simple'] = 1;
            } else if ($accmlt_dscnt && $accmlt_dscnt['discount_sum'] > $simple_dscnt['discount_sum']) {
                $_SESSION['order']['discount'] = $accmlt_dscnt['discount_sum'];
                $_SESSION['order']['discount_id'] = $accmlt_dscnt['id'];
                $_SESSION['order']['discount_is_simple'] = 0;
            }
            $_SESSION['order']['delivery_id'] = 0;
            $_SESSION['order']['delivery_cost'] = 0;
            $dlvr = $this->getDelivery($delivery);
            if ($dlvr) {
                $_SESSION['order']['delivery_id'] = $dlvr['id'];
                $_SESSION['order']['delivery_cost'] = $dlvr['cost'] > 0 ? $dlvr['cost'] : 0;
            }
            if($komplekti){
                foreach($komplekti as $komplekt){
                    $i=0;
                    $second = '';
                    $firstEl = '';
                    foreach($komplekt as $element => $skidka ){
                        $keyKom = reset(array_keys($skidka));
                        $skidkaKom = $skidka[$keyKom];

                        if($i==0){
                            $firstEl = $keyKom;
                        }
                        else{
                            $second = $keyKom;
                        }

                        if(in_array($keyKom,array_keys($_SESSION["order"]["items"]))){
                            $i++;
                        }
                        if($i == 2){

                        }
                    }

                    if($_SESSION["order"]["items"][$firstEl][0]['cnt'] < $_SESSION["order"]["items"][$second][0]['cnt']){
                        $_SESSION['order']['discount'] += $_SESSION["order"]["items"][$firstEl][0]['cnt'] * $skidkaKom;
                    }
                    else{
                        $_SESSION['order']['discount'] += $_SESSION["order"]["items"][$second][0]['cnt'] * $skidkaKom;
                    }

                }
            }
            $_SESSION['order']['cost'] = number_format($_SESSION['order']['sum'] - $_SESSION['order']['discount'] + $_SESSION['order']['delivery_cost'], 2, ".", ""); // Стоимость с учётом скидки
            $_SESSION['order']['currency'] = $CONFIG['shop_order_concurency'];
            $_SESSION['order']['total'] = $total;
        } else {
            unset ($_SESSION['order']);
        }
        return $elementsDisc;
    }

    function showPayTypes($block_name = 'block_pay_types', $json = false)
    {
        global $CONFIG, $tpl;
        $el = array();
        if (!$json) $tpl->newBlock('block_pay');
        switch ($_SESSION['order']['delivery_id']) {
//6 хрогопэй

            case "0":
                isset($_SESSION['siteuser']['id']) && $_SESSION['siteuser']['id'] == 2 ? $view = array(1, 2, 7) : $view = array(1, 2,7);
                break;

            case "3":
                isset($_SESSION['siteuser']['id']) && $_SESSION['siteuser']['id'] == 2 ? $view = array(1, 7) : $view = array(1,7);
                break;

            case "4":
                isset($_SESSION['siteuser']['id']) && $_SESSION['siteuser']['id'] == 2 ? $view = array(1, 7) : $view = array(1,7);

                break;

            case "5":
                isset($_SESSION['siteuser']['id']) && $_SESSION['siteuser']['id'] == 2 ? $view = array(2, 7) : $view = array(2,7);
                break;

            case "6":
                isset($_SESSION['siteuser']['id']) && $_SESSION['siteuser']['id'] == 2 ? $view = array(2, 7) : $view = array(2,7);
                break;

            case "7":
                isset($_SESSION['siteuser']['id']) && $_SESSION['siteuser']['id'] == 2 ? $view = array(1, 2, 7) : $view = array(1, 2,7);
                break;

            case "10":
                isset($_SESSION['siteuser']['id']) && $_SESSION['siteuser']['id'] == 2 ? $view = array(2, 7) : $view = array(2,7);
                break;
        }
        foreach ($CONFIG['order_pay_types'] as $key => $val) {

            if (in_array($key, $view))
                if ($key != 4 || (4 == $key && $CONFIG['shop_WM_activ'])) {
                    if ($json) {
                        $el[] = array('title' => $val, 'value' => $key);
                    } else {
                        $tpl->newBlock('block_pay_types');
                        $tpl->assign(array('title' => $val, 'value' => $key));
                    }

                }
        }
        return $el;
    }

    function getCompanyInfo()
    {
        global $CONFIG;

        return array(
            'concurency_id' => $CONFIG['order_concurency'],
            'concurency' => $CONFIG['catalog_currencies'][$CONFIG['order_concurency']],
            'company' => $CONFIG['shop_company'],
            'inn' => $CONFIG['shop_inn'],
            'kpp' => $CONFIG['shop_kpp'],
            'bank' => $CONFIG['shop_bank'],
            'sett_account' => $CONFIG['shop_sett_account'],
            'corr_account' => $CONFIG['shop_corr_account'],
            'bik' => $CONFIG['shop_bik'],
            'order_recipient' => $CONFIG['shop_order_recipient']
        );
    }

    /**
     * Изменить состояние заказа.
     *
     * Изменение сохраняется в "истории изменений соссояний заказа"
     *
     * @param int $order_id
     * @param int $status
     * @param string $info
     * @return unknown
     */
    function setOrderStatus($order_id, $status, $robo = false)
    {
        global $CONFIG, $db, $main;

        $order_id = (int)$order_id;
        if (!$order = $this->getOrder($order_id)) {
            return FALSE;
        }
        if (!$robo)
            $manager_id = $_SESSION['siteuser']['id'];
        else
            $manager_id = 47;
        $status = (int)$status;
        $status_list = $this->getOrdStatusList();
        if ($order['status'] == $status || !array_key_exists($status, $status_list)) {
            return FALSE;
        }

        $db->query("SELECT * FROM " . $this->table_prefix . "_shop_yandex_orders WHERE order_id=" . $order_id);
        if ($db->NF() > 0) {
            $db->next_record();

            $campaign = 21117492; // Идентификатор кампании (указывается без префикса "№11-")
            $yandex_order_id = $db->f('yandex_order_id'); // Номер заказа в Яндексе
            $token = '327a06cd46564b1a927916c778ce117a'; // Авторизационный токен (OAuth-токен)
            $app_id = '1d8ef04f0aa74bf69adcd9bfc6633c01'; // Идентификатор приложения OAuth
            $login = 'suprael2013'; // Логин пользователя, для которого был получен OAuth-токен (указывается без "@yandex.ru")

            // URL запроса
            $url = "https://api.partner.market.yandex.ru/v2/campaigns/$campaign/orders/$yandex_order_id/status?oauth_token=$token&oauth_client_id=$app_id&oauth_login=$login";

            // Содержимое запроса


            /*PROCESSING — заказ находится в обработке;
			DELIVERY — заказ передан в доставку;
			PICKUP — заказ доставлен в пункт самовывоза;
			DELIVERED — заказ получен покупателем;
			CANCELLED — заказ отменен.

			<option value="0">Неподтвержденный заказ</option>
			<option value="4">Согласован</option>
			<option value="5">Оплачен</option>
			<option value="6">Отгружен</option>
			<option value="7" selected="">Отменен</option>
			<option value="8">Выполнен</option>
			<option value="9">Выставлен счет на оплату</option>
			<option value="10">Согласование регионы</option>*/

            switch ($status) {
                case 0:
                    $order_status = 'PROCESSING';
                    break;

                case 4:
                    $order_status = 'PROCESSING';
                    break;

                case 5:
                    $order_status = 'PROCESSING';
                    break;

                case 6:
                    $order_status = 'DELIVERY';
                    break;

                case 7:
                    $order_status = 'CANCELLED';
                    break;

                case 8:
                    $order_status = 'DELIVERED';
                    break;

                case 9:
                    $order_status = 'PROCESSING';
                    break;

                case 10:
                    $order_status = 'PROCESSING';
                    break;
                case 12:
                    $order_status = 'PROCESSING';
                    break;
            }
            if ($order_status == 'CANCELLED')
                $body = json_encode(array("order" => array("status" => $order_status, "substatus" => "SHOP_FAILED")));
            else
                $body = json_encode(array("order" => array("status" => $order_status)));

            // Создание временного файла, содержимое которого будет передано методом PUT
            $fp = fopen('php://temp/maxmemory:256000', 'w');
            //if (!$fp) { die('could not open temp memory data'); }
            fwrite($fp, $body);
            fseek($fp, 0);

            // Выполнение PUT-запроса и вывод результата
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json', 'Accept: application/json', 'Expect:'));
            curl_setopt($ch, CURLOPT_PUT, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_INFILE, $fp);
            curl_setopt($ch, CURLOPT_INFILESIZE, strlen($body));

            $output = curl_exec($ch);

            /*if(!$output) { echo ("Error: ".curl_error($ch).'('.curl_errno($ch).')'); }
			else { echo $output; }*/
            curl_close($ch);

            /*$token = "327a06cd46564b1a927916c778ce117a";
			$client_id = '1d8ef04f0aa74bf69adcd9bfc6633c01';
			$login = "suprael2013";
			$yandex_id = $db->f('yandex-order_id');
			$headers = array(
			    'Content-Type: application/json',
			    'Accept: application/json', 
			    'Expect:'
			);

			$data = json_encode(array('order' => array('status' => 'DELIVERY')));
			$url = 'https://api.partner.market.yandex.ru/v2/campaigns/21117492/orders/'.$yandex_id.'/status?oauth_token='.$token.'&oauth_client_id='.$client_id.'&oauth_login='.$login;
			

			$fp = fopen('php://temp/maxmemory:256000', 'w');
			if (!$fp) { die('could not open temp memory data'); }
			fwrite($fp, $data);
			fseek($fp, 0);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_PUT, true);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
			curl_setopt($ch, CURLOPT_INFILE, $fp);
			curl_setopt($ch, CURLOPT_INFILESIZE, strlen($data));   

			$output = curl_exec($ch);
			echo "|".$output."|";*/
            //fclose($fp);
            //curl_close($ch);
        }

        $query = "UPDATE {$this->table_prefix}_shop_orders SET status={$status}, "
            . "manager_id={$manager_id} WHERE id={$order_id}";
        $db->query($query);
        if (!$db->affected_rows()) {
            return FALSE;
        }
        $info = My_Sql::escape($status_list[$order['status']]['title'] . " -> " . $status_list[$status]['title']);
        $query = "INSERT INTO " . $this->table_prefix . "_shop_order_history";
        $query .= " (order_id, manager_id, old_status, new_status, `date`, info) VALUES";
        $query .= " ({$order['order_id']}, {$manager_id}, '{$order['status']}', {$status}, NOW(), '{$info}')";
        $db->query($query);
        $order = $this->getOrder($order_id);
        if (in_array($status, array(8, 12))) {
            $db->set("core_users_bonus_data", array("status" => 1), array("order_id" => $order_id, "status" => 0));
            $db->set("core_users_bonus_data", array("status" => 2), array("order_id" => $order_id, "status" => 3));
            $db->set("core_users_bonus_data", array("status" => 1), array("order_id" => $order_id, "status" => 4));
            $db->set("core_users_bonus_data", array("status" => 2), array("order_id" => $order_id, "status" => 5));
        } elseif (in_array($status, array(7))) {
            $db->set("core_users_bonus_data", array("status" => 4), array("order_id" => $order_id, "status" => 0));
            $db->set("core_users_bonus_data", array("status" => 5), array("order_id" => $order_id, "status" => 3));
            $db->set("core_users_bonus_data", array("status" => 4), array("order_id" => $order_id, "status" => 1));
            $db->set("core_users_bonus_data", array("status" => 5), array("order_id" => $order_id, "status" => 2));
        }
        if ($order['email']) {
            if ($status == '10') {
                if ((strripos($order['address'], "Москва") !== false) || (strripos($order['address'], "москва") !== false) || (strripos($order['address'], "МОСКВА") !== false)) {

                    //$mail->AddEmbeddedImage(RP.'i/', 'cirlogo.jpg', 'cirlogo', 'base64', 'image/jpeg');
                    $tpl1 = new TemplatePower(RP . '_shop_waybill.html', T_BYFILE);
                    $items = $this->getOrderItems($order_id);
                    $orederInfo = $this->getOrder($order_id);
                    $i = 1;
                    $tableData = "";
                    foreach ($items as $item) {
                        $tableData = $tableData . "<tr><td>$i</td><td>" . $item['item_name'] . "</td><td>" . $item['item_price'] . "</td><td>" . $item['item_qty'] . "</td><td>шт.</td><td>" . $item['item_price'] . "</td></tr>";
                        $i++;
                    }
                    $tableData = $tableData . "<tr><td>" . $i . "</td><td>Доставка</td><td>" . $orederInfo['delivery_cost'] . "</td><td>1</td><td>шт.</td><td>" . $orederInfo['delivery_cost'] . "</td></tr>";
                    $i++;
                    $tableData = $tableData . "<tr><td>" . $i . "</td><td>Транспортные услуги</td><td>" . $orederInfo['transp'] . "</td><td>1</td><td>шт.</td><td>" . $orederInfo['transp'] . "</td></tr>";
                    $i++;
                    $tableData = $tableData . "<tr><td>" . $i . "</td><td>Скидка</td><td>" . $orederInfo['discount'] . "</td><td>1</td><td>шт.</td><td>" . $orederInfo['discount'] . "</td></tr>";
                    $count = $i;
                    $i++;
                    $k = $i + 3;
                    for ($i; $i < $k; $i++) {
                        $tableData = $tableData . "<tr><td>$i</td><td></td><td></td><td></td><td></td><td></td></tr>";
                    }
                    $date = $this->rusdate(time());
                    $tpl1->prepare();
                    $tpl1->assign(array(
                        "order_id" => $order_id,
                        "date" => iconv('utf-8', 'windows-1251', $date),
                        "table_data" => iconv('utf-8', 'windows-1251', $tableData),
                        "item_count" => $count,
                        "summ" => iconv('utf-8', 'windows-1251', $order['total_sum']),
                        "summ_p" => iconv('utf-8', 'windows-1251', $orederInfo['cost_rur_in_words'])

                    ));

                } else {
                    $tpl1 = new TemplatePower(RP . 'tpl/sup_rus/_kvit_template.html', T_BYFILE);//'tpl/sup_ было
                    $tpl1->prepare();
                    $tpl1->assign(array(
                        'name' => iconv('utf-8', 'windows-1251', $order['customer']),
                        'num' => $order_id,
                        'company' => iconv('utf-8', 'windows-1251', $CONFIG['shop_company']),
                        'inn' => iconv('utf-8', 'windows-1251', $CONFIG['shop_inn']),
                        'rs' => iconv('utf-8', 'windows-1251', $CONFIG['shop_sett_account']),
                        'bank' => iconv('utf-8', 'windows-1251', $CONFIG['shop_bank']),
                        'bik' => iconv('utf-8', 'windows-1251', $CONFIG['shop_bik']),
                        'corr' => iconv('utf-8', 'windows-1251', $CONFIG['shop_corr_account']),

                        'address' => iconv('utf-8', 'windows-1251', $order['address']),
                        'day' => date('d'),
                        'month' => iconv('utf-8', 'windows-1251', $this->rusdate(time(), '%MONTH%')),
                        'year' => date('Y'),
                        'total' => $order['total_sum'],

                    ));
                }
                $f = fopen(RP . "kvitancia.htm", "w+");
                fwrite($f, $tpl1->getOutputContent());
                fclose($f);
                $tpl = new AboTemplate(RP . 'tpl/' . SITE_PREFIX . '/' . $this->module_name . '_mail_notify.html');
                $tpl->prepare();
                $tpl->assign(array(
                    'order_id' => $order['order_id'],
                    'site_name' => strpos($CONFIG['sitename_rus'], '.ru') ? $CONFIG['sitename_rus'] : $CONFIG['sitename_rus'] . ".ru",
                    'status_name' => $order['order_status'],
                    'host' => 'http://suprashop.ru',
                ));

                $subject = "Изменение статуса заказа №{$order_id} на сайте {$CONFIG['sitename_rus']}";
                require_once(RP . '/inc/class.Mailer.php');
                $mail = new Mailer();
                //echo $CONFIG['email_charset'];
                $mail->CharSet = $CONFIG['email_charset'];
                $mail->Mailer = 'mail';
                $mail->ContentType = "text/html"; //$CONFIG['email_type'];
                $mail->From = $CONFIG['shop_order_recipient'];
                $mail->FromName = $CONFIG['sitename_rus'];
                $mail->AddAddress($order['email']);
                $mail->Subject = $subject;
                $mail->Body = $tpl->getOutputContent();
                $mail->AddAttachment(RP . 'kvitancia.htm');
                $mail->Send();
            } else {

                if ($status == '12')//Выполнен + отзыв
                {

                    /* include(RP.'/mod/configuration/lib/class.ConfigurationPrototype.php');


                     $a=new ConfigurationPrototype();
                     $a->ADM_cron();
                     exit;*/
                    $tpl = new AboTemplate(RP . 'tpl/' . SITE_PREFIX . '/' . $this->module_name . '_mail_notify.html');
                    $tpl->prepare();
                    $tpl->assign(array(
                        'order_id' => $order['order_id'],
                        'site_name' => strpos($CONFIG['sitename_rus'], '.ru') ? $CONFIG['sitename_rus'] : $CONFIG['sitename_rus'] . ".ru",
                        'status_name' => "Выполнен",
                        'host' => 'http://suprashop.ru',
                    ));

                    $subject = "Изменение статуса заказа №{$order_id} на сайте {$CONFIG['sitename_rus']}";
                    require_once(RP . '/inc/class.Mailer.php');
                    $mail = new Mailer();
                    //echo $CONFIG['email_charset'];
                    $mail->CharSet = $CONFIG['email_charset'];
                    $mail->Mailer = 'mail';
                    $mail->ContentType = "text/html"; //$CONFIG['email_type'];
                    $mail->From = $CONFIG['shop_order_recipient'];
                    $mail->FromName = $CONFIG['sitename_rus'];
                    $mail->AddAddress($order['email']);
                    $mail->Subject = $subject;
                    $mail->Body = $tpl->getOutputContent();
                    $mail->Send();

                    $tpl1 = new AboTemplate(RP . 'tpl/' . SITE_PREFIX . '/' . $this->module_name . '_mail_review.html');
                    $tpl1->prepare();
                    $tpl1->assign(array(
                        'order_id' => $order['order_id'],
                        'site_name' => strpos($CONFIG['sitename_rus'], '.ru') ? $CONFIG['sitename_rus'] : $CONFIG['sitename_rus'] . ".ru",
                        'status_name' => $order['order_status'],
                        'host' => 'http://suprashop.ru',
                    ));

                    $subject = "Спасибо за то, что выбрали наш Интернет-магазин {$CONFIG['sitename_rus']}";
                    require_once(RP . '/inc/class.Mailer.php');
                    $mail = new Mailer();
                    //echo $CONFIG['email_charset'];
                    $mail->CharSet = $CONFIG['email_charset'];
                    $mail->Mailer = 'mail';
                    $mail->ContentType = "text/html"; //$CONFIG['email_type'];
                    $mail->From = $CONFIG['shop_order_recipient'];
                    $mail->FromName = $CONFIG['sitename_rus'];
                    $mail->AddAddress($order['email']);
                    $mail->Subject = $subject;
                    $mail->Body = $tpl1->getOutputContent();
                    $mail->Send();
                } else {
                    $tpl = new AboTemplate(RP . 'tpl/' . SITE_PREFIX . '/' . $this->module_name . '_mail_notify.html');
                    $tpl->prepare();
                    $tpl->assign(array(
                        'order_id' => $order['order_id'],
                        'site_name' => strpos($CONFIG['sitename_rus'], '.ru') ? $CONFIG['sitename_rus'] : $CONFIG['sitename_rus'] . ".ru",
                        'status_name' => $order['order_status'],
                        'host' => 'http://suprashop.ru',
                    ));

                    $subject = "Изменение статуса заказа №{$order_id} на сайте {$CONFIG['sitename_rus']}";
                    require_once(RP . '/inc/class.Mailer.php');
                    $mail = new Mailer();
                    //echo $CONFIG['email_charset'];
                    $mail->CharSet = $CONFIG['email_charset'];
                    $mail->Mailer = 'mail';
                    $mail->ContentType = "text/html"; //$CONFIG['email_type'];
                    $mail->From = $CONFIG['shop_order_recipient'];
                    $mail->FromName = $CONFIG['sitename_rus'];
                    $mail->AddAddress($order['email']);
                    $mail->Subject = $subject;
                    $mail->Body = $tpl->getOutputContent();
                    $mail->Send();
                }

            }
            $this->sendSms($order['phone'], $order_id, $order['order_status']);


        }
        return TRUE;
    }

    function setOrderNotes($order_id, $notes)
    {
        global $CONFIG, $db;

        $db->query("UPDATE {$this->table_prefix}_shop_orders SET notes='"
            . My_Sql::escape(htmlspecialchars($notes))
            . "' WHERE id=" . intval($order_id));


        return $db->affected_rows() ? TRUE : FALSE;
    }


    function exchange($sum, $from_cur, $to_cur)
    {
        global $CONFIG;

        if (!array_key_exists($from_cur, $CONFIG['catalog_currencies'])
            || !array_key_exists($to_cur, $CONFIG['catalog_currencies'])
            //|| !is_numeric($sum)
        ) {
            return FALSE;
        }

        return $to_cur != $from_cur ? $sum * $_SESSION['catalog_rates'][$from_cur . '/' . $to_cur] : $sum;
    }

    function getFilterCondition()
    {
        if (!isset($_REQUEST['filter']) && !isset($_SESSION['shop_fltr'])) {
            return "";
        }

        $f = $_REQUEST['filter'] ? $_REQUEST['filter'] : $_SESSION['shop_fltr'];
        $_SESSION['shop_fltr'] = $f;
        $op = "AND";
        $condtn = array();

        if (isset($f['status']) && $f['status'] >= 0) {
            $condtn[] = "status = '" . intval($f['status']) . "'";
        }

        $date_from = @$f['date_from'] ? Main::transform_date(My_Sql::escape($f['date_from'])) : "";
        $date_to = @$f['date_to'] ? Main::transform_date(My_Sql::escape($f['date_to'])) : "";
        if ($date_from && !$date_to) {
            $condtn[] = "date >= '" . $date_from . "'";
        } elseif ($date_to && !$date_from) {
            $condtn[] = "date <= '" . $date_to . "'";
        } elseif ($date_from && $date_to) {
            $condtn[] = "date BETWEEN '" . $date_from . "' AND '" . $date_to . "'";
        }

        $sum_from = @$f['sum_from'] ? My_Sql::escape($f['sum_from']) : "";
        $sum_to = @$f['sum_to'] ? My_Sql::escape($f['sum_to']) : "";
        if ($sum_from && !$sum_to) {
            $condtn[] = "total_sum >= '" . $sum_from . "'";
        } elseif ($sum_to && !$sum_from) {
            $condtn[] = "total_sum <= '" . $sum_to . "'";
        } elseif ($sum_from && $sum_to) {
            $condtn[] = "total_sum BETWEEN '" . $sum_from . "' AND '" . $sum_to . "'";
        }
        $customer = @$f['customer'] ? My_Sql::escape($f['customer']) : "";
        if ($customer) {
            $condtn[] = "(customer LIKE '%" . $customer . "%' OR O.email LIKE '%" . $customer . "%')";
        }

        if (@$f['reg_user']) {
            $condtn[] = "siteuser_id";
        }
        if ($f['custm_type'] != '' && in_array($f['custm_type'], $this->client_types)) {
            $condtn[] = "type='" . My_Sql::escape($f['custm_type']) . "'";
        }
        if (isset($f['manager']) && $f['manager'] > 0) {
            $condtn[] = "manager_id='" . intval($f['manager']) . "'";
        }

        return implode(" " . $op . " ", $condtn);
    }

    function showFilter()
    {
        global $tpl, $baseurl, $lang;

        $f = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : $_SESSION['shop_fltr'];
        $start = isset($_REQUEST['start']) && $_REQUEST['start'] > 1 ? (int)$_REQUEST['start'] : 1;
        $tpl->newBlock("block_filter");
        $tpl->assign(Array(
            "MSG_Filter" => $this->_msg["Filter"],
            "MSG_Date_from" => $this->_msg["Date_from"],
            "MSG_Select" => $this->_msg["Select"],
            "MSG_date_to" => $this->_msg["date_to"],
            "MSG_sum_to" => $this->_msg["sum_to"],
            "MSG_Status" => $this->_msg["Status"],
            "MSG_All" => $this->_msg["All"],
            "MSG_Customer_FIO_Email" => $this->_msg["Customer_FIO_Email"],
            "MSG_Registered" => $this->_msg["Registered"],
            "MSG_Customer_type" => $this->_msg["Customer_type"],
            "MSG_Select" => $this->_msg["Select"],
            "MSG_Reset" => $this->_msg["Reset"],
            "MSG_Responsible" => $this->_msg["Responsible"],
        ));

        $date_from = isset($f['date_from']) ? htmlspecialchars($f['date_from']) : "";
        $sum_from = isset($f['sum_from']) ? htmlspecialchars($f['sum_from']) : "";
        $date_to = isset($f['date_to']) ? htmlspecialchars($f['date_to']) : "";
        $sum_to = isset($f['sum_to']) ? htmlspecialchars($f['sum_to']) : "";
        $customer = isset($f['customer']) ? htmlspecialchars($f['customer']) : "";
        $reg_user = isset($f['reg_user']) ? "checked" : "";

        $tpl->assign(
            array(
                "baseurl" => $baseurl,
                "lang" => $lang,
                "start" => $start,
                "filter_checked" => !empty($f) ? "checked" : "",
                "filter_display" => !empty($f) ? "block" : "none",
                "date_from_val" => $date_from,
                "sum_from_val" => $sum_from,
                "date_to_val" => $date_to,
                "sum_to_val" => $sum_to,
                "customer_val" => $customer,
                "reg_user_val" => $reg_user,
            )
        );

        $selct = isset($f['status']) && $f['status'] >= 0 ? $f['status'] : -1;
        $list = $this->getOrdStatusList();
        foreach ($list as $val) {
            $tpl->newBlock('block_order_status');
            $val['selected'] = $val['id'] == $selct ? 'selected' : '';
            $tpl->assign($val);
        }

        $tpl->newBlock('block_custm_types');
        $tpl->assign(
            array(
                "elm_idx" => 0,
                "elm_val" => $this->_msg["All"],
                "checked" => !$f['custm_type'] ? "checked" : ""

            )
        );

        if (is_array($this->client_types)) {
            foreach ($this->client_types as $key => $val) {
                $tpl->newBlock('block_custm_types');
                $tpl->assign(
                    array(
                        "elm_idx" => $key,
                        "elm_val" => $val,
                        #"checked" => (strcmp($val,$f['custm_type'])==0) ? "checked" : ""
                        "checked" => (strcmp($val, $f['custm_type']) == 0) ? "selected" : ""
                    )
                );
            }
        }

        $list = $this->getManagersList();
        $selct = isset($f['manager']) && $f['manager'] >= 0 ? $f['manager'] : -1;
        if ($list) {
            foreach ($list as $val) {
                $tpl->newBlock('block_manager');
                $val['selected'] = $val['id'] == $selct ? 'selected' : '';
                $tpl->assign($val);
            }
        }

    }

    function getFilterHref()
    {
        $f = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : array();

        $href = @$_REQUEST['filter_check'] ? "&filter_check=" . $_REQUEST['filter_check'] : "";

        $href .= isset($f['status']) && $f['status'] >= 0 ? "&filter[status]=" . (int)$f['status'] : "";

        $href .= @$f['date_from'] ? "&filter[date_from]=" . @$f['date_from'] : "";

        $href .= @$f['sum_from'] ? "&filter[sum_from]=" . @$f['sum_from'] : "";

        $href .= @$f['date_to'] ? "&filter[date_to]=" . $f['date_to'] : "";

        $href .= @$f['sum_to'] ? "&filter[sum_to]=" . $f['sum_to'] : "";

        $href .= @$f['customer'] ? "&filter[customer]=" . $f['customer'] : "";

        $href .= @$f['manager'] ? "&filter[manager]=" . $f['manager'] : "";

        $href .= @$f['reg_user'] ? "&filter[reg_user]=1" : "";

        $href .= @$f['custm_type'] > 0 && array_key_exists($f['custm_type'], $this->client_types)
            ? "&filter[custm_type]=" . (int)$f['custm_type'] : "";

        return $href;
    }

    function getManagersList()
    {
        global $db;
        $ret = false;
        $db->query("SELECT DISTINCT manager_id, username FROM {$this->table_prefix}_shop_orders O LEFT JOIN core_users U ON U.id=manager_id WHERE manager_id>0 ORDER BY username");
        while ($db->next_record()) {
            $ret[$db->Record['manager_id']] = array(
                'id' => $db->Record['manager_id'],
                'username' => $db->Record['username'] ? $db->Record['username'] : "user{$db->Record['id']}",
            );
        }
        return $ret;
    }

    function do_export_XML()
    {
        global $CONFIG, $lang;
        $doc = domxml_new_doc("1.0");
        $root = $doc->add_root("orders");
        $root->set_attribute("date", gmdate('d.m.Y H:i:s') . date("O"));
        $root->set_attribute("lang", $lang);
        if ($this->get_orders_XML($doc, $root)) {
            $out = $doc->dump_mem(TRUE, 'UTF-8');

            $file = $CONFIG['shop_export_path'] . 'orders_' . gmdate('Ymd_His') . substr(date("O"), 0, 3) . '.xml';
            $handle = fopen(RP . $file, 'w');
            fwrite($handle, $out);
            fclose($handle);
            return $file;
        } else {
            return false;
        }
    }

    function get_orders_XML(&$doc, &$parent)
    {
        global $db, $CONFIG;

        $query = "SELECT o.id, o.siteuser_id, o.delivery_id, o.manager_id, o.delivery_cost, "
            . "o.customer, o.type, o.address, o.phone, o.email, o.organization, o.jpinfo, "
            . "DATE_FORMAT(o.date,'%Y.%m.%d %H:%i:%s') AS date, o.sum, o.discount, o.total_sum, "
            . "o.currency, o.addinfo, o.status, o.pay_type, o.session, o.notes,"
            . "oi.item_id, oi.item_name, oi.item_code, oi.item_price, item_currency, item_rate, "
            . "oi.item_qty, item_cost, oi.info AS item_info "
            . "FROM `" . $this->table_prefix . "_shop_orders` o "
            . "LEFT JOIN `" . $this->table_prefix . "_shop_order_items` oi ON o.id = oi.order_id "
            . " ORDER BY o.id,oi.item_rate";
        $db->query($query);
        $cur_order_id = 0;
        if ($db->nf()) {
            while ($db->next_record()) {
                $order_id = $db->f('id');
                if ($cur_order_id != $order_id) {
                    $orders[] = $order_id;
                    $cur_order_id = $order_id;
                    $order = $doc->create_element("order");
                    $order->set_attribute("id", $order_id);
                    $order->set_attribute("user_id", $db->f('siteuser_id'));
                    $order->set_attribute("manager_id", $db->f('manager_id'));
                    $order = $parent->append_child($order);
                    $order->new_child("customer", $this->conv_str($db->f('customer')));
                    $node = $order->new_child("delivery", floatval($db->f('delivery_cost')));
                    $node->set_attribute("id", intval($db->f('delivery_id')));
                    $order->new_child("type", $this->conv_str($db->f('type')));
                    $order->new_child("address", $this->conv_str($db->f('address')));
                    $order->new_child("phone", $this->conv_str($db->f('phone')));
                    $order->new_child("email", $this->conv_str($db->f('email')));
                    $order->new_child("organization", $this->conv_str($db->f('organization')));
                    $order->new_child("jpinfo", $this->conv_str($db->f('jpinfo')));
                    $order->new_child("date", $this->conv_str($db->f('date')));
                    $order->new_child("sum", floatval($db->f('sum')));
                    $order->new_child("discount", floatval($db->f('discount')));
                    $order->new_child("total", floatval($db->f('total_sum')));
                    $order->new_child("currency", $this->conv_str($db->f('currency')));
                    $order->new_child("addinfo", $this->conv_str($db->f('addinfo')));
                    $order->new_child("status", intval($db->f('status')));
                    $payment = $order->new_child("payment", $this->conv_str($CONFIG['order_pay_types'][intval($db->f('pay_type'))]));
                    $payment->set_attribute("type", intval($db->f('pay_type')));
                    $order->new_child("session", $this->conv_str($db->f('session')));
                    $order->new_child("notes", $this->conv_str($db->f('notes')));
                    $items = $doc->create_element("items");
                    $items = $order->append_child($items);
                }
                if ($db->f('item_id') > 0) {
                    $product = $doc->create_element("product");
                    $product->set_attribute("id", intval($db->f('item_id')));
                    $product->set_attribute("quantity", intval($db->f('item_qty')));
                    $product = $items->append_child($product);
                    $product->new_child("code", $this->conv_str($db->f('item_code')));
                    $product->new_child("name", $this->conv_str($db->f('item_name')));
                    $product->new_child("price", floatval($db->f('item_price')));
                    $product->new_child("currency", $this->conv_str($db->f('item_currency')));
                    $product->new_child("rate", floatval($db->f('item_rate')));
                    $product->new_child("cost", floatval($db->f('item_cost')));
                    $product->new_child("info", $this->conv_str($db->f('item_info')));
                }
            }
            return true;
        } else {
            return false;
        }
    }

    function getPropertyFields()
    {
        GLOBAL $request_sub_action;
        if ('' != $request_sub_action) {

            $tpl = $this->getTmpProperties();

            switch ($request_sub_action) {
                default:
                    $tpl->newBlock('block_properties_none');
                    $tpl->assign(Array(
                        "MSG_No" => $this->_msg["No"],
                    ));
                    $result = $this->getOutputContent($tpl->getOutputContent());
                    break;
            }
            return $result;
        }
        return FALSE;
    }

    function getEditLink($sub_action = NULL, $block_id = NULL)
    {
        GLOBAL $request_name, $db, $lang, $server;
        $block_id = (int)$block_id;
        /*
		if ($sub_action && $block_id) {
			$db->query('SELECT	property1,
								property2,
								property3,
								property4,
								property5
							FROM '.$server.$lang.'_pages_blocks
							WHERE id = '.$block_id);
			if ($db->nf() > 0) {
				$db->next_record();
				$arr[]	= $db->f('property1');
				$arr[]	= $db->f('property2');
				$arr[]	= $db->f('property3');
				$arr[]	= $db->f('property4');
				$arr[]	= $db->f('property5');

				switch ($sub_action) {
					case 'showbanner':
						$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=showbanners&category=';
						$link['material_id']	= $arr[0];
						break;
				}
				return $link;
			}

			switch ($sub_action) {
				case 'showbanner':
					$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=showbanners&category=';
					$link['material_id']	= '';
					break;
			}
			return $link;
		}
*/
        return FALSE;
    }

    function getDiscountsAdm($url, $start)
    {
        global $CONFIG, $db, $server, $lang;
        $start = (int)$start;
        if (!$start) {
            $start = 1;
        }
        $db->query('SELECT *, DATE_FORMAT(from_date, "' . $CONFIG['calendar_date'] . '") AS from_date, DATE_FORMAT(to_date, "'
            . $CONFIG['calendar_date'] . '") AS to_date FROM ' . $server . $lang . '_shop_discounts LIMIT ' . (($start - 1) * $CONFIG['shop_max_rows'])
            . ', ' . $CONFIG['shop_max_rows']);
        if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]['id'] = $db->f('id');
                $arr[$i]['name'] = htmlspecialchars($db->f('name'));
                $arr[$i]['value'] = $db->f('value');
                $arr[$i]['min'] = $db->f('min');
                $arr[$i]['max'] = $db->f('max');
                $arr[$i]['from_date'] = $db->f('from_date');
                $arr[$i]['to_date'] = $db->f('to_date');
                $arr[$i]['is_percent'] = $db->f('is_percent') ? '%' : 'абс.';
                $arr[$i]['active'] = $db->f('active');
                $arr[$i]['img_stat'] = $db->f('active') ? 'ico_swof.gif' : 'ico_swon.gif';
                $arr[$i]['act_stat'] = $db->f('active') ? $this->_msg["Suspend"] : $this->_msg["Activate"];
                $arr[$i]['active_id'] = $db->f('active') ? $url . '&action=dsnt_d&id=' . $db->f('id') . '&start=' . $start : $url . '&action=dsnt_a&id=' . $db->f('id') . '&start=' . $start;
                $arr[$i]['edit_id'] = $url . '&action=dsnt&id=' . $db->f('id') . '&step=3&start=' . $start;
                $arr[$i]['del_id'] = $url . '&action=dsnt_del&id=' . $db->f('id') . '&start=' . $start;
            }
            return $arr;
        }
        return FALSE;
    }

    function getDiscount($id)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if ($id) {
            $db->query('SELECT *, DATE_FORMAT(from_date, "' . $CONFIG['calendar_date']
                . '") AS from_date, DATE_FORMAT(to_date, "' . $CONFIG['calendar_date']
                . '") AS to_date FROM ' . $server . $lang . '_shop_discounts WHERE id = ' . $id);
            if ($db->nf() > 0) {
                $db->next_record();
                $arr['id'] = $db->f('id');
                $arr['discount'] = htmlspecialchars($db->f('name'));
                $arr['min'] = (double)($db->f('min'));
                $arr['max'] = (double)($db->f('max'));
                $arr['value'] = (double)($db->f('value'));
                $arr['from_date'] = $db->f('from_date');
                $arr['to_date'] = $db->f('to_date');
                $arr['is_percent'] = $db->f('is_percent') ? 'checked' : '';
                return $arr;
            }
            return FALSE;
        }
    }

    function addDiscounts($discount, $min, $max, $value, $is_percent, $from_date, $to_date)
    {
        global $CONFIG, $db, $server, $lang;
        $discount = $db->escape($discount);
        $min = $min;
        $max = $max;
        $value = $value;
        if (isset($discount) && $discount != '' && ($min || $max || $value)) {
            if ($is_percent) {
                $is_percent = 1;
            } else {
                $is_percent = 0;
            }
            $query = 'INSERT INTO ' . $server . $lang . '_shop_discounts (name, value, min, max, from_date, to_date, is_percent) VALUES ("'
                . $discount . '", ' . $value . ', ' . $min . ', ' . $max . ', "' . $db->escape(Main::transform_date($from_date)) . '", "'
                . $db->escape(Main::transform_date($to_date)) . '", ' . $is_percent . ')';
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return $db->lid();
            }
        }
        return FALSE;
    }

    function updateDiscount($id, $discount, $min, $max, $value, $is_percent, $from_date, $to_date)
    {
        global $CONFIG, $db, $server, $lang;
        $dicount = $db->escape($discount);
        $id = (int)$id;
        $min = $min;
        $max = $max;
        $value = $value;
        if (isset($discount) && $discount != '' && ($min || $max || $value)) {
            if ($is_percent) {
                $is_percent = 1;
            } else {
                $is_percent = 0;
            }
            $query = 'UPDATE ' . $server . $lang . '_shop_discounts
        				SET name = "' . $discount . '",
        					min = ' . $min . ',
        					max = ' . $max . ',
        					from_date = "' . $db->escape(Main::transform_date($from_date)) . '",
        					to_date = "' . $db->escape(Main::transform_date($to_date)) . '",
        					value = ' . $value . ',
        					is_percent = ' . $is_percent . '
        				WHERE id = ' . $id;
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return $db->lid();
            }
        }
        return FALSE;
    }

    function delDiscount($id)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if ($id) {
            $query = 'DELETE FROM ' . $server . $lang . '_shop_discounts WHERE id = ' . $id;
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return $db->lid();
            }
        }
        return FALSE;
    }

    function setDiscountActive($id)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if ($id) {
            $query = 'UPDATE ' . $server . $lang . '_shop_discounts SET active = 1 WHERE id = ' . $id;
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return TRUE;
            }
        }
        return FALSE;
    }

    function setDiscountUnActive($id)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if ($id) {
            $query = 'UPDATE ' . $server . $lang . '_shop_discounts SET active = 0 WHERE id = ' . $id;
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Получение налогов
     */
    function getTaxesAdm($url, $start)
    {
        global $CONFIG, $db, $server, $lang;
        $start = (int)$start;
        if (!$start) {
            $start = 1;
        }
        $query = 'SELECT *
        			FROM ' . $server . $lang . '_shop_taxes
        			LIMIT ' . (($start - 1) * $CONFIG['shop_max_rows']) . ', ' . $CONFIG['shop_max_rows'];
        $db->query($query);
        if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]['id'] = $db->f('id');
                $arr[$i]['tax'] = htmlspecialchars($db->f('tax'));
                $arr[$i]['percent'] = $db->f('percent');
                $arr[$i]['rank'] = $db->f('rank');
                $arr[$i]['active'] = $db->f('active');
                $arr[$i]['img_stat'] = $db->f('active') ? 'ico_swof.gif' : 'ico_swon.gif';
                $arr[$i]['act_stat'] = $db->f('active') ? $this->_msg["Suspend"] : $this->_msg["Activate"];
                $arr[$i]['active_id'] = $db->f('active') ? $url . '&action=taxes_deac&id=' . $db->f('id') . '&start=' . $start : $url . '&action=taxes_act&id=' . $db->f('id') . '&start=' . $start;
                $arr[$i]['edit_id'] = $url . '&action=taxes&id=' . $db->f('id') . '&step=3&start=' . $start;
                $arr[$i]['del_id'] = $url . '&action=taxes_del&id=' . $db->f('id') . '&start=' . $start;
            }
            return $arr;
        }
        return FALSE;
    }

    /**
     * Получение налога
     */
    function getTax($id)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if ($id) {
            $query = 'SELECT *
        				FROM ' . $server . $lang . '_shop_taxes
        				WHERE id = ' . $id;
            $db->query($query);
            if ($db->nf() > 0) {
                $db->next_record();
                $arr['id'] = $db->f('id');
                $arr['tax'] = htmlspecialchars($db->f('tax'));
                $arr['percent'] = $db->f('percent');
                $arr['rank'] = $db->f('rank');
                return $arr;
            }
            return FALSE;
        }
    }

    /**
     * Добавление налога $tax,  с величиной $percent и ранком $rank в БД
     */
    function addTax($tax, $percent, $rank)
    {
        global $CONFIG, $db, $server, $lang;
        $tax = $db->escape($tax);
        $percent = (int)$percent;
        $rank = (int)$rank;
        if (isset($tax) && $tax != '' && $percent && $rank) {
            $query = 'INSERT INTO ' . $server . $lang . '_shop_taxes (tax, percent, rank)
        				VALUES ("' . $tax . '", ' . $percent . ', ' . $rank . ')';
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return $db->lid();
            }
        }
        return FALSE;
    }

    /**
     * Изменение налога $id в БД
     */
    function updateTax($id, $tax, $percent, $rank)
    {
        global $CONFIG, $db, $server, $lang;
        $tax = $db->escape($tax);
        $id = (int)$id;
        $percent = (int)$percent;
        $rank = (int)$rank;
        if (isset($tax) && $tax != '' && $percent && $rank) {
            $query = 'UPDATE ' . $server . $lang . '_shop_taxes
        				SET tax = "' . $tax . '",
        					percent = ' . $percent . ',
        					rank = ' . $rank . '
        				WHERE id = ' . $id;
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return $db->lid();
            }
        }
        return FALSE;
    }

    /**
     * Добавление налога $tax,  с величиной $percent и ранком $rank в БД
     */
    function delTax($id)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if ($id) {
            $query = 'DELETE FROM ' . $server . $lang . '_shop_taxes WHERE id = ' . $id;
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return $db->lid();
            }
        }
        return FALSE;
    }

    function setTaxActive($id)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if ($id) {
            $query = 'UPDATE ' . $server . $lang . '_shop_taxes SET active = 1 WHERE id = ' . $id;
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return TRUE;
            }
        }
        return FALSE;
    }

    function setTaxUnActive($id)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if ($id) {
            $query = 'UPDATE ' . $server . $lang . '_shop_taxes
        				SET active = 0 WHERE id = ' . $id;
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return TRUE;
            }
        }
        return FALSE;
    }


    /**
     * Получение страны
     */
    function getCity($id)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if (!$id) {
            return FALSE;
        }
        $query = 'SELECT * FROM ' . $server . $lang . '_shop_cities WHERE id = ' . $id;
        $db->query($query);
        if ($db->nf() > 0) {
            $db->next_record();
            $arr['id'] = $db->f('id');
            $arr['city'] = htmlspecialchars($db->f('city'));
            return $arr;
        }
        return FALSE;
    }

    /**
     * Получение массива городов
     */
    function getCities($group_id, $start, $url)
    {
        global $CONFIG, $db, $server, $lang;
        $start = (int)$start;
        $group_id = (int)$group_id;
        if (!$group_id) {
            return FALSE;
        }
        if (!$start) {
            $start = 1;
        }
        $query = 'SELECT * FROM ' . $server . $lang . '_shop_cities
                    WHERE group_id = ' . $group_id . '
                    ORDER BY city
                    LIMIT ' . (($start - 1) * $CONFIG['shop_max_rows']) . ', ' . $CONFIG['shop_max_rows'];
        $db->query($query);
        if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]['edit_id'] = $url . '&action=grp_cty_edit&city_id=' . $db->f('id') . '&cty_id=' . $group_id;
                $arr[$i]['del_id'] = $url . '&action=grp_cty_del&city_id=' . $db->f('id') . '&cty_id=' . $group_id;
                $arr[$i]['city'] = htmlspecialchars($db->f('city'));
            }
            return $arr;
        }
        return FALSE;
    }

    /**
     * Добавление страны $country в БД
     */
    function addCity($group_id, $city)
    {
        global $CONFIG, $db, $server, $lang;
        $city = $db->escape($city);
        if (isset($city) && $city != '') {
            $query = 'INSERT INTO ' . $server . $lang . '_shop_cities (group_id, city) VALUES (' . $group_id . ', "' . $city . '")';
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return $db->lid();
            }
        }
        return FALSE;
    }

    /**
     * Изменение страны $id в БД
     */
    function updateCity($id, $city)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        $city = $db->escape($city);
        if (isset($city) && $city != '' && isset($id)) {
            $query = 'UPDATE ' . $server . $lang . '_shop_cities SET city = "' . $city . '" WHERE id = ' . $id;
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return $db->lid();
            }
        }
        return FALSE;
    }

    /**
     * Удаление страны
     */
    function delCity($id)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if (!$id) {
            return FALSE;
        }
        $query = 'DELETE FROM ' . $server . $lang . '_shop_cities WHERE id = ' . $id;
        $db->query($query);
        if ($db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Получение массива стран
     */
    function getCountries($start, $url)
    {
        global $CONFIG, $db, $server, $lang;
        $start = (int)$start;
        if (!$start) {
            $start = 1;
        }
        $query = 'SELECT a.id AS id, a.country AS country, count(b.city) AS cities
        			FROM ' . $server . $lang . '_shop_countries AS a
        				LEFT OUTER JOIN ' . $server . $lang . '_shop_cities AS b ON a.id = b.group_id
                    GROUP BY a.id, a.country
                    ORDER BY a.country
        			LIMIT ' . (($start - 1) * $CONFIG['shop_max_rows']) . ', ' . $CONFIG['shop_max_rows'];
        $db->query($query);
        if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]['link_id'] = $url . '&action=grp_cty_list&cty_id=' . $db->f('id');
                $arr[$i]['edit_id'] = $url . '&action=cty_edit&cty_id=' . $db->f('id');
                $arr[$i]['del_id'] = $url . '&action=cty_del&cty_id=' . $db->f('id');
                $arr[$i]['country'] = htmlspecialchars($db->f('country'));
                $arr[$i]['cities'] = $db->f('cities');
            }
            return $arr;
        }
        return FALSE;
    }

    /**
     * Получение страны
     */
    function getCountry($id)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if (!$id) {
            return FALSE;
        }
        $query = 'SELECT * FROM ' . $server . $lang . '_shop_countries WHERE id = ' . $id;
        $db->query($query);
        if ($db->nf() > 0) {
            $db->next_record();
            $arr['id'] = $db->f('id');
            $arr['country'] = htmlspecialchars($db->f('country'));
            return $arr;
        }
        return FALSE;
    }

    /**
     * Добавление страны $country в БД
     */
    function addCountry($country)
    {
        global $CONFIG, $db, $server, $lang;
        $country = $db->escape($country);
        if (isset($country) && $country != '') {
            $query = 'INSERT INTO ' . $server . $lang . '_shop_countries (country) VALUES ("' . $country . '")';
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return $db->lid();
            }
        }
        return FALSE;
    }

    /**
     * Изменение страны $id в БД
     */
    function updateCountry($id, $country)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        $country = $db->escape($country);
        if (isset($country) && $country != '' && isset($id)) {
            $query = 'UPDATE ' . $server . $lang . '_shop_countries
        				SET country = "' . $country . '"
        				WHERE id = ' . $id;
            $db->query($query);
            if ($db->affected_rows() > 0) {
                return $db->lid();
            }
        }
        return FALSE;
    }

    /**
     * Удаление страны
     */
    function delCountry($id)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if (!$id) {
            return FALSE;
        }
        $query = 'DELETE FROM ' . $server . $lang . '_shop_countries WHERE id = ' . $id;
        $db->query($query);
        if ($db->affected_rows() > 0) {
            $query = 'DELETE FROM ' . $server . $lang . '_shop_cities WHERE group_id = ' . $id;
            $db->query($query);
            return TRUE;
        }
        return FALSE;
    }

    // функция для удаления заказа из базы данных
    function delete_order($id = NULL)
    {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if ($id) {
            $db->query('DELETE FROM ' . $server . $lang . '_shop_orders WHERE id = ' . $id);
            if ($db->affected_rows() > 0) {
                $db->query('DELETE FROM ' . $server . $lang . '_shop_order_items WHERE order_id = ' . $id);
            }
            return TRUE;
        }
        return FALSE;
    }

    function showWmConf()
    {
        global $CONFIG, $tpl, $baseurl;

        $tpl->newBlock('block_webmoney');
        $tpl->assign(Array(
            "MSG_Webmoney_params" => $this->_msg["Webmoney_params"],
            "MSG_Active" => $this->_msg["Active"],
            "MSG_Currency" => $this->_msg["Currency"],
            "MSG_Select" => $this->_msg["Select"],
            "MSG_Purse" => $this->_msg["Purse"],
            "MSG_Info_success_URL" => $this->_msg["Info_success_URL"],
            "MSG_Info_fail_URL" => $this->_msg["Info_fail_URL"],
            "MSG_Info_percent" => $this->_msg["Info_percent"],
        ));
        $tpl->assign(
            array(
                'WM_purse' => $CONFIG['shop_WM_purse'],
                'WM_secret_key' => $CONFIG['shop_WM_secret_key'],
                'WM_success_url' => $CONFIG['shop_WM_success_url'],
                'WM_fail_url' => $CONFIG['shop_WM_fail_url'],
                'WM_discount' => $CONFIG['shop_WM_discount'],
                'WM_checked' => $CONFIG['shop_WM_activ'] ? 'checked' : '',
                'WM_result_url' => "http://" . $_SERVER['HTTP_HOST'] . $CONFIG['shop_WM_result_url'],
            )
        );
        foreach ($CONFIG['catalog_currencies'] as $key => $val) {
            $tpl->newBlock('block_WM_currency');
            $tpl->assign(
                array(
                    'currency_id' => $key,
                    'currency_val' => $val,
                    'currency_selected' => $key == $CONFIG['shop_WM_currency'] ? 'selected' : ''
                )
            );
        }
    }

    function showAssistConf()
    {
        global $CONFIG, $tpl, $baseurl;

        $tpl->newBlock('block_assist');
        $tpl->assign(Array(
            "MSG_ASSIST_params" => $this->_msg["ASSIST_params"],
            "MSG_Active" => $this->_msg["Active"],
            "MSG_Currency" => $this->_msg["Currency"],
            "MSG_Select" => $this->_msg["Select"],
            "MSG_Shop_code" => $this->_msg["Purse"],
            "MSG_Login" => $this->_msg["Info_success_URL"],
            "MSG_Password" => $this->_msg["Info_fail_URL"],
            "MSG_Info_URLs" => $this->_msg["Info_percent"],
            "MSG_Info_success_URL" => $this->_msg["Info_success_URL"],
            "MSG_Info_fail_URL" => $this->_msg["Info_fail_URL"],
            "MSG_Info_percent" => $this->_msg["Info_percent"],
        ));
        $tpl->assign(
            array(
                'assist_idp' => $CONFIG['shop_assist_idp'],
                'assist_login' => $CONFIG['shop_assist_login'],
                'assist_password' => $CONFIG['shop_assist_password'],
                'assist_success_url' => $CONFIG['shop_assist_success_url'],
                'assist_fail_url' => $CONFIG['shop_assist_fail_url'],
                'assist_discount' => $CONFIG['shop_assist_discount'],
                'assist_checked' => $CONFIG['shop_assist_activ'] ? 'checked' : '',
                'assist_result_url' => "http://" . $_SERVER['HTTP_HOST'] . $CONFIG['shop_assist_result_url'],
            )
        );
        foreach ($CONFIG['catalog_currencies'] as $key => $val) {
            $tpl->newBlock('block_assist_currency');
            $tpl->assign(
                array(
                    'currency_id' => $key,
                    'currency_val' => $val,
                    'currency_selected' => $key == $CONFIG['shop_assist_currency'] ? 'selected' : ''
                )
            );
        }
    }

    function saveWmConf()
    {
        $conf_str = "<?php\n\$NEW = array(\n/* ABOCMS:START */\n";
        $conf_str .= "'shop_WM_currency' => '" . $_POST['WM_currency'] . "',\n'shop_WM_purse' => '" . $_POST['WM_purse'] . "',\n";
        $conf_str .= "'shop_WM_secret_key' => '" . $_POST['WM_secret_key'] . "',\n'shop_WM_success_url' => '" . $_POST['WM_success_url'] . "',\n";
        $conf_str .= "'shop_WM_fail_url' => '" . $_POST['WM_fail_url'] . "',\n'shop_WM_discount' => '" . $_POST['WM_discount'] . "'\n";
        $conf_str .= "/* ABOCMS:END */\n);\n?>";
        @chmod(RP . "/mod/shop/wm_config.php", 0777);
        $fp = @fopen(RP . "/mod/shop/wm_config.php", "w");
        if (!$fp) {
            fclose($fp);
            return FALSE;
        }
        if (!@fwrite($fp, $conf_str)) {
            fclose($fp);
            return FALSE;
        }
        fclose($fp);
        return TRUE;
    }

    function saveAssistConf()
    {
        $conf_str = "<?php\n\$NEW = array(\n/* ABOCMS:START */\n";
        $conf_str .= "'shop_assist_currency' => '" . $_POST['assist_currency'] . "',\n'shop_assist_idp' => '" . $_POST['assist_idp'] . "',\n";
        $conf_str .= "'shop_assist_login' => '" . $_POST['assist_login'] . "',\n'shop_assist_password' => '" . $_POST['assist_password'] . "',\n";
        $conf_str .= "'shop_assist_success_url' => '" . $_POST['assist_success_url'] . "',\n'shop_assist_fail_url' => '" . $_POST['assist_fail_url'] . "',\n";
        $conf_str .= "'shop_assist_discount' => '" . $_POST['assist_discount'] . "'\n";
        $conf_str .= "/* ABOCMS:END */\n);\n?>";
        @chmod(RP . "/mod/shop/assist_config.php", 0777);
        $fp = @fopen(RP . "/mod/shop/assist_config.php", "w");
        if (!$fp) {
            fclose($fp);
            return FALSE;
        }
        if (!@fwrite($fp, $conf_str)) {
            fclose($fp);
            return FALSE;
        }
        fclose($fp);
        return TRUE;
    }

    function getNodeValue($parent, $node_name)
    {
        $node_array = $parent->get_elements_by_tagname($node_name);
        return (sizeof($node_array)) ? $this->conv_str_from_utf8($node_array[0]->get_content()) : '';
    }

    function getNode($parent, $node_name)
    {
        $node_array = $parent->get_elements_by_tagname($node_name);
        return (sizeof($node_array)) ? $node_array[0] : false;
    }

    function doImportXMLData($dom)
    {
        global $db;

        $insert = array();
        $ord_goods = array();
        $ids = array();
        $ord_query = "REPLACE INTO `{$this->table_prefix}_shop_orders` "
            . "(id, siteuser_id, delivery_id, manager_id, delivery_cost, customer, type, "
            . "address, phone, email, organization, jpinfo, date, sum, discount, total_sum, "
            . "currency, addinfo, status, pay_type, session, notes) VALUES ";
        $gds_query = "INSERT INTO {$this->table_prefix}_shop_order_items ("
            . "order_id, item_id, item_name, item_code, item_price, "
            . "item_currency, item_rate, item_qty, item_cost, info) VALUES ";
        $i = $j = $k = 0;
        foreach ($dom->get_elements_by_tagname('order') as $order) {
            $i++;
            $order_id = intval($order->get_attribute('id'));
            if ($order_id) {
                $ids[] = $order_id;
                $ins = "({$order_id},";
                $ins .= intval($order->get_attribute('user_id')) . ",";
                $node = $this->getNode($order, 'delivery');
                $ins .= ($node ? intval($node->get_attribute('id')) : '0') . ",";
                $ins .= intval($order->get_attribute('manager_id')) . ",";
                $ins .= ($node ? floatval($node->get_content()) : 0) . ",";
                $ins .= "'" . mysql_real_escape_string($this->getNodeValue($order, 'customer')) . "',";
                $ins .= "'" . mysql_real_escape_string($this->getNodeValue($order, 'type')) . "',";
                $ins .= "'" . mysql_real_escape_string($this->getNodeValue($order, 'address')) . "',";
                $ins .= "'" . mysql_real_escape_string($this->getNodeValue($order, 'phone')) . "',";
                $ins .= "'" . mysql_real_escape_string($this->getNodeValue($order, 'email')) . "',";
                $ins .= "'" . mysql_real_escape_string($this->getNodeValue($order, 'organization')) . "',";
                $ins .= "'" . mysql_real_escape_stringtring($this->getNodeValue($order, 'jpinfo')) . "',";
                $ins .= "'" . mysql_real_escape_string($this->getNodeValue($order, 'date')) . "',";
                $ins .= floatval($this->getNodeValue($order, 'sum')) . ",";
                $ins .= floatval($this->getNodeValue($order, 'discount')) . ",";
                $ins .= floatval($this->getNodeValue($order, 'total')) . ",";
                $ins .= "'" . mysql_real_escape_string($this->getNodeValue($order, 'currency')) . "',";
                $ins .= "'" . mysql_real_escape_string($this->getNodeValue($order, 'addinfo')) . "',";
                $ins .= intval($this->getNodeValue($order, 'status')) . ",";
                $node = $this->getNode($order, 'payment');
                $ins .= ($node ? intval($node->get_attribute('type')) : '0') . ",";
                $ins .= "'" . mysql_real_escape_string($this->getNodeValue($order, 'session')) . "',";
                $ins .= "'" . mysql_real_escape_string($this->getNodeValue($order, 'notes')) . "')";
                $insert[] = $ins;
                foreach ($order->get_elements_by_tagname('product') as $goods) {
                    $j++;
                    $k++;
                    $gds = "(" . $order_id . "," . intval($goods->get_attribute('id')) . ",";
                    $gds .= "'" . mysql_real_escape_string($this->getNodeValue($goods, 'name')) . "',";
                    $gds .= "'" . mysql_real_escape_string($this->getNodeValue($goods, 'code')) . "',";
                    $gds .= floatval($this->getNodeValue($goods, 'price')) . ",";
                    $gds .= "'" . mysql_real_escape_string($this->getNodeValue($goods, 'currency')) . "',";
                    $gds .= floatval($this->getNodeValue($goods, 'rate')) . ",";
                    $gds .= intval($goods->get_attribute('quantity')) . ",";
                    $gds .= floatval($this->getNodeValue($goods, 'cost')) . ",";
                    $gds .= "'" . mysql_real_escape_string($this->getNodeValue($goods, 'info')) . "')";
                    $ord_goods[] = $gds;
                }
                if ($i > 100) {
                    $db->query($ord_query . implode(", ", $insert));
                    $i = 0;
                    $insert = array();
                    $db->query("DELETE FROM {$this->table_prefix}_shop_order_items WHERE order_id IN (" . implode(",", $ids) . ")");
                    $ids = array();
                }
                if ($j > 100) {
                    $db->query($gds_query . implode(", ", $ord_goods));
                    $j = 0;
                    $ord_goods = array();
                }
            }
        }
        if ($i > 0) {
            $db->query($ord_query . implode(", ", $insert));
            $db->query("DELETE FROM {$this->table_prefix}_shop_order_items WHERE order_id IN (" . implode(",", $ids) . ")");
        }
        if ($j > 0) {
            $db->query($gds_query . implode(", ", $ord_goods));
        }
    }

    function my_iconv($charset_from, $charset_to, $str)
    {
        if ($ret = iconv($charset_from, $charset_to, $str)) {
            return $ret;
        }
        $str_arr = explode(" ", $str);
        for ($i = 0; $i < sizeof($str_arr); $i++) {
            $str_arr[$i] = iconv($charset_from, $charset_to, $str_arr[$i]);
        }
        return implode(" ", $str_arr);
    }

    function conv_str($str)
    {
        switch ($this->data_charset) {
            case 'cp1251':
                return $this->my_iconv("WINDOWS-1251", "UTF-8", $str);
            case 'koi8-r':
                return $this->my_iconv("KOI8-R", "UTF-8", $str);
            case 'iso-8859-1':
                return $this->my_iconv("ISO-8859-1", "UTF-8", $str);
            default:
                return $str;
        }
    }

    function conv_str_from_utf8($str)
    {
        switch ($this->data_charset) {
            case 'cp1251':
                return $this->my_iconv("UTF-8", "WINDOWS-1251", $str);
            case 'koi8-r':
                return $this->my_iconv("UTF-8", "KOI8-R", $str);
            case 'iso-8859-1':
                return $this->my_iconv("UTF-8", "ISO-8859-1", $str);
            default:
                return $str;
        }
    }

    /**
     * Метод для получения списка пользовательских (накопителных) скидок.
     *
     * @param int $user_discount_id - идентификатор скидки, если 0 - извлекается информация о всех скидках.
     */
    function GetUsersDiscounts($users_discount_id = false)
    {
        global $db;

        if (!$users_discount_id && $users_discount_id !== false) {
            return false;
        }

        $users_discount_id = to_int($users_discount_id);

        $where = '';
        if ($users_discount_id) {
            $where .= " AND {$this->table_prefix}_shop_users_discounts.id = '$users_discount_id'";
        }

        $sql = "SELECT * FROM {$this->table_prefix}_shop_users_discounts WHERE 1 $where ORDER BY id";
        $result = $db->getArrayOfResult($sql);

        return $result;
    }

    /**
     * Метод возвращаем список всех групп и признак их включения в указанную скидку.
     *
     * @param int $discount_id - идентификатор скидки.
     */
    function GetGroupsWithDiscounts($discount_id)
    {
        global $db;

        $discount_id = to_int($discount_id);

        $sql = "SELECT ug.*, dg.users_discound_id, dg.users_group_id FROM core_groups AS ug
		LEFT JOIN {$this->table_prefix}_shop_users_discounts_groups AS dg ON (ug.id = dg.users_group_id AND dg.users_discound_id = '$discount_id')
		ORDER BY ug.id";

        $result = $db->getArrayOfResult($sql);

        return $result;
    }

    /**
     * Метод удаляет все связи с группами для указанной скидки.
     *
     * @param int $discount_id - идентификатор скидки.
     */
    function ClearGroupsOfDiscount($discount_id)
    {
        global $db;

        $discount_id = to_int($discount_id);

        $sql = "DELETE FROM {$this->table_prefix}_shop_users_discounts_groups WHERE `users_discound_id` = '$discount_id'";
        $db->query($sql);

        return true;
    }

    /**
     * Метод добавляет связь скидки с группой пользователей.
     *
     * @param int $discount_id - идентификатор скидки.
     * @param int $group_id - идентификатор группы пользователей.
     * @return int идентификатор вставленной записи.
     */
    function InsertDiscountForGroup($discount_id, $group_id)
    {
        global $db;

        $discount_id = to_int($discount_id);
        $group_id = to_int($group_id);

        $sql = "INSERT INTO `{$this->table_prefix}_shop_users_discounts_groups` SET
			`users_discound_id` = '$discount_id',
			`users_group_id` = '$group_id'";

        $db->query($sql);
        if ($db->affected_rows() > 0) {
            return $db->lid();
        } else {
            return false;
        }
    }

    function InsertDiscount($params)
    {
        global $db;

        $params = to_array($params);
        $sql_params = array();

        $id = to_int($params['id']);

        if (isset($params['name']))
            $sql_params['name'] = to_str($params['name']);

        if (isset($params['min']))
            $sql_params['min'] = to_float($params['min']);

        if (isset($params['max']))
            $sql_params['max'] = to_float($params['max']);

        if (isset($params['value']))
            $sql_params['value'] = to_float($params['value']);

        if (isset($params['interval']))
            $sql_params['interval'] = to_int($params['interval']);

        if (isset($params['is_percent']))
            $sql_params['is_percent'] = to_int($params['is_percent']);

        if (isset($params['active']))
            $sql_params['active'] = to_int($params['active']);

        if (!count($sql_params)) {
            return false;
        }

        // Скомпонуем запрос.
        $m = array();
        foreach ($sql_params as $key => $value) {
            $m[] = "`$key` = '$value'";
        }

        $sql = implode(', ', $m);

        if ($id) {
            // SQL для обновления.
            $sql = "UPDATE `{$this->table_prefix}_shop_users_discounts` SET $sql WHERE `id` = '$id'";
        } else {
            // SQL для добавления.
            $sql = "INSERT INTO `{$this->table_prefix}_shop_users_discounts` SET $sql";
        }

        $db->query($sql);
        if ($id) {
            return $db->affected_rows() > 0 ? $id : false;
        } else {
            return $db->affected_rows() > 0 ? $db->lid() : false;
        }
    }

    function DeleteDiscount($discount_id)
    {
        global $db;

        $discount_id = to_int($discount_id);
        if (!$discount_id) {
            return false;
        }

        $sql = "DELETE FROM `{$this->table_prefix}_shop_users_discounts` WHERE `id` = '$discount_id'";
        $db->query($sql);

        return $db->affected_rows() > 0;
    }

    function GetOrderStat($period, $usertype, $status = -1, $manager = -1)
    {
        global $db;

        $from = $to = false;
        $data_format = "%d.%m.%Y";
        $status = '' == $status ? array() : explode(",", $status);
        foreach ($status as $key => $val) {
            if (-1 == $val) {
                $status = array();
            } else {
                $status[$key] = (int)$val;
            }
        }
        $manager = (int)$manager;
        $clause = array();
        if ($period['from']) {
            $from = Main::transform_date($period['from']);
            $clause[] = "date > '" . My_Sql::escape($from) . "'";
        }
        if ($period['to']) {
            $to = Main::transform_date($period['to']);
            $clause[] = "date < DATE_ADD('" . My_Sql::escape($to) . "', INTERVAL 1 DAY)";
        }
        if ('0' === $usertype) {
            $clause[] = "siteuser_id=0";
        } else if ($usertype > 0) {
            $clause[] = "client_type=" . intval($usertype);
        }
        if (!$to) {
            $to = date("Y-m-d");
        }
        if ($from && $to) {
            if ((strtotime($to) - strtotime($from)) > 24 * 30 * 24 * 3600) {
                $from = strtotime($from);
                $to = strtotime($to);
                $data_format = "%Y";
            } else if ((strtotime($to) - strtotime($from)) > 60 * 24 * 3600) {
                $from = strtotime($from);
                $to = strtotime($to);
                $data_format = "%m.%Y";
            }
        }
        if (!empty($status)) {
            $clause[] = "status IN (" . implode(",", $status) . ")";
        }
        if ($manager >= 0) {
            $clause[] = "manager_id={$manager}";
        }
        $ret = false;
        if (!empty($clause)) {
            $query = "SELECT DATE_FORMAT(date, '{$data_format}') as date, UNIX_TIMESTAMP(date) AS timestamp, "
                . "COUNT(*) as cnt, SUM(ROUND(total_sum)) as sum "
                . "FROM {$this->table_prefix}_shop_orders"
                . ($usertype > 0 ? " INNER JOIN core_users U ON U.id=siteuser_id" : "")
                . " WHERE " . implode(" AND ", $clause)
                . " GROUP BY DATE_FORMAT(date, '{$data_format}') ORDER BY timestamp";
            //echo "<!--$query-->";
            $db->query($query);
            while ($db->next_record()) {
                $ret[] = array(
                    'date' => $db->Record['date'],
                    'cnt' => $db->Record['cnt'],
                    'sum' => $db->Record['sum'],
                    'timestamp' => $db->Record['timestamp']
                );
            }
        }
        return $ret;
    }


    /**
     * Расчет накопительной скидки для пользователя.
     *
     *
     * @param int $sum - сумм
     * @param int $user_id - идентификатор пользователя.
     * @return mixed размер скидки.
     */
    function getAccumulativeDiscount($sum, $currency, $user_id = 0)
    {
        global $db, $CONFIG;

        $discount = 0;
        $ret = false;
        $sum = ShopPrototype::exchange(to_float($sum), $currency, $CONFIG['shop_order_concurency']);
        $user_id = $user_id ? (int)$user_id : $_SESSION['siteuser']['id'];
        if ($user_id < 1 || $sum <= 0) {
            return $ret;
        }

        // Список групп, к которым принадлежит пользователь.
        $groups = array();
        $db->query("SELECT group_id FROM core_users_groups WHERE user_id={$user_id}");
        while ($db->next_record()) {
            $groups[] = $db->Record['group_id'];
        }
        // Извлечем информацию о скидках, доступных для групп данного пользователя.
        // Если группа пользователей для скидки не указана, то скидна действительна для всех пользователей
        $dscnt_list = $db->getArrayOfResult("SELECT DISTINCT d.* FROM
		    {$this->table_prefix}_shop_users_discounts AS d LEFT JOIN
		    {$this->table_prefix}_shop_users_discounts_groups AS dg ON d.id=dg.users_discound_id
		    WHERE d.active AND (dg.users_group_id IS NULL"
            . (!empty($groups) ? " OR dg.users_group_id IN (" . implode(",", $groups) . "))" : ")"));
        if ($dscnt_list) {
            foreach ($dscnt_list as $dscnt) {
                $sql = "SELECT (SUM(sum)) AS `disc_sum` FROM
        		{$this->table_prefix}_shop_orders WHERE siteuser_id={$user_id}
        		AND date > DATE_SUB(NOW(), INTERVAL {$dscnt['interval']} MONTH)
        		GROUP BY siteuser_id HAVING disc_sum BETWEEN {$dscnt['min']} AND {$dscnt['max']}";
                $db->query($sql);
                if ($db->next_record()) {
                    $d = ($dscnt['is_percent']) ? ($sum / 100 * to_float($dscnt['value'])) : to_float($dscnt['value']);
                    if ($d > $discount) {
                        $discount = $d;
                        $ret = $dscnt;
                        $ret['discount_sum'] = number_format(ShopPrototype::exchange($d, $currency, $CONFIG['shop_order_concurency']), 2, ".", "");
                    }
                }
            }
        }
        return $ret;
    }

    function getCartProdList()
    {
        global $db, $CONFIG;
        static $list = false;

        if (empty($_list)) {
            $ids = array();
            foreach ($_SESSION["order"]["items"] as $key => $val) {
                $ids[] = (int)$key;
            }
            $ctlg = Module::load_mod('catalog', 'only_create_object');
            if (!empty($ids)) {
                $clnt_price = 'price_' . (@$_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : '1');
                $db->query("SELECT P.*, if(P.alias is not null and P.alias!='', P.alias, P.id) as product_alias,"
                    . " if(C.alias is not null and C.alias!='', C.alias, C.id) as ctg_alias FROM "
                    . "{$this->table_prefix}_catalog_products P INNER JOIN {$this->table_prefix}_catalog_categories C "
                    . "ON C.id=category_id WHERE P.id IN (" . implode(", ", $ids) . ")");
                while ($db->next_record()) {

                    $new_price = $this->getNewProductPrice($db->Record['id']);
                    $new_price_p = $this->getNewProductPrice($db->Record['id'],true);

                    $_list[$db->Record['id']] = array(
                        'id' => $db->Record['id'],
                        'alias' => $db->Record['alias'],
                        'category_id' => $db->Record['category_id'],
                        'title' => $db->Record['product_title'],
                        'code' => $db->Record['product_code'],
                        'inf' => $db->Record['product_inf'],
                        'description' => $db->Record['product_description'],
                        'currency' => $db->Record['currency'],
                        'currency_name' => $ctlg->currencies[$db->Record['currency']],
                        'price_1' => $ctlg->getCostWithTaxes($db->Record['price_1']),
                        'price_2' => $ctlg->getCostWithTaxes($db->Record['price_2']),
                        'price_3' => $ctlg->getCostWithTaxes($db->Record['price_3']),
                        'price_4' => $ctlg->getCostWithTaxes($db->Record['price_4']),
                        'price_5' => $ctlg->getCostWithTaxes($db->Record['price_5']),
                        'new_price' => $new_price ? 'new_price' : '',
                        'new_price_p' => $new_price_p ? $new_price_p : $ctlg->getCostWithTaxes($db->Record[$clnt_price]),
                        'price' => $new_price ? $new_price : $ctlg->getCostWithTaxes($db->Record[$clnt_price]),
                        'image' => $db->Record['image_middle'],
                        'image_big' => $db->Record['image_big'],
                        'availability' => $db->Record['product_availability'],
                        'prod_alias' => $db->Record['product_alias'],
                        'ctgr_alias' => $db->Record['ctg_alias'],
                        'bonus' => $db->Record['bonus'],
                        'weight' => $db->Record['weight'],
                        'dimensions' => $db->Record['dimensions'],
                    );
                }
            }
        }

        return $_list;
    }

    /**
     * Получить уникальный ключ для списка свойств
     *
     * @param array $prop_list
     * @return mixed string OR false
     */
    function getPropKey($prop_list)
    {
        $ret = false;
        if (!empty($prop_list)) {
            ksort($prop_list);
            $ret = md5(serialize($prop_list));
        }
        return $ret;
    }

    function getDelivery($id)
    {
        global $db;

        $db->query("SELECT * FROM {$this->table_prefix}_shop_delivery WHERE id=" . intval($id));
        $ret = false;
        if ($db->next_record()) {

            if (isset($_POST['api_ems'])) {
                if ($_POST['api_ems']) {
                    $delivApi = (float)$_POST['api_ems'];
                    if ($_POST['api_ems'] == "moscov") {

                        $delivApi = 0;
                        if ($db->Record['cost']) $_SESSION['order']['cost'] = $_SESSION['order']['cost'] - $db->Record['cost'];
                        $_POST['api_ems'] = '0o';
                    }
                }
                else{
                    if($id ==10){
                        $delivApi = 300;
                    }
                }
            }
            else{
                if($id ==10){
                    $delivApi = 0;
                }
            }
            $ret = array(
                'id' => $db->Record['id'],
                'title' => $db->Record['title'],
                'cost' => isset($delivApi) ? $delivApi : $db->Record['cost'],
                'dscr' => $db->Record['dscr'],
                'rank' => $db->Record['rank'],
            );
        }
        return $ret;
    }

    function deliveryExists($id)
    {
        global $db;

        $db->query("SELECT id FROM {$this->table_prefix}_shop_delivery WHERE id=" . intval($id));
        return $db->nf() ? true : false;
    }

    function delDelivery($id)
    {
        global $db;
        $db->query("DELETE FROM {$this->table_prefix}_shop_delivery WHERE id=" . intval($id));

        return $db->affected_rows() ? true : false;
    }

    function setDelivery($data)
    {
        global $db;

        $data['title'] = My_Sql::escape($data['title']);
        $data['dscr'] = My_Sql::escape($data['dscr']);
        $data['cost'] = (float)$data['cost'];
        $ret = false;
        if (!empty($data['title']) && $data['cost'] >= 0) {
            if ($data['id']) {
                if ($this->deliveryExists($data['id'])) {
                    $db->query("UPDATE {$this->table_prefix}_shop_delivery SET title='"
                        . $data['title'] . "', cost={$data['cost']}, dscr='"
                        . $data['dscr'] . "', rank=" . $data['rank'] . " WHERE id=" . intval($data['id']));
                    $ret = true;
                }
            } else {
                $db->query("INSERT INTO {$this->table_prefix}_shop_delivery SET title='"
                    . $data['title'] . "', cost={$data['cost']}, dscr='"
                    . $data['dscr'] . "', rank=" . $data['rank']);
                $ret = $db->affected_rows() ? $db->lid() : false;
            }
        }
        return $ret;
    }

    function getDeliveryList()
    {
        global $db;

        $db->query("SELECT * FROM {$this->table_prefix}_shop_delivery ORDER BY rank,id");
        $ret = false;
        while ($db->next_record()) {
            $ret[$db->Record['id']] = array(
                'id' => $db->Record['id'],
                'title' => $db->Record['title'],
                'cost' => $db->Record['cost'],
                'dscr' => $db->Record['dscr'],
                'rank' => $db->Record['rank'],
            );
        }
        return $ret;
    }

    function showDeliveryList($transurl, $active_dlvr = 0)
    {
        global $tpl;
        $ret = false;
        $list = $this->getDeliveryList();
        if ($list) {
            $ret = true;
            $tpl->newBlock('block_delivery_list');
            $assign = array(
                'MSG_Ed' => $this->_msg['Ed'],
                'Edit' => $this->_msg['Edit'],
                'MSG_Del' => $this->_msg['Del'],
                'MSG_Delete' => $this->_msg['Delete'],
                'MSG_Title' => $this->_msg['Title'],
                'MSG_Cost' => $this->_msg['Cost'],
                'MSG_description' => $this->_msg['description'],
                'transurl' => $transurl,
                'baseurl' => $transurl
            );
            $tpl->assign($assign);

            foreach ($list as $key => $val) {
                if (!in_array($val['id'], array(7))) {
                    $tpl->newBlock('block_delivery');
                    $val['checked'] = $active_dlvr == $val['id'] ? 'checked' : '';
                    $val['selected'] = $val['checked'] ? 'selected' : '';
                    if (!in_array($val['id'], array(7))) $val['link_term'] = "<a target='_blank' href='/terms-delivery/#" . $val['id'] . "'>(Условия)</a>";
                    $tpl->assign($assign + $val);
                }

            }
        }
        return $ret;
    }

    function showDelivery($id)
    {
        global $tpl;
        $ret = false;
        $dlvr = $this->getDelivery($id);
        if ($dlvr) {
            $ret = true;
            $tpl->newBlock('block_delivery');
            $tpl->assign($dlvr);
        }
        return $ret;
    }

    function getOrdStatus($id)
    {
        global $db;

        $db->query("SELECT * FROM {$this->table_prefix}_shop_order_status WHERE id=" . intval($id));
        $ret = false;
        if ($db->nf()) {
            $db->next_record();
            $ret = array(
                'id' => $db->Record['id'],
                'title' => $db->Record['title'],
                'payable' => $db->Record['payable'],
            );
        }
        return $ret;
    }

    function ordStatusExists($id)
    {
        global $db;

        $db->query("SELECT id FROM {$this->table_prefix}_shop_order_status WHERE id=" . intval($id));
        return $db->nf() ? true : false;
    }

    function delOrdStatus($id)
    {
        global $db;
        $db->query("DELETE FROM {$this->table_prefix}_shop_order_status WHERE id=" . intval($id));

        return $db->affected_rows() ? true : false;
    }

    function setOrdStatus($data)
    {
        global $db;

        $data['title'] = My_Sql::escape($data['title']);
        $ret = false;
        if (!empty($data['title'])) {
            if ($data['id']) {
                if ($this->ordStatusExists($data['id'])) {
                    $db->query("UPDATE {$this->table_prefix}_shop_order_status SET title='"
                        . $data['title'] . "', payable=" . ($data['payable'] ? 1 : 0)
                        . " WHERE id=" . intval($data['id']));
                    $ret = true;
                }
            } else {
                $db->query("INSERT INTO {$this->table_prefix}_shop_order_status SET title='"
                    . $data['title'] . "', payable=" . ($data['payable'] ? 1 : 0));
                $ret = $db->affected_rows() ? $db->lid() : false;
            }
        }
        return $ret;
    }

    function getOrdStatusList($includ_default = true)
    {
        global $db;
        static $ret = false;

        if (!$ret) {
            $ret = $includ_default ? array(0 => array(
                'id' => 0,
                'title' => $this->_msg['Default_status'],
                'payable' => 0,
            )) : array();
            $db->query("SELECT * FROM {$this->table_prefix}_shop_order_status ORDER BY id");
            while ($db->next_record()) {
                $ret[$db->Record['id']] = array(
                    'id' => $db->Record['id'],
                    'title' => $db->Record['title'],
                    'payable' => $db->Record['payable'],
                );
            }
        }
        return $ret;
    }

    function showOrdStatusList($transurl, $includ_default = true)
    {
        global $tpl;
        $ret = false;
        $list = $this->getOrdStatusList($includ_default);
        if ($list) {
            $ret = true;
            $tpl->newBlock('block_status_list');
            $assign = array(
                'MSG_Ed' => $this->_msg['Ed'],
                'Edit' => $this->_msg['Edit'],
                'MSG_Del' => $this->_msg['Del'],
                'MSG_Delete' => $this->_msg['Delete'],
                'MSG_Title' => $this->_msg['Title'],
                'MSG_Payable' => $this->_msg["Payable"],
                'transurl' => $transurl,
                'baseurl' => $transurl
            );
            $tpl->assign($assign);
            foreach ($list as $val) {
                $tpl->newBlock('block_status');
                $tpl->assign($assign + $val);
            }
        }
        return $ret;
    }

    function getStatusTitle($title, $id)
    {
        return $title ? $title : (0 == $id ? $this->_msg['Default_status'] : $this->_msg['undef']);
    }

    function getStatParamXml()
    {
        global $db, $CONFIG;

        $siteusers = Module::load_mod('siteusers');
        $list = $siteusers->getGroups();
        $ret = "<result>\n";
        $ret .= "<usertypes>\n";
        $ret .= "<usertype><id>-1</id><title>{$this->_msg['All_groups']}</title></usertype>\n";
        $ret .= "<usertype><id>0</id><title>{$this->_msg['No_auth_user']}</title></usertype>\n";
        foreach ($CONFIG['siteusers_client_types'] as $key => $val) {
            $ret .= "<usertype><id>{$key}</id><title>{$val}</title></usertype>\n";
        }
        $ret .= "</usertypes>\n";
        $ret .= "<states>\n";
        $ret .= "<state><id>-1</id><title>{$this->_msg['All']}</title></state>\n";
        $list = $this->getOrdStatusList(true);
        foreach ($list as $key => $val) {
            $ret .= "<state><id>{$key}</id><title>{$val['title']}</title></state>\n";
        }
        $ret .= "</states>\n";
        $ret .= "<managers>\n";
        $ret .= "<manager><id>-1</id><username>{$this->_msg['All']}</username></manager>\n";
        $list = $this->getManagersList();
        if (!empty($list)) {
            foreach ($list as $key => $val) {
                $ret .= "<manager><id>{$key}</id><username>{$val['username']}</username></manager>\n";
            }
        }
        $ret .= "</managers>\n";
        $ret .= "</result>\n";
        return $ret;
    }

    function updateTransServ($id, $value)
    {
        global $db;
        $editor = $_SESSION['siteuser']['siteusername'];
        $db->query("UPDATE {$this->table_prefix}_shop_orders SET transportation_services={$value}, total_sum=total_sum+{$value},editor='{$editor}' WHERE id={$id}");

    }

    function updateDiscountEdit($id, $value)
    {
        global $db;
        $db->query("UPDATE {$this->table_prefix}_shop_orders SET discount={$value}, total_sum=total_sum-{$value} WHERE id={$id}");
    }

    function getProductLink($id)
    {
        global $db;
        $db->query("SELECT category_id FROM {$this->table_prefix}_catalog_products WHERE id={$id}");
        while ($db->next_record()) {
            $result = $db->Record['category_id'];
        }
        return "/catalog/$result/$id.html";
    }

    /**
     * @param $d
     * @param string $format
     * @param int $offset
     * @return bool|string
     */
    function rusdate($d, $format = 'j %MONTH% Y', $offset = 0)
    {
        $montharr = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        $dayarr = array('понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье');

        $d += 3600 * $offset;

        $sarr = array('/%MONTH%/i', '/%DAYWEEK%/i');
        $rarr = array($montharr[date("m", $d) - 1], $dayarr[date("N", $d) - 1]);

        $format = preg_replace($sarr, $rarr, $format);
        return date($format, $d);
    }

    function getProductTitle($id)
    {
        global $db;
        $query = "SELECT product_title FROM {$this->table_prefix}_catalog_products WHERE id={$id}";
        $res = $db->getArrayOfResult($query);
        if ($res) {
            return $res[0]['product_title'];
        }
        return "";
    }

    function sendSms($phone, $request, $status, $new = false)
    {
        $userName = 'konoshanov@deltael.ru';
        $password = '9k9a27bd';
        $senderName = "Suprashop";
        //$phone='+375297981745';
        $text = 'Статус вашего заказа №' . $request . ' на сайте suprashop.ru изменён на ' . $status;
        if ($new) $text = 'Ваш заказ номер №' . $request . ' на сайте suprashop.ru принят.';
        $text .= "<br/> Телефоны для связи: 7 (800) 775-49-37 ";
        $src = '<?xml version="1.0" encoding="UTF-8"?>
                <SMS>
                <operations>
                <operation>SEND</operation>
                </operations>
                <authentification>
                <username>' . $userName . '</username>
                <password>' . $password . '</password>
                </authentification>
                <message>
                <sender>' . $senderName . '</sender>
                <text>' . $text . '</text>
                </message>
                <numbers>
                <number messageID="msg11">' . $phone . '</number>
                </numbers>
                </SMS>';

        $Curl = curl_init();
        $CurlOptions = array(
            CURLOPT_URL => 'http://atompark.com/members/sms/xml.php',
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_POST => true,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 15,
            CURLOPT_TIMEOUT => 100,
            CURLOPT_POSTFIELDS => array('XML' => $src),
        );
        curl_setopt_array($Curl, $CurlOptions);
        if (false === ($Result = curl_exec($Curl))) {
            var_dump("СМС не отправлена");
            return $Result;
        }

        curl_close($Curl);

        return $Result;
    }

    function getUserAddress($user_id)
    {
        global $db;
        $q = "Select * from sup_rus_address as a where a.user_id= {$user_id} AND `primary`=1";
        return $db->getArrayOfResult($q);
    }


    function getUserReview($siteuser_id)
    {
        global $CONFIG, $db;
        $siteuser_id = (int)$siteuser_id;
        if ($siteuser_id) {
            $db->query('SELECT U.email,DATE_FORMAT(R.date,"' . $CONFIG['shop_date_format'] . '") date, R.message, R.manager_id, R.user_id
							FROM core_users_review R LEFT JOIN  core_users U ON U.id=R.manager_id
							WHERE R.user_id = ' . $siteuser_id . '
							ORDER BY date DESC');
            if ($db->nf() > 0) {
                for ($i = 0; $i < $db->nf(); $i++) {
                    $db->next_record();
                    $discount = Main::process_price($db->f('discount'));
                    $discount = ($discount > 0 ? "+" : "") . $discount;
                    $arr[$i]['manager'] = $db->f('email');
                    $arr[$i]['manager_id'] = $db->f('manager_id');
                    $arr[$i]['date'] = $db->f('date');
                    $arr[$i]['message'] = $db->f('message');
                }
                return $arr;
            }
        }
        return FALSE;
    }

    function getIssetUser($email, $phone)
    {
        global $db;

        $arr = $db->getArrayOfResult("SELECT id
                                FROM `core_users` WHERE email ='{$email}'");
        if (!empty($arr)) {
            return $arr[0]['id'];
        }
        $arr = $db->getArrayOfResult("SELECT u.id FROM core_users as u LEFT JOIN core_users_prop_val as v ON v.user_id= u.id
                                    WHERE v.value='{$phone}' AND v.prop_id=4");
        if (!empty($arr)) {
            return $arr[0]['id'];
        }
        return false;
    }

    function getRoboPay($inv_id, $inv_desc, $out_summ)
    {
        // your registration data
        $mrh_login = "konoshanov";
        // your login here
        $mrh_pass1 = "sgg8Ff52A61";
        // merchant pass1 here
        // build CRC value
        $crc = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1");
        // build URL
        $url = "https://auth.robokassa.ru/Merchant/Index.aspx?MrchLogin=$mrh_login&" . "OutSum=$out_summ&InvId=$inv_id&Desc=$inv_desc&SignatureValue=$crc";
        // print URL if you need
        return "<a href='$url'>Оплатить</a>";
    }

    function getAllFilesInfo()
    {
        global $db;
        return $db->getArrayOfResult("SELECT COUNT(*) AS cnt, `filename`,`name` FROM sup_rus_catalog_files GROUP BY `filename` ORDER BY `cnt` DESC");

    }

    function updateprodfiles($filename, $nFilename, $nName)
    {
        global $db;

        $db->query("UPDATE sup_rus_catalog_files SET `name`='$nName',filename='$nFilename'
                      WHERE filename='$filename'");
    }

    function deleteprodfiles($filename)
    {

        global $db;

        $r = $db->query("DELETE FROM sup_rus_catalog_files WHERE filename='$filename'");
        if ($r) {
            //@chmod (PR."/files/catalog/sup_rus/files/".$filename, 7777);
            $filename = iconv('UTF-8', 'WINDOWS-1251//IGNORE', $filename);
            @unlink(RP . "/files/catalog/sup_rus/files/" . $filename);

        }
    }

    function excelPrint($order)
    {
        global $CONFIG;
        $cur = $order['cost_rur_in_words'];
        $items = $this->getOrderItems($order['order_id']);
        $itemsInfo = "";
        require_once RP . '/inc/PHPExcel.php';
        require_once RP . '/inc/PHPExcel/IOFactory.php';
        $countI = 0;
        foreach ($items as $item) {
            $countI++;
            if ($item['gift_title']) $countI++;

        }
        if ($countI > 7) {
            $objPHPExcel = PHPExcel_IOFactory::load(RP . "/files/order_excel/Propis_middle.xls");
            $worksheet = $objPHPExcel->getActiveSheet();
            $worksheet->setCellValueByColumnAndRow(2, 47, "$cur");
        } elseif ($countI > 26) {
            $objPHPExcel = PHPExcel_IOFactory::load(RP . "/files/order_excel/Propis_big.xls");
            $worksheet = $objPHPExcel->getActiveSheet();
            $worksheet->setCellValueByColumnAndRow(2, 101, "$cur");
        } else {
            $objPHPExcel = PHPExcel_IOFactory::load(RP . "/files/order_excel/Propis.xls");
            $worksheet = $objPHPExcel->getActiveSheet();
            $worksheet->setCellValueByColumnAndRow(2, 27, "$cur");
        }
        $s = substr($order['date'], 0, 10);
        $a = explode(".", $s);
        $time = mktime(0, 0, 0, $a[1], $a[2], $a[0]);
        $month = $this->rusdate($time, '%MONTH%');
        $day = date('d', $time);
        $year = date('Y', $time);
        $worksheet->setCellValueByColumnAndRow(0, 12, "Товарная накладная №" . $order['order_id'] . "\r\n от " . $day . " " . $month . " " . $year);
        /*  $worksheet->setCellValueByColumnAndRow(1, 15, "Доставка");
         $worksheet->setCellValueByColumnAndRow(2, 15,(string)$order['delivery_cost']);
         $worksheet->setCellValueByColumnAndRow(3, 15, "1");
         $worksheet->setCellValueByColumnAndRow(5, 15, (string)$order['delivery_cost']);*/
        $worksheet->setCellValueByColumnAndRow(1, 15, "Транспортные услуги");
        $worksheet->setCellValueByColumnAndRow(2, 15, (string)$order['transp']);
        $worksheet->setCellValueByColumnAndRow(3, 15, "1");
        $worksheet->setCellValueByColumnAndRow(5, 15, (string)$order['transp']);
        /*   $worksheet->setCellValueByColumnAndRow(1, 16, "Скидка");
           $worksheet->setCellValueByColumnAndRow(3, 16, "1");
           $discountV=0;
           $worksheet->setCellValueByColumnAndRow(2, 16, (float)$order['discount']);
           if($order['discount'] > 0) $discountV= "-".(float)$order['discount'];
           $worksheet->setCellValueByColumnAndRow(5, 16, (string)$discountV);
           $worksheet->setCellValueByColumnAndRow(5, 16, (string)$discountV);*/
        $z = 16;
        $discount = (float)$order['discount'] * 100 / $order['sum'];
        foreach ($items as $item) {
            $countV = "";
            $link = $this->getProductLink($item['item_id']);
            $worksheet->setCellValueByColumnAndRow(1, $z, $item['item_name']);
            $worksheet->setCellValueByColumnAndRow(2, $z, $item['item_price']);
            $worksheet->setCellValueByColumnAndRow(3, $z, $item['item_qty']);
            $worksheet->setCellValueByColumnAndRow(4, $z, "шт.");
            $worksheet->setCellValueByColumnAndRow(5, $z, $discount ? $item['item_cost'] - ($item['item_cost'] * $discount / 100) : $item['item_cost']);
            if ($item['gift_title']) {
                $z++;
                $worksheet->setCellValueByColumnAndRow(1, $z, $item['gift_title']);
                $worksheet->setCellValueByColumnAndRow(2, $z, "подарок");
                $worksheet->setCellValueByColumnAndRow(3, $z, $item['item_qty']);
                $worksheet->setCellValueByColumnAndRow(4, $z, "шт.");
                $worksheet->setCellValueByColumnAndRow(5, $z, 0);
            } elseif ($item['kupon']) {
                $z++;
                $worksheet->setCellValueByColumnAndRow(1, $z, "Подарочный купон " . $item['kupon']);
                $worksheet->setCellValueByColumnAndRow(2, $z, $item['kupon']);
                $worksheet->setCellValueByColumnAndRow(3, $z, $item['item_qty']);
                $worksheet->setCellValueByColumnAndRow(4, $z, "шт.");
                $worksheet->setCellValueByColumnAndRow(5, $z, 0);
            }
            /* if($item['item_qty'] > 1)
             {
                 for($i=0;$i < $item['item_qty'];$i++)
                 {
                     $countV=$countV.'<input type="hidden" name="product_id[]" value="'.$item['item_id'].'" />';
                 }
             }
             else
             {*/
            $countV = '<input type="hidden" name="product_id[]" value="' . $item['item_id'] . ":" . $item['id'] . '" />';
            $countV = $countV . '<input type="text" name="' . $item['id'] . '" value="' . $item['item_qty'] . '" class="productCount"/>';
            //  }
            $giftV = "";
            if ($item['gift_id']) $giftV = '+ подарок ' . $item['gift_title'];
            elseif ($item['kupon']) $giftV = '+ подарок купон на' . $item['kupon'] . " руб.";
            $itemsInfo = $itemsInfo . '<div class="itemValue">' . $countV . '-<a href="' . $link . '">' . $item['item_name'] . $giftV . '</a>&nbsp;&nbsp;<span onclick="delI(this)" class="delL"><img class="create" src="/i/admin/b_delete.gif"/></span></div>';
            $z++;
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save(RP . '/files/order_excel/' . $order['order_id'] . '.xls');

        if ((strripos($order['address'], "Москва") !== false) || (strripos($order['address'], "москва") !== false) || (strripos($order['address'], "МОСКВА") !== false)) {

            //$mail->AddEmbeddedImage(RP.'i/', 'cirlogo.jpg', 'cirlogo', 'base64', 'image/jpeg');
            $tpl1 = new TemplatePower(RP . '_shop_waybill.html', T_BYFILE);
            $items = $this->getOrderItems($order['order_id']);
            $orederInfo = $this->getOrder($order['order_id']);
            $i = 1;
            $tableData = "";
            foreach ($items as $item) {
                $tableData = $tableData . "<tr><td>$i</td><td>" . $item['item_name'] . "</td><td>" . $item['item_price'] . "</td><td>" . $item['item_qty'] . "</td><td>шт.</td><td>" . $item['item_price'] . "</td></tr>";
                $i++;
            }
            $tableData = $tableData . "<tr><td>" . $i . "</td><td>Доставка</td><td>" . $order['delivery_cost'] . "</td><td>1</td><td>шт.</td><td>" . $order['delivery_cost'] . "</td></tr>";
            $i++;
            $tableData = $tableData . "<tr><td>" . $i . "</td><td>Транспортные услуги</td><td>" . $orederInfo['transp'] . "</td><td>1</td><td>шт.</td><td>" . $orederInfo['transp'] . "</td></tr>";
            $i++;
            $tableData = $tableData . "<tr><td>" . $i . "</td><td>Скидка</td><td>" . $orederInfo['discount'] . "</td><td>1</td><td>шт.</td><td>" . $orederInfo['discount'] . "</td></tr>";
            $count = $i;
            $i++;
            $k = $i + 3;
            for ($i; $i < $k; $i++) {
                $tableData = $tableData . "<tr><td>$i</td><td></td><td></td><td></td><td></td><td></td></tr>";
            }
            $date = $this->rusdate(time());
            $tpl1->prepare();
            $tpl1->assign(array(
                "order_id" => $order['order_id'],
                "date" => iconv('utf-8', 'windows-1251', $date),
                "table_data" => iconv('utf-8', 'windows-1251', $tableData),
                "item_count" => $count,
                "summ" => iconv('utf-8', 'windows-1251', $orederInfo['total_sum']),
                "summ_p" => iconv('utf-8', 'windows-1251', $orederInfo['cost_rur_in_words'])

            ));

        } else {
            $time = time();//strtotime($order['date']);
            $tpl1 = new TemplatePower(RP . 'tpl/sup_rus/_kvit_template.html', T_BYFILE);
            $tpl1->prepare();
            $tpl1->assign(array(
                'name' => iconv('utf-8', 'windows-1251', $order['customer']),
                'num' => $order['order_id'],
                'company' => iconv('utf-8', 'windows-1251', $CONFIG['shop_company']),
                'inn' => iconv('utf-8', 'windows-1251', $CONFIG['shop_inn']),
                'rs' => iconv('utf-8', 'windows-1251', $CONFIG['shop_sett_account']),
                'bank' => iconv('utf-8', 'windows-1251', $CONFIG['shop_bank']),
                'bik' => iconv('utf-8', 'windows-1251', $CONFIG['shop_bik']),
                'corr' => iconv('utf-8', 'windows-1251', $CONFIG['shop_corr_account']),

                'address' => iconv('utf-8', 'windows-1251', $order['address']),
                'day' => date('d'),
                'month' => iconv('utf-8', 'windows-1251', $this->rusdate($time, '%MONTH%')),
                'year' => date('Y'),
                'total' => (double)$order['total_sum'],

            ));
        }

        $f = fopen(RP . "kvitancia.htm", "w+");
        fwrite($f, $tpl1->getOutputContent());
        fclose($f);
        copy(RP . "kvitancia.htm", RP . '/files/order_excel/kvitancia' . $order['order_id'] . '.htm');
        /*$error="";
            if(extension_loaded('zip')){
                    $zip = new ZipArchive();			// Load zip library
                    $zip_name = RP."/files/order_excel/".$order['order_id'].".zip";
                    if($zip->open($zip_name, ZIPARCHIVE::CREATE)!==TRUE){		// Opening zip file to load files
                        $error .=  "* Sorry ZIP creation failed at this time<br/>";
                    }
                        $zip->addFile(RP."/files/order_excel/".$order['order_id'].".xlsx", $order['order_id'].".xlsx");			// Adding files into zip
                        $zip->addFile(RP."/files/order_excel/Propis.bas","Propis.bas");
                    $zip->close();
            }else
                $error .= "* You dont have ZIP extension<br/>";*/

        return $itemsInfo;
    }


    function getSearchForTitleData()
    {
        global $server, $lang;
        //return array();
        $query = 'SELECT product_title as label,category_id,id FROM ' . $server . $lang . '_catalog_products';
        $data = array();
        if (!$result = mysql_query($query)) die(mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            $data[] = $row;
        }
        return $data;
    }

    function getUserBonusBalance($id)
    {
        global $db;

        $pBalance = $db->getArrayOfResult("SELECT SUM(bonus)AS b FROM `core_users_bonus_data` WHERE user_id={$id} AND `status`=1");
        if (is_array($pBalance)) {
            $pBalance = $pBalance[0]['b'];
        }
        $oBalance = $db->getArrayOfResult("SELECT SUM(bonus)AS b FROM `core_users_bonus_data` WHERE user_id={$id} AND `status`=2");
        if (is_array($oBalance)) {
            $oBalance = $oBalance[0]['b'];
        }
        if (is_null($pBalance)) $pBalance = 0;
        if (is_null($oBalance)) $oBalance = 0;
        return ($pBalance - $oBalance);
    }

    function getOrderBonus($id)
    {
        global $db2;

        $arr = $db2->getArrayOfResult("SELECT SUM(bonus)AS b  FROM `core_users_bonus_data` WHERE order_id={$id} AND (`status`=2 OR `status`=3 OR `status`=5)");
        if (is_array($arr)) {
            $bonus = $arr[0]['b'];
        }
        return $bonus;


    }

    function getActionProductGifts($id)
    {
        global $server, $lang, $tpl, $db;
        if ($ret = $this->getGiftToProdEl($id)) {
            $gift = $ret[1];
            $gift_if = $ret[0][2];
            if (sizeof($ret[0]) == 1) {

                $product_info = $this->getProduct($id);
                $tpl->newBlock('choose_double_gift');
                $tpl->assign("act_sel", $product_info['product_title'] . "+" . (($ret[0][0][0]['id'] == $id) ? $ret[0][0][1]['product_title'] : $ret[0][0][0]['product_title']) . " = " . $gift['product_title']);
                $tpl->assign('noact_sel', $product_info['product_title']);
            } elseif (sizeof($ret[0]) == 2) {
                $kupon = (int)$ret[0][0][0]['kupon'];
                if ($gift && $kupon) $block = 'kupon_gift';
                elseif ($kupon) $block = 'kupon';
                elseif ($gift && (int)$ret[0][0][0]['percent']) $block = 'percent_gift';
                else $block = 'gift';
                $link = '/catalog/' . ($gift['calias'] ? $gift['calias'] : $gift['cid']) . '/' . ($gift['alias'] ? $gift['alias'] : $gift['id']) . '.html';
                if ($gift && $kupon) {
                    $tpl->newBlock('choose_gift');
                    $tpl->assign('title', $gift['product_title']);
                    $tpl->assign('link', $link);
                    $tpl->assign('price', $kupon);
                }elseif ($gift && (int)$ret[0][0][0]['percent']) {
                    $tpl->newBlock('choose_gift_percent');
                    $tpl->assign('title', $gift['product_title']);
                    $tpl->assign('link', $link);
                    $tpl->assign('price', (int)$ret[0][0][0]['percent'].'%');
                }  else {
                    $product_info = $this->getProduct($id);
                    $tpl->newBlock('one_gift');
                    $tpl->assign('title', $gift['product_title']);
                    $tpl->assign('link', $link);
                    $tpl->assign('product_title', $product_info['product_title']);
                }
            } elseif (sizeof($ret[0]) >= 3) {
                $kupon = (int)$ret[0][0][0]['kupon'];
                $block = 'gift';
                $link = '/catalog/' . ($gift['calias'] ? $gift['calias'] : $gift['cid']) . '/' . ($gift['alias'] ? $gift['alias'] : $gift['id']) . '.html';
                $link_if = '/catalog/' . ($gift_if['calias'] ? $gift_if['calias'] : $gift_if['cid']) . '/' . ($gift_if['alias'] ? $gift_if['alias'] : $gift_if['id']) . '.html';
                $tpl->newBlock('choose_gift_if');
                $tpl->assign('title', $gift['product_title']);
                $tpl->assign('link', $link);
                $tpl->assign('title_if', $gift_if['product_title']);
                $tpl->assign('link_if', $link_if);
                if (count($ret[0]) > 3) {
                    $l = 0;
                    foreach ($ret[0] as $value) {
                        $l++;
                        if ($l < 4) continue;
                        $tpl->newBlock('choose_gift_ifs');
                        $tpl->assign('title_if', $value['product_title']);
                        $link_ifs = '/catalog/' . ($value['calias'] ? $value['calias'] : $value['cid']) . '/' . ($value['alias'] ? $value['alias'] : $value['id']) . '.html';
                        $tpl->assign('link_if', $link_ifs);
                        $tpl->assign('id', $l);
                    }
                }
            } else {
                $product_info = $this->getProduct($id);
                $tpl->newBlock('percent');

            }

        }
    }

    function getProduct($id)
    {
        global $db, $CONFIG, $baseurl;

        $id = (int)$id;
        $ret = false;
        if ($id) {
            $clnt_price = 'price_' . (@$_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : '1');
            $db->query('SELECT	a.id AS id,
							a.alias,
							a.bonus,
							a.link,
							a.product_title AS product_title
						FROM ' . $this->table_prefix . '_catalog_products a
						WHERE a.id = ' . $id);
            if ($db->next_record()) {
                $ret['id'] = $db->f('id');
                $ret['alias'] = $db->f('alias');
                $ret['link'] = $db->f('link');
                $ret['bonus'] = $db->f('bonus');
                $ret['product_title'] = $db->f('product_title');
            }
        }
        return $ret;
    }

    function getSignature($Shop_IDP, $Order_ID, $Subtotal_P, $MeanType
        , $EMoneyType, $Lifetime, $Customer_IDP, $Card_IDP
        , $IData, $PT_Code, $password)
    {
        $Signature = strtoupper(
            md5(
                md5($Shop_IDP) . "&" .
                md5($Order_ID) . "&" .
                md5($Subtotal_P) . "&" .
                md5($MeanType) . "&" .
                md5($EMoneyType) . "&" .
                md5($Lifetime) . "&" .
                md5($Customer_IDP) . "&" .
                md5($Card_IDP) . "&" .
                md5($IData) . "&" .
                md5($PT_Code) . "&" .
                md5($password)
            )
        );
        return $Signature;
    }

    function checkSignature($Order_ID, $Status, $Signature)
    {
        $password = "GuLfR2liwdJbxGfUqV4z1egBk4noMGo90yh5JkNQuSGkvBMNzlc2cbWlnfbzayKT27LgCI5SCTyIkzko"; // пароль из ЛК Uniteller
        // проверка подлинности подписи и данных
        return ($Signature == strtoupper(md5($Order_ID . $Status . $password)));
    }

    function getDataUniteller($Order_ID)
    {

$Shop_ID = "00007729"; // идентификатор точки продажи
$Login = '2393'; // логин из ЛК Uniteller
$Password = "GuLfR2liwdJbxGfUqV4z1egBk4noMGo90yh5JkNQuSGkvBMNzlc2cbWlnfbzayKT27LgCI5SCTyIkzko"; // пароль из ЛК Uniteller
$Format=1;
$sPostFields =
    "Shop_ID=" . $Shop_ID . "&Login=" . $Login . "&Password=" . $Password . "&Format=1&ShopOrderNumber=" .
    $Order_ID . "&S_FIELDS=Status;ApprovalCode;BillNumber";
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://wpay.uniteller.ru/results/");
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_TIMEOUT, 60);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $sPostFields);
curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
$curl_response = curl_exec($ch);
$curl_error = curl_error($ch);
$data = array(); // результат для возврата

if ($curl_error) {
// обработка ошибки обращения за статусом платежа
} else {
// данные получены
// обработка данных из переменной $curl_response
    $arr = explode(";", $curl_response);
    if (count($arr) > 2) {
        $data = array(
            "Status"
            => $arr[0]
        , "ApprovalCode" => $arr[1]
        , "BillNumber"
            => str_replace(' ','',$arr[2])
        );
    } else {
    }
}

return $data;
}

    function getAllActionProducts()
    {
        $actions = $this->getCurrentActions();
        $a = array();
        foreach ($actions as $action) {
            $_action = $this->getAction($action['id'], true);
            foreach ($_action['skidki'] as $skidka_id) {
                $skidka = $this->getSkidka($skidka_id);
                if ($skidka) {
                    $is_gift = false;
                    $i = 1;
                    foreach ($skidka['prods'] as $prod) {
                        if ($prod['is_if']) {
                            if ($i & 1 == 1) {

                                $prod['class'] = "gift_type3_kupon_gift";

                            } else {
                                $prod['class'] = "gift_type3_gift2";
                                if (!isset($skidka['prods'][$i + 2])) {
                                    $prod['class'] = "gift_type2_gift2";
                                }

                            }
                            $i++;
                            continue;
                        }
                        if ($prod['is_gift']) {
                            $is_gift = true;
                        } else {


                        }
                        $a[$prod['id']] = $skidka['new_price'];
                        if (!$prod['is_gift']) {
                            $this->getActionProductGifts($prod['id']);
                            //     $tpl->newBlock('buy');
                            //     $tpl->assign(array('prodId'=>$prod['id']));
                            //      $cat->showGift($prod['id'],1);
                        }
                    }
                    if ($skidka['type'] == 3 && $skidka['price']) {
                        //тут хз для купона новая цена $skidka['price']

                    }
                }

            }

        }
        return $a;
    }
	function getAllActionMainProducts()
    {
        $actions = $this->getCurrentActions();
        $a = array();
        foreach ($actions as $action) {
            $_action = $this->getAction($action['id'], true);
            foreach ($_action['skidki'] as $skidka_id) {
                $skidka = $this->getSkidka($skidka_id);
                if ($skidka) {
                    $is_gift = false;
                    $i = 1;
                    foreach ($skidka['prods'] as $prod) {

	                        $a[] = $prod['id'];

                    }
                }

            }

        }
        return $a;
    }

    function getAllActionKomplektProducts()
    {
        $actions = $this->getCurrentActions();
        $a = array();
        $k = 0;
        foreach ($actions as $action) {
            $_action = $this->getAction($action['id'], true);
            foreach ($_action['skidki'] as $skidka_id) {
                $skidka = $this->getSkidka($skidka_id);
                if ($skidka['type'] == 2) {
                    foreach ($skidka['prods'] as $prod) {

                        $a[$k][] = array($prod['id'] => $skidka['price']);
                    }
                    $k++;
                }

            }

        }
        return $a;
    }

    public function uniteller($order)
    {
        global $tpl;
        $amount = $order['total_sum'];
        $tpl->newBlock('block_uniteller');
        $Shop_IDP = "00007729"; // идентификатор точки продажи
        $Lifetime = 3600; // время жизни формы оплаты в секундах
        $Order_ID = $order['order_id'];
// Сумма для оплаты и идентификатор зарегистрированного пользователя могут
// храниться в данных сессии
        $Subtotal_P = $amount;
        $Customer_IDP = isset($_SESSION['siteuser']['id']) ? $_SESSION['siteuser']['id'] : "1";
// Параметры из справочников, описанных в Техническом порядке
        $MeanType = 0; // платежная система кредитной карты (0 - любая)
        $EMoneyType = 0; // тип электронной валюты (0 - любая)
// Адреса возврата после успешной и неуспешной оплат покупателями
        $URL_RETURN_OK = "http://suprashop.ru/shop?action=resultPayUniteller";
        $URL_RETURN_NO = "http://suprashop.ru/uniteller_error";

        $password = "GuLfR2liwdJbxGfUqV4z1egBk4noMGo90yh5JkNQuSGkvBMNzlc2cbWlnfbzayKT27LgCI5SCTyIkzko"; // пароль из ЛК Uniteller
// Подпись для формы, вместо неиспользуемых параметров передаются пустые строки
        $Signature = $this->getSignature($Shop_IDP, $Order_ID, $Subtotal_P, $MeanType
            , $EMoneyType, $Lifetime, $Customer_IDP, ""
            , "", "", $password);
        $tpl->assign(
            array(
                'Shop_IDP' => $Shop_IDP,
                'Lifetime' => $Lifetime,
                'Order_ID' => $Order_ID,
                'Subtotal_P' => $Subtotal_P,
                'MeanType' => $MeanType,
                'EMoneyType' => $EMoneyType,
                'URL_RETURN_OK' => $URL_RETURN_OK,
                'URL_RETURN_NO' => $URL_RETURN_NO,
                'password' => $password,
                'Signature' => $Signature,
                'Customer_IDP' => $Customer_IDP,
            )
        );
    }
    
    public function getDiscountC($kod, $category, $price){
	    global $db;

	    $db->query('SELECT	percent 
						FROM ' . $this->table_prefix . '_catalog_discount
						WHERE categoryId = ' . $category.' AND kod = "'.$kod.'"');
            if ($db->next_record()) {
	          
	            $res = $price * ($db->f('percent')/100);
                return $res;
            }
            return 0;
    }

}

?>