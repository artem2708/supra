<?php // Vers 5.8.2 27.06.2012

/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
			  'shop_order_by'			=> array('type'		=> 'select',
												 'defval'	=> array('id DESC'	=> array(	'rus' => 'в порядке обратном добавлению',
												 											'eng' => 'reverce adding order'),
												 					 'id'		=> array(	'rus' => 'в порядке добавления',
												 											'eng' => 'adding order'),
												 					 ),
												 'descr'	=> array(	'rus' => 'Порядок вывода записей',
												 						'eng' => 'Show records order'),
												 'access'	=> 'editable',
												 ),

			   'shop_max_rows'			=> array('type'		=> 'integer',
			   									 'defval'	=> '20',
			   									 'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу',
												 						'eng' => 'Maximum quantity of records on page'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_order_concurency' 	=> array('type'		=> 'select',
			   									 'defval'	=> array('RUR'	=> array('rus' => 'Руб',
												 									'eng' => 'Rur'),
			   									 					 'USD'			=> array('rus' => 'Usd',
												 											'eng' => 'Usd'),
			   									 					 'EUR'			=> array('rus' => 'Eur',
												 											'eng' => 'Eur'),
			   									 					 ),
			   									 'descr'	=> array(	'rus' => 'Тип валюты для выписки счёта',
												 						'eng' => 'Currency type for invoice'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_date_format' 		=> array('type'		=> 'select',
			   									 'defval'	=> array('%d.%m.%Y %H:%i'	=> array(	'rus' => 'День.Месяц.Год Часы:Минуты',
												 										  			'eng' => 'Day.Month.Year Hours:Minutes'),
			   									 					 '%d.%m.%Y'			=> array(	'rus' => 'День.Месяц.Год',
												 													'eng' => 'Day.Month.Year'),
			   									 					 '%Y.%m.%d'			=> array(	'rus' => 'Год.Месяц.День',
												 													'eng' => 'Year.Month.Day'),
			   									 					 '%Y.%m.%d %H:%i'	=> array(	'rus' => 'Год.Месяц.День Часы:Минуты',
												 													'eng' => 'Year.Month.Day Hours:Minutes'),
			   									 					 ),
												 'descr'	=> array(	'rus' => 'Формат выводимой даты',
												 						'eng' => 'Format of deduced date'),
			   									 'access'	=> 'readonly'
			   									 ),

			   'shop_order_recipient' 	=> array('type'		=> 'string',
			   									 'defval'	=> 'shop@cms.ru',
			   									 'descr'	=> array(	'rus' => 'E-mail для отправки сообщения о новом заказе',
												 						'eng' => 'E-mail for sending the message on the new order'),
			   									 'access'	=> 'editable',
			   									 'multisite' => true
			   									 ),

			   'shop_company' 			=> array('type'		=> 'string',
			   									 'defval'	=> '',
			   									 'descr'	=> array(	'rus' => 'Организация',
												 						'eng' => 'Organization'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_inn' 			=> array('type'		=> 'string',
			   									 'defval'	=> '',
			   									 'descr'	=> array(	'rus' => 'ИНН',
												 						'eng' => 'INN'),
			   									 'access'	=> 'editable'
			   									 ),


			   'shop_kpp'	 			=> array('type'		=> 'string',
			   									 'defval'	=> '',
			   									 'descr'	=> array(	'rus' => 'КПП',
												 						'eng' => 'KPP'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_bank'	 			=> array('type'		=> 'string',
			   									 'defval'	=> '',
			   									 'descr'	=> array(	'rus' => 'Банк',
												 						'eng' => 'Bank'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_sett_account'		=> array('type'		=> 'string',
			   									 'defval'	=> '',
			   									 'descr'	=> array(	'rus' => 'Р. счет',
												 						'eng' => 'Settlement account'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_corr_account'		=> array('type'		=> 'string',
			   									 'defval'	=> '',
			   									 'descr'	=> array(	'rus' => 'Кор. счет',
												 						'eng' => 'Loro account'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_bik'				=> array('type'		=> 'string',
			   									 'defval'	=> '',
			   									 'descr'	=> array(	'rus' => 'БИК',
												 						'eng' => 'BIK'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_generate_XML'		=> array('type'		=> 'checkbox',
			   									 'defval'	=> '',
			   									 'descr'	=> array(	'rus' => 'Создавать XML выгрузки при добавлении заказа',
												 						'eng' => 'To create XML unloadings at addition of the order'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_nds'    			=> array('type'		=> 'integer',
			   									 'defval'	=> '18',
			   									 'descr'	=> array(	'rus' => ' НДС %',
												 						'eng' => ' VAT %'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_pay_type_cash'		=> array('type'		=> 'checkbox',
			   									 'defval'	=> '',
			   									 'descr'	=> array(	'rus' => 'Включить способ оплаты - Наличный расчет',
												 						'eng' => 'To include a way of payment - Cash calculation on deliveryr'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_pay_type_sbr'		=> array('type'		=> 'checkbox',
			   									 'defval'	=> '',
			   									 'descr'	=> array(	'rus' => 'Включить способ оплаты - Квитанция Сбербанк',
												 						'eng' => 'To include a way of payment - the Receipt the Sberbank'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_pay_type_non_cash'		=> array('type'		=> 'checkbox',
			   									 'defval'	=> '',
			   									 'descr'	=> array(	'rus' => 'Включить способ оплаты - Банковский перевод (безналичный счет)',
												 						'eng' => 'To include a way of payment - Bank remittance'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_WM_activ'		=> array('type'		=> 'checkbox',
			   									 'defval'	=> '',
			   									 'descr'	=> array(	'rus' => 'Включить способ оплаты в платежной системе Web Money ',
												 						'eng' => 'To include a way of payment - Web Money'),
			   									 'access'	=> 'editable'
			   									 ),

			   'shop_assist_activ'		=> array('type'		=> 'checkbox',
			   									 'defval'	=> '',
			   									 'descr'	=> array(	'rus' => 'Включить способ оплаты через систему электронных платежей Assist )',
												 						'eng' => 'To include a way of payment - Assist'),
			   									 'access'	=> 'editable'
			   									 ),
              'shop_chronopay_activ'		=> array('type'		=> 'checkbox',
                    'defval'	=> '',
                    'descr'	=> array(	'rus' => 'Включить способ оплаты через систему электронных платежей Chronopay )',
                        'eng' => 'To include a way of payment - Chronopay'),
                    'access'	=> 'editable'
                                                 ),
                'shop_chronopay_product_id'		=> array('type'		=> 'string',
                    'defval'	=> '007232-0001-0001',
                    'descr'	=> array(	'rus' => 'Chronopay продукт',
                        'eng' => 'Chronopay item'),
                    'access'	=> 'editable'
                ),

                'shop_chronopay_sharedSec'		=> array('type'		=> 'string',
                    'defval'	=> 'dfhdjf874gkjdhf84bfkjs',
                    'descr'	=> array(	'rus' => 'Chronopay секретный ключ',
                        'eng' => 'Chronopay key'),
                    'access'	=> 'editable'
                ),

                'shop_bonus_cost'		=> array('type'		=> 'string',
                    'defval'	=> '1',
                    'descr'	=> array(	'rus' => 'Стоимость обного бонусного балла',
                        'eng' => 'Bonus cost'),
                    'access'	=> 'editable'
                ),

);


$OPEN_NEW = array (
	'order_pay_types' =>
		array(
			'1' => 'наличный расчет',
			'2' => 'квитанция на оплату',
		/*	'3' => 'банковский перевод (безналичный счет)',
			'4' => 'оплата в платежной системе Web Money',
			'5' => 'оплата через систему электронных платежей Assist',*/
			'6' => 'оплата через систему электронных платежей ChronoPay',
			'7' => 'оплата банковской картой'		),
	'order_default_pay_type' => 1,
	'shop_WM_result_url'	=> '/mod/shop/wm_result.php',
	'shop_assist_result_url'=> '/mod/shop/assist_result.php',
	'shop_export_path'		=> 'admin/exchange/'.SITE_PREFIX.'/out/',		// путь для выгрузки данных
	'shop_import_path'		=> 'admin/exchange/'.SITE_PREFIX.'/in/',		// путь для загрузки данных

);

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'shop_order_by' => 'id DESC',
'shop_max_rows' => '100',
'shop_order_concurency' => 'RUR',
'shop_date_format' => '%Y.%m.%d %H:%i',
'shop_order_recipient' => array(
	'sup' => 'shop@supra.ru',	
	),
'shop_company' => 'ИП Полубнев Алексей Андреевич',
'shop_inn' => '772602732545',
'shop_kpp' => '',
'shop_bank' => 'ПАО &quot;Промсвязьбанк&quot; г.Москва',
'shop_sett_account' => '40802810900000005668',
'shop_corr_account' => '30101810400000000555',
'shop_bik' => '044525555',
'shop_generate_XML' => 'on',
'shop_nds' => '80',
'shop_pay_type_cash' => '',
'shop_pay_type_sbr' => '',
'shop_pay_type_non_cash' => '',
'shop_WM_activ' => '',
'shop_assist_activ' => '',
'shop_chronopay_activ' => 'on',
'shop_chronopay_product_id' => '007232-0001-0001',
'shop_chronopay_sharedSec' => 'dfhdjf874gkjdhf84bfkjs',
'shop_bonus_cost' => '1',
/* ABOCMS:END */
);
?>
