<?php

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_shop_cities` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `group_id` int(11) unsigned NOT NULL default '0',
  `city` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_shop_countries` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `country` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_shop_discounts` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `value` DECIMAL(16, 2) UNSIGNED DEFAULT 0 NOT NULL,
  `min` DECIMAL(16, 2) UNSIGNED DEFAULT 0 NOT NULL,
  `max` DECIMAL(16, 2) UNSIGNED DEFAULT 0 NOT NULL,
  `from_date` DATE,
  `to_date` DATE,
  `is_percent` tinyint(1) NOT NULL default '1',
  `active` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_shop_order_history` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `order_id` int(5) unsigned NOT NULL default '0',
  `manager_id` int unsigned default 0,
  `old_status` tinyint(4) NOT NULL default '0',
  `new_status` tinyint(4) NOT NULL default '0',
  `date` DATETIME NOT NULL,
  `info` text,
  PRIMARY KEY  (`id`),
  INDEX (`order_id`, `manager_id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_shop_order_items` (
  `id` int(5) unsigned NOT NULL auto_increment,
  `order_id` int(5) unsigned default NULL,
  `item_id` int(5) unsigned default NULL,
  `item_name` varchar(255) default NULL,
  `item_code` varchar(100) default NULL,
  `item_price` DECIMAL( 16, 2 ) UNSIGNED DEFAULT 0 NOT NULL,
  `item_currency` varchar(10) NOT NULL default 'RUR',
  `item_rate` varchar(20) default NULL,
  `item_qty` int(11) unsigned default NULL,
  `item_cost` DECIMAL( 16, 2 ) UNSIGNED DEFAULT 0 NOT NULL,
  `info` text NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_shop_orders` (
  `id` int(5) unsigned NOT NULL auto_increment,
  `siteuser_id` int(11) default NULL,
  `delivery_id` int unsigned default 0,
  `manager_id` int unsigned default 0,
  `status` tinyint unsigned NOT NULL default 0,
  `delivery_cost` DECIMAL(10,2) unsigned NOT NULL default 0,
  `customer` varchar(100) default NULL,
  `type` varchar(255) NOT NULL default '',
  `address` text,
  `phone` varchar(100) default NULL,
  `email` varchar(100) default NULL,
  `organization` varchar(255) default NULL,
  `jpinfo` text,
  `date` DATETIME NOT NULL,
  `sum` DECIMAL(16, 2) UNSIGNED DEFAULT 0 NOT NULL,
  `discount` DECIMAL(16, 2) UNSIGNED DEFAULT 0 NOT NULL,
  `total_sum` DECIMAL(16, 2) UNSIGNED DEFAULT 0 NOT NULL,
  `currency` varchar(10) NOT NULL default 'RUR',
  `addinfo` text,
  `pay_type` tinyint(4) unsigned NOT NULL default '1',
  `status_dnld` tinyint(1) NOT NULL default '0',
  `session` VARCHAR(33) NOT NULL default '',
  `notes` text NOT NULL default '',
  `notify` tinyint(1) NOT NULL default 0,
  PRIMARY KEY  (`id`),
  INDEX (`siteuser_id`, `delivery_id`, `manager_id`, `status`)
) ENGINE=MyISAM";


$MOD_TABLES[] = "CREATE TABLE `".$prefix."_shop_taxes` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `tax` varchar(255) NOT NULL default '',
  `percent` tinyint(4) NOT NULL default '0',
  `rank` tinyint(4) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_shop_users_discounts` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `active` tinyint(2) NOT NULL default '0',
  `min` decimal(16,2) NOT NULL default '0.00',
  `max` decimal(16,2) NOT NULL default '0.00',
  `value` decimal(16,2) NOT NULL default '0.00',
  `is_percent` tinyint(2) NOT NULL default '0',
  `interval` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_shop_users_discounts_groups` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `users_group_id` int(10) unsigned NOT NULL default '0',
  `users_discound_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_shop_delivery` (
    `id` int unsigned NOT NULL auto_increment,
    `title` varchar(255) NOT NULL default '',
    `dscr` TEXT NOT NULL default '',
    `cost` DECIMAL(10,2) unsigned NOT NULL default 0,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_shop_order_status` (
    `id` tinyint unsigned NOT NULL auto_increment,
    `title` varchar(255) NOT NULL default '',
    `payable` tinyint(1) unsigned NOT NULL default 0,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM";
?>