<?php
$sql = array(
    'ALTER TABLE `{site_prefix}_shop_order_items` ADD `info` TEXT NOT NULL default ""',
    'ALTER TABLE `{site_prefix}_shop_discounts` ADD `to_date` DATE AFTER max',
    'ALTER TABLE `{site_prefix}_shop_discounts` ADD `from_date` DATE AFTER max',
    "CREATE TABLE `{site_prefix}_shop_delivery` (
    `id` int unsigned NOT NULL auto_increment,
    `title` varchar(255) NOT NULL default '',
    `dscr` TEXT NOT NULL default '',
    `cost` DECIMAL(10,2) unsigned NOT NULL default 0,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM",
    'ALTER TABLE `{site_prefix}_shop_orders` ADD `delivery_cost` DECIMAL(10,2) unsigned NOT NULL default 0 AFTER siteuser_id',
    'ALTER TABLE `{site_prefix}_shop_orders` ADD `delivery_id` int unsigned default 0 AFTER siteuser_id',
    'ALTER TABLE `{site_prefix}_shop_orders` ADD INDEX (`siteuser_id`, `delivery_id`, `status`)',
    "CREATE TABLE `{site_prefix}_shop_order_status` (
    `id` tinyint unsigned NOT NULL auto_increment,
    `title` varchar(255) NOT NULL default '',
    `payable` tinyint(1) unsigned NOT NULL default 0,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM",
    'ALTER TABLE `{site_prefix}_shop_orders` CHANGE `status` `status` TINYINT unsigned DEFAULT 0',
    'ALTER TABLE `{site_prefix}_shop_discounts` CHANGE `min` `min` DECIMAL( 16, 2 ) UNSIGNED DEFAULT 0 NOT NULL, CHANGE `max` `max` DECIMAL( 16, 2 ) DEFAULT 0 NOT NULL, CHANGE `value` `value` DECIMAL( 16, 2 ) DEFAULT 0 NOT NULL',
    'ALTER TABLE `{site_prefix}_shop_order_items` CHANGE `item_price` `item_price` DECIMAL( 16, 2 ) UNSIGNED DEFAULT 0 NOT NULL, CHANGE `item_cost` `item_cost` DECIMAL( 16, 2 ) UNSIGNED DEFAULT 0 NOT NULL ',
    'ALTER TABLE `{site_prefix}_shop_orders` CHANGE `sum` `sum` DECIMAL( 16, 2 ) UNSIGNED DEFAULT 0 NOT NULL, CHANGE `discount` `discount` DECIMAL( 16, 2 ) UNSIGNED DEFAULT 0 NOT NULL, CHANGE `total_sum` `total_sum` DECIMAL( 16, 2 ) UNSIGNED DEFAULT 0 NOT NULL ',
    'UPDATE {site_prefix}_shop_discounts SET min=min/100',
    'UPDATE {site_prefix}_shop_discounts SET max=max/100',
    'UPDATE {site_prefix}_shop_discounts SET value=value/100',
    'UPDATE {site_prefix}_shop_order_items SET item_price=item_price/100',
    'UPDATE {site_prefix}_shop_order_items SET item_cost=item_cost/100',
    'UPDATE {site_prefix}_shop_orders SET sum=sum/100',
    'UPDATE {site_prefix}_shop_orders SET discount=discount/100',
    'UPDATE {site_prefix}_shop_orders SET total_sum=total_sum/100',
    'ALTER TABLE `{site_prefix}_shop_users_discounts` CHANGE `min` `min` DECIMAL( 16, 2 ) DEFAULT 0, CHANGE `max` `max` DECIMAL( 16, 2 ) DEFAULT 0, CHANGE `value` `value` DECIMAL( 16, 2 ) DEFAULT 0',
    'ALTER TABLE `{site_prefix}_shop_order_history` ADD `manager_id` INT UNSIGNED DEFAULT 0 NOT NULL AFTER `order_id`',
    'ALTER TABLE `{site_prefix}_shop_orders` ADD `manager_id` INT UNSIGNED DEFAULT 0 NOT NULL AFTER `delivery_id`',
    'ALTER TABLE `{site_prefix}_shop_orders` ADD `notes` TEXT NOT NULL default ""'
);
?>