<?
require_once('../../common.php');
require_once('lib/class.Shop.php');
require_once('lib/nusoap.php');

function utf8win1251($s)
{
    $out = $c1 = "";
    $byte2 = false;
    for ($c=0; $c<strlen($s); $c++) {
        $i = ord($s[$c]);
        if ($i<=127 && !$byte2) {
            $out .= $s[$c];
            continue;
        }
        if ($byte2) {
            $new_c2 = ($c1&3)*64+($i&63);
            $new_c1 = ($c1>>2)&5;
            $new_i = $new_c1*256+$new_c2;
            if (1025 == $new_i) {
                $out_i = 168;
            } elseif (1105 == $new_i) {
                $out_i = 184;
            } else {
                $out_i = $new_i-848;
            }
            if ($out_i>255) {
                $out_i = $out_i-83;
            }
            $out .= chr($out_i);
            $byte2 = false;
        } elseif (($i>>5)==6) {
            $c1 = $i;
            $byte2 = true;
        }
   }
 
   return $out;
}

function check_signature($sgn)
{
	return TRUE;
}

function get_orders()
{   global $db, $server, $lang;

	$query = "SELECT * FROM ".$server.$lang."_shop_orders";
	$query .= " WHERE `date` > DATE_SUB( NOW( ) , INTERVAL 3 DAY) AND status < 4 AND pay_type = 5";
	$db->query($query);
	if (!$db->nf()) {
		return FALSE;
	}
	$ret = array();
	while ($db->next_record()) {
		$ret[$db->f('id')] = array(
		    'total_sum' => Main::process_price($db->f('total_sum')),
		    'currency' => $db->f('currency')
		);
	}
	
	return $ret;
}

$main->get_rates();

$shop = new Shop('no_action');		

$test_mode = $CONFIG['shop_assist_activ'] ? 0 : 1; /** �����: 0 - ��������; 1 - ��������. **/

$glue_err = FALSE === strpos($CONFIG['shop_assist_fail_url'], "?") ? "?" : "&";
$glue_ok = FALSE === strpos($CONFIG['shop_assist_success_url'], "?") ? "?" : "&";

if (!$orders = get_orders()) {
    header("Location: ".$CONFIG['shop_assist_fail_url'].$glue_err."err=no_orders");
	exit;
}
$start_date = time() - 60*60*24*2;
$end_date = time() + 60*60*24;
$param = array('shop_id' => $CONFIG['shop_assist_idp'],
               'shopordernumber' => '%',
               'login' => $CONFIG['shop_assist_login'],
               'password' => $CONFIG['shop_assist_password'],
               'meantype' => '0', /* ��������, � ����� ��������� ��������� �������� � �����
                                     0 - �����, 1 - VISA, 2 - EC/MC, 3 - Diners Club, 6 - STB Card 
                                   */
               'paymenttype' => '0',
               'success' => '1', /* 1 - �������� ��������, 0 - ����������, 2 - ��� */
               'startday' => date("j", $start_date),
               'startmonth' => date("n", $start_date),
               'startyear' => date("Y", $start_date),
               'endday' => date("j", $end_date),
               'endmonth' => date("n", $end_date),
               'endyear' => date("Y", $end_date),
               'english' =>'1'
              );
$client = new soapclient('https://secure.assist.ru/results/results.cfm?format=5');

$client->decode_utf8 = false;
$client->soap_defencoding = 'UTF-8';
$client->xml_encoding = 'UTF-8';
$client->http_encoding = false;

$err = $client->getError();
if ($err) {
    header("Location: ".$CONFIG['shop_assist_fail_url'].$glue_err."err=soap_init");
    exit;
}

$result = $client->call('GetPaymentsResult',             // ��� ������
                        $param,                          // ������� ���������
                        'http://secure.assist.ru/results/results.wsdl', // ������������ ����  http://www.assist.ru/
                        ''                               // SOAPAction http://www.assist.ru/
);

#-----------------------------------
if ($client->fault) { 
    header("Location: ".$CONFIG['shop_assist_fail_url'].$glue_err."err=soap_call");
    exit;
} else {
    $err = $client->getError();
    if ($err || !is_array($result) || !count($result)) {
        header("Location: ".$CONFIG['shop_assist_fail_url'].$glue_err."err=soap_result");
        exit;
    } else {
        if (!$test_mode) {
            foreach ($result as $k=>$payment) {
                /* ����� ������ ASSIST � ������ �������� */
                $assist_total = Shop::exchange($payment['total'], $payment['currency'], $CONFIG['shop_order_concurency']);
                /* ����� ������ � ��������(� ������ ��������) */
                $shop_total = array_key_exists($payment['ordernumber'], $orders)
                    ? Shop::exchange($orders[$payment['ordernumber']]['total_sum'],
                                     $orders[$payment['ordernumber']]['currency'],
                                     $CONFIG['shop_order_concurency'])
                    : 0;
                if (check_signature($payment['signature']) /* ������� ����� */
                    && "Fake" != $payment['processingname'] /* �������������� ����� �� ���������� */
                    && $shop_total  
                    && $shop_total == $assist_total /* ��������� ����� ������� ASSIST � �������� */
                    ) {
                    $status_info = '';
                    foreach ($payment as $key => $val) {
                      	$status_info .= $key." - ".utf8win1251($val)."\n";
                    }  
                	$shop->setOrderStatus($payment['ordernumber'], 4, $status_info);
                }
            }
            header("Location: ".$CONFIG['shop_assist_success_url'].$glue_ok."ok");
            exit;
        }
    }
}
?>