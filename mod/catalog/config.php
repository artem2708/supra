<?php // Vers 5.8.2 27.06.2012
/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
			  'catalog_max_rows'		=> array('type'		=> 'integer',
												 'defval'	=> '10',
			   									 'descr'	=> array(	'rus' => 'максимальное количество записей на страницу',
												 						'eng' => 'maximum quantity of records on page'),
												 'access'	=> 'editable',
												 ),

			   'catalog_ctg_order_by'	=> array('type'		=> 'select',
			   									 'defval'	=> array('rank'			=> array(	'rus' => 'По возрастанию ранга',
												 												'eng' => 'On increase of a rank'),
			   									 					 'rank DESC'	=> array(	'rus' => 'По убыванию ранга',
												 												'eng' => 'On decrease of a rank'),
			   									 					 'title'		=> array(	'rus' => 'По названию категории А-Я',
												 												'eng' => 'On category name A-Z'),
			   									 					 'title DESC'	=> array(	'rus' => 'По названию категории Я-А',
												 												'eng' => 'On category name Z-A'),
			   									 ),
			   									 'descr'	=> array(	'rus' => 'Порядок вывода категорий',
						 												'eng' => 'Order of categories displaying'),
												 'access'	=> 'editable'
			   									 ),

			   'catalog_prod_order_by' 	=> array('type'		=> 'select',
			   									 'defval'	=> array('ptype_id, rank'		=> array(	'rus' => 'По производителю, по возрастанию ранга',
														 												'eng' => 'On manufacturer, on increase of a rank'),
			   									 					 'ptype_id, rank DESC'	=> array(	'rus' => 'По производителю, по убыванию ранга',
														 												'eng' => 'On manufacturer, on decrease of a rank'),
			   									 					 'rank, ptype_id'		=> array(	'rus' => 'По возрастанию ранга, производителю',
														 												'eng' => 'On increase of a rank, on manufacturer'),
			   									 					 'rank DESC, ptype_id'	=> array(	'rus' => 'По убыванию ранга, производителю',
														 												'eng' => 'On decrease of a rank, on manufacturer'),
														 				 'product_title'	=> array(	'rus' => 'По названию А-Я',
														 												'eng' => 'По названию А-Я'),
														 				 'product_title DESC'	=> array(	'rus' => 'По названию Я-А',
														 												'eng' => 'По названию Я-А'),
			   									 ),
			   									 'descr'	=> array(	'rus' => 'Порядок вывода товаров по умолчанию',
						 												'eng' => 'Default order of products displaying'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_concrn_order_by' 	=> array('type'		=> 'select',
			   									 'defval'	=> array('rank'		=> array(	'rus' => 'По возрастанию ранга',
														 												'eng' => 'On increase of a rank'),
			   									 					 'rank DESC'	=> array(	'rus' => 'По убыванию ранга',
														 												'eng' => 'On decrease of a rank'),
			   									 					 'product_title'		=> array(	'rus' => 'По названию',
														 												'eng' => 'On increase of a title'),
			   									 					 'product_title DESC'	=> array(	'rus' => 'По убыванию названия',
														 												'eng' => 'On decrease of a title'),
			   									 					 'ptype_name'		=> array(	'rus' => 'По производителю',
														 												'eng' => 'On manufacturer'),
			   									 					 'ptype_name DESC'	=> array(	'rus' => 'По производителю, по убыванию ранга',
														 												'eng' => 'On decrease of a manufacturer'),
			   									 ),
			   									 'descr'	=> array(	'rus' => 'Порядок вывода связанных товаров на сайте',
						 												'eng' => 'Default order of concerned products displaying'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_ptype_order_by' 	=> array('type'		=> 'select',
			   									 'defval'	=> array('ptype_name'		=> array(	'rus' => 'По названию А-Я',
														 												'eng' => 'On increase of a title'),
			   									 					 'ptype_name DESC'	=> array(	'rus' => 'По названия Я-А',
														 												'eng' => 'On decrease of a title'),
			   									 ),
			   									 'descr'	=> array(	'rus' => 'Порядок вывода производителей',
						 												'eng' => 'Output order manufacturers'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_group_order_by' 	=> array('type'		=> 'select',
			   									 'defval'	=> array('group_title'		=> array(	'rus' => 'По названию А-Я',
														 												'eng' => 'On increase of a title'),
			   									 					 'group_title DESC'	=> array(	'rus' => 'По названия Я-А',
														 												'eng' => 'On decrease of a title'),
			   									 ),
			   									 'descr'	=> array(	'rus' => 'Порядок вывода групп',
						 												'eng' => 'Output order group'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_curr_on_site' 	=> array('type'		=> 'select',
			   									 'defval'	=> array(
			   									     'no' => array(
			   									         'rus' => 'не выводить',
			   									         'eng' => 'dont show'),
			   									     'usd' => array(
			   									         'rus' => 'USD',
			   									         'eng' => 'USD'),
			   									     'eur' => array(
			   									         'rus' => 'EUR',
			   									         'eng' => 'EUR'),
			   									     'usdeur' => array(
			   									         'rus' => 'USD + EUR',
			   									         'eng' => 'USD + EUR')
			   									 ),
			   									 'descr'	=> array(	'rus' => 'Курс валюты на сайте',
						 												'eng' => 'Currency on site'),
			   									 'access'	=> 'editable'
			   									 ),
			  'catalog_empty_prp'		=> array('type'		=> 'string',
												 'defval'	=> '&nbsp;',
												 'descr'	=> array(	'rus' => 'Значение выводимое на месте пустого свойства при сравнении товаров',
						 												'eng' => 'Value displaying on a place of empty property at comparison of the goods'),
												 'access'	=> 'editable',
												 ),
			   'catalog_count_hits' 	    => array('type'		=> 'checkbox',
			   									 'defval'	=> 'on',
			   									 'descr'	=> array(	'rus' => 'Учитывать статистику отображения контента',
						 												'eng' => 'to consider statistics of display of a content'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_comment_cnt'		=> array('type'		=> 'integer',
			   									 'defval'	=> '20',
			   									 'descr'	=> array(	'rus' => 'Кол-во комментариев на страницу',
												 						'eng' => 'Comments count on page'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_max_img_width' 	    => array('type'		=> 'integer',
			   									 'defval'	=> '0',
			   									 'descr'	=> array(	'rus' => 'Макс. ширина большого изображения',
						 												'eng' => 'Max height of big image'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_max_img_height' 	    => array('type'		=> 'integer',
			   									 'defval'	=> '0',
			   									 'descr'	=> array(	'rus' => 'Макс. высота большого изображения',
						 												'eng' => 'Min height of big imag'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_max_prev_width' 	    => array('type'		=> 'integer',
			   									 'defval'	=> '0',
			   									 'descr'	=> array(	'rus' => 'Макс. ширина маленького изображения',
						 												'eng' => 'Max width of big imag'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_max_prev_height' 	    => array('type'		=> 'integer',
			   									 'defval'	=> '0',
			   									 'descr'	=> array(	'rus' => 'Макс. высота маленького изображения',
						 												'eng' => 'Min width of big imag'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_add_img_cnt' 	    => array('type'		=> 'integer',
			   									 'defval'	=> '10',
			   									 'descr'	=> array(	'rus' => 'Макс. кол-во дополнительных изображений',
						 												'eng' => 'Max additional images count'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_default_price' 	    => array('type'		=> 'string',
			   									 'defval'	=> '0',
			   									 'descr'	=> array(	'rus' => 'Значение цены по умолчанию',
						 												'eng' => 'Default price'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_show_price_1' 	    => array('type'		=> 'checkbox',
			   									 'defval'	=> 'on',
			   									 'descr'	=> array(	'rus' => 'Отображать розничную цену в карточке товара',
						 												'eng' => 'Show price 1'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_show_price_2' 	    => array('type'		=> 'checkbox',
			   									 'defval'	=> 'on',
			   									 'descr'	=> array(	'rus' => 'Отображать партнёрскую цену в карточке товара',
						 												'eng' => 'Show price 2'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_show_price_3' 	    => array('type'		=> 'checkbox',
			   									 'defval'	=> 'on',
			   									 'descr'	=> array(	'rus' => 'Отображать дилерскую цену в карточке товара',
						 												'eng' => 'Show price 3'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_show_price_4' 	    => array('type'		=> 'checkbox',
			   									 'defval'	=> 'on',
			   									 'descr'	=> array(	'rus' => 'Отображать старую цену в карточке товара',
						 												'eng' => 'Show price 4'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_show_price_5' 	    => array('type'		=> 'checkbox',
			   									 'defval'	=> 'on',
			   									 'descr'	=> array(	'rus' => 'Отображать дилерскую 3 цену в карточке товара',
						 												'eng' => 'Show price 5'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_default_rights'		=> array('type'		=> 'string',
			   									 'defval'	=> '111111110001',
			   									 'descr'	=> array(	'rus' => 'Права для каталога по-умолчанию (формат <img src="/i/admin/rights_format.gif" width=72 height=18 alt="формат прав">, где x - 0 или 1, каждая группа представляет набор прав: удаление, публикация, редактирование, чтение)',
 												 						'eng' => 'Catalog default rights (format <img src="/i/admin/rights_format.gif" width=72 height=18 alt="rights format">, where x - 0 or 1, every group is rights set: delete, publication, edit, read)'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_yml_name'		=> array('type'		=> 'string',
			   									 'defval'	=> 'YML - Название магазина',
			   									 'descr'	=> array(	'rus' => 'YML - Название магазина',
 												 						'eng' => 'YML - Shop title'),
			   									 'access'	=> 'editable'
			   									 ),
			   'catalog_yml_company'		=> array('type'		=> 'string',
			   									 'defval'	=> 'YML - Название компании',
			   									 'descr'	=> array(	'rus' => 'YML - Название компании',
 												 						'eng' => 'YML - Company title'),
			   									 'access'	=> 'editable'
			   									 ),
			 'catalog_file_alarm'		=> array('type'		=> 'text',
											 'defval'	=> 'Предупреждающий текст для скачивания файлов!',
											 'descr'	=> array(	'rus' => 'Предупреждающий текст для скачивания файлов',
											 						'eng' => 'Предупреждающий текст для скачивания файлов'),
											 'access'	=> 'editable'
											 )
			   );

// Настройки модуля доступные для изменения посредством административного интерфейса
$OPEN_NEW = array (
	'catalog_currencies' => array('RUR' => 'руб.',
								  'USD' => '$',
								  'EUR' => '&euro;',
								 ),
	'catalog_yml_curr' => 'RUR',
	'catalog_default_curr' => 'RUR',
								 	// Тип динамического свойства
	// ключи должны быть строго челочисленными,
	'catalog_dynamic_property_type'   => array(0 => array('property_id'		=> 0,
    													  'property_name'	=> '-- Выберите тип динамического свойства --'),
											   1 => array('property_id'		=> 1,
											   			  'property_name'	=> 'Однострочное текстовое поле'),
											   2 => array('property_id'		=> 2,
											   			  'property_name'	=> 'Выпадающий список'),
											   3 => array('property_id'		=> 3,
											   			  'property_name'	=> 'Свойства заказа'),
											   4 => array('property_id'		=> 4,
											   			  'property_name'	=> 'Разделитель')),
	'catalog_max_level'			=> 10,						// максимальная допустимая вложенность в каталоге
															// (начиная с нуля): на первый уровень нельзя добавлять
															// товары, на последний - подкатегории

	'catalog_img_path'			=> $CONFIG['files_upload_path'].'catalog/{SITE_PREFIX}/',				// картинки для товаров
	'catalog_ctg_img_path'		=> $CONFIG['files_upload_path'].'catalog/{SITE_PREFIX}/categories/',	// картинки для категорий
	'catalog_add_img_path'     	=> $CONFIG['files_upload_path'].'catalog/{SITE_PREFIX}/img/',
	'catalog_add_prev_path'    	=> $CONFIG['files_upload_path'].'catalog/{SITE_PREFIX}/prev/',
	
	'catalog_def_img_path'     	=> '/i/no_photo.gif',
	'catalog_def_prev_path'    	=> '/i/no_photo.gif',
																									// для товаров
	'catalog_export_path'		=> 'admin/exchange/{SITE_PREFIX}/out/',		// путь для выгрузки данных
	'catalog_import_path'		=> 'admin/exchange/{SITE_PREFIX}/in/',		// путь для загрузки данных
	'catalog_export_yml_path'	=> 'admin/yml/',		// путь для выгрузки данных YandexML

	'catalog_upload_csv_path'		=> 'admin/files/', 						// Место для хранения закачанных на сервер CSV файлов
	'catalog_csv_max_size'			=> 0,									// Максимальный размер закачиваемого CSV файла в байтах, 0 - не ограничен
	'catalog_upload_yml_path'		=> 'files/catalog/{SITE_PREFIX}/yml/',		// Место хранения закачанного на сервер yml файла

	'catalog_upload_rates_file'		=> 'admin/files/catalog_rates.txt',		// файл настроек курсов валют
	'catalog_csv_separator'			=> ';',									// разделитель полей в CSV файле

	'catalog_product_transfer_fields' => array(
								array('name' => 'id',			'descr' => 'id товара',						'type' => 'integer'),
								array('name' => 'categ_id',		'descr' => 'id категории',					'type' => 'integer'),
								array('name' => 'group_id',		'descr' => 'id группы',						'type' => 'integer'),
								array('name' => 'ptype_id',		'descr' => 'id производителя',				'type' => 'integer'),
								array('name' => 'title',		'descr' => 'заголовок продукта',			'type' => 'string'),
								array('name' => 'description',	'descr' => 'описание продукта',				'type' => 'text'),
								array('name' => 'image_middle',	'descr' => 'среднее изображение продукта',	'type' => 'string'),
								array('name' => 'availability',	'descr' => 'наличие',						'type' => 'bit'),
								array('name' => 'rank',			'descr' => 'ранг товара',					'type' => 'integer'),
							)
								 );

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'catalog_max_rows' => '2000',
'catalog_ctg_order_by' => 'rank',
'catalog_prod_order_by' => 'rank, ptype_id',
'catalog_concrn_order_by' => 'rank',
'catalog_ptype_order_by' => 'ptype_name DESC',
'catalog_group_order_by' => 'group_title DESC',
'catalog_curr_on_site' => 'usd',
'catalog_empty_prp' => '-/-',
'catalog_count_hits' => 'on',
'catalog_comment_cnt' => '5',
'catalog_max_img_width' => '0',
'catalog_max_img_height' => '0',
'catalog_max_prev_width' => '0',
'catalog_max_prev_height' => '0',
'catalog_add_img_cnt' => '100',
'catalog_default_price' => 'не указана',
'catalog_show_price_1' => 'on',
'catalog_show_price_2' => '',
'catalog_show_price_3' => '',
'catalog_show_price_4' => 'on',
'catalog_show_price_5' => 'on',
'catalog_default_rights' => '111111110001',
'catalog_yml_name' => 'YML - Suprashop',
'catalog_yml_company' => 'YML - Suprashop',
'catalog_file_alarm' => 'Предупреждающий текст для скачивания файлов!',
/* ABOCMS:END */
);
?>