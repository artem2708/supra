<?php
$_MSG = Array(
	"catalog"						=>	"Каталог",

	"Good"							=>	"Товар",
	"Goods"							=>	"Товары",
	"Groups"						=>	"Группы",
	"Properties"					=>	"Свойства",
	"Producers"						=>	"Производители",
	"Currencies"					=>	"Курсы валют",
	"Import_Export"					=>	"Импорт/Экпорт",
	"Catalogue_tree"				=>	"Дерево",

	"Show_assigned_goods"			=>	"Показывать связанные товары",
	"Show_leaders"					=>	"Показывать лидеров продаж",
	"Show_catalog_tree"				=>	"Показывать дерево каталога",
	"Show_top_levels"				=>	"Показывать 2 уровня категорий",
	"Show_search_form"				=>	"Показывать форму поиска",
	"Show_category"					=>	"Показывать категорию",
	"Show_catalog"					=>	"Показывать каталог",
	"Show_categories"				=>	"Показывать категории",
	"Show_grp"						=>	"Показывать для групп",
	"All_groups"					=>  "Все группы",
	"Add_good"						=>	"Добавить товар",
	"Add_category"					=>	"Добавить категорию",
	"Catalogue_root"				=>	"Корень каталога",
	"ATTENTION_Delete_error"		=>	"ПРЕДУПРЕЖДЕНИЕ: Произошла ошибка во время удаления ветки категорий, возможно дальнейшее нормальное функционирование.",
	"Deleted_categories"			=>	"Удалено категорий",
	"Errors"						=>	"Ошибок",
	"Array_with_deleting_IDs"		=>	"Массив с ID для удаления",
	"Add_group"						=>	"Добавить группу",
	"Add_property"					=>	"Добавить свойство",
	"Group_property"				=>	"Групповое свойство",
	"Search_results"				=>	"Результаты поиска",
	"Sales_leaders"					=>	"Лидеры продаж",
	"no"							=>	"нет",
	"NO"							=>	"НЕТ",
	"Suspend_publication"			=>	"Отключить публикацию",
	"Activate_publication"			=>	"Включить публикацию",
	"Category_image"				=>	"Изображение для категории",
	"Look_image"					=>	"Просмотреть изображение",
	"Confirm_suspend_publication"	=>	"Вы действительно хотите отключить публикацию?",
	"Confirm_activate_publication"	=>	"Вы действительно хотите включить публикацию?",
	"Confirm_unset_leader"			=>	"Вы действительно хотите снять отметку - лидер продаж",
	"Confirm_set_leader"			=>	"Вы действительно хотите установить отметку - лидер продаж",
	"Confirm_unset_novelty"			=>	"Вы действительно хотите снять отметку - новинка",
	"Confirm_set_novelty"			=>	"Вы действительно хотите установить отметку - новинка",
	"Unset_leader"					=>	"Снять отметку - лидер продаж",
	"Set_leader"					=>	"Поставить отметку - лидер продаж",
	"Controls"						=>	"Настройки",
	"Import_Export"					=>	"Импорт/Экспорт",
	"Currencies"					=>	"Курсы валют",
	"Catalogue_tree"				=>	"Дерево каталога",
	"Categories_goods"				=>	"Категории и товары",
	"Category_image"				=>	"Изображение для категории",
	"Create_new_category"			=>	"Создать новую категорию",
	"Edit_category"					=>	"Редактировать категорию",
	"Category_delete"				=>	"Удаление категории",
	"Add_producer"					=>	"Добавить производителя",
	"Producers"						=>	"Производители",
	"Edit_producer"					=>	"Редактировать производителя",
	"Producer_delete"				=>	"Удаление производителя товара",
	"Insert_middle_image"			=>	"Вставить маленькое изображение",
	"Insert_big_image"				=>	"Вставить большое изображение",
	"Image_name"					=>	"Название изображения",
	"Look"							=>	"Просмотреть",
	"Middle_image"					=>	"Маленькое изображение",
	"Big_image"						=>	"Большое изображение",
	"Edit_good"						=>	"Редактировать товар",
	"Goods_groups"					=>	"Группы товаров",
	"Edit_group"					=>	"Редактировать группу товаров",
	"Group_delete"					=>	"Удаление группы товаров",
	"Group_properties"				=>	"Свойства групп товаров",
	"Edit_property"					=>	"Редактировать свойство",
	"Property_delete"				=>	"Удаление свойства",
	"Define_group_properties"		=>	"Определить свойства группы",
	"Block_properties"				=>	"Свойства блока",
	"Active"						=>	"Публ.",
	"Ed"							=>	"Ред.",
	"Section"						=>	"Раздел",
	"Order"							=>	"Порядок",
	"Add"							=>	"Добавить",
	"Del"							=>	"Удл.",
	"Edit"							=>	"Редактировать",
	"Category_ID"					=>	"ID категории",
	"is"							=>	"равен",
	"Move_up"						=>	"Сместить вверх",
	"Move_down"						=>	"Сместить вниз",
	"Confirm_delete_record"			=>	"Вы действительно хотите удалить эту запись?",
	"Delete"						=>	"Удалить",
	"Goods_list"					=>	"Список товаров",
	"Select_category"				=>	"Выберите категорию.",
	"Confirm_move_good_to"			=>	"Вы действительно хотите переместить выбранный товар в категорию",
	"No_selected_goods"				=>	"Для проведения операции не выбрано ни одного товара.\\nЧтобы отметить товар, воспользуйтесь флажками в первом столбце списка.",
	"Empty_field"					=>	"Пустое поле",
	"Seek"							=>	"Искать",
	"Back_to_goods_list"			=>	"Вернуться к списку товаров",
	"Title"							=>	"Наименование",
	"Presence"						=>	"Наличие",
	"Leader"						=>	"Лидер",
	"Novelty"						=>	"Новинки",
	"Shows"							=>	"Просмотры",
	"Select_category_for_move"		=>	"Выберите категорию для перемещения товаров",
	"Move"							=>	"Переместить",
	"Not_found"						=>	"По Вашему запросу ничего не найдено.",
	"Go_back"						=>	"Вернуться назад",
	"Pages"							=>	"Страницы",
	"Show_category"					=>	"Отображать категорию",
	"All_categories"				=>	"Все категории",
	"Go_to_edit"					=>	"Перейти к редактированию",
	"Goods_per_page"				=>	"Товаров на странице",
	"Show_prod_cnt"					=>	"Показывать кол-во товаров в категории",
	"No"							=>	"Нет",
	"Calculator"					=>	"Калькулятор",
	"Parts"							=>	"Комплектующие",
	"Model"							=>	"Модель",
	"Quantity"						=>	"Количество",
	"Cost"							=>	"Стоимость",
	"Add_to_cart"					=>	"Добавить в корзину",
	"Total"							=>	"Итого",
	"Removing_category"				=>	"Удаление категории",
	"deletes_its_goods"				=>	"приведет к удалению всех связанных с ней подкатегорий и товаров!",
	"Confirm_delete_category"		=>	"Вы действительно хотите удалить эту категорию?",
	"Removing_group"				=>	"Удаление группы",
	"unsets_its_goods_group"		=>	"приведет к тому, что все связанные с ней товары будут иметь тип с group_id = 0!",
	"Confirm_delete_group"			=>	"Вы действительно хотите удалить из каталога эту группу товаров?",
	"Confirm_all_assigned_with"		=>	"Удалять все связанные с группой",
	"props_not_assigned_to_other"	=>	"свойства не связанные с другими группами?",
	"Confirm_delete_group_property"	=>	"Вы действительно хотите удалить групповое свойство",
	"Removing_producer"				=>	"Удаление производителя",
	"unsets_its_goods_ptype"		=>	"приведет к тому, что все связанные с ним товары будут иметь тип с id = 0!",
	"Confirm_delete_producer"		=>	"Вы действительно хотите удалить из каталога этот тип товаров?",
	"Category_name_not_filled"		=>	"Название категории не заполнено!",
	"Name"							=>	"Название",
	"Goods_group_default"			=>	"Группа товаров по умолчанию",
	"Select_group"					=>	"Выбрать группу",
	"Description"					=>	"Описание",
	"Page_description"				=>	"Описание страницы",
	"Keywords"						=>	"Ключевые слова",
	"required_fields"				=>	"поля, обязательные для заполнения.",
	"Save"							=>	"Сохранить",
	"Cancel"						=>	"Отменить",
	"Group_name_not_filled"			=>	"Название группы не заполнено!",
	"Group_name"					=>	"Название группы",
	"GoodID_must_be_positive"		=>	"ID товара может быть только целое положительное число.",
	"Group_ID_is"					=>	"ID группы",
	"Enter_good_name"				=>	"Введите название товара",
	"Choose_producer"				=>	"Выберите производителя товара",
	"Choose_currency"				=>	"Выберите валюту",
	"Good_code"						=>	"Код товара",
	"articul"						=>	"артикул",
	"Good_producer"					=>	"Производитель товара",
	"Select_producer"				=>	"Выберите производителя",
	"Short_description"				=>	"Краткое описание",
	"Full_description"				=>	"Полное описание",
	"Currency"						=>	"Валюта",
	"Select_currency"				=>	"Выбрать валюту",
	"Retail_price"					=>	"Розничная цена",
	"Partner_price"					=>	"Партнёрская цена",
	"Dealer_price"					=>	"Дилерская цена",
	"Price"							=>	"Цена",
	"Goods_group"					=>	"Группа товара",
	"Assigned_goods"				=>	"Связанные товары",
	"ID"							=>	"ID",
	"Confirm_delete_assigned_rec"	=>	"Вы действительно хотите удалить эту запись из связанных товаров?",
	"Property_name_not_filled"		=>	"Название свойства не заполнено!",
	"Enter_valid_property_name"		=>	"Заполните правильно имя свойства! Используйте только латинские буквы, цифры и символ подчеркивания.",
	"Save_data_error"				=>	"Ошибка сохранения данных!",
	"Property_name_not_unique"		=>	"Имя свойства неуникальное!",
	"Dynamic_property_type"			=>	"Тип динамического свойства",
	"Property_title"				=>	"Заголовок свойства",
	"Property_name"					=>	"Имя свойства",
	"Property_prefix"				=>	"Префикс свойства",
	"Property_suffix"				=>	"Суффикс свойства",
	"Default_value"					=>	"Значение по умолчанию",
	"Add_default_value"				=>	"Добавить значение по умолчанию",
	"Added_images"					=>	"Дополнительные фотографии",
	"Main_images"					=>	"Основная фотография",
	"Decrease"						=>	"Уменьшить",
	"Increase"						=>	"Увеличить",
	"Image_type"					=>	"Тип изображения",
	"Producer_name_not_filled"		=>	"Название производителя не заполнено!",
	"Link"							=>	"Ссылка",
	"Select"						=>	"Выбрать",
	"Search"						=>	"Поиск",
	"Range"							=>	"Диапазон",
	"Property"						=>	"Свойство",
	"Prefix"						=>	"Префикс",
	"Suffix"						=>	"Суффикс",
	"Group_discounts"				=>	"Скидки для группы",
	"Discount"						=>	"Скидка",
	"From"							=>	"От",
	"To"							=>	"До",
	"Value"							=>	"Значение",
	"Type"							=>	"Тип",
	"Group"							=>	"Группа",
	"Data_download_success"			=>	"Информация успешно выгружена.",
	"Data_upload_success"			=>	"Информация успешно загружена.",
	"Upload_XML_file"				=>	"Загрузить файл в формате XML",
	"Upload"						=>	"Загрузить",
	"Clear_catalogue_before"		=>	"Предварительно полностью очистить каталог",
	"Download_XML_file"				=>	"Выгрузить содержимое каталога в XML формате",
	"Download"						=>	"Выгрузить",
	"Download_YML_file"				=>	"Выгрузить содержимое каталога в YML формате",
	"Upload_CSV_file"				=>	"Загрузить CSV файл с обновлением каталога",
	"Download_CSV_file"				=>	"Выгрузить содержимое каталога в CSV формате",
	"Category_before_goods"			=>	"Категория должна всегда следовать в CSV файле раньше, чем вложенные в неё товары.",
	"Separator_is"					=>	"В качестве разделителя полей в CSV файле используется",
	"fields_order_in_CSV"			=>	"порядок следования полей в CSV файле",
	"Create_YML_file"				=>	"Создать файл в формате YML",
	"Create"						=>	"Создать",
	"About_YML"						=>	'XML файл в формате YML используется для раскрутки и продвижения интернет-магазина путем размещения информации о товарах на крупных торговых интернет-площадках, например: <a href="http://market.yandex.ru/" target=blank>Яндекс-Маркет</a>, <a href="http://pokupki.rambler.ru/" target=blank>Rambler-Покупки</a>, <a href="http://www.abc.ru/" target=blank>ABC-цены</a>, <a href="http://www.marketline.ru/" target=blank>МаркетLine</a>, <a href="http://www.price.ru/" target=blank>Price.Ru</a>',
	"Values"						=>	"Значения",
	"Choose"						=>	"Выберите",
	"Image_type"					=>	"Тип изображения",
	"size"							=>	"размер",
	"Yes"							=>	"Да",
	"Good_Id"						=>	"Id товара",
	"no_value"						=>	"нет значения",
	"Type_is_not_defined"			=>	"Тип не определён",
	"img_loaded"					=>	"изображение загруженно",
    "Alias"				        	=>	"Ссылка",
    "Alias_Info"					=>	"Если не задано, будет использоватся ID категории",
    "Generated"						=>	"Сгенерировать из большого изображения",
	"Execute"						=>	"Выполнить",
	"Choose_action"					=>	"Выберите действие",
	"Suspend"						=>	"Отключить публикацию",
	"Activate"						=>	"Включить публикацию",
	'cbr_sync'						=>	'Получить курс ЦБ РФ',
	'Rank'     						=>	'Порядок',
	'Change'     					=>	'Изменить',
	'Through_comma'					=>	"через запятую",
	'Move_categories'     			=>	'Переместить в другую категорию',
	'No_select_categories'     		=>	'Не выбраны категории',
	'Isnot_categories'      		=>	'Нет категорий для выбора',
	'Parent_category_err'     		=>	'Не верно определена родительская категория',
	'Category_moved'     			=>	'Категории перемещены',
	'Select_categories'     		=>	'Удажите куда будут перемещены категории',
	'Move'              			=>	'Переместить',
	'Close_window'      			=>	'Закрыть окно',
	'Move_categories'      			=>	'Переместить категории',
	'To_comment'					=>	'Комментировать',
	'Copy'         					=>	'Копировать',
	'Set_other_link'                =>	'Укажите другое значение ссылки',
	'Not_found'                     =>	'Не найден',
	'Add_image'                     =>	'Добавить изображение',
	'Generete_image'                =>	'Если маленькое изображение не указано, то оно будет сгенирировано из большого',
	'Add_assigned_goods'     		=>  'Добавить связанные товары',
	'Add_analogues'         		=>  'Добавить аналоги',
	'Analogues'             		=>  'Аналоги',
	'Delete_from_selected'          =>  'Удалить из выбранных товаров',
	'Loading'                       =>  'Загрузка',
	'No_goods'                      =>  'Нет товаров',
	'Selected_goods'                =>  'Выбранные товары',
	'Add_selected_goods'            =>  'Сохранить и закрыть окно',
	'Close_window'                  =>  'Закрыть окно',
	'Show_novetlies'                =>  'Показать новинки',
	'Try_on'       					=>	'Применить',
	'Clean_list'       				=>	'Очистить список',
	'Prod_prop'       				=>	'Свойства товара',
	'Order_prop'       				=>	'Свойства заказа',
	'Groupping'       				=>	'Группировать по',
	'Images'          				=>	'Изображения',
	'Relations'                     =>   'Связи',
	'Goods_added'					=>   'Добавление товаров',
	'Show_last_viewed'				=>   'Показать последние просмотренные товары',
	'Last_viewed'      				=>   'Последние просмотренные товары',
	'Old_price'      				=>   'Старая цена',
    'Prod_tpl'      				=>   'Шаблон карточки товара',
);
?>