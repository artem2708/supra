<?php
$sql = array(
    'CREATE TABLE `{site_prefix}_catalog_order_prop_val` (
    `id` int unsigned NOT NULL auto_increment,
    `prod_id` int NOT NULL default 0,
    `prop_val_id` int(11) NOT NULL default 0,
    PRIMARY KEY (`id`),
    KEY (`prod_id`)
) ENGINE=MyISAM',
    'ALTER TABLE `{site_prefix}_catalog_products` CHANGE `price_1` `price_1` DECIMAL( 16, 2 ) UNSIGNED DEFAULT 0 NOT NULL',
    'ALTER TABLE `{site_prefix}_catalog_products` CHANGE `price_2` `price_2` DECIMAL( 16, 2 ) UNSIGNED DEFAULT 0 NOT NULL',
    'ALTER TABLE `{site_prefix}_catalog_products` CHANGE `price_3` `price_3` DECIMAL( 16, 2 ) UNSIGNED DEFAULT 0 NOT NULL',
    'ALTER TABLE `{site_prefix}_catalog_products` CHANGE `price_4` `price_4` DECIMAL( 16, 2 ) UNSIGNED DEFAULT 0 NOT NULL',
    'ALTER TABLE `{site_prefix}_catalog_products` CHANGE `price_5` `price_5` DECIMAL( 16, 2 ) UNSIGNED DEFAULT 0 NOT NULL',
    'UPDATE `{site_prefix}_catalog_products` SET price_1=price_1/100',
    'UPDATE `{site_prefix}_catalog_products` SET price_2=price_2/100',
    'UPDATE `{site_prefix}_catalog_products` SET price_3=price_3/100',
    'UPDATE `{site_prefix}_catalog_products` SET price_4=price_4/100',
    'UPDATE `{site_prefix}_catalog_products` SET price_5=price_5/100',
);
?>