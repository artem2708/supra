<?php

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_catalog_categories` (
            `id` int(10) unsigned NOT NULL auto_increment,
            `cleft` int(10) unsigned NOT NULL default '0',
            `cright` int(10) unsigned NOT NULL default '0',
            `clevel` int(10) unsigned NOT NULL default '0',
            `parent_id` int(11) NOT NULL default '0',
            `active` tinyint(1) NOT NULL default '1',
            `title` varchar(255) NOT NULL default '',
            `alias` varchar(150) default NULL,
            `group_id` int(10) unsigned NOT NULL default '0',
            `description` text,
            `img` varchar(127) NOT NULL default '',
            `page_description` text,
            `keywords` text,
            `rank` int(11) NOT NULL default '0',
            `commentable` TINYINT( 1 ) UNSIGNED DEFAULT 0 NOT NULL,
            `owner_id` int(10) unsigned NOT NULL default '0',
            `usr_group_id` int(10) unsigned NOT NULL default '0',
            `rights` int(10) unsigned NOT NULL default '0',
            PRIMARY KEY  (`id`),
            UNIQUE KEY (`alias`),
            KEY `cleft` (`cleft`,`cright`,`clevel`)
			) ENGINE=MyISAM";

$MOD_TABLES[] = 'CREATE TABLE `'.$prefix.'_catalog_groups` (
			`id` int(11) NOT NULL auto_increment,
			`group_title` varchar(255) NOT NULL default "",
			PRIMARY KEY  (`id`)
			) ENGINE=MyISAM';

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_catalog_products` (
					  `id` int(11) NOT NULL auto_increment,
            `category_id` int(11) NOT NULL default '0',
            `group_id` int(11) NOT NULL default '0',
            `ptype_id` int(11) NOT NULL default '0',
            `product_code` varchar(127) NOT NULL default '',
            `product_title` varchar(255) NOT NULL default '',
            `alias` varchar(150) default NULL,
            `product_inf` text NOT NULL,
            `product_description` text NOT NULL,
            `currency` varchar(255) NOT NULL default '',
            `price_1` DECIMAL(16, 2) UNSIGNED DEFAULT 0 NOT NULL,
            `price_2` DECIMAL(16, 2) UNSIGNED DEFAULT 0 NOT NULL,
            `price_3` DECIMAL(16, 2) UNSIGNED DEFAULT 0 NOT NULL,
            `price_4` DECIMAL(16, 2) UNSIGNED DEFAULT 0 NOT NULL,
            `price_5` DECIMAL(16, 2) UNSIGNED DEFAULT 0 NOT NULL,
            `image_middle` varchar(255) default NULL,
            `image_big` varchar(255) default NULL,
            `product_availability` varchar(10) NOT NULL default '',
            `active` tinyint(1) NOT NULL default '0',
            `rank` int(11) NOT NULL default '0',
            `leader` tinyint(1) NOT NULL default '0',
            `novelty` tinyint(1) default '0',
            `seo_title` varchar(255) NOT NULL default '',
            `description` text,
            `keywords` text,
            `hits` bigint(20) unsigned NOT NULL default '0',
            `owner_id` int(10) unsigned NOT NULL default '0',
            `usr_group_id` int(10) unsigned NOT NULL default '0',
            `rights` int(10) unsigned NOT NULL default '0',
            PRIMARY KEY  (`id`),
            UNIQUE KEY (`alias`),
            KEY `category_id` (`category_id`)
					) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_catalog_properties` (
					`property_id` int(11) NOT NULL default '0',
          `group_id` int(11) NOT NULL default '0',
          `search` tinyint(1) NOT NULL default '0',
          `range` tinyint(1) NOT NULL default '0',
          `ord` smallint(6) NOT NULL default '0',
          PRIMARY KEY  (`property_id`,`group_id`)
					) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_catalog_properties_def_values` (
					`id` int(11) NOT NULL auto_increment,
          `property_id` int(11) NOT NULL default '0',
          `value` varchar(255) NOT NULL default '',
          PRIMARY KEY  (`id`),
          UNIQUE KEY `uniq` (`property_id`,`value`)
					) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_catalog_properties_discounts` (
					`discount_id` int(11) NOT NULL default '0',
          `group_id` int(11) NOT NULL default '0',
          PRIMARY KEY  (`discount_id`,`group_id`)
					) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_catalog_properties_table` (
					`id` int(11) NOT NULL auto_increment,
          `property_title` varchar(255) NOT NULL default '',
          `property_name` varchar(127) NOT NULL default '',
          `property_prefix` text NOT NULL,
          `property_suffix` text NOT NULL,
          `type` tinyint(4) NOT NULL default '0',
          PRIMARY KEY  (`id`),
          UNIQUE KEY `property_name` (`property_name`)
					) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_catalog_property_values` (
					`id` int(11) NOT NULL auto_increment,
          `product_id` int(11) NOT NULL default '0',
          `property_id` int(11) NOT NULL default '0',
          `value` text NOT NULL,
          `group_id` int(11) NOT NULL default '0',
          PRIMARY KEY  (`id`),
          UNIQUE KEY `uniq` (`product_id`,`property_id`)
					) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_catalog_ptypes` (
					`id` int(11) NOT NULL auto_increment,
          `ptype_name` varchar(255) NOT NULL default '',
          `link` varchar(255) NOT NULL default '',
          PRIMARY KEY  (`id`)
					) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_catalog_concerned_products` (
    `prod_id` int(11) NOT NULL default 0,
    `concrn_id` int(11) NOT NULL default 0,
    `type` tinyint(1) unsigned NOT NULL default 0,
    `rank` int(11) NOT NULL default 0,
    UNIQUE KEY `prod_id` (`prod_id`,`concrn_id`, `type`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_catalog_img` (
    `id` int unsigned NOT NULL auto_increment,
    `prod_id` int NOT NULL default 0,
    `img` varchar(127) NOT NULL default '',
    `preview` varchar(127) NOT NULL default '',
    `rank` tinyint unsigned NOT NULL default 0,
    PRIMARY KEY (`id`),
    KEY (`prod_id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_catalog_order_prop_val` (
    `id` int unsigned NOT NULL auto_increment,
    `prod_id` int NOT NULL default 0,
    `prop_val_id` int(11) NOT NULL default 0,
    PRIMARY KEY (`id`),
    KEY (`prod_id`)
) ENGINE=MyISAM";
?>
