<?php
$sql = array(
    'ALTER TABLE `{site_prefix}_catalog_categories` ADD `commentable` TINYINT( 1 ) UNSIGNED DEFAULT 0 NOT NULL AFTER `rank`',
    'ALTER TABLE `{site_prefix}_catalog_categories` DROP INDEX `parent_id`, ADD UNIQUE (`alias`)',
    'ALTER TABLE `{site_prefix}_catalog_products` DROP INDEX `category_id_2`, ADD UNIQUE (`alias`)',
    "CREATE TABLE `{site_prefix}_catalog_img` (`id` int unsigned NOT NULL auto_increment, `prod_id` int NOT NULL default 0, `img` varchar(127) NOT NULL default '', `preview` varchar(127) NOT NULL default '', `rank` tinyint unsigned NOT NULL default 0, PRIMARY KEY (`id`), KEY (`prod_id`)) ENGINE=MyISAM",
    'ALTER TABLE `{site_prefix}_catalog_concerned_products` ADD `type` TINYINT( 1 ) UNSIGNED DEFAULT 0 NOT NULL AFTER `concrn_id`',
    'ALTER TABLE `{site_prefix}_catalog_concerned_products` DROP INDEX `prod_id`, ADD UNIQUE `prod_id` ( `prod_id` , `concrn_id` , `type` )'
);
?>