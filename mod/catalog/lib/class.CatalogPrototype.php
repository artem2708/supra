<?php

require_once(RP.'inc/class.NsDbTree.php');
require_once(RP.'inc/class.TreeMenu.php');
require_once(RP.'inc/class.ParseCSVFormat.php');
require_once(RP.'inc/class.ImportData.php');
require_once(RP.'inc/class.ExportData.php');
require_once(RP.'mod/catalog/lib/class.catalogDB.php');
require_once(RP.'mod/catalog/lib/class.StructCSVFile.php');

class CatalogPrototype extends Module { 
    var $main_title;												// переопределяет title страницы, если модуль главный
    var $table_prefix;
    var $tpl_path			= 'mod/catalog/tpl/';
    var $module_name		= 'catalog';
    var $_msg				= array();
    var $block_module_actions		= array();
    var $block_main_module_actions	= array();
    var $default_action				= 'assortment';					// действие, выполняемое модулем по умолчанию
    var $admin_default_action		= 'assortment';
    var $addition_to_path	= array();								// добавляет к пути страницы строку, если модуль главный
    var $CDBT_fields		= array(
        'left'	=> 'cleft',
        'right'	=> 'cright',
        'level'	=> 'clevel',
        'parent_id' => 'parent_id'
    );

    var $product_properties	= array();
    var $product_images		= array(array('title' => 'Big picture', 'field_name'	=> 'big_image', 'resize' => 0),
        array('title' => 'Small picture', 'field_name'	=> 'small_image', 'resize' => 0)
    );

    var $category_images	= array(array('title' => 'Big picture', 'field_name'	=> 'big_image', 'resize' => 0),
        array('title' => 'Small picture', 'field_name'	=> 'small_image', 'resize' => 0)
    );
    var $catalog_image_formats = array(
        'image/jpeg'	=> 'jpg',
        'image/jpg'		=> 'jpg',
        'image/pjpeg'	=> 'jpg',
        'image/gif'		=> 'gif',
        'image/x-png'	=> 'png',
        'image/png'		=> 'png',
        'message/rfc822'=> 'mht',
    );
    var $img_exts = array('jpg', 'jpeg', 'gif', 'png');
    var $currencies = array();
    var $alow_prp_types_id		 = array(1, 2);
    var $alow_prp_range_types_id = array(1);
    var $data_charset			 = 'utf-8'; // cp1251, koi8-r, iso-8859-1, utf-8
    var $cache = array();
    var $action_tpl = array(
        'assortment' => 'catalog_products.html',
        'showsearchform' => 'catalog_searchform.html',
        'showleaders' => 'catalog_leaders.html',
        'showtree' => 'catalog_cat_tree.html',
        'showcatmenu' => 'catalog_menu.html',
        'showtoplevels' => 'catalog_top_levels.html',
        'novelties' => 'catalog_novelties.html',
        'last_viewed' => 'catalog_last_viewed.html'
    );
    var $cats = array();
    var $show_diog = false;
    var $archive = 0;
    var $cats1 = array();
    var $is_get_gp = false;
    var $gp = array();
    function CatalogPrototype($action = '', $transurl = '?', $properties = array(), $prefix = NULL, $rewrite_mod_params = NULL, $adminurl = NULL, $noadmin = false) {
        GLOBAL	$main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $core,	$server, $lang, $request_clearcmplist, $permissions, $request_type, $_RESULT, $db, $tree;

        /* Get $action, $tpl_name, $permissions */
        extract( $this->init(array(
            'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
            'action' => $action, 'tpl_name' => $properties['tpl']
        )), EXTR_OVERWRITE);

        $CONFIG['catalog_img_path'] = str_replace("{SITE_PREFIX}", $this->table_prefix, $CONFIG['catalog_img_path']);
        $CONFIG['catalog_ctg_img_path'] = str_replace("{SITE_PREFIX}", $this->table_prefix, $CONFIG['catalog_ctg_img_path']);
        $CONFIG['catalog_add_img_path'] = str_replace("{SITE_PREFIX}", $this->table_prefix, $CONFIG['catalog_add_img_path']);
        $CONFIG['catalog_add_prev_path'] = str_replace("{SITE_PREFIX}", $this->table_prefix, $CONFIG['catalog_add_prev_path']);
        $CONFIG['catalog_export_path'] = str_replace("{SITE_PREFIX}", $this->table_prefix, $CONFIG['catalog_export_path']);
        $CONFIG['catalog_import_path'] = str_replace("{SITE_PREFIX}", $this->table_prefix, $CONFIG['catalog_import_path']);
        $CONFIG['catalog_upload_yml_path'] = str_replace("{SITE_PREFIX}", $this->table_prefix, $CONFIG['catalog_upload_yml_path']);

        $CDBT = new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
        $this->currencies = $CONFIG['catalog_currencies'];

        if ('only_create_object' == $action) {
            return;
        }

        $max_rows = (int)$properties[1];											// показывать товаров на странице
        // очистим список сравнения
        $request_clearcmplist = strip_tags(trim($request_clearcmplist));
        $request_clearcmplist = str_replace(array("<", ">", "\"", "'"), "", $request_clearcmplist);
        $request_clearcmplist = str_replace('\\', '', $request_clearcmplist);
        if ($request_clearcmplist === "yes") {
            unset($_SESSION["compare_prod"]);
            unset($_SESSION["compare_gr_id"]);
            header('Location: '.$_SERVER["HTTP_REFERER"]);
            exit();
        }
        //var_dump($action);
        if (! self::is_admin()) {

//---------------------------------- обработка действий с сайта --------------------------------//
            if ($PAGE['dop_params'] && 0 == $PAGE['field']) {
                if ($PAGE['dop_params'][2]) {
                    $ID = $this->prodExists($PAGE['dop_params'][2], $PAGE['dop_params'][1]);
                    if ($ID) {
                        $action = 'shwprd';
                        $GLOBALS['request_id'] = (int) $ID;
                        $PAGE['process_params'] = true;
                        $tpl_name = $this->checkTplName($tpl_name, $action);
                        $prod = $this->getProduct($ID);
                        $PAGE['real_id'] = $prod['category_id'];
                        $PAGE['top_category'] = $this->getTopCategory($PAGE['real_id']);

                    }
                } elseif ($PAGE['dop_params'][1]) {
                    $ID = $this->ctgrExists($PAGE['dop_params'][1]);
                    if ($ID) {
                        $action = $main_field_act = 'assortment';
                        $GLOBALS['request_id'] = (int) $ID;
                        $PAGE['process_params'] = true;
                        $tpl_name = $this->checkTplName($tpl_name, $action);
                        CatalogPrototype::mainFieldProp(array('act' => $action, 'ctgr_id' => $ID));
                        $PAGE['real_id'] = $ID;
                        $PAGE['top_category'] = $this->getTopCategory($ID);

                    }
                }
            }

            if ($this->actionExists($action)) {
                $this->doAction($action);
                return;
            }
            //echo $action.'<br />';
            if(isset($_GET['check_action']))
            {
                var_dump($action);
            }
            switch ($action) {

                case 'add_mod_bd':
                    global $db;
                    $arr=$db->getArrayOfResult("Select id, product_description From sup_rus_catalog_products");
                    foreach($arr as $b){
                        $s=$b['product_description'];
                        $l=str_replace('ybobra.ru','',$s);
                        $l=str_replace('stroymaterialy21.ru','',$l);
                        $l=str_replace('http:///','',$l);
                        if($b['product_description'] != $l)
                        {
                            $i++;
                            var_dump($b['id'],$l,'----','----');
                            $l=$db->escape(trim($l));
                            $db->query("Update sup_rus_catalog_products SET product_description='{$l}' Where id=".$b['id']);
                        }
                    }
   
                    exit;
                    break;
                case 'set_compare':

                    $id = (int)$_POST['id'];
                    if($id && $prod = $this->getProduct($id)){
                        if(!isset($_SESSION['compare'])) $_SESSION['compare'] = array();
                        $find = false;
                        foreach($_SESSION['compare'] as $k => $v){
                            if( $id == $v )
                            {
                                $find = true;
                                $_SESSION['compare'][$k] = NULL;
                                unset($_SESSION['compare'][$k]);
                                break;
                            }
                        }
                        if ( !$find )
                            $_SESSION['compare'][] = $id ;

                        $compare_count = count ($_SESSION['compare']);
                        echo $compare_count;
                    }
                    exit;
                    break;
                case 'download_count':

                    $id = (int)$_POST['id'];
                    global $db;
                    $arr=$db->getArrayOfResult("Select download_count From sup_rus_catalog_files Where id={$id}");
                    if(!empty($arr))
                    {
                        $db->set('sup_rus_catalog_files',array('download_count'=>$arr[0]['download_count']+1),array('id'=>$id));
                    }
                        echo  json_encode(array('data'=>1));
                    exit;
                    break;
                case 'del_compare':
                    $id = (int)$_GET['id'];
                    //$category = (int)$_GET['category']; 				

                    if(isset($_SESSION['compare']))
                        foreach($_SESSION['compare'] as $k => $v){
                            if( $id == $v ){
                                $find = true;
                                $_SESSION['compare'][$k] = NULL;
                                unset($_SESSION['compare'][$k]);
                                break;
                            }
                        }
                    header('Location: '.$baseurl.'&action=compare&category='.$category);
                    exit;
                    break;
                case 'del_compare_session' :
                    if ( isset( $_SESSION['compare'] ))
                        unset ($_SESSION['compare']);

                    echo count( $_SESSION['compare'] );
                    exit();
                    break;
                case 'compare':
                    $main->include_main_blocks('catalog_compare.html', 'main');
                    $tpl->prepare();
                    //$category = (int)$_GET['category']; 				

                    $ids = array();
                    if(isset($_SESSION['compare']))
                        foreach($_SESSION['compare'] as $v)
                            $ids[] = $v;

                    if ( isset($ids) && count($ids) > 0 )
                        $ids_str = implode ( ',', $ids );

                    if ( !empty ( $ids_str ) )
                    {
                        $sql = "SELECT category_id FROM ".$this->table_prefix."_catalog_products 
							WHERE id IN (" . $ids_str . ")";

                        $compare_cats = $db->getArrayOfResult($sql);
                        foreach ( $compare_cats as $value )
                            $compare_cats_out[] = $value['category_id'];

                        $parent = $tree->GetParentInfo( $compare_cats_out[0], '' );
                        if ( !empty( $parent ) )
                        {
                            $parent = $parent['id'];

                            $tree->Ajar( $parent, array ('id') );
                            while ( $item = $tree->NextRow() )
                                $first[] = $item['id'];

                            $wrong_categories = false;

                            foreach ( $compare_cats_out as $value )
                            {
                                if ( !in_array( $value, $first ) )
                                    $wrong_categories = true;
                            }
                        }
                    }
                    if(sizeof($ids) < 2)
                        $tpl->newBlock('no_prods');
                    elseif ( $wrong_categories == true )
                        $tpl->newBlock('wrong_categories');
                    else{
                        $tpl->newBlock('prods');
                        $prods = $this->getProdsOfSubCategs(1, 100, 1, '', $ids);

                        foreach($prods as $v){
                            $tpl->newBlock('image');
                            $tpl->assign(array(
                                'src' =>  $v['img'],
                                'title' => htmlspecialchars($v['prod_title'])
                            ));

                            $tpl->newBlock('model');
                            $tpl->assign(array(
                                'link' =>  $v['prod_link'],
                                'del_link' => $baseurl.'&action=del_compare&id='.$v['prod_id'],
                                'title' => htmlspecialchars($v['prod_title'])
                            ));
                            if($v['prod_price_1']){
                                $tpl->newBlock('link');
                                $tpl->assign('link', 'http://www.suprashop.ru'.$v['prod_link']);
                            }
                            $tpl->newBlock('price_block');
                            if($v['prod_price_2'] == 0){

                                $tpl->newBlock('price');
                                $tpl->assign('link', $v['prod_link']);
                                $tpl->assign('price', $v['prod_price_1']);
                            }else{
                                $tpl->newBlock('dprice');
                                $tpl->assign('link', $v['prod_link']);
                                $tpl->assign('price', $v['prod_price_1']);
                                $tpl->assign('percent', calcDiscount($v['prod_price_1'], $v['prod_price_2']));
                            }


                            $tpl->newBlock('description');
                            $tpl->assign(array(
                                'description' =>  $v['description']
                            ));



                        }
                        $props = array();
                        $p = array();
                        foreach($prods as $v){
                            $prp = $this->getProductProperies($v['prod_id'], $v['group_id']);
                            unset($prp['dynamic_properties']);
                            $p = $prp;
                            $props[$v['prod_id']] = $prp;
                        }

                        foreach($p as $k => $v_){
                            if($v['type'] == 4) continue;

                            $continue = true;

                            foreach($prods as $v) if($props[$v['prod_id']][$k]['value']) $continue = false;

                            if($continue) continue;

                            $tpl->newBlock('property');
                            $tpl->assign('property_title', $v_['title']);
                            foreach($prods as $v){
                                $tpl->newBlock('property_value');
                                $tpl->assign('value', is_array($props[$v['prod_id']][$k]['value']) ? implode(', ', $props[$v['prod_id']][$k]['value']) : (strlen($props[$v['prod_id']][$k]['value']) ? $props[$v['prod_id']][$k]['value'] : '&nbsp;'));
                            }
                        }

                    }
                    break;

                case 'showsearchform':
                    global $db, $request_id, $request_sdata;
                    $main->include_main_blocks($tpl_name, 'main');
                    $tpl->prepare();

                    $curr_group = intval($request_sdata["group_id"]);
                    $request_id = (int) $request_id;

                    if(!$curr_group && isset($properties[3]) && $properties[3]!='all') {
                        $curr_group = $properties[3];
                    }
                    if (!$curr_group && $action == 'assortment'
                        && $res = $db->getArrayOfResult('SELECT group_id FROM '.$this->table_prefix.'_catalog_categories WHERE id=\''.$request_id."'")
                    ) {
                        $curr_group = $res[0]["group_id"];
                    } elseif (!$curr_group && $action == 'shwprd'
                        && $res = $db->getArrayOfResult('SELECT group_id FROM '.$this->table_prefix.'_catalog_products WHERE id=\''.$request_id."'")
                    ) {
                        $curr_group = $res[0]["group_id"];
                    }

                    if ($properties[4]->name) {
                        $tpl->newBlock('block_show_name');
                        $tpl->assign(array(
                            'pname'	=> trim(htmlspecialchars($request_sdata["pname"])),
                        ));
                    }
                    if ($properties[4]->code) {
                        $tpl->newBlock('block_show_code');
                        $tpl->assign(array(
                            'code'	=> trim(htmlspecialchars($request_sdata["code"])),
                        ));
                    }
                    if ($properties[4]->ptype) {
                        $tpl->newBlock('block_show_ptype');
                        $this->show_ptypes(intval($request_sdata["ptype_id"]));
                    }
                    if ($properties[4]->availability) {
                        $tpl->newBlock('block_show_availability');
                        $tpl->assign(array(
                            'availability_checked' => $request_sdata["availability"] ? 'checked' : '',
                        ));
                    }
                    if ($properties[4]->price) {
                        $tpl->newBlock('block_show_price');
                        $tpl->assign(array(
                            'price1'=> $request_sdata["pprice1"] >0 ? intval($request_sdata["pprice1"]) : "",
                            'price2'=> $request_sdata["pprice2"] >0 ? intval($request_sdata["pprice2"]) : ""
                        ));
                    }
                    if ($properties[4]->sort) {
                        $tpl->newBlock('block_show_sort');
                        $tpl->assign(array(
                            'sort_price_selected' => $request_sdata["sort"] == 'price' ? 'selected' : '',
                            'sort_name_selected' => $request_sdata["sort"] == 'name' ? 'selected' : '',
                            'sort_ptype_selected' => $request_sdata["sort"] == 'ptype' ? 'selected' : '',
                        ));
                    }
                    $tpl->gotoBlock('_ROOT');
                    $tpl->assign(array(
                        'pname'	=> trim(str_replace(array("<", ">", "\"", "'", ".", "@", "\\"), "", $request_sdata["pname"])),
                        'price1' => intval($request_sdata["pprice1"]) ? intval($request_sdata["pprice1"]) : "",
                        'price2' => intval($request_sdata["pprice2"]) ? intval($request_sdata["pprice2"]) : "",
                        'cnt' => $properties[1] >=1 ? intval($properties[1]) : "",
                        'form_action'	=>	$core->formPageLink($CONFIG['catalog_page_link'],$CONFIG['catalog_page_address'], $lang),
                        'action' =>	"searchresult",
                        'lang' => $lang,
                        'link' => $this->module_name,
                    ));
                    if ($properties[4]->group) {
                        $tpl->newBlock('block_show_group');
                        $tpl->assign_array('block_group', $this->getSearchLineType($curr_group));
                        $this->formDynamic($transurl);
                    }

                    break;

                case "searchresult":
                    global $request_sdata, $request_start;
                    $main->include_main_blocks($this->module_name.'_products.html', 'main');
                    $tpl->prepare();

                    $request_cnt = $request_sdata['cnt'] >= 1 ? (int) $request_sdata['cnt']  : $CONFIG['catalog_max_rows'];
                    $prods = $this->getSearchProds( $request_sdata, $request_cnt, $request_start);
                    $nav_str = $prods["nav_str"];
                    unset($prods["nav_str"]);

                    $this->showProdList($prods);
                    if (is_array($prods) && !empty($prods)) {
                        $tpl->gotoBlock('block_products');
                        $tpl->assign(
                            array(
                                'categ_title' => $categ[0]['title'],
                                'categ_id' => $request_id,
                                'categ_descr' => $categ[0]['description'],
                                'baseurl' => $baseurl,
                                'cnt_compare_prod'	=> sizeof($_SESSION["compare_prod"]),
                            )
                        );

                        $main->_show_nav_string(
                            $nav_str['tables'],
                            ' AND '.$nav_str['where'],
                            'nav',
                            'action='.$action.$nav_str['search_params'],
                            $request_start, $request_cnt,
                            '', '', 0, 0, $nav_str
                        );
                    } elseif(empty($categs)) {
                        $tpl->newBlock('block_products_absent');
                    }

                    $this->main_title = $this->_msg["Search_results"];
                    break;

                case "showcatmenu":
                    global $db, $tpl, $request_id;
                    $tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                    $main->include_main_blocks($tpl_name, 'main');
                    $tpl->prepare();
                    $parent_id = intval($properties[2] > 1 ? $properties[2] : 1);

                    if ($properties[3]) {
                        $db->query("SELECT C.id, C.title, if(C.alias is not null and C.alias!='', C.alias, C.id) as alias, COUNT(P.id) as cnt"
                            ." FROM {$this->table_prefix}_catalog_categories C INNER JOIN {$this->table_prefix}_catalog_categories CH"
                            ." ON CH.cleft>=C.cleft AND CH.cright<=C.cright LEFT JOIN {$this->table_prefix}_catalog_products P"
                            ." ON CH.id=P.category_id AND P.active WHERE C.parent_id={$parent_id} AND C.active=1"
                            ." GROUP BY C.id"
                            ." ORDER BY C.{$CONFIG['catalog_ctg_order_by']}");
                    } else {
                        $db->query("SELECT id, title, if(alias is not null and alias!='', alias, id) as alias, 0 as cnt"
                            ." FROM {$this->table_prefix}_catalog_categories WHERE parent_id={$parent_id} and active=1"
                            ." ORDER BY {$CONFIG['catalog_ctg_order_by']}");
                    }

                    if(($cnt = $db->nf())) {
                        $tpl->newBlock("block_category");
                        while($db->next_record()) {
                            $tpl->newBlock("block_category_row");
                            if($db->Record['id'] == $request_id) {
                                $tpl->newBlock("block_category_row_active");
                                $tpl->assign("cat_title", $db->Record["title"]);
                                if ($db->Record["cnt"] > 0) {
                                    $tpl->newBlock("block_active_prod_cnt");
                                    $tpl->assign("cnt", $db->Record["cnt"]);
                                }
                            } else {
                                $tpl->newBlock("block_category_row_nonactive");
                                $tpl->assign("cat_link", $this->getCtgrLink($transurl, $db->Record['alias'], $db->Record['id']));
                                $tpl->assign("cat_title", $db->Record["title"]);
                                if ($db->Record["cnt"] > 0) {
                                    $tpl->newBlock("block_noactive_prod_cnt");
                                    $tpl->assign("cnt", $db->Record["cnt"]);
                                }
                            }
                        }
                    }

                    break;
                case 'search':
                    GLOBAL $request_start;
                    $PAGE['title'] = $PAGE['alt_title'] = 'Поиск';
                    $main->include_main_blocks('catalog_products_search.html', 'main');
                    $tpl->prepare();
                    $request_start = $request_start > 1 ? (int)$request_start : 1;
                    $prods = $this->getProdsOfSubCategs(0, 20, $request_start, '', array(), $_GET['query']);


                    if (is_array($prods) && !empty($prods))
                    {
                        if ( count ( $prods ) == 1)
                        {
                            $prods[0]['prod_link'] = substr($prods[0]['prod_link'], 1);
                            $redirect_url = '/'.$prods[0]['prod_link'];
                            header("Location: ".$redirect_url );
                            exit();
                        }
                        else
                        {
                            $this->showProdList($prods, 1);
                            $tpl->gotoBlock('block_products');
                            $tpl->assign('count', sizeof($this->cats1));
                            $pages_cnt = ceil(sizeof($this->cats1)/20);
                            $main->_show_nav_block($pages_cnt, null, "action=".$action."&query=".urlencode($_GET['query']), $request_start);


                        }
                    }
                    else
                    {
                        $tpl->newBlock('block_products_absent');
                    }
                    $this->main_title = 'Поиск';
                    break;

                case 'assortment':
                    if($_SERVER['SERVER_NAME'] != "supra.ru" && !isset($_GET['sort']))
                    {
                        /* $_GET['sort'] ="d";
                         $_GET['supra']=1;*/
                    }
                    GLOBAL $request_id, $request_parent, $request_start;
                    $max_rows	= $max_rows ? $max_rows : $CONFIG['catalog_max_rows'];
                    $ctgr_id	= (int)$request_id ? (int)$request_id : 'all';
                    $tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                    $main->include_main_blocks($tpl_name, 'main');

                    if(/*!$_REQUEST['new_s'] && */$PAGE['id'] == 300) $this->archive=1;

                    $tpl->prepare();
                    $cat_path = $this->get_nav_path($ctgr_id, $baseurl);
                    //break;
                    if(strripos($cat_path[1],'Телевизоры<') !== false)
                        $tv=true;

                    $_path = $this->addition_to_path;
                    $filter = array(
                        'valid_ctgr' => (is_array($properties[2]) || is_object($properties[2])) && !empty($properties[2])
                                ? array_map('intval', (array)$properties[2]) : false,
                        'with_prod_cnt' => $properties[3]
                    );

                    $categ = $this->getCategory($ctgr_id);

                    if ( $categ['clevel'] == 2 )
                        $main_cat_prods = $this->getMainCatProducts($categ['id']);
                    $main_cat_prods=array();

                    $tpl->assign( array('parent_title' => $categ['title'],
                        'parent_descr' => $categ['description'],
                    ));
                    $ctgr_link = $this->getCtgrLink($transurl, $categ['ctgr_alias'], $categ['id']);

                        $PAGE['description'] = $categ['page_description'] ? strip_tags($categ["page_description"]) : $categ['title'].'-'.$categ['description'];
                        $PAGE['keywords'] = $categ['keywords'] ? strip_tags($categ["keywords"]) : $categ['title'];


                    $categs = $this->getSubCategsForRootCateg($ctgr_id, $transurl, $filter);


                    $tpl->assign(
                        array(
                            'categ_title' => $categ['title'],
                            'categ_id' => $ctgr_id,
                            'categ_img' => '/'.$CONFIG['catalog_ctg_img_path'].$categ['img'],
                            'categ_descr' => $categ['description'],
                            'baseurl' => $baseurl,

                        )
                    );
                    $ctg_list = $this->showCtgList($categs, true, $ctgr_id);
 
                    if(strripos($_SERVER['REQUEST_URI'],'arhive') == false){
		               $tpl->newBlock('block_category_arhive');
		               $tpl->assign('id', $ctgr_id);
                	}

                    if(!in_array($request_id,array('83','84'))){
                        if($this->getTopCategoryByLevel($ctgr_id, 2) == 144) $this->show_diog = true;
                    if($this->show_diog){
                        $tpl->newBlock('diog_sort');
                        $tpl->assign('link', $ctgr_link.'&sort=d');
                    }
                    if($_SERVER['SERVER_NAME'] != "supra.ru")
                    {
                        $tpl->newBlock('product_sort');
                        if($_REQUEST['sort'] == 'price')
                        {
                            if(isset($_REQUEST['decrise']))$tpl->assign('linkP', $ctgr_link.'?sort=price');
                            else $tpl->assign('linkP', $ctgr_link.'?sort=price&decrise');
                            $tpl->assign('linkA', $ctgr_link.'?sort=alf');
                        }
                        elseif($_REQUEST['sort'] == 'alf')
                        {
                            if(isset($_REQUEST['decrise']))$tpl->assign('linkA', $ctgr_link.'?sort=alf');
                            else $tpl->assign('linkA', $ctgr_link.'?sort=alf&decrise');
                            $tpl->assign('linkP', $ctgr_link.'?sort=price');
                        }
                        else
                        {
                            $tpl->assign('linkP', $ctgr_link.'?sort=price');
                            $tpl->assign('linkA', $ctgr_link.'?sort=alf');
                        }
                    }

                    $request_start = $request_start > 1 ? (int)$request_start : 1;
                        $ids=array();
                        if($categ) if($categ['tematik'])     {
                            $arr=$db->getArrayOfResult("Select concrn_id From ".$server.$lang."_catalog_concerned_products Where prod_id=".$ctgr_id." AND type=2");
                            if(!empty($arr))foreach($arr as $k=>$v){

                                $ids[]=  $v['concrn_id'];
                            }
                            foreach($categs as $c=>$k){
                                if($k['tematik'])    {
                                    $arr=$db->getArrayOfResult("Select concrn_id From ".$server.$lang."_catalog_concerned_products Where prod_id=".$k['ctg_id']." AND type=2");
                                    if(!empty($arr))   foreach($arr as $k=>$v){

                                        $ids[]=  $v['concrn_id'];
                                    }
                                }

                            }
                        }


                    if (!$filter['valid_ctgr'] || ($filter['valid_ctgr'] && in_array($ctgr_id, $filter['valid_ctgr']))) {
                        $prods = $this->getProdsOfSubCategs($ctgr_id, $max_rows, $request_start, $categ['title'],$ids);
                        if($_SERVER['SERVER_NAME'] == "supra.ru" && isset($prods[0]['prod_id']) && isset($tv) && !isset($_GET['sort']) )
                        {
                            $prods=$this->sortForTVSupC($prods);
                        }
                    } else {
                        $prods = false;
                    }
                        $this->showProdList($prods);
            }

                    if (is_array($prods) && !empty($prods)) {
                        $shop_link = Core::formPageLink($CONFIG['shop_page_link'],$CONFIG['shop_page_address'], $lang, 1);
                        $tpl->gotoBlock('block_products');
                        $compare_count = 0;
                        if(isset($_SESSION['compare']))
                            $compare_count = count ( $_SESSION['compare'] );
                        $tpl->assign(
                            array(
                                'categ_title' => $categ['title'],
                                'categ_id' => $ctgr_id,
                                'compare_count' => $compare_count,
                                'compare_link' => $baseurl.'&action=compare',
                                'categ_img' => '/'.$CONFIG['catalog_ctg_img_path'].$categ['img'],
                                'categ_descr' => $categ['description'],
                                'baseurl' => $baseurl,
                                'cnt_compare_prod'	=> sizeof($_SESSION["compare_prod"])
                            )
                        );



                        $_baseurl = $baseurl;
                        $baseurl = $ctgr_link;

                        $main->_show_nav_string($this->table_prefix.'_catalog_products',
                            ' AND active = 1 AND category_id='.$ctgr_id,
                            'nav',
                            '',
                            $request_start,
                            $max_rows,
                            '',
                            '',
                            0,
                            0);
                        $baseurl = $_baseurl;
                    } elseif(empty($categs)) {
                        $tpl->newBlock('block_products_absent');
                    }

                    /* Лидеры продаж */
                    /*
				$prods = $this->getCategoriesLeades($request_id, $max_rows, $transurl);
				$this->showLeadersList($prods);
				*/
                    $this->main_title = $categ['ctg_name'];
                    //var_dump($categ);
                    $PAGE['alt_title'] = $categ['title'] ? $categ['title'] : $PAGE['alt_title'];
                    $this->addition_to_path = $_path;
                    

                    if ( !empty ( $main_cat_prods ) && empty ( $_GET['sort'] ) && $ctg_list == true )
                    {

                        $tpl->newBlock ('main_products');
                        $compare_count = count ( $_SESSION['compare'] );
                        $display = ( $compare_count == 0 ) ? 'display: none;' : 'display: block;';
                        $tpl->assign (array (  'compare_count' => $compare_count,
                            'compare_link'  => $baseurl.'&action=compare',
                            'display'       => $display ) );
                        if($_SERVER['SERVER_NAME'] == "supra.ru" && isset($tv))$main_cat_prods=$this->sortForTV($main_cat_prods);

                        foreach ( $main_cat_prods as $value )
                        {
                            $tpl->newBlock('item_main_product');
                            $link =  $this->getProdLink( $baseurl, $value['alias'] ? $value['alias'] : $value['id'], $value['id'],
                                $value['cat_alias'] ? $value['cat_alias'] : $value['cat_id']);
                            $checked = ( $this->isCompared( $value['id'] ) == true ) ? 'checked' : '';

                            $assign = array ( 'title'   => $value['product_title'],
                                'id'      => $value['id'],
                                'link'    => $link,
                                'img'     => '/'.$CONFIG['catalog_img_path'].$value['image_middle'],
                                'checked' => $checked,
                            );
                            $tpl->assign ( $assign );
                            if($value['price_1'] == 0 || ! (int)$value["product_availability"]){

                                $tpl->newBlock('mprice_no');
                                $tpl->assign('link', $link);
                                $tpl->assign('price', (int)$value['price_1']);
                            }elseif($value['price_2'] == 0){

                                $tpl->newBlock('mprice');
                                $tpl->assign('link', $link);
                                $tpl->assign('price', (int)$value['price_1']);
                            }else{
                                $tpl->newBlock('percent');
                                $tpl->newBlock('dprice');
                                $tpl->assign('link', $link);
                                $tpl->assign('price', (int)$value['price_1']);
                                $tpl->assign('percent', calcDiscount($value['price_1'], $value['price_2']));
                            }


                            if ( $this->show_diog )
                            {
                                $tpl->newBlock('dioganal');
                                $tpl->assign  ('diog', $this->getProductProperty($value["id"], 359) );
                            }



                        }

                    }




                    break;

                /* Лидеры продаж */
                case 'showleaders':
                    GLOBAL $request_id, $request_orderby, $request_parent, $request_start;
                    $ctgr_id = $properties[2] && 'all' != $properties[2]
                        ? $properties[2] : $request_id;
                    $tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                    $main->include_main_blocks($tpl_name, 'main');
                    $tpl->prepare();
                    $prods = $this->getCategoriesLeades($ctgr_id, $max_rows, $transurl);
                    $this->showLeadersList($prods);
                    $this->main_title = $this->_msg["Sales_leaders"];
                    break;

                /* Лидеры продаж */
                case 'novelties':
                    GLOBAL $request_id, $request_orderby, $request_parent, $request_start;
                    $ctgr_id = $properties[2] && 'all' != $properties[2]
                        ? $properties[2] : $request_id;
                    $tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                    $main->include_main_blocks($tpl_name, 'main');
                    $tpl->prepare();
                    $prods = $this->getNovelties($ctgr_id, $max_rows, $transurl);
                    $this->showProdList($prods);
                    $this->main_title = $this->_msg["Novelty"];
                    break;

                case "shwprd":
                    global $request_id, $request_prd_id, $request_ptp, $request_image_id, $request_print,$db,$db2;
                    $prod = $this->getProduct($request_id);
                    if($prod['archive']) $tpl_name = $this->module_name."_product_archive.html";
                    else $tpl_name = $properties[5] ? $properties[5] : $this->module_name."_product.html";

                    $main->include_main_blocks($tpl_name, "main");
                    $tpl->prepare();




                    //$prod = $this->getProduct($request_id);

                    $this->addLastViewed($prod['id']);
                    if(isShop()) $prod['product_description'] = $prod['product_description_shop'];
                    CatalogPrototype::mainFieldProp(array('act' => $action, 'ctgr_id' => $prod['category_id']));
                    if (! $prod['product_price']) {
                        $prod['product_price'] = $CONFIG['catalog_default_price'];
                    }
                    if (! $prod['product_price_1']) {
                        $prod['product_price_1'] = $CONFIG['catalog_default_price'];
                    }
                    if (! $prod['product_price_2']) {
                        $prod['product_price_2'] = $CONFIG['catalog_default_price'];
                    }
                    if (! $prod['product_price_3']) {
                        $prod['product_price_3'] = $CONFIG['catalog_default_price'];
                    }
                    if (! $prod['product_price_4']) {
                        $prod['product_price_4'] = $CONFIG['catalog_default_price'];
                    }
                    if (! $prod['product_price_5']) {
                        $prod['product_price_5'] = $CONFIG['catalog_default_price'];
                    }
                    if (!$prod || 0 == $prod['active']) {
                        return;
                    }
                    if ($prod["description"]) {
                        $PAGE['description'] = $prod["description"];
                    } elseif ($prod["ctg_description"]) {
                        $PAGE['description'] = strip_tags($prod["ctg_description"]);
                    }
                    else{
                        $PAGE['description'] = $prod["products_synonyms"].', '.$prod["product_categ"].' '.$prod["product_title"].' - '.$prod["product_description"];
                    }

                    if ($prod["keywords"]) {
                        $PAGE['keywords'] = $prod["keywords"];
                    } elseif ($prod["ctg_keywords"]) {
                        $PAGE['keywords'] = strip_tags($prod["ctg_keywords"]);
                    }
                    else{
                        $PAGE['keywords'] =   $prod["products_synonyms"].', '.$prod["product_categ"].' '. $prod["product_title"].', интернет-магазин, supra';
                    }

                    $PAGE['alt_title'] = $prod['seo_title'] ? $prod['seo_title'] : $prod["title"];

                    $shop_link = Core::formPageLink($CONFIG['shop_page_link'],$CONFIG['shop_page_address'], $lang, 1);

                    if (file_exists(RP."mod/comments")) {
                        require_once(RP."mod/comments/lib/class.CommentsPrototype.php");
                        $comm = new CommentsPrototype('only_create_object');
                        $comm->showProductComments($request_id, $CONFIG['catalog_comment_cnt']);
                    }

                    $this->get_nav_path($prod['category_id'], $baseurl, $request_id);
                    $tpl->newBlock('block_product');
   

                    $kkk = array(0,0,0,0);
 

                    if(!$prod['swf_file']){
                        $prod['swf_style'] = 'style="display:none;"';
                        $kkk[3] = 1;
                    }
        
                    if(!strlen(strip_tags($prod['product_description']))){
                        $prod['desc_style'] = 'style="display:none;"';
                        $kkk[1] = 1;
                    }
                    $id = (int)$request_id;
                    $shops = array();
                    $query = "SELECT * FROM  ".$server.$lang."_catalog_products_y WHERE prod_id=$id";
                    if(!$result = mysql_query($query)) die(mysql_error());

                    if(mysql_num_rows($result)){
                        //$tpl->newBlock('shops');
                        while($row = mysql_fetch_assoc($result)){
                            //$tpl->newBlock('shop');
                            $row['name'] = $shops[$row['shop_id']];
                            $shops[] = ($row);
                        }
                    }

                    if(!sizeof($shops)){
                        $prod['shop_style'] = 'style="display:none;"';
                        $kkk[5] = 1;
                    }

                    $tpl->assign($prod);

                    if( $this->getProductProperty($prod["id"],1172) == "Рубит кубиками")
                    {
                        $tpl->newBlock('rubit');
                        $tpl->assign(array("kub_disp"=>"block", "inv_disp" =>"none"));
                    }
                    if( $this->getProductProperty($prod["id"],1172) == "Инвертор")
                    {
                        $tpl->newBlock('rubit');
                        $tpl->assign(array("kub_disp"=>"none", "inv_disp" =>"block"));
                    }

                    if($permissions['e']){
                        $tpl->newBlock('block_product_edit');
                        $tpl->assign("product_edit_url", "http://suprashop.ru/"."admin.php?lang=".$CONFIG['admin_lang']."&name=catalog&id=".$prod['id']."&category_id=".$prod['category_id']."&rank=".$prod['rank']."&action=editproduct");
                    }




                    if(sizeof($shops)){
                        $tpl->newBlock('shops');
                        foreach($shops as $row){
                            $tpl->newBlock('shop');
                            //$row['name'] = $shops[$row['shop_id']];
                            //$shops[] = ($row);
                            $tpl->assign($row);
                        }
                    }
                    $actionsPrd=$this->get_product_action($prod['id']);
                    if(!empty($actionsPrd))
                    {
                        $tpl->newBlock('in_action');
                        $tpl->assign(array("product_title"  =>$prod['product_title']));
                        $iL=0;
                      foreach($actionsPrd as $val)
                      {
                          if(!$iL)
                          {
                              $tpl->newBlock('in_action_item_one');
                              $tpl->assign(array("title"  =>$val['title'],
                                  "link"   =>"/actions/?action=action&id=".$val['id']));
                              $iL++;
                          }
                          else{

                              $tpl->newBlock('in_action_item_other');
                              $other=1;
                              $tpl->assign(array("title"  =>$val['title'],
                                  "link"   =>"/actions/?action=action&id=".$val['id']));
                              $iL++;
                          }

                      }
                        if($iL > 1)
                        {

                            $tpl->newBlock('in_action_item_more');
                            $tpl->newBlock('in_action_item_more_end');
                        }

                    }

                    $db->query("SELECT * FROM ".$server.$lang."_faqtov_faqtov as  p
                                LEFT JOIN ".$server.$lang."_faqtov_ids as ids ON(p.id = ids.id_faq)
                                WHERE ids.id_tov = ".$prod['id']);
                    if($db->nf()!=0)
                    {
                        $tpl->newBlock("faq_isset");
                        $tpl->newBlock("faq");
                        while( $db->next_record())
                        {
                            $tpl->newBlock("accordion_lvl1");
                            $tpl->assign("faq_text",$db->f('faq'));
                            $tpl->assign("title",$db->f('title'));
                        }

                    }
                    $db->query("SELECT * FROM ".$server.$lang."_faqtov_faqtov as  p
                                LEFT JOIN ".$server.$lang."_faqtov_ids as ids ON(p.id = ids.id_faq)
                                WHERE ids.id_cat = ".$prod['category_id']);
                    if($db->nf()!=0) {
                        $tpl->newBlock("faq_isset");
                        $tpl->newBlock("faq");
                        while ($db->next_record()) {
                            $tpl->newBlock("accordion_lvl1");
                            $tpl->assign("faq_text", $db->f('faq'));
                            $tpl->assign("title", $db->f('title'));
                        }
                    }

                    if($this->getConfSup()){
                        $tpl->newBlock("analog_isset");
                        $tpl->newBlock("analog");
                    }
                    else{
	                    if($this->checkYa($prod['category_id'])){
		                    $this->yaMarket($prod['product_title']);
	                    }
	                    
                    }



                    if($prod['swf_file']){
                        $tpl->newBlock('swf');
                        $tpl->assign('swf_text', $prod['swf_text']);
                        $tpl->assign('swf', $prod['swf_file']);
                    }
                    if($prod['leader']) $tpl->newBlock('lead');
                    if($prod['archive']) $tpl->newBlock('archive');
                    if($prod['novelty']) $tpl->newBlock('new');
                    if ($prod["bonus"]) {
                        $tpl->newBlock('bonus');
                        $tpl->assign("bonus",$prod['bonus']); 
                    }
                    if($prod['price_2']>0) $tpl->newBlock('percent');

                    $ret = $this->showFiles((int)$request_id);
                    if(!$ret) $kkk[2] = 1;

                    if ($prod["product_description"]){
                        $tpl->newBlock('block_description');
                        $tpl->assign('product_description', $prod['product_description']);
                    }
                    if ($prod["product_ptype_title"]){
                        $tpl->newBlock('block_ptype_product');
                        $tpl->assign($prod);
                    }
                    if ($prod["product_group"]){
                        $tpl->newBlock('block_group');
                        $tpl->assign($prod);
                    }
                    if ($prod["product_code"]){
                        $tpl->newBlock('block_code');
                        $tpl->assign($prod);
                    }

                    //$prod['link'] = 'http://new.suprashop.ru'.$_SERVER['REQUEST_URI'];

                    if ($prod["product_availability"]){
                        $tpl->newBlock('block_availability');
                        $tpl->assign($prod);
                    }

                    $tpl->assign(array(
                        'block_product.add_to_cart_action'	=> $shop_link.($CONFIG['rewrite_mod'] ? '' : '&').'action=addtocart',
                        'block_product.action'				=> 'addtocart',
                        'block_product.link_category'		=> $this->getCtgrLink($transurl, $prod["ctg_alias"], $prod["category_id"]),
                    ));

                    if (empty($prod["product_availability"])
                        || $prod["product_availability"] == $this->_msg["no"]
                        || $prod["product_availability"] == $this->_msg["NO"]
                    ){
                        $tpl->newBlock('block_order_product');
                    }

                    if ($prod["product_ptype_link"]) {
                        $tpl->newBlock('block_product_ptype'.((strtolower(substr($prod["product_ptype_link"], 0, 7)) === "http://") ? '' : '_local').'_link');
                        $tpl->assign(array(
                            'ptype_name'	=> $prod["product_ptype_title"],
                            'ptype_link'	=> $prod["product_ptype_link"],
                        ));
                    } else if ($prod["product_ptype_title"]) {
                        $tpl->newBlock('block_product_ptype_without_link');
                        $tpl->assign('ptype_name', $prod["product_ptype_title"]);
                    }

                    /* Розничная цена */
                    if ($prod['price_1']>0 && $CONFIG['catalog_show_price_1']) {
                        $tpl->newBlock('block_price_1');
                        $tpl->assign('price_1', $prod['price_1']);
                    }
                    /* Партнёрская цена */
                    if ($prod['price_2']>0 && $CONFIG['catalog_show_price_1']) {
                        $tpl->newBlock('block_price_2');
                        $tpl->assign('price_2', $prod['price_2']);
                    }

                    /* Дилерская цена */
                    if ($prod['price_3']>0 && $CONFIG['catalog_show_price_3']) {
                        $tpl->newBlock('block_price_3');
                        $tpl->assign('price_3', $prod['price_3']);
                    }
                    /* Старая цена */
                    if ($prod['price_4']>0 && $CONFIG['catalog_show_price_4']) {
                        $tpl->newBlock('block_price_4');
                        $tpl->assign('price_4', $prod['price_4']);
                    }

                    if(intval($prod['product_price'])>0 && (int)$prod["product_availability"]) {
                        $tpl->newBlock('block_product_price');

                        $ar=$db->getArrayOfResult("SELECT sk.percent,sk.percent_p,c.product_title,c.category_id,p1.prod_id FROM sup_rus_skidki_prods AS p
                                                    LEFT JOIN sup_rus_skidki_prods AS p1 ON p1.skidka_id = p.skidka_id
                                                    LEFT JOIN sup_rus_catalog_products AS c ON c.id = p1.prod_id
                                                    JOIN sup_rus_actions_skidki AS a ON p.skidka_id = a.skidka_id
                                                    JOIN sup_rus_skidki AS sk ON sk.id = a.skidka_id
                                                    WHERE p.prod_id={$request_id} AND p1.is_gift=0 AND p1.is_if=0 AND sk.percent <> 0 AND sk.percent_p =0 ");
                        if(!empty($ar))
                        {
                            foreach($ar as $val)
                            {
                                $v=$prod['product_price'] - $prod['product_price']*$val['percent']/100;
                                $val_price='<span class="sale_prices">'.$prod['product_price'].' '.$prod['product_currency'].'</span><strong>'.$v.'</strong>';
                                $cur="";
                            }
                        }
                        else
                        {
                            $val_price=$prod['product_price'];
                            $cur=$prod['product_currency'];
                        }
                        $tpl->assign(array(
                            'product_price'  =>$val_price,
                            'product_currency' => $cur
                        ));
                    } else {
                        $tpl->newBlock('block_product_noprice');
                        $tpl->assign(array(
                            'product_price'  =>$prod['product_price']
                        ));
                    }

                    $prp = $this->getProductProperies($request_id, $prod['group_id']);
                    $prp_list = $prp['dynamic_properties'];
                    unset($prp['dynamic_properties']);

                    if (is_array($prp)) {
                        $tpl->newBlock('block_group_properties');
                        $tpl->assign_array('block_list_properties_test', $prp_list);
                        $i = 1;
                        $count = sizeof($prp);

                        foreach ($prp as $pk => $val) {
                            if(is_array($val['value'])){
                                $tpl->newBlock('block_group_properties_row');
                                $tpl->newBlock('block_group_delim');
                                $tpl->assign($val);
                                foreach($val['value'] as $v){
                                    $tpl->newBlock('block_group_properties_row');
                                    $tpl->newBlock('block_group_property');
                                    $tpl->assign(array(
                                        'title' => $v,
                                        'value' => '+'
                                    ));
                                }
                            }
                            else{
                                $tpl->newBlock('block_group_properties_row');
                                if (4 == $val['type']) {
                                    //echo $i.' - '.$count.'<br>'; 
                                    if($i != $count && ($prp[$pk]['type'] != 4)){ $tpl->newBlock('block_group_delim');
                                        $tpl->assign($val);
                                    }
                                } elseif($val['value']) {

                                    $tpl->newBlock('block_group_property');
                                    $tpl->assign($val);
                                }

                            }
                            $i++;
                        }
                    }else $kkk[0] = 1;

                    $act = false;
                    $tpl->gotoBlock('block_product');
                    if(!$kkk[1]){
                        $tpl->assign('desc', 'opened');
                        $tpl->assign('desc_active', 'active');
                        foreach($kkk as $kk => $vv){
                            if($kk == 0) $al = 'klient_foool';
                            if($kk == 1) $al = 'desc';
                            if($kk == 2) $al = 'files';
                            if($kk == 3) $al = 'swf';
                            if($kk == 5) $al = 'shop';
                            if($vv){
                                $tpl->assign($al.'_style', 'style="display:none"');
                            }
                        }
                    }else{
                        foreach($kkk as $kk => $vv){
                            if($kk == 0) $al = 'klient_foool';
                            if($kk == 1) $al = 'desc';
                            if($kk == 2) $al = 'files';
                            if($kk == 3) $al = 'swf';
                            if($kk == 5) $al = 'shop';
                            if(!$vv && !$act){
                                $tpl->assign($al, 'opened');
                                $tpl->assign($al.'_active', 'active');
                                $act = true;
                            }elseif($vv){
                                $tpl->assign($al.'_style', 'style="display:none"');
                            }
                        }
                    }
                    $tpl->newBlock('block_concernd_c');
                    $tpl->assign(array(
                        //'conf' => $this->getConfSup(false)
                    ));
                                  //$tpl->assign(array('a'=>$this->getConfSup())) ;
                    /* Связанные товары */
                    $concrn_list = $this->getConcernedProducts(0, $request_id, TRUE);
                    if ($concrn_list) {
                        $tpl->newBlock('block_concernd');
                        $tpl->assign(array(
                                'title' => $prod["product_title"]
                        ));
                        $this->showConcernedProducts($request_id, $concrn_list, 'concernd', '');
                    }

                    /* Аналоги */
                    $concrn_list = $this->getConcernedProducts(1, $request_id, TRUE, '', $prod['category_id']);
                    if ($concrn_list) {
                        $tpl->newBlock('block_analogues');
                        $tpl->assign(array(
                            'title' => $prod["product_title"]
                        ));
                        $this->showConcernedProducts($request_id, $concrn_list, 'analogues', '');
                    }

                    $this->showNewsFront($request_id, $prod['category_id']);
                    if ($CONFIG['catalog_count_hits']) {
                        Main::addHits($this->table_prefix."_catalog_products", $request_id);
                    }
                    $this->main_title = $prod['product_title'];

                    $this->get_list_with_rights(
                        "p.id, if(p.alias is not null and p.alias!='', p.alias, p.id) as alias", $this->table_prefix."_catalog_products AS p",
                        "p", 'category_id = '.$prod["category_id"].' AND active=1',
                        "", $CONFIG['catalog_prod_order_by']);
                    $prev_id = 0; $find = 0;
                    if ($db->nf()) {
                        while (!$find && $db->next_record()) {
                            if ($db->Record['id'] == $request_id) {
                                $find = true;
                                if ($db->next_record()) {
                                    $tpl->newBlock('block_next_prod');
                                    $tpl->assign('next_prod_link', $this->getProdLink($transurl, $db->Record['alias'], $db->Record['id'], $prod['ctg_alias']));
                                }
                                if ($prev) {
                                    $tpl->newBlock('block_prev_prod');
                                    $tpl->assign('prev_prod_link', $this->getProdLink($transurl, $prev[1], $prev[0], $prod['ctg_alias']));
                                }
                            }
                            $prev = array($db->Record['id'], $db->Record['alias']);
                        }
                    }
                    /* Изображения */
                    /*
				if ($prod['product_image'] && file_exists(RP.$CONFIG['catalog_img_path'].$prod['product_image'])) {
				    $tpl->newBlock('block_image_middle');
				    $tpl->assign(array(
				        'image_path' => "/".$CONFIG['catalog_img_path'].$prod['product_image'],
				        'big_image_path' => $prod['product_image_big'] && file_exists(RP.$CONFIG['catalog_img_path'].$prod['product_image_big'])
				            ? "/".$CONFIG['catalog_img_path'].$prod['product_image_big']
				            : $CONFIG['catalog_def_img_path']
				    ));
				} else {
				    $tpl->newBlock('block_image_middle_no');
				    $tpl->assign(array(
				        'big_image_path' => $prod['product_image_big'] && file_exists(RP.$CONFIG['catalog_img_path'].$prod['product_image_big'])
				            ? "/".$CONFIG['catalog_img_path'].$prod['product_image_big']
				            : $CONFIG['catalog_def_img_path']
				    ));
				}

				if ($prod['product_image_big'] && file_exists(RP.$CONFIG['catalog_img_path'].$prod['product_image_big'])) {
				    $tpl->newBlock('block_image_big');
				    $tpl->assign('image_path', "/".$CONFIG['catalog_img_path'].$prod['product_image_big']);
				} else {
				    $tpl->newBlock('block_image_big_no');
				}
				*/

                    $vids = array();
                    $query = 'SELECT * FROM '.$server.$lang.'_news WHERE source="'.mysql_real_escape_string($prod['product_code']).'" AND active=1';
                    //echo $query;
                    if(!$result = mysql_query($query)) die(mysql_error());
                    while($row = mysql_fetch_assoc($result)) $vids[] = $row;

                    $ret = $this->showAdditionalImgs($request_id, array($prod['product_image'], $prod['product_image_big']), $vids);





                    $db->query("SELECT * FROM ".$server.$lang."_catalog_products_video WHERE id_prod = ".$request_id);
                    if($db->nf()!=0 || count($vids)!=0) {
                        $tpl->newBlock("isset_videos");
                        while ($db->next_record()) {
                            $vid = $db->f('video');
                            $vid = str_replace('<p>', '', $vid);
                            $vid = str_replace('</p>', '', $vid);
                            $arr = explode('src="', $vid);
                            $arr = explode('"', $arr[1]);
                            $arr = explode('/', $arr[0]);
                            
                            $code = $arr[sizeof($arr)-1] ? $arr[sizeof($arr)-1] : $arr[sizeof($arr)-2];
                            if(strpos($code, "?"))
                                $code = substr($code, 0, strpos($code, "?"));


                            $src = 'http://img.youtube.com/vi/'.$code.'/mqdefault.jpg';
                            $tpl->newBlock("videos");
                            $tpl->assign(Array(
                                'src' => $src,
                                'html' => $vid,
                                'descr' => $db->f('desc')
                            ));
                        }

                        foreach($vids as $row){
                            if(!$ret) $tpl->newBlock('block_product_img_list');
                            $ret = true;
                            $tpl->newBlock('videos');
                            $row['body'] = str_replace('<p>', '', $row['body']);
                            $row['body'] = str_replace('</p>', '', $row['body']);
                            $arr = explode('src="', $row['body']);
                            $arr = explode('"', $arr[1]);
                            $arr = explode('/', $arr[0]);

                            $code = $arr[sizeof($arr)-1] ? $arr[sizeof($arr)-1] : $arr[sizeof($arr)-2];
                            if(strpos($code, "?"))
                                $code = substr($code, 0, strpos($code, "?"));
                            $src = 'http://img.youtube.com/vi/'.$code.'/mqdefault.jpg';
                            $tpl->assign('src', $src);
                            $tpl->assign('html', $row['body']);
                            $tpl->assign('descr', $row['announce_with_links']);
                        }
                        //var_dump($vids);

                    }
                    if (/*$prod["link"]*/ $prod["product_availability"] && $prod['price_1'] > 0.1 && !$prod['archive']){
                        // var_dump($_SERVER);
                        if($ret) $tpl->newBlock('block_link');
                        else $tpl->newBlock('block_link1');
                        //if($_GET['test']) var_dump($PAGE);
                        $tpl->assign('link', /*$prod['link']*/ 'http://suprashop.ru/catalog/'.$PAGE['params']);
                    }
                    $this->showGift($request_id);

                    $this->showProdOrderPropList($this->getProdOrderPropValIds($request_id), $prod['group_id']);
                    $PAGE['alt_title'] = $prod["product_title"];
                    break;

// Вывод дерева категорий
                case "showtree":
                    $tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                    $main->include_main_blocks($tpl_name, 'main');
                    $tpl->prepare();
                    $tree_list = $this->getSubTree($properties[2], $properties[3]);
                    $prop = CatalogPrototype::mainFieldProp();
                    $this->showTree($tree_list, $transurl, $prop ? $prop['act'] : false, $prop ? $prop['ctgr_id'] : false);
                    /*
				$html	= $this->get_tree_2($transurl, $properties[3]);
				$tpl->assign(array("sub_menu_two" => $html));
				*/
                    break;

// добавление продукта для сравнения
                case 'addcmpprd':
                    global $request_id, $_RESULT, $JsHttpRequest;
                    if ($prod = $this->getProduct(@$_REQUEST['id'])) {
                        if ($_SESSION["compare_gr_id"] && in_array($prod["group_id"], $_SESSION["compare_gr_id"])) {
                            $_SESSION["compare_prod"][] = $prod["id"];
                            $_SESSION["compare_gr_id"][] = $prod["group_id"];
                            $_RESULT = array(
                                "cnt" => @sizeof($_SESSION["compare_prod"]),
                                "prod_id" => $prod["id"],
                            );
                        } elseif (!$_SESSION["compare_gr_id"]) {
                            $_SESSION["compare_prod"][] = $prod["id"];
                            $_SESSION["compare_gr_id"][] = $prod["group_id"];
                            $_RESULT = array(
                                "cnt" => @sizeof($_SESSION["compare_prod"]),
                                "prod_id" => $prod["id"],
                            );
                        } else {
                            unset($_SESSION["compare_prod"]);
                            unset($_SESSION["compare_gr_id"]);
                            $_SESSION["compare_prod"][] = $prod["id"];
                            $_SESSION["compare_gr_id"][] = $prod["group_id"];
                            $_RESULT = array(
                                "cnt" => @sizeof($_SESSION["compare_prod"]),
                                "prod_id" => $prod["id"],
                            );
                        }
                        if ($JsHttpRequest) {
                            $_RESULT = array(
                                "cnt" => @sizeof($_SESSION["compare_prod"]),
                                "prod_id" => $prod["id"]
                            );
                            exit;
                        } else if (! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && 'XMLHttpRequest' == $_SERVER['HTTP_X_REQUESTED_WITH'] ) {
                            echo sizeof($_SESSION["compare_prod"]);
                            exit;
                        }
                    }
                    Module::go_back();
                    exit;
                    break;

// удаление продуктов для сравнения
                case 'delcmpprd':
                    global $request_id;
                    if (is_array($request_id)) {
                        foreach($request_id as $id) {
                            if ( false !== ($k = array_search($id, $_SESSION["compare_prod"])) ) {
                                unset($_SESSION["compare_prod"][$k]);
                            }
                        }
                    } elseif (false !== ($k = array_search($id, $_SESSION["compare_prod"])) ) {
                        unset($_SESSION["compare_prod"][$k]);
                    }
                    header('Location: '.$baseurl.'&action=compare');
                    exit;
                    break;

// вывод таблицы сравнения продуктов
                case 'compare':
                    global $request_compare, $request_id;

                    $PAGE["tpl_id"] = 5;
                    $main->include_main_blocks($this->module_name.'_compare.html', 'main');
                    $tpl->prepare();

                    if (is_array($request_compare)) {
                        foreach ($request_compare as $idx => $id) {
                            $_SESSION["compare_prod"][] = $id;
                        }
                    }
                    $this->get_nav_path($request_id, $baseurl);
                    $this->showComparedProducts($_SESSION["compare_prod"]);
                    break;

// Вывод 2х верхних уровней каталога

                case "showtoplevels":
                    global $db, $tpl,$new_supra, $request_id ;

                    $tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];

                    if($PAGE['field'] == 5 && $_GET['action'] == 'compare'){
                        $main->include_main_blocks('_empty.html', 'main');
                        $tpl->prepare();
                        break;
                    }


                    $main->include_main_blocks($tpl_name, 'main');
                    $tpl->prepare();

                    if($PAGE['field'] == 4){
                        $cats = array();
                        $query = 'SELECT title,id,clevel,alias, parent_id FROM '.$server.$lang.'_catalog_categories WHERE clevel>0 AND active=1 '.($new_supra ? ' AND nshow_new=0' : ' AND nshow=0').' ORDER BY rank';
                        if(!$result = mysql_query($query)) die(mysql_error());
                        $i = 0;
                        $j = 0;
                        while($row = mysql_fetch_assoc($result)){
                            $cats[] = $row;
                        }

                        foreach($cats as $cat){
                            if($cat['clevel'] == 1){
                                $tpl->newBlock('b1');
                                $tpl->assign(array(
                                    'pos' => $i++,
                                    'link' => $cat['id'] == 88 ? ($_REQUEST['new_s']?'http://suprashop.ru/catalog/_/':/*'http://suprasvet.ru/'*/'http://supra.ru/catalog/_/') : ('/catalog/'.($cat['alias'] ? $cat['alias'] : $cat['id']).'/'),
                                    'title' => htmlspecialchars($cat['title']),
                                    'active' => $PAGE['top_category'] == $cat['id'] ? 'active' : ''
                                ));

                                $sub = array();
                                foreach($cats as $cat1) if($cat1['parent_id'] == $cat['id']) $sub[] = $cat1;
                                if(sizeof($sub)){
                                    $tpl->newBlock('bb');
                                    foreach($sub as $cat1){
                                        $tpl->newBlock('b2');
                                        $tpl->assign(array(
                                            //'pos' => $i++,
                                            'link' => '/catalog/'.($cat1['alias'] ? $cat1['alias'] : $cat1['id']).'/',
                                            'title' => htmlspecialchars($cat1['title']),
                                            //'active' => $PAGE['top_category'] == $cat['id'] ? 'active' : ''
                                        ));
                                        $sub_sub = array();
                                        foreach($cats as $cat2) if($cat2['parent_id'] == $cat1['id']) $sub_sub[] = $cat2;
                                        if(sizeof($sub_sub)){
                                            $tpl->assign('class', 'dir dir'.($j++));
                                            $tpl->newBlock('bbb');
                                            foreach($sub_sub as $cat2){
                                                $tpl->newBlock('b3');
                                                $tpl->assign(array(
                                                    //'pos' => $i++,
                                                    'link' => '/catalog/'.($cat2['alias'] ? $cat2['alias'] : $cat2['id']).'/',
                                                    'title' => htmlspecialchars($cat2['title']),
                                                    //'active' => $PAGE['top_category'] == $cat['id'] ? 'active' : ''
                                                ));
                                            }
                                        }
                                    }
                                }
                            }
                            if($i==7){
                                $query = 'SELECT title,id,clevel,alias, parent_id FROM '.$server.$lang.'_catalog_categories WHERE tematik_menu=1 AND active=1 '.($new_supra ? ' AND nshow_new=0' : ' AND nshow=0').' ORDER BY rank';
                                if(!$result = mysql_query($query)) die(mysql_error());
                                while($row = mysql_fetch_assoc($result)){
                                    $catsTem= $row;
                                }
                                if(!empty($catsTem)){
                                    $tpl->newBlock('b1');
                                    $tpl->assign(array(
                                        'pos' => $i++,
                                        'link' => ('/catalog/'.($catsTem['alias'] ? $catsTem['alias'] : $catsTem['id']).'/'),
                                        'title' => htmlspecialchars($catsTem['title']),
                                        'active' => $PAGE['top_category'] == $catsTem['id'] ? 'active' : ''
                                    ));
                                }
                                continue;
                            }
                        }

                        break;
                    }

                    //$top_level = $this->getAllRootCategs();
                    $ctgr_id = $properties[2] >= 1 ? (int) $properties[2] : 1;

                    if($PAGE['field'] == 5) $ctgr_id = $PAGE['top_category'];

                    $groupping = $properties[3] >= 1 ? (int) $properties[3] : 0;
                    $top_level = $this->getSubCategsForRootCateg($ctgr_id, $transurl, false, ($_GET['action'] == 'search' && $PAGE['field'] == 5) ? 1 : 0, $PAGE['field'] == 5 ? false : true);
                    //if($PAGE['field'] == 5) var_dump($top_level);
                    $i = 0;
                    if($PAGE['field'] == 3) $top_level = array_slice($top_level,0,5);
                    foreach ($top_level as $key => $val) {
                        if (0 == $i || ($groupping && 0 == $i%$groupping)) {
                            $tpl->newBlock('block_groupping');
                            //$tpl->assign(array('conf'=>$this->getConfSup()))    ;
                        }

                        $is_sub = false;
                        $top_level_2 = $this->getSubCategsForRootCateg($val['ctg_id'], $transurl, false, 0, $PAGE['field'] == 5 ? false : true);
                        foreach ($top_level_2 as $k => $v) if($v['ctg_id'] == $PAGE['real_id']) $is_sub = true;
                        if($val['ctg_id'] == $PAGE['real_id']) $is_sub = true;

                        $tpl->newBlock('block_level1');
                        //$link_parent = $this->getCtgrLink($transurl, $val['alias'], $val['ctg_id']);
                        $tpl->assign(array(
                            'link' => $val['ctg_id'] == 88 ? ($_REQUEST['new_s']?'http://suprashop.ru/catalog/_/':/*'http://suprasvet.ru/'*/'http://supra.ru/catalog/_/') : ($val['ctg_id'] == 89 ? 'http://supraposuda.ru/' :  $val['ctg_link']),
                            'title' => htmlspecialchars($val['ctg_name']),
                            'id' => $val['ctg_id'],
                            'plus' => $top_level_2 ? ($is_sub ? 'minus' : 'plus') : '',
                            'activelink' => $is_sub ? 'activelink' : '',
                            'pos' => $i,
                            'active' => ($PAGE['top_category'] == $val['ctg_id']) ? 'active' : '',
                            'desc' => $val['ctg_descr']
                        ));
                        if($is_sub) $tpl->assign('style', 'style="display:block"');
                        ++$i;

                        if ($val['ctg_img'] && file_exists(RP.$CONFIG['catalog_ctg_img_path'].$val['ctg_img'])) {
                            $tpl->newBlock("block_level1_img");
                            $tpl->assign("path", "/".$CONFIG['catalog_ctg_img_path'].$val['ctg_img']);
                        } else {
                            $tpl->newBlock("block_level1_no_img");
                        }
                        if ($properties[3]) {
                            $tpl->newBlock("block_level1_cnt");
                            $tpl->assign("cnt", $this->getProdCount($val['ctg_id'], 1));
                        }
                        if($top_level_2) {
                            foreach ($top_level_2 as $k => $v) {
                                $tpl->newBlock('block_level2');
                                $no = "no";
                                if ($request_id == $v['ctg_id']) {
                                    $no = "";
                                }
                                $tpl->assign('pos', $i);
                                if($is_sub) $tpl->assign('style', 'style="display:block"');
                                $tpl->newBlock('block_level2_'.$no.'active');
                                $tpl->assign(array(
                                    'link' => $v['ctg_link'],
                                    'title' => $v['ctg_name'],
                                    'desc' => $v['ctg_descr']
                                ));
                                if ($v['ctg_img'] && file_exists(RP.$CONFIG['catalog_ctg_img_path'].$v['ctg_img'])) {
                                    $tpl->newBlock("block_level2_".$no."active_img");
                                    $tpl->assign("path", "/".$CONFIG['catalog_ctg_img_path'].$v['ctg_img']);
                                } else {
                                    $tpl->newBlock("block_level2_".$no."active_no_img");
                                }
                                if ($properties[3]) {
                                    $tpl->newBlock("block_level2_".$no."active_cnt");
                                    $tpl->assign("cnt", $this->getProdCount( $v['ctg_id'], 1));
                                }
                            }
                        }
                    }

                    break;

                /* Показать последние просмотренные товары */
                case 'last_viewed':
                    $tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                    $main->include_main_blocks($tpl_name, 'main');
                    $tpl->prepare();
                    $param = array('start' => $request_start);
                    if ($properties[1] >= 1) {
                        $param['cnt'] = $properties[1];
                    }
                    $prods = $this->getLastViewed($param);
                    $this->showProdList($prods);
                    $this->main_title = $this->_msg["Novelty"];
                    break;

// Default...
                default:
                    if (is_object($tpl)) $tpl->prepare();
                    return;
            }
//------------------------------- конец обработки действий с сайта -----------------------------//
        } else {
            $this->admin = true;
//------------------------------ обработка действий из админ части -----------------------------//
            // действия, которые пользователь имеет возможность задать для блока с данным модулем
            $this->block_module_actions	= array(
                //	'showarticul'   => $this->_msg["Show_assigned_goods"],
                'showleaders'	=> $this->_msg["Show_leaders"],
                'showtree'		=> $this->_msg["Show_catalog_tree"],
                'showsearchform'=> $this->_msg["Show_search_form"],
                'showcatmenu'	=> $this->_msg["Show_categories"],
                'showtoplevels'	=> $this->_msg["Show_top_levels"],
                'novelties'    	=> $this->_msg["Show_novetlies"],
                'last_viewed'   => $this->_msg["Show_last_viewed"],
            );
            // если поле блока ЯВЛЯЕТСЯ полем главного модуля:
            $this->block_main_module_actions = array(
                //	'assort_ctg'    => $this->_msg["Show_category"],
                'assortment'	=> $this->_msg["Show_catalog"],
                'showtree'		=> $this->_msg["Show_catalog_tree"],
                'showsearchform'=> $this->_msg["Show_search_form"],
                'showleaders'	=> $this->_msg["Show_leaders"],
                'showcatmenu'	=> $this->_msg["Show_categories"],
                'showtoplevels'	=> $this->_msg["Show_top_levels"],
                'novelties'    	=> $this->_msg["Show_novetlies"],
                'last_viewed'   => $this->_msg["Show_last_viewed"],
            );

            if (!isset($tpl)) $main->message_die(iconv("WINDOWS-1251", "UTF-8", 'Не подключен класс для работы с шаблонами'));

            if (empty($action)) $action = $this->admin_default_action;

            if ($this->actionExists($action)) {
                $this->doAction($action);
                return;
            }

            switch($action) {
                case 'prp':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_step;
                    $main->include_main_blocks('_properties.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Information"	=> $main->_msg["Information"],
                        "MSG_Value"			=> $main->_msg["Value"],
                        "MSG_Save"			=> $main->_msg["Save"],
                        "MSG_Cancel"		=> $main->_msg["Cancel"],
                    ));
                    if (!$request_step || $request_step == 1) {
                        $tpl->assign(array('form_action'	=> $baseurl,
                            'step'			=> 2,
                            'lang'			=> $lang,
                            'name'			=> $this->module_name,
                            'action'			=> 'prp'));
                        $prp_html = $main->getModuleProperties($this->module_name);
                        $this->main_title = $this->_msg["Controls"];
                    } elseif (2 == $request_step) {
                        $main->setModuleProperties($this->module_name, $_POST);
                        header('Location: '.$baseurl.'&action=prp&step=1');
                    }
                    break;

                case 'block_prp':
                    global $request_sub_action, $request_block_id;
                    $arr  = $this->getEditLink($request_sub_action, $request_block_id);
                    $prop_html = $this->getPropertyFields();
                    $js = '';
                    if (preg_match_all("/<script[^>]*>(.*)<\/script>/iuU", $prop_html, $matches)) {
                        foreach ($matches[1] as $key => $val) {
                            $js .= $val."\n";
                        }
                        $prop_html = preg_replace("/<script[^>]*>.*<\/script>/iuU", '', $prop_html);
                    }
                    $cont =
                        'var container = document.getElementById(\'target_span\');
                                container.innerHTML = "'.$prop_html.'";
                                material_id = "'.$arr['material_id'].'";
                                material_url = "'.$arr['material_url'].'";
                                changeAction();
                                setURL("property1");
                                '.str_replace(array('\\\\', '\"'), array('\\', '"'), $js);
                    header('Content-Length: '.strlen($cont));
                    echo $cont;
                    exit;
                    break;
                case 'uploadcsvweight':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);

                    if ($_FILES['csv_user_file']['tmp_name']) {
                        $results = $this->imp_csv_weight($_FILES['csv_user_file']['tmp_name']);
                    }
                    if ($results) {
                        //$results = 'Изменения произведены';
                    } else {
                        $results = 'Ошибка выполнения';
                    }
                    header('Location: '.$baseurl.'&action=imp-exp');

                    break;
// Редактирование свойств блока
                case 'properties':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $module_id, $request_field, $request_block_id, $title;
                    $main->include_main_blocks('_block_properties.html', 'main');
                    $tpl->prepare();

                    if ($main->show_actions($request_field, '', $this->block_main_module_actions, $this->block_module_actions)) {
                        $main->show_linked_pages($module_id, $request_field);
                    }
                    $tpl->assign(array( '_ROOT.title'		=> $title,
                        '_ROOT.username'	=> $_SESSION['session_login'],
                        '_ROOT.password'	=> $_SESSION['session_password'],
                        '_ROOT.name'		=> $this->module_name,
                        '_ROOT.lang'		=> $lang,
                        '_ROOT.block_id'	=> $request_block_id,
                    ));
                    $this->main_title	= $this->_msg["Block_properties"];
                    // Load JsHttpRequest backend.
                    require_once RP."inc/class.JsHttpRequest.php";
                    // Create main library object. You MUST specify page encoding!
                    $JsHttpRequest =& new JsHttpRequest("utf-8");
                    break;

// Показ каталога
                case 'imp-exp':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
                    $main->include_main_blocks_2($this->module_name.'_import_export.html', $this->tpl_path);
                    $tpl->prepare();
                    if ($_SESSION["info_block_name"]) {
                        $tpl->newBlock('block_info_box');
                        $tpl->newBlock($_SESSION["info_block_name"]);
                        $tpl->assign(Array(
                            "MSG_Data_download_success" => $this->_msg["Data_download_success"],
                            "MSG_Data_upload_success" => $this->_msg["Data_upload_success"],
                            'file' => $_SESSION["info_block_file"]
                        ));
                        unset($_SESSION["info_block_name"]);
                        unset($_SESSION["info_block_file"]);
                    }

                    $tpl->newBlock('block_upload_csv');
                    $tpl->assign(Array(
                        "MSG_Upload_CSV_file" => $this->_msg["Upload_CSV_file"],
                        "MSG_Upload" => $this->_msg["Upload"],
                        "MSG_Clear_catalogue_before" => $this->_msg["Clear_catalogue_before"],
                    ));
                    $tpl->assign(array(	'upload_csv_url'		=> $baseurl.'&action=uploadcsv',
                        'catalog_csv_separator'	=> $CONFIG['catalog_csv_separator']));
                    $tpl->newBlock('block_upload_csv_weight');
                    $tpl->assign(Array(
                        "MSG_Upload_CSV_file" => $this->_msg["Upload_CSV_file"],
                        "MSG_Upload" => $this->_msg["Upload"],
                        "MSG_Clear_catalogue_before" => $this->_msg["Clear_catalogue_before"],
                    ));
                    $tpl->assign(array(	'upload_csv_url'		=> $baseurl.'&action=uploadcsvweight',
                                           'catalog_csv_separator'	=> $CONFIG['catalog_csv_separator']));
                    if (is_callable('domxml_new_doc')) {
                        $tpl->newBlock('block_xml');
                        $tpl->newBlock('block_upload_xml');
                        $tpl->assign(Array(
                            "MSG_Upload_XML_file" => $this->_msg["Upload_XML_file"],
                            "MSG_Upload" => $this->_msg["Upload"],
                            "MSG_Clear_catalogue_before" => $this->_msg["Clear_catalogue_before"],
                        ));
                        $tpl->assign(array(	'upload_xml_url'		=> $baseurl.'&action=uploadxml',));
                        $tpl->newBlock('block_get_xml');
                        $tpl->assign(Array(
                            "MSG_Download_XML_file" => $this->_msg["Download_XML_file"],
                            "MSG_Download" => $this->_msg["Download"],
                        ));
                        $tpl->assign(array(	'upload_xml_url'	=> $baseurl.'&action=getxml',));
                    } else {
                        $tpl->newBlock('block_xml_no');
                    }


                    $tpl->newBlock('block_export_yml');
                    $tpl->assign(Array(
                        "MSG_Download_YML_file" => $this->_msg["Download_YML_file"],
                        "MSG_Download" => $this->_msg["Download"],
                    ));
                    $tpl->assign(array(
                        'export_yml_url'		=> $baseurl.'&action=exportyml',
                    ));
                    $ymlfile = $CONFIG["catalog_export_yml_path"] . $server . $lang . ".yml";
                    if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/" . $ymlfile))
                    {
                        $tpl->newBlock('block_export_yml_file');
                        $tpl->assign(Array(
                            "MSG_size" => $this->_msg["size"],
                        ));
                        $tpl->assign(array(
                            'file_yml_link'			=> $ymlfile,
                            'file_yml_name'			=> basename($ymlfile),
                            'file_yml_size'			=> round(filesize($ymlfile) / 1024, 1),
                        ));
                    }


                    $tpl->newBlock('block_export_yml_mobile');
                    $tpl->assign(Array(
                        "MSG_Download_YML_file" => $this->_msg["Download_YML_file"],
                        "MSG_Download" => $this->_msg["Download"],
                    ));
                    $tpl->assign(array(
                        'export_yml_url'		=> $baseurl.'&action=exportymlmobile',
                    ));
                    $ymlfile = $CONFIG["catalog_export_yml_path"] . "suprashop_mobile_version.yml";
                    //var_dump($ymlfile);
                    if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/" . $ymlfile))
                    {
                        $tpl->newBlock('block_export_yml_mobile_file');
                        $tpl->assign(Array(
                            "MSG_size" => $this->_msg["size"],
                        ));
                        $tpl->assign(array(
                            'file_yml_link'			=> $ymlfile,
                            'file_yml_name'			=> basename($ymlfile),
                            'file_yml_size'			=> round(filesize($ymlfile) / 1024, 1),
                        ));
                    }

                    $tpl->newBlock('block_export_yml1');
                    $tpl->assign(Array(
                        "MSG_Download_YML_file" => $this->_msg["Download_YML_file"],
                        "MSG_Download" => $this->_msg["Download"],
                    ));
                    $tpl->assign(array(
                        'export_yml_url'		=> $baseurl.'&action=exportyml&all=1',
                    ));
                    $ymlfile = $CONFIG["catalog_export_yml_path"] . $server . $lang . "_all.yml";
                    if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/" . $ymlfile))
                    {
                        $tpl->newBlock('block_export_yml1_file');
                        $tpl->assign(Array(
                            "MSG_size" => $this->_msg["size"],
                        ));
                        $tpl->assign(array(
                            'file_yml_link'			=> $ymlfile,
                            'file_yml_name'			=> basename($ymlfile),
                            'file_yml_size'			=> round(filesize($ymlfile) / 1024, 1),
                        ));
                    }

                    $tpl->newBlock('block_export_yml2');
                    $tpl->assign(Array(
                        "MSG_Download_YML_file" => $this->_msg["Download_YML_file"],
                        "MSG_Download" => $this->_msg["Download"],
                    ));
                    $tpl->assign(array(
                        'export_yml_url'		=> $baseurl.'&action=exportyml&vendor=1',
                    ));
                    $ymlfile = $CONFIG["catalog_export_yml_path"] . $server . $lang . "_vendor.yml";
                    if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/" . $ymlfile))
                    {
                        $tpl->newBlock('block_export_yml2_file');
                        $tpl->assign(Array(
                            "MSG_size" => $this->_msg["size"],
                        ));
                        $tpl->assign(array(
                            'file_yml_link'			=> $ymlfile,
                            'file_yml_name'			=> basename($ymlfile),
                            'file_yml_size'			=> round(filesize($ymlfile) / 1024, 1),
                        ));
                    }

                    $tpl->newBlock('block_get_csv');
                    $tpl->assign(Array(
                        "MSG_Download_CSV_file" => $this->_msg["Download_CSV_file"],
                        "MSG_Download" => $this->_msg["Download"],
                    ));
                    $tpl->assign(array(	'upload_csv_url'	=> $baseurl.'&action=getcsv',));

                    $aStructCSVFile	= new StructCSVFile();
                    $tpl->newBlock('block_format_csv');
                    $tpl->assign(Array(
                        "MSG_Category_before_goods" => $this->_msg["Category_before_goods"],
                        "MSG_Separator_is" => $this->_msg["Separator_is"],
                    ));
                    $astr = $aStructCSVFile->getFormatUploadCSV();
                    foreach($astr as $k => $v)
                    {
                        $tpl->newBlock("block_format_upload_csv");
                        $tpl->assign(Array(
                            "MSG_fields_order_in_CSV" => $this->_msg["fields_order_in_CSV"],
                        ));
                        $tpl->assign($v);
                    }
                    $tpl->newBlock('block_import_dimensions');
                    $tpl->assign(Array(
                        "MSG_Download_YML_file" => $this->_msg["Download_YML_file"],
                        "MSG_Download" => $this->_msg["Download"],
                    ));
                    $tpl->assign(array(
                        'url'		=> $baseurl.'&action=uploadxmldimensions',
                    ));
                    $this->main_title	= $this->_msg["Import_Export"];
                    break;

                case 'getxml':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);

                    $doc = domxml_new_doc("1.0");
                    $root = $doc->add_root("catalog");
                    $root->set_attribute("date", gmdate('d.m.Y H:i:s').date("O"));
                    $root->set_attribute("lang", $lang);
                    $this->get_groups_XML($doc, $root);
                    $ptypes = $this->get_ptypes_XML($doc, $root);
                    $this->get_category_XML($doc, $root, $ptypes);
                    $out = $doc->dump_mem(TRUE, 'UTF-8');

                    $file	= $CONFIG['catalog_export_path'].'catalog_'.gmdate('Ymd_His').substr(date("O"), 0, 3).'.xml';
                    if($handle = fopen(RP.$file, 'w'))
                    {
                        fwrite($handle, $out);
                        fclose($handle);
                    }
                    $_SESSION["info_block_name"] = "block_info_download_success";
                    $_SESSION["info_block_file"] = "/".$file;

                    header('Location: '.$baseurl.'&action=imp-exp');
                    break;

                case 'uploadxml':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    if ($_FILES['xml_user_file']['tmp_name'] && ($dom = domxml_open_file($_FILES['xml_user_file']['tmp_name']))) {
                        $this->doImportXMLData($dom);
                    }
                    $_SESSION["info_block_name"] = "block_info_upload_success";

                    header('Location: '.$baseurl.'&action=imp-exp');
                    break;

                case 'uploadxmldimensions':
                    global $db;
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    if (!$_FILES['file']['tmp_name']) {
                        break;
                    }
                    require_once RP . '/inc/PHPExcel.php';
                    require_once RP . '/inc/PHPExcel/IOFactory.php';
                    $objPHPExcel = PHPExcel_IOFactory::load($_FILES['file']['tmp_name']);
                    if(!$objPHPExcel){
                        break;
                    }

                    $sheet = $objPHPExcel->getActiveSheet();

                    // Получили строки и обойдем их в цикле
                    $rowIterator = $sheet->getRowIterator();
                    foreach ($rowIterator as $k => $row) {
                        // Получили ячейки текущей строки и обойдем их в цикле
                        $cellIterator = $row->getCellIterator();
                        if($k==1){
                            continue;
                        }

                        foreach ($cellIterator as $i => $cell) {

                           if($i == 0 )   echo "<td>" .$i. $cell->getCalculatedValue() . "</td>";
                           if($i == 1){
                               $title = $cell->getCalculatedValue();

                           }
                           if($i == 2) echo "<td>" .$i. $cell->getCalculatedValue() . "</td>";
                           if($i == 3){
                               $weight = $cell->getCalculatedValue();
                           }
                           if($i == 4)  {
                               $d = round($cell->getCalculatedValue()* 100);
                           }
                           if($i == 5){
                               $sh = round($cell->getCalculatedValue()* 100);
                           }
                           if($i == 6)  {
                               $v = round($cell->getCalculatedValue() * 100);
                           }

                        }

                        $dimensions=$d.'x'.$sh.'x'.$v;
                        $weight=$weight*1000;

                        if(!$d && !$sh && !$v){
                            $db->query("Update sup_rus_catalog_products SET weight='{$weight}' Where product_title='".$title."'");
                        }else{
                            $db->query("Update sup_rus_catalog_products SET weight='{$weight}',dimensions='{$dimensions}' Where product_title='".$title."'");
                        }

                    }

                    $_SESSION["info_block_name"] = "block_info_upload_success";

                    header('Location: '.$baseurl.'&action=imp-exp');
                    break;

                case 'exportyml':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);

                    if($_GET['vendor']) $this->generateVendorYMLData();
                    else $this->generateYMLData($_REQUEST['all']);

                    header('Location: '.$baseurl.'&action=imp-exp');
                    break;

                case 'exportymlmobile':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);

                    if($_GET['vendor']) $this->generateVendorYMLDataMobile();
                    else $this->generateVendorYMLDataMobile($_REQUEST['all']);

                    header('Location: '.$baseurl.'&action=imp-exp');
                    break;

                case 'getcsv':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
                    $aParseCSVFormat	= new ParseCSVFormat(new StructCSVFile());
                    $aParseCSVFormat->setSeparator($CONFIG['catalog_csv_separator']);
                    $aExportData		= new ExportData($aParseCSVFormat, $catalog);
                    $aExportData->setDescriptorDB($db);
                    $aExportData->setOutputStream($this->module_name);
                    $aExportData->doExportData();
                    break;

                case 'uploadcsv':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    if ($_FILES['csv_user_file']['tmp_name']) {
                        $aParseCSVFormat	= new ParseCSVFormat(new StructCSVFile(), $_FILES['csv_user_file']);

                        $aParseCSVFormat->setSeparator($CONFIG['catalog_csv_separator']);
                        $aImportData = new ImportData($aParseCSVFormat, $catalog, $_POST['clear']);
                        $aImportData->setDescriptorDB($db);
                        $aImportData->doImportData();
                        $CDBT = new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
                        $CDBT->fixTree();
                        //$CDBT->treeReload();
                    }

                    header('Location: '.$baseurl.'&action=imp-exp');
                    break;

                case "showrates":
                    if (!$permissions["r"])
                    {
                        $main->message_access_denied($this->module_name, $action);
                    }

                    //print_r($_SESSION['catalog_rates']);

                    // В сессии уже записан ядром массив с курсами валют ($_SESSION['catalog_rates']), переопределим его.
                    $rates = false;
                    if (isset($_REQUEST['get_cbrf']))
                    {
                        $rates = $this->get_central_bank_rates('http://www.cbr.ru/scripts/XML_daily.asp');
                    }

                    $main->include_main_blocks_2($this->module_name."_rates.html", $this->tpl_path);
                    $tpl->prepare();


                    $tpl->assign(Array(
                        "MSG_required_fields" => $this->_msg["required_fields"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                        "MSG_cbr_sync" => $this->_msg["cbr_sync"],
                    ));

                    $tpl->assign(array(
                        "_ROOT.form_action"	=>	$baseurl.'&action=updaterates',
                        "_ROOT.cbr_sync"	=>	$baseurl.'&get_cbrf=1&action=showrates'
                    ));

                    $main->get_rates(1);

                    // Если используем курс ЦБ РФ, то перекрываем параметры курса, установленные в $main->get_rates(1);
                    if ($rates)
                    {
                        $tpl->assign($rates);
                    }

                    $this->main_title = $this->_msg["Currencies"];

                    break;

// Обновление курсов валют
                case "updaterates":
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_rates;

                    $this->set_rates($request_rates);
                    header('Location: '.$baseurl.'&action=showrates');
                    break;

// Полностью ОЧпСТпТЬ каталог от категорий (продукты не затрагиваются. Требуется для инициализации каталога,
// т.е. использоваться должно 1 раз в жизни при установке)
                case "clear":
                    if (!$permissions["e"] AND !$permissions["d"]) $main->message_access_denied($this->module_name, $action);
                    $CDBT = new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
                    $CDBT->clear(array("parent_id"=>"0", "active"=>"1", "title"=>"Корень каталога", "description"=>"Это неоперабельный элемент - корень каталога.", "rank"=>"0"));
                    header("Location: $baseurl&action=assortment");
                    break;
// Вывод дерева категорий
                case 'showtree':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
                    global $request_parent;
                    $main->include_main_blocks_2($this->module_name.'_cat_tree.html', $this->tpl_path);
                    $tpl->prepare();
                    //$this->create_tree($request_parent, 'tree', '', $baseurl, 1);
                    $tpl->assign('catalog', $this->get_tree('tree', $request_parent, $transurl, 1));
                    $this->main_title = $this->_msg["Catalogue_tree"];
                    break;
                case 'setfield':
                    $id = (int)$_GET['id'];
                    $v = (int)$_GET['v'];

                    $query = 'UPDATE '.$server.$lang.'_catalog_products SET '.$_GET['field'].'='.$v.' WHERE category_id='.$id;
                    if(!mysql_query($query)) die(mysql_error());
                    header('Location: '.$_SERVER['HTTP_REFERER']);
                    exit;

                    break;
// Показ каталога
                case 'assortment':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_start, $request_order;
                    $id		= (int)$request_id;
                    $start	= $request_start>1 ? (int)$request_start : 1;
                    if ($id == 0)	$id		= 1;
                    $ctg_owners = $this->get_category_owners($id);
                    $main->include_main_blocks_2($this->module_name.'_assortment.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign('lang',  $lang);
                    $tpl->assign('id',  $id);
                    $level = $this->show_nav_path($id, $baseurl);
                    $catZ=$this->getCategory($id)       ;
                    $this->show_add_to_current($level, $id ,$catZ['tematik']);
                    $this->show_categories($id, null, null, $ctg_owners);

                    $pages_cnt = $this->show_products($request_id, $baseurl, $CONFIG['catalog_max_rows'], $start, $request_order, $ctg_owners);
                    if ($pages_cnt) {
                        $order_link = $request_order ? "&order=".$request_order : "";
                        $main->_show_nav_block($pages_cnt, null, "action=".$action."&id=".$id.$order_link, $start);
                        $tpl->assign(Array(
                            "MSG_Pages" => $this->_msg["Pages"],
                        ));
                    }
                    $this->main_title	= $this->_msg["Categories_goods"];


                    break;
// Показ списка товаров с русскими символами в названии
                case 'russian_titles':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_start, $request_order;
                    $id		= (int)$request_id;
                    $start	= $request_start>1 ? (int)$request_start : 1;
                    if ($id == 0)	$id		= 1;
                    $products = $this->getProductsWithRussianTitles();

                    $main->include_main_blocks_2($this->module_name.'_russian_titles.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign('lang',  $lang);
                    $tpl->assign('id',  $id);
                    $level = $this->show_nav_path($id, $baseurl);
                    $this->show_russian_products($products);
                    $this->main_title	= $this->_msg["Categories_goods"];


                    break;
                //Показ списка товаров для которых загружен 3d ролик
                case 'list3d':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_start, $request_order;
                    $id		= (int)$request_id;
                    $start	= $request_start>1 ? (int)$request_start : 1;
                    if ($id == 0)	$id		= 1;
                    $products = $this->getProductsWith3dVideo();

                    $main->include_main_blocks_2($this->module_name.'_3d_video.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign('lang',  $lang);
                    $tpl->assign('id',  $id);
                    $level = $this->show_nav_path($id, $baseurl);
                    $this->show_3dlist_products($products);
                    $this->main_title	= $this->_msg["Categories_goods"];
                    break;


                    //Показ списка товаров для которых загружен 3d ролик
                case 'listoff':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_start, $request_order;
                    $id		= (int)$request_id;
                    $start	= $request_start>1 ? (int)$request_start : 1;
                    if ($id == 0)	$id		= 1;
                    $products = $this->getProductsOff();

                    $main->include_main_blocks_2($this->module_name.'_3d_video.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign('lang',  $lang);
                    $tpl->assign('id',  $id);
                    $level = $this->show_nav_path($id, $baseurl);
                    $this->show_3dlist_products($products);
                    $this->main_title	= $this->_msg["Categories_goods"];
                    break;

                case 'listdimensions':
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);

                    $products = $this->getProductsDimensions();

                    $main->include_main_blocks_2($this->module_name.'_3d_video.html', $this->tpl_path);
                    $tpl->prepare();
                   // $level = $this->show_nav_path($id, $baseurl);
                    $this->show_3dlist_products($products);
                    $this->main_title	= $this->_msg["Categories_goods"];
                    break;
					    //Показ списка товаров для которых загружен 3d ролик
                case 'promo':
                if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_time, $request_start, $request_promo;
				$main->include_main_blocks_2($this->module_name.'_promo.html', $this->tpl_path);
				$tpl->prepare();

		        $this->getSelectListOfTime(@$request_time, '&action='.$action);
				$tpl->assign(Array(
					"block_period.MSG_Enter_period" => $this->_msg["Enter_period"],
					"block_period.MSG_from" => $this->_msg["from"],
					"block_period.MSG_to" => $this->_msg["to"],
					"block_period.MSG_Show" => $this->_msg["Show"],
					"block_period.MSG_Reset" => $this->_msg["Reset"],
				));
		        $request_start = @$request_start > 1 ? (int)$request_start : 1;
		        
		        $tpl->assign_array('block_sites',	$this->getReferringHosts(@$request_time, $request_start,true, $request_promo ));

		        $tables = $this->getTableByDate(@$request_time);
		        $link = 'action='.$action.(@$request_time ? '&time='.@$request_time : '').($request_promo ? '&promo='.$request_promo : '');
			$where = $tables['where'] ? ' AND '.$tables['where']. ' AND LENGTH(promo_code) ' : ' AND LENGTH(promo_code)';
			$where .= $request_promo ? ' AND promo_code ="'.$request_promo.'"' : '';
		$main->_show_nav_string_2('promo_code','sup_rus_shop_orders', $where,
												  '', $link, $request_start, 10, '', '');
				$tpl->assign(Array(
					"nav.MSG_Pages" => $this->_msg["Pages"],
				));
                    $this->main_title	= 'Информация про промокодам';
                    break;
// Результат поиска
                case 'search':
                    if (!$permissions['r']) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $request_id, $request_start, $request_search, $request_order;

                    $id    = (int)$request_id ? (int)$request_id : 0;
                    $start = (int)$request_start ? (int)$request_start : 1;

                    $main->include_main_blocks_2($this->module_name.'_assortment.html', $this->tpl_path);
                    $tpl->prepare();

                    $ctg_owners = $this->get_category_owners($id);
                    $level = $this->show_nav_path($id, $baseurl);
                    $this->show_add_to_current($level, $id);
                    $this->show_categories($id, null, null, $ctg_owners);

                    $request_search = trim($request_search);
                    if ($search = preg_replace("/\s+/", "&nbsp;", $request_search)) {
                        $search = explode("&nbsp;", $search);
                        $where = '';
                        foreach ($search as $word) {
                            $where .= "product_title LIKE '%".My_Sql::escape($word)."%' AND ";
                        }
                        $where = substr($where, 0, -5);
                        if ($where
                            && $this->show_search_result($id,
                                $request_search,
                                $baseurl,
                                $CONFIG['catalog_max_rows'],
                                $start,
                                $request_order)
                        ) {
                            if($id)$catS=' AND '.'category_id = '.$id;
                            else $catS='';
                            $where = $catS.' AND ('.$where.')';
                            $order_link = $request_order ? "&order=".$request_order : "";
                            $main->_show_nav_string($this->table_prefix.'_catalog_products',
                                $where,
                                '',
                                'action='.$action.'&id='.$id."&search=".trim($request_search).$order_link,
                                $start,
                                $CONFIG['catalog_max_rows'],
                                $CONFIG['catalog_prod_order_by'],
                                '',
                                1,
                                0);
                        } else {
                            $tpl->newBlock('block_no_search');
                            $tpl->assign(Array(
                                "MSG_Not_found" => $this->_msg["Not_found"],
                                "MSG_Go_back" => $this->_msg["Go_back"],
                            ));
                        }

                    }

                    $this->main_title	= $this->_msg["Categories_goods"];
                    break;

                case 'searchprod':
                    if (!$permissions['r']) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $request_id, $request_start, $request_search, $request_order;

                    //$id    = (int)$request_id ? (int)$request_id : 1;
                    $start = (int)$request_start ? (int)$request_start : 1;

                    $main->include_main_blocks_2($this->module_name.'_assortment.html', $this->tpl_path);
                    $tpl->prepare();

                    //$this->get_nav_path('49', $baseurl);
                    //$ctg_owners = $this->get_category_owners($id);
                    $level = $this->show_nav_path('1', $baseurl);
                    $this->show_add_to_current($level, '1');
                    //$this->show_categories($id, null, null, $ctg_owners);

                    $request_search = trim($request_search);
                    if ($search = preg_replace("/\s+/", "&nbsp;", $request_search)) {
                        $search = explode("&nbsp;", $search);
                        $where = '';
                        foreach ($search as $word) {
                            $where .= "product_title LIKE '%".My_Sql::escape($word)."%' AND ";
                        }
                        $where = substr($where, 0, -5);
                        if ($where
                            && $this->show_search_result_prod($request_search,
                                $baseurl,
                                $CONFIG['catalog_max_rows'],
                                $start,
                                $request_order)
                        ) {
                            $where = ' AND ('.$where.')';
                            $order_link = $request_order ? "&order=".$request_order : "";
                            $main->_show_nav_string($this->table_prefix.'_catalog_products',
                                $where,
                                '',
                                'action='.$action."&search=".trim($request_search).$order_link,
                                $start,
                                $CONFIG['catalog_max_rows'],
                                $CONFIG['catalog_prod_order_by'],
                                '',
                                1,
                                0);
                        } else {
                            $tpl->newBlock('block_no_search');
                            $tpl->assign(Array(
                                "MSG_Not_found" => $this->_msg["Not_found"],
                                "MSG_Go_back" => $this->_msg["Go_back"],
                            ));
                        }

                    }

                    $this->main_title	= $this->_msg["Categories_goods"];
                    break;

// Перемещение элементов каталога
                case 'changecatgr':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_new_ctgr, $request_prods, $request_new_ctgr, $request_category_id;

                    if ($this->changCatgrOfProducts($request_new_ctgr, $request_prods)) {
                        header("Location: $baseurl&action=assortment&id=".(int)$request_new_ctgr);
                    } else {
                        header("Location: $baseurl&action=assortment&id=".(int)$request_category_id);
                    }
                    exit;

                    break;
                case 'changemshow':
                    if($_GET['f']) $field = 'nshow_new';
                    else $field = 'nshow';
                    $query = 'UPDATE '.$server.$lang.'_catalog_categories SET '.$field.'='.(int)$_GET['mshow'].' WHERE id='.(int)$_GET['id'];
                    if(!mysql_query($query)) die(mysql_error());
                    header('Location: '.$_SERVER['HTTP_REFERER']);
                    exit;
				case 'changemshowya':

                    $query = 'UPDATE '.$server.$lang.'_catalog_categories SET nshow_ya='.(int)$_GET['mshow2'].' WHERE id='.(int)$_GET['id'];
                    if(!mysql_query($query)) die(mysql_error());
                    header('Location: '.$_SERVER['HTTP_REFERER']);
                    exit;
                case 'changetematik':
                    $field = 'tematik';
                    $query = 'UPDATE '.$server.$lang.'_catalog_categories SET '.$field.'='.(int)$_GET['tematik'].' WHERE id='.(int)$_GET['id'];
                    if(!mysql_query($query)) die(mysql_error());
                    header('Location: '.$_SERVER['HTTP_REFERER']);
                    exit;
                case 'changetematikmenu':
                    $field = 'tematik_menu';
                    $query = 'UPDATE '.$server.$lang.'_catalog_categories SET '.$field.'=0 WHERE '.$field.'=1';
                    if(!mysql_query($query)) die(mysql_error());
                    $query = 'UPDATE '.$server.$lang.'_catalog_categories SET '.$field.'=1 WHERE id='.(int)$_GET['id'];
                    if(!mysql_query($query)) die(mysql_error());
                    header('Location: '.$_SERVER['HTTP_REFERER']);
                    exit;

                    break;
// Добавление категории
                case "addcat":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_step, $request_id, $request_start, $request_block_id;
                    $ctg_owners = $this->get_category_owners($request_id);
                    if ($request_id > 1) {
                        list($allow, $ctg_rights) = $this->test_item_rights("e", "id", $this->table_prefix."_catalog_categories",
                            $request_id, false, $ctg_owners, true);
                        if (!$allow) $main->message_access_denied($this->module_name, $action);
                    } else {
                        $ctg_rights = $CONFIG["catalog_default_rights"];
                    }

                    $main->include_main_blocks_2($this->module_name."_edit_category.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Category_name_not_filled" => $this->_msg["Category_name_not_filled"],
                        "MSG_Name" => $this->_msg["Name"],
                        "MSG_Goods_group_default" => $this->_msg["Goods_group_default"],
                        "MSG_Select_group" => $this->_msg["Select_group"],
                        "MSG_Description" => $this->_msg["Description"],
                        "MSG_Delete" => $this->_msg["Delete"],
                        "MSG_Page_description" => $this->_msg["Page_description"],
                        "MSG_Keywords" => $this->_msg["Keywords"],
                        "MSG_required_fields" => $this->_msg["required_fields"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                        "MSG_Alias" => $this->_msg["Alias"],
                        "MSG_Alias_Info" => $this->_msg["Alias_Info"],
                        "MSG_Set_other_link" => $this->_msg['Set_other_link'],
                        'baseurl' => $baseurl
                    ));
                    if (!$request_step || $request_step == 1) {
                        $tpl->assign(array(
                            "_ROOT.form_action"		=>	"$baseurl&action=addcat&start=$request_start",
                            "_ROOT.referer"			=>	$_SERVER['HTTP_REFERER'],
                            "_ROOT.id"				=>	$request_id,
                            "_ROOT.cancel_link"		=>	"javascript:history.back();",
                            "_ROOT.str_path"		=>	"/admin/?name=catalog&action=str",
                            "action"                =>  $action
                        ));
                        $grp = $this->get_all_groups(0);
                        $tpl->assign_array('block_group', $grp);
                        $tpl->newBlock('block_ctg_image');
                        $tpl->assign(array(
                            image_title				=>	$this->_msg["Category_image"],
                            field_name				=>	'img',
                            "MSG_Delete" => $this->_msg["Delete"],
                        ));

                        if (file_exists(RP."mod/comments")) {
                            require_once(RP."mod/comments/lib/class.CommentsPrototype.php");
                            $comm = new CommentsPrototype('only_create_object');
                            $comm->showCommentableForm();
                        }
                        $this->showNews();
                        $this->main_title = $this->_msg["Create_new_category"];
                        break;
                    } else if ($request_step == 2) {
                        $block_id = intval(substr($request_block_id, 10));
                        $lid = $this->add_catalog($ctg_rights, $block_id);
                        if ($lid) @$cache->delete_cache_files();
                        $this->storeNews($lid);
                        header("Location: $baseurl&action=assortment&id=$request_id");
                        exit;
                        break;
                    }
                    break;

// Редактирование категории.
                case "editcat":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_step, $request_start, $request_id, $request_parent;
                    $ctg_owners = $this->get_category_owners($request_parent);
                    if (!$this->test_item_rights("e", "id", $this->table_prefix."_catalog_categories", $request_id, false, $ctg_owners))
                        $main->message_access_denied($this->module_name, $action);

                    $main->include_main_blocks_2($this->module_name."_edit_category.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Category_name_not_filled" => $this->_msg["Category_name_not_filled"],
                        "MSG_Name" => $this->_msg["Name"],
                        "MSG_Goods_group_default" => $this->_msg["Goods_group_default"],
                        "MSG_Select_group" => $this->_msg["Select_group"],
                        "MSG_Description" => $this->_msg["Description"],
                        "MSG_Delete" => $this->_msg["Delete"],
                        "MSG_Page_description" => $this->_msg["Page_description"],
                        "MSG_Keywords" => $this->_msg["Keywords"],
                        "MSG_required_fields" => $this->_msg["required_fields"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                        "MSG_Alias" => $this->_msg["Alias"],
                        "MSG_Alias_Info" => $this->_msg["Alias_Info"],
                        "MSG_Set_other_link" => $this->_msg['Set_other_link'],
                        'baseurl' => $baseurl
                    ));
                    if (!$request_step || $request_step == 1) {
                        $tpl->assign(array(
                            "_ROOT.form_action"		=>	"$baseurl&action=editcat&start=$request_start",
                            "_ROOT.referer"			=>	$_SERVER['HTTP_REFERER'],
                            "_ROOT.id"				=>	$request_id,
                            "_ROOT.cancel_link"		=>	"javascript:history.back();",
                            "_ROOT.str_path"		=>	"/admin/?name=catalog&action=str",
                            "action"                =>  $action
                        ));

                        $this->main_title = $this->_msg["Edit_category"];
                        $ctgr = $this->show_category($request_id, $baseurl);
                        if (! $ctgr) { // если нет такой записи то редирект
                            header("Location: $baseurl&action=assortment");
                            exit;
                        }
                        if (file_exists(RP."mod/comments")) {
                            require_once(RP."mod/comments/lib/class.CommentsPrototype.php");
                            $comm = new CommentsPrototype('only_create_object');
                            $comm->showCommentableForm($ctgr->commentable);
                        }
                        $this->showNews($this->getNewsIds($request_id));
                        break;
                    } else if ($request_step == 2) {
                        if ($this->edit_catalog()) @$cache->delete_cache_files();
                        $CDBT	=	new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
                        $parent = $CDBT->getParent($request_id);
                        $this->storeNews($request_id);
                        header('Location: '.$baseurl.'&action=assortment&id='.$parent[0]);
                        exit;
                        break;
                    }
                    break;

// Удаление категории с вложениями.
                case "delcat":
                    if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
                    global $request_step, $request_start, $request_id, $request_parent;
                    $ctg_owners = $this->get_category_owners($request_parent);
                    if (!$this->test_item_rights("d", "id", $this->table_prefix."_catalog_categories", $request_id, false, $ctg_owners))
                        $main->message_access_denied($this->module_name, $action);

                    // Удаление категории, первый шаг
                    if (!$request_step || $request_step == 1) {
                        $main->include_main_blocks_2($this->module_name.'_del_category.html', $this->tpl_path);
                        $tpl->prepare();
                        $tpl->assign(array(
                            "MSG_Removing_category" => $this->_msg["Removing_category"],
                            "MSG_deletes_its_goods" => $this->_msg["deletes_its_goods"],
                            "MSG_Confirm_delete_category" => $this->_msg["Confirm_delete_category"],
                            "MSG_Delete" => $this->_msg["Delete"],
                            "MSG_Cancel" => $this->_msg["Cancel"],
                            '_ROOT.form_action'		=>	 $baseurl.'&action=delcat&start='.$request_start,
                            '_ROOT.id'				=>	 $request_id,
                            '_ROOT.parent'			=>	 $request_parent,
                            '_ROOT.cancel_link'		=>	 'javascript:history.back();',
                        ));
                        $this->show_category($request_id, $baseurl);
                        $this->main_title = $this->_msg["Category_delete"];
                        break;
                        // Удаление категории, шаг второй
                    } else if ($request_step == 2) {
                        $parent = $this->delCatalog($request_id);
                        @$cache->delete_cache_files();
                        header("Location: $baseurl&action=assortment&id=".$parent[0]);
                        exit;
                    }
                    $this->main_title = "Категории товаров";
                    break;

// поднятие каталога вверх на $shift позиций
                case "upcat":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_shift, $request_rank, $request_clevel, $request_id;
                    $ctg_owners = $this->get_category_owners($request_parent);
                    if (!$this->test_item_rights("p", "id", $this->table_prefix."_catalog_categories", $request_id, false, $ctg_owners))
                        $main->message_access_denied($this->module_name, $action);

                    if (is_numeric($request_shift) && $request_shift != $request_rank) {
                        $shift = (int)$request_shift;
                    } else {
                        $shift = (int)$request_rank;
                    }
                    if ($main->change_order('up', $this->table_prefix.'_catalog_categories', "rank", $request_rank, $shift, "clevel = $request_clevel AND")) {
                        @$cache->delete_cache_files();
                    }
                    $CDBT	=	new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
                    $parent	=	$CDBT->getParent($request_id);
                    header("Location: $baseurl&action=assortment&id=".$parent[0]);
                    exit;
                    break;

// опускание каталога вверх на $shift позиций
                case "downcat":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_shift, $request_rank, $request_clevel, $request_id;
                    $ctg_owners = $this->get_category_owners($request_parent);
                    if (!$this->test_item_rights("p", "id", $this->table_prefix."_catalog_categories", $request_id, false, $ctg_owners))
                        $main->message_access_denied($this->module_name, $action);

                    if (is_numeric($request_shift) && $request_shift != $request_rank) {
                        $shift = (int)$request_shift;
                    } else {
                        $shift = (int)$request_rank;
                    }
                    if ($main->change_order('down', $this->table_prefix.'_catalog_categories', 'rank', $request_rank, $shift, "clevel = $request_clevel AND" )) {
                        @$cache->delete_cache_files();
                    }
                    $CDBT	=	new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
                    $parent	=	$CDBT->getParent($request_id);
                    header("Location: $baseurl&action=assortment&id=".$parent[0]);
                    exit;
                    break;

// Выключение публикации категории
                case "csuspend":
                    if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id;
                    $ctg_owners = $this->get_category_owners($request_parent);
                    if (!$this->test_item_rights("p", "id", $this->table_prefix."_catalog_categories", $request_id, false, $ctg_owners))
                        $main->message_access_denied($this->module_name, $action);

                    if ($main->suspend($this->table_prefix.'_catalog_categories', $request_id, 'active')) @$cache->delete_cache_files();
                    $CDBT	=	new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
                    $parent	=	$CDBT->getParent($request_id);
                    header("Location: $baseurl&action=assortment&id=".$parent[0]);
                    exit;
                    break;

// Включение публикации категории
                case "cactivate":
                    if (!$permissions['p']) $main->message_access_denied($this->module_name, $action);
                    global $request_id;
                    $ctg_owners = $this->get_category_owners($request_parent);
                    if (!$this->test_item_rights("p", "id", $this->table_prefix."_catalog_categories", $request_id, false, $ctg_owners))
                        $main->message_access_denied($this->module_name, $action);

                    if ($main->activate($this->table_prefix.'_catalog_categories', $request_id, "active")) @$cache->delete_cache_files();
                    $CDBT	=	new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
                    $parent	=	$CDBT->getParent($request_id);
                    header("Location: $baseurl&action=assortment&id=".$parent[0]);
                    exit;
                    break;

// поднятие каталога вверх на $shift позиций
                case "productup":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_shift, $request_rank, $request_category_id;
                    if (is_numeric($request_shift) && $request_shift != $request_rank) {
                        $shift = (int)$request_shift;
                    } else {
                        $shift = (int)$request_rank;
                    }
                    if ($main->change_order('up', $this->table_prefix.'_catalog_products', 'rank', $request_rank, $shift, "category_id = $request_category_id AND" )) {
                        @$cache->delete_cache_files();
                    }
                    header("Location: ".($_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $baseurl."&action=assortment&id=".$request_category_id));
                    exit;
                    break;

// опускание каталога вверх на $shift позиций
                case "productdown":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_shift, $request_rank, $request_category_id;
                    if (is_numeric($request_shift) && $request_shift != $request_rank) {
                        $shift = (int)$request_shift;
                    } else {
                        $shift = (int)$request_rank;
                    }
                    if ($main->change_order('down', $this->table_prefix.'_catalog_products', 'rank', $request_rank, $shift, "category_id = $request_category_id AND" )) {
                        @$cache->delete_cache_files();
                    }
                    header("Location: ".($_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $baseurl."&action=assortment&id=".$request_category_id));
                    exit;
                    break;

// Редактирование прав категории
                case 'editctgrights':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_parent, $request_start;
                    $ctg_owners = $this->get_category_owners($request_parent);
                    if (!$this->test_item_rights("e", "id", $this->table_prefix."_catalog_categories", $request_id, true, $ctg_owners, false, true))
                        $main->message_access_denied($this->module_name, $action);

                    if (!$this->show_rights_form($this->table_prefix."_catalog_categories", "title", $request_id,
                        "&id=".$request_id."&parent=".$request_parent, $this->_msg["Category"], "savectgrights")) {
                        header('Location: '.$baseurl.'&action=assortment&id='.$request_parent.'&start='.$request_start);
                    }
                    if ($request_type == "JsHttpRequest") {
                        require_once RP."inc/class.JsHttpRequest.php";
                        $JsHttpRequest =& new JsHttpRequest("utf-8");
                    }
                    break;

// Сохранение прав категории
                case 'savectgrights':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_parent, $request_start, $request_rights;
                    $ctg_owners = $this->get_category_owners($request_parent);
                    if (!$this->test_item_rights("e", "id", $this->table_prefix."_catalog_categories", $request_id, true, $ctg_owners, false, true))
                        $main->message_access_denied($this->module_name, $action);

                    $this->save_rights_form($this->table_prefix."_catalog_categories", $request_id, $request_rights);
                    if ($request_type == "JsHttpRequest") {
                        require_once RP."inc/class.JsHttpRequest.php";
                        $JsHttpRequest =& new JsHttpRequest("utf-8");
                        $_RESULT = array(
                            "res"	=> 1,
                            "category"	=> $request_parent,
                        );
                    } else {
                        header('Location: '.$baseurl.'&action=assortment&id='.$request_parent.'&start='.$request_start);
                    }
                    exit;
                    break;


// Выключение публикации продукта
                case "psuspend":
                    if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_category_id;
                    if ($main->suspend($this->table_prefix.'_catalog_products', $request_id, "active")) @$cache->delete_cache_files();
                    header("Location: ".($_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $baseurl."&action=assortment&id=".$request_category_id));
                    exit;
                    break;

// Включение публикации продукта
                case "pactivate":
                    if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_category_id;
                    if ($main->activate($this->table_prefix.'_catalog_products', $request_id, "active")) @$cache->delete_cache_files();
                    header("Location: ".($_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $baseurl."&action=assortment&id=".$request_category_id));
                    exit;
                    break;

// Добавление производителя
                case 'addptype':
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_step, $request_ptypename, $request_link;
                    $main->include_main_blocks_2($this->module_name."_edit_ptype.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Producer_name_not_filled" => $this->_msg["Producer_name_not_filled"],
                        "MSG_Good_producer" => $this->_msg["Good_producer"],
                        "MSG_Link" => $this->_msg["Link"],
                        "MSG_required_fields" => $this->_msg["required_fields"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                    ));
                    if (!$request_step || $request_step == 1) {
                        $tpl->assign("_ROOT.form_action", "$baseurl&action=addptype");
                        $this->main_title = $this->_msg["Add_producer"];
                    } else if ($request_step == 2) {
                        $this->add_ptype($request_ptypename, $request_link);
                        header("Location: ".$baseurl."&action=showptypes");
                    }
                    break;

// Вывод производителя
                case 'showptypes':
                    if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
                    $main->include_main_blocks_2($this->module_name."_ptypes.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Add_producer" => $this->_msg["Add_producer"],
                        "MSG_Ed" => $this->_msg["Ed"],
                        "MSG_Producers" => $this->_msg["Producers"],
                        "MSG_Del" => $this->_msg["Del"],
                    ));
                    $tpl->assign(array('add_ptype' => '?lang='.$lang.'&name=catalog&action=addptype'));
                    $this->show_ptypes();
                    $this->main_title = $this->_msg["Producers"];
                    break;

// пзменение производителя
                case 'editptype':
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id;
                    $main->include_main_blocks_2($this->module_name."_edit_ptype.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Producer_name_not_filled" => $this->_msg["Producer_name_not_filled"],
                        "MSG_Good_producer" => $this->_msg["Good_producer"],
                        "MSG_Link" => $this->_msg["Link"],
                        "MSG_required_fields" => $this->_msg["required_fields"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                    ));
                    $tpl->assign(
                        array(
                            "_ROOT.form_action"	=>	$baseurl.'&action=updateptype',
                        )
                    );
                    $this->show_ptype($request_id);
                    $this->main_title = $this->_msg["Edit_producer"];
                    break;

                case 'updateptype':
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_ptypename, $request_link, $request_id;
                    $this->update_ptype($request_ptypename, $request_link, $request_id);
                    header("Location: ".$baseurl."&action=showptypes");
                    exit;
                    break;

// Удаление производителя
                case 'delptype':
                    if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
                    global $request_step, $request_id;
                    // Удаление группы, первый шаг
                    if (!$request_step || $request_step == 1) {
                        $main->include_main_blocks_2($this->module_name."_del_ptype.html", $this->tpl_path);
                        $tpl->prepare();
                        $tpl->assign(Array(
                            "MSG_Removing_producer" => $this->_msg["Removing_producer"],
                            "MSG_unsets_its_goods_ptype" => $this->_msg["unsets_its_goods_ptype"],
                            "MSG_Confirm_delete_producer" => $this->_msg["Confirm_delete_producer"],
                            "MSG_Delete" => $this->_msg["Delete"],
                            "MSG_Cancel" => $this->_msg["Cancel"],
                        ));
                        $tpl->assign(
                            array(
                                "_ROOT.form_action"	=>	$baseurl."&action=delptype&step=2&id=".$request_id,
                            )
                        );
                        $this->show_ptype($request_id);
                        $this->main_title = $this->_msg["Producer_delete"];
                        // Удаление группы, шаг второй
                    } else if ($request_step == 2) {
                        if ($this->delete_ptype($request_id)) @$cache->delete_cache_files();
                        header("Location: ".$baseurl."&action=showptypes");
                        exit;
                    }
                    break;

// Добавление продукта
                case 'addproduct':
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_copy, $request_parent, $request_start, $request_step,
                           $request_group_id, $request_prp_val, $request_ord_prp_val, $request_currency,
                           $request_price_1, $request_price_2, $request_price_3, $request_price_4, $request_price_5,
                           $request_ptype_id, $request_pcode, $request_pname, $request_info, $request_txt_content,
                           $request_seo_title, $request_description, $request_keywords, $request_availability, $request_full_description,
                           $request_block_id, $request_consernd, $request_tryon, $request_referer,
                           $request_del_image, $request_del_img, $request_img_rank, $request_update_img_rank;

                    $parent = $request_step == 2 ? $request_parent : $request_id;
                    $ctg_owners = $this->get_category_owners($parent);
                    list($allow, $ctg_rights) = $this->test_item_rights("e", "id", $this->table_prefix."_catalog_categories",
                        $parent, false, $ctg_owners, true);
                    if (!$allow) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    $this->showProdForm($request_copy, $request_copy ? true : false);
                    if (!$request_step || $request_step == 1) {
                        $this->main_title = $this->_msg["Add_good"];
                        $this->showNews();
                        $this->showFiles();
                        break;
                    } else if ($request_step == 2) {
                        $block_id = intval(substr($request_block_id, 10));
                        $lid = $this->add_product(
                            $request_parent,
                            $request_group_id,
                            $request_prp_val,
                            $request_ord_prp_val,
                            $request_currency,
                            $request_price_1,
                            $request_price_2,
                            $request_price_3,
                            $request_price_4,
                            $request_price_5,
                            $request_ptype_id,
                            $request_pcode,
                            $request_pname,
                            $request_info,
                            $request_txt_content,
                            $request_seo_title,
                            $request_description,
                            $request_keywords,
                            $request_availability,
                            $request_consernd,
                            $ctg_rights,
                            $block_id,
                            $request_id ? true : false,
                            $request_del_image,
                            $request_del_img,
                            $request_img_rank,
                            $request_img_rank
                        );

                        if ($lid) {
                            $lc->add_internal_links($this->module_name,
                                $this->table_prefix.'catalog_products',
                                'full_description',
                                $lid,
                                'ссылка в каталоге',
                                $request_full_description);
                            $this->storeNews($lid, 1);
                            $this->addFiles($lid);
                            @$cache->delete_cache_files();
                        }
                        if ($request_tryon) {
                            header('Location: '.$baseurl.'&action=editproduct&id='.$lid.'&start='.$request_start);
                        } else {
                            Module::go_back($request_referer);
                        }
                        exit;
                    }
                    break;
                case 'addtematik':
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_copy, $request_parent, $request_start, $request_step,
                           $request_consernd, $request_tryon, $request_referer;

                    $parent = $request_step == 2 ? $request_parent : $request_id;
                    $ctg_owners = $this->get_category_owners($parent);
                    list($allow, $ctg_rights) = $this->test_item_rights("e", "id", $this->table_prefix."_catalog_categories",
                        $parent, false, $ctg_owners, true);
                    if (!$allow) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    $this->showTematikForm($request_id, $request_copy ? true : false);
                    if (!$request_step || $request_step == 1) {
                        $this->main_title ='Товары в тематике';
                        $this->showNews();
                        $this->showFiles();
                        break;
                    } else if ($request_step == 2) {
                        $this->delAllConcernedProducts(2, $request_id);
                        $this->addConcernedProducts(2, $request_id, $request_consernd[1]);
                        if ($request_tryon) {
                            header('Location: '.$baseurl.'&action=addtematik&id='.$request_id.'&start='.$request_start);
                        } else {
                            Module::go_back($request_referer);
                        }
                        exit;
                    }
                    break;

                case 'addtematikcateg':
                    global $db,$reguest_id,$reguest_price_assoc;
                    $price=(int)$_REQUEST['price_assoc'];
                    $id=(int)$_REQUEST['id'];
                    $ctgr=$_REQUEST['ctgr'];
                    $ctg_ids='';
                    $ctg_ids_d='';
                    if(!empty($ctgr)){
                        foreach($ctgr as $ct){
                            $ctg_ids = $ctg_ids ? $ctg_ids.','.$ct : $ct;
                        }
                    }

                    else{
                        $this->delAllConcernedProducts(2, $id);
                        header('Location: '.$_SERVER['HTTP_REFERER']); exit;
                    }
                    $arr=$db->getArrayOfResult("SELECT c.concrn_id FROM sup_rus_catalog_concerned_products AS c
                                                         LEFT JOIN sup_rus_catalog_products AS p
                                                        ON  c.concrn_id=p.id
                                                        WHERE p.category_id NOT IN ($ctg_ids) AND c.type=2 AND c.prod_id=$id");
                    if(!empty($arr))foreach($arr as $v){
                        $ctg_ids_d = $ctg_ids_d ? $ctg_ids_d.','.$v['concrn_id'] : $v['concrn_id'];
                    }
                    if($ctg_ids_d){
                        $db->query("DELETE FROM sup_rus_catalog_concerned_products WHERE prod_id=$id AND concrn_id IN ($ctg_ids_d)" );
                    }

                    header('Location: '.$_SERVER['HTTP_REFERER']);
                    break;

                case 'addtematikassoc':
                    global $db,$reguest_id,$reguest_price_assoc;
                    $id=(int)$_REQUEST['id'];
                    $price=(int)$_REQUEST['price_assoc'];
                    $price_from=(int)$_REQUEST['price_assoc_from'];
                    $arr=$db->getArrayOfResult("SELECT id,price_1 FROM sup_rus_catalog_products WHERE price_1<= $price AND price_1>$price_from AND active=1");
                    $ids=array();
                    if(!empty($arr))foreach($arr as $v){
                        $ids[$v['id']]= 0   ;
                    }

                    if(!empty($ids)){
                        $this->delAllConcernedProducts(2, $id);
                        $this->addConcernedProducts(2, $id, $ids);
                    }

                    header('Location: '.$_SERVER['HTTP_REFERER']);
                    break;
                case 'del_video':
                    global $db,$server,$lang,$request_id;
                    $db->query("DELETE FROM ".$server.$lang."_catalog_products_video WHERE id=".$request_id);
                    break;
// Редактировать продукт
                case 'editproduct':
                    if (!$permissions["e"]) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $request_id, $request_category_id, $request_start, $request_parent;

                    $prod = $this->getProduct($request_id);
                    if ($prod) {
                        $ctg_owners = $this->get_category_owners($prod['category_id']);
                        if (!$this->test_item_rights("e", "id", $this->table_prefix."_catalog_products", $request_id, false, $ctg_owners)) {
                            $main->message_access_denied($this->module_name, $action);
                        }
                        if ($this->showProdForm($request_id)) {
                            $this->main_title = $this->_msg["Edit_good"];
                            $this->showNews($this->getNewsIds($request_id, 1));
                            $this->showFiles($request_id);
                            break;
                        }
                    }
                    header('Location: '.$baseurl.'&action=addproduct');
                    exit;

                case 'updateproduct':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);

                    global $request_id, $request_group_id, $request_prp_val, $request_ord_prp_val, $request_currency,
                           $request_price_1, $request_price_2, $request_price_3, $request_price_4, $request_price_5,
                           $request_ptype_id, $request_pcode, $request_pname, $request_info,
                           $request_txt_content, $request_seo_title, $request_description, $request_keywords, $request_availability,
                           $request_del_image, $request_del_image, $request_del_pics, $request_parent,
                           $request_start, $request_concrn_id, $request_referer, $request_del_img,
                           $request_consernd, $request_tryon, $request_img_rank, $request_update_img_rank,$request_descr_img,$request_update_img_desc,$request_videos,$request_descr_v;
                    $dsc = $request_descr_img;
                    $ctg_owners = $this->get_category_owners($request_parent);



                    if (!$this->test_item_rights("e", "id", $this->table_prefix."_catalog_products", $request_id, false, $ctg_owners))
                        $main->message_access_denied($this->module_name, $action);

                    $request_id = (int)$request_id;
                    if ($this->update_product($request_id, $request_group_id, $request_prp_val, $request_ord_prp_val,
                        $request_currency, $request_price_1, $request_price_2, $request_price_3, $request_price_4,
                        $request_price_5, $request_ptype_id, $request_pcode, $request_pname, $request_info,
                        $request_txt_content, $request_seo_title, $request_description, $request_keywords,
                        $request_availability, $request_del_image,
                        $request_del_pics, $request_del_img, $request_img_rank, $request_update_img_rank, $request_consernd,$request_update_img_desc,$dsc)
                    ) {
                        $this->storeNews($request_id,1);
                        $this->addFiles($request_id);
                        @$cache->delete_cache_files();
                    }
                    $this->load_video($request_id,$request_videos,$request_descr_v);
                    //exit();
                    if ($request_tryon) {
                        Module::go_back();
                    } else if (false !== strpos($request_referer, 'updateproduct')) {
                        Module::go_back($request_referer);
                    }


                    header("Location: ".$baseurl."&action=assortment&id=".$request_parent.($request_start ? '&start='.$request_start : ''));
                    exit;
                    break;

// Удаление продукта
                case "delproduct":
                    if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_category_id, $request_start;
                    $ctg_owners = $this->get_category_owners($request_category_id);
                    if (!$this->test_item_rights("d", "id", $this->table_prefix."_catalog_products", $request_id, false, $ctg_owners))
                        $main->message_access_denied($this->module_name, $action);

                    if ($this->delProduct($request_id)) {
                        $lc->delete_internal_links($this->module_name, $CONFIG["catalogue_products_table"], $request_id);
                        @$cache->delete_cache_files();
                    }
                    Module::go_back();
                    header("Location: ".$baseurl."&action=assortment&id=".$request_category_id.($request_start ? '&start='.$request_start : ''));
                    exit;
                    break;

// Вывод групп
                case "groups":
                    if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_start;
                    $start = intval($request_start);
                    $id = (int)$request_id;
                    if ($id == 0) $id = 1;
                    $main->include_main_blocks_2($this->module_name.'_groups.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign($this->get_add_group());
                    $tpl->newBlock('block_groups');
                    $tpl->assign(Array(
                        "MSG_Ed" => $this->_msg["Ed"],
                        "MSG_Group" => $this->_msg["Group"],
                        "MSG_Del" => $this->_msg["Del"],
                    ));
                    $groups = $this->get_groups($start, $CONFIG['catalog_max_rows']);
                    $tpl->assign(Array(
                        "MSG_Edit" => $this->_msg["Edit"],
                        "MSG_Confirm_delete_record" => $this->_msg["Confirm_delete_record"],
                        "MSG_Delete" => $this->_msg["Delete"],
                    ));
                    if (is_array($groups)) {
                        foreach ($groups as $val) {
                            $tpl->newBlock('block_group');
                            $val["MSG_Edit"] = $this->_msg["Edit"];
                            $val["MSG_Confirm_delete_record"] = $this->_msg["Confirm_delete_record"];
                            $val["MSG_Delete"] = $this->_msg["Delete"];
                            $tpl->assign($val);
                        }
                        $main->_show_nav_string($this->table_prefix.'_catalog_groups', '', '', "action=".$action, $request_start, $CONFIG['catalog_max_rows'], 'id', "", 1, 0);
                    }
                    $tpl->assign(Array(
                        "MSG_Pages" => $this->_msg["Pages"],
                    ));
                    $this->main_title = $this->_msg["Goods_groups"];
                    break;

// Добавление группы
                case "addgroup":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_step, $request_groupname;
                    $main->include_main_blocks_2($this->module_name."_edit_group.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Group_name_not_filled" => $this->_msg["Category_name_not_filled"],
                        "MSG_Group_name" => $this->_msg["Group_name"],
                        "MSG_required_fields" => $this->_msg["required_fields"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                    ));
                    if (!$request_step || $request_step == 1) {
                        $tpl->assign("_ROOT.form_action", $baseurl.'&action=addgroup');
                        $this->main_title = $this->_msg["Add_group"];
                    } else if ($request_step == 2) {
                        $this->add_group($request_groupname);
                        header("Location: ".$baseurl."&action=groups");
                    }
                    break;

// пзменение группы
                case 'editgroup':
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id;
                    $main->include_main_blocks_2($this->module_name."_edit_group.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Group_name_not_filled" => $this->_msg["Category_name_not_filled"],
                        "MSG_Group_name" => $this->_msg["Group_name"],
                        "MSG_required_fields" => $this->_msg["required_fields"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                    ));
                    $tpl->assign($this->get_group($request_id));
                    $tpl->assign(
                        array(
                            "_ROOT.form_action"	=>	$baseurl.'&action=updategroup',
                        )
                    );
                    $this->main_title = $this->_msg["Edit_group"];
                    break;

                case 'updategroup':
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_groupname;
                    $this->upd_group($request_id, $request_groupname);
                    header("Location: ".$baseurl."&action=groups");
                    exit;
                    break;

// Удаление группы
                case "delgroup":
                    if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
                    global $request_step, $request_id, $request_del_prp_grp;
                    // Удаление группы, первый шаг
                    if (!$request_step || $request_step == 1) {
                        $main->include_main_blocks_2($this->module_name."_del_group.html", $this->tpl_path);
                        $tpl->prepare();
                        $tpl->assign(Array(
                            "MSG_Removing_group" => $this->_msg["Removing_group"],
                            "MSG_unsets_its_goods_group" => $this->_msg["unsets_its_goods_group"],
                            "MSG_Confirm_delete_group" => $this->_msg["Confirm_delete_group"],
                            "MSG_Confirm_all_assigned_with" => $this->_msg["Confirm_all_assigned_with"],
                            "MSG_props_not_assigned_to_other" => $this->_msg["props_not_assigned_to_other"],
                            "MSG_Delete" => $this->_msg["Delete"],
                            "MSG_Cancel" => $this->_msg["Cancel"],
                        ));
                        $tpl->assign($this->get_group($request_id));
                        $tpl->assign(
                            array(
                                "_ROOT.form_action"	=>	$baseurl."&action=delgroup&step=2&id=".$request_id,
                            )
                        );
                        $this->main_title = $this->_msg["Group_delete"];
                        // Удаление группы, шаг второй
                    } else if ($request_step == 2) {
                        if ($this->del_group($request_id, $request_del_prp_grp)) {
                            @$cache->delete_cache_files();
                        }
                        header("Location: ".$baseurl."&action=groups");
                        exit;
                    }
                    break;

// Вывод свойств
                case "propertis":
                    if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);

                    global $request_start, $request_c;

                    $start = $request_start > 1 ? (int)$request_start : 1;

                    $main->include_main_blocks_2($this->module_name."_propertis.html", $this->tpl_path);
                    $tpl->prepare();

                    $tpl->assign(Array(
                        "MSG_Add_property" => $this->_msg["Add_property"],
                        "baseurl" => $baseurl,
                    ));

                    $tpl->newBlock('block_prts');

                    $link = "action=".$action.($_REQUEST['c'] ? '&c='.$_REQUEST['c'] : '');

                    $tpl->assign(Array(
                        "MSG_Ed" => $this->_msg["Ed"],
                        "MSG_ID" => $this->_msg["ID"],
                        "MSG_Property" => $this->_msg["Property"],
                        "MSG_Add_Property" => $this->_msg["Add_property"],
                        "MSG_Name" => $this->_msg["Name"],
                        "MSG_Prefix" => $this->_msg["Prefix"],
                        "MSG_Suffix" => $this->_msg["Suffix"],
                        "MSG_Values" => $this->_msg["Values"],
                        "MSG_Del" => $this->_msg["Del"],
                        "MSG_Seek" => $this->_msg["Seek"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                        "baseurl" => $baseurl,
                        "action" => $action,
                        "lang" => $lang,
                        "link" => $baseurl.'&'.$link,
                        "c" => $_REQUEST['c'] ? htmlspecialchars($_REQUEST['c']) : '',
                        'id_order' => 'id' == $_REQUEST['order'] ? '-id' : 'id',
                        'name_order' => 'name' == $_REQUEST['order'] ? '-name' : 'name',
                        'title_order' => 'title' == $_REQUEST['order'] ? '-title' : 'title',
                        'id_order_mark' => 'id' == $_REQUEST['order'] ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">"
                                : ('-id' == $_REQUEST['order'] ? "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">" : "&uarr;&darr;"),
                        'title_order_mark' => 'title' == $_REQUEST['order'] ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">"
                                : ('-title' == $_REQUEST['order'] ? "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">" : "&uarr;&darr;"),
                        'name_order_mark' => 'name' == $_REQUEST['order'] ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">"
                                : ('-name' == $_REQUEST['order'] ? "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">" : "&uarr;&darr;"),
                    ));

                    $propertis = $this->get_propertis($start, $CONFIG['catalog_max_rows'], $_REQUEST);
                    if ($propertis) {
                        foreach($propertis as $k => $v) {
                            $tpl->newBlock("block_prt");
                            $tpl->assign(Array(
                                "MSG_Edit" => $this->_msg["Edit"],
                                "MSG_Confirm_delete_record" => $this->_msg["Confirm_delete_record"],
                                "MSG_Delete" => $this->_msg["Delete"],
                            ));
                            $tpl->assign($v);
                        }

                        $cnt = $this->get_propertis_cnt($_REQUEST);
                        $link .= $_REQUEST['order'] ? '&order='.$_REQUEST['order'] : '';
                        $main->_show_nav_block(ceil($cnt/$CONFIG['catalog_max_rows']), null, $link, $start);
                        //$main->_show_nav_string($this->table_prefix.'_catalog_properties_table', '', '', "action=".$action, $request_start, $CONFIG['catalog_max_rows'], 'id', "", 1, 0);    				
                        $tpl->assign(Array(
                            "MSG_Pages" => $this->_msg["Pages"],
                        ));
                    }
                    $this->main_title = $this->_msg["Group_properties"];
                    break;

// Добавление свойства
                case "addprty":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_step, $request_select_type, $request_prttitle, $request_prtname,
                           $request_prefix, $request_suffix, $request_new_value, $request_def_value;
                    $main->include_main_blocks_2($this->module_name."_edit_property.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Property_name_not_filled" => $this->_msg["Property_name_not_filled"],
                        "MSG_Enter_valid_property_name" => $this->_msg["Enter_valid_property_name"],
                        "MSG_Dynamic_property_type" => $this->_msg["Dynamic_property_type"],
                        "MSG_Property_name" => $this->_msg["Property_name"],
                        "MSG_Property_title" => $this->_msg["Property_title"],
                        "MSG_Property_prefix" => $this->_msg["Property_prefix"],
                        "MSG_Property_suffix" => $this->_msg["Property_suffix"],
                        "MSG_Default_value" => $this->_msg["Default_value"],
                        "MSG_Add_default_value" => $this->_msg["Add_default_value"],
                        "MSG_Add" => $this->_msg["Add"],
                        "MSG_Del" => $this->_msg["Del"],
                        "MSG_Delete" => $this->_msg["Delete"],
                        "MSG_Added_images" => $this->_msg["Added_images"],
                        "MSG_Decrease" => $this->_msg["Decrease"],
                        "MSG_Increase" => $this->_msg["Increase"],
                        "MSG_Value" => $this->_msg["Value"],
                        "MSG_required_fields" => $this->_msg["required_fields"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                    ));
                    if (!$request_step || $request_step == 1) {
                        $tpl->assign_array('block_property_type_select', $CONFIG['catalog_dynamic_property_type']);
                        $tpl->assign(array(
                            '_ROOT.form_action' => $baseurl.'&action=addprty',
                            '_ROOT.display_def_val' => 'none',
                            '_ROOT.display_def_val_list' => 'none',
                            '_ROOT.display_type_add' => 'none',
                            '_ROOT.display_pref' => 'none',
                            '_ROOT.display_suff' => 'none',
                            '_ROOT.disabled_type' => ''
                        ));
                        $tpl->newBlock('block_new_value');
                        $this->main_title = $this->_msg["Add_property"];
                    } else if ($request_step == 2) {
                        $this->add_property($request_select_type,
                            $request_prttitle,
                            $request_prtname,
                            $request_prefix,
                            $request_suffix,
                            $request_def_value,
                            $request_new_value);
                        header('Location: '.$baseurl.'&action=propertis');
                    }
                    break;

// пзменение свойства
                case "updprty":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_select_type, $request_id, $request_prttitle, $request_prtname,
                           $request_prefix, $request_suffix, $request_def_value, $request_new_value;
                    $err = $this->update_property($request_select_type,
                        $request_id,
                        $request_prttitle,
                        $request_prtname,
                        $request_prefix,
                        $request_suffix,
                        $request_def_value,
                        $request_new_value);
                    if ($err == 0) {
                        header('Location: '.$baseurl.'&action=propertis');
                        exit;
                        break;
                    }
                    // ошибка сохранения
                    switch ($err) {
                        case 2:
                            $error_block = "not_unique_name_block";
                            break;
                        default:
                            $error_block = "common_error_block";
                            break;
                    }

                case "editprty":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id;
                    $main->include_main_blocks_2($this->module_name."_edit_property.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Property_name_not_filled" => $this->_msg["Property_name_not_filled"],
                        "MSG_Enter_valid_property_name" => $this->_msg["Enter_valid_property_name"],
                        "MSG_Dynamic_property_type" => $this->_msg["Dynamic_property_type"],
                        "MSG_Property_name" => $this->_msg["Property_name"],
                        "MSG_Property_title" => $this->_msg["Property_title"],
                        "MSG_Property_prefix" => $this->_msg["Property_prefix"],
                        "MSG_Property_suffix" => $this->_msg["Property_suffix"],
                        "MSG_Default_value" => $this->_msg["Default_value"],
                        "MSG_Add_default_value" => $this->_msg["Add_default_value"],
                        "MSG_Add" => $this->_msg["Add"],
                        "MSG_Del" => $this->_msg["Del"],
                        "MSG_Delete" => $this->_msg["Delete"],
                        "MSG_Added_images" => $this->_msg["Added_images"],
                        "MSG_Decrease" => $this->_msg["Decrease"],
                        "MSG_Increase" => $this->_msg["Increase"],
                        "MSG_Value" => $this->_msg["Value"],
                        "MSG_required_fields" => $this->_msg["required_fields"],
                        "MSG_Save" => $this->_msg["Save"],
                        "MSG_Cancel" => $this->_msg["Cancel"],
                    ));
                    if (!empty($error_block)){
                        $tpl->newBlock($error_block);
                    }
                    $tpl->assign(Array(
                        "common_error_block.MSG_Save_data_error" => $this->_msg["Save_data_error"],
                    ));
                    $tpl->assign(Array(
                        "not_unique_name_block.MSG_Property_name_not_unique" => $this->_msg["Property_name_not_unique"],
                    ));
                    $this->showProp($request_id);
                    $tpl->assign(array(
                        '_ROOT.form_action'			=> $baseurl.'&action=updprty',
                    ));
                    $this->main_title = $this->_msg["Edit_property"];
                    break;

// Удаление свойства
                case "delprty":
                    if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
                    global $request_step, $request_id;
                    // Удаление свойства, первый шаг
                    if (!$request_step || $request_step == 1) {
                        $main->include_main_blocks_2($this->module_name."_del_property.html", $this->tpl_path);
                        $tpl->prepare();
                        $tpl->assign(Array(
                            "MSG_Confirm_delete_group_property" => $this->_msg["Confirm_delete_group_property"],
                            "MSG_Delete" => $this->_msg["Delete"],
                            "MSG_Cancel" => $this->_msg["Cancel"],
                        ));
                        $this->showProp($request_id);
                        $tpl->assign(
                            array(
                                "_ROOT.form_action"	=>	$baseurl."&action=delprty&step=2&id=".$request_id,
                            )
                        );
                        $this->main_title = $this->_msg["Property_delete"];
                        // Удаление группы, шаг второй
                    } else if ($request_step == 2) {
                        if ($this->delete_property($request_id)) {
                            @$cache->delete_cache_files();
                        }
                        header("Location: ".$baseurl."&action=propertis");
                        exit;
                    }
                    break;

// прикрепление свойства к группе
                case "addgrprty":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_step, $request_id, $request_group_id, $request_group,
                           $request_search, $request_range, $request_ord;
                    $main->include_main_blocks_2($this->module_name."_group_property.html", $this->tpl_path);
                    $tpl->prepare();

                    if (!$request_step || $request_step == 1) {
                        $grp = $this->get_group($request_id);
                        if (! $grp) {
                            header("Location: ".$baseurl."&action=groups");
                            exit;
                        }
                        $tpl->newBlock('block_property');
                        $tpl->assign(Array(
                            "MSG_Select" => $this->_msg["Select"],
                            "MSG_Search" => $this->_msg["Search"],
                            "MSG_Range" => $this->_msg["Range"],
                            "MSG_Property" => $this->_msg["Property"],
                            "MSG_Prefix" => $this->_msg["Prefix"],
                            "MSG_Suffix" => $this->_msg["Suffix"],
                            "MSG_Save" => $this->_msg["Save"],
                            "MSG_Cancel" => $this->_msg["Cancel"],
                            "MSG_Order" => $this->_msg["Order"],
                        ));
                        $tpl->assign(array(
                            'group_id'     => $request_id,
                            "form_action"  => $baseurl.'&action=addgrprty'
                        ));
                        $arr = $this->getPropList($request_id, true);
                        $arr2 = $this->getPropList($request_id, false);
                        $arr = array_merge($arr ? $arr : array(), $arr2 ? $arr2 : array());
                        foreach ($arr as $key => $val) {
                            $tpl->newBlock('block_group');
                            $tpl->assign(array(
                                "prt_id"		=>	$val['prt_id'],
                                "prt_name"		=>	$val['prt_name'],
                                "prt_title"		=>	$val['prt_title'],
                                "prt_prefix"	=>	$val['prt_prefix'],
                                "prt_suffix"	=>	$val['prt_suffix'],
                                "prt_ord"   	=>	$val['prt_ord'],
                                "checked"		=>	$val['in_group'] ? 'checked' : '',
                            ));
                            if (in_array($val['prt_type'], $this->alow_prp_types_id)) {
                                $tpl->newBlock('block_search_checkbox');
                                $tpl->assign(array(
                                    "prt_id"		=>	$val['prt_id'],
                                    "checked"		=>	$val['search'] ? 'checked' : '',
                                ));
                            } else {
                                $tpl->newBlock('block_nbsp');
                            }
                            if (in_array($val['prt_type'], $this->alow_prp_range_types_id)) {
                                $tpl->newBlock('block_range_checkbox');
                                $tpl->assign(array(
                                    "prt_id" =>	$val['prt_id'],
                                    "checked" => $val['range'] ? 'checked' : '',
                                ));
                            } else {
                                $tpl->newBlock('block_range_nbsp');
                            }
                        }
                        $this->main_title = $this->_msg["Define_group_properties"]." - ".$grp['group_name'];
                    } elseif ($request_step == 2) {
                        $this->add_prp_grp($request_group_id, $request_group, $request_search, $request_range, $request_ord);
                        header("Location: ".$baseurl."&action=groups");
                    }
                    break;
// групповые свойства категорий
//
                case "suspendctg_checked":
                    if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
                    $ctg_owners = $this->get_category_owners($request_parent);
                    global $request_check;
                    if(is_array($request_check) && sizeof($request_check)>0){
                        foreach($request_check as $_v){
                            if($this->test_item_rights("p", "id", $this->table_prefix."_catalog_categories", $_v, false, $ctg_owners)){
                                if ($main->suspend($this->table_prefix.'_catalog_categories', $_v, 'active')) @$cache->delete_cache_files();
                            }
                        }
                    }
                    $CDBT	=	new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
                    $parent	=	$CDBT->getParent($_v);
                    header("Location: $baseurl&action=assortment&id=".$parent[0]);
                    exit;
                    break;

                case "activatectg_checked":
                    if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
                    $ctg_owners = $this->get_category_owners($request_parent);
                    global $request_check;
                    if(is_array($request_check) && sizeof($request_check)>0){
                        foreach($request_check as $_v){
                            if($this->test_item_rights("p", "id", $this->table_prefix."_catalog_categories", $_v, false, $ctg_owners)){
                                if ($main->activate($this->table_prefix.'_catalog_categories', $_v, 'active')) @$cache->delete_cache_files();
                            }
                        }
                    }
                    $CDBT	=	new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
                    $parent	=	$CDBT->getParent($_v);
                    header("Location: $baseurl&action=assortment&id=".$parent[0]);
                    exit;
                    break;

                case "deletectg_checked":
                    if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
                    global $request_start, $request_id, $request_parent, $request_check;
                    $ctg_owners = $this->get_category_owners($request_parent);
                    if(is_array($request_check) && sizeof($request_check)>0){
                        foreach($request_check as $_v){
                            if(!$parent){
                                $CDBT	=	new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
                                $parent	=	$CDBT->getParent($_v);
                                unset($CDBT);
                            }
                            if ($this->test_item_rights("d", "id", $this->table_prefix."_catalog_categories", $_v, false, $ctg_owners)){
                                $parent = $this->delCatalog($_v);

                            }
                        }
                        @$cache->delete_cache_files();
                    }
                    header("Location: $baseurl&action=assortment&id=".$parent[0]);
                    exit;
                    break;


                case "deleteprod_checked":
                    if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
                    global $request_check, $request_category_id;

                    if(is_array($request_check) && sizeof($request_check)>0){
                        $this->delProduct($request_check);
                        @$cache->delete_cache_files();
                    }
                    Module::go_back();
                    header("Location: $baseurl&action=assortment&id=".$request_category_id);
                    exit;
                    break;

                case "suspendprod_checked":
                    if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
                    global $request_check, $request_category_id;
                    if(is_array($request_check) && sizeof($request_check)>0){
                        foreach($request_check as $_v){
                            if ($main->suspend($this->table_prefix.'_catalog_products', $_v, 'active')) @$cache->delete_cache_files();
                        }
                    }
                    Module::go_back();
                    header("Location: $baseurl&action=assortment&id=".$request_category_id);
                    exit;
                    break;

                case "activateprod_checked":
                    if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
                    global $request_check, $request_category_id;
                    if(is_array($request_check) && sizeof($request_check)>0){
                        foreach($request_check as $_v){
                            if ($main->activate($this->table_prefix.'_catalog_products', $_v, 'active')) @$cache->delete_cache_files();
                        }
                    }
                    Module::go_back();
                    header("Location: $baseurl&action=assortment&id=".$request_category_id);
                    exit;
                    break;


// пзменить отметку - лидер продаж
                case "changeleader":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_category_id, $request_start;
                    $ctg_owners = $this->get_category_owners($request_category_id);
                    if (!$this->test_item_rights("e", "id", $this->table_prefix."_catalog_products", $request_id, false, $ctg_owners))
                        $main->message_access_denied($this->module_name, $action);

                    $this->changeLeader($request_id);

                    if (isset($_SERVER['HTTP_REFERER']) ) {
                        $url = parse_url($_SERVER['HTTP_REFERER']);
                        if ($url && $_SERVER["HTTP_HOST"] == $url['host']) {
                            header("Location: {$url['path']}?{$url['query']}");
                            exit;
                        }
                    }
                    Module::go_back();
                    header("Location: ".$baseurl."&action=assortment&id=".$request_category_id.($request_start ? '&start='.$request_start : ''));
                    exit;
                    break;
                case "changearchive":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_category_id, $request_start;
                    $ctg_owners = $this->get_category_owners($request_category_id);
                    if (!$this->test_item_rights("e", "id", $this->table_prefix."_catalog_products", $request_id, false, $ctg_owners))
                        $main->message_access_denied($this->module_name, $action);

                    $this->changeArchive($request_id);

                    if (isset($_SERVER['HTTP_REFERER']) ) {
                        $url = parse_url($_SERVER['HTTP_REFERER']);
                        if ($url && $_SERVER["HTTP_HOST"] == $url['host']) {
                            header("Location: {$url['path']}?{$url['query']}");
                            exit;
                        }
                    }
                    Module::go_back();
                    header("Location: ".$baseurl."&action=assortment&id=".$request_category_id.($request_start ? '&start='.$request_start : ''));
                    exit;
                    break;

// Изменить отметку - новинки продаж
                case "changenovelty":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_category_id;
                    $ctg_owners = $this->get_category_owners($request_category_id);
                    if (!$this->test_item_rights("e", "id", $this->table_prefix."_catalog_products", $request_id, false, $ctg_owners))
                        $main->message_access_denied($this->module_name, $action);

                    $this->changeNovelty($request_id);
                    Module::go_back();
                    header("Location: ". ($_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $baseurl."&action=assortment&id=".$request_category_id));
                    exit;
                    break;

// Редактирование прав товара
                case 'editrights':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_category_id, $request_start;
                    if (!$this->test_item_rights("e", "id", $this->table_prefix."_catalog_products", $request_id, true))
                        $main->message_access_denied($this->module_name, $action);

                    if (!$this->show_rights_form($this->table_prefix."_catalog_products", "product_title", $request_id,
                        "&id=".$request_id."&category_id=".$request_category_id."&start=".$request_start, $this->_msg["Good"])) {
                        header('Location: '.$baseurl.'&action=assortment&id='.$request_category_id.'&start='.$request_start);
                    }
                    if ($request_type == "JsHttpRequest") {
                        require_once RP."inc/class.JsHttpRequest.php";
                        $JsHttpRequest =& new JsHttpRequest("utf-8");
                    }
                    break;

// Сохранение прав товара
                case 'saverights':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_id, $request_category_id, $request_start, $request_rights;
                    if (!$this->test_item_rights("e", "id", $this->table_prefix."_catalog_products", $request_id, true))
                        $main->message_access_denied($this->module_name, $action);

                    $this->save_rights_form($this->table_prefix."_catalog_products", $request_id, $request_rights);
                    if ($request_type == "JsHttpRequest") {
                        require_once RP."inc/class.JsHttpRequest.php";
                        $JsHttpRequest =& new JsHttpRequest("utf-8");
                        $_RESULT = array(
                            "res"	=> 1,
                            "id"	=> $request_id,
                        );
                    } else {
                        header('Location: '.$baseurl.'&action=assortment&id='.$request_category_id.'&start='.$request_start);
                    }
                    exit;
                    break;

// Выбор новой родительской категории
                case 'select_parent':
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $request_id;
                    $main->include_main_blocks_2($this->module_name."_".$action.".html", $this->tpl_path);
                    $tpl->prepare();
                    $ctgr_list = $this->getCategsList($request_id);
                    if (empty($ctgr_list)) {
                        $tpl->newBlock('block_err');
                    } else {
                        $tree = $this->getCategsTree($request_id);
                        if (empty($tree)) {
                            $tpl->newBlock('block_tree_empty');
                        } else {
                            $tpl->newBlock('block_tree');
                            $tpl->assign('baseurl', $baseurl);
                            foreach ($ctgr_list as $ctgr) {
                                $tpl->newBlock('block_child_ctgr');
                                $tpl->assign($ctgr);
                            }
                            $level = 1;
                            foreach ($tree as $node) {
                                $tpl->newBlock('block_item');
                                $tpl->assign($node + array(
                                        'baseurl' => $baseurl,
                                        'title_qoute' => str_replace("'", "\'", $node['title'])
                                    ));
                                if ($node['level'] > $level) {
                                    $tpl->newBlock('block_level_begin');
                                }
                                if ($node['level'] < $level) {
                                    $tpl->newBlock('block_level_end');
                                    for ($i = $level; $i < $node['level']; $i--) {
                                        $tpl->newBlock('block_level_end');
                                    }
                                }
                                $level = $node['level'];
                            }
                            $tpl->gotoBlock('block_tree');
                        }
                    }
                    $tpl->assign(array(
                        'MSG_No_select_categories' => $this->_msg['No_select_categories'],
                        'MSG_Isnot_categories' => $this->_msg['Isnot_categories'],
                        'MSG_Parent_category_err' => $this->_msg['Parent_category_err'],
                        'MSG_Select_categories' => $this->_msg['Select_categories'],
                        'MSG_Move' => $this->_msg['Move'],
                        'MSG_Close_window' => $this->_msg['Close_window'],
                        'MSG_Select_category' => $this->_msg['Select_category']
                    ));
                    $this->main_title = $this->_msg['Move_categories'];
                    break;

// Сена родительской категории
                case 'set_parent':
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    $main->include_main_blocks_2($this->module_name."_select_parent.html", $this->tpl_path);
                    $tpl->prepare();

                    global $request_id,  $request_parent_id;
                    $ctgr_list = $this->getCategsList($request_id);
                    $request_parent_id = (int) $request_parent_id;
                    if (!empty($ctgr_list) && $this->getCategsList(array($request_parent_id))) {
                        $tree = $this->getCategsTree($request_id, true);
                        if (!empty($tree)) {
                            /* Проверяем чтобы родительская категория не совпадала с совпадала с перемещаемыми категориями и их потомками */
                            $find = false;
                            while (!$find && (list(,$node) = each($tree)) ) {
                                if ($request_parent_id == $node['id']) {
                                    $find = true;
                                }
                            }
                            if ($find) {
                                $db->query("UPDATE {$this->table_prefix}_catalog_categories SET parent_id={$request_parent_id} WHERE id IN (".
                                    implode(", ", array_keys($ctgr_list)).")");
                                $db_tree = new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
                                foreach ($ctgr_list as $key => $val) {
                                    $db_tree->moveAll($key, $request_parent_id);
                                }
                                $db_tree->fixTree();
                                $tpl->newBlock('block_set_ok');
                                $tpl->assign(array(
                                    'baseurl' => $baseurl,
                                    'parent_id' => $request_parent_id,
                                    'backurl' => "{$baseurl}&action=assortment&id=".$request_parent_id,
                                ));
                            } else {
                                $tpl->newBlock('block_parent_err');
                            }
                        } else {
                            $tpl->newBlock('block_tree_empty');
                        }
                    } else {
                        $tpl->newBlock('block_err');
                    }
                    $tpl->assign(array(
                        'MSG_No_select_categories' => $this->_msg['No_select_categories'],
                        'MSG_Isnot_categories' => $this->_msg['Isnot_categories'],
                        'MSG_Parent_category_err' => $this->_msg['Parent_category_err'],
                        'MSG_Category_moved' => $this->_msg['Category_moved'],
                    ));
                    $this->main_title = $this->_msg['Move_categories'];
                    break;

// Интерфейс для добавления связанных товаров
                case 'addconcernd':
                    if (!$permissions['r']) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    global $request_id, $request_parent, $request_type;
                    $main->include_main_blocks_2("{$this->module_name}_{$action}.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(array(
                        'MSG_Loading' => $this->_msg['Loading'],
                        'MSG_Goods' => $this->_msg['Goods'],
                        'MSG_Select' => $this->_msg['Select'],
                        'MSG_Name' => $this->_msg['Name'],
                        'MSG_Presence' => $this->_msg['Presence'],
                        'MSG_Price' => $this->_msg['Price'],
                        'MSG_No_goods' => $this->_msg['No_goods'],
                        'MSG_Selected_goods' => $this->_msg['Selected_goods'],
                        'MSG_Add_selected_goods' => $this->_msg['Add_selected_goods'],
                        'MSG_Clean_list' => $this->_msg['Clean_list'],
                        'MSG_Close_window' => $this->_msg['Close_window'],
                        'MSG_Pages' => $this->_msg['Pages'],
                        'MSG_Delete' => $this->_msg['Delete'],
                        'MSG_Goods_added' => $this->_msg['Goods_added'],
                        'baseurl' => $baseurl,
                        'type' => (int)$request_type
                    ));

                    $this->showCtgrTree($this->getCategsTree());
                    break;
                case 'addprodfiles':
                    if (!$permissions['r']) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    $main->include_main_blocks_2($this->module_name.'_add_prod_files.html', $this->tpl_path);
                    $tpl->prepare();
                    require_once RP."mod/shop/lib/class.ShopPrototype.php";
                    $thisShop= new ShopPrototype();
                    $root_cats = $thisShop->getCategories(1);
                    $allFileInfo=$thisShop->getAllFilesInfo();
                    $add = '&nbsp;&nbsp;&nbsp;';
                    $tpl->newBlock("files");
                    foreach($allFileInfo as $file)
                    {
                        $tpl->newBlock("file");
                        $tpl->assign($file);

                    }
                    foreach($root_cats as $root){
                        $sep = '';

                        $thisShop->showCat($root, $sep);
                        $sep1 = $add.$sep;
                        $cats1 = $thisShop->getCategories($root['id']);
                        if(!sizeof($cats1)){
                            //$products = $this->getProducts($root['id']);
                            //$this->showProducts($products, $sep1);
                        }else{
                            foreach($cats1 as $cat1){
                                $thisShop->showCat($cat1, $sep1);
                                $sep2 = $add.$sep1;
                                $cats2 = $thisShop->getCategories($cat1['id']);
                                if(!sizeof($cats2)){
                                    //$products = $this->getProducts($cat1['id']);
                                    //$this->showProducts($products, $sep2);
                                }else{
                                    foreach($cats2 as $cat2){
                                        $thisShop->showCat($cat2, $sep2);
                                        $sep3 = $add.$sep2;
                                        $cats3 = $thisShop->getCategories($cat2['id']);
                                        if(!sizeof($cats3)){
                                            //$products = $this->getProducts($cat2['id']);
                                            //$this->showProducts($products, $sep3);
                                        }else{
                                            foreach($cats3 as $cat3){
                                                $thisShop->showCat($cat3, $sep3);
                                                $sep4 = $add.$sep3;
                                                $cats4 = $thisShop->getCategories($cat3['id']);
                                                if(!sizeof($cats4)){
                                                    //$products = $this->getProducts($cat3['id']);
                                                    //$this->showProducts($products, $sep4);
                                                }else{
                                                    foreach($cats4 as $cat4){
                                                        $thisShop->showCat($cat4, $sep4);
                                                        //$sep = '  '.$sep;
                                                        //$cats3 = $this->getCategories($parent_id);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }


                            }
                        }

                    }
                    $mes="";
                    $tpl->newBlock('messager');
                    if(isset($_REQUEST['prod_id'])){
                        if(isset($_REQUEST['items']))
                        {

                            $i=0;
                            foreach($_REQUEST['items'] as $val)
                            {

                                if(!$i)
                                {
                                    $mes=(float)$val;
                                    $this->addFiles($val);
                                    $file_name=$val;
                                    $i++;
                                    continue;
                                }

                                $mes=$mes.",".$val;

                                foreach($_FILES['afile']['name'] as $k => $v){
                                    $img_name = $_FILES['afile']['name'][$k];//$file_name."_".
                                    $name = $db->escape($_POST['aname'][$k]);

                                    if(!mysql_query("INSERT INTO ".$server.$lang."_catalog_files(product_id,filename, name) VALUES($val,'$img_name', '$name') ")){
                                        $tpl->assign(array('message'=>"Ошибка добавления в БД для id №".$val));
                                    }

                                }

                            }
                            $tpl->assign(array('message'=>"Файл успешно добавлен для товаров с id :".$mes));

                        }
                        elseif(isset($_REQUEST['allCheck'])){
                            global $db;
                            $r=$this->getSubCategsForRootCateg($_REQUEST['prod_id']);
                            $allCateg=$this->getCategsTree();
                            for($i=0;$i < count($r);$i++)
                            {
                                $c=$r[$i]['ctg_id'];
                                $cat[]=$c;
                                foreach($allCateg as $v)
                                {
                                    if($v['parent_id']==$r[$i]['ctg_id'])
                                    {
                                        $u=$this->getSubCategsForRootCateg($v['id']);
                                        if($u){
                                            $r=array_merge($r,$u);
                                        }
                                        else
                                        {
                                            $cat[]=$v['id'];
                                        }

                                    }
                                }

                            }
                            foreach($cat as $val)
                            {

                                $prodId=$this->getProductsByCategory($val);
                                foreach($prodId as $id)
                                {
                                    if($mes)
                                        $mes=$mes.",".$id;
                                    else $mes=$id;
                                    $ids[]=$id;
                                }

                            }
                            $i=0;
                            foreach($ids as $val)
                            {
                                if(!$i)
                                {
                                    $this->addFiles($val);
                                    $file_name=$val;
                                    $i++;
                                    continue;
                                }
                                foreach($_FILES['afile']['name'] as $k => $v){

                                    $img_name = $_FILES['afile']['name'][$k];//$file_name."_".
                                    $name = $db->escape($_POST['aname'][$k]);
                                    if(!mysql_query("INSERT INTO ".$server.$lang."_catalog_files(product_id,filename, name) VALUES($val,'$img_name', '$name') ")){
                                        $tpl->assign(array('message'=>"Ошибка добавления в БД для id №".$val));
                                    }

                                }

                            }
                            $tpl->assign(array('message'=>"Файл успешно добавлен для товаров с id :".$mes));
                        }


                    }
                    break;
// Получить список товаров
                case 'getprodlist':
                    if (!$permissions['r']) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    if ($GLOBALS['JsHttpRequest']) {
                        $res = $this->getSearchProds(
                            array('ctgr_id' => $_REQUEST['ctgr_id']),
                            $CONFIG['catalog_max_rows'],
                            $_REQUEST['start'],
                            false);
                        if ($res) {
                            $nav = $res['nav_str'];
                            unset($res['nav_str']);
                            $nav = array(
                                'start' => (int)$_REQUEST['start'],
                                'page_cnt' => ceil($nav['result'] / $CONFIG['catalog_max_rows'])
                            );
                            $list = array();
                            foreach ($res as $val) {
                                $list[$val['prod_id']] = array(
                                    'id' => $val['prod_id'],
                                    'title' => $val['prod_title'],
                                    'availability' => $val['prod_availability'],
                                    'price' => (int)$val['prod_price_1'],
                                    'active' => $val['active'] ? $this->_msg['Yes'] : $this->_msg['No']
                                );
                                $GLOBALS['_RESULT'] = array('list' => $list, 'pager' => $nav);
                            }
                        } else {
                            $GLOBALS['_RESULT']['err'] = true;
                        }
                    }

                    exit;

// Проверить фопустимость ссылки товара
                case 'isvalidprodalias':
                    if (!$permissions['r']) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    if ($GLOBALS['JsHttpRequest']) {
                        $GLOBALS['_RESULT'] = $this->isValidProdAlias($_REQUEST['alias'], $_REQUEST['id']);
                    }

                    exit;

// Проверить фопустимость ссылки товара
                case 'isvalidctgralias':
                    if (!$permissions['r']) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    if ($GLOBALS['JsHttpRequest']) {
                        $GLOBALS['_RESULT'] = $this->isValidCtgrAlias($_REQUEST['alias'], $_REQUEST['id']);
                    }
                    exit;

// Динамические свойства группы
                case 'groupprp':
                    if (!$permissions['r']) {
                        $main->message_access_denied($this->module_name, $action);
                    }
                    if ($GLOBALS['JsHttpRequest']) {
                        $GLOBALS['_RESULT'] = $this->getPropList($_REQUEST['id'], true, true);
                    }

                    exit;

// Default...
                default:
                    $main->include_main_blocks();
                    $tpl->prepare();
            }

//--------------------------- конец обработки действий из админ части --------------------------//
        }
    }
    function getGiftsToProds(){
        if (file_exists(RP."mod/shop/lib/class.ShopPrototype.php")) {
            require_once(RP."mod/shop/lib/class.ShopPrototype.php");
            $shop = new ShopPrototype('only_create_object');
            return $shop->getGiftsToProds();
        }
        return array();
    }
    function getGiftToProd($id){
        if(!$this->is_get_gp){
            $this->gp = $this->getGiftsToProds();
            $this->is_get_gp = true;
        }

        foreach($this->gp as $p){
            if(sizeof($p['gifts']) == 0 && $p[0][0]['id'] == $id)
            {
                return array($p[0], $p['gifts'][0]);
            }
            elseif(sizeof($p['gifts']) == 1 && ($p[0][0]['id'] == $id || $p[0][1]['id'] == $id))
                return array(array($p[0], $p['gifts'][0]), $p['gifts'][0]);
            elseif(sizeof($p['gifts']) >= 2 && ($p[0][0]['id'] == $id || $p[0][1]['id'] == $id || $p[0][2]['id'] == $id))
            {
                $arr= array(array($p[0] ), $p['gifts'][0]);
                $i=0;
                foreach($p['gifts'] as $val)
                {
                    if(!$i) {
                        $i++;
                        //  continue;
                    }
                    $arr[0][]=$val;
                }
                return $arr;
            }

        }
        return 0;
    }
    function showGift($id){
        global $server, $lang, $tpl,$db;
        if($ret = $this->getGiftToProd($id)){
            $gift = $ret[1];
            $gift_if = $ret[0][2];
            if(sizeof($ret[0]) == 1){
                $product_info = $this->getProduct($id);
                $tpl->newBlock('double_gift');
                $tpl->assign("msg", "+".(($ret[0][0][0]['id'] == $id) ? $ret[0][0][1]['product_title'] : $ret[0][0][0]['product_title'])." = ");
                $tpl->assign('title', $gift['product_title']);
                $tpl->newBlock('choose_double_gift');
                $tpl->assign("act_sel", $product_info['product_title']."+".(($ret[0][0][0]['id'] == $id) ? $ret[0][0][1]['product_title'] : $ret[0][0][0]['product_title'])." = ".$gift['product_title']);
                $tpl->assign('noact_sel', $product_info['product_title']);
            }elseif(sizeof($ret[0]) == 2){
                $kupon = (int)$ret[0][0][0]['kupon'];
                $percent = (int)$ret[0][0][0]['percent'];
                if($gift && $kupon) $block = 'kupon_gift';
                elseif($gift && $percent) $block = 'percent_gift';
                elseif($kupon) $block = 'kupon';
                else $block = 'gift';
                $link = '/catalog/'.($gift['calias'] ? $gift['calias'] : $gift['cid']).'/'.($gift['alias'] ? $gift['alias'] : $gift['id']).'.html';
                $tpl->newBlock($block);
                $tpl->assign('title', $gift['product_title']);
                $tpl->assign('comment', $gift['comment']);
                $tpl->assign('alter', $gift['product_inf']);
                $tpl->assign('link', $link);
                $tpl->assign('price', !$kupon ? $percent : $kupon);
                if($gift['image_middle']){
                    $tpl->newBlock($block.'_img');
                    $tpl->assign('src', '/files/catalog/'.$server.$lang.'/'.$gift['image_middle']);
                }
                if($gift && $kupon){
                    $tpl->newBlock('choose_gift');
                    $tpl->assign('title', $gift['product_title']);
                    $tpl->assign('link', $link);
                    $tpl->assign('price', $kupon);
                }elseif($gift && $percent){
                    $tpl->newBlock('choose_gift_percent');
                    $tpl->assign('title', $gift['product_title']);
                    $tpl->assign('link', $link);
                    $tpl->assign('price', $percent);
                }else
                {
                    $tpl->newBlock('double_gift_hidden');
                }
            }
            elseif(sizeof($ret[0]) >= 3)
            {
                $kupon = (int)$ret[0][0][0]['kupon'];
                $block = 'gift';
                $link = '/catalog/'.($gift['calias'] ? $gift['calias'] : $gift['cid']).'/'.($gift['alias'] ? $gift['alias'] : $gift['id']).'.html';
                $link_if = '/catalog/'.($gift_if['calias'] ? $gift_if['calias'] : $gift_if['cid']).'/'.($gift_if['alias'] ? $gift_if['alias'] : $gift_if['id']).'.html';
                $tpl->newBlock($block);
                $tpl->assign('title', $gift['product_title']);
                $tpl->assign('alter', $gift_if['product_inf']);
                $tpl->assign('comment', $gift['comment']);
                $tpl->assign('alter', $gift['product_inf']);
                $tpl->assign('link', $link);
                if($gift['image_middle']){
                    $tpl->newBlock($block.'_img');
                    $tpl->assign('src', '/files/catalog/'.$server.$lang.'/'.$gift['image_middle']);
                }
                $tpl->newBlock($block."_if");
                $tpl->assign('title', $gift_if['product_title']);
                $tpl->assign('link', $link_if);
                $tpl->assign('comment', $gift_if['comment']);
                $tpl->assign('alter', $gift_if['product_inf']);
                if($gift['image_middle']){
                    $tpl->assign('src', '/files/catalog/'.$server.$lang.'/'.$gift_if['image_middle']);
                }
                if(count($ret[0]) > 3)
                {
                    $l=0;
                    foreach($ret[0] as $value)
                    {
                        $l++;
                        if($l < 4) continue;
                        $tpl->newBlock($block."_if");
                        $tpl->assign('title', $value['product_title']);
                        $link_ifs = '/catalog/'.($value['calias'] ? $value['calias'] : $value['cid']).'/'.($value['alias'] ? $value['alias'] : $value['id']).'.html';
                        $tpl->assign('link', $link_ifs);
                        if($value['image_middle']){
                            $tpl->assign('src', '/files/catalog/'.$server.$lang.'/'.$value['image_middle']);
                        }
                        $tpl->assign('comment', $value['comment']);
                        $tpl->assign('alter', $value['product_inf']);
                    }
                }
                $tpl->newBlock('choose_gift_if');
                $tpl->assign('title', $gift['product_title']);
                $tpl->assign('link', $link);
                $tpl->assign('title_if', $gift_if['product_title']);
                $tpl->assign('link_if', $link_if);
                if(count($ret[0]) > 3)
                {
                    $l=0;
                    foreach($ret[0] as $value)
                    {
                        $l++;
                        if($l < 4) continue;
                        $tpl->newBlock('choose_gift_ifs');
                        $tpl->assign('title_if', $value['product_title']);
                        $link_ifs = '/catalog/'.($value['calias'] ? $value['calias'] : $value['cid']).'/'.($value['alias'] ? $value['alias'] : $value['id']).'.html';
                        $tpl->assign('link_if', $link_ifs);
                        $tpl->assign('id', $l);
                    }
                }
            }
            else
            {
                $product_info = $this->getProduct($id);
                $tpl->newBlock('percent');
            }

        }
        $ar=$db->getArrayOfResult("SELECT   c.product_title,c.category_id,p1.prod_id FROM sup_rus_skidki_prods as p
                                LEFT JOIN sup_rus_skidki_prods  AS p1 ON p1.skidka_id = p.skidka_id
                                LEFT JOIN sup_rus_catalog_products AS c ON c.id = p1.prod_id
                                 JOIN sup_rus_actions_skidki AS a
                                ON  p.skidka_id = a.skidka_id
                                WHERE p.prod_id={$id} AND p1.is_gift=0 AND p1.is_if=0");
        if(!empty($ar))
        {

            if(count($ar)==1 && $ar[0]['prod_id']==$id)
            { return true;}
            $k=0;
                foreach($ar as $val)
                {
                    if($val['prod_id']==$id) continue;
                    if(!$k)
                    {
                        $tpl->newBlock('products_gift');
                        $tpl->newBlock('product_inform_one');
                        $tpl->assign(array('link'=>"/catalog/".$val['category_id']."/".$val['prod_id'].".html",
                            'title' =>$val['product_title'] ));
                    }
                    else
                    {
                        $tpl->newBlock('product_inform_other');
                        $tpl->assign(array('link'=>"/catalog/".$val['category_id']."/".$val['prod_id'].".html",
                            'title' =>$val['product_title'] ));
                    }
                    $k++;

                }
            if($k > 1)
            {

                $tpl->newBlock('product_inform_more');
                $tpl->newBlock('product_inform_more_end');
            }


        }

    }

    function showNews($ids = array()){
        global $server,$lang,$tpl;
        $news = array();
        $query = 'SELECT id,title FROM '.$server.$lang.'_news WHERE category_id=12';
        if(!$result = mysql_query($query)) die(mysql_error());

        while($row = mysql_fetch_assoc($result)) $news[] = $row;

        foreach($news as $item){
            $tpl->newBlock('news_item_div');
            $tpl->assign(array(
                'id' => $item['id'],
                'title' => $item['title']
            ));
        }

        foreach($ids as $k => $id){
            $tpl->newBlock('news');

            foreach($news as $k => $item){
                $tpl->newBlock('news_item');
                $tpl->assign(array(
                    'id' => $item['id'],
                    'title' => $item['title'],
                    'selected' => $id == $item['id'] ? 'selected' : ''
                ));
            }
        }

    }
    function getNewsIds($id, $type = 0){
        global $server,$lang;
        $query = 'SELECT news_id FROM '.$server.$lang.'_catalog_news WHERE item_id='.(int)$id.' AND type='.(int)$type;
        if(!$result = mysql_query($query)) die(mysql_error());
        $ids = array();
        while($row = mysql_fetch_assoc($result)) $ids[] = $row['news_id'];
        return $ids;
    }
    function storeNews($id, $type = 0){
        global $server,$lang;
        $query = 'DELETE FROM '.$server.$lang.'_catalog_news WHERE item_id='.(int)$id.' AND type='.(int)$type;
        if(!$result = mysql_query($query)) die(mysql_error());
        foreach($_POST['news'] as $idx){
            if((int)$idx){
                $query = 'INSERT INTO '.$server.$lang.'_catalog_news (item_id,news_id,type) VALUES('.(int)$id.','.(int)$idx.','.(int)$type.')';
                if(!$result = mysql_query($query)) die(mysql_error());
            }
        }
    }
    function showNewsFront($id, $category_id){
        global $server, $lang, $tpl, $PAGE;
        if(!$PAGE['top_category'])$ids = getAllCats($category_id);//$PAGE['top_category']
        else
            $ids = getAllCats($PAGE['top_category']);//$PAGE['top_category']
        //var_dump($ids);
        $query = 'SELECT news_id FROM '.$server.$lang.'_catalog_news WHERE (item_id='.(int)$id.' AND type=1) OR (item_id IN ('.implode(',', $ids).') AND type=0)';
        if(!$result = mysql_query($query)) die($query.' - '.mysql_error());
        //echo $query;
        $news_ids = array();
        if(!mysql_num_rows($result)) return false;
        while($row = mysql_fetch_assoc($result)) if(!in_array($row['news_id'], $news_ids) )$news_ids[] = $row['news_id'];

        $query = 'SELECT id,category_id,title,announce_with_links as announce, date, img FROM '.$server.$lang.'_news WHERE id IN ('.implode(',', $news_ids).')  AND active=1 ORDER BY date DESC LIMIT 2';
        if(!$result = mysql_query($query)) die($query.' - '.mysql_error());
        if(!mysql_num_rows($result)) return false;
        $tpl->newBlock('news');
        $tpl->assign('link', '/news-products/?ids='.$id.'-'.implode('.',$news_ids));
        while($row = mysql_fetch_assoc($result)){
            $tpl->newBlock('news_item');
            $link = '/news-products/'.$row['category_id'].'/'.$row['id'].'.html';
            $tpl->assign(array(
                'title' => htmlspecialchars($row['title']),
                'date' => $this->getNewsDate(date('d.m.Y', strtotime($row['date']))),
                'link' => $link,
                'announce' => $row['announce']
            ));
            if($row['img']){
                $tpl->newBlock('news_img');
                $tpl->assign(array(
                    'title' => htmlspecialchars($row['title']),
                    'link' => $link,
                    'src' => $row['img']
                ));
            }
        }

    }

    function getNewsDate($date){
        $darr = explode('.', $date);
        $i = 1;
        $arr[$i++] = 'января';
        $arr[$i++] = 'февраля';
        $arr[$i++] = 'марта';
        $arr[$i++] = 'апреля';
        $arr[$i++] = 'мая';
        $arr[$i++] = 'июня';
        $arr[$i++] = 'июля';
        $arr[$i++] = 'августа';
        $arr[$i++] = 'сентября';
        $arr[$i++] = 'октября';
        $arr[$i++] = 'ноября';
        $arr[$i++] = 'декабря';
        $darr[1] = $arr[(int)$darr[1]];
        return implode(' ', $darr);
    }
    
    function getTopCategorys(){
        global $db, $server, $lang;
        $query = 'SELECT id,title FROM '.$server.$lang.'_catalog_categories WHERE clevel=1 Order By title';
        $db->query($query);
        if ($db->nf()) {
        $list = array();
        while ($db->next_record()) {
    
                $list[$db->Record['id']] = $db->Record['title'];
            }
        }
        return $list;
    }

    function getTopCategory($id){
        global $server, $lang;
        $i = 0;
        do{
            $query = 'SELECT id,parent_id, clevel FROM '.$server.$lang.'_catalog_categories WHERE id='.$id;
            if(!$result = mysql_query($query)) die(mysql_error());
            $row = mysql_fetch_assoc($result);
            $id = $row['parent_id'];
            if($i++ == 20) die('ooups...');
        }while($row['clevel'] > 1);
        return $row['id'];
    }


    function getSubTree($root_id, $show_count)
    {   global $db, $CONFIG;
        $root_id = (int) $root_id;
        $ctgr = $this->getCategory($root_id);
        $clause = "C.active=1".($ctgr ? " AND C.cleft>={$ctgr['cleft']} AND C.cright<={$ctgr['cright']}" : "");
        $db->query( $show_count
            ? "SELECT C.id, C.parent_id, C.title, C.clevel, COUNT(P.id) as cnt, "
            ."if(C.alias is not null and C.alias!='', C.alias, C.id) as alias FROM "
            ."{$this->table_prefix}_catalog_categories AS C "
            ."LEFT JOIN {$this->table_prefix}_catalog_categories AS C1 ON ( C1.cleft >=  C.cleft AND C1.cright <=  C.cright) "
            ."LEFT JOIN {$this->table_prefix}_catalog_products AS P ON category_id=C1.id AND P.active=1 "
            ."WHERE {$clause} GROUP BY C.id ORDER BY C.clevel, C.{$CONFIG['catalog_ctg_order_by']}"
            : "SELECT id, parent_id, title, clevel, rank, if(alias is not null and alias!='', alias, id) as alias FROM "
            .$this->table_prefix."_catalog_categories AS C WHERE {$clause} ORDER BY clevel, {$CONFIG['catalog_ctg_order_by']}");
        $ret = false;
        if ($db->nf()) {
            $list = array();
            while ($db->next_record()) {
                if ( ( ($ctgr ? $ctgr['id'] : 1) == $db->Record['parent_id'])
                    || isset($list[$db->Record['parent_id']])
                ) {
                    $list[$db->Record['id']] = array(
                        'id' => $db->Record['id'],
                        'alias' => $db->Record['alias'],
                        'parent_id' => $db->Record['parent_id'],
                        'title' => $db->Record['title'],
                        'clevel' => $db->Record['clevel'],
                        'cnt' => (int)$db->Record['cnt'],
                        'childs' => array()
                    );
                    if (isset($list[$db->Record['parent_id']])) {
                        $list[$db->Record['parent_id']]['childs'][] = $db->Record['id'];
                    }
                }
            }
            $curr = reset($list);
            unset($list[$curr['id']]);
            if ($curr && empty($list)) {
                $ret[$curr['id']] = $curr;
            }
            while (! empty($list)) {
                $ret[$curr['id']] = $curr;
                if (! empty($curr['childs'])) {
                    /* Добавить в результат потомка текущего елемента и сделать его текущим */
                    $id = array_shift($ret[$curr['id']]['childs']);
                    $curr = $list[$id];
                    unset($list[$id]);
                } else {
                    /* найти ближайшего предка имеющего брата, брата добавить в результат и сделать его текущим элементом */
                    $prnt_id = $curr['parent_id'];
                    while ($ret[$prnt_id] && empty($ret[$prnt_id]['childs'])) {
                        $prnt_id = $ret[$prnt_id]['parent_id'];
                    }
                    if (empty($ret[$prnt_id]['childs'])) {
                        $curr = reset($list);
                        unset($list[$curr['id']]);
                    } else {
                        $id = array_shift($ret[$prnt_id]['childs']);
                        $curr = $list[$id];
                        unset($list[$id]);
                    }
                }
                if (empty($list)) {
                    $ret[$curr['id']] = $curr;
                }
            }
        }
        return $ret;
    }

    function showTree($tree_list, $transurl, $main_field_act, $main_field_ctgr)
    {   global $tpl;
        if (! empty($tree_list)) {
            $node = reset($tree_list);
            $lvl = $start_level = $node['clevel'];
            if ($main_field_ctgr && isset($tree_list[$main_field_ctgr])) {
                $tree_list[$main_field_ctgr]['current'] = 'assortment' == $main_field_act ? true : false;
                $tree_list[$main_field_ctgr]['active'] = 'assortment' == $main_field_act ? false : true;
                $prnt_id = $tree_list[$main_field_ctgr]['id'];
                while ( $prnt_id = $tree_list[$prnt_id]['parent_id']) {
                    if (isset($tree_list[$prnt_id])) {
                        $tree_list[$prnt_id]['active'] = true;
                    }
                }
            }
            $tpl->newBlock('block_tree');
            foreach ($tree_list as $id => $node) {
                $tpl->newBlock('block_node');
                $assign = array(
                    'transurl' => $transurl,
                    'title' => $node['title'],
                    'id' => $node['id'],
                    'level' => $node['clevel'],
                    'cnt' => $node['cnt'],
                    'parent_id' => $node['parent_id'],
                    'link' => $this->getCtgrLink($transurl, $node['alias'], $node['id']),
                    'active' => $node['active'] ? 'active' : '',
                    'current' => $node['current'] ? 'current' : ''
                );
                if ($node['active']) {
                    $tpl->newBlock('block_node_item_active');
                    $tpl->assign($assign);
                } else if ($node['current']) {
                    $tpl->newBlock('block_node_item_current');
                    $tpl->assign($assign);
                } else {
                    $tpl->newBlock('block_node_item');
                    $tpl->assign($assign);
                }
                if ($node['cnt'] > 0) {
                    $tpl->newBlock('block_node_item_cnt');
                    $tpl->assign('cnt', $node['cnt']);
                }
                if ($node['clevel'] > $lvl) {
                    $tpl->newBlock('block_start_level');
                }
                while ($node['clevel'] < $lvl--) {
                    $tpl->newBlock('block_end_level');
                }
                $lvl = $node['clevel'];
            }
            while ($start_level < $lvl--) {
                $tpl->newBlock('block_node');
                $tpl->newBlock('block_end_level');
            }
        }
    }

    function get_tree_2($transurl, $show_count) {
        global $db, $CONFIG, $server, $lang;
        $show_count;
        $query = $show_count
            ? 'SELECT C.id, C.parent_id, C.title, C.clevel, C.rank, COUNT(P.id) as cnt, '
            .'if(C.alias is not null and C.alias!=\'\', C.alias, C.id) as alias FROM '
            .$this->table_prefix.'_catalog_categories AS C
			   LEFT JOIN '.$this->table_prefix.'_catalog_categories AS C1 ON ( C1.cleft >=  C.cleft AND C1.cright <=  C.cright)
			   LEFT JOIN '.$this->table_prefix.'_catalog_products AS P ON category_id=C1.id AND P.active=1 WHERE C.active=1
			   GROUP BY C.id ORDER BY C.clevel, C.'.$CONFIG['catalog_ctg_order_by']
            : 'SELECT id, parent_id, title, clevel, rank, if(alias is not null and alias!=\'\', alias, id) as alias FROM '
            .$this->table_prefix.'_catalog_categories WHERE active=1 ORDER BY clevel, '.$CONFIG['catalog_ctg_order_by'];
        $db->query($query);
        if ($db->nf() > 0) {
            for ($i=0; $i < $db->nf(); $i++) {
                $db->next_record();
                $category[$i]['id']		= $db->f('id');
                $category[$i]['parent_id'] = $db->f('parent_id');
                $category[$i]['name']	  = $db->f('title');
                $category[$i]['level']	 = $db->f('clevel');
                $category[$i]['alias']	 = $db->f('alias');
                $category[$i]['cnt']	 = $show_count ? $db->f('cnt') : 0;
            }
            $html = $this->get_sub_tree($category, $transurl, 1, 0, $show_count);
        } else {
            $html = '';
        }
        return $html;
    }


    function get_sub_tree($array, $transurl, $id = 0, $level = 0, $show_count = 0) {
        $html = $level == 0 ? '<ul id="mn">' : '<ul>';
        for ($i=0; $i < ($iii = sizeof($array)); $i++) {
            if ( (($id == $array[$i]['parent_id']) && ($array[$i]['level'] == ($level+1)))
                || (($id == $array[$i]['parent_id']) && ($level == 0))
            ) {
                $array_id		= $array[$i]['id'];
                $array_level	= $array[$i]['level'];
                $sub_html = $this->get_sub_tree($array, $transurl, $array_id, $array_level, $show_count);
                $cnt = $array[$i]['cnt'] ? " (".$array[$i]['cnt'].")" : "";

                $link = $this->getCtgrLink($transurl, $array[$i]['alias'], $array_id);

                if ($array[$i]['id'] == $_GET['parent']) {
                    $html .= '<li class="current"><a href="'.$link.'">'.$array[$i]['name'].'</a>'.$cnt.$sub_html.'</li>';
                } else {
                    $html .= '<li><a href="'.$link.'">'.$array[$i]['name'].'</a>'.$cnt.$sub_html.'</li>';
                }
            }
        }
        $html .= '</ul>';
        $html == '<ul id="mn"></ul>' ? $html = '' : $html;
        $html == '<ul></ul>' ? $html = '' : $html;
        return $html;
    }

    function getCostWithTaxes($sum)
    {   global $CONFIG;
        $sum = round($sum*100);
        if ($sum > 0) {
            $arr = $this->getTaxes($sum);
            if (is_array($arr)) {
                for($i = 0; $i < sizeof($arr); $i++) {
                    $total += (($arr[$i]['value']/100)*$sum);
                }
                return round($sum + $total)/100;
            }
            return $sum/100;
        } else {
            return $CONFIG['catalog_default_price'];
        }
    }

    function getTaxes()
    {   global $CONFIG, $db1;
        static $_cache = false;

        if (! $_cache) {
            if ($this->issetShopMod()) {
                $db1->query('SELECT rank, sum(percent) value FROM '.$this->table_prefix.'_shop_taxes '
                    .'WHERE active = 1 GROUP BY rank ORDER BY rank');
                if ($db1->nf() > 0) {
                    $_cache = array();
                    for ($i = 0; $i < $db1->nf(); $i++) {
                        $db1->next_record();
                        $_cache[$i]['rank'] = $db1->f('rank');
                        $_cache[$i]['value'] = $db1->f('value');
                    }
                }
            }
        }

        return $_cache;
    }

    function getTotalProdsInBasket() {
        if (sizeof($_SESSION["order"]["items"]) > 0) {
            foreach($_SESSION["order"]["items"] as $item_idx => $item_array) {
                $flag = FALSE;
                foreach($item_array as $it_idx => $it_array) {
                    if (is_array($it_array)) {
                        $it_array['razm']	= $it_idx;
                        $arr[]				= $it_array;
                        $flag				= TRUE;
                    }
                }
                if (!$flag) {$arr[] = $item_array;}
            }
            $total = 0;
            for ($i = 0; $i < sizeof($arr); $i++) {
                $total += $arr[$i]['quantity'];
            }
        } else {
            $total = 0;
        }
        return $total;
    }

    function getCategsTree($abort_ids = false, $with_root = false){
        GLOBAL $CONFIG, $server, $lang, $db;
        $db->query('SELECT id, alias, title, parent_id, clevel, cleft, cright FROM '
            .$this->table_prefix.'_catalog_categories WHERE '.($with_root ? '1' : 'clevel>0')
            .' ORDER BY cleft');
        $ret = array();
        $clr = array(); /* [cleft, cright] */
        if ($db->nf() > 0) {
            while ($db->next_record()) {
                if ($abort_ids && in_array($db->Record['id'], $abort_ids)) {
                    $clr[] = array($db->f('cleft'), $db->f('cright'));
                }
                $add = true;
                if (! empty($clr)) {
                    while ($add && list(,$c)=each($clr)) {
                        if ($db->Record['cleft'] >= $c[0] && $db->Record['cleft'] < $c[1]) {
                            $add = false;
                        }
                    }
                }
                if ($add) {
                    $ret[] = array(
                        'id' => $db->Record['id'],
                        'parent_id' => $db->Record['parent_id'],
                        'title' => $db->f('title'),
                        'level' => $db->f('clevel'),
                        'alias' => $db->Record['alias'] ? $db->Record['alias'] : $db->Record['id']
                    );
                }
            }
        }
        return $ret;
    }

    function getCategsTreeWithProducts($abort_ids = false, $with_root = false){
        GLOBAL $CONFIG, $server, $lang, $db,$db2;
        $db->query('SELECT id, alias, title, parent_id, clevel, cleft, cright FROM '
                   .$this->table_prefix.'_catalog_categories WHERE '.($with_root ? '1' : 'clevel>0')
                   .' ORDER BY cleft');
        $ret = array();
        $clr = array(); /* [cleft, cright] */
        if ($db->nf() > 0) {
            while ($db->next_record()) {
                if ($abort_ids && in_array($db->Record['id'], $abort_ids)) {
                    $clr[] = array($db->f('cleft'), $db->f('cright'));
                }
                $add = true;
                if (! empty($clr)) {
                    while ($add && list(,$c)=each($clr)) {
                        if ($db->Record['cleft'] >= $c[0] && $db->Record['cleft'] < $c[1]) {
                            $add = false;
                        }
                    }
                }
                if ($add) {
                    $array=$db2->getArrayOfResult('Select id,product_title From '. $server . $lang . '_catalog_products Where category_id='.$db->Record['id'].' and active=1');
                    $ret[] = array(
                        'id' => $db->Record['id'],
                        'parent_id' => $db->Record['parent_id'],
                        'title' => $db->f('title'),
                        'level' => $db->f('clevel'),
                        'alias' => $db->Record['alias'] ? $db->Record['alias'] : $db->Record['id'],
                        'pr'=>$array
                    );
                }
            }
        }
        return $ret;
    }
    function showCtgrTree($list, $tpl_param = false)
    {   global $tpl;

        $_tpl = $tpl_param ? $tpl_param : $tpl;
        $first = reset($list);
        if ($first) {
            $level = $first['level']-1;
            foreach ($list as $node) {
                $node['level2'] = $level;
                $_tpl->newBlock('block_tree_level');
                $_tpl->assign($node);
                if ($node['level'] > $level) {
                    $_tpl->newBlock('block_tree_level_start');
                    $_tpl->assign($node);
                    $level = $node['level'];
                }
                $_tpl->newBlock('block_tree_node');
                $_tpl->assign($node);
                if(isset($node['pr']))
                {
                    $_tpl->newBlock('block_tree_node_products');
                    foreach($node['pr'] as $prod){
                        $_tpl->newBlock('block_tree_node_product');
                        $_tpl->assign($prod);
                    }
                }
                if ($node['level'] < $level) {
                    while ($node['level'] < $level) {
                        $_tpl->newBlock('block_tree_level_end');
                        $node['level'] = $level;
                        $_tpl->assign($node);
                        $level--;
                    }
                }
            }
            if ($level >= $first['level']) {
                while ($level >= $first['level']) {
                    $_tpl->newBlock('block_tree_level');
                    $_tpl->newBlock('block_tree_level_end');
                    $node['level'] = $level;
                    $_tpl->assign($node);
                    $level--;
                }
            }
        }
        return $first ? true : false;
    }

    function getCategsList($ids){
        GLOBAL $db;

        $ret = false;
        if (is_array($ids)) {
            $ids = array_map("intval", $ids);
        }
        $ret = array();
        if (! empty($ids)) {
            $db->query("SELECT id, title FROM {$this->table_prefix}_catalog_categories WHERE id IN ("
                .implode(", ", $ids).")");
            while ($db->next_record()) {
                $ret[$db->Record['id']] = array(
                    'id' => $db->Record['id'],
                    'title' => $db->Record['title']
                );
            }
        }
        return $ret;
    }


    function getSearchProds($search_data, $max_rows, $start, $only_active = true)
    {
        global $db, $server, $lang, $CONFIG, $baseurl;
        $sort_arr = array(
            'price' => 'price_1',
            'name' => 'product_title',
            'ptype' => 'ptype_name'
        );
        $pname		= My_Sql::escape($search_data["pname"]);
        $code		= My_Sql::escape($search_data["code"]);
        $ptype_id	= (int)$search_data["ptype_id"];
        $price1		= abo_float($search_data["pprice1"]);
        $price2		= abo_float($search_data["pprice2"]);
        $group_id	= (int)$search_data["group_id"];
        $ctgr_id	= (int)$search_data["ctgr_id"];
        $availability	= (int)$search_data["availability"];
        $max_rows	= $max_rows>=1 ? (int)$max_rows : $CONFIG['catalog_max_rows'];
        $start		= $start >=1 ? (int)($start - 1) :  0;
        $gr_filter = $gr_table = $gr_fields = $pt_filter = $pt_table = $pt_fields ='';

        $tables = array($this->table_prefix.'_catalog_products p INNER JOIN '
        .$this->table_prefix.'_catalog_categories c ON p.category_id=c.id'
        .' LEFT JOIN '.$this->table_prefix.'_catalog_ptypes AS pt ON p.ptype_id=pt.id');

        $fields = array();
        $filter = $only_active ? array('p.active = 1') : array('1');
        $tbl['search_params'] = '&sdata[cnt]='.$max_rows;
        $order_by = 'p.rank';
        if ($search_data["sort"] && array_key_exists($search_data["sort"], $sort_arr)) {
            $order_by = "{$sort_arr[$search_data["sort"]]}";
            $tbl['search_params'] .= "&sdata[sort]={$search_data["sort"]}";
        }
        if ($ctgr_id) {
            $line .= " AND p.category_id={$ctgr_id}";
            $tbl['search_params'] .= "&sdata[ctgr_id]={$ctgr_id}";
        }
        if ($pname) {
            $pname = preg_replace(array('/\s+/', '/\W/'), array('_', ''), $pname);
            $pname = explode('_', $pname);
            $line .= ' AND p.product_title LIKE "%'.implode('%" AND p.product_title LIKE "%', $pname).'%"';
            $tbl['search_params'] .= "&sdata[pname]=".implode(' ', $pname);
        }
        if ($code) {
            $line .= ' AND p.product_code LIKE "%'.$code.'%"';
            $tbl['search_params'] .= "&sdata[code]=".$code;
        }
        if ($availability) {
            $line .= " AND p.product_availability!=0 AND p.product_availability!='' AND p.product_availability!='".$this->_msg['no']."' AND p.product_availability IS NOT NULL";
            $tbl['search_params'] .= "&sdata[availability]=1";
        }
        if ($ptype_id) {
            $line .= ' AND p.ptype_id='.$ptype_id;
            $tbl['search_params'] .= "&sdata[ptype_id]=".$ptype_id;
            $filter[] = ' p.ptype_id=pt.id ';
        }
        if ($price1>=1) {
            $line .= ' AND p.price_1>='.$price1;
            $tbl['search_params'] .= "&sdata[pprice1]=".$price1;
        }
        if ($price2>=1) {
            $line .= ' AND p.price_1<='.$price2;
            $tbl['search_params'] .= "&sdata[pprice2]=".$price2;
        }

        if ($group_id) {
            $line .= ' AND p.group_id = '.$group_id;
            $tbl['search_params'] .= "&sdata[group_id]=".$group_id;
            $filter[] = 'p.group_id=g.id';
            $tables[] = $this->table_prefix.'_catalog_groups g';
            $fields[] = 'g.group_title';
        }

        $dynamic = 0;
        $dynamic_sql = '';
        if (is_array($search_data["dynamic"])) {
            foreach($search_data["dynamic"] as $id => $value)	{
                if (is_array($value)) {
                    $tmp_dynamic_sql = '';
                    if (intval($value[1])) {
                        $tmp_dynamic_sql .= 'pv.property_id = '.$id;
                        $tmp_dynamic_sql .= ' AND ROUND(pv.value) >= '.intval($value[1]);
                        $tbl['search_params'] .= "&sdata[dynamic][".$id."][1]=".intval($value[1]);
                    }
                    if (intval($value[2]))
                    {
                        $tmp_dynamic_sql .= ($tmp_dynamic_sql) ? '' : 'pv.property_id = '.$id;
                        $tmp_dynamic_sql .= ' AND ROUND(pv.value) <= '.intval($value[2]);
                        $tbl['search_params'] .= "&sdata[dynamic][".$id."][1]=".intval($value[1]);
                    }
                    if ($tmp_dynamic_sql) {
                        $dynamic_sql .= (($dynamic) ? ' OR (' : '(').$tmp_dynamic_sql.')';
                        $dynamic++;
                    }
                }	else {
                    $value = str_replace(array("%", "_"), array("\%", "\_"), My_Sql::escape((trim($value))));
                    if ($value) {
                        $dynamic_sql .= (($dynamic) ? ' OR ' : '').'(pv.property_id = '.$id;
                        $dynamic_sql .= ' AND pv.value LIKE "%'.$value.'%")';
                        $dynamic++;
                        $tbl['search_params'] .= "&sdata[dynamic][".$id."]=".$value;
                    }
                }
            }
        }
        if ($dynamic) {
            $line .= ' AND ('.$dynamic_sql.')';
            $tables[] = $this->table_prefix.'_catalog_property_values pv';
            $filter[] = 'p.id=pv.product_id';
        }

        $tbl['tables']	= implode(", ", $tables);
        $tbl['where']	= implode(" AND ", $filter).$line;

        $clnt_price = 'prod_price_'.(@$_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : '1');

        // если не указаны динамические свойства установим количество в 1
        if (!$dynamic) $dynamic = 1;
        $query = 'SELECT p.id, count(*)='.$dynamic.' AS dinamic_true, p.product_title, p.product_inf, p.image_middle, p.image_big,p.bonus,p.weight,p.file_alarm,p.dimensions,p.products_synonyms,p.product_count,
              			product_code, p.product_availability, p.price_1, p.price_2, p.price_3, p.price_4, p.price_5, p.currency, p.rank,
              			p.active, if(p.alias is not null and p.alias!=\'\', p.alias, p.id) as prod_alias,
              			if(c.alias is not null and c.alias!=\'\', c.alias, c.id) as ctg_alias, ptype_name, ptype_id'.$pt_fields.' '.$gr_fields.'
              		FROM '.$tbl['tables'].' WHERE '.$tbl['where']
            .' GROUP BY id HAVING count(*)='.$dynamic
            .' ORDER BY '.$order_by.' LIMIT '.($start*$max_rows).', '.$max_rows;
        //echo $query;
        $arr = false;
        $db->query($query);
        if ($db->nf() > 0) {
            $arr = array();
            $add_to_cart_link = Core::formPageLink($CONFIG['shop_page_link'], $CONFIG['shop_page_address'], $lang);
            $add_to_cart_link .= (FALSE === strpos($add_to_cart_link, "?") ? "?" : "&")."action=addtocart&id=";
            $start_nn = $start*$max_rows;
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                if (!$db->f('dinamic_true')) continue;

                $img = $db->f('image_middle');
                $img = $img && file_exists(RP.$CONFIG['catalog_img_path'].$img) ? '/'.$CONFIG['catalog_img_path'].$img : "";
                $b_img = $db->f('image_big');
                $b_img = $b_img && file_exists(RP.$CONFIG['catalog_img_path'].$b_img) ? '/'.$CONFIG['catalog_img_path'].$b_img : "";
                $arr[$i]['prod_num']		= $start_nn+$i+1;
                $arr[$i]['prod_id']			= $prod_id = $db->f('id');
                $arr[$i]['prod_link']		= $this->getProdLink($baseurl, $db->f('prod_alias'), $prod_id, $db->f('ctg_alias'));
                $arr[$i]['prod_title']		= $db->f('product_title');
                $arr[$i]['prod_descr']		= $db->f('product_inf');
                $arr[$i]['prod_code']		= $db->f('product_code');
                $arr[$i]['prod_image']		= $img;
                $arr[$i]['prod_image_big']	= $b_img;
                $arr[$i]['prod_price_1']	= $this->getCostWithTaxes($db->f('price_1'));
                $arr[$i]['prod_price_2']	= $this->getCostWithTaxes($db->f('price_2'));
                $arr[$i]['prod_price_3']	= $this->getCostWithTaxes($db->f('price_3'));
                $arr[$i]['prod_price_4']	= $this->getCostWithTaxes($db->f('price_2'));
                $arr[$i]['prod_price_5']	= $this->getCostWithTaxes($db->f('price_3'));
                $arr[$i]['prod_price']		= $arr[$i][$clnt_price];
                $arr[$i]['prod_currency']	= $CONFIG['catalog_currencies'][$db->f('currency')];
                $arr[$i]['prod_currency_name']	= $this->currencies[$db->f('currency')];
                $arr[$i]['prod_availability']	= $db->f('product_availability');
                $arr[$i]['add_to_cart_link']= $add_to_cart_link.$prod_id;
                $arr[$i]['group_title']		= $db->f('group_title');
                $arr[$i]['ptype_name']		= $db->f('ptype_name');
                $arr[$i]['ptype_link']		= $db->f('ptype_link');
                $arr[$i]['rank']    		= $db->f('rank');
                $arr[$i]['active']    		= $db->f('active');
                $arr[$i]['bonus']    		= $db->f('bonus');
                $arr[$i]['weight']    		= $db->f('weight');
                $arr[$i]['file_alarm']    		= $db->f('file_alarm');
                $arr[$i]['dimensions']    		= $db->f('dimensions');
                $arr[$i]['products_synonyms']    		= $db->f('products_synonyms');
                $arr[$i]['product_count']    		= $db->f('product_count');
            }
            $this->setPropValForProdList($arr);
            $arr["nav_str"] = $tbl;
            //временная заглушка на количество страниц в результатах поиска
            $db->query('SELECT p.id	FROM '.$tbl['tables'].' WHERE '.$tbl['where'].' GROUP BY id HAVING count(*)='.$dynamic);
            $arr["nav_str"]["result"] = $db->nf();
            //echo 'SELECT p.id	FROM '.$tbl['tables'].' WHERE '.$tbl['where'].' GROUP BY id HAVING count(*)='.$dynamic." - ".$arr["nav_str"]["result"];
            $db->free();
        }
        //
        return $arr;
    }

    function getSearchLinePrice() {
        global $db, $lang, $CONFIG;
        $arr[0]['search_price'] = '< $500';
        $arr[0]['search_price_range'] = '1';
        $arr[1]['search_price'] = '> $500';
        $arr[1]['search_price_range'] = '2';
        return $arr;
        return FALSE;
    }

    function getSearchLineType($current_type = 0) {
        global $db, $server, $lang, $CONFIG;

        $db->query('SELECT * FROM '.$this->table_prefix.'_catalog_groups ORDER BY '.$CONFIG['catalog_group_order_by']);
        if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]['search_type_id']	= $db->f('id');
                $arr[$i]['search_type']		= $db->f('group_title');
                $arr[$i]['selected']		= ($db->f('id') == $current_type) ? ' selected' : '';
            }

            return $arr;
        }
        return FALSE;
    }

    function getSearchLineByArticul() {
        global $db, $server, $lang, $CONFIG;
        $db->query('SELECT * FROM '.$this->table_prefix.'_catalog_groups ORDER BY '.$CONFIG['catalog_group_order_by']);
        if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]['search_art_id']	= $db->f('id');
                $arr[$i]['search_art']		= $db->f('group_title');
            }
            return $arr;
        }
        return FALSE;
    }

    function getDynamicPropertyDefValues($title, $group_id) {
        global $db, $server, $lang, $CONFIG;
        $title     = $db->escape($title);
        $group_id = (int)$group_id;
        if (!$group_id || !$title) return FALSE;
        $db->query('SELECT c.value AS value
                        FROM '.$this->table_prefix.'_catalog_properties_table a,
                             '.$this->table_prefix.'_catalog_properties b,
                             '.$this->table_prefix.'_catalog_properties_def_values c
                        WHERE a.property_title = "'.$title.'" AND
                              a.id = b.property_id AND
                              b.group_id = '.$group_id.' AND
                              a.id = c.property_id
                        ORDER BY c.value');
        if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]['razm_val'] = $db->f('value');
            }
            return $arr;
        }
        return FALSE;
    }

    function getProductProperies($prod_id, $group_id)
    {   global $db, $CONFIG;

        $ret = false;
        $group_prop = $this->getPropList($group_id, true, false);
        //var_dump($group_prop);
        $prop_vals = $this->getProdPropVal($prod_id);
        //var_dump($prop_vals);
        if ($group_prop && $prop_vals) {
            $i = 0;
            foreach ($group_prop as $prop) {
                if (4 == $prop['prt_type'] || array_key_exists($prop['prt_id'], $prop_vals) || true) {
                    $ret[$i]['value']   = array_key_exists($prop['prt_id'], $prop_vals) ? $prop_vals[$prop['prt_id']] : '';
                    $ret[$i]['title']   = $prop['prt_title'];
                    $ret[$i]['name']    = $prop['prt_name'];
                    $ret[$i]['type']    = $prop['prt_type'];
                    $ret[$i]['prefix']  = $prop['prt_prefix'];
                    $ret[$i]['suffix']  = $prop['prt_suffix'];
                    $ret['dynamic_properties'][0]['property_'.$prop['prt_name'].'_title'] = $ret[$i]['title'];
                    $ret['dynamic_properties'][0]['property_'.$prop['prt_name'].'_value'] = $ret[$i]['value'];
                    $ret['dynamic_properties'][0]['property_'.$prop['prt_name'].'_prefix'] = $ret[$i]['prefix'];
                    $ret['dynamic_properties'][0]['property_'.$prop['prt_name'].'_suffix'] = $ret[$i]['suffix'];
                    $i++;
                }
            }
        }

        return $ret;
    }

    function getProdPropVal($id) {
        global $db;
        $ret = false;
        $db->query('SELECT * FROM '.$this->table_prefix.'_catalog_property_values WHERE product_id='.intval($id));

        while ($db->next_record()) {
            $ret[$db->Record['property_id']] = $db->Record['value'];
        }

        $db->query('SELECT df.property_id,df.value FROM '.$this->table_prefix.'_catalog_properties_def_values as df
                   INNER JOIN '.$this->table_prefix.'_catalog_order_prop_val as v ON (v.prop_val_id = df.id) WHERE v.prod_id='.intval($id));

        while ($db->next_record()) {
            if(isset($ret[$db->Record['property_id']])) $ret[$db->Record['property_id']][] = $db->Record['value'];
            else $ret[$db->Record['property_id']] = array($db->Record['value']);
        }

        return $ret;
    }

    function getPrevButton($arr = array()) {
        global $db, $lang, $CONFIG, $baseurl;
        if (is_array($arr) && sizeof($arr) > 0) {
            if ($arr[0]['l_id']) {
                $arr2[0]['onclick_prev']      = ' onclick="document.location.href=\''.$baseurl.'&action=shwprd&id='.$arr[0]['l_id'].'\'"';
                $arr2[0]['image_prev']        = '/i/left.gif';
            } else {
                $arr2[0]['onclick_prev']      = ' disabled';
                $arr2[0]['prod_prev']         = '';
                $arr2[0]['image_prev']        = '/i/left_dis.gif';
            }
            return $arr2;
        }
        return FALSE;
    }

    function getNextButton($arr = array()) {
        global $db, $lang, $CONFIG, $baseurl;
        if (is_array($arr) && sizeof($arr) > 0) {
            if ($arr[0]['r_id']) {
                $arr2[0]['onclick_next']      = ' onclick="document.location.href=\''.$baseurl.'&action=shwprd&id='.$arr[0]['r_id'].'\'"';
                $arr2[0]['image_next']        = '/i/right.gif';
            } else {
                $arr2[0]['onclick_next']      = ' disabled';
                $arr2[0]['prod_next']         = '';
                $arr2[0]['image_next']        = '/i/right_dis.gif';
            }
            return $arr2;
        }
        return FALSE;
    }

    function getProductSearch($search)
    {   global $db, $CONFIG, $baseurl;
        if(is_numeric($search)){
            $search = 'a.id ='.(int)$search;
        }
        else{
            $search = "a.alias ='".$search."'";
        }
        $ret = false;

        if ($search) {
            $clnt_price = 'price_'.(@$_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : '1');
            $db->query('SELECT	a.id AS id,
							a.category_id AS category_id,
							a.alias,
							a.product_count,
							a.currency,
							a.hits,
							a.product_title AS product_title,
							a.product_code AS product_code,
							a.product_description AS product_description,
							a.product_description_shop AS product_description_shop,
							a.price_5 AS price_5,
							a.price_5 AS price_1,
							a.image_middle AS image_middle,
							a.product_availability AS product_availability,
							a.active AS active,
							if(a.alias is not null and a.alias!=\'\', a.alias, a.id) as product_alias,
							c.title as c_title,
							c.synonyms as synonyms
						FROM '.$this->table_prefix.'_catalog_products a
						INNER JOIN '.$this->table_prefix.'_catalog_categories c ON a.category_id = c.id
						WHERE '.$search);
            if ($db->next_record()) {
	            
                $ret['id']						= $db->f('id');
                $ret['category_id']				= $db->f('product_description');
                $ret['category_title']			= $db->f('c_title');
                $ret['product_code']			= $db->f('product_code');
                $ret['product_title']			= $db->f('product_title');
                $ret['product_description']		= $db->f('product_description');
                $ret['product_description_shop']		= $db->f('product_description_shop');
                $ret['product_currency']		= $db->f('currency') && array_key_exists($db->f('currency'), $CONFIG['catalog_currencies'])
                    ? $CONFIG['catalog_currencies'][$db->f('currency')] : '';
                $ret['product_currency_name']	= $this->currencies[$db->f('currency')];

                $ret['product_price_5']			= $this->getCostWithTaxes($db->f('price_5'));
                $ret['product_price']			= $ret["product_{$clnt_price}"];
                $ret['price']			= $ret[$clnt_price];
                $ret['price_1']			= $db->f('price_1');
                $ret['product_image']			= $db->f('image_middle');
                $ret['product_availability']	= $db->f('product_availability');
                $ret['product_alias']        	= $db->f('product_alias');
                $ret['active']					= $db->f('active');
                $ret['hits']					= $db->f('hits');
                $ret['synonyms']					= $db->f('synonyms');
                
            }
        }
        return $ret;
    }

    function getProduct($id)
    {   global $db, $CONFIG, $baseurl;

        $id = (int)$id;
        if ($this->cache["prod{$id}"]) {
            return $this->cache["prod{$id}"];
        }
        $ret = false;
        if ($id) {
            $clnt_price = 'price_'.(@$_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : '1');
            $db->query('SELECT	a.id AS id,
							a.category_id AS category_id,
							a.group_id AS group_id,
							a.alias,
							a.atrade,
							a.bonus,
							a.weight,
							a.file_alarm,
							a.dimensions,
							a.products_synonyms as product_synonym,
							a.product_count,
							a.nyml,
							a.archive,
							a.swf_file,
							a.swf_text,
							a.ptype_id AS ptype_id,
							a.product_code AS product_code,
							a.product_title AS product_title,
							a.product_inf AS product_inf,
							a.product_description AS product_description,
							a.product_description_shop AS product_description_shop,
							a.currency AS currency,
							a.price_1 AS price_1,
							a.price_2 AS price_2,
							a.price_3 AS price_3,
							a.price_4 AS price_4,
							a.price_5 AS price_5,
							a.image_middle AS image_middle,
							a.image_big AS image_big,
							a.product_availability AS product_availability,
							a.active AS active,
							a.rank AS rank,
							a.seo_title,
							a.dates,
							a.link,
							a.leader,
							a.novelty,
							a.img_desc,
							a.description AS description,
							a.keywords AS keywords,
							if(a.alias is not null and a.alias!=\'\', a.alias, a.id) as product_alias,
							b.ptype_name	AS ptype_title,
							b.link	AS ptype_link,
							c.title AS categ_title,
							c.products_synonyms AS products_synonyms,
							d.group_title AS prod_grp,
							c.page_description AS ctg_description,
							c.keywords AS ctg_keywords,
							if(c.alias is not null and c.alias!=\'\', c.alias, c.id) as ctg_alias
						FROM '.$this->table_prefix.'_catalog_products a
						INNER JOIN '.$this->table_prefix.'_catalog_categories c ON a.category_id = c.id
						LEFT JOIN '.$this->table_prefix.'_catalog_ptypes b ON a.ptype_id = b.id
						LEFT JOIN '.$this->table_prefix.'_catalog_groups d ON d.id = a.group_id
						WHERE a.id = '.$id);
            if ($db->next_record()) {
                $ret['id']						= $db->f('id');
                $ret['alias']					= $db->f('alias');
                $ret['date']					= $db->f('dates');
                $ret['archive']					= $db->f('archive');
                $ret['nyml']					= $db->f('nyml');
                $ret['leader']					= $db->f('leader');
                $ret['novelty']					= $db->f('novelty');
                $ret['link']					= $db->f('link');
                $ret['atrade']					= $db->f('atrade');
                $ret['bonus']					= $db->f('bonus');
                $ret['weight']					= $db->f('weight');
                $ret['file_alarm']					= $db->f('file_alarm');
                $ret['dimensions']					= $db->f('dimensions');
                $ret['product_synonym']		    = $db->f('product_synonym');
                $ret['product_count']					= $db->f('product_count');
                $ret['swf_file']					= $db->f('swf_file');
                $ret['swf_text']					= $db->f('swf_text');
                $ret['category_id']				= (int)$db->f('category_id');
                $ret['product_categ']			= $db->f('categ_title');
                $ret['group_id']				= $db->f('group_id');
                $ret['product_group']			= $db->f('prod_grp');
                $ret['ptype_id']				= $db->f('ptype_id');
                $ret['product_ptype_title']		= $db->f('ptype_title');
                $ret['product_ptype_link']		= $db->f('ptype_link');
                $ret['product_title_a']			= mb_strtolower($db->f('product_title'));
                $ret['product_title']			= $db->f('product_title');
                $ret['product_code']		    = $db->f('product_code');
                $ret['product_inf']				= $db->f('product_inf');
                $ret['product_description']		= $db->f('product_description');
                $ret['product_description_shop']		= $db->f('product_description_shop');
                $ret['product_currency']		= $db->f('currency') && array_key_exists($db->f('currency'), $CONFIG['catalog_currencies'])
                    ? $CONFIG['catalog_currencies'][$db->f('currency')] : '';
                $ret['product_currency_name']	= $this->currencies[$db->f('currency')];
                $ret['product_price_1']			= $this->getCostWithTaxes($db->f('price_1'));
                $ret['product_price_2']			= $this->getCostWithTaxes($db->f('price_2'));
                $ret['product_price_3']			= $this->getCostWithTaxes($db->f('price_3'));
                $ret['product_price_4']			= $this->getCostWithTaxes($db->f('price_4'));
                $ret['product_price_5']			= $this->getCostWithTaxes($db->f('price_5'));
                $ret['product_price']			= $ret["product_{$clnt_price}"];
                $ret['price_1']			= ($db->f('price_1'));
                $ret['price_2']			= ($db->f('price_2'));
                $ret['price_3']			= ($db->f('price_3'));
                $ret['price_4']			= ($db->f('price_4'));
                $ret['price_5']			= ($db->f('price_5'));
                $ret['price']			= $ret[$clnt_price];
                $ret['product_image']			= $db->f('image_middle');
                $ret['product_image_big']		= $db->f('image_big');
                $ret['product_availability']	= $db->f('product_availability');
                $ret['product_alias']        	= $db->f('product_alias');
                $ret['active']					= $db->f('active');
                $ret['rank']					= $db->f('rank');
                $ret['seo_title']				= $db->f('seo_title');
                $ret['description']				= $db->f('description');
                $ret['keywords']				= $db->f('keywords');
                $ret['spisok']					= $baseurl.'&action=assortment&id='.$db->f('category_id');
                $ret['ctg_description']			= $db->f('ctg_description');
                $ret['ctg_keywords']			= $db->f('ctg_keywords');
                $ret['ctg_alias']    			= $db->f('ctg_alias');
                $ret['img_desc']                = $db->f('img_desc');
                $ret['products_synonyms']       = $db->f('products_synonyms') ? $db->f('products_synonyms') : $db->f('product_synonym');
                $this->cache["prod{$id}"] = $ret;
            }
        }
        return $ret;
    }

    function isValidProdAlias($alias, $prod_id = 0)
    {   global $db;

        $ret = true;
        $alias = My_Sql::escape($alias);
        $prod_id = (int) $prod_id;
        if ($alias) {
            $db->query("SELECT COUNT(*) FROM {$this->table_prefix}_catalog_products WHERE (alias='{$alias}' OR id='{$alias}')"
                .($prod_id ? " AND id<>{$prod_id}" : ""));
            $db->next_record();
            $ret = $db->Record[0] > 0 ? false : true;
        }
        return $ret;
    }

    function isValidCtgrAlias($alias, $ctgr_id = 0)
    {   global $db;

        $ret = true;
        $alias = My_Sql::escape($alias);
        $ctgr_id = (int) $ctgr_id;
        if ($alias) {
            $db->query("SELECT COUNT(*) FROM {$this->table_prefix}_catalog_categories WHERE (alias='{$alias}' OR id='{$alias}')"
                .($ctgr_id ? " AND id<>{$ctgr_id}" : ""));
            $db->next_record();
            $ret = $db->Record[0] > 0 ? false : true;
        }
        return $ret;
    }

    function getTopCategoryByLevel($id, $level){
        do{
            $cat = $this->getCategory($id);
            //var_dump($cat);
            $id = $cat['parent_id'];
        }while($cat['clevel'] != $level && $cat);
        return $cat ? $cat['id'] : false;

    }

    function getProductProperty($product_id, $property_id){
        global $server,$lang;
        $query = 'SELECT value FROM '.$server.$lang.'_catalog_property_values WHERE product_id='.(int)$product_id.' AND property_id='.(int)$property_id;
        if(!$result = mysql_query($query)) die(mysql_error());
        $row = mysql_fetch_assoc($result);
        return $row ? $row['value'] : false;
    }

    function getCategory($id)
    {   global $db;

        $id = (int)$id;
        $ret = false;
        if (isset($this->cache["ctgr{$id}"])) {
            $ret = $this->cache["ctgr{$id}"];
        } else {
            $ret = $db->getArrayOfResult("SELECT *, if(alias is not null and alias!='', alias, id) as ctgr_alias FROM "
                .$this->table_prefix."_catalog_categories WHERE id={$id}");
            $this->cache["ctgr{$id}"] = $ret ? $ret[0] : false;
        }
        return $this->cache["ctgr{$id}"];
    }

    function getProdsOfSubCategs($prp = NULL, $how = '', $start = 1, $parent_title, $ids = array(), $query = '') {
        global $db, $server, $lang, $CONFIG, $baseurl;

        $prp = (int)$prp;
        $how = (int)$how;
        $start = $start > 1 ? intval($start) : 1;
        $ctgr = $this->getCategory($prp);
        $clnt_price = 'prod_price_'.(@$_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : '1');
        $q = $db->escape($query);
        if ($ctgr || sizeof($ids) || strlen($query) > 1) {
            if(strlen($query) > 1){

                $db->query('SELECT DISTINCT P.id,P.category_id
                               FROM '.$this->table_prefix.'_catalog_products AS P
                               INNER JOIN '.$this->table_prefix.'_catalog_categories AS c ON c.id = P.category_id
							   LEFT JOIN '.$this->table_prefix.'_catalog_groups AS G ON P.group_id = G.id
                               LEFT JOIN '.$this->table_prefix."_catalog_ptypes AS PT ON P.ptype_id = PT.id
                               WHERE P.product_title LIKE  '%$q%' AND P.active = 1
                               ");
                if ($db->nf() > 0) {
                    for($i = 0; $i < $db->nf(); $i++) {
                        $db->next_record();
                        if(!(int)$_GET['category'] || (int)$_GET['category'] == $db->f('category_id')) $this->cats1[] = $db->f('category_id');
                        elseif(!(int)$_GET['category']) $this->cats1[] = $db->f('category_id');
                        $this->cats[] = $db->f('category_id');
                    }
                }
                //var_dump($this->cats1);
            }

            if(isset($_GET['decrise']))
            {
                $s="Desc";
            }
            else
            {
                $s="ASC";
            }

            if($_GET['sort'] == 'price'){
                $categs = $this->getSubCategsForRootCateg((int)$prp);

                $cids = array((int)$prp);
                if(is_array($categs) && sizeof($categs) > 0){
                    foreach($categs as $v) $cids[] = $v['ctg_id'];
                }
                if(sizeof($cids)){
                    $qu = 'SELECT id FROM '.$server.$lang.'_catalog_categories WHERE parent_id IN ('.implode(',',$cids).') AND active=1';
                    if(!$result = mysql_query($qu)) die(mysql_error());
                    while($row = mysql_fetch_assoc($result)) $cids[] = $row['id'];
                }

                $db->query('SELECT DISTINCT P.id,P.archive,P.leader,P.link, product_description,c.alias as calias,c.id as cid, product_title,P.group_id, product_inf,c.title as categ_title,P.dates, product_description, image_middle, image_big,
								   price_1, price_2, price_3, price_4, price_5, currency, novelty, product_availability, product_code,bonus,weight,file_alarm,dimensions,product_count,
								   if(P.alias is not null and P.alias!=\'\', P.alias, P.id) as alias,CAST(pv.value AS UNSIGNED) as sort_v, group_title, ptype_name, PT.link as ptype_link
								   FROM '.$this->table_prefix.'_catalog_products AS P
								   INNER JOIN '.$this->table_prefix.'_catalog_categories AS c ON c.id = P.category_id
								   LEFT JOIN '.$this->table_prefix.'_catalog_property_values AS pv ON pv.product_id = P.id AND pv.property_id=359
								   LEFT JOIN '.$this->table_prefix.'_catalog_groups AS G ON P.group_id = G.id
								   LEFT JOIN '.$this->table_prefix.'_catalog_ptypes AS PT ON P.ptype_id = PT.id
								   WHERE '.(sizeof($ids) ? ' P.id IN ('.implode(',', $ids).')' : (strlen($query) ? "P.product_title LIKE  '%$q%'".((int)$_GET['category'] ? ' AND P.category_id='.(int)$_GET['category'] : ' ') : 'archive='.$this->archive.' AND category_id IN ('.implode(',',$cids).')'.($_REQUEST['new_s']&&false ? ' AND P.price_1>0 AND product_availability=\'1\'' : ' '))).' AND P.active = 1
								   ORDER BY P.product_availability DESC, price_1 '.$s/*
					.($how > 0 ? " LIMIT ".(($start-1)*$how).",".$how : '')*/);

                /* $db->query('SELECT DISTINCT P.id,P.archive, IF( P.price_1 < 0.1 OR P.product_availability = \'0\', 0, 1 ) AS nrank,P.leader,P.link, product_description,c.alias as calias,c.id as cid, product_title,P.group_id, product_inf,c.title as categ_title,P.dates, product_description, image_middle, image_big,
                                    price_1, price_2, price_3, price_4, price_5, currency, novelty, product_availability, product_code,
                                    if(P.alias is not null and P.alias!=\'\', P.alias, P.id) as alias, group_title, ptype_name, PT.link as ptype_link
                                    FROM '.$this->table_prefix.'_catalog_products AS P
                                    INNER JOIN '.$this->table_prefix.'_catalog_categories AS c ON c.id = P.category_id
                                    LEFT JOIN '.$this->table_prefix.'_catalog_groups AS G ON P.group_id = G.id
                                    LEFT JOIN '.$this->table_prefix.'_catalog_ptypes AS PT ON P.ptype_id = PT.id
                                    WHERE '.(sizeof($ids) ? ' P.id IN ('.implode(',', $ids).')' : (strlen($query) ? "P.product_title LIKE  '%$q%'".((int)$_GET['category'] ? 'AND P.category_id='.(int)$_GET['category'] : ' ') : 'archive='.$this->archive.' AND category_id = '.$prp.($_REQUEST['new_s']&&false ? ' AND P.price_1>0 AND product_availability=\'1\'' : ' ') )).' AND P.active = 1
                                    ORDER BY P.product_availability DESC, price_1 '.$s);


                 /*echo 'SELECT DISTINCT P.id,IF( P.price_1 < 0.1 OR P.product_availability = \'0\', 0, 1 ) AS nrank,P.leader,P.link, product_description,c.alias as calias,c.id as cid, product_title,P.group_id, product_inf,c.title as categ_title,P.dates, product_description, image_middle, image_big,
                                    price_1, price_2, price_3, price_4, price_5, currency, novelty, product_availability, product_code,
                                    if(P.alias is not null and P.alias!=\'\', P.alias, P.id) as alias, group_title, ptype_name, PT.link as ptype_link
                                    FROM '.$this->table_prefix.'_catalog_products AS P
                                    INNER JOIN '.$this->table_prefix.'_catalog_categories AS c ON c.id = P.category_id
                                    LEFT JOIN '.$this->table_prefix.'_catalog_groups AS G ON P.group_id = G.id
                                    LEFT JOIN '.$this->table_prefix.'_catalog_ptypes AS PT ON P.ptype_id = PT.id
                                    WHERE '.(sizeof($ids) ? ' P.id IN ('.implode(',', $ids).')' : (strlen($query) ? "P.product_title LIKE  '%$q%'".((int)$_GET['category'] ? ' AND P.category_id='.(int)$_GET['category'] : ' ') : 'category_id = '.$prp.($_REQUEST['new_s']&&false ? ' AND P.price_1>0 AND product_availability=\'1\'' : ' ') )).' AND P.active = 1
                                    ORDER BY nrank DESC, P.'.$CONFIG['catalog_prod_order_by']
                     .($how > 0 ? " LIMIT ".(($start-1)*$how).",".$how : '');*/

            }elseif($_GET['sort'] == 'alf'){
                $categs = $this->getSubCategsForRootCateg((int)$prp);

                $cids = array((int)$prp);
                if(is_array($categs) && sizeof($categs) > 0){
                    foreach($categs as $v) $cids[] = $v['ctg_id'];
                }
                if(sizeof($cids)){
                    $qu = 'SELECT id FROM '.$server.$lang.'_catalog_categories WHERE parent_id IN ('.implode(',',$cids).') AND active=1';
                    if(!$result = mysql_query($qu)) die(mysql_error());
                    while($row = mysql_fetch_assoc($result)) $cids[] = $row['id'];
                }

                $db->query('SELECT DISTINCT P.id,P.archive,P.leader,P.link, product_description,c.alias as calias,c.id as cid, product_title,P.group_id, product_inf,c.title as categ_title,P.dates, product_description, image_middle, image_big,
								   price_1, price_2, price_3, price_4, price_5, currency, novelty, product_availability, product_code,bonus,weight,file_alarm,dimensions,product_count,
								   if(P.alias is not null and P.alias!=\'\', P.alias, P.id) as alias,CAST(pv.value AS UNSIGNED) as sort_v, group_title, ptype_name, PT.link as ptype_link
								   FROM '.$this->table_prefix.'_catalog_products AS P
								   INNER JOIN '.$this->table_prefix.'_catalog_categories AS c ON c.id = P.category_id
								   LEFT JOIN '.$this->table_prefix.'_catalog_property_values AS pv ON pv.product_id = P.id AND pv.property_id=359
								   LEFT JOIN '.$this->table_prefix.'_catalog_groups AS G ON P.group_id = G.id
								   LEFT JOIN '.$this->table_prefix.'_catalog_ptypes AS PT ON P.ptype_id = PT.id
								   WHERE '.(sizeof($ids) ? ' P.id IN ('.implode(',', $ids).')' : (strlen($query) ? "P.product_title LIKE  '%$q%'".((int)$_GET['category'] ? ' AND P.category_id='.(int)$_GET['category'] : ' ') : 'archive='.$this->archive.' AND category_id IN ('.implode(',',$cids).')'.($_REQUEST['new_s']&&false ? ' AND P.price_1>0 AND product_availability=\'1\'' : ' '))).' AND P.active = 1
								   ORDER BY product_title '.$s);
                /*$db->query('SELECT DISTINCT P.id,P.archive, IF( P.price_1 < 0.1 OR P.product_availability = \'0\', 0, 1 ) AS nrank,P.leader,P.link, product_description,c.alias as calias,c.id as cid, product_title,P.group_id, product_inf,c.title as categ_title,P.dates, product_description, image_middle, image_big,
								   price_1, price_2, price_3, price_4, price_5, currency, novelty, product_availability, product_code,
								   if(P.alias is not null and P.alias!=\'\', P.alias, P.id) as alias, group_title, ptype_name, PT.link as ptype_link
								   FROM '.$this->table_prefix.'_catalog_products AS P
								   INNER JOIN '.$this->table_prefix.'_catalog_categories AS c ON c.id = P.category_id
								   LEFT JOIN '.$this->table_prefix.'_catalog_groups AS G ON P.group_id = G.id
								   LEFT JOIN '.$this->table_prefix.'_catalog_ptypes AS PT ON P.ptype_id = PT.id
								   WHERE '.(sizeof($ids) ? ' P.id IN ('.implode(',', $ids).')' : (strlen($query) ? "P.product_title LIKE  '%$q%'".((int)$_GET['category'] ? 'AND P.category_id='.(int)$_GET['category'] : ' ') : 'archive='.$this->archive.' AND category_id = '.$prp.($_REQUEST['new_s']&&false ? ' AND P.price_1>0 AND product_availability=\'1\'' : ' ') )).' AND P.active = 1
								   ORDER BY product_title '.$s);


                /*echo 'SELECT DISTINCT P.id,IF( P.price_1 < 0.1 OR P.product_availability = \'0\', 0, 1 ) AS nrank,P.leader,P.link, product_description,c.alias as calias,c.id as cid, product_title,P.group_id, product_inf,c.title as categ_title,P.dates, product_description, image_middle, image_big,
								   price_1, price_2, price_3, price_4, price_5, currency, novelty, product_availability, product_code,
								   if(P.alias is not null and P.alias!=\'\', P.alias, P.id) as alias, group_title, ptype_name, PT.link as ptype_link
								   FROM '.$this->table_prefix.'_catalog_products AS P
								   INNER JOIN '.$this->table_prefix.'_catalog_categories AS c ON c.id = P.category_id
								   LEFT JOIN '.$this->table_prefix.'_catalog_groups AS G ON P.group_id = G.id
								   LEFT JOIN '.$this->table_prefix.'_catalog_ptypes AS PT ON P.ptype_id = PT.id
								   WHERE '.(sizeof($ids) ? ' P.id IN ('.implode(',', $ids).')' : (strlen($query) ? "P.product_title LIKE  '%$q%'".((int)$_GET['category'] ? ' AND P.category_id='.(int)$_GET['category'] : ' ') : 'category_id = '.$prp.($_REQUEST['new_s']&&false ? ' AND P.price_1>0 AND product_availability=\'1\'' : ' ') )).' AND P.active = 1
								   ORDER BY nrank DESC, P.'.$CONFIG['catalog_prod_order_by']
					.($how > 0 ? " LIMIT ".(($start-1)*$how).",".$how : '');*/

            }elseif($_GET['sort'] != 'd'){

                $categs = $this->getSubCategsForRootCateg((int)$prp);

                $cids = array((int)$prp);
                if(is_array($categs) && sizeof($categs) > 0){
                    foreach($categs as $v) $cids[] = $v['ctg_id'];
                }
                if(sizeof($cids)){
                    $qu = 'SELECT id FROM '.$server.$lang.'_catalog_categories WHERE parent_id IN ('.implode(',',$cids).') AND active=1';
                    if(!$result = mysql_query($qu)) die(mysql_error());
                    while($row = mysql_fetch_assoc($result)) $cids[] = $row['id'];
                }
                if($_SERVER['SERVER_NAME'] == "supra.ru" )
                {
                    $db->query('SELECT DISTINCT P.id,P.archive,P.leader,P.link,P.rank, product_description,c.alias as calias,c.id as cid, product_title,P.group_id, product_inf,c.title as categ_title,P.dates, product_description, image_middle, image_big,
								   price_1, price_2, price_3, price_4, price_5, currency, novelty, product_availability, product_code,bonus,weight,file_alarm,dimensions,product_count,
								   if(P.alias is not null and P.alias!=\'\', P.alias, P.id) as alias,CAST(pv.value AS UNSIGNED) as sort_v, group_title, ptype_name, PT.link as ptype_link
								   FROM '.$this->table_prefix.'_catalog_products AS P
								   INNER JOIN '.$this->table_prefix.'_catalog_categories AS c ON c.id = P.category_id
								   LEFT JOIN '.$this->table_prefix.'_catalog_property_values AS pv ON pv.product_id = P.id AND pv.property_id=359
								   LEFT JOIN '.$this->table_prefix.'_catalog_groups AS G ON P.group_id = G.id
								   LEFT JOIN '.$this->table_prefix.'_catalog_ptypes AS PT ON P.ptype_id = PT.id
								   WHERE '.(sizeof($ids) ? ' P.id IN ('.implode(',', $ids).')' : (strlen($query) ? "P.product_title LIKE  '%$q%'".((int)$_GET['category'] ? ' AND P.category_id='.(int)$_GET['category'] : ' ') : 'archive='.$this->archive.' AND category_id IN ('.implode(',',$cids).')'.($_REQUEST['new_s']&&false ? ' AND P.price_1>0 AND product_availability=\'1\'' : ' '))).' AND P.active = 1
								   ORDER BY rank '/*
					.($how > 0 ? " LIMIT ".(($start-1)*$how).",".$how : '')*/);
                }
                else
                $db->query('SELECT DISTINCT P.id,P.archive,P.leader,P.link, product_description,c.alias as calias,c.id as cid, product_title,P.group_id, product_inf,c.title as categ_title,P.dates, product_description, image_middle, image_big,
								   price_1, price_2, price_3, price_4, price_5, currency, novelty, product_availability, product_code,bonus,weight,file_alarm,dimensions,product_count,
								   if(P.alias is not null and P.alias!=\'\', P.alias, P.id) as alias,CAST(pv.value AS UNSIGNED) as sort_v, group_title, ptype_name, PT.link as ptype_link
								   FROM '.$this->table_prefix.'_catalog_products AS P
								   INNER JOIN '.$this->table_prefix.'_catalog_categories AS c ON c.id = P.category_id
								   LEFT JOIN '.$this->table_prefix.'_catalog_property_values AS pv ON pv.product_id = P.id AND pv.property_id=359
								   LEFT JOIN '.$this->table_prefix.'_catalog_groups AS G ON P.group_id = G.id
								   LEFT JOIN '.$this->table_prefix.'_catalog_ptypes AS PT ON P.ptype_id = PT.id
								   WHERE '.(sizeof($ids) ? ' P.id IN ('.implode(',', $ids).')' : (strlen($query) ? "P.product_title LIKE  '%$q%'".((int)$_GET['category'] ? ' AND P.category_id='.(int)$_GET['category'] : ' ') : 'archive='.$this->archive.' AND category_id IN ('.implode(',',$cids).')'.($_REQUEST['new_s']&&false ? ' AND P.price_1>0 AND product_availability=\'1\'' : ' '))).' AND P.active = 1
								   ORDER BY P.product_availability DESC, price_1 DESC'/*
                                    .($how > 0 ? " LIMIT ".(($start-1)*$how).",".$how : '')*/);

// Было для вывода по рангу
                /*        $db->query('SELECT DISTINCT P.id,P.archive, IF( P.price_1 < 0.1 OR P.product_availability = \'0\', 0, 1 ) AS nrank,P.leader,P.link, product_description,c.alias as calias,c.id as cid, product_title,P.group_id, product_inf,c.title as categ_title,P.dates, product_description, image_middle, image_big,
                                           price_1, price_2, price_3, price_4, price_5, currency, novelty, product_availability, product_code,
                                           if(P.alias is not null and P.alias!=\'\', P.alias, P.id) as alias, group_title, ptype_name, PT.link as ptype_link
                                           FROM '.$this->table_prefix.'_catalog_products AS P
                                           INNER JOIN '.$this->table_prefix.'_catalog_categories AS c ON c.id = P.category_id
                                           LEFT JOIN '.$this->table_prefix.'_catalog_groups AS G ON P.group_id = G.id
                                           LEFT JOIN '.$this->table_prefix.'_catalog_ptypes AS PT ON P.ptype_id = PT.id
                                           WHERE '.(sizeof($ids) ? ' P.id IN ('.implode(',', $ids).')' : (strlen($query) ? "P.product_title LIKE  '%$q%'".((int)$_GET['category'] ? 'AND P.category_id='.(int)$_GET['category'] : ' ') : 'archive='.$this->archive.' AND category_id = '.$prp.($_REQUEST['new_s']&&false ? ' AND P.price_1>0 AND product_availability=\'1\'' : ' ') )).' AND P.active = 1
                                           ORDER BY '.($_REQUEST['new_s'] ? 'nrank DESC, price_1 DESC,' : ''). 'P.'.$CONFIG['catalog_prod_order_by']
                            .($how > 0 ? " LIMIT ".(($start-1)*$how).",".$how : ''));

        */
                /*echo 'SELECT DISTINCT P.id,IF( P.price_1 < 0.1 OR P.product_availability = \'0\', 0, 1 ) AS nrank,P.leader,P.link, product_description,c.alias as calias,c.id as cid, product_title,P.group_id, product_inf,c.title as categ_title,P.dates, product_description, image_middle, image_big,
								   price_1, price_2, price_3, price_4, price_5, currency, novelty, product_availability, product_code,
								   if(P.alias is not null and P.alias!=\'\', P.alias, P.id) as alias, group_title, ptype_name, PT.link as ptype_link
								   FROM '.$this->table_prefix.'_catalog_products AS P
								   INNER JOIN '.$this->table_prefix.'_catalog_categories AS c ON c.id = P.category_id
								   LEFT JOIN '.$this->table_prefix.'_catalog_groups AS G ON P.group_id = G.id
								   LEFT JOIN '.$this->table_prefix.'_catalog_ptypes AS PT ON P.ptype_id = PT.id
								   WHERE '.(sizeof($ids) ? ' P.id IN ('.implode(',', $ids).')' : (strlen($query) ? "P.product_title LIKE  '%$q%'".((int)$_GET['category'] ? ' AND P.category_id='.(int)$_GET['category'] : ' ') : 'category_id = '.$prp.($_REQUEST['new_s']&&false ? ' AND P.price_1>0 AND product_availability=\'1\'' : ' ') )).' AND P.active = 1
								   ORDER BY nrank DESC, P.'.$CONFIG['catalog_prod_order_by']
					.($how > 0 ? " LIMIT ".(($start-1)*$how).",".$how : '');*/

            }else{

                if(isset($_GET['supra']))$ord="DESC";
                else $ord="ASC";
                $categs = $this->getSubCategsForRootCateg((int)$prp);

                $cids = array((int)$prp);
                if(is_array($categs) && sizeof($categs) > 0){
                    foreach($categs as $v) $cids[] = $v['ctg_id'];
                }
                if(sizeof($cids)){
                    $qu = 'SELECT id FROM '.$server.$lang.'_catalog_categories WHERE parent_id IN ('.implode(',',$cids).') AND active=1';
                    if(!$result = mysql_query($qu)) die(mysql_error());
                    while($row = mysql_fetch_assoc($result)) $cids[] = $row['id'];
                }

                $db->query('SELECT DISTINCT P.id,P.archive,P.leader,P.link, product_description,c.alias as calias,c.id as cid, product_title,P.group_id, product_inf,c.title as categ_title,P.dates, product_description, image_middle, image_big,
								   price_1, price_2, price_3, price_4, price_5, currency, novelty, product_availability, product_code,bonus,weight,file_alarm,dimensions,product_count,
								   if(P.alias is not null and P.alias!=\'\', P.alias, P.id) as alias,CAST(pv.value AS UNSIGNED) as sort_v, group_title, ptype_name, PT.link as ptype_link
								   FROM '.$this->table_prefix.'_catalog_products AS P
								   INNER JOIN '.$this->table_prefix.'_catalog_categories AS c ON c.id = P.category_id
								   LEFT JOIN '.$this->table_prefix.'_catalog_property_values AS pv ON pv.product_id = P.id AND pv.property_id=359
								   LEFT JOIN '.$this->table_prefix.'_catalog_groups AS G ON P.group_id = G.id
								   LEFT JOIN '.$this->table_prefix.'_catalog_ptypes AS PT ON P.ptype_id = PT.id
								   WHERE '.(sizeof($ids) ? ' P.id IN ('.implode(',', $ids).')' : (strlen($query) ? "P.product_title LIKE  '%$q%'".((int)$_GET['category'] ? ' AND P.category_id='.(int)$_GET['category'] : ' ') : 'archive='.$this->archive.' AND category_id IN ('.implode(',',$cids).')'.($_REQUEST['new_s']&&false ? ' AND P.price_1>0 AND product_availability=\'1\'' : ' '))).' AND P.active = 1
								   ORDER BY P.product_availability DESC,sort_v '.$ord/*
					.($how > 0 ? " LIMIT ".(($start-1)*$how).",".$how : '')*/);
            }
            if ($db->nf() > 0) {
                $add_to_cart_link = Core::formPageLink($CONFIG['shop_page_link'], $CONFIG['shop_page_address'], $lang);
                $add_to_cart_link .= (FALSE === strpos($add_to_cart_link, "?") ? "?" : "&")."action=addtocart&id=";
                $ids = array();
                for($i = 0; $i < $db->nf(); $i++) {
                    $db->next_record();
                    $img = $db->f('image_middle');
                    $img = $img && file_exists(RP.$CONFIG['catalog_img_path'].$img) ? "/".$CONFIG['catalog_img_path'].$img : "";
                    $b_img = $db->f('image_big');
                    $b_img = $b_img && file_exists(RP.$CONFIG['catalog_img_path'].$b_img) ? "/".$CONFIG['catalog_img_path'].$b_img : "";
                    $arr[$i]['prod_num']			= ($i+1);
                    $arr[$i]['prod_id']				= $prod_id = $db->f('id');
                    $arr[$i]['prod_alias']			= $db->f('alias');
                    $arr[$i]['category_id']			= $db->f('cid');

                    $arr[$i]['leader']			= $db->f('leader');
                    $arr[$i]['date']			= $db->f('dates');
                    $arr[$i]['group_id']			= $db->f('group_id');
                    $arr[$i]['link']			= $db->f('link');
                    $arr[$i]['archive']			= $db->f('archive');
                    $arr[$i]['description']			= $db->f('product_description');
                    $arr[$i]['categ_title']			= $db->f('categ_title');
                    $arr[$i]['prod_code']			= $db->f('product_code');
                    $arr[$i]['prod_link']			= $this->getProdLink($db->f('archive') ? '/arhive/' : '/catalog/', $db->f('alias'), $prod_id, $db->f('calias') ? $db->f('calias') : $db->f('cid'));
                    $arr[$i]['prod_title']			= htmlspecialchars($db->f('product_title'));
                    $arr[$i]['prod_descr']			= nl2br($db->f('product_inf'));
                    $arr[$i]['prod_image']			= $img;
                    $arr[$i]['img']			= $img;
                    $arr[$i]['prod_image_big']		= $b_img;
                    $arr[$i]['parent_title']		= $parent_title;
                    $arr[$i]['prod_price_1']		= $this->getCostWithTaxes($db->f('price_1'));
                    $arr[$i]['prod_price_2']		= $this->getCostWithTaxes($db->f('price_2'));
                    $arr[$i]['prod_price_3']		= $this->getCostWithTaxes($db->f('price_3'));
                    $arr[$i]['prod_price_4']		= $this->getCostWithTaxes($db->f('price_4'));
                    $arr[$i]['prod_price_5']		= $this->getCostWithTaxes($db->f('price_5'));
                    $arr[$i]['prod_price']			= $arr[$i][$clnt_price];
                    $arr[$i]['prod_currency']		= $CONFIG['catalog_currencies'][$db->f('currency')];
                    $arr[$i]['prod_novelty']		= $db->f('novelty');
                    $arr[$i]['prod_currency_name']	= $this->currencies[$db->f('currency')];
                    $arr[$i]['prod_availability']	= $db->f('product_availability');
                    $arr[$i]['add_to_cart_link']	= $add_to_cart_link.$prod_id;
                    $arr[$i]['group_title']			= $db->f('group_title');
                    $arr[$i]['ptype_name']			= $db->f('ptype_name');
                    $arr[$i]['ptype_link']			= $db->f('ptype_link');
                    $arr[$i]['bonus']			    = $db->f('bonus');
                    $arr[$i]['weight']			    = $db->f('weight');
                    $arr[$i]['file_alarm']			    = $db->f('file_alarm');
                    $arr[$i]['dimensions']			    = $db->f('dimensions');
                    $arr[$i]['product_count']			    = $db->f('product_count');
                    $ids[$prod_id] = $i;
                }
                $this->setPropValForProdList($arr);

                return $arr;
            }
        }
        return FALSE;
    }

    function getSubCategsForRootCateg($ctgr_id = NULL, $transurl = '', $filter = false, $is_search = 0, $is_menu = false)
    {    global $db, $server, $lang, $new_supra, $baseurl,$PAGE, $CONFIG;

        $transurl = $transurl ? $transurl : $baseurl;
        $group = false;
        if($is_search){
        }
        if ($filter['with_prod_cnt']) {
            $query = 'SELECT C.id, C.title, c.tematik, C.img, C.description, if(C.alias is not null and C.alias!=\'\', C.alias, C.id) as alias, COUNT(P.id) as prod_cnt FROM '
                .$this->table_prefix.'_catalog_categories C INNER JOIN '.$this->table_prefix.'_catalog_products P ON C.id=P.category_id'
                .' WHERE P.active AND C.active = 1'.($is_menu ? ($new_supra ? ' AND nshow_new=0' : ' AND nshow=0') : '');
        } else {
            $query = 'SELECT id, title, tematik, img, description, if(alias is not null and alias!=\'\', alias, id) as alias FROM '
                .$this->table_prefix.'_catalog_categories C WHERE active = 1 '.($is_menu ? ($new_supra ? ' AND nshow_new=0' : ' AND nshow=0') : '');
        }

        $ret = false;
        if (is_array($filter['valid_ctgr']) && !empty($filter['valid_ctgr'])) {
            $query .= ' AND C.id IN ('.implode(',', array_map('intval', $filter['valid_ctgr'])).')';
            if ($ctgr_id > 0) {
                $query .= ' AND C.parent_id = '.intval($ctgr_id);
            }
            $ret = true;
        } else if ($ctgr_id > 0) {
            $query .= ' AND C.parent_id = '.intval($ctgr_id);
            $ret = true;
        } else if ('all' == $ctgr_id) {
            $query .= ' AND C.clevel = 1';
            $ret = true;
        }
        if(sizeof($this->cats) < 1 && $is_search) return;
        //if($PAGE['field'] == 0)echo $query.($filter['with_prod_cnt'] ? ' GROUP BY C.id' : '').' ORDER BY C.'.$CONFIG['catalog_ctg_order_by'];
        if ($ret || $is_search) {
            if($is_search){
                $c = array();
                foreach($this->cats as $v){
                    if(!$c[$v]) $c[$v] = 1;
                    else $c[$v]++;
                }
                $db->query('SELECT id, title, tematik, img, description, if(alias is not null and alias!=\'\', alias, id) as alias FROM '
                    .$this->table_prefix.'_catalog_categories C WHERE active = 0 AND id IN ('.implode(',', array_unique($this->cats)).')'.($is_menu ? ($new_supra ? ' AND nshow_new=0' : ' AND nshow=0') : '').' ORDER BY title');

            }
            else{
                $db->query($query.($filter['with_prod_cnt'] ? ' GROUP BY C.id' : '').' ORDER BY C.'.$CONFIG['catalog_ctg_order_by']);
                // echo $query.($filter['with_prod_cnt'] ? ' GROUP BY C.id' : '').' ORDER BY C.'.$CONFIG['catalog_ctg_order_by'];
            }
            while($db->next()) {
                $arr[] = array(
                    'ctg_id' => $db->f('id'),
                    'ctg_link' => ($is_search) ? $baseurl.'&action=search&query='.urlencode($_GET['query']).'&category='.$db->f('id') : $this->getCtgrLink($transurl, $db->f('alias'), $db->f('id')),
                    'ctg_name' => ($is_search) ? ($db->f('title').' ('.$c[$db->f('id')].')') : $db->f('title'),
                    'ctg_descr' => $db->f('description'),
                    'ctg_img' => $db->f('img'),
                    'ctg_alias' => $db->f('alias'),
                    'prod_cnt' => $db->f('prod_cnt')  ,
                    'tematik' => $db->f('tematik')
                );
            }
            return $arr;
        }

        return FALSE;
    }

    // вывести навигационную строчку, отображающую путь к текущей категории
    function show_nav_path($id = 1, $transurl = "?", $product_id = 0) {
        GLOBAL $tpl;

        list($level, $path) = $this->get_nav_path($id, $transurl, $product_id);
        $tpl->assign(array(
            '_ROOT.path'	=>	$path,
        ));

        return $level;
    }

    // получить навигационную строчку, отображающую путь к текущей категории
    // строчка вида Каталог >> Бытовая техника >> Пылесосы... и т.д.
    // функция также возвращает уровень вложенности $level для данного элемента
    function get_nav_path($id, $transurl = '?', $product_id = 0, $wh = '') {
        GLOBAL $server, $lang, $CONFIG;
        $id				= (int)$id;
        $product_id		= (int)$product_id;
        $arr			= array();
        $separator		= $CONFIG['nav_separator'];
        $path			= '';
        $level			= 0;
        $i				= 0;
        $CC = new CCatalogCategories_DB($this->table_prefix.'_catalog_categories', 'id');
        if (!isset($CC2)) {
            $CC2	=$CC->_clone();
        }
        $CC2->load(1);

        $root = ($CC2->id) ? '<a href="'.$transurl.'&action=assortment&id=0" class="header1">'.str_replace('\\', '', $CC2->title).'</a>' : '<span class="modul">'.str_replace('\\', '', $CC2->title).'</span>';

        if ($product_id) {
            $CD = new CCatalogData_DB($this->table_prefix.'_catalog_products', 'id');

            $CD->load($product_id);
            $path[] = '<span class="modul">'.$CD->product_title.'</span>';
            $this->addition_to_path[$CD->product_title] = '';
        }

        if ($id != "1") {
            while ($id) {
                $level++;
                $CC->load($id);

                if ($CC->id) {
                    $name = str_replace("\\", "", $CC->title);

                    $alias = $CC->alias ? $CC->alias : $CC->id;

                    $this->addition_to_path[$name] = !$this->admin
                        ? $this->getCtgrLink($transurl, $alias, $CC->id)
                        : $transurl.'&action=assortment&id='.$CC->id;

                    if ($i == 0 && !$product_id) {
                        $path[] = sprintf('<span class="modul">%s</span>',$name);

                    } else {
                        $path[] = !$this->admin
                            ? sprintf('<a href="%s">%s</a>', $this->getCtgrLink($transurl, $alias, $CC->id), $name)
                            : sprintf('<a href="%s&%s">%s</a>', $transurl, 'action=assortment&id='.$CC->id, $name);

                    }
                    if ($CC->parent_id != 1) {
                        $id = $CC->parent_id;
                    } else {
                        $id = null;
                    }
                    $i++;
                }
            }

        }




        if(is_array($path)){
            $path_ret = implode($separator, array_reverse($path));
        }



        $path_ret = ($root && !empty($path_ret)) ? $root . $separator . $path_ret : $root . $path_ret;
        $this->addition_to_path = array_reverse($this->addition_to_path);

        return array($level, $path_ret);
    }

    // функция для вывода таблицы товаров с русскими именами
    function show_russian_products($products) {
        global $main, $CONFIG, $tpl, $baseurl, $db, $group_action, $lang;
        if (!isset($transurl)) $transurl = $baseurl;
        $admin = Module::is_admin();;
        $tpl->newBlock("block_categories");
        foreach($products as $k=>$v) {
            $tpl->newBlock("block_category");
            $tpl->assign(array(
                'id'					=> $v["product_id"],
                'product_name'			=> str_replace("\\", "", $v["product_title"]),
                'url'			=> "admin.php?lang=".$lang."&name=catalog&id=".$v["product_id"]."&category_id=".$v["product_category"]."&action=editproduct",

            ));

            $i++;
        }

        $tpl->gotoBlock('block_categories');
        return TRUE;
    }

    function show_3dlist_products($products)
    {
        global $main, $CONFIG, $tpl, $baseurl, $db, $group_action, $lang;
        if (!isset($transurl)) $transurl = $baseurl;
        $admin = Module::is_admin();;
        $tpl->newBlock("block_categories");
        foreach($products as $k=>$v) {
            $tpl->newBlock("block_category");
            $tpl->assign(array(
                'i'				=>	$k+1,
                'id'					=> $v["product_id"],
                'product_name'			=> str_replace("\\", "", $v["product_title"]),
                'url'			=> "admin.php?lang=".$lang."&name=catalog&id=".$v["product_id"]."&category_id=".$v["product_category"]."&action=editproduct",

            ));

            $i++;
        }

        $tpl->gotoBlock('block_categories');
        return TRUE;
    }

    // функция для вывода таблицы категорий/разделов
    function show_categories($cid = 1, $transurl = null, $selected_category = null, $ctg_owners = 0) {
        global $main, $CONFIG, $tpl, $baseurl, $db, $group_action, $lang;
        $CC = new CCatalogCategories_DB($this->table_prefix.'_catalog_categories', 'id');
        if (!isset($transurl)) $transurl = $baseurl;
        $admin = Module::is_admin();
        if (!isset($admin)) $active_str = " AND active = 1";
        if ($cid) $cid = intval($cid);
        $this->get_list_with_rights(
            "c.*", $this->table_prefix."_catalog_categories c", "c",
            "clevel != 0 AND parent_id = ".$cid." ".$active_str, "", "rank ASC",
            1, 0, false, $ctg_owners, true);
        while ($db->next_record()) $temp[] = $db->Record;
        $is_first = $temp[0]["id"];
        $is_last = $temp[count($temp)-1]["id"];
        if (count($temp) > 0) {
            $level = $CC->getLevel($cid);
            $tpl->newBlock("block_categories");
            $tpl->assign(array(
                "block_categories.MSG_Active" => $this->_msg["Active"],
                "block_categories.MSG_Ed" => $this->_msg["Ed"],
                "block_categories.MSG_Rights" => $main->_msg["Rights"],
                "block_categories.MSG_Section" => $this->_msg["Section"],
                "block_categories.MSG_Order" => $this->_msg["Order"],
                "block_categories.MSG_Add" => $this->_msg["Add"],
                "block_categories.MSG_Del" => $this->_msg["Del"],
                "block_categories.MSG_Goods_list" => $this->_msg["Goods_list"],
                "block_categories.MSG_Seek" => $this->_msg["Seek"],
                "block_categories.MSG_Back_to_goods_list" => $this->_msg['Back_to_goods_list'],
                "block_categories.MSG_Empty_field" => $this->_msg["Empty_field"],
                "block_categories.MSG_Select_category" => $this->_msg["Select_category"],
                "block_categories.MSG_Move_categories" => $this->_msg["Move_categories"],
                "block_categories.action_lang" => $lang,
                "clevel" =>	 $level,
                "baseurl" => $baseurl,
                'menu_check' =>  $CC->getTematik($cid) ? '<th width="5%">Тематические<br>акции(главное меню)</th>':'',
            ));
            $h = ceil(count($CC)/2);
            $actions = array(   "addctg"	=> "addcat",
                "ctg_max_lvl"	=> $CONFIG["catalog_max_level"],
                "addprd"	=> "addproduct",
                "addprdmsg"	=> $this->_msg["Add_good"],
                "edit"		=> "editcat",
                "editrights"=> "editctgrights",
                "activate"	=>	array("cactivate","csuspend"),
                "delete"	=> "delcat",
                "confirm_delete"	=> $this->_msg["Confirm_delete_record"]
            );
            foreach($temp as $k=>$v) {
                $change_status		= $v["active"] ? "csuspend" : "cactivate";

                if ($is_first == $v["id"]) {
                    $img_up = "ico_ligrup.gif";
                    $first = 1;
                } else {
                    $img_up = "ico_listup.gif";
                    $first = 0;
                }
                if ($is_last == $v["id"]) {
                    $img_down = "ico_ligrdw.gif";
                    $last = 1;
                } else {
                    $img_down = "ico_listdw.gif";
                    $last = 0;
                }

                $adm_actions = $this->get_actions_by_rights($transurl."&id=".$v["id"]."&parent=".$v["parent_id"], $v["active"], $v["is_owner"], $v["user_rights"], $actions);
                $tpl->newBlock("block_category");
                $off3= !$v['tematik_menu'] ?'_off':'';
                $tpl->assign(array(
                    "block_category.MSG_Category_ID" => $this->_msg["Category_ID"],
                    "block_category.MSG_Move_up" => $this->_msg["Move_up"],
                    "block_category.MSG_Move_down" => $this->_msg["Move_down"],
                    'id' => $v["id"],
                    'category_id' => $v["id"],
                    'category_name' => str_replace("\\", "", $v["title"]),
                    'category_description' => str_replace("\\", "", $v["description"]),
                    'cat_link' => $transurl . "&action=assortment&id=" . $v["id"],
                    'ctg_link' => $transurl . '&action=assortment&id=' . $v['id'],
                    'cat_add_action' => $adm_actions["addctg_action"],
                    'prd_add_action' => $adm_actions["addprd_action"],
                    'cat_edit_action' => $adm_actions["edit_action"],
                    'cat_edit_rights_action' => $adm_actions["edit_rights_action"],
                    'cat_del_action' => $adm_actions["delete_action"],
                    'change_status_action' => $adm_actions["change_status_action"],
                    'cat_up_link' => $transurl . "&action=upcat&id=" . $v["id"] . "&parent=" . $v["parent_id"] . "&rank=" . $v["rank"],
                    'cat_down_link' => $transurl . "&action=downcat&id=" . $v["id"] . "&parent=" . $v["parent_id"] . "&rank=" . $v["rank"],
                    'img_down' => $img_down,
                    'img_up' => $img_up,
                    'mshow' => $v['nshow'] ? 0 : 1,
                    'tematik' => $v['tematik'] ? 0 : 1,
                    'menu_check_value' => $CC->getTematik($cid) ? '<td align="center">
                                    <a href="admin.php?lang=rus&name=catalog&id=' . $v["id"] . '&action=changetematikmenu"><img width="15" height="15" border="0"  src="/i/admin/b_select' . $off3 . '.gif"></a>
                                </td>' : '',
                    'mshow1' => $v['nshow_new'] ? 0 : 1,
                    'mshow2' => $v['nshow_ya'] ? 0 : 1,
                    'off' => $v['nshow'] ? '' : '_off',
                    'off1' => $v['nshow_new'] ? '' : '_off',
                    'off2' => $v['tematik'] ? '' : '_off',
                    'off3' => $v['tematik_menu'] ? '' : '_off',
                    'off4' => $v['nshow_ya'] ? '' : '_off',
                    'is_first' => $first,
                    'is_last' => $last,
                    'selected' => ($v["id"] == $selected_category) ? " selected" : "",
                    'rank' => $v["rank"],
                    'clevel' => $v["clevel"],
                ));
                if ($adm_actions["rights"]["d"] || $adm_actions["rights"]["p"]) {
                    $tpl->newBlock('block_ctg_check');
                    $tpl->assign("category_id", $v["id"]);
                    $group_action = true;
                }

                $i++;
            }

            $tpl->gotoBlock('block_categories');
            $tpl->assign(Array(
                "group_action" 		=> $transurl."&action=gr_act&parent=".$cid,
                "MSG_Choose_action" => $this->_msg["Choose_action"],
                "MSG_Activate" 		=> $this->_msg["Activate"],
                "MSG_Suspend" 		=> $this->_msg["Suspend"],
                "MSG_Delete" 		=> $this->_msg["Delete"],
                "MSG_Execute" 		=> $this->_msg["Execute"],
            ));
            return TRUE;
        }
        return FALSE;
    }

    // функция для вывода информации о категории по id
    function show_category($id, $transurl, $blockname = "") {
        GLOBAL $server, $lang, $CONFIG, $main, $tpl, $baseurl;
        $admin = Module::is_admin();
        $id = intval($id);
        if (!$id) return 0;
        $CC = new CCatalogCategories_DB($this->table_prefix.'_catalog_categories', 'id');
        $CC->load($id);
        if (count($CC) > 0) {
            if ($admin) {
                $title					= str_replace("\\", "", htmlspecialchars($CC->title));
                $alias			        = preg_replace("~[^a-z0-9_-]~i", "", $CC->alias);
                $description			= str_replace("\\", "", htmlspecialchars($CC->description));
                $page_description		= str_replace("\\", "", htmlspecialchars($CC->page_description));
                $keywords				= str_replace("\\", "", htmlspecialchars($CC->keywords));
                $synonyms				= str_replace("\\", "", htmlspecialchars($CC->synonyms));
            } else {
                $cname					= str_replace("\\", "", $CC->title);
                $description			= str_replace("\\", "", $CC->description);
                $page_description		= str_replace("\\", "", $CC->page_description);
                $keywords				= str_replace("\\", "", $CC->keywords);
                $synonyms				= str_replace("\\", "", htmlspecialchars($CC->synonyms));
                $this->main_title		= $cname;
            }
            $products_synonyms = str_replace("\\", "", $CC->products_synonyms);
            if ($blockname) $tpl->newBlock($blockname);
            $tpl->assign(array(
                id							=>	$CC->id,
                title						=>	$title,
                main => $CC->main ? 'checked' : '',
                alias                       =>  $alias,
                description					=>	$description,
                category_link				=>	"$transurl&action=assortment&id=".$CC->id,
                rank						=>	$CC->rank,
                page_description			=>	$page_description,
                page_keywords				=>	$keywords,
                page_synonyms				=>	$synonyms,
                products_synonyms			=>	$products_synonyms,
            ));
            if ($admin) {
                $grp = $this->get_all_groups($CC->group_id);
                $tpl->assign_array('block_group', $grp);
            }
            if ($admin || $CC->img) {
                $tpl->newBlock('block_ctg_image');
                $tpl->assign(array(
                    'image_title'				=>	$this->_msg["Category_image"],
                    'image_exists'			=>	($CC->img)
                            ? '<a href="/'.$CONFIG["catalog_img_path"]."categories/".$CC->img.'" target="_blank" class="thickbox">' . $this->_msg["Look_image"] . '</a>'
                            : '',
                    'field_name'				=>	'img',
                    'image_name'				=>	$CC->img,
                    "MSG_Delete" => $this->_msg["Delete"],
                ));
            }
            $this->main_title = $title;
            return $CC;
        }
        return 0;
    }

    //Функция получения списка товаров в названии которых присутствуют русские символы
    function getProductsWithRussianTitles() {
        global $db;
        $sql = "SELECT
				a.id AS product_id,
				a.category_id AS product_category,
				a.product_title AS product_title
			FROM ".$this->table_prefix."_catalog_products a
			WHERE
				a.product_title REGEXP '[а-я]'";
        $result = db_loadList($sql);
        return $result;
    }

    function getProductsWith3dVideo()
    {
        global $db;
        $sql = "SELECT
        a.id AS product_id,
        a.category_id AS product_category,
        a.product_title AS product_title
         FROM ".$this->table_prefix."_catalog_products a
         WHERE swf_file <> ''";
        $result = db_loadList($sql);
        return $result;
    }


    function getProductsOff()
    {
        global $db;
        $sql = "SELECT
        a.id AS product_id,
        a.category_id AS product_category,
        a.product_title AS product_title
         FROM ".$this->table_prefix."_catalog_products a
         WHERE price_5 > 0 ";
        $result = db_loadList($sql);
        return $result;
    }

    function getProductsDimensions()
    {
        global $db;
        $sql = "SELECT
        a.id AS product_id,
        a.category_id AS product_category,
        a.product_title AS product_title
         FROM ".$this->table_prefix."_catalog_products a
         WHERE (LENGTH(dimensions) = 0 OR LENGTH(weight) = 0 ) AND archive = 0";
        $result = db_loadList($sql);
        return $result;
    }

    // функция для получения информации о владельцах категории по id
    function get_category_owners($id) {
        global $db;
        if ($id == 0) return 0;
        $ret = 0;
        $db->query("SELECT id, parent_id, owner_id FROM ".$this->table_prefix."_catalog_categories");
        if ($db->nf()) {
            while ($db->next_record()) {
                $parents[$db->f("id")] = array("parent_id" => $db->f("parent_id"), "owner_id" => $db->f("owner_id"));
            }

            $ret = array();
            while ($id) {
                if (!in_array($parents[$id]["owner_id"], $ret)) $ret[] = $parents[$id]["owner_id"];
                $id = $parents[$id]["parent_id"];
            }
        }
        return $ret;
    }

    function get_category_owners_p($id) {
        global $db;
        if ($id == 0) return 0;
        $ret = 0;
        $db->query("SELECT id, parent_id, owner_id FROM ".$this->table_prefix."_catalog_categories");
        if ($db->nf()) {
            while ($db->next_record()) {
                $parents[$db->f("id")] = array("parent_id" => $db->f("parent_id"), "owner_id" => $db->f("owner_id"));
            }

            $ret = array();
            while ($id) {
                if (!in_array($parents[$id]["parent_id"], $ret)) $ret[] = $parents[$id]["parent_id"];
                $id = $parents[$id]["parent_id"];
            }
        }
        return $ret;
    }

    function show_tree($id = 0)
    {
        global $CONFIG, $db, $tpl, $transurl, $server, $lang;
        $sql = "SELECT * FROM " . $server . $lang . "_catalog_categories WHERE  parent_id = '" . (integer)$id . "' ORDER BY rank";
        $db->query($sql);
        if($db->nf())
        {
            $dbcopy = Array();
            for($i = 0; $i < $db->nf(); $i++)
            {
                $db->next_record();
                $dbcopy[$i]->id = $db->f("id");
                $dbcopy[$i]->title = $db->f("title");
            }
//			$tpl->newBlock("block_tree_ul");
//			$tpl->newBlock("block_tree_ul_start");
            if($id == 1)
            {
                $str .= '<ul id="ul_' . $id . '" style="display: block;">';
            }
            else
            {
                $str .= '<ul id="ul_' . $id . '" style="display: none;">';
            }
            foreach($dbcopy as $k => $v)
            {
//				$tpl->newBlock("block_tree_li");
//				$tpl->newBlock("block_tree_li_start");
                $str .= '<li>';
                $tmp = $this->show_tree($v->id);
                if($tmp)
                {
                    $str .= '<a href="#" onclick="openNode(' . $v->id . ', this); return false;">[+]</a>';
                }
                else
                {
                    $str .= '[&nbsp;&nbsp;&nbsp;]';
                }
//				$tpl->assign("block_tree_li.link", $transurl . "?action=assortment&id=" . $v->id);
//				$tpl->assign("block_tree_li.title", $v->title);
                $str .= '<a href="' . $transurl . "?action=assortment&id=" . $v->id . '">' . $v->title . '</a>';
                $str .= $tmp;
//				$tpl->newBlock("block_tree_li_end");
                $str .= '</li>';
            }
//			$tpl->newBlock("block_tree_ul_end");
            $str .= '</ul>';
        }
        return $str;
    }


    // Две функции для вывода дерева каталога с возможностью перехода в ветку.
    /*
	function get_tree($parent_id = null, $transurl = "?") {
		GLOBAL $lang, $CONFIG, $tpl, $baseurl;

		$admin = Module::is_admin();
		if ($admin) $active_str = ""; else $active_str = " AND active = '1'";
		$text = "";
		$catalog = $this->ReadLevel(1);
		$tpl->assign(array(
			"catalog"	 => $catalog,
		));
	}
*/
////////////////////////////////////////
    function get_tree($type, $parent = 0, $transurl = '?', $admin = 0, $active = FALSE) {
        GLOBAL $CONFIG, $db, $tpl, $namem, $baseurl, $server, $lang;
        $str = '';
        $arr = $this->get_node($parent, $active);
        if (count($arr)) {
            $str = implode(',', $arr);
            $str = 'AND id IN ('.$str.')';
        }

        $active_str = $active ? " AND active" : "";
        $db->query('SELECT * FROM '.$this->table_prefix.'_catalog_categories WHERE !ISNULL(parent_id) '.$str.$active_str.' ORDER BY clevel, '.$CONFIG['catalog_ctg_order_by']);
        if ($db->nf() > 0) {
            $icon		= 'folder.gif';
            $open_icon	= 'folderopen.gif';

            $menu  = new TreeMenu();
            $first = 1; // указатель на первой позиции в выборке из БД

            while($db->next_record()) {
                $id		= $db->f('id');
                $pid	= $db->f('parent_id');
                $cname	= htmlspecialchars($db->f('title'), ENT_QUOTES);
                $level	= $db->f('level');
                $alias	= htmlspecialchars($db->f('alias'));
                if ($type != 'tree') {
                    $text = $cname;
                    $link = $id;
                } else {
                    $a			= ($admin) ? $baseurl : $transurl;
                    //$ctg_link	= $baseurl.'&action='.$a.'&id='.$id;

                    if($CONFIG['rewrite_mod'] && !$admin){
                        $ctg_link = substr($transurl,0,strpos($transurl,'?'));
                        $ctg_link .= sprintf('%s/', ($alias)?$alias:$id);
                    }
                    else {
                        $ctg_link	= $a.'&action=assortment&id='.$id;
                    }

                    #$ctg_link	= $a.'&action=assortment&id='.$id;
                    if ($id != $parent) $cname = sprintf('<a href="%s">%s</a>', $ctg_link, $cname);
                    $text = sprintf('%s', $cname);
                    $link = '';
                }
                $array	= array('text' => $text,
                    'link' => $link,
                    'icon' => $icon,
                    'expandedIcon' => $open_icon);
                // 'ensureVisible' => true

                if ($pid == 0) {
                    $node[$id]	= new TreeNode($array);
                    $menu->addItem($node[$id]);
                } else if ($pid != 0 && $first == 1) {
                    $node[$id]	= new TreeNode($array);
                    $menu->addItem($node[$id]);
                } else {
                    if (is_object($node[$pid])) $node[$id] = &$node[$pid]->addItem(new TreeNode($array));
                }
                $first = 0;
            }

            $treeMenu = &new TreeMenu_DHTML($menu);
            return $treeMenu->toHTML();
        }
        return FALSE;
    }

    function get_node($id = NULL, $arr = NULL, $active = FALSE) {
        GLOBAL $db, $server, $lang, $CONFIG;
        $id = (int)$id;
        if (!$id) return array();
        // $arr - массив с id-никами элемента и его дочерних элементов
        if (!in_array($id, $arr)) {
            array_push($arr, $id); $str = implode(',', $arr);
        }
        $next = 0;

        $active_str = $active ? " AND active" : "";
        $db->query('SELECT id FROM '.$this->table_prefix.'_catalog_categories WHERE !ISNULL(parent_id) AND parent_id IN ('.$str.')'.$active_str.' GROUP BY id ORDER BY clevel, '.$CONFIG['catalog_ctg_order_by']);
        if ($db->nf() > 0) {
            while($db->next_record()) {
                if (!in_array($db->f('id'), $arr)) {
                    array_push($arr,$db->f('id'));
                    $next = 1;
                }
            }
            if ($next) return $this->get_node($id, $arr);
        }

        return $arr;
    }

    function ReadLevel($id,$level=0) {
        global $server, $lang, $baseurl, $tpl;
        $CC = new CCatalogCategories_DB($this->table_prefix.'_catalog_categories', 'id');
        $str = "";
        $list=$CC->listAll($id);
        foreach($list as $r){
            $CC->load($r[id]);
            $subitems=$this->ReadLevel($r[id],$level+1);
            $str.="<li><a href=\"#\"></a><a href=\"".$baseurl."&action=assortment&id=".$r["id"]."\" class=\"lnk\">".str_replace("\\", "", $r["title"])."</a>";
            if ($subitems) $str .= "\n<ul>\n".$subitems."</ul>\n";
            $str.="</li>\n";
        }
        return ($str);
    }


    // функция вывода списка продуктов заданной категории
    function show_products ($category = null, $transurl = null, $max_rows = 10, $start = 1, $order_by = '', $ctg_owners = 0)
    {
        global $main, $db, $lang, $tpl, $baseurl, $CONFIG, $group_action;
        if (!isset($category)) return FALSE;
        $category = intval($category);
        $start_link = $start>1 ? "&start=".$start : '';

        $admin = Module::is_admin();

        $ord_vals = array(	'id' => 'id',
            'title' => 'product_title',
            'availability' => 'product_availability',
            'rank' => 'rank',
            'active' => 'active',
            'leader' => 'leader',
            'novelty' => 'novelty',
            'hits' => 'hits');
        $order = 0 === strpos($order_by, "-") ? " DESC" : "";
        $order_by = $order ? substr($order_by, 1) : $order_by;
        if (in_array($order_by, array_keys($ord_vals))) {
            $order = $ord_vals[$order_by].$order;
        } else {
            $order = $CONFIG['catalog_prod_order_by'];
        }
        if (!$admin) $active_str = " AND active = 1";

        $total = $this->get_list_with_rights(
            "a.*, a.id AS product_id, a.category_id AS product_category, a.active AS product_active,
								a.rank AS product_rank, a.leader AS product_leader,a.archive as product_archive, a.novelty AS product_novelty, a.hits AS product_hits",
            $this->table_prefix."_catalog_products a", "a",
            "a.category_id = ".$category.$active_str, "", $order,
            $start, $max_rows,
            $admin ? true : false, $ctg_owners);
        $pages_cnt = ceil($total/$max_rows);

        if (!$db->nf()) return FALSE;
        while ($db->next_record()) {
            $cat_products[] = $db->Record;
        }

        $desc = 0 === strpos($order_by, "-") ? TRUE : FALSE;
        $order_by = $desc ? substr($order_by, 1) : $order_by;
        $id_order           = (!$desc && 'id' == $order_by ? "-" : "").'id';
        $title_order        = (!$desc && 'title' == $order_by ? "-" : "").'title';
        $availability_order = (!$desc && 'availability' == $order_by ? "-" : "").'availability';
        $rank_order         = (!$desc && 'rank' == $order_by ? "-" : "").'rank';
        $active_order       = (!$desc && 'active' == $order_by ? "-" : "").'active';
        $leader_order       = (!$desc && 'leader' == $order_by ? "-" : "").'leader';
        $novelty_order       = (!$desc && 'novelty' == $order_by ? "-" : "").'novelty';
        $hits_order         = (!$desc && 'hits' == $order_by ? "-" : "").'hits';

        $id_order_mark           = 'id' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $title_order_mark        = 'title' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $availability_order_mark = 'availability' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $rank_order_mark         = 'rank' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $active_order_mark       = 'active' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $leader_order_mark       = 'leader' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $novelty_order_mark       = 'novelty' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $hits_order_mark         = 'hits' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");

        $id_order_mark2           = 'id' != $order_by ? "" : " class=\"order_curr\"";
        $title_order_mark2        = 'title' != $order_by ? "" : " class=\"order_curr\"";
        $availability_order_mark2 = 'availability' != $order_by ? "" : " class=\"order_curr\"";
        $rank_order_mark2         = 'rank' != $order_by ? "" : " class=\"order_curr\"";
        $active_order_mark2       = 'active' != $order_by ? "" : " class=\"order_curr\"";
        $leader_order_mark2       = 'leader' != $order_by ? "" : " class=\"order_curr\"";
        $novelty_order_mark2       = 'novelty' != $order_by ? "" : " class=\"order_curr\"";
        $hits_order_mark2         = 'hits' != $order_by ? "" : " class=\"order_curr\"";

        $tpl->newBlock("block_product_table");
        $tpl->assign(array(
            "MSG_Goods_list" => $this->_msg["Goods_list"],
            "MSG_Select_category" => $this->_msg["Select_category"],
            "MSG_Confirm_move_good_to" => $this->_msg["Confirm_move_good_to"],
            "MSG_No_selected_goods" => $this->_msg["No_selected_goods"],
            "MSG_Empty_field" => $this->_msg["Empty_field"],
            "MSG_Seek" => $this->_msg["Seek"],
            "MSG_Active" => $this->_msg["Active"],
            "MSG_Ed" => $this->_msg["Ed"],
            "MSG_Rights" => $main->_msg["Rights"],
            "MSG_ID" => $this->_msg["ID"],
            "MSG_Title" => $this->_msg["Title"],
            "MSG_Presence" => $this->_msg["Presence"],
            "MSG_Order" => $this->_msg["Order"],
            "MSG_Leader" => $this->_msg["Leader"],
            "MSG_Novelty" => $this->_msg["Novelty"],
            "MSG_Shows" => $this->_msg["Shows"],
            "MSG_Del" => $this->_msg["Del"],
            "MSG_Choose_action" => $this->_msg["Choose_action"],
            "MSG_Activate" 		=> $this->_msg["Activate"],
            "MSG_Suspend" 		=> $this->_msg["Suspend"],
            "MSG_Delete" 		=> $this->_msg["Delete"],
            "MSG_Select_category_for_move" => $this->_msg["Select_category_for_move"],
            "MSG_Move" => $this->_msg["Move"],
            "MSG_Execute" 		=> $this->_msg["Execute"],
            "MSG_Copy" 		=> $this->_msg["Copy"],
            "action_lang" => $lang,
            "change_catgr_action" => $baseurl."&action=changecatgr",
            "category_id" => $category,
            "search_action" => $baseurl."&action=search&id=".$category,
            "baseurl" => $baseurl,
            "id_order" => $baseurl."&action=assortment&id=".$category."&order=".$id_order,
            "title_order" => $baseurl."&action=assortment&id=".$category."&order=".$title_order,
            "availability_order" => $baseurl."&action=assortment&id=".$category."&order=".$availability_order,
            "rank_order" => $baseurl."&action=assortment&id=".$category."&order=".$rank_order,
            "active_order" => $baseurl."&action=assortment&id=".$category."&order=".$active_order,
            "leader_order" => $baseurl."&action=assortment&id=".$category."&order=".$leader_order,
            "novelty_order" => $baseurl."&action=assortment&id=".$category."&order=".$leader_order,
            "hits_order" => $baseurl."&action=assortment&id=".$category."&order=".$hits_order,
            "id_order_mark" => $id_order_mark,
            "title_order_mark" => $title_order_mark,
            "availability_order_mark" => $availability_order_mark,
            "rank_order_mark" => $rank_order_mark,
            "active_order_mark" => $active_order_mark,
            "leader_order_mark" => $leader_order_mark,
            "novelty_order_mark" => $leader_order_mark,
            "hits_order_mark" => $hits_order_mark,
            "id_order_mark2" => $id_order_mark2,
            "title_order_mark2" => $title_order_mark2,
            "availability_order_mark2" => $availability_order_mark2,
            "rank_order_mark2" => $rank_order_mark2,
            "active_order_mark2" => $active_order_mark2,
            "leader_order_mark2" => $leader_order_mark2,
            "novelty_order_mark2" => $leader_order_mark2,
            "hits_order_mark2" => $hits_order_mark2
        ));
        $is_first = $cat_products[0]["product_id"];
        $is_last = $cat_products[count($cat_products)-1]["product_id"];
        $actions = array(	"edit"		=> "editproduct".$start_link,
            "editrights"=> "editrights".$start_link,
            "activate"	=>	array("pactivate".$start_link,"psuspend".$start_link),
            "select"	=> "changeleader".$start_link,
            "novelty"	=> "changenovelty".$start_link,
            //		"archive"	=> "changearchive".$start_link,
            "delete"	=> "delproduct".$start_link,
            "confirm_delete"	=> $this->_msg["Confirm_delete_record"]
        );
        foreach ($cat_products as $product) {
            $tpl->newBlock("block_product_row");

            if( $this->getProductProperty($product["product_id"],1172) == "Рубит кубиками")
                $kub_disp = 'block';
            else
                $kub_disp = 'none';

            if( $this->getProductProperty($product["product_id"],1172) == "Инвертор")
                $inv_disp = 'block';
            else
                $inv_disp = 'none';


            $img_up   = "ico_listup.gif";
            $first    = 0;
            $img_down = "ico_listdw.gif";
            $last     = 0;

            $actions["select_title"] =	$product["product_leader"] ? $this->_msg["Confirm_unset_leader"] : $this->_msg["Confirm_set_leader"];
            $actions["select_img"] =	$product["product_leader"] ? "b_select.gif" : "b_select_off.gif";

            $actions["novelty_title"] =	$product["product_novelty"] ? $this->_msg["Confirm_unset_leader"] : $this->_msg["Confirm_set_leader"];
            $actions["novelty_img"] =	$product["product_novelty"] ? "b_select.gif" : "b_select_off.gif";


            $actions["archive_title"] =	$product["product_archive"] ? '' : '';
            $actions["archive_img"]	=	$product["product_archive"] ? "b_select.gif" : "b_select_off.gif";

            $archive_t = '<a href="'.$baseurl.'&id='.$product['product_id'].'&category_id='.$product['product_category'].'&rank='.$product['product_rank'].'&action=changearchive"><img width="15" height="15" border="0" title=""  src="/i/admin/'.$actions["archive_img"].'"></a>';

            $adm_actions	= $this->get_actions_by_rights($transurl."&id=".$product['product_id'].'&category_id='.$product['product_category'].'&rank='.$product['product_rank'], $product["active"], $product["is_owner"], $product["user_rights"], $actions);

            $tpl->assign(array(
                "MSG_Move_up"			=> $this->_msg["Move_up"],
                "MSG_Move_down"			=> $this->_msg["Move_down"],
                "MSG_Copy"  			=> $this->_msg["Copy"],
                "kub_disp"              =>  $kub_disp,
                "inv_disp"              =>  $inv_disp,
                "product_id"			=>	$product["product_id"],
                "product_name"			=>	$product["product_title"],
                "product_hits"			=>	$product["product_hits"],
                "product_currency"		=>	$product["currency"],
                "product_price_1"		=>	(float)($product["price_1"]),
                "product_price_2"		=>	(float)($product["price_2"]),
                "product_price_3"		=>	(float)($product["price_3"]),
                "product_availability"	=>	$product["product_availability"] ? $product["product_availability"] : "&nbsp;",
                "product_rank"			=>	$product["product_rank"],
                "product_edit_action"	=>	$adm_actions["edit_action"],
                "product_edit_rights_action"	=>	$adm_actions["edit_rights_action"],
                "product_del_action"	=>	$adm_actions["delete_action"],
                "change_status_action"	=>	$adm_actions["change_status_action"],
                "leader_action"			=>	$adm_actions["select_action"],
                "archive_action"			=>	$archive_t,//$adm_actions["archive_action"],
                "novelty_action"		=>	$adm_actions["novelty_action"],
                "product_link"			=>	$transurl.'&action=shwprd&prd_id='.$product['product_id'].'&id='.$product['product_category'],
                "img_dwn"				=>	$img_down,
                "img_up"				=>	$img_up,
                "is_first"				=>	$first,
                "is_last"				=>	$last,
                "product_up_link"		=>	"$transurl&action=productup&id=".$product["product_id"]."&category_id=".$product["product_category"]."&rank=".$product["product_rank"],
                "product_down_link"		=>	"$transurl&action=productdown&id=".$product["product_id"]."&category_id=".$product["product_category"]."&rank=".$product["product_rank"],
                "copy_link"     		=>	"$transurl&action=addproduct&amp;copy=".$product["product_id"],
            ));
            if ($adm_actions["rights"]["d"] || $adm_actions["rights"]["p"]) {
                $tpl->newBlock('block_check');
                $tpl->assign("product_id", $product["product_id"]);
                $group_action = true;
            }
        }
        /**
         * Вывод дерева категорий
         */
        $catgr_tree = $this->getCategsTree();
        foreach ($catgr_tree as $ctgr) {
            $tpl->newBlock("block_catgr_tree");

            $ctgr_title = "";
            for ($i = 1; $i < $ctgr['level']; $i++) {
                $ctgr_title .= "&nbsp;&nbsp;";
            }
            $ctgr_title .= $ctgr['title'];

            $tpl->assign(array(
                'ctgr_id' => $ctgr['id'],
                'ctgr_title' => $ctgr_title
            ));
        }
        return $pages_cnt;
    }

    // функция вывода списка продуктов согласно критериям поиска
    function show_search_result ($category, $search_str, $transurl = null, $max_rows = 10, $start = 1, $order_by = '') {
        global $main, $db, $server, $lang, $tpl, $baseurl, $CONFIG;
        if (!$search_str = trim($search_str)) {
            return FALSE;
        }

        $search = preg_replace("/\s+/", "&nbsp;", $search_str);
        $search = explode("&nbsp;", $search);
        $where = '';
        foreach ($search as $word) {
            $where .= "product_title LIKE '%".My_Sql::escape($word)."%' AND ";
        }
        $where = substr($where, 0, -5);
        $start_link = $start>1 ? "&start=".$start : '';

        $ord_vals = array('id' => 'id',
            'title' => 'product_title',
            'availability' => 'product_availability',
            'rank' => 'rank',
            'active' => 'active',
            'leader' => 'leader',
            'novelty' => 'novelty',
            'hits' => 'hits');
        $order = 0 === strpos($order_by, "-") ? " DESC" : "";
        $order_key = $order ? substr($order_by, 1) : $order_by;
        if (in_array($order_key, array_keys($ord_vals))) {
            $order = $ord_vals[$order_key].$order;
        } else {
            $order = $CONFIG['catalog_prod_order_by'];
        }

        $cat_products = array();
        if($category!=0)$catSearch=" category_id = ".$category." AND ";
        else $catSearch="";
        $total = $this->get_list_with_rights(
            "a.*, a.id AS product_id, a.category_id AS product_category, a.active AS product_active,
								a.rank AS product_rank, a.leader AS product_leader, a.novelty AS product_novelty, a.hits AS product_hits",
            $this->table_prefix."_catalog_products a", "a",
            $catSearch.$where, "", $order,
            $start, $max_rows,
            $admin ? true : false, $ctg_owners);
        $pages_cnt = ceil($total/$max_rows);
        if (!$db->nf()) return FALSE;
        while ($db->next_record()) $cat_products[] = $db->Record;

        if (!count($cat_products)) {
            return FALSE;
        }

        $desc = 0 === strpos($order_by, "-") ? TRUE : FALSE;
        $order_by = $desc ? substr($order_by, 1) : $order_by;
        $id_order           = (!$desc && 'id' == $order_by ? "-" : "").'id';
        $title_order        = (!$desc && 'title' == $order_by ? "-" : "").'title';
        $availability_order = (!$desc && 'availability' == $order_by ? "-" : "").'availability';
        $rank_order         = (!$desc && 'rank' == $order_by ? "-" : "").'rank';
        $active_order       = (!$desc && 'active' == $order_by ? "-" : "").'active';
        $leader_order       = (!$desc && 'leader' == $order_by ? "-" : "").'leader';
        $novelty_order       = (!$desc && 'novelty' == $order_by ? "-" : "").'novelty';
        $hits_order         = (!$desc && 'hits' == $order_by ? "-" : "").'hits';

        $id_order_mark           = 'id' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $title_order_mark        = 'title' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $availability_order_mark = 'availability' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $rank_order_mark         = 'rank' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $active_order_mark       = 'active' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $leader_order_mark       = 'leader' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $novelty_order_mark      = 'novelty' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $hits_order_mark         = 'hits' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");

        $id_order_mark2           = 'id' != $order_by ? "" : " class=\"order_curr\"";
        $title_order_mark2        = 'title' != $order_by ? "" : " class=\"order_curr\"";
        $availability_order_mark2 = 'availability' != $order_by ? "" : " class=\"order_curr\"";
        $rank_order_mark2         = 'rank' != $order_by ? "" : " class=\"order_curr\"";
        $active_order_mark2       = 'active' != $order_by ? "" : " class=\"order_curr\"";
        $leader_order_mark2       = 'leader' != $order_by ? "" : " class=\"order_curr\"";
        $novelty_order_mark2       = 'novelty' != $order_by ? "" : " class=\"order_curr\"";
        $hits_order_mark2         = 'hits' != $order_by ? "" : " class=\"order_curr\"";

        $tpl->newBlock("block_product_table");
        $tpl->assign(array(
            "MSG_Goods_list" => $this->_msg["Goods_list"],
            "MSG_Select_category" => $this->_msg["Select_category"],
            "MSG_Confirm_move_good_to" => $this->_msg["Confirm_move_good_to"],
            "MSG_No_selected_goods" => $this->_msg["No_selected_goods"],
            "MSG_Empty_field" => $this->_msg["Empty_field"],
            "MSG_Seek" => $this->_msg["Seek"],
            "MSG_Active" => $this->_msg["Active"],
            "MSG_Ed" => $this->_msg["Ed"],
            "MSG_Rights" => $main->_msg["Rights"],
            "MSG_ID" => $this->_msg["ID"],
            "MSG_Title" => $this->_msg["Title"],
            "MSG_Presence" => $this->_msg["Presence"],
            "MSG_Order" => $this->_msg["Order"],
            "MSG_Leader" => $this->_msg["Leader"],
            "MSG_Novelty" => $this->_msg["Novelty"],
            "MSG_Shows" => $this->_msg["Shows"],
            "MSG_Del" => $this->_msg["Del"],
            "MSG_Choose_action" => $this->_msg["Choose_action"],
            "MSG_Activate" 		=> $this->_msg["Activate"],
            "MSG_Suspend" 		=> $this->_msg["Suspend"],
            "MSG_Delete" 		=> $this->_msg["Delete"],
            "MSG_Select_category_for_move" => $this->_msg["Select_category_for_move"],
            "MSG_Move" => $this->_msg["Move"],
            "MSG_Execute" 		=> $this->_msg["Execute"],
            "MSG_Copy" 		=> $this->_msg["Copy"],

            "action_lang" => $lang,
            "change_catgr_action" => $baseurl."&action=changecatgr",
            "category_id" => $category,
            "search_action" => $baseurl."&action=search&id=".$category,
            "search_str" => $search_str,
            "baseurl" => $baseurl,
            "id_order" => $baseurl."&action=search&id=".$category."&search=".$search_str."&order=".$id_order,
            "title_order" => $baseurl."&action=search&id=".$category."&search=".$search_str."&order=".$title_order,
            "availability_order" => $baseurl."&action=search&id=".$category."&search=".$search_str."&order=".$availability_order,
            "rank_order" => $baseurl."&action=search&id=".$category."&search=".$search_str."&order=".$rank_order,
            "active_order" => $baseurl."&action=search&id=".$category."&search=".$search_str."&order=".$active_order,
            "leader_order" => $baseurl."&action=search&id=".$category."&search=".$search_str."&order=".$leader_order,
            "novelty_order" => $baseurl."&action=search&id=".$category."&search=".$search_str."&order=".$novelty_order,
            "hits_order" => $baseurl."&action=search&id=".$category."&search=".$search_str."&order=".$hits_order,
            "id_order_mark" => $id_order_mark,
            "title_order_mark" => $title_order_mark,
            "availability_order_mark" => $availability_order_mark,
            "rank_order_mark" => $rank_order_mark,
            "active_order_mark" => $active_order_mark,
            "leader_order_mark" => $leader_order_mark,
            "novelty_order_mark" => $novelty_order_mark,
            "hits_order_mark" => $hits_order_mark,
            "id_order_mark2" => $id_order_mark2,
            "title_order_mark2" => $title_order_mark2,
            "availability_order_mark2" => $availability_order_mark2,
            "rank_order_mark2" => $rank_order_mark2,
            "active_order_mark2" => $active_order_mark2,
            "leader_order_mark2" => $leader_order_mark2,
            "novelty_order_mark2" => $novelty_order_mark2,
            "hits_order_mark2" => $hits_order_mark2
        ));

        $is_first = $cat_products[0]["id"];
        $is_last = $cat_products[count($cat_products)-1]["id"];


        $actions = array(	"edit"		=> "editproduct".$start_link,
            "editrights"=> "editrights".$start_link,
            "activate"	=>	array("pactivate".$start_link,"psuspend".$start_link),
            "select"	=> "changeleader".$start_link,
            "novelty"	=> "changenovelty".$start_link,
            "delete"	=> "delproduct".$start_link,
            "confirm_delete"	=> $this->_msg["Confirm_delete_record"]
        );

        foreach ($cat_products as $product) {
            $tpl->newBlock("block_product_row");
            $img_up   = "ico_listup.gif";
            $first    = 0;
            $img_down = "ico_listdw.gif";
            $last     = 0;

            $actions["select_title"]=	$product["product_leader"] ? $this->_msg["Confirm_unset_leader"] : $this->_msg["Confirm_set_leader"];
            $actions["select_img"]	=	$product["product_leader"] ? "b_select.gif" : "b_select_off.gif";
            $actions["novelty_title"] =	$product["product_novelty"] ? $this->_msg["Confirm_unset_novelty"] : $this->_msg["Confirm_set_novelty"];
            $actions["novelty_img"]	=	$product["product_novelty"] ? "b_select.gif" : "b_select_off.gif";

            $actions["archive_title"] =	$product["product_archive"] ? '' : '';
            $actions["archive_img"]	=	$product["archive"] ? "b_select.gif" : "b_select_off.gif";

            $archive_t = '<a href="'.$baseurl.'&id='.$product['product_id'].'&category_id='.$product['product_category'].'&rank='.$product['product_rank'].'&action=changearchive"><img width="15" height="15" border="0" title=""  src="/i/admin/'.$actions["archive_img"].'"></a>';

            $adm_actions	= $this->get_actions_by_rights($transurl."&id=".$product['product_id'].'&category_id='.$product['product_category'].'&rank='.$product['product_rank'], $product["active"], $product["is_owner"], $product["user_rights"], $actions);
            $tpl->assign(array(
                "MSG_Move_up"			=> $this->_msg["Move_up"],
                "MSG_Move_down"			=> $this->_msg["Move_down"],
                "product_id"			=>	$product["product_id"],
                "product_name"			=>	$product["product_title"],
                "product_hits"			=>	$product["product_hits"],
                "product_currency"		=>	$product["currency"],
                "product_price_1"		=>	(float)($product["price_1"]),
                "product_price_2"		=>	(float)($product["price_2"]),
                "product_price_3"		=>	(float)($product["price_3"]),
                "product_availability"	=>	$product["product_availability"] ? $product["product_availability"] : "&nbsp;",
                "product_rank"			=>	$product["product_rank"],
                "product_edit_action"	=>	$adm_actions["edit_action"],
                "product_edit_rights_action"	=>	$adm_actions["edit_rights_action"],
                "product_del_action"	=>	$adm_actions["delete_action"],
                "change_status_action"	=>	$adm_actions["change_status_action"],
                "leader_action"			=>	$adm_actions["select_action"],
                "archive_action"			=>	$archive_t,//$adm_actions["archive_action"],
                "novelty_action"		=>	$adm_actions["novelty_action"],
                "product_link"			=>	$transurl.'&action=shwprd&prd_id='.$product['product_id'].'&id='.$product['product_category'],
                "img_dwn"				=>	$img_down,
                "img_up"				=>	$img_up,
                "is_first"				=>	$first,
                "is_last"				=>	$last,
                "product_up_link"		=>	"$transurl&action=productup&id=".$product["product_id"]."&category_id=".$product["product_category"]."&rank=".$product["product_rank"],
                "product_down_link"		=>	"$transurl&action=productdown&id=".$product["product_id"]."&category_id=".$product["product_category"]."&rank=".$product["product_rank"],
            ));
            if ($adm_actions["rights"]["d"] || $adm_actions["rights"]["p"]) {
                $tpl->newBlock('block_check');
                $tpl->assign("product_id", $product["product_id"]);
            }


        }
        /**
         * Вывод дерева категорий
         */
        $catgr_tree = $this->getCategsTree();
        foreach ($catgr_tree as $ctgr) {
            $tpl->newBlock("block_catgr_tree");

            $ctgr_title = "";
            for ($i = 1; $i < $ctgr['level']; $i++) {
                $ctgr_title .= "&nbsp;&nbsp;";
            }
            $ctgr_title .= $ctgr['title'];

            $tpl->assign(array(
                'ctgr_id' => $ctgr['id'],
                'ctgr_title' => $ctgr_title
            ));
        }

        $tpl->newBlock("block_search_on");
        $tpl->assign(array("category_id" => $category,
                "baseurl" => $baseurl,
                "MSG_Back_to_goods_list" => $this->_msg['Back_to_goods_list']
            )
        );

        return TRUE;
    }

    function show_search_result_prod ($search_str, $transurl = null, $max_rows = 10, $start = 1, $order_by = '') {
        global $main, $db, $server, $lang, $tpl, $baseurl, $CONFIG;
        if (!$search_str = trim($search_str)) {
            return FALSE;
        }

        $search = preg_replace("/\s+/", "&nbsp;", $search_str);
        $search = explode("&nbsp;", $search);
        $where = '';
        foreach ($search as $word) {
            $where .= "product_title LIKE '%".My_Sql::escape($word)."%' AND ";
        }
        $where = substr($where, 0, -5);

        $ord_vals = array('id' => 'id',
            'title' => 'product_title',
            'availability' => 'product_availability',
            'rank' => 'rank',
            'active' => 'active',
            'leader' => 'leader',
            'hits' => 'hits');
        $order = 0 === strpos($order_by, "-") ? " DESC" : "";
        $order_key = $order ? substr($order_by, 1) : $order_by;
        if (in_array($order_key, array_keys($ord_vals))) {
            $order = $ord_vals[$order_key].$order;
        } else {
            $order = $CONFIG['catalog_prod_order_by'];
        }

        $cat_products = array();

        $total = $this->get_list_with_rights(
            "a.*, a.id AS product_id, a.category_id AS product_category, a.active AS product_active,
								a.rank AS product_rank, a.leader AS product_leader, a.novelty AS product_novelty, a.hits AS product_hits",
            $this->table_prefix."_catalog_products a", "a",
            $where, "", $order,
            $start, $max_rows,
            $admin ? true : false, $ctg_owners);
        $pages_cnt = ceil($total/$max_rows);

        if (!$db->nf()) return FALSE;
        while ($db->next_record()) $cat_products[] = $db->Record;

        if (!count($cat_products)) {
            return FALSE;
        }

        $desc = 0 === strpos($order_by, "-") ? TRUE : FALSE;
        $order_by = $desc ? substr($order_by, 1) : $order_by;
        $id_order           = (!$desc && 'id' == $order_by ? "-" : "").'id';
        $title_order        = (!$desc && 'title' == $order_by ? "-" : "").'title';
        $availability_order = (!$desc && 'availability' == $order_by ? "-" : "").'availability';
        $rank_order         = (!$desc && 'rank' == $order_by ? "-" : "").'rank';
        $active_order       = (!$desc && 'active' == $order_by ? "-" : "").'active';
        $leader_order       = (!$desc && 'leader' == $order_by ? "-" : "").'leader';
        $hits_order         = (!$desc && 'hits' == $order_by ? "-" : "").'hits';

        $id_order_mark           = 'id' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $title_order_mark        = 'title' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $availability_order_mark = 'availability' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $rank_order_mark         = 'rank' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $active_order_mark       = 'active' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $leader_order_mark       = 'leader' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");
        $hits_order_mark         = 'hits' != $order_by ? "" : ($desc ? "<img src=\"/i/admin/darr.gif\" alt=\"\" border=\"0\">" : "<img src=\"/i/admin/uarr.gif\" alt=\"\" border=\"0\">");

        $id_order_mark2           = 'id' != $order_by ? "" : " class=\"order_curr\"";
        $title_order_mark2        = 'title' != $order_by ? "" : " class=\"order_curr\"";
        $availability_order_mark2 = 'availability' != $order_by ? "" : " class=\"order_curr\"";
        $rank_order_mark2         = 'rank' != $order_by ? "" : " class=\"order_curr\"";
        $active_order_mark2       = 'active' != $order_by ? "" : " class=\"order_curr\"";
        $leader_order_mark2       = 'leader' != $order_by ? "" : " class=\"order_curr\"";
        $hits_order_mark2         = 'hits' != $order_by ? "" : " class=\"order_curr\"";

        $tpl->newBlock("block_product_table");
        $tpl->assign(array(
            "MSG_Goods_list" => $this->_msg["Goods_list"],
            "MSG_Select_category" => $this->_msg["Select_category"],
            "MSG_Confirm_move_good_to" => $this->_msg["Confirm_move_good_to"],
            "MSG_No_selected_goods" => $this->_msg["No_selected_goods"],
            "MSG_Empty_field" => $this->_msg["Empty_field"],
            "MSG_Seek" => $this->_msg["Seek"],
            "MSG_Active" => $this->_msg["Active"],
            "MSG_Ed" => $this->_msg["Ed"],
            "MSG_Rights" => $main->_msg["Rights"],
            "MSG_ID" => $this->_msg["ID"],
            "MSG_Title" => $this->_msg["Title"],
            "MSG_Presence" => $this->_msg["Presence"],
            "MSG_Order" => $this->_msg["Order"],
            "MSG_Leader" => $this->_msg["Leader"],
            "MSG_Novelty" => $this->_msg["Novelty"],
            "MSG_Shows" => $this->_msg["Shows"],
            "MSG_Del" => $this->_msg["Del"],
            "MSG_Choose_action" => $this->_msg["Choose_action"],
            "MSG_Activate" 		=> $this->_msg["Activate"],
            "MSG_Suspend" 		=> $this->_msg["Suspend"],
            "MSG_Delete" 		=> $this->_msg["Delete"],
            "MSG_Select_category_for_move" => $this->_msg["Select_category_for_move"],
            "MSG_Move" => $this->_msg["Move"],
            "MSG_Execute" 		=> $this->_msg["Execute"],
            "MSG_Copy" 		=> $this->_msg["Copy"],

            "action_lang" => $lang,
            "change_catgr_action" => $baseurl."&action=changecatgr",
            //"category_id" => $category,
            "search_action" => $baseurl."&action=search",
            "search_str" => $search_str,
            "baseurl" => $baseurl,
            "id_order" => $baseurl."&action=search&search=".$search_str."&order=".$id_order,
            "title_order" => $baseurl."&action=search&search=".$search_str."&order=".$title_order,
            "availability_order" => $baseurl."&action=search&search=".$search_str."&order=".$availability_order,
            "rank_order" => $baseurl."&action=search&search=".$search_str."&order=".$rank_order,
            "active_order" => $baseurl."&action=search&search=".$search_str."&order=".$active_order,
            "leader_order" => $baseurl."&action=search&search=".$search_str."&order=".$leader_order,
            "hits_order" => $baseurl."&action=search&search=".$search_str."&order=".$hits_order,
            "id_order_mark" => $id_order_mark,
            "title_order_mark" => $title_order_mark,
            "availability_order_mark" => $availability_order_mark,
            "rank_order_mark" => $rank_order_mark,
            "active_order_mark" => $active_order_mark,
            "leader_order_mark" => $leader_order_mark,
            "hits_order_mark" => $hits_order_mark,
            "id_order_mark2" => $id_order_mark2,
            "title_order_mark2" => $title_order_mark2,
            "availability_order_mark2" => $availability_order_mark2,
            "rank_order_mark2" => $rank_order_mark2,
            "active_order_mark2" => $active_order_mark2,
            "leader_order_mark2" => $leader_order_mark2,
            "hits_order_mark2" => $hits_order_mark2
        ));

        $is_first = $cat_products[0]["id"];
        $is_last = $cat_products[count($cat_products)-1]["id"];

        $actions = array(	"edit"		=> "editproduct",
            "editrights"=> "editrights",
            "activate"	=>	array("pactivate","psuspend"),
            "select"	=> "changeleader",
            "novelty"	=> "changenovelty",
            "delete"	=> "delproduct",
            "confirm_delete"	=> $this->_msg["Confirm_delete_record"]
        );

        foreach ($cat_products as $product) {
            $tpl->newBlock("block_product_row");

            $img_up   = "ico_listup.gif";
            $first    = 0;
            $img_down = "ico_listdw.gif";
            $last     = 0;

            $actions["select_title"]=	$product["product_leader"] ? $this->_msg["Confirm_unset_leader"] : $this->_msg["Confirm_set_leader"];
            $actions["select_img"]	=	$product["product_leader"] ? "b_select.gif" : "b_select_off.gif";
            $actions["novelty_title"] =	$product["product_novelty"] ? $this->_msg["Confirm_unset_novelty"] : $this->_msg["Confirm_set_novelty"];
            $actions["novelty_img"]	=	$product["product_novelty"] ? "b_select.gif" : "b_select_off.gif";
            $adm_actions	= $this->get_actions_by_rights($transurl."&id=".$product['product_id'].'&category_id='.$product['product_category'].'&rank='.$product['product_rank'], $product["active"], $product["is_owner"], $product["user_rights"], $actions);
            $tpl->assign(array(
                "MSG_Move_up"			=> $this->_msg["Move_up"],
                "MSG_Move_down"			=> $this->_msg["Move_down"],
                "product_id"			=>	$product["product_id"],
                "product_name"			=>	$product["product_title"],
                "product_hits"			=>	$product["product_hits"],
                "product_currency"		=>	$product["currency"],
                "product_price_1"		=>	(float)($product["price_1"]),
                "product_price_2"		=>	(float)($product["price_2"]),
                "product_price_3"		=>	(float)($product["price_3"]),
                "product_availability"	=>	$product["product_availability"] ? $product["product_availability"] : "&nbsp;",
                "product_rank"			=>	$product["product_rank"],
                "product_edit_action"	=>	$adm_actions["edit_action"],
                "product_edit_rights_action"	=>	$adm_actions["edit_rights_action"],
                "product_del_action"	=>	$adm_actions["delete_action"],
                "change_status_action"	=>	$adm_actions["change_status_action"],
                "leader_action"			=>	$adm_actions["select_action"],
                "novelty_action"		=>	$adm_actions["novelty_action"],
                "product_link"			=>	$transurl.'&action=shwprd&prd_id='.$product['product_id'].'&id='.$product['product_category'],
                "img_dwn"				=>	$img_down,
                "img_up"				=>	$img_up,
                "is_first"				=>	$first,
                "is_last"				=>	$last,
                "product_up_link"		=>	"$transurl&action=productup&id=".$product["product_id"]."&category_id=".$product["product_category"]."&rank=".$product["product_rank"],
                "product_down_link"		=>	"$transurl&action=productdown&id=".$product["product_id"]."&category_id=".$product["product_category"]."&rank=".$product["product_rank"],
                "copy_link"     		=>	"$transurl&action=addproduct&amp;copy=".$product["product_id"],
            ));
            if ($adm_actions["rights"]["d"] || $adm_actions["rights"]["p"]) {
                $tpl->newBlock('block_check');
                $tpl->assign("product_id", $product["product_id"]);
            }


        }
        /**
         * Вывод дерева категорий
         */
        $catgr_tree = $this->getCategsTree();
        foreach ($catgr_tree as $ctgr) {
            $tpl->newBlock("block_catgr_tree");

            $ctgr_title = "";
            for ($i = 1; $i < $ctgr['level']; $i++) {
                $ctgr_title .= "&nbsp;&nbsp;";
            }
            $ctgr_title .= $ctgr['title'];

            $tpl->assign(array(
                'ctgr_id' => $ctgr['id'],
                'ctgr_title' => $ctgr_title
            ));
        }

        $tpl->newBlock("block_search_on");
        $tpl->assign(array("baseurl" => $baseurl,
                "MSG_Back_to_goods_list" => $this->_msg['Back_to_goods_list']
            )
        );

        return TRUE;
    }

    function getInstruction($id){
        global $server,$lang;
        $id = (int)$id;
        $query = 'SELECT * FROM '.$server.$lang.'_catalog_files WHERE product_id='.$id;
        if(!$result = mysql_query($query)) die(mysql_error());
        while($row = mysql_fetch_assoc($result)){
            if($row['name'] == 'Руководство по эксплуатации' && strstr(strtolower($row['filename']), '.pdf'))
                return '/files/catalog/'.$server.$lang.'/files/'.urlencode($row['filename']);
        }
        return '';
    }

    function showFiles($id = 0){
        global $server,$lang,$tpl,$CONFIG;
        if($id){
            $query = 'SELECT '.$server.$lang.'_catalog_files.*, pr.file_alarm as file_alarm, pr.category_id  FROM '.$server.$lang.'_catalog_files  LEFT JOIN '.$server.$lang.'_catalog_products as pr ON pr.id='.$server.$lang.'_catalog_files.product_id WHERE product_id='.$id." ORDER BY position DESC";
            if(!$result = mysql_query($query)) die(mysql_error());
            if($num = mysql_num_rows($result)) $tpl->newBlock('loaded_block');
            else $tpl->assign('files_style', 'style="display:none"');
            $i=0;
            while($row = mysql_fetch_assoc($result)){
                if($i == 0 ){
                    $tpl->assign(array(
                    'file_alarm' => $row['file_alarm'] ? '<div style="color: red;padding: 11px; border: 2px solid red;">'.$CONFIG['catalog_file_alarm'].'</div>': '',
                    'file_alarm_message' => $row['file_alarm'] && ($row['category_id'] == 446 || $row['category_id'] == 447)  ? '<a style="border: 2px solid #00aeff; color: #00aeff; float: right; padding: 5px; font-size: 14px;" href="http://www.rd-forum.ru/forums/dobavlenie-koordinat-stacionarnyx-radarov.155/">Нашли неточность в базе данных?</a>': '' ));
                    $i++;
                }

                $tpl->newBlock('loaded');
                $tpl->assign(array(
                    'name' => $row['name'],
                    'filename' => $row['filename'],
                    'position' => $row['position'],
                    'type' => $this->getType($row['filename']),
                    'path' => (strpos($row['filename'], "/"))? $row['filename'] : '/files/catalog/'.$server.$lang.'/files/'.$row['filename'],
                    'id' => $row['id'],
                    'download_count' => $row['download_count'],
                    'warning' => ($id == 2478 || $id == 2219) && ( strpos($row['name'],'Полная база данных') !== false || strpos($row['name'],'Прошивка') !== false) ? 'warning' : '',//вывод информативрного сообщения для конкретных товаров
                ));
            }
        }
        for($i = 0; $i<6;$i++){
            $tpl->newBlock('load');
        }
        return $num;
    }
    function getType($file){
        $arr = explode('.', $file);
        $ext = strtolower($arr[sizeof($arr) - 1]);
        if($ext == 'xlsx' || $ext == 'xls') return 'xls';
        if($ext == 'doc' || $ext == 'docx') return 'doc';
        if($ext == 'pdf') return 'pdf';
        if($ext == 'tiff') return 'tiff';
        if($ext == 'bmp') return 'bmp';
        if($ext == 'jpg' || $ext == 'jpeg') return 'jpg';
    }
    function addFiles($id){
        global $server,$lang;
        if($_REQUEST['pos'] && is_array($_REQUEST['pos'])){
            foreach ($_REQUEST['pos'] as $i => $v) {
                if($i) $this->update_file_position($i, $v);
            }
        }
        if($_REQUEST['dp'] && is_array($_REQUEST['dp'])){
            foreach($_REQUEST['dp'] as $k => $v)
                if($v) $this->deleteFile($k);
        }
        if($_FILES['afile'] && is_array($_FILES['afile'])){
            foreach($_FILES['afile']['name'] as $k => $v){
                if(isset($_REQUEST['urlfile'][$k]) && $_REQUEST['urlfile'][$k] != "") continue;
                $res = $this->uploadFile($k,RP.'files/catalog/'.$server.$lang.'/files/',$id);

            }
        }

        foreach($_REQUEST['urlfile'] as $k => $v){
            if(isset($v) && $v != "")
                $res = $this->SaveURLFile($v,$id, $k);
        }
    }
    function deleteFile($id){
        global $server,$lang;
        $query = 'SELECT * FROM '.$server.$lang.'_catalog_files WHERE id='.(int)$id;
        if(!$result = mysql_query($query)) die(mysql_error().$query);
        $row = mysql_fetch_assoc($result);
        if(!$row) return;
        $query1 = 'SELECT * FROM '.$server.$lang.'_catalog_files WHERE id <> '.(int)$id.' AND filename ="'.$row['filename'].'"';
        if(!$result1 = mysql_query($query1)) die(mysql_error().$query1);
        $row1 = mysql_fetch_assoc($result1);
        $row['filename']=iconv('UTF-8', 'WINDOWS-1251//IGNORE',$row['filename']);
        if(!$row1) unlink(RP.'files/catalog/'.$server.$lang.'/files/'.$row['filename']);
        $query = 'DELETE  FROM '.$server.$lang.'_catalog_files WHERE id='.(int)$id;
        if(!$result = mysql_query($query)) die(mysql_error().$query);
    }

    function update_file_position($id, $position){
        global $server,$lang, $db;
        if(mysql_query("UPDATE ".$server.$lang."_catalog_files SET position = ".$position." WHERE id=".$id)){
            return 1;
        }else return false;
    }

    function uploadFile($key,$path,$id)
    {
        global $server,$lang, $db;

        if (@$_FILES['afile']['name'][$key]) {
            if (eregi("\.(phtml|php|php3|php4|php5|shtml|cgi|exe|pl|asp|aspx|htaccess|htgroup|htpasswd)$", $_FILES['afile']['name'][$key])) {
                return -10;
            }

            if (is_uploaded_file($_FILES['afile']['tmp_name'][$key])) {

                $img_name = $_FILES['afile']['name'][$key];//   $id."_".
                $img_name=iconv('UTF-8', 'WINDOWS-1251//IGNORE',$img_name);
                if(move_uploaded_file($_FILES['afile']['tmp_name'][$key],$path.$img_name )) {
                    $img_name=iconv('WINDOWS-1251', 'UTF-8//IGNORE',$img_name);
                    @chmod ($path.$img_name, 0666);
                    $name = $db->escape($_POST['aname'][$key]);

                    if(mysql_query("INSERT INTO ".$server.$lang."_catalog_files(product_id,filename, name) VALUES($id,'$img_name', '$name') ")){
                        return 1;
                    }
                    else return -10;
                }else return -7;

            }
        }
        return 0;
    }

    function SaveURLFile($fileurl,$id, $key)
    {
        global $server,$lang, $db;

        if (@$fileurl) {
            $name = $db->escape($_POST['aname'][$key]);
            if(mysql_query("INSERT INTO ".$server.$lang."_catalog_files(product_id,filename, name) VALUES($id,'$fileurl', '$name') ")){
                return 1;
            }else return -10;
        }
        return 0;
    }



    function show_product_2($id) {
        global $server, $lang, $db, $CONFIG;
        $id = (int)$id;
        $query 	= 'SELECT * FROM '.$this->table_prefix.'_catalog_products WHERE id='.$id;
        $db->query($query);
        if ($db->nf() > 0) {
            $db->next_record();
            $array['id']					= $db->f('id');
            $array['category_id']			= $db->f('category_id');
            $array['group_id']				= $db->f('group_id');
            $array['ptype_id']				= $db->f('ptype_id');
            $array['product_code']			= htmlspecialchars($db->f('product_code'));
            $array['product_title']			= htmlspecialchars($db->f('product_title'));
            $array['alias']			        = htmlspecialchars($db->f('alias'));
            $array['product_inf']           = htmlspecialchars($db->f('product_inf'));
            $array['product_description']	= $db->f('product_description');
            $array['description']			= $db->f('description');
            $array['keywords']				= $db->f('keywords');
            $array['currency']				= $CONFIG['catalog_currencies'][$db->f('currency')];
            $array['price_1']				= (float)($db->f('price_1'));
            $array['price_2']				= (float)($db->f('price_2'));
            $array['price_3']				= (float)($db->f('price_3'));
            $array['price_4']				= (float)($db->f('price_4'));
            $array['price_5']				= (float)($db->f('price_5'));
            $array['image_middle']			= $db->f('image_middle');
            $array['image_big']				= $db->f('image_big');
            $array['product_availability']	= htmlspecialchars($db->f('product_availability'));
            $array['active']				= $db->f('active');
            $array['rank']					= $db->f('rank');
        }
        return $array;
    }

    /**
     * Получить список новинок
     * @param $ctgr_id	ID категории
     * @return mixed	FALSE - если категория не найдена, не указан ID или не содержит лидеров продаж
     * 					array() - список лидеров продаж, в противном случае
     */
    function getNovelties($ctgr_id = NULL, $maxrows = 5, $transurl = "?")
    {   global $db, $CONFIG, $baseurl;
        $ctgr_id = (int) $ctgr_id;
        if ($ctgr_id > 0) {
            $ctgr_ids = $this->getSubCtgrIds($ctgr_id);
            if ($ctgr_ids) {
                $clause = "category_id IN ({$ctgr_id}, ".implode(", ", $ctgr_ids).")";
            } else {
                $clause = "1";
            }
        } else {
            $clause = "1";
        }
        if(!$maxrows)
            $maxrows = -1;

        $clnt_price = 'prod_price_'.(@$_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : '1');

        $db->query('SELECT p.*, pt.ptype_name, pt.link as ptype_link, if(c.alias is not null and c.alias!=\'\', c.alias, c.id) as ctg_alias,
                    if(p.alias is not null and p.alias!=\'\', p.alias, p.id) as prod_alias
					FROM '.$this->table_prefix.'_catalog_products p INNER JOIN '
            .$this->table_prefix.'_catalog_categories c ON c.id=p.category_id
						LEFT JOIN '.$this->table_prefix.'_catalog_ptypes pt ON p.ptype_id = pt.id
                    WHERE '.$clause.' AND p.active = 1 AND novelty
                    ORDER BY p.rank DESC'.($maxrows == -1 ? '' : ' LIMIT 0,' . $maxrows));
        if ($db->nf() > 0) {
            $add_to_cart_link = Core::formPageLink($CONFIG['shop_page_link'], $CONFIG['shop_page_address'], $lang);
            $add_to_cart_link .= (FALSE === strpos($add_to_cart_link, "?") ? "?" : "&")."action=addtocart&id=";
            for($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $img = $db->f('image_middle');
                $img = $img && file_exists(RP.'/'.$CONFIG['catalog_img_path'].$img) ? '/'.$CONFIG['catalog_img_path'].$img : '';
                $b_img = $db->f('image_big');
                $b_img = $b_img && file_exists(RP.'/'.$CONFIG['catalog_img_path'].$b_img) ? '/'.$CONFIG['catalog_img_path'].$b_img : '';
                $arr[$i]['prod_num']		= ($i+1);
                $arr[$i]['prod_id']			= $prod_id = $db->f('id');
                $arr[$i]['prod_link']		= $this->getProdLink($transurl, $db->f('prod_alias'), $prod_id, $db->f('ctg_alias'));
                $arr[$i]['prod_title']		= $db->f('product_title');
                $arr[$i]['prod_descr']		= $db->f('product_inf');
                $arr[$i]['prod_image']		= $img;
                $arr[$i]['prod_image_big']	= $b_img;
                $arr[$i]['prod_price_1']	= $this->getCostWithTaxes($db->f('price_1'));
                $arr[$i]['prod_price_2']	= $this->getCostWithTaxes($db->f('price_2'));
                $arr[$i]['prod_price_3']	= $this->getCostWithTaxes($db->f('price_3'));
                $arr[$i]['prod_price_4']	= $this->getCostWithTaxes($db->f('price_4'));
                $arr[$i]['prod_price_5']	= $this->getCostWithTaxes($db->f('price_5'));
                $arr[$i]['prod_price']		= $arr[$i][$clnt_price];
                $arr[$i]['prod_currency']	= $CONFIG['catalog_currencies'][$db->f('currency')];
                $arr[$i]['prod_currency_name']	= $this->currencies[$db->f('currency')];
                $arr[$i]['prod_availability']	= $db->f('product_availability');
                $arr[$i]['prod_ptype_name']	= $db->f('ptype_name');
                $arr[$i]['prod_ptype_link']	= $db->f('ptype_link');
                $arr[$i]['bonus']	= $db->f('bonus');
                $arr[$i]['weight']	= $db->f('weight');
                $arr[$i]['file_alarm']	= $db->f('file_alarm');
                $arr[$i]['dimensions']	= $db->f('dimensions');
                $arr[$i]['product_count']	= $db->f('product_count');
                $arr[$i]['add_to_cart_link']= $add_to_cart_link . $prod_id;
            }
            $this->setPropValForProdList($arr);
            return $arr;
        }

        return FALSE;
    }

    /**
     * Получить список лидеров продаж для категории
     * @param $ctgr_id	ID категории
     * @return mixed	FALSE - если категория не найдена, не указан ID или не содержит лидеров продаж
     * 					array() - список лидеров продаж, в противном случае
     */
    function getCategoriesLeades($ctgr_id = NULL, $maxrows = 5, $transurl = "?")
    {   global $db, $CONFIG, $baseurl;
        $ctgr_id = (int) $ctgr_id;
        if ($ctgr_id > 0) {
            $ctgr_ids = $this->getSubCtgrIds($ctgr_id);
            if ($ctgr_ids) {
                $clause = "category_id IN ({$ctgr_id}, ".implode(", ", $ctgr_ids).")";
            } else {
                $clause = "1";
            }
        } else {
            $clause = "1";
        }
        $maxrows = $maxrows >= 1 ? (int)$maxrows : 0;

        $clnt_price = 'prod_price_'.(@$_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : '1');

        $db->query('SELECT p.*, pt.ptype_name, pt.link as ptype_link, if(c.alias is not null and c.alias!=\'\', c.alias, c.id) as ctg_alias,
                    if(p.alias is not null and p.alias!=\'\', p.alias, p.id) as prod_alias
					FROM '.$this->table_prefix.'_catalog_products p INNER JOIN '
            .$this->table_prefix.'_catalog_categories c ON c.id=p.category_id
						LEFT JOIN '.$this->table_prefix.'_catalog_ptypes pt ON p.ptype_id = pt.id
                    WHERE '.$clause.' AND p.active = 1 AND leader
                    ORDER BY p.rank DESC'.($maxrows>0 ? ' LIMIT 0,' . $maxrows : ''));

        if ($db->nf() > 0) {
            $add_to_cart_link = Core::formPageLink($CONFIG['shop_page_link'], $CONFIG['shop_page_address'], $lang);
            $add_to_cart_link .= (FALSE === strpos($add_to_cart_link, "?") ? "?" : "&")."action=addtocart&id=";
            for($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $img = $db->f('image_middle');
                $img = $img && file_exists(RP.'/'.$CONFIG['catalog_img_path'].$img) ? '/'.$CONFIG['catalog_img_path'].$img : '';
                $b_img = $db->f('image_big');
                $b_img = $b_img && file_exists(RP.'/'.$CONFIG['catalog_img_path'].$b_img) ? '/'.$CONFIG['catalog_img_path'].$b_img : '';
                $arr[$i]['prod_num']		= ($i+1);
                $arr[$i]['prod_id']			= $prod_id = $db->f('id');
                $arr[$i]['prod_link']		= $this->getProdLink($transurl, $db->f('prod_alias'), $prod_id, $db->f('ctg_alias'));
                $arr[$i]['prod_title']		= $db->f('product_title');
                $arr[$i]['prod_descr']		= $db->f('product_inf');
                $arr[$i]['prod_image']		= $img;
                $arr[$i]['prod_image_big']	= $b_img;
                $arr[$i]['prod_price_1']	= $this->getCostWithTaxes($db->f('price_1'));
                $arr[$i]['prod_price_2']	= $this->getCostWithTaxes($db->f('price_2'));
                $arr[$i]['prod_price_3']	= $this->getCostWithTaxes($db->f('price_3'));
                $arr[$i]['prod_price_4']	= $this->getCostWithTaxes($db->f('price_4'));
                $arr[$i]['prod_price_5']	= $this->getCostWithTaxes($db->f('price_5'));
                $arr[$i]['prod_price']		= $arr[$i][$clnt_price];
                $arr[$i]['prod_currency']	= $CONFIG['catalog_currencies'][$db->f('currency')];
                $arr[$i]['prod_currency_name']	= $this->currencies[$db->f('currency')];
                $arr[$i]['prod_availability']	= $db->f('product_availability');
                $arr[$i]['prod_ptype_name']	= $db->f('ptype_name');
                $arr[$i]['prod_ptype_link']	= $db->f('ptype_link');
                $arr[$i]['add_to_cart_link']= $add_to_cart_link . $prod_id;
            }
            $this->setPropValForProdList($arr);

            return $arr;
        }

        return FALSE;
    }

    /**
     * Получить список лидеров продаж
     */
    function getLeades()
    {   global $db, $CONFIG, $baseurl;
        $clnt_price = 'prod_price_'.(@$_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : '1');

        $db->query('SELECT p.*, if(c.alias is not null and c.alias!=\'\', c.alias, c.id) as ctg_alias,
                    if(p.alias is not null and p.alias!=\'\', p.alias, c.id) as prod_alias FROM '
            .$this->table_prefix.'_catalog_products p INNER JOIN '.$this->table_prefix.'_catalog_categories c ON c.id=p.category_id
                    WHERE p.active = 1 AND leader
                    ORDER BY p.rank DESC');
        if ($db->nf() > 0) {
            $add_to_cart_link = Core::formPageLink($CONFIG['shop_page_link'], $CONFIG['shop_page_address'], $lang);
            $add_to_cart_link .= (FALSE === strpos($add_to_cart_link, "?") ? "?" : "&")."action=addtocart&id=";
            for($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]['prod_num']		= ($i+1);
                $arr[$i]['prod_id']			= $prod_id = $db->f('id');
                $arr[$i]['prod_link']		= $this->getProdLink($transurl, $db->f('prod_alias'), $prod_id, $db->f('ctg_alias'));
                $arr[$i]['prod_title']		= $db->f('product_title');
                $arr[$i]['prod_descr']		= $db->f('product_inf');
                $arr[$i]['prod_image']		= '/'.$CONFIG['catalog_img_path'].$db->f('image_middle');
                $arr[$i]['prod_price_1']	= $this->getCostWithTaxes($db->f('price_1'));
                $arr[$i]['prod_price_2']	= $this->getCostWithTaxes($db->f('price_2'));
                $arr[$i]['prod_price_3']	= $this->getCostWithTaxes($db->f('price_3'));
                $arr[$i]['prod_price']		= $arr[$i][$clnt_price];
                $arr[$i]['prod_currency']	= $CONFIG['catalog_currencies'][$db->f('currency')];
                $arr[$i]['add_to_cart_link']= $add_to_cart_link.$prod_id;
            }
            $this->setPropValForProdList($arr);

            return $arr;
        }

        return FALSE;
    }

    /**
     * Установлен ли модуль "Магазин"
     * @param void
     * @return boolean
     */
    function issetShopMod()
    {   GLOBAL $CONFIG;
        return isset($CONFIG['shop_order_recipient']);
    }

    /**
     * Получить все статические и динамические свойства товаров
     * и список используемых динамических свойств
     *
     * @param array $prod_id_list Список id товаров
     * @return mixed array/false
     */
    function getProductsByIdList($prod_id_list)
    {
        global $db, $CONFIG;

        if (!is_array($prod_id_list)) {
            return FALSE;
        }

        $id_list_str = "";
        foreach ($prod_id_list as $k=>$id) {
            $id_list_str .= $id > 1 ? (int)$id.", " : "";
        }
        $id_list_str = substr($id_list_str, 0, -2);

        if (!$id_list_str) {
            return FALSE;
        }

        $sql = "SELECT category_id FROM ".$this->table_prefix."_catalog_products WHERE id IN (" . $id_list_str . ")";
        $cats = $db->getArrayOfResult($sql);
        foreach ( $cats as $value )
            $cats_out[] = $value;

        $first = getAllCats($cats_out[0]);
        echo '<pre>';
        print_r($first);
        die();


        $query = "SELECT P.id, category_id, P.group_id, ptype_id, ptype_name, PR.link AS ptype_link, product_title, product_inf,";
        $query .= " currency, price_1, price_2, price_3, image_middle, product_availability,";
        $query .= " if(c.alias is not null and c.alias!='', c.alias, c.id) as ctg_alias,";
        $query .= " if(P.alias is not null and P.alias!='', P.alias, P.id) as prod_alias";
        $query .= " FROM ".$this->table_prefix."_catalog_products AS P INNER JOIN "
            .$this->table_prefix."_catalog_categories c ON c.id=P.category_id LEFT JOIN ";
        $query .= $this->table_prefix."_catalog_ptypes AS PR ON ptype_id=PR.id WHERE P.id IN (".$id_list_str.")";

        $db->query($query);
        if ($db->nf() < 0) {
            return FALSE;
        }

        $ret = array();
        while ($db->next_record()) {
            $prod_id = $db->f('id');
            $ret[$prod_id]['category_id']  = $db->f('category_id');
            $ret[$prod_id]['group_id']     = $db->f('group_id');
            $ret[$prod_id]['ptype_id']     = $db->f('ptype_id');
            $ret[$prod_id]['producer']     = $db->f('ptype_name');
            $ret[$prod_id]['producer_link']= $db->f('ptype_link');
            $ret[$prod_id]['title']        = $db->f('product_title');
            $ret[$prod_id]['info']         = $db->f('product_inf');
            $ret[$prod_id]['currency']     = $CONFIG['catalog_currencies'][$db->f('currency')];
            $ret[$prod_id]['price_1']      = $this->getCostWithTaxes($db->f('price_1'));
            $ret[$prod_id]['price_2']      = $this->getCostWithTaxes($db->f('price_2'));
            $ret[$prod_id]['price_3']      = $this->getCostWithTaxes($db->f('price_3'));
            $ret[$prod_id]['img']          = $db->f('image_middle');
            $ret[$prod_id]['availability'] = $db->f('product_availability');
            $ret[$prod_id]['ctg_alias'] = $db->f('ctg_alias');
            $ret[$prod_id]['prod_alias'] = $db->f('prod_alias');
            $ret[$prod_id]['prp_val']     = array();
        }

        /**
         * Выбор всех динамических свойств сравниваемых продуктов
         */
        $prp_list = array(); /* Список динамических свойств */
        $query = "SELECT * FROM ".$this->table_prefix."_catalog_property_values WHERE product_id IN (".$id_list_str.")";
        //echo $query;
        $db->query($query);
        if ($db->nf() > 0) {
            while ($db->next_record()) {
                $val_id  = $db->f('id');
                $prod_id = $db->f('product_id');
                $prp_id  = $db->f('property_id');

                $ret[$prod_id]['prp_val'][$prp_id] = $db->f('value');
                //$ret[$prod_id]['prop_val'][$val_id]['property_id'] = $prp_id;

                if (!in_array($prp_id, $prp_list)) {
                    $prop_list[] = $prp_id;
                }
            }
        }
        $ret['propertys'] = $prop_list;

        return $ret;
    }

    /**
     * Получить список динамических свойств
     *
     * @param array $prop_id_list Список id свойств
     * @return array/false
     */
    function getPropertysByIdList($prp_id_list)
    {
        global $db;

        if (!is_array($prp_id_list) || empty($prp_id_list)) {
            return FALSE;
        }

        $id_list_str = implode(",", array_map('intval', $prp_id_list));

        $db->query("SELECT * FROM ".$this->table_prefix
            ."_catalog_properties_table WHERE id IN (".$id_list_str.")");
        if ($db->nf() < 0) {
            return FALSE;
        }

        $ret = array();
        while ($db->next_record()) {
            $prp_id = $db->f('id');
            $ret[$prp_id]['property_title']  = $db->f('property_title');
            $ret[$prp_id]['property_name']   = $db->f('property_name');
            $ret[$prp_id]['property_prefix'] = $db->f('property_prefix');
            $ret[$prp_id]['property_suffix'] = $db->f('property_suffix');
            $ret[$prp_id]['type']            = $db->f('type ');
        }

        return $ret;
    }

    /**
     * Отобразить сравниваемые товары
     *
     * @param array $prod_id_list
     * @return boolean
     */
    function showComparedProducts($prod_id_list)
    {
        global $tpl, $CONFIG, $baseurl;

        if (!$prod_list = $this->getProductsByIdList($prod_id_list)) {
            $tpl->newBlock('block_no_compare');
            return FALSE;
        }

        $prp_list = $this->getPropertysByIdList($prod_list['propertys']); /* Список используемых динамических свойств */
        unset($prod_list['propertys']);

        $tpl->newBlock('block_compare');
        $prod_count = count($prod_list);
        $tpl->assign(array(
            'prod_count'	=> $prod_count,
            'form_act'		=> false == strpos($baseurl, "?")
                    ? $baseurl."?action=delcmpprd"
                    : $baseurl."&action=delcmpprd",
        ));
        $clnt_price = 'price_'.(@$_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : '1');
        /**
         * Вывод статических свойств
         */
        $isset_prop = array(
            'producer' => false,
            'info' => false,
            'currency' => false,
            'price' => false,
            'price_1' => false,
            'price_2' => false,
            'price_3' => false,
            'availability' => false,
            'img' => false,
        );
        /* Название */
        $tpl->newBlock('block_title');
        foreach ($prod_list as $id=>$val) {
            $tpl->newBlock('block_title_row');
            //$tpl->assign('title', '' === $val['title'] ? $CONFIG['catalog_empty_prp'] : $val['title']);
            $tpl->assign(array(
                'title' => '' === $val['title'] ? $CONFIG['catalog_empty_prp'] : $val['title'],
                'prod_id' => $id,
                'prod_link'	=> $this->getProdLink($baseurl, $val['prod_alias'], $id, $val['ctg_alias'])
            ));
            if ($val['producer']) {
                $isset_prop['producer'] = true;
            }
            if ($val['info']) {
                $isset_prop['info'] = true;
            }
            if ($val['currency']) {
                $isset_prop['currency'] = true;
            }
            if ($val[$clnt_price]) {
                $isset_prop['price'] = true;
            }
            if ($val['price_1']) {
                $isset_prop['price_1'] = true;
            }
            if ($val['price_2']) {
                $isset_prop['price_2'] = true;
            }
            if ($val['price_3']) {
                $isset_prop['price_3'] = true;
            }
            if ($val['availability']) {
                $isset_prop['availability'] = true;
            }
            if ($val['img'] && file_exists(RP.$CONFIG['catalog_img_path'].$val['img'])) {
                $isset_prop['img'] = true;
            }
        }

        /* Производитель */
        if ($isset_prop['producer']) {
            $tpl->newBlock('block_producer');
            foreach ($prod_list as $id=>$val) {
                $tpl->newBlock('block_producer_row');
                $tpl->assign('producer', '' === $val['producer'] ? $CONFIG['catalog_empty_prp'] : $val['producer']);
            }
        }

        /* Краткое описание */
        if ($isset_prop['info']) {
            $tpl->newBlock('block_info');
            foreach ($prod_list as $id=>$val) {
                $tpl->newBlock('block_info_row');
                $tpl->assign('info', '' === $val['info'] ? $CONFIG['catalog_empty_prp'] : $val['info']);
            }
        }
        /* Валюта */
        if ($isset_prop['currency']) {
            $tpl->newBlock('block_currency');
            foreach ($prod_list as $id=>$val) {
                $tpl->newBlock('block_currency_row');
                $tpl->assign('currency', '' === $val['currency'] ? $CONFIG['catalog_empty_prp'] :  $val['currency']);
            }
        }
        /* Цена */
        if ($isset_prop['price']) {
            $tpl->newBlock('block_price');
            foreach ($prod_list as $id=>$val) {
                $tpl->newBlock('block_price_row');
                $tpl->assign('price', 0 == $val[$clnt_price] ? $CONFIG['catalog_empty_prp'] : $val[$clnt_price]);
            }
        }

        /* Розничная цена */
        if ($isset_prop['price_1'] && $CONFIG['catalog_show_price_1']) {
            $tpl->newBlock('block_price_1');
            foreach ($prod_list as $id=>$val) {
                $tpl->newBlock('block_price_1_row');
                $tpl->assign('price_1', 0 == $val['price_1'] ? $CONFIG['catalog_empty_prp'] : $val['price_1']);
            }
        }
        /* Партнёрская цена */
        if ($isset_prop['price_2'] && $CONFIG['catalog_show_price_2']) {
            $tpl->newBlock('block_price_2');
            foreach ($prod_list as $id=>$val) {
                $tpl->newBlock('block_price_2_row');
                $tpl->assign('price_2', 0 == $val['price_2'] ? $CONFIG['catalog_empty_prp'] : $val['price_2']);
            }
        }

        /* Дилерская цена */
        if ($isset_prop['price_3'] && $CONFIG['catalog_show_price_3']) {
            $tpl->newBlock('block_price_3');
            foreach ($prod_list as $id=>$val) {
                $tpl->newBlock('block_price_3_row');
                $tpl->assign('price_3', 0 == $val['price_3'] ? $CONFIG['catalog_empty_prp'] : $val['price_3']);
            }
        }

        /* Наличие */
        if ($isset_prop['availability']) {
            $tpl->newBlock('block_availability');
            foreach ($prod_list as $id=>$val) {
                $tpl->newBlock('block_availability_row');
                $tpl->assign('availability', '' === $val['availability'] ? $CONFIG['catalog_empty_prp'] : $val['availability']);
            }
        }

        /* пзображение */
        if ($isset_prop['img']) {
            $tpl->newBlock('block_img');
            foreach ($prod_list as $id=>$val) {
                $tpl->newBlock('block_img_row');
                if ($val['img'] && file_exists(RP.$CONFIG['catalog_img_path'].$val['img'])) {
                    $tpl->newBlock('block_img_exist');
                    $tpl->assign('img_path', $CONFIG['catalog_img_path'].$val['img']);
                } else {
                    $tpl->newBlock('block_img_no_exist');
                }
            }
        }

        /**
         * Вывод динамических свойств
         */
        if (!$prp_list) {
            return TRUE;
        }

        $tpl->newBlock('block_property_list');
        $tpl->assign('prod_count', $prod_count+1);

        foreach ($prp_list as $prp_id=>$val) {
            $tpl->newBlock('block_property');
            $tpl->assign('property_title', $val['property_title']);
            foreach ($prod_list as $prod_id=>$prod_val) {
                $tpl->newBlock('block_property_row');
                $prp_pref = $prp_suf = '';
                $prp_val = $CONFIG['catalog_empty_prp'];
                if (isset($prod_val['prp_val'][$prp_id]) && '' !== $prod_val['prp_val'][$prp_id]) {
                    $prp_val  = $prod_val['prp_val'][$prp_id];
                    $prp_pref = $val['property_prefix'];
                    $prp_suf  = $val['property_suffix'];
                }
                $tpl->assign(array('property_value' => $prp_val,
                        'property_prefix' => $prp_pref,
                        'property_suffix' => $prp_suf
                    )
                );
            }
        }

        return TRUE;
    }

    /**
     * Получить связанных соваров
     *
     * @param int     $prod_id ID товара
     * @param boolean $active  Флаг - только опубликованные товары
     * @return mixed false/array
     */
    function getConcernedProducts($type, $prod_id, $active = FALSE, $order_by = '', $category_id)
    {
        global $db, $tpl, $CONFIG;
        $prod_id = (int)$prod_id;
        if ($prod_id < 1) {
            return FALSE;
        }
        $order_by = $order_by ? $db->escape($order_by) : $CONFIG['catalog_concrn_order_by'];
        $active_str = $active ? " AND P.active" : "";

        $query = "SELECT C.*, product_title, product_code, category_id, product_inf, currency, price_1, price_2, price_3,"
            ." price_4, price_5, image_middle, image_big, product_availability, P.active, leader, novelty, ptype_name, PT.link AS ptype_link,"
            ." if(P.alias is not null and P.alias!='', P.alias, P.id) as prod_alias,"
            ." if(CT.alias is not null and CT.alias!='', CT.alias, CT.id) as ctg_alias,CT.title as categ_title"
            ." FROM ".$this->table_prefix."_catalog_concerned_products AS C INNER JOIN "
            .$this->table_prefix."_catalog_products AS P ON C.concrn_id = P.id INNER JOIN "
            .$this->table_prefix."_catalog_categories  CT ON CT.id=P.category_id LEFT JOIN "
            .$this->table_prefix."_catalog_ptypes AS PT ON P.ptype_id = PT.id"
            ." WHERE C.type=".intval($type)." AND C.prod_id = ".$prod_id.$active_str.($_REQUEST['new_s'] ? ' AND P.price_1>0 AND product_availability=\'1\'' : '')
            ." ORDER BY P.rank";
        $db->query($query);
        if (!$db->nf()) {
            if($type == 1 || $type == 2){
                $query = "SELECT  product_title, product_code, category_id, product_inf, currency, price_1, price_2, price_3,"
                    ." price_4, price_5, image_middle, image_big, product_availability, P.active, leader, novelty, ptype_name, PT.link AS ptype_link,"
                    ." if(P.alias is not null and P.alias!='', P.alias, P.id) as prod_alias,"
                    ." if(CT.alias is not null and CT.alias!='', CT.alias, CT.id) as ctg_alias,CT.title as categ_title"
                    ." FROM  "
                    .$this->table_prefix."_catalog_products AS P  INNER JOIN "
                    .$this->table_prefix."_catalog_categories AS CT ON CT.id=P.category_id LEFT JOIN "
                    .$this->table_prefix."_catalog_ptypes AS PT ON P.ptype_id = PT.id"
                    ." WHERE P.category_id=".intval($category_id)." AND P.active = 1 AND P.id!=".(int)$prod_id.($_REQUEST['new_s'] ? ' AND P.price_1>0 AND product_availability=\'1\'' : '')
                    ." ORDER BY P.rank LIMIT 9";
                //echo $query;
                $db->query($query);
            }
            else return FALSE;
        }

        $ret = array();
        while ($db->next_record()) {
            $ret[] = array('concrn_id'    => $db->f('concrn_id'),
                'concrn_title' => $db->f('product_title'),
                'type'         => $db->f('type'),
                'product_code' => $db->f('product_code'),
                'concrn_title_strip' => strip_tags($db->f('product_title')),
                'category_id'  => $db->f('category_id'),
                'categ_title'  => $db->f('categ_title'),
                'active'       => $db->f('active'),
                'rank'         => $db->f('rank'),
                'product_inf'  => $db->f('product_inf'),
                'currency'     => $CONFIG['catalog_currencies'][$db->f('currency')],
                'price_1'      => $db->f('price_1'),
                'price_2'      => $db->f('price_2'),
                'price_3'      => $db->f('price_3'),
                'price_4'      => $db->f('price_4'),
                'price_5'      => $db->f('price_5'),
                'image_middle' => $db->f('image_middle'),
                'image_big'	=> $db->f('image_big'),
                'availability' => $db->f('product_availability'),
                'leader' => $db->f('leader'),
                'novelty' => $db->f('novelty'),
                'ptype_name'	  => $db->f('ptype_name'),
                'ptype_link'	  => $db->f('ptype_link'),
                'ctg_alias'	  => $db->f('ctg_alias'),
                'prod_alias'	  => $db->f('prod_alias'),
            );
        }

        return $ret;
    }

    /**
     * Отобразить связанные товары
     *
     * @param int $prod_id
     * @param array $prod_list
     * @return boolean
     */
    function showConcernedProducts($prod_id, $prod_list, $black_name, $tarnseurl = '')
    {
        global $tpl, $baseurl, $CONFIG;
        $prod_id = (int) $prod_id;
        if (!is_array($prod_list) || !count($prod_list) || $prod_id < 1) {
            return FALSE;
        }
        $tarnseurl = $tarnseurl ? $tarnseurl : $baseurl;
        $clnt_price = 'price_'.(@$_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : '1');
        $ii = 1;
        $len = sizeof($prod_list);
        foreach ($prod_list as $key=>$prod) {
            $shop_link = Core::formPageLink($CONFIG['shop_page_link'],$CONFIG['shop_page_address'], $lang, 1);
            $tpl->newBlock('block_'.$black_name.'_row');
            $link = $this->getProdLink($tarnseurl, $prod['prod_alias'], $prod['concrn_id'], $prod['ctg_alias']);
            $tpl->assign(array(
                'id' => $prod['concrn_id'],
                'category_id' => $prod['category_id'],
                'rank' => $prod['rank'],
                'categ_title' => $prod['categ_title'],
                'title' => htmlspecialchars($prod['concrn_title']),
                'title_strip' => $prod['concrn_title_strip'],
                'prod_link'		=> $link,
                'product_inf'	=> $prod['product_inf'],
                'currency'		=> $prod['currency'],
                'currency_name'	=> $this->currencies[$prod['currency']],
                'price_1'		=> $this->getCostWithTaxes($prod['price_1']),
                'price_2'		=> $this->getCostWithTaxes($prod['price_2']),
                'price_3'		=> $this->getCostWithTaxes($prod['price_3']),
                'price_4'		=> $this->getCostWithTaxes($prod['price_4']),
                'price_5'		=> $this->getCostWithTaxes($prod['price_5']),
                'price'			=> (int)$this->getCostWithTaxes($prod[$clnt_price]),
                'availability'	=> $prod['availability'],
                'ptype_name'		=> $prod['ptype_name'],
                'ptype_link'		=> $prod['ptype_link'],
                'add_to_cart_action' => $shop_link.($CONFIG['rewrite_mod'] ? '' : '&').'action=addtocart',
                'action'			=> 'addtocart',
                'active'			=> $prod['active'] ? $this->_msg['Yes'] : $this->_msg['No'],
                'del_link'		=> $baseurl.'&action=delconcrn&id='.$prod_id.'&concrn_id='.$prod['concrn_id'].'&category_id='.$prod['category_id'],
                'edit_link'		=> $baseurl.'&action=editproduct&id='.$prod['concrn_id'].'&category_id='.$prod['category_id'],
                "MSG_Edit"		=> $this->_msg["Edit"],
                "MSG_Confirm_delete_assigned_rec" => $this->_msg["Confirm_delete_assigned_rec"],
                "MSG_Delete"	=> $this->_msg["Delete"],
            ));
            if ($prod['ptype_link']) {
                $tpl->newBlock('block_'.$black_name.'_ptype'.((strtolower(substr($prod['ptype_link'], 0, 7)) === "http://") ? '' : '_local').'_link');
                $tpl->assign(array(
                    'ptype_name' => $prod['ptype_name'],
                    'ptype_link' => $prod['ptype_link'],
                ));
            } else if ($prod['ptype_name']) {
                $tpl->newBlock('block_'.$black_name.'_ptype_without_link');
                $tpl->assign('ptype_name', $prod['ptype_name']);
            }

            if ($prod['image_middle'] && file_exists(RP.$CONFIG['catalog_img_path'].$prod['image_middle'])) {
                $tpl->newBlock('block_'.$black_name.'_image_middle');
                $tpl->assign(array(
                    'image_path'	=> "/".$CONFIG['catalog_img_path'].$prod['image_middle'],
                    'img_alt'		=> htmlspecialchars($prod['concrn_title']),
                    'prod_link'		=> $link,
                ));
            } else {
                $tpl->newBlock('block_'.$black_name.'_image_middle_no');
            }
            if ($prod['image_big'] && file_exists(RP.$CONFIG['catalog_img_path'].$prod['image_big'])) {
                $tpl->newBlock('block_'.$black_name.'_image_big');
                $tpl->assign(array(
                    'image_path'	=> "/".$CONFIG['catalog_img_path'].$prod['image_big'],
                    'img_alt'		=> htmlspecialchars($prod['concrn_title']),
                    'prod_link'		=> $link,
                ));
            } else {
                $tpl->newBlock('block_'.$black_name.'_image_big_no');
            }
            if ($prod['availability']) {
                $tpl->newBlock('block_'.$black_name.'_prd_availability');
                $tpl->assign('availability', $prod['availability']);
            }
            if($ii != $len) $tpl->newBlock($black_name.'_delim');
            $ii++;
            if($prod['price_2'] == 0){

                $tpl->newBlock($black_name.'_price');
                $tpl->assign('link', $link);
                $tpl->assign('price', (int)$prod['price_1']);
            }else{
                $tpl->newBlock($black_name.'_dprice');
                $tpl->assign('link', $link);
                $tpl->assign('price', (int)$prod['price_1']);
                $tpl->assign('percent', calcDiscount($prod['price_1'], $prod['price_2']));
            }


        }

        return TRUE;
    }

    // функция для отображения производителей
    function show_ptypes($selected_ptype = '') {
        global $CONFIG, $db, $tpl, $baseurl, $server, $lang;
        $selected_ptype = (int)$selected_ptype;
        $lang		 	= $db->escape(trim($lang));
        $query = 'SELECT * FROM '.$this->table_prefix.'_catalog_ptypes ORDER BY '.$CONFIG['catalog_ptype_order_by'];
        $db->query($query);
        if ($db->nf() > 0) {
            while($db->next_record()) {
                // вывод на экран списка
                $selected = ($db->f("id") == $selected_ptype) ? " selected" : "";
                $tpl->newBlock("block_ptype");
                $tpl->assign(Array(
                    "MSG_Edit" => $this->_msg["Edit"],
                    "MSG_Delete" => $this->_msg["Delete"],
                ));
                $tpl->assign(array(
                    'ptype_name'	    =>	$db->f("ptype_name"),
                    'link'		    =>	$db->f("link"),
                    'ptype_link'	    =>	$baseurl.'&action=showptype&id='.$db->f("id"),
                    'ptype_edit_link'	=>	$baseurl.'&action=editptype&id='.$db->f("id"),
                    'ptype_del_link'	=>	$baseurl.'&action=delptype&id='.$db->f("id"),
                    'ptype_id'	    =>	$db->f('id'),
                    'selected'	    =>	$selected,
                ));
                if ($db->f("link")) {
                    $tpl->newBlock('block_ptype_link');
                    $tpl->assign(array(
                        'ptype_title' => $db->f("ptype_name"),
                        'ptype_link' => $db->f("link"),
                    ));
                } else if ($db->f("ptype_name")) {
                    $tpl->newBlock('block_ptype_without_link');
                    $tpl->assign('ptype_title', $db->f("ptype_name"));
                }
            }
            return $db->nf();
        }
        return FALSE;
    }

    // функция для формирования блоков динамических свойств
    function formDynamic($transurl) {
        global $db, $tpl, $server, $lang, $request_sdata, $CONFIG, $core;
        $js_arrays .= "groupBlocks[0] = [];";
        $query = 'SELECT g.id, p.property_id FROM '.$this->table_prefix.'_catalog_groups g
					LEFT JOIN '.$this->table_prefix.'_catalog_properties p ON g.id=p.group_id AND p.search=1
					ORDER BY g.id, property_id';
        $db->query($query);
        $curr_group = 0;
        if ($db->nf() > 0) {
            while($db->next_record()) {
                if ($curr_group != $db->f('id')) {
                    if ($curr_group != 0) $js_arrays = substr($js_arrays,0,-1)."];";
                    $curr_group = $db->f('id');
                    $js_arrays .= "groupBlocks[".$curr_group."] = [";
                }
                $js_arrays .= $db->f('property_id').",";
            }
            $js_arrays = substr($js_arrays,0,-1)."];";
        }
        $js_arrays .= "dynBlocks = [ ";

        $query = 'SELECT DISTINCT p.property_id, p.range, pt.property_title, pt.property_name, pt.property_prefix, pt.property_suffix, pt.type, pdv.value
					FROM '.$this->table_prefix.'_catalog_properties p
                    JOIN '.$this->table_prefix.'_catalog_properties_table pt
						LEFT JOIN '.$this->table_prefix.'_catalog_properties_def_values pdv ON p.property_id = pdv.property_id
					WHERE p.search = 1
						AND p.property_id = pt.id
					ORDER BY p.ord, pdv.value';
        $properties = array();
        $db->query($query);
        $last_property = 0;
        if ($db->nf() > 0) {
            while($db->next_record()) {
                if ($last_property != $db->f('property_id')) {
                    $last_property = $db->f('property_id');
                    $js_arrays .= $last_property.",";
                }
                $properties[$db->f('property_id')]["title"] = $db->f('property_title');
                $properties[$db->f('property_id')]["name"] = $db->f('property_name');
                $properties[$db->f('property_id')]["prefix"] = $db->f('property_prefix');
                $properties[$db->f('property_id')]["suffix"] = $db->f('property_suffix');
                $properties[$db->f('property_id')]["type"] = $db->f('type');
                $properties[$db->f('property_id')]["range"] = $db->f('range');
                $properties[$db->f('property_id')]["values"][] = $db->f('value');
            }
        }
        $js_arrays = substr($js_arrays,0,-1)."];";
        $tpl->assign(array(
            "_ROOT.js_arrays"	=>	$js_arrays,
        ));
        /*if (!$CONFIG['rewrite_mod'] && !$CONFIG['multilanguage']) {
			$tpl->newBlock('block_chpu_off');
			$tpl->assign(
				array(
					"lang"		=>	$lang,
					"link"		=>	$this->module_name,
					)
			);
		}*/
        foreach ($properties as $id => $property) {
            $tpl->newBlock('block_dynamic_property');
            switch ($property["type"]) {
                case 1: // text
                    $tpl->newBlock('block_dynamic_text'.(($property["range"]) ? '_range' : ''));
                    $tpl->assign(
                        array(
                            "id"	    =>	$id,
                            "title"	    =>	$property["title"],
                            "prefix"	=>	$property["prefix"],
                            "suffix"	=>	$property["suffix"],
                            "value"		=>	str_replace(array("<", ">", "\"", "'", ".", "@", "\\"), "", $request_sdata["dynamic"][$id]),
                            "value1"	=>	str_replace(array("<", ">", "\"", "'", ".", "@", "\\"), "", $request_sdata["dynamic"][$id][1]),
                            "value2"	=>	str_replace(array("<", ">", "\"", "'", ".", "@", "\\"), "", $request_sdata["dynamic"][$id][2]),
                        )
                    );
                    break;
                case 2: // select
                    $tpl->newBlock('block_dynamic_select');
                    $tpl->assign(
                        array(
                            "id"	    =>	$id,
                            "title"	    =>	$property["title"],
                            "prefix"	=>	$property["prefix"],
                            "suffix"	=>	$property["suffix"],
                        )
                    );
                    for ($i=0; $i < sizeof($property["values"]); $i++) {
                        $tpl->newBlock('block_option');
                        $tpl->assign(
                            array(
                                "v_name"	=> $property["values"][$i],
                                "selected"	=> ($request_sdata["dynamic"][$id] == $property["values"][$i]) ? ' selected' : '',
                            ));
                    }
                    break;
            }
        }
    }

    function getPropertyFields() {
        GLOBAL $request_site_target, $request_sub_action, $request_field;
        if ('' != $request_sub_action) {
            $tpl = $this->getTmpProperties();
            switch ($request_sub_action) {
                case 'assortment':
                    $tpl->newBlock('block_properties');
                    $tpl->newBlock('block_all_ctg_list');
                    $this->showCtgrTree($this->getCategsTree(), $tpl);
                    $tpl->newBlock('block_properties_prod_cnt');
                    $tpl->assign(Array(
                        "MSG_Show_prod_cnt" => $this->_msg["Show_prod_cnt"],
                        "MSG_No" => $this->_msg["No"],
                        "MSG_Yes" => $this->_msg["Yes"],
                    ));
                    break;


                case 'assortment':
                case 'showcatmenu':
                case 'showleaders':
                case 'novelties':
                    $tpl->newBlock('block_properties');
                    $tpl->newBlock('block_properties_ctg_list');
                    $tpl->assign(Array(
                        "MSG_Show_category" => $this->_msg["Show_category"],
                        "MSG_All_categories" => $this->_msg["All_categories"],
                    ));
                    $list = $this->getAllRootCategs(true);
                    abo_str_array_crop($list);
                    $tpl->assign_array('block_properties_ctg', $list);

                    if ('showcatmenu' != $request_sub_action) {
                        $tpl->newBlock('block_properties_prods');
                        $tpl->assign(Array(
                            "MSG_Goods_per_page" => $this->_msg["Goods_per_page"],
                        ));
                    }
                    $tpl->newBlock('block_page_edit');
                    $tpl->assign(Array(
                        "MSG_Go_to_edit" => $this->_msg["Go_to_edit"],
                        "site_target" => $request_site_target,
                    ));
                    if ('showcatmenu' == $request_sub_action) {
                        $tpl->newBlock('block_properties_prod_cnt');
                        $tpl->assign(Array(
                            "MSG_Show_prod_cnt" => $this->_msg["Show_prod_cnt"],
                            "MSG_No" => $this->_msg["No"],
                            "MSG_Yes" => $this->_msg["Yes"],
                        ));
                    }
                    break;

                case "showtoplevels":
                case "showtree":
                    $tpl->newBlock('block_properties');
                    $tpl->newBlock('block_properties_ctg_list');
                    $tpl->assign(Array(
                        "MSG_Show_category" => $this->_msg["Show_category"],
                        "MSG_All_categories" => $this->_msg["All_categories"],
                    ));
                    $list = $this->getAllRootCategs(true);
                    abo_str_array_crop($list);
                    $tpl->assign_array('block_properties_ctg', $list);
                    $tpl->newBlock('block_properties_prod_cnt');
                    $tpl->assign(Array(
                        "MSG_Show_prod_cnt" => $this->_msg["Show_prod_cnt"],
                        "MSG_No" => $this->_msg["No"],
                        "MSG_Yes" => $this->_msg["Yes"],
                    ));
                    if ('showtoplevels' == $request_sub_action) {
                        $tpl->newBlock('block_properties_grouping');
                        $tpl->assign(Array(
                            "MSG_Groupping" => $this->_msg["Groupping"],
                        ));
                    }
                    break;

                case "last_viewed":
                    $tpl->newBlock('block_properties');
                    $tpl->newBlock('block_properties_prods');
                    $tpl->assign(Array(
                        "MSG_Goods_per_page" => $this->_msg["Goods_per_page"],
                    ));
                    break;

                case "showsearchform":
                    $tpl->newBlock('block_properties');
                    $tpl->newBlock('block_properties_prods');
                    $tpl->assign(Array(
                        "MSG_Goods_per_page" => $this->_msg["Goods_per_page"],
                    ));
                    $tpl->newBlock('block_properties_grp_list');
                    $tpl->assign(Array(
                        "MSG_Show_grp" => $this->_msg["Show_grp"],
                        "MSG_All_grp" => $this->_msg["All_groups"],
                    ));

                    $list = $this->get_all_groups(true);
                    abo_str_array_crop($list);

                    $tpl->assign_array('block_properties_grp', $list);
                    $tpl->newBlock('block_properties_searchform');
                    break;

                default:
                    $tpl->newBlock('block_properties_none');
                    $tpl->assign(Array(
                        "MSG_No" => $this->_msg["No"],
                    ));
                    break;
            }
            if (0 == $request_field) {
                $tpl->newBlock('block_prod_tpl');
                $tpl->assign(array(
                    "MSG_Prod_tpl" => $this->_msg["Prod_tpl"],
                    "MSG_Select" => $this->_msg["Select"],
                ));
                Module::show_select('block_prod_tpl_list', false, false, Module::get_templates(SITE_PREFIX, 'catalog_product.html'), $tpl);
            }
            return $this->getOutputContent($tpl->getOutputContent());
        }
        return FALSE;
    }

    function getEditLink($sub_action = NULL, $block_id = NULL) {
        GLOBAL $request_name, $db, $lang, $server;
        $block_id	= (int)$block_id;
        if ($sub_action && $block_id) {
            $db->query('SELECT	property1,
								property2,
								property3,
								property4,
								property5
							FROM '.$this->table_prefix.'_pages_blocks
							WHERE id = '.$block_id);
            if ($db->nf() > 0) {
                $db->next_record();
                $arr[]	= $db->f('property1');
                $arr[]	= $db->f('property2');
                $arr[]	= $db->f('property3');
                $arr[]	= $db->f('property4');
                $arr[]	= $db->f('property5');

                switch ($sub_action) {
                    case 'showcatmenu':
                    case 'showleaders':
                    case 'novelties':
                        $link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=assortment&menu=false&id=';
                        $link['material_id']	= $arr[1];
                        break;
                }
                return $link;
            }

            switch ($sub_action) {
                case 'assortment':
                case 'showcatmenu':
                case 'showleaders':
                case 'novelties':
                    $link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=assortment&menu=false&id=';
                    $link['material_id']	= '';
                    break;
            }
            return $link;
        }
        return FALSE;
    }

    /**
     * Получение корневых категорий
     */
    function getAllRootCategs($flag = false) {
        GLOBAL $CONFIG, $server, $lang, $db;
        $filter = $flag ? '' : ' clevel = 1 AND ';
        $db->query('SELECT *, if(alias is not null and alias!=\'\', alias, id) as ctg_alias FROM '.$this->table_prefix.'_catalog_categories WHERE '.$filter.' active = 1 ORDER BY '.$CONFIG['catalog_ctg_order_by']);
        if ($db->nf() > 0) {
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]['categ_id']   = $db->f('id');
                $arr[$i]['categ_name'] = $db->f('title');
                $arr[$i]['alias'] = $db->f('ctg_alias');
                $arr[$i]['desc'] = $db->f('description');
                $arr[$i]['img'] = $db->f('img');
                $arr[$i]['parent_id'] = $db->f('parent_id');
            }
            return $arr;
        }
        return FALSE;
    }

    // Получение выпадающего списка валют
    function getCurrencies($curr = '') {
        global $CONFIG;
        foreach ($CONFIG['catalog_currencies'] as $key => $val) {
            if (($curr == $CONFIG['catalog_currencies'][$key]) && ($curr != '')) {
                @$currencies .= '<option value="'.$key.'" selected>'.$CONFIG['catalog_currencies'][$key]."\n";
            } else {
                @$currencies .= '<option value="'.$key.'">'.$CONFIG['catalog_currencies'][$key]."\n";
            }
        }
        if ($currencies) {
            return $currencies;
        }
        return FALSE;
    }

    // функция для обновления курсов валют
    function set_rates($rates = array()) {
        global $CONFIG, $main, $tpl;
        $text = "";
        if (is_array($rates) && count($rates) > 0) {
            foreach ($rates as $k => $v) {
                settype($v, "double");
                $text .= "$k:$v\n";
            }
            if ($handle = fopen($CONFIG['catalog_upload_rates_file'], 'w')) {
                fwrite($handle, $text);
                fclose($handle);
                return TRUE;
            }
        }
        return FALSE;
    }

    // функция для показа строки с функциональностью добавления
    // товара или подкатегории в данную категорию
    function show_add_to_current($level, $parent, $tematik=false) {
        global $lang, $CONFIG, $tpl, $name, $baseurl;
        if (0 < $level) {
           if(!$tematik) $product_add_link = $baseurl.'&action=addproduct&id='.$parent;
            else      $product_add_link = $baseurl.'&action=addtematik&id='.$parent;
            $product_add_img  = '<img src="/'.$CONFIG["admin_img_path"].'ico_addprod.gif" alt="' . $this->_msg["Add_good"] . '" title="' . $this->_msg["Add_good"] . '" width=17 height=18 border=0 valign="absmiddle">&nbsp;' . $this->_msg["Add_good"] . '';
            $product_add	  = '<a class="add_btn" href="'.$product_add_link.'"><span>'.$product_add_img.'</span></a>';
        } else {
            $product_add = '';
        }
        $cat_add_link = $baseurl.'&action=addcat&id='.$parent;
        $cat_add_img  = '<img src="'.$CONFIG["admin_img_path"].'ico_addcat.gif" alt="' . $this->_msg["Add_category"] . '" title="' . $this->_msg["Add_category"] . '" width=17 height=18 border=0>&nbsp;' . $this->_msg["Add_category"] . '';
        $cat_add	  = '<a class="add_btn" href="'.$cat_add_link.'"><span>'.$cat_add_img.'</span></a>';
        $tpl->assign(array(
            'add_to_current'	=>	$cat_add.' '.$product_add,
        ));
        return TRUE;
    }


    // функция для добавления категории
    function add_catalog($item_rights, $block_id = 0) {
        global $request_id, $request_title, $request_group_id, $request_txt_content, $request_rank,
               $request_description, $request_keywords, $request_synonyms,$request_alias, $request_commentable, $request_products_synonyms, $server, $lang, $main, $db, $CONFIG;
        $id			 = (int)$request_id;
        $title		 = $db->escape(trim($request_title));
        $alias		 = $this->translit($request_alias);
        if(empty($alias)) $alias = null;
        $group_id	 = (int)$request_group_id;
        $description = $db->escape(trim($request_txt_content));
        $page_description = $db->escape(trim($request_description));
        $keywords	 = $db->escape(trim($request_keywords));
        $synonyms	 = $db->escape(trim($request_synonyms));
        $omain = (int)$_POST['main'];
        list($max_rank, $min_rank) = $main->get_max_min_rank($this->table_prefix.'_catalog_categories');
        $rank = $max_rank + 1;
        if (!isset($id)) $id	= 1;
        if($id == 1) {
            $db->query("SELECT id FROM " . $server . $lang . "_catalog_categories WHERE id = 1 LIMIT 1");
            if(!$db->nf()) {
                $db->query("INSERT INTO " . $server . $lang . "_catalog_categories (id, active, title, rank) VALUES ('1', '1', '" . $this->_msg["Catalogue_root"] . "', '1' )");
            }
        }
        if (!$this->isValidCtgrAlias($alias)) {
            return false;
        }
        $owner_id = $_SESSION["siteuser"]["id"];
        if ($parent_id) {
            $db->query("SELECT usr_group_id FROM ".$this->table_prefix."_catalog_categories WHERE id=".$id);
            if (!$db->next_record()) {
                return false;
            }
            list($usr_group_id, $rights) = $this->get_group_and_rights('catalog_default_rights', $block_id, $db->f("usr_group_id"));
            $rights = $item_rights ? $item_rights : $rights;
        } else {
            list($usr_group_id, $rights) = $this->get_group_and_rights('catalog_default_rights', $block_id);
        }
        $arr = array( "title" => $title, "parent_id" => $id, "main"=>$omain,"group_id" => $group_id,
            "description" => $description, "page_description" => $page_description,
            "keywords" => $keywords,"synonyms" => $synonyms, "rank" => $rank, "active" => "1",
            "owner_id" => $owner_id, "usr_group_id" => $usr_group_id, "rights" => $rights,
            "commentable" => intval($request_commentable),
            "products_synonyms" => $request_products_synonyms);
        if($alias){$arr['alias'] = $alias;}
        $CDBT = new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
        if ($id = $CDBT->insert( $id,  $arr) ) {

            $update_img_str = $this->upload_catalog_images('img', RP.$CONFIG["catalog_img_path"]."categories/", $id, 'img');
            if ($update_img_str) {
                $db->query("UPDATE ".$this->table_prefix."_catalog_categories SET title='".$title."' ".$update_img_str." WHERE id=".$id);
            }
            return $id;
        } else {
            return FALSE;
        }
    }


    // функция для редактирования категории
    function edit_catalog() {
        global $request_id, $request_title, $request_group_id, $request_txt_content, $request_rank, $request_alias,
               $request_del_img, $request_description, $request_keywords,$request_synonyms, $request_commentable, $request_products_synonyms, $request_products_synonyms, $server, $lang, $db, $CONFIG;
        $id			 = (int)$request_id;
        $rank		 = (int)$request_rank;
        $title		 = $db->escape(trim($request_title));
        $group_id	 = (int)$request_group_id;
        $omain = (int)$_POST['main'];
        $description = $db->escape(trim($request_txt_content));
        $alias       = $this->translit($request_alias);
        if (!$this->isValidCtgrAlias($alias, $id)) {
            return false;
        }
        $page_description = $db->escape(trim($request_description));
        $keywords	 = $db->escape(trim($request_keywords));
        $synonyms	 = $db->escape(trim($request_synonyms));
        $CDBT = new NsDbTree($this->table_prefix."_catalog_categories", "id", $this->CDBT_fields);
        $arr = array( "title" => $title, "group_id" => $group_id, "main"=>$omain,"description" => $description,
            "page_description" => $page_description, "keywords" => $keywords, "synonyms" => $synonyms, "commentable" => intval($request_commentable), "products_synonyms" => $request_products_synonyms);
        if(!empty($alias))  $arr["alias"] = $alias;
        $CDBT->update( $id,  $arr);

        $update_img_str = $this->upload_catalog_images('img', RP.$CONFIG["catalog_img_path"]."categories/", $id, 'img');
        if ($request_del_img || $update_img_str) {
            $db->query("SELECT img FROM ".$this->table_prefix."_catalog_categories WHERE id=".$id);
            $db->next_record();
            if ($db->f('img') && file_exists(RP.$CONFIG["catalog_img_path"]."categories/".$db->f('img'))) {
                @unlink(RP.$CONFIG["catalog_img_path"]."categories/".$db->f('img'));
                $db->query("UPDATE ".$this->table_prefix."_catalog_categories SET img = '' WHERE id = ".$id);
            }
        }

        if ($update_img_str) {
            $db->query("UPDATE ".$this->table_prefix."_catalog_categories SET title='".$title."' ".$update_img_str." WHERE id=".$id);
        }

        return;
    }


    // функция для удаления категории
    // ---------------------------------------------------------
    function delCatalog($id) {
        global $request_id, $server, $lang, $main, $db, $CONFIG;
        $CDBT				=	new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
        $id					=	(int)$id;
        if (!$id) return FALSE;
        $parent				=	$CDBT->getParent($id);
        $chieldArray		=	$CDBT->enumChildrenAll($id);

        if (empty($chieldArray)) {
            $chieldArray = array($id);
        } else {
            $chieldArray[] = $id;
        }
        // удаление картинок категорий
        $db->query("SELECT img FROM ".$this->table_prefix."_catalog_categories WHERE id IN (".implode(",", $chieldArray).") AND LENGTH(img) > 0");
        while ($db->next_record()) {
            if ($db->f('img')) {
                @unlink(RP.$CONFIG["catalog_ctg_img_path"].$db->f('img'));
            }
        }
        $this->delCtgrProducts($chieldArray);
        $CDBT->deleteAll($id);

        return $parent;
    }

    /**
     * Удаление товаров категорий
     *
     * @param int $ctgr_id
     * @return boolean
     */
    function delCtgrProducts($ctgr_ids)
    {   global $db, $CONFIG;

        $ctgr_ids = (array) $ctgr_ids;
        $ctgr_ids = array_map('intval', $ctgr_ids);
        if (empty($ctgr_ids)) {
            return FALSE;
        }

        /* Получаем список идентификаторов товаров категории */
        $prod_ids = array();
        $db->query("SELECT id FROM ".$this->table_prefix."_catalog_products WHERE category_id IN (".implode(",", $ctgr_ids).")");
        if ($db->nf() > 0) {
            while($db->next_record()) {
                $prod_ids[] = $db->f('id');
            }
        } else {
            return FALSE;
        }
        $this->delProduct($prod_ids);

        return count($prod_ids);
    }

    // функция для добавления типа товара
    function add_ptype($ptypename = '', $link = '') {
        global $CONFIG, $db, $server, $lang;
        $lang		= $db->escape(trim($lang));
        $ptypename	= $db->escape(trim($ptypename));
        $link		= $db->escape(trim($link));
        if (!@isset($ptypename)) return FALSE;
        $query = "INSERT INTO ".$this->table_prefix."_catalog_ptypes (ptype_name, link) VALUES ('".$ptypename."', '".$link."')";
        $db->query($query);
        if ($db->affected_rows() > 0) {
            $lid = $db->lid();
            return $lid;
        }
        return FALSE;
    }

    // функция для вывода информации о типе товара
    function show_ptype($id = '') {
        global $CONFIG, $main, $db, $tpl, $baseurl, $server, $lang;
        $id   = (int)$id;
        $lang = $db->escape(trim($lang));
        if (!@isset($id)) return FALSE;
        $query = 'SELECT * FROM '.$this->table_prefix.'_catalog_ptypes WHERE id = '.$id;
        $db->query($query);
        if ($db->nf() > 0) {
            $db->next_record();
            $tpl->assign(
                array(
                    ptype_id	=>	$db->f('id'),
                    ptype_name	=>	htmlspecialchars($db->f('ptype_name')),
                    link		=>	htmlspecialchars($db->f('link')),
                    ptype_link	=>	$baseurl.'&action=showptype&id='.$db->f('id'),
                )
            );
            return $ptypename;
        }
        return 0;
    }

    // функция для обновления типа товара
    function update_ptype($ptypename = '', $link = '', $id = '') {
        global $CONFIG, $db, $server, $lang;
        $id			= (int)$id;
        $ptypename	= $db->escape(trim($ptypename));
        $link		= $db->escape(trim($link));
        $lang		= $db->escape(trim($lang));
        if (!@isset($ptypename) || !@isset($id)) return FALSE;
        $query = 'UPDATE '.$this->table_prefix.'_catalog_ptypes SET
					ptype_name = "'.$ptypename.'",
					link = "'.$link.'"
					WHERE id='.$id;
        $db->query($query);
        return ($db->affected_rows() > 0) ? 1 : 0;
    }

    // функция для удаления тип товара
    function delete_ptype($id = '') {
        global $CONFIG, $db, $server, $lang;
        $id = (int)$id;
        if (!@isset($id)) return FALSE;
        if ($this->delete_entry($this->table_prefix.'_catalog_ptypes', $id)) {
            $query = 'UPDATE '.$this->table_prefix.'_catalog_products SET ptype_id = 0 WHERE ptype_id = '.$id;
            $db->query($query);
            return TRUE;
        }
        return FALSE;
    }

    // метод для добавления продукта в категори
    function add_product($category_id, $group_id, $prp_val, $ord_prp_val, $request_currency,
                         $request_price_1, $request_price_2, $request_price_3, $request_price_4,
                         $request_price_5, $ptype_id, $pcode, $pname, $info, $full_description,
                         $seo_title, $description, $keywords, $availability, $consernd, $item_rights, $block_id = 0,
                         $copy = false, $del_image = false, $del_img = false, $img_rank = array(),$img_desc = array()
    ) {
        global $CONFIG, $main, $db, $server, $lang, $request_alias;
        $pname 			  = $db->escape(abo_quote_encode(trim($pname)));
        if (!@$pname) return FALSE;
        $category_id 	  = (int)$category_id;
        $group_id 		  = (int)$group_id;
        $ptype_id 		  = (int)$ptype_id;
        $price_1		  = $request_price_1;
        $price_2		  = $request_price_2;
        $price_3		  = $request_price_3;
        $price_4		  = $request_price_4;
        $price_5		  = $request_price_5;
        $currency 		  = $db->escape(trim($request_currency));
        $lang 			  = $db->escape(trim($lang));
        $pcode 			  = $db->escape(abo_quote_encode(trim($pcode)));
        $info             = $db->escape(trim($info));
        $full_description = $db->escape(trim($full_description));
        $full_description_shop = $db->escape(trim($_POST['txt_content_shop']));
        $seo_title	  = $db->escape(trim($seo_title));
        $description	  = $db->escape(trim($description));
        $keywords		  = $db->escape(trim($keywords));
        $swf_file		  = $db->escape(trim($_POST['img']));
        $atrade		  = $db->escape(trim($_POST['atrade']));
        $bonus		  = $db->escape(trim($_POST['bonus']));
        $weight		  = $db->escape(trim($_POST['weight']));
        $file_alarm		  = $db->escape(trim($_POST['file_alarm']));
        $dimensions		  = $db->escape(trim($_POST['dimensions']));
        $products_synonyms		  = $db->escape(trim($_POST['products_synonyms']));
        $product_count	  = $db->escape(trim($_POST['product_count']));
        $nyml		  = $db->escape(trim($_POST['nyml']));
        $swf_text		  = $db->escape(trim($_POST['swf_text']));
        $availability 	  = $db->escape(substr(trim($availability), 0, 10));
        $alias            = $this->translit($request_alias);

        $date = $db->escape($_POST['date']);
        $link = $db->escape($_POST['link']);

        $owner_id = $_SESSION["siteuser"]["id"];
        $db->query("SELECT usr_group_id FROM ".$this->table_prefix."_catalog_categories WHERE id=".$category_id);
        if (!$db->next_record() || !$this->isValidProdAlias($alias)) {
            return false;
        }
        $alias            = (empty($alias)) ? 'NULL' : "'{$alias}'";

        list($usr_group_id, $rights) = $this->get_group_and_rights('catalog_default_rights', $block_id, $db->f("usr_group_id"));
        $rights = $item_rights ? $item_rights : $rights;

        // Получить значение rank (порядок следования в каталоге)
        list($max_rank, $min_rank) = $main->get_max_min_rank($this->table_prefix.'_catalog_products');
        $rank = $max_rank + 1;
        $db->query("INSERT INTO ".$this->table_prefix."_catalog_products ("
            ."category_id, group_id, ptype_id, product_code, product_title,"
            ."alias, product_inf, product_description, product_description_shop, currency, price_1,"
            ."price_2, price_3, price_4, price_5, product_availability,dates,link,swf_file,swf_text,atrade,bonus,weight,file_alarm,dimensions,products_synonyms,product_count,nyml, "
            ."seo_title, description, keywords, rank, owner_id, usr_group_id, rights) VALUES ("
            .$category_id.", ".$group_id.", ".$ptype_id.", '".$pcode."', '".$pname."',"
            .$alias.", '".$info."', '".$full_description."', '".$full_description_shop."','".$currency."', '".$price_1."', '"
            .$price_2."', '".$price_3."', '".$price_4."', '".$price_5."', '".$availability."', '".$date."', '".$link."', '$swf_file', '$swf_text', '$atrade','$bonus', '$weight', '$file_alarm', '$dimensions','$products_synonyms', '$product_count', '$nyml','"
            .$seo_title."', '".$description."', '".$keywords."', '".$rank."', ".$owner_id.", ".$usr_group_id.", ".$rights
            .")");

        if ($db->affected_rows() > 0) {
            $lid = $db->lid();

            $this->setProdPropVal($lid, $prp_val, $group_id);
            $this->setProdOrderPropVal($lid, $ord_prp_val);

            $this->check_path($CONFIG["catalog_img_path"]);
            // закачаем картинки
            if (!$del_image) {
                $update_img_str = $this->upload_catalog_images('image_big', RP.$CONFIG["catalog_img_path"], $lid, 'image_big', 'big');
                if ($update_img_str) {
                    $_REQUEST['image_big_name'] = substr($update_img_str,15,-1);
                } else {
                    if ($_REQUEST['image_big_name'] && ! $_REQUEST['del_image_big']
                        && file_exists(RP.$CONFIG["catalog_img_path"].$_REQUEST['image_big_name'])
                    ) {
                        $ext = CatalogPrototype::getFileExt($_REQUEST['image_big_name']);
                        if ($ext) {
                            do {
                                $file_name = md5(uniqid(rand(), true)).".{$ext}";
                            } while (file_exists(RP.$CONFIG["catalog_img_path"].$file_name));
                            if (copy(RP.$CONFIG["catalog_img_path"].$_REQUEST['image_big_name'], RP.$CONFIG["catalog_img_path"].$file_name)) {
                                $update_img_str = ", image_big='{$file_name}'";
                                $_REQUEST['image_big_name'] = $file_name;
                            }
                        }
                    }
                }
                $update_middle_str = $this->upload_catalog_images(
                    'image_middle',
                    RP.$CONFIG["catalog_img_path"],
                    $lid,
                    'image_middle',
                    'image',
                    array('width' => $CONFIG['catalog_max_prev_width'], 'height' => $CONFIG['catalog_max_prev_height']));
                if (!$update_middle_str && $_REQUEST['image_middle_name'] && !$_REQUEST['del_image_middle']
                    && file_exists(RP.$CONFIG["catalog_img_path"].$_REQUEST['image_middle_name'])
                ) {
                    $ext = CatalogPrototype::getFileExt($_REQUEST['image_middle_name']);
                    if ($ext) {
                        do {
                            $file_name = md5(uniqid(rand(), true)).".{$ext}";
                        } while (file_exists(RP.$CONFIG["catalog_img_path"].$file_name));
                        if (copy(RP.$CONFIG["catalog_img_path"].$_REQUEST['image_middle_name'], RP.$CONFIG["catalog_img_path"].$file_name)) {
                            $update_middle_str = ", image_middle='{$file_name}'";
                        }
                    }
                }
            }
            $db->query("UPDATE ".$this->table_prefix."_catalog_products SET product_title='".$pname."' ".$update_img_str.$update_middle_str." WHERE id=".$lid);
            $this->addAdditionalImgs($lid, $copy, $del_img, $img_rank,$img_desc);

            $this->addConcernedProducts(0, $lid, $consernd[0]);
            $this->addConcernedProducts(1, $lid, $consernd[1]);

            return $lid;
        }
    }


    // функция для обновления товара в базе данных
    function update_product($id,
                            $group_id,
                            $prp_val,
                            $ord_prp_val,
                            $prp_curr,
                            $price_1,
                            $price_2,
                            $price_3,
                            $price_4,
                            $price_5,
                            $ptype_id,
                            $pcode,
                            $pname,
                            $info,
                            $full_description,
                            $seo_title,
                            $description,
                            $keywords,
                            $availability,
                            $del_image,
                            $del_pic,
                            $del_img,
                            $img_rank,
                            $update_img_rank,
                            $consernd,
                                $img_desc,
                    $desc) {
        global $CONFIG, $db, $server, $lang, $request_alias;
        $id 				= ($id) ? (int)$id : 0;
        $group_id			= (int)$group_id;
        $ptype_id			= (int)$ptype_id;
        $price_1			= $price_1;
        $price_2			= $price_2;
        $price_3			= $price_3;
        $price_4			= $price_4;
        $price_5			= $price_5;
        $prp_curr			= $db->escape(trim($prp_curr));
        $lang				= $db->escape(trim($lang));
        $pcode				= $db->escape(abo_quote_encode(trim($pcode)));
        $pname				= $db->escape(abo_quote_encode(trim($pname)));
        $short_description	= $db->escape(trim($short_description));
        $info               = $db->escape(trim($info));
        $full_description	= $db->escape(trim($full_description));
        $seo_title		= $db->escape(trim($seo_title));
        $description		= $db->escape(trim($description));
        $keywords			= $db->escape(trim($keywords));
        $availability		= $db->escape(trim($availability));
        $desc               = $db->escape(trim($desc));
        $swf_file		  = $db->escape(trim($_POST['img']));
        $swf_text		  = $db->escape(trim($_POST['swf_text']));
        $alias            = $this->translit($request_alias);
        $link = $db->escape($_POST['link']);
        if (!@$id || !@$pname || !$this->isValidProdAlias($alias, $id)) {
            return FALSE;
        }
        $nyml		  = $db->escape(trim($_POST['nyml']));
        $full_description_shop = $db->escape(trim($_POST['txt_content_shop']));
        $alias            = (empty($alias)) ? 'NULL' : "'{$alias}'";
        $date = $db->escape($_POST['date']);
        $atrade		  = $db->escape(trim($_POST['atrade']));
        $bonus		  = $db->escape(trim($_POST['bonus']));
        $weight		  = $db->escape(trim($_POST['weight']));
        $file_alarm		  = $db->escape(trim($_POST['file_alarm']));
        $dimensions		  = $db->escape(trim($_POST['dimensions']));
        $products_synonyms	  = $db->escape(trim($_POST['products_synonyms']));
        $product_count		  = $db->escape(trim($_POST['product_count']));
        $query = "UPDATE ".$this->table_prefix."_catalog_products
					SET group_id = '$group_id',
						ptype_id = '$ptype_id',
						currency = '$prp_curr',
						price_1	 = '$price_1',
						price_2	 = '$price_2',
						price_3	 = '$price_3',
						price_4	 = '$price_4',
						nyml = '$nyml',
						price_5	 = '$price_5',
						product_code = '$pcode',
						product_title = '$pname',
						dates = '$date',
						link = '$link',
                        alias = $alias,
						atrade='$atrade',
						bonus='$bonus',
						weight='$weight',
						file_alarm='$file_alarm',
						dimensions='$dimensions',
						products_synonyms='$products_synonyms',
						product_count='$product_count',
						swf_file = '$swf_file',
						swf_text = '$swf_text',
                        product_inf = '$info',
						product_description = '$full_description',
						product_description_shop = '$full_description_shop',
						product_availability = '$availability',
						seo_title	= '$seo_title',
						description	= '$description',
						keywords = '$keywords',
						img_desc = '$desc'
					WHERE id = ".$id;
        //var_dump($query);
        $db->query($query);

        $this->setProdPropVal($id, $prp_val, $group_id);
        $this->setProdOrderPropVal($id, $ord_prp_val);

        $update_img_str = $update_big_img_str = '';

        $_REQUEST['image_big_name'] = '';
        $update_big_img_str = $this->upload_catalog_images('image_big', RP.$CONFIG["catalog_img_path"], $id, 'image_big', 'big');
        if ($update_big_img_str) {
            $_REQUEST['image_big_name'] = substr($update_big_img_str,15,-1);
        }
        $update_img_str = $this->upload_catalog_images(
            'image_middle',
            RP.$CONFIG["catalog_img_path"],
            $id,
            'image_middle',
            'image',
            array('width' => $CONFIG['catalog_max_prev_width'], 'height' => $CONFIG['catalog_max_prev_height'])
        );
        /*		    
		$update_img_str = $this->upload_catalog_images(
		    'image_middle',
		    RP.$CONFIG["catalog_img_path"],
		    $id);
		*/

        if ($del_image || $update_img_str || $update_big_img_str) {
            $db->query("SELECT image_middle, image_big FROM ".$this->table_prefix."_catalog_products WHERE id=".$id);
            $db->next_record();
            $im = $db->f('image_middle');
            $ib = $db->f('image_big');
            if ($im && ($del_image || $update_img_str)) {
                @unlink(RP.$CONFIG["catalog_img_path"].$im);
                $update_img_str = $update_img_str ? $update_img_str : ", image_middle = ''";
            }
            if ($ib && ($del_image || $update_big_img_str)) {
                @unlink(RP.$CONFIG["catalog_img_path"].$ib);
                $update_big_img_str = $update_big_img_str ? $update_big_img_str : ", image_big = ''";
            }
        }

        if ($update_img_str || $update_big_img_str) {
            $db->query("UPDATE ".$this->table_prefix."_catalog_products SET product_title='".$pname."' ".$update_img_str.$update_big_img_str." WHERE id=".$id);
        }

        if (array($update_img_rank) && !empty($update_img_rank)) {
            foreach ($update_img_rank as $key => $val) {
                $db->set("{$this->table_prefix}_catalog_img", array('rank' => intval($val)), array('id' => intval($key)));
            }
        }


        if (array($img_desc) && !empty($img_desc)) {
            foreach ($img_desc as $key => $val) {
                $db->set("{$this->table_prefix}_catalog_img", array('desc' => $val), array('id' => intval($key)));
            }
        }

        $this->delAdditionalImgs($del_img);
        $this->addAdditionalImgs($id, false, false, $img_rank,$img_desc);

        $this->delAllConcernedProducts(0, $id);
        $this->delAllConcernedProducts(1, $id);
        $this->addConcernedProducts(0, $id, $consernd[0]);
        $this->addConcernedProducts(1, $id, $consernd[1]);

        return TRUE;
    }

    function setProdPropVal($prod_id, $prp_val, $group_id)
    {   global $db;

        $prod_id = (int)$prod_id;
        $db->query("DELETE FROM {$this->table_prefix}_catalog_property_values WHERE product_id={$prod_id}");
        if (is_array($prp_val)) {
            $ins = array();
            foreach($prp_val as $key => $value) {
                if ($key && '' !== $value) {
                    $value = $db->escape($value);
                    $ins[] = "({$prod_id}, {$key}, '{$value}', {$group_id})";
                }
            }
            if (! empty($ins)) {
                $db->query("INSERT INTO {$this->table_prefix}_catalog_property_values"
                    ." (product_id, property_id, value, group_id) VALUES ".implode(", ", $ins));
            }
        }
    }

    function setProdOrderPropVal($prod_id, $prp_val)
    {   global $db;

        $prod_id = (int)$prod_id;
        $db->query("DELETE FROM {$this->table_prefix}_catalog_order_prop_val WHERE prod_id={$prod_id}");
        if (is_array($prp_val)) {
            $ins = array();
            foreach($prp_val as $key => $value) {
                if ($key && $value) {
                    $value = $db->escape($value);
                    $ins[] = "({$prod_id}, {$key})";
                }
            }
            $db->query("INSERT INTO {$this->table_prefix}_catalog_order_prop_val"
                ." (prod_id, prop_val_id) VALUES ".implode(", ", $ins));
        }
    }
    /**
     * Получить id зничений свойств заказа для заданного товара
     *
     * @param int $prod_id
     * @return mixed
     */
    function getProdOrderPropValIds($prod_id)
    {   global $db;
        $ret = false;
        $db->query("SELECT prop_val_id FROM {$this->table_prefix}_catalog_order_prop_val WHERE prod_id=".intval($prod_id));
        while ($db->next_record()) {
            $ret[] = $db->Record['prop_val_id'];
        }
        return $ret;
    }
    /**
     * Enter description here...
     *
     * @param unknown_type $prod_list
     */
    function setPropValForProdList(&$prod_list)
    {   global $db;

        $ids = array();
        foreach ($prod_list as $key => $val) {
            if ($val['prod_id']) {
                $ids[$val['prod_id']] = $key;
            }
        }
        if (! empty($ids)) {
            $db->query("SELECT prod_id, prop_val_id FROM {$this->table_prefix}_catalog_order_prop_val WHERE prod_id IN ("
                .implode(",", array_keys($ids)).")");
            while ($db->next_record()) {
                $prod_list[$ids[$db->Record['prod_id']]]['ord_prop_val'][] = $db->Record['prop_val_id'];
            }
        }
    }

    // Удаления товара из базы данных
    function delProduct($ids)
    {   global $CONFIG, $db;

        $ret = false;

        $ids = array_map('intval', (array) $ids);
        if (! empty($ids)) {
            $ids_str = implode(", ", $ids);
            $db->query("SELECT image_middle, image_big FROM {$this->table_prefix}_catalog_products WHERE id IN ({$ids_str})");
            if ($db->nf()) {
                while ($db->next_record()) {
                    if ($db->Record['image_middle'] && file_exists(RP.$CONFIG['catalog_img_path'].$db->Record['image_middle'])) {
                        @unlink(RP.$CONFIG['catalog_img_path'].$db->Record['image_middle']);
                    }
                    if ($db->Record['image_big'] && file_exists(RP.$CONFIG['catalog_img_path'].$db->Record['image_big'])) {
                        @unlink(RP.$CONFIG['catalog_img_path'].$db->Record['image_big']);
                    }
                }
                $db->query("DELETE FROM {$this->table_prefix}_catalog_property_values WHERE product_id IN ({$ids_str})");
                // Удаление связанных товаров
                $db->query("DELETE FROM {$this->table_prefix}_catalog_concerned_products WHERE prod_id IN ({$ids_str})");
                // Удаление значений свойств заказа
                $db->query("DELETE FROM {$this->table_prefix}_catalog_order_prop_val WHERE prod_id IN ({$ids_str})");
                // удаление дополнительных изображений товара
                $this->delProdAdditionalImgs($ids);

                $db->query("DELETE FROM {$this->table_prefix}_catalog_products WHERE id IN ({$ids_str})");
            }
        }

        return TRUE;
    }

    // функция для загрузки картинок для товара
    function upload_catalog_images($img, $path, $id, $field_name='image_middle', $img_type = 'image', $img_size = false)
    {   global $CONFIG, $main, $db;

        $id    = (int)$id;
        $lang = $db->escape(trim($lang));
        $path = $db->escape(trim($path));
        if (!@$id || !@$path) {
            return FALSE;
        }
        $id = str_pad($id, 6, "0", STR_PAD_LEFT);
        $imagenames_array	= array();
        $update_img_str		= '';
        if($field_name=='image_middle' && @is_file(RP.$CONFIG['catalog_img_path'].$_REQUEST['image_big_name'])
                && !($_FILES['image_middle']['tmp_name']) && !$_REQUEST['del_image']
        ) {
            $image_big = $_REQUEST['image_big_name'];
            $image = '';//$_REQUEST['image_middle_name'];
            if ($this->check_image($image_big, $image)) {
                return  ", image_middle = '".$image."'";
            } else {
                return false;
            }
        }
        $w = $img_size && $img_size['width'] > 0 ? (int)$img_size['width'] : $CONFIG['catalog_max_img_width'];
        $h = $img_size && $img_size['height'] > 0 ? (int)$img_size['height'] : $CONFIG['catalog_max_img_height'];
        if (is_uploaded_file($_FILES[$img]['tmp_name'])) {
            $set_file_name = $id.'_'.$img_type.'_'.$_FILES[$img]['name'];
            if (Main::generateImage(
                $_FILES[$img]['tmp_name'],
                $path.$set_file_name,
                array('width' => $w, 'height' => $h)
            )) {
                $update_img_str .= ", ".$field_name." = '".$set_file_name."'";
            }
        }
        /*
		foreach ($_FILES[$img] as $field => $arr) {
// загрузить картинку
			if ($arr && ($field == 'name')) {
				$set_file_name = $id.'_'.$img_type.'_'.$arr;
				$imagename = $main->upload_file($img,
												$CONFIG['files_img_max_size'],
												$path,
												'',
												$this->catalog_image_formats,
												$set_file_name);
				if ($imagename) {
// подогнать по ширине
					$update_img_str .= ", ".$field_name." = '".$imagename."'";
				}
			}
		}
		*/

        return $update_img_str;
    }

    // функция для удаления записи из базы данных
    function delete_entry($table = "", $id = 0) {
        global $CONFIG, $db, $lang;
        $lang  = $db->escape(trim($lang));
        $table = $db->escape(trim($table));
        $id    = (int)$id;
        if (!@$id || !@$table) return FALSE;
        $query = 'DELETE FROM '.$table.' WHERE id='.$id;
        $db->query($query);
        return ($db->affected_rows() > 0) ? TRUE : FALSE;
    }


    /**
     * Метод для показа строки с функциональностью добавления
     * группы
     */
    function get_add_group() {
        global $lang, $CONFIG, $tpl, $name, $baseurl;
        $grp_add_link	= $baseurl.'&action=addgroup';
        $grp_add_img	= '<img src="'.$CONFIG["admin_img_path"].'ico_addcat.gif" alt="' . $this->_msg["Add_group"] . '" title="' . $this->_msg["Add_group"] . '" width=17 height=18 border=0>&nbsp;' . $this->_msg["Add_group"] . '</a>';
        $grp_add_2		= '<a class="add_btn" href="'.$grp_add_link.'"><span>'.$grp_add_img.'</span></a>';
        $grp_add		= '';//'<a href="'.$grp_add_link.'">' . $this->_msg["Add_group"] . '</a>';
        return array('path' => $grp_add, 'add_to_current' => $grp_add_2);
    }

    /**
     *
     * Получение массива групп
     *
     */
    function get_groups($start = 0, $limit = 0) {
        global $baseurl, $db, $server, $lang, $CONFIG;
        $lang  = $db->escape(trim($lang));
        $start = (int)$start;
        $limit = (int)$limit;
        if (!@isset($start) || !@isset($limit)) return FALSE;
        if ($start > 0) $start = $start-1;
        $db->query('SELECT *
						FROM '.$this->table_prefix.'_catalog_groups
						ORDER BY '.$CONFIG['catalog_group_order_by'].'
						LIMIT '.($start*$limit).', '.$limit);
        if ($db->nf() > 0) {
            for ($i=0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]['group_id']   		= $db->f('id');
                $arr[$i]['group_name'] 		= $db->f('group_title');
                $arr[$i]['grp_link']   		= $baseurl.'&action=addgrprty&start='.$start.'&id='.$db->f('id');
                $arr[$i]['grp_edit_link']	= $baseurl.'&action=editgroup&start='.$start.'&id='.$db->f('id');
                $arr[$i]['grp_del_link']	= $baseurl.'&action=delgroup&start='.$start.'&id='.$db->f('id');
                $arr[$i]['grp_add_prp']		= $baseurl.'&action=addgrprty&start='.$start.'&id='.$db->f('id');
            }
            return $arr;
        }
        return FALSE;
    }

    /**
     *
     * Получение массива групп
     *
     */
    function get_all_groups($group_id = '') {
        global $baseurl, $db, $server, $lang, $CONFIG;
        $lang 	  = $db->escape(trim($lang));
        $group_id = (int)$group_id;
        if (!@isset($group_id)) return FALSE;
        $query = 'SELECT * FROM '.$this->table_prefix.'_catalog_groups ORDER BY '.$CONFIG['catalog_group_order_by'];
        $db->query($query);
        if ($db->nf() > 0) {
            for ($i=0; $i < $db->nf(); $i++) {
                $db->next_record();
                if ($db->f('id') > 0 && $db->f('id') == $group_id) {
                    $selected = ' selected';
                } else {
                    $selected = '';
                }
                $arr[$i]['group_id']   		= $db->f('id');
                $arr[$i]['group_name'] 		= $db->f('group_title');
                $arr[$i]['group_selected'] 	= $selected;
            }
            return $arr;
        }
        return FALSE;
    }

    /**
     *
     * Получение массива свойств группы для js
     *
     */
    function array_types_group($group_id = '') {
        global $baseurl, $db, $server, $lang;
        $lang 	  = $db->escape(trim($lang));
        $group_id = (int)$group_id;
        if (!@$group_id) return FALSE;
        $query = 'SELECT * FROM '.$this->table_prefix.'_catalog_properties WHERE group_id='.$group_id;
        $db->query($query);
        if ($db->nf() > 0) {
            for ($i=0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i] = $db->f('property_id');
            }
            $query = 'SELECT * FROM '.$this->table_prefix.'_catalog_properties WHERE group_id='.$group_id;
            $db->query($query);
        }
        return FALSE;
    }


    /**
     *
     * Получение идентификатора группы у категории
     *
     */
    function get_category_group_id($category_id) {
        global $db, $server, $lang;
        $category_id = (int)$category_id;
        $lang = $db->escape(trim($lang));
        if (!@$category_id) return 0;
        $query = 'SELECT group_id FROM '.$this->table_prefix.'_catalog_categories WHERE id='.$category_id;
        $db->query($query);
        if ($db->nf() > 0) {
            $db->next_record();
            return intval($db->f('group_id'));
        }
        return 0;
    }

    /**
     *
     * Получение массива группы
     *
     */
    function get_group($id)
    {
        global $db;

        $id = (int)$id;
        if (!$id) return FALSE;
        $db->query('SELECT * FROM '.$this->table_prefix.'_catalog_groups WHERE id='.$id);
        if ($db->nf() > 0) {
            $db->next_record();
            return array (
                'group_id' => htmlspecialchars($db->f('id')),
                'group_name' => htmlspecialchars($db->f('group_title'))
            );
        }
        return FALSE;
    }


    /**
     *
     * Добавить группу
     *
     */
    function add_group($group_name) {
        global $db, $server, $lang;
        $lang 		= $db->escape(trim($lang));
        $group_name = $db->escape(trim($group_name));
        if (!@$group_name) return FALSE;
        $query = 'INSERT INTO '.$this->table_prefix.'_catalog_groups (group_title) VALUES ("'.$group_name.'")';
        $db->query($query);
        if ($db->affected_rows() > 0) {
            $lid = $db->lid();
            return $lid;
        }
        return FALSE;
    }

    /**
     *
     * пзменить группу
     *
     */
    function upd_group($id, $group_name) {
        global $db, $server, $lang;
        $id 		= (int)$id;
        $group_name = $db->escape(trim($group_name));
        $lang 		= $db->escape(trim($lang));
        if (!@$group_name || !@$id) return FALSE;
        $query = 'UPDATE '.$this->table_prefix.'_catalog_groups  SET group_title="'.$group_name.'" WHERE id='.$id;
        $db->query($query);
        if ($db->affected_rows() > 0) {
            $lid = $db->lid();
            return $lid;
        }
        return FALSE;
    }


    /**
     *
     * Удаление группы
     *
     */
    function del_group($id, $del) {
        global $baseurl, $db, $server, $lang;
        $id = (int)$id;
        if (!@$id) return FALSE;
        $lang = $db->escape(trim($lang));

        // Установить group_id у товаров равным 0
        $query = 'UPDATE '.$this->table_prefix.'_catalog_products SET group_id=0 WHERE group_id='.$id;
        $db->query($query);

        // Удаление значений всех свойств группы
        $query = 'DELETE FROM '.$this->table_prefix.'_catalog_property_values WHERE group_id='.$id;
        $db->query($query);
        if ($del != '') {
            // Проверка принадлежать ли свойства этой группы ещё какой либо группе и
            // в случае не принадлежности свойства удалаются
            $query = 'SELECT * FROM '.$this->table_prefix.'_catalog_properties WHERE group_id='.$id;
            $db->query($query);
            if ($db->nf() > 0) {
                for ($i=0; $i < $db->nf(); $i++) {
                    $db->next_record();
                    $prp_grp .= $db->f('property_id');
                    if (($i+1) < $db->nf()) {$prp_grp .= ', ';}
                }
                // список свойств принадлежащих не только текущей группе
                $query = 'SELECT DISTINCT property_id FROM '.$this->table_prefix.'_catalog_properties
							WHERE property_id IN ('.$prp_grp.') AND group_id!='.$id;
                $db->query($query);
                if ($db->nf() > 0) {
                    for ($i=0; $i < $db->nf(); $i++) {
                        $db->next_record();
                        $prp_grp_2 .= $db->f('property_id');
                        if (($i+1) < $db->nf()) {$prp_grp_2 .= ', ';}
                    }
                    $query = 'SELECT * FROM '.$this->table_prefix.'_catalog_properties WHERE property_id NOT IN ('.$prp_grp_2.') AND group_id='.$id;
                    $db->query($query);
                    if ($db->nf() > 0) {
                        for ($i=0; $i < $db->nf(); $i++) {
                            $db->next_record();
                            $prp_grp_3 .= $db->f('property_id');
                            if (($i+1) < $db->nf()) {$prp_grp_3 .= ', ';}
                        }
                        // Удаление привязки группы к свойствам
                        $query = 'DELETE FROM '.$this->table_prefix.'_catalog_properties WHERE group_id='.$id;
                        $db->query($query);
                        // Удаление свойст группы не принадлежащих другим группам
                        $query = 'DELETE FROM '.$this->table_prefix.'_catalog_properties_table WHERE id IN ('.$prp_grp_3.')';
                        $db->query($query);
                        // Удаление значений по умолчанию свойст группы не принадлежащих другим группам
                        $query = 'DELETE FROM '.$this->table_prefix.'_catalog_properties_def_values WHERE property_id IN ('.$prp_grp_3.')';
                        $db->query($query);
                    }
                } else {
                    $query = 'SELECT DISTINCT property_id FROM '.$this->table_prefix.'_catalog_properties
							    WHERE property_id IN ('.$prp_grp.') AND group_id='.$id;
                    $db->query($query);
                    if ($db->nf() > 0) {
                        for ($i=0; $i < $db->nf(); $i++) {
                            $db->next_record();
                            $prp_grp_2 .= $db->f('property_id');
                            if (($i+1) < $db->nf()) {$prp_grp_2 .= ', ';}
                        }
                        // Удаление привязки группы к свойствам
                        $query = 'DELETE FROM '.$this->table_prefix.'_catalog_properties WHERE group_id='.$id;
                        $db->query($query);
                        // Удаление свойст группы не принадлежащих другим группам
                        $query = 'DELETE FROM '.$this->table_prefix.'_catalog_properties_table WHERE id IN ('.$prp_grp_2.')';
                        $db->query($query);
                        // Удаление значений по умолчанию свойст группы не принадлежащих другим группам
                        $query = 'DELETE FROM '.$this->table_prefix.'_catalog_properties_def_values WHERE property_id IN ('.$prp_grp_2.')';
                        $db->query($query);
                    }
                }
            }
        } else {
            $query = 'DELETE FROM '.$this->table_prefix.'_catalog_properties WHERE group_id='.$id;
            $db->query($query);
        }
        $query = 'DELETE FROM '.$this->table_prefix.'_catalog_groups WHERE id='.$id;
        $db->query($query);
        if ($db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Получение массива свойств
     */
    function get_propertis($start = 0, $limit = 0, $filter)
    {   global $baseurl, $db, $server, $lang;

        $order_list = array(
            'name' => 'property_name',
            '-name' => 'property_name DESC',
            'title' => 'property_title',
            '-title' => 'property_title DESC',
            'id' => 'id',
            '-id' => 'id DESC'
        );
        $order = 'id';
        if ($filter['order'] && isset($order_list[$filter['order']])) {
            $order = $order_list[$filter['order']];
        }
        $lang  = $db->escape(trim($lang));
        $start = (int)$start;
        $limit = (int)$limit;
        if (!@$start || !@$limit) return FALSE;
        if ($start > 0) {$start = $start-1;}

        $clause = $this->get_propertis_clause($filter);

        $query = "SELECT * FROM {$this->table_prefix}_catalog_properties_table"
            .($clause ? " WHERE ".$clause : '')
            ." ORDER BY {$order} LIMIT ".($start*$limit).", ".$limit;
        $db->query($query);

        if ($db->nf() > 0) {
            $ids = array();
            for ($i = 0; $i < $db->nf(); $i++) {
                $db->next_record();
                $arr[$i]['id']   		    = $db->f('id');
                $arr[$i]['prp_id']   		= '&nbsp;';
                $arr[$i]['prp_prefix'] 		= $db->f('property_prefix') ? $db->f('property_prefix') : '&nbsp;';
                $arr[$i]['prp_title'] 		= $db->f('property_title');
                $arr[$i]['prp_name'] 		= $db->f('property_name');
                $arr[$i]['prp_suffix'] 		= $db->f('property_suffix') ? $db->f('property_suffix') : '&nbsp;';
                $arr[$i]['prp_def_val']		= '&nbsp;';
                $arr[$i]['prp_link']   		= $baseurl.'&action=property&id='.$db->f('id');
                $arr[$i]['prp_edit_link']	= $baseurl.'&action=editprty&id='.$db->f('id');
                $arr[$i]['prp_del_link']	= $baseurl.'&action=delprty&id='.$db->f('id');
                $arr[$i]['prp_add_prp']		= $baseurl.'&action=addprty&id='.$db->f('id');
                $ids[] = $arr[$i]['id'];
            }

            $ids = implode(',', $ids);

            if (strlen($ids) > 0) {
                $query = 'SELECT *
                            FROM '.$this->table_prefix.'_catalog_properties_def_values
                            WHERE property_id IN ('.$ids.')
                            ORDER BY property_id DESC, id, value';
                $db->query($query);
                if ($db->nf() > 0) {
                    for($i = 0; $i < $db->nf(); $i++) {
                        $db->next_record();
                        $prt_val[$i]['id']          = $db->f('id');
                        $prt_val[$i]['property_id'] = $db->f('property_id');
                        $prt_val[$i]['value']       = $db->f('value');
                    }

                    for ($i = 0; $i < sizeof($prt_val); $i++) {
                        for ($j = 0; $j < sizeof($arr); $j++) {
                            if ($arr[$j]['id'] == $prt_val[$i]['property_id']) {
                                $arr[$j]['prp_id']      = $prt_val[$i]['property_id'];
                                if ($arr[$j]['prp_def_val'] != '&nbsp;') {
                                    $arr[$j]['prp_def_val']	= $arr[$j]['prp_def_val'].$prt_val[$i]['value'].'<br>';
                                } else {
                                    $arr[$j]['prp_def_val']	= $prt_val[$i]['value'].'<br>';
                                }
                            }
                        }
                    }
                }
            }
            return $arr;
        }
        return FALSE;
    }

    function get_propertis_cnt($filter)
    {   global $db;

        $clause = $this->get_propertis_clause($filter);
        $db->query("SELECT COUNT(*) AS cnt FROM {$this->table_prefix}_catalog_properties_table"
            .($clause ? " WHERE ".$clause : '')
        );

        return $db->next_record() ? $db->Record['cnt'] : '';
    }

    function get_propertis_clause($filter)
    {
        $ret = array();
        if ($filter['c']) {
            $c = My_Sql::escape(str_replace(
                array('%', '_'),
                array('\%', '\_'),
                $filter['c']
            ));
            $ret[] = '(property_title LIKE "%'.$c.'%" OR property_name LIKE "%'.$c.'%")';
        }

        return empty($c) ? false : implode(' ANS ', $ret);
    }

    /**
     * Получить возможные значения свойства товара
     *
     * @param int $id
     * @return mixed array or false
     */
    function getPropValList($id)
    {   global $db;
        $db->query("SELECT id, value FROM {$this->table_prefix}_catalog_properties_def_values WHERE property_id=".intval($id)." ORDER BY id");
        $ret = false;
        while ($db->next_record()) {
            $ret[$db->Record['id']] = $db->Record['value'];
        }

        return $ret;
    }
    /**
     * Получить параметры свойства
     *
     * @param int $id
     * @return mixed array or false
     */
    function getProp($id)
    {   global $db;
        $id = (int) $id;
        $ret = false;
        $db->query("SELECT * FROM {$this->table_prefix}_catalog_properties_table WHERE id={$id}");
        if ($db->next_record()) {
            $ret = array(
                'id' => $db->Record['id'],
                'title' => $db->Record['property_title'],
                'name' => $db->Record['property_name'],
                'prefix' => $db->Record['property_prefix'],
                'suffix' => $db->Record['property_suffix'],
                'type' => $db->Record['type']
            );
            $val = $this->getPropValList($id);
            $ret['val'] = 1 == $ret['type'] ? reset($val) : $val;
        }

        return $ret;
    }

    function showProp($id)
    {   global $tpl, $CONFIG;
        if ( ($prop = $this->getProp($id)) ) {
            $tpl->gotoBlock('_ROOT');
            $tpl->assign(array(
                'prt_id' => htmlspecialchars($prop['id']),
                'prt_title' => htmlspecialchars($prop['title']),
                'prt_name' => htmlspecialchars($prop['name']),
                'prefix' => htmlspecialchars($prop['prefix']),
                'suffix' => htmlspecialchars($prop['suffix']),
                'disabled_type' => 'disabled'
            ));
            switch ($prop['type']) {
                case 1:
                    $tpl->assign(array(
                        'display_def_val' => 'block',
                        'display_def_val_list' => 'none',
                        'display_type_add'	=> 'block',
                        'def_value' => htmlspecialchars($prop['val']),
                        'display_pref' => 'block',
                        'display_suff' => 'block'));
                    break;
                case 2:
                    $tpl->assign(array(
                        'display_def_val' => 'none',
                        'display_def_val_list' => 'block',
                        'display_type_add'	=> 'block',
                        'def_value' => htmlspecialchars($prop['val']),
                        'display_pref' => 'block',
                        'display_suff' => 'block'));
                    break;
                case 3:
                    $tpl->assign(array(
                        'display_def_val' => 'none',
                        'display_def_val_list' => 'block',
                        'display_type_add'	=> 'block',
                        'display_pref' => 'block',
                        'display_suff' => 'block'));
                    break;
                case 4:
                    $tpl->assign(array(
                        'display_def_val' => 'none',
                        'display_def_val_list' => 'none',
                        'display_type_add'	=> 'block',
                        'def_value' => '',
                        'display_pref' => 'none',
                        'display_suff' => 'none'));
                    break;
                default:
                    $tpl->assign(array(
                        'display_def_val' => 'none',
                        'display_def_val_list' => 'none',
                        'display_type_add'	=> 'none'));
                    break;
            }
            $tpl->assign_array('block_property_type_select', Main::getSelectTag($CONFIG['catalog_dynamic_property_type'], $prop['type']));

            if (is_array($prop['val']) && sizeof($prop['val']) > 0) {
                foreach ($prop['val'] as $key => $val) {
                    $tpl->newBlock('block_properies');
                    $tpl->assign(array(
                        'id' => $key,
                        'def_value' => $val));
                }
            }
        }
    }

    function getOrderPropList($group_id = 0)
    {   global $db;
        $group_id = (int)$group_id;
        if (! isset($this->cache['order_prop_list'][$group_id])) {
            $this->cache['order_prop_list'][$group_id] = false;
            if ($group_id) {
                $db->query("SELECT PT.* FROM {$this->table_prefix}_catalog_properties_table PT"
                    ." INNER JOIN {$this->table_prefix}_catalog_properties P ON PT.id=P.property_id "
                    ."WHERE P.group_id={$group_id} AND 3=PT.type ORDER BY P.ord");
            } else {
                $db->query("SELECT * FROM {$this->table_prefix}_catalog_properties_table WHERE 3=type ORDER BY id DESC");
            }
            while ($db->next_record()) {
                $this->cache['order_prop_list'][$group_id][$db->Record['id']] = array(
                    'id' => $db->Record['id'],
                    'title' => $db->Record['property_title'],
                    'name' => $db->Record['property_name'],
                    'prefix' => $db->Record['property_prefix'],
                    'suffix' => $db->Record['property_suffix'],
                    'type' => $db->Record['type']
                );
            }
            if ($this->cache['order_prop_list'][$group_id]) {
                $db->query("SELECT * FROM {$this->table_prefix}_catalog_properties_def_values WHERE property_id IN ("
                    .implode(",", array_keys($this->cache['order_prop_list'][$group_id])).") ORDER BY id");
                while ($db->next_record()) {
                    $this->cache['order_prop_list'][$group_id][$db->Record['property_id']]['val'][$db->Record['id']] = $db->Record['value'];
                }
            }
        }

        return $this->cache['order_prop_list'][$group_id];
    }
    /**
     * Отобразить список свойств заказа вместе со значениями
     *
     * @param array $selected_vals Выбранные значения свойств звказа
     */
    function showOrderPropList($selected_vals = false)
    {   global $tpl, $CONFIG;
        $list = $this->getOrderPropList();
        if ($list) {
            $tpl->newBlock('block_order_properties');
            $tpl->newBlock('block_order_property_list');
            $i = 0;
            foreach ($list as $key => $val) {
                if (0 == $i%4) {
                    $tpl->newBlock('block_order_property_group');
                }
                $i++;
                $tpl->newBlock('block_order_property');
                $tpl->assign(array(
                    'id' => $val['id'],
                    'name' => $val['name'],
                    'title' => $val['title'],
                    'prefix' => $val['prefix'],
                    'suffix' => $val['suffix'],
                    'type' => $val['type']
                ));
                if (!empty($val['val'])) {
                    foreach ($val['val'] as $k => $v) {
                        $tpl->newBlock('block_order_property_val');
                        $tpl->assign(array(
                            'id' => $k,
                            'value' => $v,
                            'checked' => is_array($selected_vals) && in_array($k, $selected_vals) ? 'checked' : ''
                        ));
                    }
                }
            }
        }
    }
    /**
     * Отобразить список свойств заказа определнного продукта вместе со значениями
     *
     * @param int $prod_id
     */
    function showProdOrderPropList($prod_ord_prop_vals, $group_id = 0)
    {   global $tpl, $CONFIG;
        $list = $this->getOrderPropList($group_id);
        if ($list && $prod_ord_prop_vals) {
            $tpl->newBlock('block_order_properties');
            $tpl->newBlock('block_order_property_list');
            $i = 0;//return;
            foreach ($list as $key => $val) {
                $i++;
                $intersection = !empty($val['val']) ? array_intersect($prod_ord_prop_vals, array_keys($val['val'])) : false;
                if (! empty($intersection)) {
                    $tpl->newBlock('block_order_property');
                    $tpl->assign(array(
                        'id' => $val['id'],
                        'name' => $val['name'],
                        'title' => $val['title'],
                        'prefix' => $val['prefix'],
                        'suffix' => $val['suffix'],
                        'type' => $val['type']
                    ));
                    if (count($intersection)>1) {
                        $tpl->newBlock('block_order_property_val_list');
                        $tpl->assign(array(
                            'id' => $val['id'],
                            'name' => $val['name'],
                            'title' => $val['title'],
                            'prefix' => $val['prefix'],
                            'suffix' => $val['suffix'],
                            'type' => $val['type']
                        ));
                        foreach ($val['val'] as $k => $v) {
                            if (in_array($k, $intersection)) {
                                $tpl->newBlock('block_order_property_val');
                                $tpl->assign(array(
                                    'id' => $k,
                                    'value' => $v,
                                    'name' => $val['name'],
                                    'prop_id' => $val['id'],
                                ));
                            }
                        }
                    } else {
                        $tpl->newBlock('block_order_property_val_one');
                        list($k, $v) = each($intersection);
                        $tpl->assign(array(
                            'id' => $v,
                            'name' => $val['name'],
                            'title' => $val['title'],
                            'prefix' => $val['prefix'],
                            'suffix' => $val['suffix'],
                            'type' => $val['type'],
                            'value' => $val['val'][$v],
                            'prop_id' => $val['id'],
                        ));
                    }
                }
            }
        }
    }

    /**
     *
     * Проверка имени свойства на уникальность
     *
     */
    function is_property_name_unique($name, $id = 0) {
        global $db, $server, $lang;
        $db->query('SELECT count(*) as cnt FROM '.$this->table_prefix.'_catalog_properties_table
            		WHERE property_name = "'.$name.'"'.(($id) ? ' AND id <> '.$id : ''));
        $db->next_record();
        return intval($db->f('cnt')) == 0;
    }

    /**
     *
     * Добавить свойство
     *
     */
    function add_property($select_type, $title, $name, $prefix, $suffix, $def_value, $new_value) {
        global $db, $server, $lang;
        if (!@$name) return FALSE;
        $select_type	= (int)$select_type;
        $title          = $db->escape(trim($title));
        $name           = $db->escape(trim($name));
        $lang 	        = $db->escape(trim($lang));
        $prefix         = $db->escape(trim($prefix));
        $suffix         = $db->escape(trim($suffix));

        switch ($select_type) {
            case 1:
                if (!$this->is_property_name_unique($name)) return FALSE;
                $db->query('INSERT INTO '.$this->table_prefix.'_catalog_properties_table '
                    .'(property_title, property_name, property_prefix, property_suffix, type)'
                    .' VALUES ("'.$title.'", "'.$name.'", "'.$prefix.'", "'.$suffix.'", '.$select_type.')');
                if ( ($property_id = $db->lid()) ) {
                    $defValue = isset($def_value[0]) ? $db->escape(trim($def_value[0])) : false;
                    if (false !== $defValue) {
                        $db->query('INSERT INTO '.$this->table_prefix.'_catalog_properties_def_values '
                            .'(property_id, value) VALUES ('.$property_id.', "'.$defValue.'")');
                    }
                }
                break;
            case 2:
            case 3:
                if (!$this->is_property_name_unique($name) || empty($def_value)) return FALSE;
                $db->query('INSERT INTO '.$this->table_prefix.'_catalog_properties_table '
                    .'(property_title, property_name, property_prefix, property_suffix, type)'
                    .' VALUES ("'.$title.'", "'.$name.'", "'.$prefix.'", "'.$suffix.'", '.$select_type.')');
                if ( ($property_id = $db->lid()) ) {
                    $ins = isset($def_value[0]) && '' !== $def_value[0] ? array("({$property_id}, '".$db->escape(trim($def_value[0]))."')") : array();
                    foreach($new_value as $val) {
                        $val = $db->escape(trim($val));
                        if ('' !== $val) {
                            $ins[] = "({$property_id}, '{$val}')";
                        }
                    }
                    if (! empty($ins)) {
                        $ins = array_unique($ins);
                        $db->query("INSERT INTO {$this->table_prefix}_catalog_properties_def_values"
                            ." (property_id, value) VALUES ".implode(", ", $ins));
                    }
                    /*
            		foreach($def_value as $key => $val) {
    					$def_value[$key] = $db->escape(trim($val));
    					if ('' === $def_value[$key]) {
    					    unset($def_value[$key]);
    					}
    				}
    				$db->query("INSERT INTO {$this->table_prefix}_catalog_properties_def_values (property_id, value)"
    				    ." VALUES ({$property_id}, '".implode("'), ({$property_id}, '", $def_value)."')");
    				*/
                }
                break;
            case 4:
                if (!$this->is_property_name_unique($name)) return FALSE;
                $db->query('INSERT INTO '.$this->table_prefix.'_catalog_properties_table '
                    .'(property_title, property_name, property_prefix, property_suffix, type)'
                    .' VALUES ("'.$title.'", "'.$name.'", "", "", '.$select_type.')');
                break;
            case 2:
        }
        return TRUE;
    }

    /**
     *
     * Изменить свойство
     *
     */
    function update_property($select_type, $id, $title, $name, $prefix, $suffix, $def_value, $new_value) {
        global $db, $server, $lang;
        $id             = (int)$id;
        $select_type	= (int)$select_type;
        $lang           = $db->escape(trim($lang));
        $title          = $db->escape(trim($title));
        $name           = $db->escape(trim($name));
        $prefix         = $db->escape(trim($prefix));
        $suffix         = $db->escape(trim($suffix));

        if (!@$title || !@$name || !@$id && (! empty($def_value) || !empty($new_value)) ) {
            return 1;
        }
        switch ($select_type) {
            case 1:
                if (!$this->is_property_name_unique($name,$id)) {
                    return 2;
                }
                $db->query("UPDATE {$this->table_prefix}_catalog_properties_table "
                    ."SET property_title = '{$title}', property_name = '{$name}', "
                    ."property_prefix = '{$prefix}', property_suffix = '{$suffix}', "
                    ."type = {$select_type} WHERE id = {$id}");

                $db->query("DELETE FROM {$this->table_prefix}_catalog_properties_def_values WHERE property_id = {$id}");
                $db->query("OPTIMIZE TABLE {$this->table_prefix}_catalog_properties_def_values");

                $defValue = $db->escape(trim($def_value[0]));
                if ('' !== $defValue) {
                    $db->query("INSERT INTO {$this->table_prefix}_catalog_properties_def_values  (property_id, value) "
                        ."VALUES ({$id}, '{$defValue}')");
                }
                break;
            case 2:
            case 3:
                if (!$this->is_property_name_unique($name,$id)) {
                    return 2;
                }
                $prop_vals = $this->getPropValList($id);
                $db->query("UPDATE {$this->table_prefix}_catalog_properties_table "
                    ."SET property_title = '{$title}', property_name = '{$name}', "
                    ."property_prefix = '{$prefix}', property_suffix = '{$suffix}', "
                    ."type = {$select_type} WHERE id = {$id}");

                $db->query("DELETE FROM {$this->table_prefix}_catalog_properties_def_values WHERE property_id = {$id}");
                $upd = array();
                foreach($def_value as $key => $val) {
                    $val = trim($val);
                    if (2 == $select_type && $val != $prop_vals[$key]) {
                        $val = $db->escape($val);
                        $db->query("UPDATE {$this->table_prefix}_catalog_property_values SET value='{$val}' "
                            ." WHERE property_id = {$id} AND value='".My_Sql::escape($prop_vals[$key])."'");
                    } else {
                        $val = $db->escape($val);
                    }

                    if ('' !== $val) {
                        $upd[] = "({$key}, {$id}, '{$val}')";
                    }
                }
                if (! empty($upd)) {
                    $db->query("REPLACE INTO {$this->table_prefix}_catalog_properties_def_values"
                        ." (id, property_id, value) VALUES ".implode(", ", $upd));
                }
                $upd = array();
                foreach($new_value as $val) {
                    $val = $db->escape(trim($val));
                    if ('' !== $val) {
                        $upd[] = "({$id}, '{$val}')";
                    }
                }
                if (! empty($upd)) {
                    $db->query("INSERT INTO {$this->table_prefix}_catalog_properties_def_values"
                        ." (property_id, value) VALUES ".implode(", ", $upd));
                }
                $db->query("OPTIMIZE TABLE {$this->table_prefix}_catalog_properties_def_values");
                break;
            case 4:
                if (!$this->is_property_name_unique($name,$id)) {
                    return 2;
                }
                $db->query("UPDATE {$this->table_prefix}_catalog_properties_table "
                    ."SET property_title = '{$title}', property_name = '{$name}', "
                    ."property_prefix = '', property_suffix = '', "
                    ."type = {$select_type} WHERE id = {$id}");
                $db->query("DELETE FROM {$this->table_prefix}_catalog_properties_def_values WHERE property_id = {$id}");

                break;
        }
        return 0;
    }

    /**
     *
     * Удалить свойство
     *
     */
    function delete_property($id) {
        global $db, $server, $lang;
        $id   = (int)$id;
        $lang = $db->escape(trim($lang));
        if (!@$id) return FALSE;
        // Удаление привязки св-ва к группе
        $db->query('DELETE FROM '.$this->table_prefix.'_catalog_properties WHERE property_id='.$id);
        // удаление значений свойства по умолчанию
        $db->query('DELETE FROM '.$this->table_prefix.'_catalog_properties_def_values WHERE property_id='.$id);
        // удаление значений св-ва
        $db->query('DELETE FROM '.$this->table_prefix.'_catalog_property_values WHERE property_id='.$id);
        // удаление самого свойства
        $db->query('DELETE FROM '.$this->table_prefix.'_catalog_properties_table WHERE id='.$id);
        if ($db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Получить список свойств
     *
     * @param int $group_id - id группы
     * @param boolean $in_group - true - вернуть список свойств группы, false - все свойства кроме свойств группы
     * @param boolean $get_vals - для каждого свойства возвращать список  возможных значений
     * @return mixed
     */
    function getPropList($group_id, $in_group, $get_vals = true)
    {   global $db;

        $group_id = (int) $group_id;
        $ret = false;
        $db->query("SELECT PT.*, P.search, P.range, P.ord FROM "
            .$this->table_prefix."_catalog_properties_table AS PT"
            ." LEFT JOIN ".$this->table_prefix."_catalog_properties AS P ON P.group_id=".$group_id." AND P.property_id=PT.id"
            ." WHERE P.property_id IS ".(!$in_group ? '' : 'NOT ')."NULL"
            ." ORDER BY ".(!$in_group ? "PT.id " : 'P.ord'));
        while ($db->next_record()) {
            $ret[$db->f('id')] =  array (
                'prt_id' => $db->f('id'),
                'prt_title' => $db->f('property_title'),
                'prt_name' => $db->f('property_name'),
                'prt_prefix' => $db->f('property_prefix'),
                'prt_suffix' => $db->f('property_suffix'),
                'prt_type' => $db->f('type'),
                'prt_ord' => $db->f('ord'),
                'in_group' => (boolean)$in_group,
                'search' => $db->f('search'),
                'range'	=> $db->f('range'),
                'vals' => false
            );
        }
        if ($get_vals && $ret) {
            $db->query("SELECT * FROM {$this->table_prefix}_catalog_properties_def_values"
                ." WHERE property_id IN (".implode(",", array_keys($ret)).")");
            while ($db->next_record()) {
                $ret[$db->f('property_id')]['vals'][$db->f('id')] = $db->f('value');
            }
        }
        return $ret;
    }

    /**
     *
     * Прикрепить к группе свойства
     *
     */
    function add_prp_grp($id, $prp, $search, $range, $ord)
    {
        global $db;

        $id = (int)$id;
        if (!$id) return FALSE;
        // Свойства группы
        //var_dump($_POST);
        $insert = array();
        //var_dump($prp);
        for ($i=0; $i < sizeof($prp); $i++) {
            $prp_1 = $db->escape(trim($prp[$i]));

            if ($prp_1) {
                $is_search = (is_array($search) && in_array($prp_1, $search)) ? 1 : 0;
                $is_range = (is_array($range) && in_array($prp_1, $range)) ? 1 : 0;
                $order = isset($ord[$prp_1]) ? (int) $ord[$prp_1] : 0;
                $insert[] = '('.$prp_1.', '.$id.', '.$is_search.', '.$is_range.', '.$order.')';
                $query[] = 'INSERT INTO '.$this->table_prefix.'_catalog_properties  VALUES ('
                    .$prp_1.', '.$id.', '.$is_search.', '.$is_range.', '.$order.')';
            }
        }
        //	var_dump($insert);exit;
        if (!empty($insert)) {
            $db->query('DELETE FROM '.$this->table_prefix.'_catalog_properties WHERE group_id='.$id);
            $db->query('INSERT INTO '.$this->table_prefix.'_catalog_properties (property_id, group_id, search, `range`, ord) VALUES '.implode(", ", $insert));
            return true;
        }
        return FALSE;
    }

    /**
     * пзменить свойство товара - лидер продаж
     * @param int $id	Id товара
     * @return boolean
     */
    function changeLeader($id)
    {   global $db;

        if (!$id = (int)$id) {
            return FALSE;
        }

        $db->query('UPDATE '.$this->table_prefix.'_catalog_products
		        	SET leader = IF (leader, 0, 1) WHERE id = '.$id);

        return $db->affected_rows() ? TRUE : FALSE;

    }
    function changeArchive($id)
    {   global $db;

        if (!$id = (int)$id) {
            return FALSE;
        }

        $db->query('UPDATE '.$this->table_prefix.'_catalog_products
		        	SET archive = IF (archive, 0, 1) WHERE id = '.$id);

        return $db->affected_rows() ? TRUE : FALSE;

    }
    /**
     * Изменить свойство товара - новинки продаж
     * @param int $id	Id товара
     * @return boolean
     */
    function changeNovelty($id)
    {   global $db;

        if (!$id = (int)$id) {
            return FALSE;
        }

        $db->query('UPDATE '.$this->table_prefix.'_catalog_products
		        	SET novelty = IF (novelty, 0, 1) WHERE id = '.$id);

        return $db->affected_rows() ? TRUE : FALSE;

    }

    /**
     * Сменить категорию у товаров
     *
     * @param int	$new_ctgr	ID новой категории
     * @param array	$prod_list	Массив идентификаторов товаров
     * @return boolean
     */
    function changCatgrOfProducts($new_ctgr, $prod_list)
    {   global $db;

        $new_ctgr = (int) $new_ctgr;
        $ctgr = $this->getCategory($new_ctgr);
        $ret = false;
        if ($ctgr && is_array($prod_list) && !empty($prod_list)) {
            $db->query("UPDATE {$this->table_prefix}_catalog_products SET category_id={$new_ctgr} WHERE id IN (".
                implode(",", array_map('intval', $prod_list)).")");
        }

        return $ret;
    }

    /**
     * Добавить связанные товары
     *
     * @param int $type
     * @param int $prod_id
     * @param int $concrn_ids
     * @return boolean
     */
    function addConcernedProducts($type, $prod_id, $concrn_ids)
    {
        global $db;

        $prod_id = (int)$prod_id;
        $type = (int) $type;
        if (0 >= $prod_id || empty($concrn_ids)) {
            return FALSE;
        }
        $ins = array();
        foreach ($concrn_ids as $concrn_id => $rank) {
            $ins[] = "({$prod_id}, ".intval($concrn_id).", {$type}, ".intval($rank).")";
        }
        $db->query("REPLACE INTO ".$this->table_prefix."_catalog_concerned_products (prod_id, concrn_id, type, rank) VALUES "
            .implode(", ", $ins));

        return $db->nf() ? TRUE : FALSE;
    }

    function delAllConcernedProducts($type, $prod_id)
    {
        global $db;
        $db->query("DELETE FROM ".$this->table_prefix."_catalog_concerned_products WHERE type=".intval($type)
            ." AND prod_id=".intval($prod_id));
        return $db->affected_rows() ? TRUE : FALSE;
    }

    function get_groups_XML(&$doc, &$parent)
    {
        global $db;

        $query = "SELECT property_id, value FROM `".$this->table_prefix."_catalog_properties_def_values` ORDER BY property_id,value";
        $db->query($query);
        while ($db->next_record()) {
            $def_values[$db->f('property_id')][] = $this->conv_str($db->f('value'));
        }

        $query	= "SHOW TABLES LIKE '".$this->table_prefix."_shop_discounts'";
        $db->query($query);
        if ($db->nf()) {
            $query = "SELECT * FROM `".$this->table_prefix."_shop_discounts` ORDER BY id";
            $db->query($query);
            while ($db->next_record()) {
                $discounts[$db->f('id')]['title']	= $this->conv_str($db->f('name'));
                $discounts[$db->f('id')]['value']	= $db->f('value');
                $discounts[$db->f('id')]['from']	= $db->f('min');
                $discounts[$db->f('id')]['to']		= $db->f('max');
                $discounts[$db->f('id')]['type']	= $db->f('is_percent');
                $discounts[$db->f('id')]['active']	= $db->f('active');
            }
        }

        $query = "SELECT g.id as group_id,g.*, p.property_id, p.search, pt.*
					FROM `".$this->table_prefix."_catalog_groups` g
						 LEFT JOIN `".$this->table_prefix."_catalog_properties` p ON g.id=p.group_id
						 LEFT JOIN `".$this->table_prefix."_catalog_properties_table` pt ON p.property_id=pt.id
					ORDER BY g.id";
        $db->query($query);
        if (!$db->nf()) {
            return FALSE;
        }

        $groups = $doc->create_element("groups");
        $groups = $parent->append_child($groups);
        $cur_group_id = 0;
        while ($db->next_record()) {
            if ($cur_group_id != $db->f('group_id')) {
                $cur_group_id = $db->f('group_id');
                $group = $doc->create_element("group");
                $group->set_attribute("id", $cur_group_id);
                $group = $groups->append_child($group);
                $group->new_child("name", $this->conv_str($db->f('group_title')));
                if ($db->f('property_id')) {
                    $properties = $doc->create_element("properties");
                    $properties = $group->append_child($properties);
                }
            }
            if ($db->f('property_id')) {
                $property = $doc->create_element("property");
                $property->set_attribute("id", $db->f('property_id'));
                $property->set_attribute("type", $db->f('type'));
                $property->set_attribute("search", $db->f('search'));
                $property = $properties->append_child($property);
                if ($db->f('property_title'))
                    $property->new_child("property_title", $this->conv_str($db->f('property_title')));
                if ($db->f('property_name'))
                    $property->new_child("property_name", $this->conv_str($db->f('property_name')));
                if ($db->f('property_prefix'))
                    $property->new_child("property_prefix", $this->conv_str($db->f('property_prefix')));
                if ($db->f('property_suffix'))
                    $property->new_child("property_suffix", $this->conv_str($db->f('property_suffix')));

                if (sizeof($def_values[$db->f('property_id')])) {
                    $property_default_values = $doc->create_element("property_default_values");
                    $property_default_values = $property->append_child($property_default_values);
                    for ($i=0; $i < sizeof($def_values[$db->f('property_id')]); $i++)
                        $property_default_values->new_child("value", $def_values[$db->f('property_id')][$i]);
                }
            }
        }
    }

    function get_ptypes_XML(&$doc, &$parent) {
        global $db;

        $ret = array();
        $query = "SELECT * FROM `".$this->table_prefix."_catalog_ptypes` ORDER BY id";
        $db->query($query);

        $manufacturers = $doc->create_element("manufacturers");
        $manufacturers = $parent->append_child($manufacturers);
        while ($db->next_record()) {
            $ret[$db->f('id')] = $db->f('ptype_name');

            $manufacturer = $doc->create_element("manufacturer");
            $manufacturer->set_attribute("id", $db->f('id'));
            $manufacturer = $manufacturers->append_child($manufacturer);
            $manufacturer->new_child("name", $this->conv_str($db->f('ptype_name')));
            $manufacturer->new_child("link", $this->conv_str($db->f('link')));
        }
        return $ret;
    }

    function get_category_XML(&$doc, &$parent, &$ptypes, $category_id=1)
    {
        global $CONFIG, $db;

        if (!($cat_info = $this->getCategory($category_id))) return;

        $category = $doc->create_element("category");
        $category->set_attribute("id", $cat_info["id"]);
        $category->set_attribute("active", $cat_info["active"]);
        if ($cat_info["group_id"])
            $category->set_attribute("group", $cat_info["group_id"]);
        $category = $parent->append_child($category);
        $category->new_child("title", $this->conv_str($cat_info["title"]));
        $category->new_child("owner_id", (int) $cat_info["owner_id"]);
        $category->new_child("usr_group_id", (int) $cat_info["usr_group_id"]);
        $category->new_child("rights", decbin($cat_info["rights"]));

        $description = $doc->create_element("description");
        $description = $category->append_child($description);
        $description_cdata = $doc->create_cdata_section($this->conv_str($cat_info["description"]));
        $description_cdata = $description->append_child($description_cdata);

        $query = "SELECT id FROM `".$this->table_prefix."_catalog_categories`
					WHERE parent_id = ".$category_id."
					ORDER BY ".$CONFIG['catalog_ctg_order_by'];
        $res = $db->getArrayOfResult($query);
        if ($res) {
            for ($i=0; $i < sizeof($res); $i++) {
                $this->get_category_XML($doc, $category, $ptypes, $res[$i]['id']);
            }
        }

        // информация о товарах
        $CD = new CCatalogData_DB($this->table_prefix.'_catalog_products', 'id');
        $cat_products = $CD->getProducts($category_id, '', null, null, true);

        if ($cat_products) {
            for ($i=0; $i < sizeof($cat_products); $i++) {
                $product = $doc->create_element("product");
                $product->set_attribute("id", $cat_products[$i]["product_id"]);
                $product->set_attribute("group", $cat_products[$i]["group_id"]);
                $product->set_attribute("active", $cat_products[$i]["product_active"]);
                if ($cat_products[$i]["product_leader"])
                    $product->set_attribute("leader", $cat_products[$i]["product_leader"]);
                $product = $category->append_child($product);
                $product->new_child("code", $this->conv_str($cat_products[$i]["product_code"]));
                $product->new_child("name", $this->conv_str($cat_products[$i]["product_title"]));
                // короткое описание
                $short_description = $doc->create_element("short_description");
                $short_description = $product->append_child($short_description);
                $short_description_cdata = $doc->create_cdata_section($this->conv_str($cat_products[$i]["product_inf"]));
                $short_description_cdata = $short_description->append_child($short_description_cdata);
                // полное описание
                $full_description = $doc->create_element("full_description");
                $full_description = $product->append_child($full_description);
                $full_description_cdata = $doc->create_cdata_section($this->conv_str($cat_products[$i]["product_description"]));
                $full_description_cdata = $full_description->append_child($full_description_cdata);
                // производитель
                $manufacturer = $product->new_child("manufacturer", $this->conv_str($ptypes[$cat_products[$i]["ptype_id"]]));
                $manufacturer->set_attribute("id", $cat_products[$i]["ptype_id"]);
                // валюта и цены
                $product->new_child("currency", $cat_products[$i]["currency"]);
                if ($cat_products[$i]["price_1"])
                    $product->new_child("price1", $cat_products[$i]["price_1"]);
                if ($cat_products[$i]["price_2"])
                    $product->new_child("price2", $cat_products[$i]["price_2"]);
                if ($cat_products[$i]["price_3"])
                    $product->new_child("price3", $cat_products[$i]["price_3"]);
                if ($cat_products[$i]["price_4"])
                    $product->new_child("price4", $cat_products[$i]["price_4"]);
                if ($cat_products[$i]["price_5"])
                    $product->new_child("price5", $cat_products[$i]["price_5"]);
                if ($cat_products[$i]["image_middle"])
                    $product->new_child("image", $this->conv_str($cat_products[$i]["image_middle"]));
                if ($cat_products[$i]["image_big"])
                    $product->new_child("image_big", $this->conv_str($cat_products[$i]["image_big"]));
                if ($cat_products[$i]["product_availability"])
                    $product->new_child("availability", $this->conv_str($cat_products[$i]["product_availability"]));
                $product->new_child("owner_id", (int) $cat_products[$i]["owner_id"]);
                $product->new_child("usr_group_id", (int) $cat_products[$i]["usr_group_id"]);
                $product->new_child("rights", decbin($cat_products[$i]["rights"]));

                // динамические свойства
                $query = "SELECT p.property_id, pt.property_title, pv.value, pt.type
							FROM ".$this->table_prefix."_catalog_properties_table pt
							JOIN ".$this->table_prefix."_catalog_properties p
							LEFT JOIN ".$this->table_prefix."_catalog_property_values pv
								ON p.property_id = pv.property_id AND pv.product_id = ".$cat_products[$i]["product_id"]."
							WHERE pt.id = p.property_id
							AND p.group_id = ".$cat_products[$i]["group_id"];
                $db->query($query);

                if ($db->nf()) {
                    $dynamic_properties = $doc->create_element("dynamic_properties");
                    $dynamic_properties = $product->append_child($dynamic_properties);
                    while ($db->next_record()) {
                        $property = $doc->create_element("property");
                        $property->set_attribute("id", $db->f("property_id"));
                        $property = $dynamic_properties->append_child($property);
                        $item = $doc->create_element("item");
                        $item = $property->append_child($item);
                        $item->new_child("title", $this->conv_str($db->f("property_title")));
                        $item->new_child("value", $this->conv_str($db->f("value")));

                    }
                }
            }
        }
        unset($cat_products);
        unset($CD);
    }

    function getNodeValue($parent, $node_name) {
        $node_array = $parent->get_elements_by_tagname($node_name);
        return (sizeof($node_array)) ? $this->conv_str_from_utf8(trim($node_array[0]->get_content())) : '';
    }

    function doImportXMLData($dom) {
        global $request_clear_xml, $db;
        $_groups_ = array();
        $_properties_ = array();
        $_discounts_ = array();

        // обработка информации о группах
        $groups_array = $dom->get_elements_by_tagname('groups');
        if (is_array($groups_array)) {
            // сформируем массив с информацией о группах
            foreach ($groups_array as $idx => $groups) {
                $group_array = $groups->get_elements_by_tagname('group');
                foreach ($group_array as $idx => $group) {
                    $group_id = $group->get_attribute('id');
                    $_groups_[$group_id]['name'] = $this->getNodeValue($group, 'name');
                    $properties = $group->get_elements_by_tagname('properties');
                    if ($cnt = sizeof($properties)) {
                        for ($i=0; $i < $cnt; $i++) {
                            foreach ($properties[$i]->get_elements_by_tagname('property') as $idx => $property) {
                                $prty_id = $property->get_attribute('id');
                                $_groups_[$group_id]['properties'][] = array(
                                    'prty_id'	=> $prty_id,
                                    'search'	=> $property->get_attribute('search'),
                                );
                                if (!is_array($_properties_[$prty_id])) {
                                    // сформируем массив с информацией о свойствах
                                    $_properties_[$prty_id] = array (
                                        'property_title'	=> $this->getNodeValue($property, 'property_title'),
                                        'property_name'		=> $this->getNodeValue($property, 'property_name'),
                                        'property_prefix'	=> $this->getNodeValue($property, 'property_prefix'),
                                        'property_suffix'	=> $this->getNodeValue($property, 'property_suffix'),
                                        'type'				=> $property->get_attribute('type'),
                                        'default_values'	=> array(),
                                    );
                                    $default_values_arr = $property->get_elements_by_tagname('property_default_values');
                                    if (sizeof($default_values_arr)) {
                                        foreach ($default_values_arr as $idx => $default_values) {
                                            foreach ($default_values->get_elements_by_tagname('value') as $idx=>$value) {
                                                $_properties_[$prty_id]['default_values'][] = $this->conv_str_from_utf8($value->get_content());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $discounts = $group->get_elements_by_tagname('discounts');
                    if ($cnt = sizeof($discounts)) {
                        for ($i=0; $i < $cnt; $i++) {
                            foreach ($discounts[$i]->get_elements_by_tagname('discount') as $idx => $discount) {
                                $discount_id = $discount->get_attribute('id');
                                $_groups_[$group_id]['discounts'][] = $discount_id;
                                if (!is_array($_discounts_[$discount_id])) {
                                    // сформируем массив с информацией о скидках
                                    $_discounts_[$discount_id] = array (
                                        'name'		=> $this->getNodeValue($discount, 'title'),
                                        'value'		=> intval($this->getNodeValue($discount, 'value')),
                                        'min'		=> intval($this->getNodeValue($discount, 'from')),
                                        'max'		=> intval($this->getNodeValue($discount, 'to')),
                                        'is_percent'=> intval($discount->get_attribute('type')),
                                        'active'	=> intval($discount->get_attribute('active')),
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }

        // загрузим информацию о группах, свойствах и скидках в БД
        $query	= "SHOW TABLES LIKE '".$this->table_prefix."_shop_discounts'";
        $db->query($query);
        $shop_exist = $db->nf();

        if ($request_clear_xml) {
            $db->query("TRUNCATE TABLE ".$this->table_prefix."_catalog_groups");
            $db->query("TRUNCATE TABLE ".$this->table_prefix."_catalog_properties");
            $db->query("TRUNCATE TABLE ".$this->table_prefix."_catalog_properties_table");
            $db->query("TRUNCATE TABLE ".$this->table_prefix."_catalog_properties_def_values");
            if ($shop_exist)
                $db->query("TRUNCATE TABLE ".$this->table_prefix."_shop_discounts");
        }
        foreach ($_groups_ as $group_id => $group_info) {
            if ($group_info["name"]) {
                //$db->query("INSERT INTO ".$this->table_prefix."_catalog_groups SET
                $db->query("REPLACE INTO ".$this->table_prefix."_catalog_groups SET
							id = ".$group_id.",
							group_title='".addslashes($group_info["name"])."'");
            }
            if (is_array($group_info["properties"])) {
                foreach ($group_info["properties"] as $idx => $prty_info) {
                    //$db->query("INSERT INTO ".$this->table_prefix."_catalog_properties SET
                    $db->query("REPLACE INTO ".$this->table_prefix."_catalog_properties SET
								property_id = ".$prty_info["prty_id"].",
								group_id = ".$group_id.",
								search = ".$prty_info["search"]);
                }
            }
        }
        foreach ($_properties_ as $prty_id => $prty_info) {
            //$db->query("INSERT INTO ".$this->table_prefix."_catalog_properties_table SET
            $db->query("REPLACE INTO ".$this->table_prefix."_catalog_properties_table SET
						id = ".$prty_id.",
						property_title = '".addslashes($prty_info["property_title"])."',
						property_name = '".addslashes($prty_info["property_name"])."',
						property_prefix = '".addslashes($prty_info["property_prefix"])."',
						property_suffix = '".addslashes($prty_info["property_suffix"])."',
						type = ".$prty_info["type"]);
            if (is_array($prty_info["default_values"])) {
                $def_table = "_catalog_properties_def_values";
                foreach ($prty_info["default_values"] as $idx => $value) {
                    //$db->query("INSERT INTO ".$this->table_prefix.$def_table." SET
                    $db->query("REPLACE INTO ".$this->table_prefix."_catalog_properties_def_values SET property_id = "
                        .$prty_id.", value = '".$value."'");
                }
            }
        }
        if ($shop_exist && is_array($_discounts_)) {
            foreach ($_discounts_ as $discount_id => $discount_info) {
                //$db->query("INSERT INTO ".$this->table_prefix."_shop_discounts SET
                $db->query("REPLACE INTO ".$this->table_prefix."_shop_discounts SET
							id = ".$discount_id.",
							name = '".addslashes($discount_info["name"])."',
							value = ".$discount_info["value"].",
							min = ".$discount_info["min"].",
							max = ".$discount_info["max"].",
							is_percent = ".$discount_info["is_percent"].",
							active = ".$discount_info["active"]);
            }
        }

        // очистим таблицы
        if ($request_clear_xml) {
            $db->query("TRUNCATE TABLE ".$this->table_prefix."_catalog_ptypes");
            $db->query("TRUNCATE TABLE ".$this->table_prefix."_catalog_categories");
            $db->query("TRUNCATE TABLE ".$this->table_prefix."_catalog_products");
            $db->query("TRUNCATE TABLE ".$this->table_prefix."_catalog_property_values");
        }

        // обработка информации о производителях
        $manufacturers_array = $dom->get_elements_by_tagname('manufacturers');
        if (is_array($manufacturers_array)) {
            foreach ($manufacturers_array as $idx => $manufacturers) {
                foreach($manufacturers->get_elements_by_tagname('manufacturer') as $idx => $manufacturer) {
                    //$db->query("INSERT INTO ".$this->table_prefix."_catalog_ptypes SET
                    $db->query("REPLACE INTO ".$this->table_prefix."_catalog_ptypes SET
								id = ".$manufacturer->get_attribute('id').",
								ptype_name = '".addslashes($this->getNodeValue($manufacturer, 'name'))."',
								link = '".addslashes($this->getNodeValue($manufacturer, 'link'))."' ");
                }
            }
        }

        // обработка информации о категориях и товарах
        $CDBT = new NsDbTree($this->table_prefix.'_catalog_categories', 'id', $this->CDBT_fields);
        $this->doImportCategoryXMLData($CDBT, $_properties_, $dom->document_element());
    }

    function doImportCategoryXMLData(&$CDBT, &$_properties_, $parent, $parent_id = 0) {
        global $db, $CONFIG;
        $rank = 0;

        list($curnt_gid, $curnt_rights) = $this->get_group_and_rights('catalog_default_rights');
        foreach ($parent->get_elements_by_tagname('category') as $idx => $category) {
            // пропустим элементы нетекущей категории
            $parent_node = $category->parent_node();
            if (intval($parent_node->get_attribute('id')) != $parent_id) continue;

            $rank++;
            $id = intval($category->get_attribute('id'));
            $group_id = intval($category->get_attribute('group'));
            $active = intval($category->get_attribute('active'));
            $title = addslashes($this->getNodeValue($category, 'title'));
            $description = addslashes($this->getNodeValue($category, 'description'));

            $oid = (int) $this->getNodeValue($category, 'owner_id');
            $oid = $oid > 0 ? $oid : $_SESSION["siteuser"]["id"];
            $gid = (int) $this->getNodeValue($category, 'usr_group_id');
            $gid = $gid > 0 ? $gid : $curnt_gid;
            $rights = bindec($this->getNodeValue($category, 'rights'));
            $rights = $rights > 0 ? $rights : $curnt_rights;
            if ($parent_id) {
                $CDBT->insert($parent_id,
                    array(
                        "id" => $id,
                        "title" => $title,
                        "parent_id" => $parent_id,
                        "group_id" => $group_id,
                        "description" => $description,
                        "rank" => $rank,
                        "active" => $active,
                        "owner_id" => $oid,
                        "usr_group_id" => $gid,
                        "rights" => $rights )
                );
            } else {
                $db->query("REPLACE INTO ".$this->table_prefix."_catalog_categories SET
							id=".$id.",
							cleft=0, cright=".$id.",clevel=0,
							parent_id=".$parent_id.",
							active=".$active.",
							title='".$title."',
							group_id=".$group_id.",
							description='".$description."',
							rank=".$rank.",
							owner_id=".(int)$oid.",
							usr_group_id=".(int)$gid.",
							rights=".$rights );
            }
            $this->doImportCategoryXMLData($CDBT, $_properties_, $category, $id);

            // обработаем продукты данной категории
            $prd_rank = 0;
            $parent_ctg_id = $id;
            foreach ($category->get_elements_by_tagname('product') as $idx => $product) {
                // пропустим элементы нетекущей категории
                $parent_node = $product->parent_node();
                if (intval($parent_node->get_attribute('id')) != $parent_ctg_id) continue;

                $prd_rank++;
                $id				= intval($product->get_attribute('id'));
                $group_id		= intval($product->get_attribute('group'));
                #$leader			= intval($product->get_attribute('leader'));
                $leader			= intval($this->getNodeValue($product, 'leader')) || intval($product->get_attribute('leader'));
                $active			= intval($product->get_attribute('active'));
                $code			= addslashes($this->getNodeValue($product, 'code'));
                $title			= addslashes($this->getNodeValue($product, 'name'));
                $short_descr	= addslashes($this->getNodeValue($product, 'short_description'));
                $full_descr		= addslashes($this->getNodeValue($product, 'full_description'));
                $currency		= addslashes($this->getNodeValue($product, 'currency'));
                $price1			= intval($this->getNodeValue($product, 'price1'));
                $price2			= intval($this->getNodeValue($product, 'price2'));
                $price3			= intval($this->getNodeValue($product, 'price3'));
                $price4			= intval($this->getNodeValue($product, 'price4'));
                $price5			= intval($this->getNodeValue($product, 'price5'));
                $image			= addslashes($this->getNodeValue($product, 'image'));
                $image_big		= addslashes($this->getNodeValue($product, 'image_big'));
                $availability	= addslashes($this->getNodeValue($product, 'availability'));
                $ptype_id		= 0;
                foreach ($product->get_elements_by_tagname('manufacturer') as $idx => $manufacturer) {
                    $ptype_id = intval($manufacturer->get_attribute('id'));
                }
                $oid = (int) $this->getNodeValue($product, 'owner_id');
                $oid = $oid > 0 ? $oid : $_SESSION["siteuser"]["id"];
                $gid = (int) $this->getNodeValue($product, 'usr_group_id');
                $gid = $gid > 0 ? $gid : $curnt_gid;
                $rights = bindec($this->getNodeValue($product, 'rights'));
                $rights = $rights > 0 ? $rights : $curnt_rights;


                $this->check_image($image_big, $image);

                $arr = array (
                    'category_id'	=> $parent_ctg_id,
                    'group_id'  	=> $group_id,
                    'ptype_id'  	=> $ptype_id,
                    'product_code'  => $code,
                    'product_title' => $title,
                    'product_inf'  	=> $short_descr,
                    'product_description'  => $full_descr,
                    'currency'  	=> $currency,
                    'price_1'  		=> $price1,
                    'price_2'  		=> $price2,
                    'price_3'  		=> $price3,
                    'price_4'  		=> $price4,
                    'price_5'  		=> $price5,
                    'image_middle'  => $image,
                    'image_big'  	=> $image_big,
                    'product_availability'  => $availability,
                    'leader'  		=> $leader,
                    'active'  		=> $active,
                    'rank'  		=> $prd_rank,
                    'owner_id'  	=> (int) $oid,
                    'usr_group_id'  => (int) $gid,
                    'rights'  		=> $rights,
                );
                $this->update_prd($id, $arr);


                // обработка динамических свойств
                foreach ($product->get_elements_by_tagname('property') as $idx => $property) {
                    $property_id = intval($property->get_attribute('id'));
                    foreach ($property->get_elements_by_tagname('item') as $idx => $item) {
                        $value = addslashes($this->getNodeValue($item, 'value'));
                        if (empty($value)) continue;
                        //$db->query("INSERT INTO ".$this->table_prefix."_catalog_property_values SET
                        $db->query("REPLACE INTO ".$this->table_prefix."_catalog_property_values SET
									property_id=".$property_id.",
									product_id=".$id.",
									value='".$value."',
									group_id=".$group_id);
                    }
                }
            }
        }
    }
    function update_prd($ID, $DATA){
        global $db,$CONFIG; $ID = intval($ID);
        static $TBL_INFO;
        if(!is_array($DATA)){return false;}
        if(!$TBL_INFO){
            $i = $db->metadata($this->table_prefix."_catalog_products");
            foreach($i as $v){$TBL_INFO[($v['name'])] = array ('type' => $v['type'],'len'=> $v['len'],'flags'=> $v['flags']);}
            unset($i);
        }
        $query = "select * from {$this->table_prefix}_catalog_products where id={$ID} FOR UPDATE";
        $res = $db->query($query);
        if($db->nf()){
            $arr = mysql_fetch_assoc($res);
            foreach($arr as $k=>$v){if(!isset($DATA[$k])){$DATA[$k]=$v;}}
        }
        else {
            $insert = true;
        }

        unset($DATA['id']);

        $DATA['currency'] = $CONFIG['catalog_currencies'][$DATA['currency']] ? $DATA['currency'] : '';
        $QUERY  = (!$insert) ? "UPDATE " : "INSERT INTO ";
        $QUERY .= " {$this->table_prefix}_catalog_products SET id=$ID ";
        foreach($DATA as $k => $v){
            if(empty($v) && false===strpos($TBL_INFO[$k]['flags'], 'not_null')){
                $QUERY .= " , `$k` = NULL ";
            }
            elseif($TBL_INFO[$k]['type']=='int') {
                $QUERY .= " , `$k` =  ".intval($v)." ";
            }
            else {
                $QUERY .= " , `$k` = '{$v}' ";
            }

        }
        if(!$insert){ $QUERY .= " WHERE id=$ID ";}
        return $db->query($QUERY);
    }
//	Сгенерировать YML файл
    function generateYMLData($all = 0)
    {
        require_once(RP."mod/shop/lib/class.ShopPrototype.php");
        $cat = new ShopPrototype('only_create_object');
        $skidki=$cat->getAllActionProducts();
        $id_skidok=array();
        foreach($skidki as $skidka=>$price){
            $id_skidok[]=$skidka;
        }
        global $CONFIG, $db, $main, $server, $lang;
        $CONFIG['web_address'] = 'http://suprashop.ru/';
        $CONFIG['domen'] = 'suprashop.ru';
        $content = "";
        $ymlfile = $all ? $CONFIG["catalog_export_yml_path"] . $server . $lang . "_all.yml" : $CONFIG["catalog_export_yml_path"] . $server . $lang . ".yml";

        //trance price
        if (!isset($_SESSION["catalog_rates"])) {
            $main->get_rates();
        }
        $baseurl =  Core::formPageLink($CONFIG['catalog_page_link'], $CONFIG['catalog_page_address'], $lang);
        if ($handle = fopen(RP.$ymlfile, "w")) {
//			список категорий
            $ctgr_alias = array();
            $query = "SELECT * FROM " . $this->table_prefix . "_catalog_categories WHERE parent_id > 0 AND active = 1 ORDER BY id";
            $db->query($query);
            if($db->nf() > 0) {
                $content .= "<categories>\n";
                while($db->next_record()) {
                    $content .= "\t<category id=\"" . $db->f("id") . "\" parentId=\"" . $db->f("parent_id") . "\">" . htmlspecialchars($db->f("title")) . "</category>\n";
                    $ctgr_alias[$db->f("id")] = $db->f("alias") ? $db->f("alias") : $db->f("id");
                }
                $content .= "</categories>\n\n";
            }
//			список товаров
            if($all) $query = "SELECT P.* FROM ".$this->table_prefix."_catalog_products P INNER JOIN "
                .$this->table_prefix."_catalog_categories C ON category_id=C.id WHERE"
                ." 1=1 AND nyml=0 ORDER BY category_id, P.rank";
            else $query = "SELECT P.* FROM ".$this->table_prefix."_catalog_products P INNER JOIN "
                .$this->table_prefix."_catalog_categories C ON category_id=C.id WHERE"
                ." C.active = 1 AND P.active = 1 AND nyml=0 AND (product_availability='1' OR price_5>0)  AND price_1>0 ORDER BY category_id, P.rank";
            $db->query($query);
            if($db->nf() > 0)
            {
                $content = "<?xml version=\"1.0\" encoding=\"windows-1251\"?>". //".$CONFIG['pages_charset']."
                    "<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">
<yml_catalog date=\"" . date("Y-m-d G:i") . "\">
<shop>
<name>" . $CONFIG["catalog_yml_name"] . "</name>
<company>" . $CONFIG["catalog_yml_company"] . "</company>
<url>" . $CONFIG["web_address"] . "</url>
<currencies>
<currency id=\"RUR\" rate=\"1\"/>
</currencies>
{$content}
<offers>";
                fwrite($handle, iconv('UTF-8', 'WINDOWS-1251//IGNORE', $content));
                while($db->next_record())
                {
                    $colors = array(0 => '');
                    //if(!$all){
                    //	$colors = explode('#@#', $db->f("colors"));
                    //}

                    foreach($colors as $color){
                        $content = '';
                        $exists = (!in_array($db->f('product_availability'), array('нет','отсутствует', '0', 0))) ? 'true' : 'false';
                        $curr = $db->Record['currency'] ? $db->Record['currency'] : $CONFIG['catalog_default_curr'];
                        $price = $this->getCostWithTaxes($main->process_price($db->f("price_1")));
                        $price = round(CatalogPrototype::exchange($price, $curr, $CONFIG['catalog_yml_curr']), 2);
                        if(in_array($db->f("id"),$id_skidok)) {
	                       
	                        $price = $skidki[$db->f("id")] ? round(CatalogPrototype::exchange($skidki[$db->f("id")], $curr, $CONFIG['catalog_yml_curr']), 2) : $price;
                            //var_dump($price);exit;
                        }
                        $image = $db->f("image_big");
                        $content .= "\t<offer id=\"" . $db->f("id") . "\" available=\"{$exists}\">\n";
                        $content .= "\t\t<url>".htmlspecialchars("http://{$CONFIG['domen']}"
                                .CatalogPrototype::getProdLink(
                                    $baseurl,
                                    ($db->f("alias") ? $db->f("alias") : $db->f("id")),
                                    $db->f("id"),
                                    $ctgr_alias[$db->f("category_id")]))
                            ."</url>\n";
                        $content .= "\t\t<price>{$price}</price>\n";
                        $content .= "\t\t<currencyId>{$CONFIG['catalog_yml_curr']}</currencyId>\n";
                        $content .= "\t\t<categoryId>" . $db->f("category_id") . "</categoryId>\n";
                        if($image) {
                            $content .= "\t\t<picture>" . $CONFIG["web_address"] . $CONFIG["catalog_img_path"] . urlencode($image) . "</picture>\n";
                        }
                        $content .= "\t\t<name>" . htmlspecialchars(trim($db->f("product_title").' '.$color)) . "</name>\n";
                        $content .= "\t\t<vendorCode>" . htmlspecialchars($db->f("product_code")) . "</vendorCode>\n";
                        $content .= "\t\t<description>" . htmlspecialchars(strip_tags($db->f("product_description_shop"))) . "</description>\n";
                        $content .= "\t</offer>\n";
                        fwrite($handle, iconv('UTF-8', 'WINDOWS-1251//IGNORE', $content));
                    }
                }
            }
            fwrite($handle, "</offers>\n</shop>\n</yml_catalog>");
            fclose($handle);
            @chmod($cash_filename, 0757);
            $_SESSION["info_block_name"] = "block_info_download_success";
            $_SESSION["info_block_file"] = "/".$ymlfile;
            return 1;
        }
        return 0;
    }


    function generateVendorYMLData($all = 0)
    {
        global $CONFIG, $db2, $main, $server, $lang;
        $CONFIG['web_address'] = 'http://supra.ru/';
        $CONFIG['domen'] = 'supra.ru';
        $content = "";
        $ymlfile = $CONFIG["catalog_export_yml_path"] . $server . $lang . "_vendor.yml";

        //trance price
        if (!isset($_SESSION["catalog_rates"])) {
            $main->get_rates();
        }
        $baseurl =  Core::formPageLink($CONFIG['catalog_page_link'], $CONFIG['catalog_page_address'], $lang);
        if ($handle = fopen(RP.$ymlfile, "w")) {
//			список категорий
            $ctgr_alias = array();
            $query = "SELECT * FROM " . $this->table_prefix . "_catalog_categories WHERE parent_id > 0 AND active = 1 ORDER BY id";
            $db2->query($query);
            if($db2->nf() > 0) {
                $content .= "<categories>\n";
                while($db2->next_record()) {
                    $content .= "\t<category id=\"" . $db2->f("id") . "\" parentId=\"" . $db2->f("parent_id") . "\">" . htmlspecialchars($db2->f("title")) . "</category>\n";
                    $ctgr_alias[$db2->f("id")] = $db2->f("alias") ? $db2->f("alias") : $db2->f("id");
                }
                $content .= "</categories>\n\n";
            }
//			список товаров
            $query = "SELECT P.* FROM ".$this->table_prefix."_catalog_products P INNER JOIN "
                .$this->table_prefix."_catalog_categories C ON category_id=C.id WHERE"
                ." P.price_1>0 AND P.active=1 ORDER BY category_id, P.rank";

            $db2->query($query);
            if($db2->nf() > 0)
            {
                $content = "<?xml version=\"1.0\" encoding=\"windows-1251\"?>". //".$CONFIG['pages_charset']."
                    "
<yml_catalog xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' date=\"" . date("Y-m-d")."T".date("G:i:s") . "\" version=\"1.0\" xsi:noNamespaceSchemaLocation=\"VendorYML-1.0.xsd\">
<vendor name=\"Supra\">
        <url>". $CONFIG["web_address"] ."</url>
        
{$content}
<models>";
                fwrite($handle, iconv('UTF-8', 'WINDOWS-1251//IGNORE', $content));
                while($db2->next_record())
                {
                    $colors = array(0 => '');
                    //if(!$all){
                    //	$colors = explode('#@#', $db2->f("colors"));
                    //}

                    //foreach($colors as $color){
                    $content = '';
                    $exists = (!in_array($db2->f('product_availability'), array('нет','отсутствует', '0'))) ? 'true' : 'false';
                    $curr = $db2->Record['currency'] ? $db2->Record['currency'] : $CONFIG['catalog_default_curr'];
                    $price = $this->getCostWithTaxes($main->process_price($db2->f("price_1")));
                    $price = round(CatalogPrototype::exchange($price, $curr, $CONFIG['catalog_yml_curr']), 2);
                    $image = $db2->f("image_big");
                    $content .= "\t<model id=\"" . $db2->f("id") . "\" categoryId=\"".$db2->f("category_id")."\">\n";
                    $content .= "\t\t<promoUrl>".htmlspecialchars("http://{$CONFIG['domen']}"
                            .CatalogPrototype::getProdLink(
                                $baseurl,
                                ($db2->f("alias") ? $db2->f("alias") : $db2->f("id")),
                                $db2->f("id"),
                                $ctgr_alias[$db2->f("category_id")]))
                        ."</promoUrl>\n";
                    $content .= "\t\t<recomendedPrice currency=\"".$CONFIG['catalog_yml_curr']."\">{$price}</recomendedPrice>\n";

                    if($image) {
                        $content .= "\t\t<pictureUrl>" . $CONFIG["web_address"] . $CONFIG["catalog_img_path"] . urlencode($image) . "</pictureUrl>\n";
                    }
                    $content .= "\t\t<name>" . htmlspecialchars(trim($db2->f("product_title").' '.$color)) . "</name>\n";
                    $content .= "\t\t<vendorCode>" . htmlspecialchars($db2->f("product_code")) . "</vendorCode>\n";
                    $content .= "\t\t<description>" . htmlspecialchars(strip_tags($db2->f("product_description_shop"))) . "</description>\n";

                    if($instr = $this->getInstruction($db2->f("id"))){
                        $content .= "\t\t<instructionUrl>" . $CONFIG["web_address"].($instr) . "</instructionUrl>\n";
                    }





                    ///////////
                    //$db2_tmp = $db2;
                    $prp = $this->getProductProperies($db2->f("id"), $db2->f('group_id'));
                    //$db2 = $db2_tmp;
                    $prp_list = $prp['dynamic_properties'];
                    unset($prp['dynamic_properties']);

                    if (is_array($prp)) {
                        //$tpl->newBlock('block_group_properties');
                        //$tpl->assign_array('block_list_properties_test', $prp_list);
                        $i = 1;
                        $count = sizeof($prp);
                        //var_dump($prp);
                        foreach ($prp as $pk => $val) {
                            if(is_array($val['value'])){
                                //$tpl->newBlock('block_group_properties_row');
                                //$tpl->newBlock('block_group_delim');
                                //$tpl->assign($val);
                                foreach($val['value'] as $v){
                                    //$tpl->newBlock('block_group_properties_row');
                                    //$tpl->newBlock('block_group_property');
                                    //$tpl->assign(array(
                                    //	'title' => $v,
                                    //	'value' => '+'
                                    //));
                                    $content .= "\t\t<param name=\"".htmlspecialchars(strip_tags($v))."\">+</param>\n";
                                }
                            }
                            else{
                                //$tpl->newBlock('block_group_properties_row');
                                if (4 == $val['type']) {
                                    //echo $i.' - '.$count.'<br>'; 
                                    if($i != $count && ($prp[$pk]['type'] != 4)){
                                        //$tpl->newBlock('block_group_delim');
                                        //$tpl->assign($val);
                                    }
                                } elseif($val['value']) {
                                    $content .= "\t\t<param name=\"".htmlspecialchars(strip_tags($val['title']))."\">".htmlspecialchars(strip_tags($val['value']))."</param>\n";
                                    //$tpl->newBlock('block_group_property');
                                    //$tpl->assign($val);
                                }

                            }
                            $i++;
                        }
                    }else $kkk[0] = 1;
                    ///////////




                    $content .= "\t</model>\n";
                    fwrite($handle, iconv('UTF-8', 'WINDOWS-1251//IGNORE', $content));
                    //}
                }
            }
            fwrite($handle, "</models>\n</vendor>\n</yml_catalog>");
            fclose($handle);
            @chmod($cash_filename, 0757);
            $_SESSION["info_block_name"] = "block_info_download_success";
            $_SESSION["info_block_file"] = "/".$ymlfile;
            return 1;
        }
        return 0;
    }

    function generateVendorYMLDataMobile($all = 0)
    {
        global $CONFIG, $db, $main, $server, $lang;
        $CONFIG['web_address'] = 'http://suprashop.ru/';
        $CONFIG['domen'] = 'suprashop.ru';
        $content = "";

        $ymlfile = $CONFIG["catalog_export_yml_path"] . "suprashop_mobile_version.yml";

        //trance price
        if (!isset($_SESSION["catalog_rates"])) {
            $main->get_rates();
        }
        $baseurl =  Core::formPageLink($CONFIG['catalog_page_link'], $CONFIG['catalog_page_address'], $lang);
        if ($handle = fopen(RP.$ymlfile, "w")) {
//			список категорий
            $ctgr_alias = array();
            $query = "SELECT * FROM " . $this->table_prefix . "_catalog_categories WHERE parent_id > 0 AND active = 1 ORDER BY id";
            $db->query($query);
            if($db->nf() > 0) {
                $content .= "<categories>\n";
                while($db->next_record()) {
                    $content .= "\t<category id=\"" . $db->f("id") . "\" ". ($db->f("parent_id") != 1 ? ("parentId=\"" . $db->f("parent_id")) ."\"": "") . ">" . htmlspecialchars($db->f("title")) . "</category>\n";
                    $ctgr_alias[$db->f("id")] = $db->f("alias") ? $db->f("alias") : $db->f("id");
                }
                $content .= "</categories>\n\n";
            }
            /*if($db->nf() > 0) {
				$content .= "<categories>\n";
				while($db->next_record()) {
  					$content .= "\t<category id=\"" . $db->f("id") . "\" parentId=\"" . $db->f("parent_id") . "\">" . htmlspecialchars($db->f("title")) . "</category>\n";
  					$ctgr_alias[$db->f("id")] = $db->f("alias") ? $db->f("alias") : $db->f("id");
				}
				$content .= "</categories>\n\n";
			}*/
//			список товаров
            if($all) $query = "SELECT P.* FROM ".$this->table_prefix."_catalog_products P INNER JOIN "
                .$this->table_prefix."_catalog_categories C ON category_id=C.id WHERE"
                ." 1=1 AND nyml=0 ORDER BY category_id, P.rank";
            else $query = "SELECT P.* FROM ".$this->table_prefix."_catalog_products P INNER JOIN "
                .$this->table_prefix."_catalog_categories C ON category_id=C.id WHERE"
                ." C.active = 1 AND P.active = 1 AND nyml=0 AND (product_availability='1' OR price_5>0)  AND price_1>0 ORDER BY category_id, P.rank";
            $db->query($query);
            if($db->nf() > 0)
            {
                $content = "<?xml version=\"1.0\" encoding=\"windows-1251\"?>". //".$CONFIG['pages_charset']."
                    "<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">
<yml_catalog date=\"" . date("Y-m-d G:i") . "\">
<shop>
<name>" . $CONFIG["catalog_yml_name"] . "</name>
<company>" . $CONFIG["catalog_yml_company"] . "</company>
<url>" . $CONFIG["web_address"] . "</url>
<currencies>
<currency id=\"RUR\" rate=\"1\"/>
</currencies>
{$content}
<offers>";
                fwrite($handle, iconv('UTF-8', 'WINDOWS-1251//IGNORE', $content));
                while($db->next_record())
                {
                    $colors = array(0 => '');
                    //if(!$all){
                    //	$colors = explode('#@#', $db->f("colors"));
                    //}

                    foreach($colors as $color){
                        $content = '';
                        $exists = (!in_array($db->f('product_availability'), array('нет','отсутствует', '0'))) ? 'true' : 'false';
                        $curr = $db->Record['currency'] ? $db->Record['currency'] : $CONFIG['catalog_default_curr'];
                        $price = $this->getCostWithTaxes($main->process_price($db->f("price_1")));
                        $price = round(CatalogPrototype::exchange($price, $curr, $CONFIG['catalog_yml_curr']), 2);
                        $image = $db->f("image_big");
                        $content .= "\t<offer id=\"" . $db->f("id") . "\" available=\"{$exists}\">\n";
                        $content .= "\t\t<url>".htmlspecialchars("http://{$CONFIG['domen']}"
                                .CatalogPrototype::getProdLink(
                                    $baseurl,
                                    ($db->f("alias") ? $db->f("alias") : $db->f("id")),
                                    $db->f("id"),
                                    $ctgr_alias[$db->f("category_id")]))
                            ."</url>\n";
                        $content .= "\t\t<price>{$price}</price>\n";
                        $content .= "\t\t<currencyId>{$CONFIG['catalog_yml_curr']}</currencyId>\n";
                        $content .= "\t\t<categoryId>" . $db->f("category_id") . "</categoryId>\n";
                        if($image) {
                            $content .= "\t\t<picture>" . $CONFIG["web_address"] . $CONFIG["catalog_img_path"] . urlencode($image) . "</picture>\n";
                        }
                        $content .= "\t\t<name>" . htmlspecialchars(trim($db->f("product_title").' '.$color)) . "</name>\n";
                        $content .= "\t\t<vendorCode>" . htmlspecialchars($db->f("product_code")) . "</vendorCode>\n";
                        $content .= "\t\t<description>" . htmlspecialchars(strip_tags($db->f("product_description_shop"))) . "</description>\n";
                        $content .= "\t</offer>\n";
                        fwrite($handle, iconv('UTF-8', 'WINDOWS-1251//IGNORE', $content));
                    }
                }
            }
            fwrite($handle, "</offers>\n</shop>\n</yml_catalog>");
            fclose($handle);
            @chmod($cash_filename, 0757);
            $_SESSION["info_block_name"] = "block_info_download_success";
            $_SESSION["info_block_file"] = "/".$ymlfile;
            return 1;
        }
        return 0;
    }

    /*function generateVendorYMLDataMobile($all = 0)
	{
		global $CONFIG, $db2, $main, $server, $lang;
		$CONFIG['web_address'] = 'http://supra.ru/';
		$CONFIG['domen'] = 'supra.ru';
		$content = "";
		$ymlfile = $CONFIG["catalog_export_yml_path"] . "suprashop_mobile_version.yml";

		//trance price
		if (!isset($_SESSION["catalog_rates"])) {
			$main->get_rates();
		}
		$baseurl =  Core::formPageLink($CONFIG['catalog_page_link'], $CONFIG['catalog_page_address'], $lang);
		if ($handle = fopen(RP.$ymlfile, "w")) {
//			список категорий
            $ctgr_alias = array();
			$query = "SELECT * FROM " . $this->table_prefix . "_catalog_categories WHERE parent_id > 0 AND active = 1 ORDER BY id";
			$db2->query($query);
			if($db2->nf() > 0) {
				$content .= "<categories>\n";
				while($db2->next_record()) {
  					$content .= "\t<category id=\"" . $db2->f("id") . "\" ". ($db2->f("parent_id") != 1 ? ("parentId=\"" . $db2->f("parent_id")) ."\"": "") . ">" . htmlspecialchars($db2->f("title")) . "</category>\n";
  					$ctgr_alias[$db2->f("id")] = $db2->f("alias") ? $db2->f("alias") : $db2->f("id");
				}
				$content .= "</categories>\n\n";
			}
//			список товаров
			$query = "SELECT P.* FROM ".$this->table_prefix."_catalog_products P INNER JOIN "
			    .$this->table_prefix."_catalog_categories C ON category_id=C.id WHERE"
			    ." P.price_1>0 AND P.active=1 ORDER BY category_id, P.rank";
			
			$db2->query($query);
			if($db2->nf() > 0)
			{
			    $content = "<?xml version=\"1.0\" encoding=\"windows-1251\"?>". //".$CONFIG['pages_charset']."
"
<yml_catalog xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' date=\"" . date("Y-m-d")."T".date("G:i:s") . "\" version=\"1.0\" xsi:noNamespaceSchemaLocation=\"VendorYML-1.0.xsd\">
<vendor name=\"Supra\">
        <url>". $CONFIG["web_address"] ."</url>
        
{$content}
<models>";
                fwrite($handle, iconv('UTF-8', 'WINDOWS-1251//IGNORE', $content));
				while($db2->next_record())
				{
				    $colors = array(0 => '');
					//if(!$all){
					//	$colors = explode('#@#', $db2->f("colors"));
					//}
					
					//foreach($colors as $color){
						$content = '';
						$exists = (!in_array($db2->f('product_availability'), array('нет','отсутствует', '0'))) ? 'true' : 'false';
						$curr = $db2->Record['currency'] ? $db2->Record['currency'] : $CONFIG['catalog_default_curr'];
						$price = $this->getCostWithTaxes($main->process_price($db2->f("price_1")));
						$price = round(CatalogPrototype::exchange($price, $curr, $CONFIG['catalog_yml_curr']), 2);
						$image = $db2->f("image_big");
						$content .= "\t<model id=\"" . $db2->f("id") . "\" categoryId=\"".$db2->f("category_id")."\">\n";
						$content .= "\t\t<promoUrl>".htmlspecialchars("http://{$CONFIG['domen']}"
							.CatalogPrototype::getProdLink(
								$baseurl,
								($db2->f("alias") ? $db2->f("alias") : $db2->f("id")),
								$db2->f("id"),
								$ctgr_alias[$db2->f("category_id")]))
							."</promoUrl>\n";
						$content .= "\t\t<recomendedPrice currency=\"".$CONFIG['catalog_yml_curr']."\">{$price}</recomendedPrice>\n";
						
						if($image) {
							$content .= "\t\t<pictureUrl>" . $CONFIG["web_address"] . $CONFIG["catalog_img_path"] . urlencode($image) . "</pictureUrl>\n";
						}
						$content .= "\t\t<name>" . htmlspecialchars(trim($db2->f("product_title").' '.$color)) . "</name>\n";
						$content .= "\t\t<vendorCode>" . htmlspecialchars($db2->f("product_code")) . "</vendorCode>\n";
						$content .= "\t\t<description>" . htmlspecialchars(strip_tags($db2->f("product_description_shop"))) . "</description>\n";
						
						if($instr = $this->getInstruction($db2->f("id"))){
							$content .= "\t\t<instructionUrl>" . $CONFIG["web_address"].($instr) . "</instructionUrl>\n";
						}
						
						
						
						
						
						///////////
						//$db2_tmp = $db2;
						$prp = $this->getProductProperies($db2->f("id"), $db2->f('group_id'));
						//$db2 = $db2_tmp;
                $prp_list = $prp['dynamic_properties'];
                unset($prp['dynamic_properties']);
				
                if (is_array($prp)) {
                	//$tpl->newBlock('block_group_properties');
                	//$tpl->assign_array('block_list_properties_test', $prp_list);
                	$i = 1;
					$count = sizeof($prp);
					//var_dump($prp);
					foreach ($prp as $pk => $val) {
                		if(is_array($val['value'])){
						    //$tpl->newBlock('block_group_properties_row');
							//$tpl->newBlock('block_group_delim');
							//$tpl->assign($val);
							foreach($val['value'] as $v){
							    //$tpl->newBlock('block_group_properties_row');
								//$tpl->newBlock('block_group_property');
								//$tpl->assign(array(
								//	'title' => $v,
								//	'value' => '+'
								//));
								$content .= "\t\t<param name=\"".htmlspecialchars(strip_tags($v))."\">+</param>\n";
							}
						}
						else{
							//$tpl->newBlock('block_group_properties_row');
							if (4 == $val['type']) {
								//echo $i.' - '.$count.'<br>'; 
								if($i != $count && ($prp[$pk]['type'] != 4)){ 
								       //$tpl->newBlock('block_group_delim');
								       //$tpl->assign($val);
								}
							} elseif($val['value']) {
								$content .= "\t\t<param name=\"".htmlspecialchars(strip_tags($val['title']))."\">".htmlspecialchars(strip_tags($val['value']))."</param>\n";
								//$tpl->newBlock('block_group_property');
								//$tpl->assign($val);
							}
							
						}
						$i++;
                	}
                }else $kkk[0] = 1;
						///////////
						
						
						
						
						$content .= "\t</model>\n";
						fwrite($handle, iconv('UTF-8', 'WINDOWS-1251//IGNORE', $content));
					//}
				}
			}
			fwrite($handle, "</models>\n</vendor>\n</yml_catalog>");
			fclose($handle);
			@chmod($cash_filename, 0757);
			$_SESSION["info_block_name"] = "block_info_download_success";
			$_SESSION["info_block_file"] = "/".$ymlfile;
			return 1;	
		}
		return 0;
	}*/


    function load_video($id_prod,$videos,$description)
    {
        global $db,$server,$lang;
        if(is_array($videos)) {
            $i=0;
            foreach ($videos as $row) {
                //var_dump("INSERT INTO " . $server . $lang . "_catalog_products_video (id_prod,video,desc) VALUES('$id_prod','$row','$description[$i]')");
                $db->query("INSERT INTO " . $server . $lang . "_catalog_products_video (id_prod,video,`desc`) VALUES('$id_prod','$row','$description[$i]')");
                //var_dump($description[$i]);
                $i++;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    function my_iconv($charset_from, $charset_to, $str){
        if ($ret = iconv($charset_from, $charset_to, $str)) {
            return $ret;
        }
        $str_arr = explode(" ", $str);
        for ($i=0; $i < sizeof($str_arr); $i++) {
            $str_arr[$i] = iconv($charset_from, $charset_to, $str_arr[$i]);
        }
        return implode(" ", $str_arr);
    }

    function conv_str($str){
        switch ($this->data_charset) {
            case 'cp1251':
                return $this->my_iconv("WINDOWS-1251", "UTF-8", $str);
            case 'koi8-r':
                return $this->my_iconv("KOI8-R", "UTF-8", $str);
            case 'iso-8859-1':
                return $this->my_iconv("ISO-8859-1", "UTF-8", $str);
            default:
                return $str;
        }
    }

    function conv_str_from_utf8($str){
        switch ($this->data_charset) {
            case 'cp1251':
                return $this->my_iconv("UTF-8", "WINDOWS-1251", $str);
            case 'koi8-r':
                return $this->my_iconv("UTF-8", "KOI8-R", $str);
            case 'iso-8859-1':
                return $this->my_iconv("UTF-8", "ISO-8859-1", $str);
            default:
                return $str;
        }
    }

    function getConfSup($l=true){
        global $CONFIG, $db;
        $ip=$_SERVER['REMOTE_ADDR'];
        $ipArr=explode(".",$ip);
        $k='';
        $h='';
        $resIp="";
        $arr=array('178.124.157.42','178.215.66.186','95.84.182.111','5.228.84.47','5.228.83.228','5.228.20.108','46.53.195.148','178.120.114.133');//
        $arrC=$db->getArrayOfResult("SELECT ip
                                FROM `sup_rus_countir`
                                GROUP BY ip");
        if(is_array($arrC))foreach($arrC as $key=>$val)
        {
            $arr[]=$val['ip'];
        }
        $arrQ=$db->getArrayOfResult("SELECT ip
                                FROM `sup_rus_configuration_log`
                                GROUP BY ip");

        if(is_array($arrQ))foreach($arrQ as $key=>$val)
        {
            $arr[]=$val['ip'];
        }
        $i=0;
        foreach($ipArr as $v){
            if($i==3) continue;
            $resIp ? $resIp=$resIp.".".$v : $resIp=$v;
            $i++;
        }
        if(!in_array($ip,$arr) &&  !isset($_SESSION['siteuser']['id'])  && !isset($_COOKIE['supra_c']) )//&& ($_SERVER['SERVER_NAME'] != "supra.ru")  && !$this->is_mobile()
        {
            return true;//base64_decode($l ? $k : $h);
        }
        else{
            setcookie('supra_c',1,null,'/');
        }

        return "";

    }
    function is_mobile() {
        $user_agent=strtolower(getenv('HTTP_USER_AGENT'));
        $accept=strtolower(getenv('HTTP_ACCEPT'));

        if ((strpos($accept,'text/vnd.wap.wml')!==false) ||
            (strpos($accept,'application/vnd.wap.xhtml+xml')!==false)) {
            return true; // Возращает 1 если мобильный браузер определен по HTTP-заголовкам
        }

        if (isset($_SERVER['HTTP_X_WAP_PROFILE']) ||
            isset($_SERVER['HTTP_PROFILE'])) {
            return true; // Возвращает 2 если мобильный браузер определен по установкам сервера
        }

        if (preg_match('/(mini 9.5|vx1000|lge |m800|e860|u940|ux840|compal|'.
            'wireless| mobi|lg380|ahong|lgku|lgu900|lg210|lg47|lg920|lg840|'.
            'lg370|sam-r|mg50|s55|g83|mk99|vx400|t66|d615|d763|sl900|el370|'.
            'mp500|samu4|samu3|vx10|xda_|samu6|samu5|samu7|samu9|a615|b832|'.
            'm881|s920|n210|s700|c-810|_h797|mob-x|sk16d|848b|mowser|s580|'.
            'r800|471x|v120|rim8|c500foma:|160x|x160|480x|x640|t503|w839|'.
            'i250|sprint|w398samr810|m5252|c7100|mt126|x225|s5330|s820|'.
            'htil-g1|fly v71|s302|-x113|novarra|k610i|-three|8325rc|8352rc|'.
            'sanyo|vx54|c888|nx250|n120|mtk |c5588|s710|t880|c5005|i;458x|'.
            'p404i|s210|c5100|s940|teleca|c500|s590|foma|vx8|samsu|vx9|a1000|'.
            '_mms|myx|a700|gu1100|bc831|e300|ems100|me701|me702m-three|sd588|'.
            's800|8325rc|ac831|mw200|brew |d88|htc\/|htc_touch|355x|m50|km100|'.
            'd736|p-9521|telco|sl74|ktouch|m4u\/|me702|8325rc|kddi|phone|lg |'.
            'sonyericsson|samsung|nokia|240x|x320vx10|sony cmd|motorola|'.
            'up.browser|up.link|mmp|symbian|android|tablet|iphone|ipad|mobile|smartphone|j2me|wap|vodafone|o2|'.
            'pocket|kindle|mobile|psp|treo)/', $user_agent)) {
            return true; // Возвращает 3 если мобильный браузер определен по сигнатуре User Agent
        }

        if (in_array(substr($user_agent,0,4),
            Array("1207", "3gso", "4thp", "501i", "502i", "503i", "504i", "505i", "506i",
                "6310", "6590", "770s", "802s", "a wa", "abac", "acer", "acoo", "acs-",
                "aiko", "airn", "alav", "alco", "alca", "amoi", "anex", "anyw", "anny",
                "aptu", "arch", "asus", "aste", "argo", "attw", "au-m", "audi", "aur ",
                "aus ", "avan", "beck", "bell", "benq", "bilb", "bird", "blac", "blaz",
                "brew", "brvw", "bumb", "bw-n", "bw-u", "c55/", "capi", "ccwa", "cdm-",
                "cell", "chtm", "cldc", "cmd-", "dmob", "cond", "craw", "dait", "dall", "dang",
                "dbte", "dc-s", "devi", "dica", "doco", "dopo", "ds-d", "ds12",
                "el49", "elai", "eml2", "emul", "eric", "erk0", "esl8", "ez40", "ez60",
                "ez70", "ezos", "ezwa", "ezze", "fake", "fetc", "fly-", "fly_", "g-mo",
                "g1 u", "g560", "gene", "gf-5", "go.w", "good", "grad", "grun", "haie",
                "hcit", "hd-m", "hd-p", "hd-t", "hei-", "hiba", "hipt", "hita", "hp i",
                "hpip", "hs-c", "htc ", "htc-", "htc_", "htca", "htcg", "htcp", "htcs",
                "htct", "http", "hutc", "huaw", "i-20", "i-go", "i-ma", "i230", "iac",
                "iac-", "iac/", "ibro", "idea", "ig01", "ikom", "im1k", "inno", "ipaq",
                "iris", "jata", "java", "jbro", "jemu", "jigs", "kddi", "keji", "kgt",
                "kgt/", "klon", "kpt ", "kwc-", "kyoc", "kyok", "leno", "lexi", "lg g",
                "lg-a", "lg-b", "lg-c", "lg-d", "lg-f", "lg-g", "lg-k", "lg-l", "lg-m",
                "lg-o", "lg-p", "lg-s", "lg-t", "lg-u", "lg-w", "lg/k", "lg/l", "lg/u",
                "lg50", "lg54", "lge-", "lge/", "libw", "lynx", "m-cr", "m1-w", "m3ga",
                "m50/", "mate", "maui", "maxo", "mc01", "mc21", "mcca", "medi", "merc",
                "meri", "midp", "mio8", "mioa", "mits", "mmef", "mo01", "mo02", "mobi",
                "mode", "modo", "mot ", "mot-", "moto", "motv", "mozz", "mt50", "mtp1",
                "mtv ", "mwbp", "mywa", "n100", "n101", "n102", "n202", "n203", "n300",
                "n302", "n500", "n502", "n505", "n700", "n701", "n710", "nec-", "nem-",
                "neon", "netf", "newg", "newt", "nok6", "noki", "nzph", "o2 x", "o2-x",
                "o2im", "opti", "opwv", "oran", "owg1", "p800", "palm", "pana", "pand",
                "pant", "pdxg", "pg-1", "pg-2", "pg-3", "pg-6", "pg-8", "pg-c", "pg13",
                "phil", "pire", "play", "pluc", "pn-2", "pock", "port", "pose", "prox",
                "psio", "pt-g", "qa-a", "qc-2", "qc-3", "qc-5", "qc-7", "qc07", "qc12",
                "qc21", "qc32", "qc60", "qci-", "qtek", "qwap", "r380", "r600", "raks",
                "rim9", "rove", "rozo", "s55/", "sage", "sama", "sams", "samm", "sany",
                "sava", "sc01", "sch-", "scoo", "scp-", "sdk/", "se47", "sec-", "sec0",
                "sec1", "semc", "send", "seri", "sgh-", "shar", "sie-", "siem", "sk-0",
                "sl45", "slid", "smal", "smar", "smb3", "smit", "smt5", "soft", "sony",
                "sp01", "sph-", "spv ", "spv-", "sy01", "symb", "t-mo", "t218", "t250",
                "t600", "t610", "t618", "tagt", "talk", "tcl-", "tdg-", "teli", "telm",
                "tim-", "topl", "treo", "tosh", "ts70", "tsm-", "tsm3", "tsm5", "tx-9",
                "up.b", "upg1", "upsi", "utst", "v400", "v750", "veri", "virg", "vite",
                "vk-v", "vk40", "vk50", "vk53", "vk52", "vm40", "vulc", "voda", "vx52",
                "vx53", "vx60", "vx61", "vx70", "vx80", "vx81", "vx83", "vx85", "vx98",
                "w3c ", "w3c-", "wap-", "wapa", "wapi", "wapj", "wapp", "wapm", "wapr",
          "waps", "wapt", "wapu", "wapv", "wapy", "webc", "whit", "wig ", "winc",
          "winw", "wmlb", "wonu", "x700", "xda-", "xdag", "xda2", "yas-", "your",
          "zeto", "zte-"))) {
            return true; // Возвращает 4 если мобильный браузер определен по сигнатуре User Agent
        }

  return false; // Возвращает false если мобильный браузер не определен или браузер стационарный
}
    /**
     * Получить Id всех дочерних разделов
     *
     * @param int $id
     * @param boolean $active  true - только опубликованные
     * @return array
     */
    function getChildIds($id, $active = true)
    {   global $db1;

        $id = (int)$id;
        $query = "SELECT CH.id FROM ".$this->table_prefix."_catalog_categories AS P, ";
        $query .= $this->table_prefix."_catalog_categories AS CH";
        $query .= " WHERE P.id=".$id." AND CH.cleft >= P.cleft AND CH.cright <= P.cright";
        $query .= $active ? " AND CH.active" : "";
        $ret = array();
        $db1->query($query);
        while ($db1->next_record()) {
            $ret[] = $db1->f('id');
        }

        return $ret;
    }

    /**
     * Кол-во товаров в катагории и всех её подкатегориях
     *
     * @param int $id
     * @param boolean $active true - только опубликованные каткгории и товары
     * @return int
     */
    function getProdCount($id, $active = true)
    {   global $db1;

        $id = (int)$id;
        $child_ids = $this->getChildIds($id, $active);
        $query = 'SELECT COUNT(*) AS cnt FROM '.$this->table_prefix.'_catalog_products';
        $query .= ' WHERE category_id IN ('.$id;
        $query .= (count($child_ids) ? ', '.implode(', ', $child_ids).')' : ')').($active ? " AND active" : "");
        $db1->query($query);
        if ($db1->next_record()) {
            return $db1->f('cnt');
        }

        return 0;
    }

    function getProdsCount($ids){
        global $server,$lang;
        $query = 'SELECT COUNT(id) as count FROM '.$server.$lang.'_catalog_products WHERE archive='.$this->archive.' AND category_id IN ('.implode(',', $ids).') AND active=1 '.($_REQUEST['new_s']&&false ? ' AND price_1>0 AND product_availability=\'1\'' : '');
        //echo $query.'<br>';
        if(!$result = mysql_query($query)) die(mysql_error());
        $row = mysql_fetch_assoc($result);
        return $row['count'];
    }
    function showCtgList($ctg_list, $show_count = false, $id){
        global $tpl, $CONFIG, $PAGE;

        if (is_array($ctg_list)) {
            $tpl->newBlock('block_categories');
            $tpl->assign(array('id'=>$id));
            foreach ($ctg_list as $key => $val) {
                if($show_count) $count = $this->getProdsCount(getAllCats($val['ctg_id']));
                else $count = 0;

                if(!$count && $PAGE['id'] == 300) continue;
                //if(!$count) continue;

                $tpl->newBlock('block_category_row');

                $tpl->assign(

                    array(
                        'ctg_id'    => $val['ctg_id'],
                        'ctg_link'  => $val['ctg_link'],
                        'ctg_name'  => $val['ctg_name'],
                        'ctg_descr' => $val['ctg_descr'],
                        'ctg_img'   => $val['ctg_img'],
                        'ctg_alias'   => $val['lias'],
                        'prod_cnt'   => $val['prod_cnt'],
                        'count' => $count ? $count : 'группа' ,
                    )
                );
                if ($val['ctg_img'] && file_exists(RP.$CONFIG['catalog_ctg_img_path'].$val['ctg_img'])) {
                    $tpl->newBlock('block_category_img');
                    $tpl->assign(
                        array(
                            'ctg_id' => $val['ctg_id'],
                            'ctg_link' => $val['ctg_link'],
                            'ctg_name' => $val['ctg_name'],
                            'img' => "/".$CONFIG['catalog_ctg_img_path'].$val['ctg_img']
                        )
                    );
                }

                if ($val['prod_cnt']) {
                    $tpl->newBlock('block_prod_cnt');
                    $tpl->assign('cnt', $val['prod_cnt']);
                }
            }
            return true;
        }
    }

    function showLeadersList($list)
    {
        global $tpl, $CONFIG, $lang;

        if (is_array($list)) {
            $tpl->newBlock('block_leaders');
            $shop_link = Core::formPageLink($CONFIG['shop_page_link'],$CONFIG['shop_page_address'], $lang, 1);
            foreach ($list as $k => $prod_info) {
                $tpl->newBlock('block_leader_row');
                $prod_info['add_to_cart_action'] = $shop_link.($CONFIG['rewrite_mod'] ? '' : '&').'action=addtocart';
                $tpl->assign($prod_info);
                if (!is_array($_SESSION["compare_prod"])
                    || !in_array($prod_info["prod_id"], $_SESSION["compare_prod"])
                ) {
                    $tpl->newBlock('block_leader_compare_elements');
                    $tpl->assign("prod_id", $prod_info["prod_id"]);
                } else {
                    $tpl->newBlock('block_leader_compare_elements_no');
                }
                if(intval($prod_info['prod_price'])>0){
                    $tpl->newBlock('block_leader_price');
                    $tpl->assign(array(
                        'prod_price'  => $prod_info['prod_price'],
                        'prod_currency' => $prod_info['prod_currency']
                    ));
                } else {
                    $tpl->newBlock('block_leader_noprice');
                    $tpl->assign(array(
                        'prod_price'  => $prod_info['prod_price']
                    ));
                }
                if ($prod_info["prod_image"]) {
                    $tpl->newBlock('block_leader_img');
                    $tpl->assign(
                        array(
                            "img"	=> $prod_info["prod_image"],
                            "prod_title"	=> htmlspecialchars($prod_info["prod_title"]),
                            "prod_link"	=> $prod_info["prod_link"],
                        ));
                } else {
                    $tpl->newBlock('block_leader_img_no');
                }

                if ($prod_info["prod_image_big"]) {
                    $tpl->newBlock('block_leader_img_big');
                    $tpl->assign(
                        array(
                            "img"	=> $prod_info["prod_image_big"],
                            "prod_title"	=> htmlspecialchars($prod_info["prod_title"]),
                            "prod_link"	=> $prod_info["prod_link"],
                        ));
                } else {
                    $tpl->newBlock('block_leader_img_big_no');
                }

                if ($prod_info["prod_ptype_link"]) {
                    $tpl->newBlock('block_leader_ptype'.((strtolower(substr($prod_info["prod_ptype_link"], 0, 7)) === "http://") ? '' : '_local').'_link');
                    $tpl->assign(array(
                        'ptype_name'	=> $prod_info["prod_ptype_name"],
                        'ptype_link'	=> $prod_info["prod_ptype_link"],
                    ));
                } else if ($prod_info["prod_ptype_name"]) {
                    $tpl->newBlock('block_leader_ptype_without_link');
                    $tpl->assign('ptype_name', $prod_info["prod_ptype_name"]);
                }
                if ($prod_info["prod_availability"]) {
                    $tpl->newBlock('block_leader_availability');
                    $tpl->assign("availability", $prod_info["prod_availability"]);
                }
                if ($prod_info['ord_prop_val']) {
                    $this->showProdOrderPropList($prod_info['ord_prop_val']);
                }
            }
        }
    }
    function isCompared($id){
        if(isset($_SESSION['compare']))
            foreach($_SESSION['compare'] as $v)
                if($v == $id) return true;
        return false;
    }
    function cutText($text){
        $arr = explode(' ', $text);
        if(sizeof($arr) < 24) return $text;
        $arr = array_slice($arr, 0, 23);
        return implode(' ', $arr).'...';
    }
    function showProdList($list, $is_search = 0)
    {
        global $tpl, $CONFIG, $lang, $baseurl, $PAGE;

        if (is_array($list) && !empty($list)) {
            $tpl->newBlock('block_products');
            $display = ( !empty ( $_SESSION['compare'] ) ) ? 'display: block;' : 'display: none;';
            $tpl->assign ('display', $display);
            $shop_link = Core::formPageLink($CONFIG['shop_page_link'],$CONFIG['shop_page_address'], $lang, 1);
            $i = 0;
            $j = 1;
            foreach ($list as $k => $prod_info) {
                if (! $prod_info['prod_price']) {
                    $prod_info['prod_price'] = $CONFIG['catalog_default_price'];
                }
                if (! $prod_info['prod_price_1']) {
                    $prod_info['prod_price_1'] = $CONFIG['catalog_default_price'];
                }
                if (! $prod_info['prod_price_2']) {
                    $prod_info['prod_price_2'] = $CONFIG['catalog_default_price'];
                }
                if (! $prod_info['prod_price_3']) {
                    $prod_info['prod_price_3'] = $CONFIG['catalog_default_price'];
                }
                if (! $prod_info['prod_price_4']) {
                    $prod_info['prod_price_4'] = $CONFIG['catalog_default_price'];
                }
                if (! $prod_info['prod_price_5']) {
                    $prod_info['prod_price_5'] = $CONFIG['catalog_default_price'];
                }
                if($i == 0) $prod_info['class'] = 'first';
                if($i == 1) $prod_info['class'] = 'second';
                if($i == 2) $prod_info['class'] = 'third';
                $prod_info['add_to_cart_action'] = $shop_link.($CONFIG['rewrite_mod'] ? '' : '&').'action=addtocart';
                $tpl->newBlock('block_product_row');
                if( $this->getProductProperty($prod_info["prod_id"],1172) == "Рубит кубиками")
                    $kub_disp = 'block';
                else
                    $kub_disp = 'none';

                if( $this->getProductProperty($prod_info["prod_id"],1172) == "Инвертор")
                    $inv_disp = 'block';
                else
                    $inv_disp = 'none';

                $tpl->assign('inv_disp', $inv_disp);
                $tpl->assign('kub_disp', $kub_disp);

                if($this->isCompared($prod_info["prod_id"])) $prod_info['checked'] = 'checked';
                // var_dump($prod_info);  
                if($is_search){
                    $this->get_nav_path($prod_info['category_id'], $baseurl);
                    $path = array();
                    $path[] = '<a href="'.$baseurl.'">'.$PAGE['title'].'</a>';
                    foreach($this->addition_to_path as $k => $v) $path[] = '<a href="'.$v.'">'.$k.'</a>';
                    $prod_info['path'] = implode(' » ', $path);
                    $this->addition_to_path = array();
                }
                $this->addition_to_path = array();
                $prod_info['description'] = $this->cutText(strip_tags($prod_info['description']));

                $prod_info['categ_title'] = $prod_info['prod_descr'] ? $prod_info['prod_descr'] : $prod_info['categ_title'];

                $tpl->assign($prod_info);

                if($this->show_diog){
                    $diog = $this->getProductProperty($prod_info["prod_id"], 359);
                    $tpl->newBlock('diog');
                    $tpl->assign('value', $diog);
                }

                if (!is_array($_SESSION["compare_prod"])
                    || !in_array($prod_info["prod_id"], $_SESSION["compare_prod"])
                ) {
                    $tpl->newBlock('block_compare_elements');
                    $tpl->assign("prod_id", $prod_info["prod_id"]);
                } else {
                    $tpl->newBlock('block_compare_elements_no');
                }
                if ($prod_info["prod_image"]) {
                    $tpl->newBlock('block_product_img');
                    $tpl->assign(
                        array(
                            "img"		=> $prod_info["prod_image"],
                            "prod_title"	=> htmlspecialchars($prod_info["prod_title"]),
                            "prod_link"	=> $prod_info["prod_link"],
                        )
                    );
                } else {
                    $tpl->newBlock('block_product_img_no');
                }
                if ($prod_info["prod_image_big"]) {
                    $tpl->newBlock('block_product_img_big');
                    $tpl->assign(
                        array(
                            "img"		=> $prod_info["prod_image_big"],
                            "prod_title"	=> htmlspecialchars($prod_info["prod_title"]),
                            "prod_link"	=> $prod_info["prod_link"],
                        )
                    );
                } else {
                    $tpl->newBlock('block_product_img_big_no');
                }

                if ($prod_info["ptype_link"]) {
                    $tpl->newBlock('block_product_ptype'.((strtolower(substr($prod_info["ptype_link"], 0, 7)) === "http://") ? '' : '_local').'_link');
                    $tpl->assign(array(
                        'ptype_name'	=> $prod_info["ptype_name"],
                        'ptype_link'	=> $prod_info["ptype_link"],
                    ));
                } else if ($prod_info["ptype_name"]) {
                    $tpl->newBlock('block_product_ptype_without_link');
                    $tpl->assign('ptype_name', $prod_info["ptype_name"]);
                }
                if ($prod_info["prod_code"]) {
                    $tpl->newBlock('block_product_code');
                    $tpl->assign("prod_code", $prod_info["prod_code"]);
                }
                if ($prod_info["prod_availability"]) {
                    $tpl->newBlock('block_product_availability');
                    $tpl->assign("prod_availability", $prod_info["prod_availability"]);
                }

                if ($prod_info["prod_novelty"]) {
                    $tpl->newBlock('new');
                }
                if ($prod_info["bonus"]) {
                    $tpl->newBlock('bonus');
                    $tpl->assign("bonus",$prod_info["bonus"]);
                }
                if ($prod_info["archive"]) {
                    $tpl->newBlock('archive');
                }

                if(intval($prod_info['prod_price'])>0){
                    $tpl->newBlock('block_product_price');
                    $tpl->assign(array(
                        'prod_price'  => $prod_info['prod_price'],
                        'prod_currency' => $prod_info['prod_currency']
                    ));
                } else {
                    $tpl->newBlock('block_product_noprice');
                    $tpl->assign(array(
                        'prod_price'  => $prod_info['prod_price']
                    ));
                }
                if ($prod_info['ord_prop_val']) {
                    $this->showProdOrderPropList($prod_info['ord_prop_val']);
                }
   
                if($prod_info['leader']){
                    //echo "hh";
                    $tpl->newBlock('lead');
                }
                if($prod_info['date']){
                    $arr = explode('.',$prod_info['date']);
                    $date_t= strtotime($arr[2].'-'.$arr[1].'-'.$arr[0].' 23:59:59');
                    if(time() < $date_t) $tpl->newBlock('new');

                }
                if((int)$prod_info['prod_price_1'] == 0 || $prod_info["prod_availability"] == '0'){

                    $tpl->newBlock('price_no');
                    $tpl->assign('link', $prod_info['prod_link']);
                    $tpl->assign('price', $prod_info['prod_price_1']);
                }elseif($prod_info['prod_price_2'] == 0){

                    $tpl->newBlock('price');
                    $tpl->assign('link', $prod_info['prod_link']);
                    $tpl->assign('price', $prod_info['prod_price_1']);
                }else{
                    $tpl->newBlock('percent');
                    $tpl->newBlock('dprice');
                    $tpl->assign('link', $prod_info['prod_link']);
                    $tpl->assign('price', $prod_info['prod_price_1']);
                    $tpl->assign('percent', calcDiscount($prod_info['prod_price_1'], $prod_info['prod_price_2']));
                }
                $this->showGift($prod_info["prod_id"]);
                if($i++ == 2 ){ $tpl->newBlock('delim'); $i = 0; }

            }
        }
    }
    function prodExists($prod_alias, $ctgr_alias)
    {   global $db1;

        $db1->query("select P.id, if(P.alias is not null and P.alias!='',LCASE(P.alias),P.id) AS prod_alias, "
            ."if(C.alias is not null and C.alias!='',LCASE(C.alias),C.id) AS ctgr_alias "
            ."FROM`{$this->table_prefix}_catalog_products` P INNER JOIN "
            ."`{$this->table_prefix}_catalog_categories` C ON P.category_id=C.id "
            ."HAVING prod_alias='".My_Sql::escape(strtolower($prod_alias))
            ."' AND ctgr_alias='".My_Sql::escape(strtolower($ctgr_alias))."'");
        return $db1->next_record() ? $db1->f('id') : false;
    }
    function ctgrExists($ctgr_alias)
    {   global $db1;

        $db1->query("select id, if(alias is not null and alias!='', alias, id) AS alias "
            ."FROM`{$this->table_prefix}_catalog_categories` "
            ."HAVING LCASE(alias)='".My_Sql::escape(strtolower($ctgr_alias))."'");
        return $db1->next_record() ? $db1->f('id') : false;
    }
    function getProdLink($transurl, $prod_alias, $prod_id, $ctgr_alias)
    {   global $CONFIG;

        return CatalogPrototype::prepareUrl($transurl).($CONFIG['rewrite_mod']
            ? "{$ctgr_alias}/{$prod_alias}.html"
            : "action=shwprd&id={$prod_id}");
    }
    function getCtgrLink($transurl, $ctgr_alias, $ctgr_id)
    {   global $CONFIG;
        return CatalogPrototype::prepareUrl($transurl).($CONFIG['rewrite_mod']
            ? "{$ctgr_alias}/"
            : "action=assortment&id={$ctgr_id}");
    }
    function prepareUrl($url)
    {   global $CONFIG;
        $ret = '';
        $i = parse_url($url);
        if ($i) {
            if ($CONFIG['rewrite_mod']) {
                $ret = $i['path'].("/" == substr($i['path'], -1) ? "" : "/");
            } else {
                $ret = $i['path'].($i['query'] ? "?{$i['query']}&" : "");
            }
        }
        return $ret;
    }
    function check_path($dir){
        if ('/' == substr($dir, -1)) {
            $dir = substr($dir, 0, -1);
        }
        if(is_dir(RP.$dir)) return true;
        $prev = substr($dir, 0, strrpos($dir, '/'));
        if(CatalogPrototype::check_path($prev)){@mkdir(RP.$dir, 0777);}
    }
    function check_image(&$from, &$to, $id = null)
    {
        if (!function_exists('img_resize')) {
            require_once(RP.'inc/img.php');
        }
        global $CONFIG; static $types;
        if (!$types) {
            $types = array(
                'image/jpeg' => 'jpg',
                'image/gif'  => 'gif',
                'image/x-png'=> 'png',
            );
        }
        if (empty($from) || !is_file(RP.$CONFIG['catalog_img_path'].$from)) {
            $from=NULL; return false;
        }
        if (!empty($to) && is_file(RP.$CONFIG['catalog_img_path'].$to)) {
            return true;
        }
        $type = getimagesize(RP.$CONFIG['catalog_img_path'].$from);
        if (!$type || !$types[($type['mime'])]) {
            return false;
        }

        $img = (!empty($to)) ? $to : sprintf('%s_%s.%s',$id ? $id : uniqid(''), 'middle', $types[($type['mime'])]);
        $IMG = array (
            'old_path' => RP.$CONFIG['catalog_img_path'].$from,
            'new_path' => RP.$CONFIG['catalog_img_path'].$img,
            'width'    => $CONFIG['catalog_max_prev_width'],
            'height'   => $CONFIG['catalog_max_prev_height'],
#			'resize'   => "fixed",
            'bg_color' => "#ffffff",
            'extExists'	=> true,
        );
        $to = img_resize($IMG) ? $img : NULL;
        return true;
    }
    function translit($STRING){
        $russ=array(
            "ё"=>"yo", "Ё"=>"yo", "й"=>"y",  "Й"=>"y",  "ц"=>"z",  "Ц"=>"z",  "у"=>"u", "У"=>"u",
            "к"=>"k",  "К"=>"k",  "е"=>"e",  "Е"=>"e",  "н"=>"n",  "Н"=>"n",  "г"=>"g", "Г"=>"g",
            "ш"=>"sh", "Ш"=>"sh", "щ"=>"sh", "Щ"=>"sh", "з"=>"z",  "З"=>"z",  "х"=>"h", "Х"=>"h",
            "ъ"=>"`",  "Ъ"=>"`",  "ф"=>"f",  "Ф"=>"f",  "ы"=>"i",  "Ы"=>"i",  "в"=>"v", "В"=>"v",
            "а"=>"a",  "А"=>"a",  "п"=>"p",  "П"=>"p",  "р"=>"r",  "Р"=>"r",	 "о"=>"o", "О"=>"o",
            "л"=>"l",  "Л"=>"l",  "д"=>"d",  "Д"=>"d",  "ж"=>"zh", "Ж"=>"zh", "э"=>"e", "Э"=>"e",
            "я"=>"ya", "Я"=>"ya", "ч"=>"ch", "Ч"=>"ch", "с"=>"s",  "С"=>"s", "м"=>"m", "М"=>"m",
            "и"=>"i",  "п"=>"i",  "т"=>"t",  "Т"=>"t",  "ь"=>"",   "Ь"=>"",	 "б"=>"b", "Б"=>"b",
            "ю"=>"yu", "Ю"=>"yu", " "=>"_"
        );
        $text = strtolower (strtr ($STRING, $russ));
        return ereg_replace("[^A-Za-z0-9\._\-]", "", $text);
    }
    function UTF8toCP1251($str){
        static $table = array("\xD0\x81" => "\xA8", // Ё
            "\xD1\x91" => "\xB8", // ё
        );
        return preg_replace(
            '#([\xD0-\xD1])([\x80-\xBF])#se',
            'isset($table["$0"]) ? $table["$0"] : chr(ord("$2")+("$1" == "\xD0" ? 0x30 : 0x70))',
            $str
        );
    }
    function delOldImg(){
        global $db,$CONFIG;
        $db->query("select image_middle,image_big from {$this->table_prefix}_catalog_products");
        if(!$db->nf()){return false;}
        while($db->next_record()){
            if(!empty($db->Record[0])) $img[] = $db->Record[0];
            if(!empty($db->Record[1])) $img[] = $db->Record[1];
        }
        $imgs = glob(RP.$CONFIG['catalog_img_path'].'*.*');$imgs = array_map('basename',$imgs);
        $dels = array_diff($imgs , $img);
        foreach($dels as $v){$f = realpath(RP.$CONFIG['catalog_img_path'].$v);if(is_file($f)){@unlink($f);}}
    }

    function getSubCtgrIds($id)
    {   global $db;
        $ret = false;
        $db->query("SELECT S.id FROM {$this->table_prefix}_catalog_categories AS P INNER JOIN "
            .$this->table_prefix."_catalog_categories AS S ON S.cleft BETWEEN P.cleft AND P.cright"
            ." WHERE P.id=".intval($id));
        while ($db->next_record()) {
            $ret[] = $db->Record['id'];
        }

        return $ret;
    }

    /**
     * Функция получает значения курса валют с сайта ЦБ РФ и выдает нужные параметры в виде массива
     *
     * @param string $xml_file_url - URL адрес страницы
     * @return mixed массив с данными или false
     */
    function get_central_bank_rates($xml_file_url)
    {
        // Загрузим XML валют.
        $content = file_get_contents($xml_file_url);
        //$content = iconv('windows-1251', 'UTF-8', $content);

        $xml = domxml_open_mem($content);
        if (!$xml)
        {
            return false;
        }

        // Получаем верхний узел.
        $child_nodes = $xml->child_nodes();
        if (count($child_nodes) == 0)
        {
            return false;
        }
        $xml = $child_nodes[0];

        // Получим список узлов валют.
        $child_nodes = $xml->child_nodes();

        if (!$child_nodes)
        {
            return false;
        }

        $result = array();


        // Ищем интересующие нас курсы.
        foreach ($child_nodes as $xml)
        {
            $sub_nodes = $xml->child_nodes();

            //print_r($sub_nodes);

            if (!$sub_nodes)
            {
                continue;
            }

            // Ищем название валюты.
            $currency_name = '';
            foreach ($sub_nodes as $node)
            {
                $node_name = trim(strtolower($node->node_name()));
                if ($node_name != 'charcode')
                {
                    continue;
                }

                // Извлекаем значение.
                $value_nodes = $node->child_nodes();
                foreach ($value_nodes as $value_node)
                {
                    if ($value_node->node_type() == XML_TEXT_NODE)
                    {
                        $currency_name .= trim(strtolower($value_node->node_value()));
                    }
                }
            }

            if (empty($currency_name))
            {
                continue;
            }

            $value = '';

            reset($sub_nodes);
            foreach ($sub_nodes as $node)
            {
                $node_name = strtolower($node->node_name());
                if ($node_name != 'value')
                {
                    continue;
                }

                // Извлекаем значение.
                $value_nodes = $node->child_nodes();
                foreach ($value_nodes as $value_node)
                {
                    if ($value_node->node_type() == XML_TEXT_NODE)
                    {
                        $value .= strtolower($value_node->node_value());
                    }
                }
            }

            // Заменяем запятую на точку и преобразуем в вещественное число.
            $value = str_replace(',', '.', $value);
            $value = floatval($value);
            if (!$value)
            {
                continue;
            }

            // Добавим валюту.
            switch ($currency_name)
            {
                case 'usd':
                {
                    $result['USD/RUR'] = $value;
                    break;
                }
                case 'eur':
                {
                    $result['EUR/RUR'] = $value;
                }
            }

        }

        if (count($result))
        {
            // Произведем дополнительные расчеты между иностранными валютами.
            if (isset($result['USD/RUR']) && isset($result['EUR/RUR']))
            {
                $result['EUR/USD'] = round($result['EUR/RUR'] / $result['USD/RUR'], 4);
            }

            return $result;
        }
        else
        {
            return false;
        }
    }

    function getCommentableItem($id)
    {   global $db;

        $ret = false;
        $db->query("SELECT P.id, product_title, category_id, C.cleft, C.cright FROM ".$this->table_prefix."_catalog_products P INNER JOIN "
            .$this->table_prefix."_catalog_categories C ON category_id=C.id WHERE P.id=".intval($id));
        if ($db->next_record()) {
            $title = $db->Record['product_title'];
            $db->query("SELECT commentable FROM {$this->table_prefix}_catalog_categories "
                ."WHERE cleft<={$db->Record['cleft']} AND cright>={$db->Record['cright']} "
                ."AND commentable>0 ORDER BY cright LIMIT 1");
            if ($db->next_record() &&
                (1 == $db->Record['commentable'] || (2 == $db->Record['commentable'] && $_SESSION['siteuser']['id'] > 0) )
            ) {
                $ret = array('id' => $id, 'title' => $title);
            }
        }

        return $ret;

    }

    function addAdditionalImgs($prod_id, $copy = false, $del_img = false, $img_rank = array(),$img_desc = array())
    {   global $CONFIG, $db;
        $prod_id = (int) $prod_id;
        if ($prod_id) {
            $img_cnt = true;
            if ($CONFIG['catalog_add_img_cnt'] > 0) {
                $db->query("SELECT COUNT(*) FROM {$this->table_prefix}_catalog_img WHERE prod_id={$prod_id}");
                if ($db->next_record()) {
                    /* Сколько картинок еще можно закачать */
                    $img_cnt = $CONFIG['catalog_add_img_cnt'] - intval($db->Record[0]);
                }
            }
            $img_num = 0;
            foreach (glob(RP.$CONFIG['catalog_add_img_path'].$prod_id."-*.*") as $filename) {
                $num = (int) substr(strstr($filename, "-"), 1);
                if ($num >= $img_num) {
                    $img_num = ++$num;
                }
            }
            $upload = array();
            CatalogPrototype::check_path($CONFIG['catalog_add_img_path']);
            CatalogPrototype::check_path($CONFIG['catalog_add_prev_path']);
            while ($img_cnt > 0 && (list($key, $val) = each($_FILES['img']['tmp_name']))) {
                $ext = CatalogPrototype::getFileExt($_FILES['img']['name'][$key]);
                if ($val && $ext && in_array($ext, $this->img_exts)) {
                    $img_name = "{$prod_id}-{$img_num}.{$ext}";
                    $upload_img = false;
                    if ($CONFIG['catalog_max_img_height']>0 || $CONFIG['catalog_max_img_width']>0) {
                        if (Main::generateImage(
                            $val,
                            RP.$CONFIG['catalog_add_img_path'].$img_name,
                            array('width' => $CONFIG['catalog_max_img_width'], 'height' => $CONFIG['catalog_max_img_height'])
                        )
                        ) {
                            $upload_img = true;
                        }
                    } else {
                        if (move_uploaded_file($val, RP.$CONFIG['catalog_add_img_path'].$img_name)) {
                            $upload_img = true;
                            if ($img_cnt && is_int($img_cnt)) {
                                $img_cnt--;
                            }
                        }
                    }
                    $upload_prev = false;
                    if ($upload_img) {
                        $img_num++;
                        if ($_FILES['prev']['tmp_name'][$key]) {
                            $ext = CatalogPrototype::getFileExt($_FILES['prev']['name'][$key]);
                            $prev_name = "{$prod_id}-{$img_num}.{$ext}";
                            if (Main::generateImage(
                                $_FILES['prev']['tmp_name'][$key],
                                RP.$CONFIG['catalog_add_prev_path'].$prev_name,
                                array('width' => $CONFIG['catalog_max_prev_width'], 'height' => $CONFIG['catalog_max_prev_height'])
                            )
                            ) {
                                $upload_prev = true;
                            }
                        } else {
                            $prev_name = $img_name;
                            if (Main::generateImage(
                                RP.$CONFIG['catalog_add_img_path'].$img_name,
                                RP.$CONFIG['catalog_add_prev_path'].$prev_name,
                                array('width' => $CONFIG['catalog_max_prev_width'], 'height' => $CONFIG['catalog_max_prev_height'])
                            )
                            ) {
                                $upload_prev = true;
                            }
                        }
                        $upload[] = "({$prod_id}, '{$img_name}', '".($upload_prev ? $prev_name : '')."', "
                            .(isset($img_rank[$key]) ? (int)$img_rank[$key] : 0 ).","
                            .(isset($img_desc[$key]) ? "$img_desc[$key]" : "''" ).")";
                    }
                }
            }

            if ($copy) {
                foreach ($_REQUEST['copy_img'] as $key => $val) {
                    if (!$del_img || ($del_img && !in_array($key, $del_img)) ) {
                        $img_num++;
                        $prev_name = $img_name = '';
                        if ($val && file_exists(RP.$CONFIG['catalog_add_img_path'].$val)) {
                            $ext = CatalogPrototype::getFileExt($val);
                            $img_name = "{$prod_id}-{$img_num}.{$ext}";
                            if (!$ext ||
                                !copy(RP.$CONFIG['catalog_add_img_path'].$val, RP.$CONFIG['catalog_add_img_path'].$img_name)
                            ) {
                                $img_name = '';
                            }
                        }
                        if ($_REQUEST['copy_prev'][$key] && file_exists(RP.$CONFIG['catalog_add_prev_path'].$_REQUEST['copy_prev'][$key])) {
                            $ext = CatalogPrototype::getFileExt(RP.$CONFIG['catalog_add_prev_path'].$_REQUEST['copy_prev'][$key]);
                            $prev_name = "{$prod_id}-{$img_num}.{$ext}";
                            if (!$ext ||
                                !copy(RP.$CONFIG['catalog_add_prev_path'].$_REQUEST['copy_prev'][$key], RP.$CONFIG['catalog_add_prev_path'].$prev_name)
                            ) {
                                $prev_name = '';
                            }
                        }
                        if ($prev_name || $img_name) {
                            $upload[] = "({$prod_id}, '{$img_name}', '{$prev_name}', 0,'')";
                        }
                    }
                }
            }

            if (!empty($upload)) {
                $db->query("INSERT INTO {$this->table_prefix}_catalog_img (prod_id, img, preview, rank, `desc`) VALUES "
                    .implode(", ", $upload));
            }
        }
    }

    function delAdditionalImgs($ids)
    {   global $CONFIG, $db;

        $ids = (array) $ids;
        $ids = array_map('intval', $ids);
        $ret = false;
        if (!empty($ids)) {
            $db->query("SELECT img, preview FROM {$this->table_prefix}_catalog_img WHERE id IN (".implode(", ", $ids).")");
            while ($db->next_record()) {
                if ($db->Record['img']) {
                    @unlink(RP.$CONFIG['catalog_add_img_path'].$db->Record['img']);
                }
                if ($db->Record['preview']) {
                    @unlink(RP.$CONFIG['catalog_add_prev_path'].$db->Record['preview']);
                }
            }
            $db->query("DELETE FROM {$this->table_prefix}_catalog_img WHERE id IN (".implode(", ", $ids).")");
            $ret = $db->affected_rows();
        }

        return $ret;
    }

    function delProdAdditionalImgs($prod_ids)
    {   global $CONFIG, $db;

        $prod_ids = (array) $prod_ids;
        $prod_ids = array_map('intval', $prod_ids);
        $ret = false;
        if (!empty($prod_ids)) {
            $db->query("SELECT img, preview FROM {$this->table_prefix}_catalog_img WHERE prod_id IN (".implode(", ", $prod_ids).")");
            while ($db->next_record()) {
                if ($db->Record['img'] && file_exists(RP.$CONFIG['catalog_add_img_path'].$db->Record['img'])) {
                    @unlink(RP.$CONFIG['catalog_add_img_path'].$db->Record['img']);
                }
                if ($db->Record['preview'] && file_exists(RP.$CONFIG['catalog_add_prev_path'].$db->Record['preview'])) {
                    @unlink(RP.$CONFIG['catalog_add_prev_path'].$db->Record['preview']);
                }
            }
            $db->query("DELETE FROM {$this->table_prefix}_catalog_img WHERE prod_id IN (".implode(", ", $prod_ids).")");
            $ret = $db->affected_rows();
        }

        return $ret;
    }

    function getAdditionalImgs($prod_id)
    {   global $CONFIG, $db;
        $ret = false;
        $db->query("SELECT id, img, preview, rank, `desc` FROM {$this->table_prefix}_catalog_img WHERE prod_id=".intval($prod_id)." ORDER BY rank, img");
        while ($db->next_record()) {
            $ret[] = array(
                'id' => $db->Record['id'],
                'img' => $db->Record['img'],
                'rank' => $db->Record['rank'],
                'preview' => $db->Record['preview'],
                'img_path' => $db->Record['img'] && file_exists(RP.$CONFIG['catalog_add_img_path'].$db->Record['img'])
                        ? "/".$CONFIG['catalog_add_img_path'].$db->Record['img']
                        : $CONFIG['catalog_def_img_path'],
                'preview_path' => $db->Record['preview'] && file_exists(RP.$CONFIG['catalog_add_prev_path'].$db->Record['preview'])
                        ? "/".$CONFIG['catalog_add_prev_path'].$db->Record['preview']
                        : $CONFIG['catalog_def_prev_path'],
                'img_exists' => $db->Record['img'] && file_exists(RP.$CONFIG['catalog_add_img_path'].$db->Record['img']),
                'preview_exists' => $db->Record['preview'] && file_exists(RP.$CONFIG['catalog_add_prev_path'].$db->Record['preview']),
                'desc' => $db->Record['desc'],
            );
            //var_dump($ret);
        }
        return $ret;
    }


    function showAdditionalImgs($prod_id, $main_img = false, $vids = array())
    {   global $tpl, $CONFIG;
        $list = $this->getAdditionalImgs($prod_id);
        $list = $list ? $list : array();
        if ($main_img && is_array($main_img)) {
            $main_img = array(
                'id' => 0,
                'img' => $main_img[1],
                'rank' => 0,
                'preview' => $main_img[0],
                'img_path' => $main_img[1] && file_exists(RP.$CONFIG['catalog_img_path'].$main_img[1])
                        ? "/".$CONFIG['catalog_img_path'].$main_img[1]
                        : $CONFIG['catalog_def_img_path'],
                'preview_path' => $main_img[0] && file_exists(RP.$CONFIG['catalog_img_path'].$main_img[0])
                        ? "/".$CONFIG['catalog_img_path'].$main_img[0]
                        : $CONFIG['catalog_def_prev_path'],
                'img_exists' => $main_img[1] && file_exists(RP.$CONFIG['catalog_img_path'].$main_img[1]),
                'preview_exists' => $main_img[0] && file_exists(RP.$CONFIG['catalog_img_path'].$main_img[0]),
            );
        } else {
            $main_img = false;
        }
        if (! empty($list) || $main_img) {
            if (empty($list) || (! empty($list) && $main_img['img_exists'])) {
                array_unshift($list, $main_img);
            }
            $first = reset($list);
            $tpl->newBlock('block_product_first_img');
            $tpl->assign($first);
            if ($first['preview_exists']) {
                $tpl->newBlock('block_product_first_preview_exists');
            } else {
                $tpl->newBlock('block_product_first_preview_no_exists');
            }
            $tpl->assign($first);
            if ($first['img_exists']) {
                $tpl->newBlock('block_product_first_img_exists');
            } else {
                $tpl->newBlock('block_product_first_img_no_exists');
            }
            $cycle = false;
            $tpl->assign($first);
            if (count($list) > 1 || (!$main_img && count($list) == 1) || sizeof($vids)) {
                $tpl->newBlock('block_product_img_list');
                $tpl->assign(array(
                    'MSG_Big_image' => $this->_msg['Big_image'],
                    'MSG_Middle_image' => $this->_msg['Middle_image'],
                    'MSG_Del' => $this->_msg['Del'],
                    'MSG_Delete' => $this->_msg['Delete'],
                ));
                $i = 0;
                $cycle = true;
                foreach ($list as $val) {

                    $val['MSG_Not_found'] = $this->_msg['MSG_Not_found'];
                    $val['MSG_Delete'] = $this->_msg['Delete'];
                    $val['MSG_Del'] = $this->_msg['Del'];
                    $val['css_selected_class'] = 0 == $i++ ? 'selected' : '';
                    $tpl->newBlock('block_product_img');
                    if($i == 1) $val['active'] = 'active';
                    $tpl->assign($val);
                    if ($val['preview_exists']) {
                        $tpl->newBlock('block_product_preview_exists');
                    } else {
                        $tpl->newBlock('block_product_preview_no_exists');
                    }
                    if($i == 1) $val['active'] = 'active';
                    $tpl->assign($val);
                    if ($val['img_exists']) {
                        $tpl->newBlock('block_product_img_exists');
                    } else {
                        $tpl->newBlock('block_product_img_no_exists');
                    }

                    $tpl->assign($val);
                }
            }
        }
        if (0 == $CONFIG['catalog_add_img_cnt'] || !$list || ($CONFIG['catalog_add_img_cnt'] > 0 && count($list) < $CONFIG['catalog_add_img_cnt'])) {
            $tpl->newBlock('block_product_img_form');
            $tpl->assign(array(
                'MSG_Big_image' => $this->_msg['Big_image'],
                'MSG_Middle_image' => $this->_msg['Middle_image'],
                'MSG_Add_image' => $this->_msg['Add_image'],
                'MSG_Generete_image' => $this->_msg['Generete_image'],
                'MSG_Del' => $this->_msg['Del'],
                'MSG_Delete' => $this->_msg['Delete'],
            ));
            if (count($list) < $CONFIG['catalog_add_img_cnt']-1) {
                $tpl->newBlock('block_product_add_img');
                $tpl->assign(array(
                    'MSG_Add_image' => $this->_msg['Add_image'],
                    'MSG_Generete_image' => $this->_msg['Generete_image'],
                ));
            }
        }
        return $cycle;
    }

    /**
     * Генерация изображения из исходного
     * @version	3.0
     * @access	public
     * @param	string			$source			источник
     * @param	string			$destination	создаваемый файл
     * @param	integer			$w				высота
     * @param	integer			$h				ширина
     * @param	integer			$jpeg_quality	качество jpeg картинки
     * @return	bool
     */
    function generateImage($source, $destination, $w, $h, $jpeg_quality = "100")
    {   global $CONFIG;

        $size = getimagesize($source);
        list($sw, $sh) = $size;
        $w = $w <= 0 ? $sw : $w;
        $h = $h <= 0 ? $sh : $h;
        $filetype = $size["mime"];
        if ($filetype == "image/jpeg" || $filetype == "image/pjpeg" || $filetype == "image/jpg") {
            $src = imagecreatefromjpeg($source);
            $type = 'jpg';
        } else if ($filetype == "image/gif") {
            $src = imagecreatefromgif($source);
            $type = 'gif';
        } else if ($filetype == "image/x-png" || $filetype == 'image/png') {
            $src = imagecreatefrompng($source);
            $type = 'png';
        } else {
            return false;
        }
        $dw = $sw/$w;
        $dh = $sh/$h;
        $d = max($dw, $dh);
        $dw = floor($sw/$d);
        $dh = floor($sh/$d);
        if (! ($dest = imagecreatetruecolor($dw, $dh)) ) {
            return false;
        }
        if ($src && $dest) {
            if ('png' == $type) {
                $trans_index = imagecolortransparent($src);
                $trans_color = array('red' => 255, 'green' => 255, 'blue' => 255);
                if ($transparencyIndex >= 0) {
                    $trans_color = imagecolorsforindex($src, $trans_index);
                }
                $trans_index = imagecolorallocate($dest, $trans_color['red'], $trans_color['green'], $trans_color['blue']);
                imagefill($dest, 0, 0, $trans_index);
                imagecolortransparent($dest, $trans_index);
            } else if ('gif' == $type) {
                $color_count = imagecolorstotal($src);
                imagetruecolortopalette($dest, true, $color_count);
                imagepalettecopy($dest, $src);
                $transparentcolor = imagecolortransparent($src);
                imagefill($dest, 0, 0, $transparentcolor);
                imagecolortransparent($dest, $transparentcolor);
            }
            /*
		    $d = min($dw, $dh);
			imagecopyresized($dest, $src, 
			    0, 0,
			    round(($sw - ($w*$d))/2), round(($sh - ($h*$d))/2),
			    $w, $h,
			    round($w*$d), round($h*$d) );
			*/
            //echo "$source, $destination (0, 0, 0, 0, ".round($sw/$d).", ".round($sh/$d).", $sw, $sh )"; exit;
            imagecopyresampled($dest, $src, 0, 0, 0, 0, $dw, $dh, $sw, $sh );
            if ($filetype == "image/jpeg" || $filetype == "image/pjpeg" || $filetype == "image/jpg") {
                imagejpeg($dest, $destination, $jpeg_quality);
            } else if ($filetype == "image/gif") {
                imagegif($dest, $destination);
            } else if ($filetype == "image/x-png" || $filetype == "image/png") {
                imagepng($dest, $destination);
            }
            @imagedestroy($src);
            @imagedestroy($dest);
            return TRUE;
        }
        return FALSE;
    }

    function showTematikForm($id = 0, $is_add = false)
    {   global $baseurl, $CONFIG,$db,$server,$lang;

        $ret = true;
        $prod = $this->getConcernedProducts(2, $_REQUEST['id'], false, 'rank');

        if ($ret) {
            $GLOBALS['main']->include_main_blocks_2($this->module_name.'_edit_tematik.html', $this->tpl_path);
            $GLOBALS['tpl']->prepare();
            $GLOBALS['tpl']->assign(Array(
                "MSG_GoodID_must_be_positive" => $this->_msg["GoodID_must_be_positive"],
                "MSG_Group_ID_is" => $this->_msg["Group_ID_is"],
                "MSG_Enter_good_name" => $this->_msg["Enter_good_name"],
                "MSG_Set_other_link" => $this->_msg["Set_other_link"],
                "MSG_Add_assigned_goods" => $this->_msg["Add_assigned_goods"],
                "MSG_Add_analogues" => $this->_msg["Add_analogues"],
                "MSG_Analogues" => $this->_msg["Analogues"],
                "MSG_Delete_from_selected" => $this->_msg["Delete_from_selected"],
                "MSG_Added_images" => $this->_msg["Added_images"],
                "MSG_Main_images" => $this->_msg["Main_images"],
                "MSG_Choose_producer" => $this->_msg["Choose_producer"],
                "MSG_Choose_currency" => $this->_msg["Choose_currency"],
                "MSG_Good_code" => $this->_msg["Good_code"],
                "MSG_articul" => $this->_msg["articul"],
                "MSG_Name" => $this->_msg["Name"],
                "MSG_Good_producer" => $this->_msg["Good_producer"],
                "MSG_Select_producer" => $this->_msg["Select_producer"],
                "MSG_Short_description" => $this->_msg["Short_description"],
                "MSG_Full_description" => $this->_msg["Full_description"],
                "MSG_Page_description" => $this->_msg["Page_description"],
                "MSG_Keywords" => $this->_msg["Keywords"],
                "MSG_Currency" => $this->_msg["Currency"],
                "MSG_Select_currency" => $this->_msg["Select_currency"],
                "MSG_Retail_price" => $this->_msg["Retail_price"],
                "MSG_Partner_price" => $this->_msg["Partner_price"],
                "MSG_Dealer_price" => $this->_msg["Dealer_price"],
                "MSG_Old_price" => $this->_msg["Old_price"],
                "MSG_Price" => $this->_msg["Price"],
                "MSG_Presence" => $this->_msg["Presence"],
                "MSG_required_fields" => $this->_msg["required_fields"],
                "MSG_Save" => $this->_msg["Save"],
                "MSG_Cancel" => $this->_msg["Cancel"],
                "MSG_Alias" => $this->_msg["Alias"],
                "MSG_Alias_Info" => $this->_msg["Alias_Info"],
                "MSG_no_value" => $this->_msg["no_value"],
                "MSG_Assigned_goods" => $this->_msg["Assigned_goods"],
                "MSG_Ed" => $this->_msg["Ed"],
                "MSG_Rank" => $this->_msg["Rank"],
                "MSG_Del" => $this->_msg["Del"],
                "MSG_Active" => $this->_msg["Active"],
                "MSG_Try_on" => $this->_msg["Try_on"],
                "MSG_Prod_prop" => $this->_msg["Prod_prop"],
                "MSG_Order_prop" => $this->_msg["Order_prop"],
                "MSG_Middle_image" => $this->_msg["Middle_image"],
                "MSG_Big_image" => $this->_msg["Big_image"],
                "MSG_Generated" => $this->_msg["Generated"],
                "MSG_Delete" => $this->_msg["Delete"],
                "MSG_Edit" => $this->_msg["Edit"],
                "MSG_Del" => $this->_msg["Del"],
                "MSG_Ed" => $this->_msg["Ed"],
                "MSG_Images" => $this->_msg["Images"],
                "MSG_Properties" => $this->_msg["Properties"],
                "MSG_Relations" => $this->_msg["Relations"],
                'baseurl' => $baseurl
            ));
                $this->show_nav_path($_REQUEST['id'], $baseurl);
            $GLOBALS['tpl']->assign(array(
                "referer" => $_SERVER['HTTP_REFERER'],
                "start" => $_REQUEST['start'] >1 ? (int)$_REQUEST['start'] : '',
                "baseurl" => $baseurl,
            ));
            if ($prod) {
                $list1 = $prod;
                $cons_list1 = false;
                $category_tree=array();
                $k=array();
                $id_tree='';
                $id_p_h=array();
                if ($list1) {
                    foreach ($list1 as $val) {
                        $cons_list1[$val['concrn_id']] = array(
                            'id' => $val['concrn_id'],
                            'title' => $val['concrn_title'],
                            'availability' => $val['availability'],
                            'price' => (int)$val['price_1'],
                            'active' => $val['active'] ? $this->_msg['Yes'] : $this->_msg['No'],
                            'rank' => (int)$val['rank']
                        );
                        $k[$val['category_id']]=  $val['categ_title'];
                         if(!in_array($category_tree,$k)) {
                             $category_tree[$val['category_id']]= $val['categ_title'];
                            $parent_id= $this->get_category_owners_p((int)$val['category_id'])  ;
                             foreach($parent_id as $id_p){
                                 if(!in_array($id_p,$id_p_h)){
                                     $id_p_h[]= $id_p  ;
                                      $id_tree=    $id_tree ? $id_tree .=','.$id_p : $id_p ;
                                 }

                             }
                             if(!in_array($val['category_id'],$id_p_h)){
                                 $id_p_h[]= $val['category_id']  ;
                                 $id_tree=    $id_tree ? $id_tree .=','.$val['category_id'] : $val['category_id'] ;
                             }


                         }
                    }
                }
                /*foreach($category_tree as $key=>$val){
                          $ret=$this->get_category_owners($key);
                }     */
               $arr = $db->getArrayOfResult("SELECT id FROM sup_rus_catalog_categories WHERE id NOT IN ($id_tree)");
                $ids_tree_anul=array();
                if($arr)
                foreach($arr as $ids_anul){
                          $ids_tree_anul[]= $ids_anul['id']  ;
                }

                Module::init_json();
                $GLOBALS['tpl']->assign(array(
                    'product_id'				=> $_REQUEST['id'],
                    'json_concerned1'           => json_encode($cons_list1),
                    'json_concerned1'           => json_encode($cons_list1),
                    'form_action' => !$is_add
                            ? $baseurl.'&action=addtematik&start='.$request_start
                            : $baseurl.'&action=addtematik',
                    'parent' => $_REQUEST['id'],
                    'cancel_link' => $baseurl.'&action=assortment&parent='.$_REQUEST['id'],
                    'add_concernd_link' => $baseurl.'&action=addconcernd&menu=false&id='.$_REQUEST['id'],
                ));
            } else {
                $GLOBALS['tpl']->assign(array(
                    'json_concerned0' => 'false',
                    'json_concerned1' => 'false',
                    'json_prop_vals' => 'false',
                    'json_ord_prop_vals' => 'false',
                    'form_action' => $baseurl.'&action=addtematik',
                    'parent' => $_REQUEST['id'],
                    'cancel_link' => $baseurl.'&action=assortment&parent='.$_REQUEST['id'],
                    'add_concernd_link' => $baseurl.'&action=addconcernd&menu=false',
                ));
            }

            $GLOBALS['tpl']->assign(array(
                'lang'			=>  $GLOBALS['lang'],
            ));
           if($prod){
               $GLOBALS['tpl']->newBlock('b_tree');
               $GLOBALS['tpl']->assign(array(
                   'parent'			=>  $_REQUEST['id'],
               ));
               $this->showCtgrTree($this->getCategsTree($ids_tree_anul,true));
           }
        }
        return $ret;
    }

    function showProdForm($id = 0, $is_add = false)
    {   global $baseurl, $CONFIG,$db,$server,$lang;

        $ret = true;
        $prod = false;
        if ($id) {
            if ( !($prod = $this->getProduct($id)) ) {
                $ret = false;
            }
        }
        if ($ret) {
            $GLOBALS['main']->include_main_blocks_2($this->module_name.'_edit_product.html', $this->tpl_path);
            $GLOBALS['tpl']->prepare();
            $GLOBALS['tpl']->assign(Array(
                "MSG_GoodID_must_be_positive" => $this->_msg["GoodID_must_be_positive"],
                "MSG_Group_ID_is" => $this->_msg["Group_ID_is"],
                "MSG_Enter_good_name" => $this->_msg["Enter_good_name"],
                "MSG_Set_other_link" => $this->_msg["Set_other_link"],
                "MSG_Add_assigned_goods" => $this->_msg["Add_assigned_goods"],
                "MSG_Add_analogues" => $this->_msg["Add_analogues"],
                "MSG_Analogues" => $this->_msg["Analogues"],
                "MSG_Delete_from_selected" => $this->_msg["Delete_from_selected"],
                "MSG_Added_images" => $this->_msg["Added_images"],
                "MSG_Main_images" => $this->_msg["Main_images"],
                "MSG_Choose_producer" => $this->_msg["Choose_producer"],
                "MSG_Choose_currency" => $this->_msg["Choose_currency"],
                "MSG_Good_code" => $this->_msg["Good_code"],
                "MSG_articul" => $this->_msg["articul"],
                "MSG_Name" => $this->_msg["Name"],
                "MSG_Good_producer" => $this->_msg["Good_producer"],
                "MSG_Select_producer" => $this->_msg["Select_producer"],
                "MSG_Short_description" => $this->_msg["Short_description"],
                "MSG_Full_description" => $this->_msg["Full_description"],
                "MSG_Page_description" => $this->_msg["Page_description"],
                "MSG_Keywords" => $this->_msg["Keywords"],
                "MSG_Currency" => $this->_msg["Currency"],
                "MSG_Select_currency" => $this->_msg["Select_currency"],
                "MSG_Retail_price" => $this->_msg["Retail_price"],
                "MSG_Partner_price" => $this->_msg["Partner_price"],
                "MSG_Dealer_price" => $this->_msg["Dealer_price"],
                "MSG_Old_price" => $this->_msg["Old_price"],
                "MSG_Price" => $this->_msg["Price"],
                "MSG_Presence" => $this->_msg["Presence"],
                "MSG_required_fields" => $this->_msg["required_fields"],
                "MSG_Save" => $this->_msg["Save"],
                "MSG_Cancel" => $this->_msg["Cancel"],
                "MSG_Alias" => $this->_msg["Alias"],
                "MSG_Alias_Info" => $this->_msg["Alias_Info"],
                "MSG_no_value" => $this->_msg["no_value"],
                "MSG_Assigned_goods" => $this->_msg["Assigned_goods"],
                "MSG_Ed" => $this->_msg["Ed"],
                "MSG_Rank" => $this->_msg["Rank"],
                "MSG_Del" => $this->_msg["Del"],
                "MSG_Active" => $this->_msg["Active"],
                "MSG_Try_on" => $this->_msg["Try_on"],
                "MSG_Prod_prop" => $this->_msg["Prod_prop"],
                "MSG_Order_prop" => $this->_msg["Order_prop"],
                "MSG_Middle_image" => $this->_msg["Middle_image"],
                "MSG_Big_image" => $this->_msg["Big_image"],
                "MSG_Generated" => $this->_msg["Generated"],
                "MSG_Delete" => $this->_msg["Delete"],
                "MSG_Edit" => $this->_msg["Edit"],
                "MSG_Del" => $this->_msg["Del"],
                "MSG_Ed" => $this->_msg["Ed"],
                "MSG_Images" => $this->_msg["Images"],
                "MSG_Properties" => $this->_msg["Properties"],
                "MSG_Relations" => $this->_msg["Relations"],
                'baseurl' => $baseurl
            ));
            if ($prod) {
                $this->show_nav_path($prod['category_id'], $baseurl, $id);
            } else {
                $this->show_nav_path($_REQUEST['id'], $baseurl);
            }
            $currencies	= $this->getCurrencies($prod ? $prod['product_currency'] : '');
            $group_id = $prod ? $prod['group_id'] : $this->get_category_group_id($_REQUEST['id']);
            $do_load = $group_id
                ? ($prod
                    ? "<script>doLoad('".$GLOBALS['lang']."', '".$group_id."', '".$id."');</script>"
                    : "<script>doLoad('".$GLOBALS['lang']."', '".$group_id."');</script>")
                : "";
            $GLOBALS['tpl']->assign(array(
                'currencies' => $currencies,
                "referer" => $_SERVER['HTTP_REFERER'],
                "start" => $_REQUEST['start'] >1 ? (int)$_REQUEST['start'] : '',
                "baseurl" => $baseurl,
                "group_id" => $group_id
            ));
            if ($prod) {
                $list0 = $this->getConcernedProducts(0, $prod['id'], false, 'rank');
                $cons_list0 = false;
                if ($list0) {
                    foreach ($list0 as $val) {
                        $cons_list0[$val['concrn_id']] = array(
                            'id' => $val['concrn_id'],
                            'title' => $val['concrn_title'],
                            'availability' => $val['availability'],
                            'price' => (int)$val['price_1'],
                            'active' => $val['active'] ? $this->_msg['Yes'] : $this->_msg['No'],
                            'rank' => (int)$val['rank']
                        );
                    }
                }
                $list1 = $this->getConcernedProducts(1, $prod['id'], false, 'rank');
                $cons_list1 = false;
                if ($list1) {
                    foreach ($list1 as $val) {
                        $cons_list1[$val['concrn_id']] = array(
                            'id' => $val['concrn_id'],
                            'title' => $val['concrn_title'],
                            'availability' => $val['availability'],
                            'price' => (int)$val['price_1'],
                            'active' => $val['active'] ? $this->_msg['Yes'] : $this->_msg['No'],
                            'rank' => (int)$val['rank']
                        );
                    }
                }
                Module::init_json();
                $GLOBALS['tpl']->assign(array(
                    'product_price_1'			=> $prod['product_price_1'],
                    'product_price_2'			=> $prod['product_price_2'],
                    'product_price_3'			=> $prod['product_price_3'],
                    'product_price_4'			=> $prod['product_price_4'],
                    'product_price_5'			=> $prod['product_price_5'],
                    'price_1'					=> $prod['price_1'],
                    'price_2'					=> $prod['price_2'],
                    'date'					=> $prod['date'],
                    'swf_text' => $prod['swf_text'],
                    'atrade' => $prod['atrade'],
                    'bonus' => $prod['bonus'],
                    'weight' => $prod['weight'],
                    'dimensions' => $prod['dimensions'],
                    'product_synonym' => $prod['product_synonym'],
                    'file_alarm' => $prod['file_alarm'],
                    'file_alarm' => $prod['file_alarm'] ? 'checked' : '',
                    'product_count' => $prod['product_count'],
                    'nyml_c' => $prod['nyml'] ? 'checked' : '',
                    'img'					=> $prod['swf_file'],
                    'link'					=> $prod['link'],
                    'price_3'					=> $prod['price_3'],
                    'price_4'					=> $prod['price_4'],
                    'price_5'					=> $prod['price_5'],
                    'text'						=> $prod['product_description'],
                    'text_shop'						=> $prod['product_description_shop'],
                    'product_id'				=> $prod['id'],
                    'product_code'				=> $prod['product_code'],
                    'product_name'				=> $prod['product_title'],
                    'product_alias'				=> $is_add ? '' : $prod['alias'],
                    'product_inf'				=> $prod['product_inf'],
                    'page_description'			=> $prod['description'] ? $prod['description'] : $prod['product_description'],
                    'page_keywords'				=> $prod['keywords'] ? $prod['keywords'] : $prod['keywords'],
                    'seo_title'				=> $prod['seo_title'] ? $prod['seo_title'] : $prod['product_title'],
                    'availability' 				=> $prod['product_availability'],
                    'image_middle_file'  		=> $prod['product_image'],
                    'image_big_file'			=> $prod['product_image_big'],
                    'photo_desc'                 => $prod['img_desc'],
                    'json_concerned0'           => json_encode($cons_list0),
                    'json_concerned1'           => json_encode($cons_list1),
                    'json_prop_vals'           => json_encode($this->getProdPropVal($prod['id'])),
                    'json_ord_prop_vals'        => json_encode($this->getProdOrderPropValIds($prod['id'])),
                    'json_concerned1'           => json_encode($cons_list1),
                    'form_action' => !$is_add
                            ? $baseurl.'&action=updateproduct&start='.$request_start
                            : $baseurl.'&action=addproduct',
                    'parent' => $prod['category_id'],
                    'cancel_link' => $baseurl.'&action=assortment&parent='.$prod['category_id'],
                    'add_concernd_link' => $baseurl.'&action=addconcernd&menu=false&id='.$prod['id'],
                ));
            } else {
                $GLOBALS['tpl']->assign(array(
                    'json_concerned0' => 'false',
                    'json_concerned1' => 'false',
                    'json_prop_vals' => 'false',
                    'json_ord_prop_vals' => 'false',
                    'form_action' => $baseurl.'&action=addproduct',
                    'parent' => $_REQUEST['id'],
                    'cancel_link' => $baseurl.'&action=assortment&parent='.$_REQUEST['id'],
                    'add_concernd_link' => $baseurl.'&action=addconcernd&menu=false',
                ));
            }
            if($prod['swf_file']){
                $GLOBALS['tpl']->newBlock('img');
                $GLOBALS['tpl']->assign(array(
                    'img' => $prod['swf_file']
                ));
            }
            if ($CONFIG['catalog_show_price_1']) {
                $GLOBALS['tpl']->newBlock('block_show_price_1');
                $GLOBALS['tpl']->assign(array(
                    'price_1' => $prod ? $prod['price_1'] : '',
                    "MSG_Retail_price" => $this->_msg["Retail_price"],
                    "MSG_Partner_price" => $this->_msg["Partner_price"],
                    "MSG_Dealer_price" => $this->_msg["Dealer_price"],
                    "MSG_Old_price" => $this->_msg["Old_price"],
                ));
            }
            if ($CONFIG['catalog_show_price_2']) {
                $GLOBALS['tpl']->newBlock('block_show_price_2');
                $GLOBALS['tpl']->assign(array(
                    'price_2' => $prod ? $prod['price_2'] : '',
                    "MSG_Retail_price" => $this->_msg["Retail_price"],
                    "MSG_Partner_price" => $this->_msg["Partner_price"],
                    "MSG_Dealer_price" => $this->_msg["Dealer_price"],
                    "MSG_Old_price" => $this->_msg["Old_price"],
                ));
            }
            if ($CONFIG['catalog_show_price_3']) {
                $GLOBALS['tpl']->newBlock('block_show_price_3');
                $GLOBALS['tpl']->assign(array(
                    'price_3' => $prod ? $prod['price_3'] : '',
                    "MSG_Retail_price" => $this->_msg["Retail_price"],
                    "MSG_Partner_price" => $this->_msg["Partner_price"],
                    "MSG_Dealer_price" => $this->_msg["Dealer_price"],
                    "MSG_Old_price" => $this->_msg["Old_price"],
                ));
            }
            if ($CONFIG['catalog_show_price_4']) {
                $GLOBALS['tpl']->newBlock('block_show_price_4');
                $GLOBALS['tpl']->assign(array(
                    'price_4' => $prod ? $prod['price_4'] : '',
                    "MSG_Retail_price" => $this->_msg["Retail_price"],
                    "MSG_Partner_price" => $this->_msg["Partner_price"],
                    "MSG_Dealer_price" => $this->_msg["Dealer_price"],
                    "MSG_Old_price" => $this->_msg["Old_price"],
                ));
            }
            if ($CONFIG['catalog_show_price_5']) {
                $GLOBALS['tpl']->newBlock('block_show_price_5');
                $GLOBALS['tpl']->assign(array(
                    'price_5' => $prod ? $prod['price_5'] : '',
                    'checked' => (int)$prod['price_5']>0 ? 'checked' : '',
                    "MSG_Retail_price" => $this->_msg["Retail_price"],
                    "MSG_Partner_price" => $this->_msg["Partner_price"],
                    "MSG_Dealer_price" => $this->_msg["Dealer_price"],
                    "MSG_Old_price" => $this->_msg["Old_price"],
                ));
            }
            //echo htmlspecialchars($do_load)."<hr>";
            $GLOBALS['tpl']->newBlock('block_groups');
            $GLOBALS['tpl']->assign(Array(
                "MSG_Goods_group" => $this->_msg["Goods_group"],
                "MSG_Select_group" => $this->_msg["Select_group"],
            ));
            $GLOBALS['tpl']->assign(array(
                'lang'       => $GLOBALS['lang'],
                'doLoad'     => $do_load,
                'product_id' => $id
            ));
            $grp = $this->get_all_groups($group_id);
            $GLOBALS['tpl']->assign_array('block_group', $grp);
            if ($prod && $prod['product_image']) {
                $image_middle = '<a target="new" href="'.$CONFIG['catalog_img_path'].$prod['product_image'].'" class="thickbox">' . $this->_msg["Look"] . '</a>';
            }
            if ($prod && $prod['product_image_big']) {
                $GLOBALS['tpl']->newBlock('block_big_image');
                $GLOBALS['tpl']->assign(array(
                    "MSG_Delete" => $this->_msg["Delete"],
                    'path' => "/".$CONFIG['catalog_img_path'].$prod['product_image_big'],
                    'file' => $prod['product_image_big'],
                ));
            }
            if ($prod) {
                if ($prod['product_image']) {
                    $path = "/".$CONFIG['catalog_img_path'].$prod['product_image'];
                    $GLOBALS['tpl']->newBlock('block_middle_image');
                    $GLOBALS['tpl']->assign(array(
                        "MSG_Delete" => $this->_msg["Delete"],
                        'path' => $path,
                        'big_img_path' => $prod && $prod['product_image_big']
                                ? "/".$CONFIG['catalog_img_path'].$prod['product_image_big']
                                : $CONFIG['catalog_def_img_path']
                    ));
                } else if ($prod['product_image_big']) {
                    $path = $CONFIG['catalog_def_prev_path'];
                    $GLOBALS['tpl']->newBlock('block_no_middle_image');
                    $GLOBALS['tpl']->assign(array(
                        "MSG_Delete" => $this->_msg["Delete"],
                        'path' => $path,
                        'big_img_path' => $prod && $prod['product_image_big']
                                ? "/".$CONFIG['catalog_img_path'].$prod['product_image_big']
                                : $CONFIG['catalog_def_img_path']
                    ));
                }
            }
            $GLOBALS['tpl']->newBlock('block_product_image');
            $image_middle = "";
            $GLOBALS['tpl']->assign(array(
                'image_title'	=>	$this->_msg["Middle_image"],
                'image_exists'	=>	$image_middle,
                'field_name'	=>	'image_middle',
                'image_name'	=>	$prod ? $prod['product_image'] : '',
                "MSG_Delete" => $this->_msg["Delete"],
            ));
            $GLOBALS['tpl']->newBlock('block_product_image_generated');
            $GLOBALS['tpl']->assign(array(
                'lang'			=>  $GLOBALS['lang'],
                'image_title'	=>	$this->_msg["Insert_middle_image"],
                'image_exists'	=>	'',
                'field_name'	=>	'image_middle',
                'image_name'	=>	$this->_msg["Image_name"],
                "MSG_Generated" =>  $this->_msg["Generated"]
            ));
            $GLOBALS['tpl']->newBlock('block_product_image');
            $image_big = "";
            if ($prod && $prod['product_image_big']) {
                $image_big = '<a target="new" href="'.$CONFIG['catalog_img_path'].$prod['product_image_big'].'" class="thickbox">' . $this->_msg["Look"] . '</a>';
            }
            $GLOBALS['tpl']->assign(array(
                'image_title'	=>	$this->_msg["Big_image"],
                'image_exists'	=>	$image_big,
                'field_name'	=>	'image_big',
                'image_name'	=>	$prod ? $prod['product_image_big'] : '',
                'MSG_Delete' => $this->_msg['Delete'],
            ));
            $this->show_ptypes($prod ? $prod['ptype_id'] : '');
            $res = $this->showAdditionalImgs($prod ? $prod['id'] : '');
            $add_img_cnt = $res ? $res : 0;
            $GLOBALS['tpl']->assign(array(
                '_ROOT.add_img_cnt' => $add_img_cnt,
                '_ROOT.max_add_img_cnt' => $CONFIG['catalog_add_img_cnt']
            ));
            if($id!=0) {
                $db->query("SELECT * FROM " . $server . $lang . "_catalog_products_video WHERE id_prod=" . $id);
                while ($db->next_record()) {
                    $GLOBALS['tpl']->newBlock('videos');
                    $GLOBALS['tpl']->assign(Array(
                        "id_video" => $db->f('id'),
                        "video_block" => $db->f('video'),
                        "descr" => $db->f('desc')
                    ));
                }
            }
        }
        return $ret;
    }

    function getFileExt($filename)
    {
        $ext = strrchr($filename, '.');
        return $ext ? strtolower(substr($ext, 1)) : false;
    }

    function exchange($sum, $from_cur, $to_cur)
    {   global $CONFIG;

        if (!array_key_exists($from_cur, $CONFIG['catalog_currencies'])
            || !array_key_exists($to_cur, $CONFIG['catalog_currencies'])
            || !is_numeric($sum)
        ) {
            return FALSE;
        }

        return $to_cur != $from_cur ? $sum * $_SESSION['catalog_rates'][$from_cur . '/' . $to_cur] : $sum;
    }

    function setReviewedProd($id)
    {
        if (!isset($_SESSION['catalog_reviewed'])) {
            $_SESSION['catalog_reviewed'] = array();
        }
        if ( $id && FALSE !== ($k = array_search($id, $_SESSION['catalog_reviewed'])) ) {
            unset($_SESSION['catalog_reviewed'][$k]);
        }
        array_unshift($_SESSION['catalog_reviewed'], $id);
    }

    function getReviewedProdIds()
    {
        return empty($_SESSION['catalog_reviewed']) ? false : $_SESSION['catalog_reviewed'];
    }

    function getReviewedProdList($transurl, $limit = 0, $start = 0)
    {   global $db, $CONFIG, $lang;

        $ret = false;
        $ids = $this->getReviewedProdIds();
        if ( ! empty($ids) ) {
            $limit = $limit >=1 ? (int) $limit : 0;
            $start = $start > 1 ? (int) $start : 1;
            $ids = array_map('intval', $ids);
            $clnt_price = 'prod_price_'.(@$_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : '1');
            $add_to_cart_link = Core::formPageLink($CONFIG['shop_page_link'], $CONFIG['shop_page_address'], $lang);
            $add_to_cart_link .= (FALSE === strpos($add_to_cart_link, "?") ? "?" : "&")."action=addtocart&id=";

            $db->query("SELECT P.*, C.title, if(C.alias is not null and C.alias!='', C.alias, C.id) as ctg_alias, "
                ."if(P.alias is not null and P.alias!='', P.alias, P.id) as prod_alias "
                ."FROM {$this->table_prefix}_catalog_products P INNER JOIN "
                ."{$this->table_prefix}_catalog_categories C ON category_id=C.id "
                ."WHERE P.id IN (".implode(",", $ids).")"
                .($limit ? " LIMIT ".($start-1)*$limit.",".$limit : ""));
            while ($db->next_record()) {
                $img = $db->Record['image_middle'] && file_exists(RP.'/'.$CONFIG['catalog_img_path'].$db->Record['image_middle'])
                    ? '/'.$CONFIG['catalog_img_path'].$db->Record['image_middle'] : '';
                $b_img = $db->Record['image_big'] && file_exists(RP.'/'.$CONFIG['catalog_img_path'].$db->Record['image_big'])
                    ? '/'.$CONFIG['catalog_img_path'].$db->Record['image_big'] : '';
                $data = array(
                    'prod_id' => $db->Record['id'],
                    'prod_link' => $this->getProdLink($transurl, $db->f('prod_alias'), $db->Record['id'], $db->f('ctg_alias')),
                    'prod_title' => $db->f('product_title'),
                    'prod_descr' => $db->f('product_inf'),
                    'prod_image' => $img,
                    'prod_image_big' => $b_img,
                    'prod_price_1' => $this->getCostWithTaxes($db->f('price_1')),
                    'prod_price_2' => $this->getCostWithTaxes($db->f('price_2')),
                    'prod_price_3' => $this->getCostWithTaxes($db->f('price_3')),
                    'prod_price_4' => $this->getCostWithTaxes($db->f('price_4')),
                    'prod_price_5' => $this->getCostWithTaxes($db->f('price_5')),
                    'prod_currency' => $CONFIG['catalog_currencies'][$db->f('currency')],
                    'prod_currency_name' => $this->currencies[$db->f('currency')],
                    'prod_availability' => $db->f('product_availability'),
                    'prod_ptype_name' => $db->f('ptype_name'),
                    'prod_ptype_link' => $db->f('ptype_link'),
                    'add_to_cart_link' => $add_to_cart_link . $db->Record['id']
                );
                $data['prod_price'] = $data[$clnt_price];
                $ids[array_search($db->Record['id'], $_SESSION['catalog_reviewed'])] = $data;
            }
            foreach ($ids as $val) {
                if (is_array($val)) {
                    $ret[] = $val;
                }
            }
        }

        return $ret;
    }

    function mainFieldProp($prop = false)
    {   global $PAGE;
        static $sProp = false;
        $ret = false;
        if (empty($prop)) {
            $ret = $sProp;
        } else if (0 == $PAGE['field']) {
            $sProp = $prop;
            $ret = true;
        }
        return $ret;
    }

    function addLastViewed($prod_id)
    {
        $ret = false;
        if ($prod_id > 0) {
            if (! isset($_SESSION['catalog_last_viewed'])) {
                $_SESSION['catalog_last_viewed'] = array();
            }
            if (false !== ($k = array_search($prod_id, $_SESSION['catalog_last_viewed']))) {
                unset($_SESSION['catalog_last_viewed'][$k]);
            }
            array_unshift($_SESSION['catalog_last_viewed'], $prod_id);
        }

        return $ret;
    }

    function getLastViewed($param = array())
    {   global $db, $CONFIG, $lang;

        $ret = false;
        if (! empty($_SESSION['catalog_last_viewed']) ) {
            $prod_ids = array_map('intval', $_SESSION['catalog_last_viewed']);
            $cnt = $param['cnt'] >= 1 ? (int)$param['cnt'] : $CONFIG['catalog_max_rows'];
            $start = $param['start'] >= 1 ? (int)$param['start']-1 : 0;
            $prod_ids = array_chunk($prod_ids, $cnt);
            $prod_ids = isset($prod_ids[$start]) ? $prod_ids[$start] : $prod_ids[0];
            $transurl = $param['transurl'] ? $param['transurl'] : Core::formPageLink($CONFIG['catalog_page_link'], $CONFIG['catalog_page_address'], $lang);
            $clnt_price = 'prod_price_'.(@$_SESSION['siteuser']['client_type'] ? $_SESSION['siteuser']['client_type'] : '1');
            $db->query('SELECT p.*, pt.ptype_name, pt.link as ptype_link, if(c.alias is not null and c.alias!=\'\', c.alias, c.id) as ctg_alias,'
                .' if(p.alias is not null and p.alias!=\'\', p.alias, p.id) as prod_alias'
                .' FROM '.$this->table_prefix.'_catalog_products p INNER JOIN '
                .$this->table_prefix.'_catalog_categories c ON c.id=p.category_id LEFT JOIN '
                .$this->table_prefix.'_catalog_ptypes pt ON p.ptype_id = pt.id WHERE p.id IN ('
                .implode(',', $prod_ids).') AND p.active = 1');
            if ($db->nf() > 0) {
                $add_to_cart_link = Core::formPageLink($CONFIG['shop_page_link'], $CONFIG['shop_page_address'], $lang);
                $add_to_cart_link .= (FALSE === strpos($add_to_cart_link, "?") ? "?" : "&")."action=addtocart&id=";
                $i = 0;
                while($db->next_record()) {
                    $id = $db->Record['id'];
                    $img = $db->f('image_middle');
                    $img = $img && file_exists(RP.'/'.$CONFIG['catalog_img_path'].$img) ? '/'.$CONFIG['catalog_img_path'].$img : '';
                    $b_img = $db->f('image_big');
                    $b_img = $b_img && file_exists(RP.'/'.$CONFIG['catalog_img_path'].$b_img) ? '/'.$CONFIG['catalog_img_path'].$b_img : '';
                    $arr[$id]['prod_num']		= ($i++);
                    $arr[$id]['prod_id']			= $prod_id = $db->f('id');
                    $arr[$id]['prod_link']		= $this->getProdLink($transurl, $db->f('prod_alias'), $prod_id, $db->f('ctg_alias'));
                    $arr[$id]['prod_title']		= $db->f('product_title');
                    $arr[$id]['prod_descr']		= $db->f('product_inf');
                    $arr[$id]['prod_image']		= $img;
                    $arr[$id]['prod_image_big']	= $b_img;
                    $arr[$id]['prod_price_1']	= $this->getCostWithTaxes($db->f('price_1'));
                    $arr[$id]['prod_price_2']	= $this->getCostWithTaxes($db->f('price_2'));
                    $arr[$id]['prod_price_3']	= $this->getCostWithTaxes($db->f('price_3'));
                    $arr[$id]['prod_price_4']	= $this->getCostWithTaxes($db->f('price_4'));
                    $arr[$id]['prod_price_5']	= $this->getCostWithTaxes($db->f('price_5'));
                    $arr[$id]['prod_price']		= $arr[$id][$clnt_price];
                    $arr[$id]['prod_currency']	= $CONFIG['catalog_currencies'][$db->f('currency')];
                    $arr[$id]['prod_currency_name']	= $this->currencies[$db->f('currency')];
                    $arr[$id]['prod_availability']	= $db->f('product_availability');
                    $arr[$id]['prod_ptype_name']	= $db->f('ptype_name');
                    $arr[$id]['prod_ptype_link']	= $db->f('ptype_link');
                    $arr[$id]['product_count']	= $db->f('product_count');
                    $arr[$id]['add_to_cart_link']= $add_to_cart_link . $prod_id;
                }
                $this->setPropValForProdList($arr);
                foreach ($prod_ids as $id) {
                    if (isset($arr[$id])) {
                        $ret[$id] = $arr[$id];
                    }
                }
            }
        }

        return $ret;
    }

    function getMainCatProducts($id)
    {
        global $db, $server, $lang;
        $cats = getAllCats($id);

        if ( !empty ( $cats ) )
        {
            $cats_ids = implode ( ',', $cats );
            $sql = "SELECT p.*, IF( p.price_1 < 0.1
OR p.product_availability = '0', 0, 1 ) AS nrank, c.title as cat_title, c.id as cat_id, c.alias as cat_alias 
				        FROM ".$server.$lang."_catalog_products as p
				        INNER JOIN ".$server.$lang."_catalog_categories as c ON ( c.id = p.category_id )
						WHERE p.archive=".$this->archive." AND p.category_id IN ($cats_ids) AND p.active = 1 ".($_REQUEST['new_s']&&false ? ' AND p.price_1>0 AND product_availability=\'1\'' : '')." ORDER BY ".($_REQUEST['new_s'] ? "nrank DESC," : '')." p.category_id ";
            //echo $sql; die();
            $cat_prd = $db->getArrayOfResult($sql);

            return $cat_prd;
        }

        return false;



    }

    function sortForTV($val)
    {
        $i=0;
        foreach($val as $item)
        {
            $elem=preg_replace('/[^0-9]/', '', $item['product_code']);
            $elem=substr($elem,2);
            $elem=substr($elem,0,3);
            $val[$i]['sort_S']=$elem;
            $i++;
        }

     /*   function cmp($a, $b)
        {
            if ($a['sort_S'] == $b['sort_S']) {
                return 0;
            }
            return ($a['sort_S'] > $b['sort_S']) ? -1 : 1;
        }*/
        usort($val, function ($a, $b)
        {
            if ($a['sort_S'] == $b['sort_S']) {
                return 0;
            }
            return ($a['sort_S'] > $b['sort_S']) ? -1 : 1;
        });

        return $val;
    }
    function cmp($a, $b){
        if ($a['sort_S'] == $b['sort_S']) {
            return 0;
        }
        return ($a['sort_S'] > $b['sort_S']) ? -1 : 1;

    }
    function sortForTVSupC($val)
    {
        $i=0;
        foreach($val as $item)
        {
            $elem=preg_replace('/[^0-9]/', '', $item['prod_code']);
            $elem=substr($elem,2);
            $elem=substr($elem,0,3);
            $val[$i]['sort_S']=$elem;
            $i++;
        }

     /*   function cmp($a, $b)
        {
            if ($a['sort_S'] == $b['sort_S']) {
                return 0;
            }
            return ($a['sort_S'] > $b['sort_S']) ? -1 : 1;
        }*/
        usort($val, function ($a, $b)
        {
            if ($a['sort_S'] == $b['sort_S']) {
                return 0;
            }
            return ($a['sort_S'] > $b['sort_S']) ? -1 : 1;
        });

        return $val;
    }

    function getProductsByCategory($category_id){
        global $server,$lang;
        //return array();
        $query = 'SELECT id FROM '.$server.$lang.'_catalog_products WHERE category_id='.(int)$category_id.' ORDER BY rank';
        $prods = array();
        if(!$result = mysql_query($query)) die(mysql_error());
        while($row = mysql_fetch_assoc($result)) $prods[] = $row['id'];
        return $prods;
    }


    function get_product_action($prod_id)
    {
        global $db;
        return  $db->getArrayOfResult("SELECT a.title,a.id FROM sup_rus_skidki_prods AS p
                                LEFT JOIN sup_rus_actions_skidki AS s ON p.skidka_id=s.skidka_id
                                LEFT JOIN sup_rus_actions AS a ON a.id=s.action_id
                                WHERE p.prod_id=$prod_id AND a.time_to > ".time()." GROUP BY a.id");

    }

    function imp_csv_weight($file_name)
    {
        setlocale(LC_ALL, 'ru_RU');
        GLOBAL $server, $lang, $db, $main;

        $handle = fopen($file_name, "r");
        $message = '';
        if ($handle) {

            while (false !== ($data = fgets($handle))) {
                $row++;
                $data = str_replace('"""', '"', $data);
                $data = str_replace('""', '"', $data);
                $arr = explode(';',$data);
                $prod_id = iconv('windows-1251', 'UTF-8',$arr[0]);
                $weight = iconv('windows-1251', 'UTF-8',(int)$arr[1]);
                if($prod_id && strlen($arr[1])){
                    if(strlen($arr[1])) $update = 'weight="'.$weight.'"';
                    $query = 'UPDATE '.$server.$lang.'_catalog_products SET '.$update.' WHERE product_code="'.$prod_id.'"';
                    if(!mysql_query($query)) die(mysql_error());
                    $message .= 'Товар :'.$prod_id.' Вес:'.(strlen($arr[1]) ? $weight : 'не обновлено').'<br>';
                }
            }
            fclose($handle);
            return $message;
        } else {
            return false;
        }

        return true;
    }
    
    function getSelectListOfTime($time = '', $link_suff = '') {
		global $CONFIG, $tpl, $baseurl, $db2;

		$time = $this->checkPeriod($time);

		$tpl->newBlock('block_period');
		$tpl->assign(array('action' => $baseurl.$link_suff,
		                   'date_from_val' => is_array($_REQUEST['time']) && @$_REQUEST['time']['from'] ? $_REQUEST['time']['from'] : "",
		                   'code' => $_REQUEST['promo'] ? $_REQUEST['promo'] : "",
		                   'date_to_val' => is_array($_REQUEST['time']) && @$_REQUEST['time']['to']  ? $_REQUEST['time']['to'] : ""));
		                   
		$db2->query('Select promo_code FROM '.$this->table_prefix.'_shop_orders
						WHERE LENGTH (promo_code)
                        GROUP BY promo_code ');
                        
        if ($db2->nf() > 0) {
            for($i = 0; $i < $db2->nf(); $i++) {
	            $db2->next_record();
	            $tpl->newBlock('block_code');
	            $tpl->assign(array('code' => $db2->f('promo_code'),
	            					'selected' => $_REQUEST['promo'] == $db2->f('promo_code') ? "selected='selected'" : ''
				                  )
				            );
			}

		} 
		 
		for($i = 0; $i < sizeof($CONFIG['counter_variants_of_time'][$CONFIG['admin_lang']]); $i++) {
			$tpl->newBlock('block_time');
			if ($i != $time) {
				$tpl->newBlock('block_active_time');
				$tpl->assign(array('link' => $baseurl.$link_suff."&time=".$i,
				                   'name' => $CONFIG['counter_variants_of_time'][$CONFIG['admin_lang']][$i]
				                  )
				            );
			} else {
				$tpl->newBlock('block_no_active_time');
				$tpl->assign('name', $CONFIG['counter_variants_of_time'][$CONFIG['admin_lang']][$i]);
			}
        }
    }
    
    function checkPeriod($time)
	{   global $CONFIG;

	    return '' === $time
		       || (!is_array($time) && !array_key_exists($time, $CONFIG['counter_variants_of_time'][$CONFIG['admin_lang']]))
		       || (is_array($time) && !isset($time['from'])&& !isset($time['to']) )
		       ? $CONFIG['counter_default_time']
		       : $time;
	}
	
		function getReferringHosts($time, $start = 1, $all_links = true, $code=false)
	{   global $CONFIG, $db, $lang;

        if (!$start) {$start = 1;}
		$selects = $this->getTableByDate($time);

        $limit = $all_links ? 'LIMIT '.(($start - 1)*$CONFIG['counter_max_rows']).', '.$CONFIG['counter_max_rows'] : '';

   		$db->query('Select * FROM '.$this->table_prefix.'_shop_orders
						WHERE '.($selects['where'] ? $selects['where'].' AND LENGTH (promo_code)': 'LENGTH (promo_code)' ).'
						'.($code ? ' AND promo_code ="'.$code.'"' : '').'
                        ORDER BY date DESC '.$limit);
                                
   		if ($db->nf() > 0) {
            for($i = 0; $i < $db->nf(); $i++) {
            	$db->next_record();
				$arr[$i]['link']			= $db->f('customer');
				$arr[$i]['count_link']		= $db->f('total_sum');
				$arr[$i]['code']		= $db->f('promo_code');
				$arr[$i]['discount']		= $db->f('discount');
				$arr[$i]['id']		= $db->f('id');
			}
			return $arr;
		} else {
			return $this->_msg["No_data"];
		}
		return FALSE;
	}
	
		function getTableByDate($time)
	{   global $CONFIG, $db, $server, $lang;

        $time = $this->checkPeriod($time);

        switch ($time) {
            default: /*  Период */
        		if (is_array($time) && (isset($time['from']) || isset($time['to'])) ) {
        		    $from = @$time['from'] ? $time['from'] : -1;
        		    $to = @$time['to'] ? $time['to'] : -1;
        		    //$to = -1 == $to ? $to : $to + 60*60*24;
        		    if (-1 != $from && -1 != $to) {
        		        if ($from > $to) {
        		        	$t = $from;
        		        	$from = $to;
        		        	$to = $t;
        		        }
        		    	$where = "date >= '".$from."' AND date <= '".$to."'";
        		    } elseif (-1 != $from) {
        		        $where = "date >= ".$from;
        		    } elseif (-1 != $to) {
        		        $where = "date <= ".$to;
        		    } else {
        		        $where = '1 = 1';
        		    }
        		    $table = $server.$lang.'_shop_orders';
        		}
                break;
		}

        $arr['where'] = $where;
        $arr['table'] = $table;
    	return $arr;
	}
	
	public function yaMarket($title){
			$title = str_replace(' ', '+', $title);
				$headers = array(
		  "GET 'https://api.content.market.yandex.ru/v1/model/match.json' HTTP/1.1",
		  "Host:api.content.market.yandex.ru",
		  "Accept:*/*",
		  "Authorization:qH3c3tiGm2XBnpcTA4ud3mnPDHnTmv"
		);
		 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://api.content.market.yandex.ru/v1/model/match.json?name=".$title."&currency=rur&fields=category,discounts,facts,media,offers,photo,price,rating,vendor");
		 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		 
		$data = curl_exec($ch);
		 
		if (!curl_errno($ch)) {
			$data = json_decode($data);
			curl_close($ch);
			if(isset($data->model)){

				$this->yaOffers($data->model->id);
			}
			else{

				$this->yaSearch($title);
			}
		  
		}
		
		//exit;
	}
	
	public function yaOffers($id){
		global $tpl;
		$title = str_replace(' ', '+', $title);
				$headers = array(
		  "GET 'https://api.content.market.yandex.ru/v1/model/$id/offers.json,filters' HTTP/1.1",
		  "Host:api.content.market.yandex.ru",
		  "Accept:*/*",
		  "Authorization:qH3c3tiGm2XBnpcTA4ud3mnPDHnTmv"
		);
		 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://api.content.market.yandex.ru/v1/model/$id/offers.json?remote_ip=".$_SERVER["REMOTE_ADDR"]."&fields=discounts,filters");
		 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		 
		$data = curl_exec($ch);

		if (!curl_errno($ch)) {
			$data = json_decode($data);

			if(isset($data->offers)){
				$i=0;
				foreach($data->offers->items as $offer){
				if($i==0){
					$tpl->newBlock('analog_market_isset');
					$tpl->newBlock('analog_market');
					$tpl->assign(array('product_id'=>'market.yandex.ru/product/'.$id));
					 $i++;
				}
				$tpl->newBlock('analog_market_offer');
				$tpl->assign(array(
					'name' => $offer->name,
					'image' => $offer->previewPhotos[0]->url,
					'url' => $offer->url,
					'price' => $offer->price->value,
					'currency' => $offer->price->currencyName,
					'description' => $offer->description,
					'delivery' => $offer->delivery->available ? 'truck' : '',
					'delivery_price' => $offer->delivery->localDeliveryList[0]->price->value ? $offer->delivery->localDeliveryList[0]->price->value.',': '0,',
					'onStock' => $offer->onStock? 'в наличии': 'нет в наличии',
					'site' => $offer->shopInfo->name,
					'star_1' => $offer->shopInfo->rating >= 1 ? 'on' : 'off',
					'star_2' => $offer->shopInfo->rating >= 2 ? 'on' : 'off',
					'star_3' => $offer->shopInfo->rating >= 3 ? 'on' : 'off',
					'star_4' => $offer->shopInfo->rating >= 4 ? 'on' : 'off',
					'star_5' => $offer->shopInfo->rating >= 5 ? 'on' : 'off',
					'warranty' => $offer->warranty ? 'Гарантия производителя': '',
					'reviews' => $offer->shopInfo->gradeTotal,
				));
				//var_dump($offer);exit;
			}


		  curl_close($ch);
		}
		//exit;
		}
	}
	
	public function yaSearch($title){
		global $tpl;
			//$title = str_replace('+', ' ', $title);
				$headers = array(
		  "GET 'https://api.content.market.yandex.ru/v1/search.json' HTTP/1.1",
		  "Host:api.content.market.yandex.ru",
		  "Accept:*/*",
		  "Authorization:qH3c3tiGm2XBnpcTA4ud3mnPDHnTmv"
		);
		 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://api.content.market.yandex.ru/v1/search.json?remote_ip=".$_SERVER["REMOTE_ADDR"]."&text=".$title);
		 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		 
		$data = curl_exec($ch);
		if (!curl_errno($ch)){
			
			 $data = json_decode($data);
			if(isset($data->searchResult)){
				$i=0;
				
				foreach($data->searchResult->results as $offer){
				if($i==0){
					$tpl->newBlock('analog_market_isset');
					$tpl->newBlock('analog_market');
                    $title = str_replace('+', ' ', $title);
					$tpl->assign(array('product_id'=>'market.yandex.by/search.xml?text='.$title.'&cvredirect=0'));
					 $i++;
				}

				$tpl->newBlock('analog_market_offer');
				$tpl->assign(array(
					'name' => $offer->offer->name,
					'image' => $offer->offer->previewPhotos[0]->url,
					'url' => $offer->offer->url,
					'price' => $offer->offer->price->value,
					'currency' => $offer->offer->price->currencyName,
					'description' => $offer->offer->description,
					'delivery' => $offer->offer->delivery->available ? 'truck' : '',
					'delivery_price' => $offer->offer->delivery->localDeliveryList[0]->price->value ? $offer->offer->delivery->localDeliveryList[0]->price->value.',': '0,',
					'onStock' => $offer->offer->onStock? 'в наличии': 'нет в наличии',
					'site' => $offer->offer->shopInfo->name,
					'star_1' => $offer->offer->shopInfo->rating >= 1 ? 'on' : 'off',
					'star_2' => $offer->offer->shopInfo->rating >= 2 ? 'on' : 'off',
					'star_3' => $offer->offer->shopInfo->rating >= 3 ? 'on' : 'off',
					'star_4' => $offer->offer->shopInfo->rating >= 4 ? 'on' : 'off',
					'star_5' => $offer->offer->shopInfo->rating >= 5 ? 'on' : 'off',
					'warranty' => $offer->offer->warranty ? 'Гарантия производителя': '',
					'reviews' => $offer->offer->shopInfo->gradeTotal,
				));
				//var_dump($offer);exit;
			}
			}
			

		  curl_close($ch);
		}
		//exit;
	}
	
	public function checkYa($categoryId){
		$category = $this->getCategory($categoryId);
		return $category['nshow_ya'];
	}

}
?>