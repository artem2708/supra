<?php

class StructCSVFile {

	var $aIterator		= NULL;

	var $aStructTables	= array('catalog_categories'			=> array('updateable'		=> TRUE,
																		 'cdbtree'			=> TRUE,
																		 'key_of_table'		=> array('id','parent_id'),
																		 'type_of_table'	=> 'Категории товаров',
																		 'descr_fields'		=> '- ID родительской директории<br>
																		 						- ID категории<br>
																		 					    - название категории<br>
																		 					    - ID группы<br>
																		 					    - описание категории<br>
																		 					    - изображение для категории<br>
																		 					    - порядок следования категории<br>
																		 					    - признак активности (0 - не публиковать, 1 - публиковать)<br>
																		 					    - описание страницы<br>
																		 					    - ключевые слова страницы<br>
																		 					    - id владельца<br>
																		 					    - id группы<br>
																		 					    - права доступа(в десятичном формате)<br>',
																		 'marker'			=> '~~',
																		 'fields'			=> array('parent_id',
																		 							 'id',
																		 							 'title',
																		 							 'group_id',
  																									 'description',
  																									 'img',
																		 							 'rank',
  																									 'active',
  																									 'page_description',
  																									 'keywords',
  																									 'owner_id',
  																									 'usr_group_id',
  																									 'rights',
  																									 'alias',
  																									 'commentable'
  																									),
																		),

								'catalog_groups'				=> array('updateable'		=> FALSE,
																		 'cdbtree'			=> FALSE,
																		 'key_of_table'		=> array('id'),
																		 'type_of_table'	=> 'Группы товаров',
																		 'descr_fields'		=> '- ID группы товаров<br>
																		 						- название группы<br>',
																		 'marker'		=> '!!',
																		 'fields'		=> array('id','group_title'),
																		),

								'catalog_products'				=> array('updateable'		=> TRUE,
																		 'cdbtree'			=> FALSE,
																		 'key_of_table'		=> array('id'),
																		 'type_of_table'	=> 'Товары',
																		 'descr_fields'		=> '- ID товара<br>
																		 						- ID категории<br>
																		 						- ID группы<br>
																		 						- ID производителя<br>
																		 						- код товара (артикул)<br>
																		 						- название товара<br>
																		 						- краткое описание товара<br>
																		 						- полное описание товара<br>
																		 						- валюта<br>
																		 						- цена №1<br>
																		 						- цена №2<br>
																		 						- цена №3<br>
																		 						- цена №4<br>
																		 						- цена №5<br>
																		 						- изображение среднее<br>
																		 						- изображение большое<br>
																		 						- наличие<br>
																		 						- признак активности (0 - не публиковать, 1 - публиковать)<br>
																		 						- порядок следования товара<br>
																		 						- лидер продаж (0 - нет, 1 - да)<br>
																		 						- новинка (0 - нет, 1 - да)<br>
																		 						- описание страницы<br>
																		 					    - ключевые слова страницы<br>
																		 					    - id владельца<br>
																		 					    - id группы<br>
																		 					    - права доступа(в десятичном формате)<br>',
																		 'marker'		=> '@@',
																		 'fields'		=> array('id',
																								 'category_id',
																								 'group_id',
																								 'ptype_id',
																								 'product_code',
																								 'product_title',
																								 'product_inf',
																								 'product_description',
																								 'currency',
																								 'price_1',
																								 'price_2',
																								 'price_3',
																								 'price_4',
																								 'price_5',
																								 'image_middle',
																								 'image_big',
																								 'product_availability',
																								 'active',
																								 'rank',
																								 'leader',
																								 'novelty',
																								 'description',
																								 'keywords',
																								 'owner_id',
																								 'usr_group_id',
																								 'rights',
																								 'alias'
																								),
																		),

								'catalog_properties'			=> array('updateable'		=> FALSE,
																		 'cdbtree'			=> FALSE,
																		 'key_of_table'		=> array('property_id'),
																		 'type_of_table'	=> 'Динамические свойства/группы',
																		 'descr_fields'		=> '- ID свойства<br>
																		 						- ID группы<br>
																		 						- флаг - ипользуется ли при поиска<br>
																		 						- флаг - если ипользуется при поиска, то искакть в диапазоне<br>',
																		 'marker'		=> '##',
																		 'fields'		=> array('property_id',
																								 'group_id',
																								 'search',
																								 'range',
																								 'ord'
																								),
																		),

								'catalog_properties_def_values'	=> array('updateable'		=> FALSE,
																		 'cdbtree'			=> FALSE,
																		 'key_of_table'		=> array('id'),
																		 'type_of_table'	=> 'Значения динамических свойств по умолчанию',
																		 'descr_fields'		=> '- ID значения по умолчанию<br>
																		 						- ID динамического свойства<br>
																		 						- значение по умолчанию<br>',
																		 'marker'		=> '$$',
																		 'fields'		=> array('id',
																								 'property_id',
																								 'value',
																								),
																		),

								'catalog_properties_discounts'	=> array('updateable'		=> FALSE,
																		 'cdbtree'			=> FALSE,
																		 'key_of_table'		=> array('discount_id'),
																		 'type_of_table'	=> 'Скидки',
																		 'descr_fields'		=> '- ID скидки<br>
																		 						- ID группы<br>',
																		 'marker'		=> '%%',
																		 'fields'		=> array('discount_id',
																								 'group_id',
																								),
																		),

								'catalog_properties_table'		=> array('updateable'		=> FALSE,
																		 'cdbtree'			=> FALSE,
																		 'key_of_table'		=> array('id'),
																		 'type_of_table'	=> 'Динамические свойства',
																		 'descr_fields'		=> '- ID динамического свойства<br>
																		 						- заголовок динамического свойства<br>
																		 						- имя свойства<br>
																		 						- префикс динамического свойства<br>
																		 						- суффикс динамического свойства<br>
																		 						- дополнительные изображения<br>
																		 						- тип динамического свойства<br>',
																		 'marker'		=> '^^',
																		 'fields'		=> array('id',
																								 'property_title',
																								 'property_name',
																								 'property_prefix',
																								 'property_suffix',
																								 'type',
																								),
																		),

								'catalog_property_values'		=> array('updateable'		=> FALSE,
																		 'cdbtree'			=> FALSE,
																		 'key_of_table'		=> array('id'),
																		 'type_of_table'	=> 'Значения дин.свойств',
																		 'descr_fields'		=> '- ID значения динамического свойства<br>
																		 						- ID продукта<br>
																		 						- ID динамического свойства<br>
																		 						- значение динамического свойства<br>
																		 						- ID группы<br>',
																		 'marker'		=> '((',
																		 'fields'		=> array('id',
																								 'product_id',
																								 'property_id',
																								 'value',
																								 'group_id',
																								),
																		),

								'catalog_ptypes'				=> array('updateable'		=> FALSE,
																		 'cdbtree'			=> FALSE,
																		 'key_of_table'		=> array('id'),
																		 'type_of_table'	=> 'Производители товаров',
																		 'descr_fields'		=> '- ID производителя<br>
																		 						- название производителя<br>',
																		 'marker'		=> '))',
																		 'fields'		=> array('id',
																								 'ptype_name',
																								 'link',
																								),
																		),

								'catalog_concerned_products'	=> array('updateable'		=> FALSE,
																		 'cdbtree'			=> FALSE,
																		 'key_of_table'		=> array('prod_id', 'concrn_id'),
																		 'type_of_table'	=> 'Связанные товары',
																		 'descr_fields'		=> '- ID товара<br>
																		 						- ID связанного товара<br>
																		 						- тип саязи, 0 - связанные товары, 1 - аналоги
																		 						- номер вывода по порядку<br>',
																		 'marker'		=> '<>',
																		 'fields'		=> array('prod_id',
																								 'concrn_id',
																								 'type',
																								 'rank',
																								)
																		),

								'catalog_img'       	=> array('updateable'		=> FALSE,
																		 'cdbtree'			=> FALSE,
																		 'key_of_table'		=> array('id'),
																		 'type_of_table'	=> 'Дополнительные изображения',
																		 'descr_fields'		=> '- ID изображения<br>
																		                        - ID товара<br>
																		 						- имя файла с изображением<br>
																		 						- имя файла с превью<br>
																		 						- номер вывода по порядку<br>',
																		 'marker'		=> '??',
																		 'fields'		=> array('id',
																								 'prod_id',
																								 'img',
																								 'preview',
																								 'rank'
																								)
																		),
								);

	function getNextTable() {
		if (sizeof($this->aStructTables) > 0 && !$this->aIterator) {
			reset($this->aStructTables);
			$this->aIterator	= 1;
			return array(current($this->aStructTables), key($this->aStructTables));
		} else if ($this->aIterator < sizeof($this->aStructTables) && sizeof($this->aStructTables) > 0) {
			$this->aIterator++;
			return array(next($this->aStructTables), key($this->aStructTables));
		}
		return FALSE;
	}

	function getTable($table = NULL) {
		if ($table) {
			return $this->aStructTables[$table];
		}
		return FALSE;
	}

	function isUpdateable($table = NULL) {
		if ($table) {
			if (array_key_exists($table, $this->aStructTables)) {
				return $this->aStructTables[$table]['updateable'];
			}
		}
		return FALSE;
	}

	function getLineMarker($table = NULL) {
		if ($table) {
			if (array_key_exists($table, $this->aStructTables)) {
				return $this->aStructTables[$table]['marker'];
			}
		}
		return FALSE;
	}

	function getTableFields($table = NULL) {
		if ($table) {
			if (array_key_exists($table, $this->aStructTables)) {
				return $this->aStructTables[$table]['fields'];
			}
		}
		return FALSE;
	}

	function getTypeOfTable($table) {
		if (array_key_exists($table, $this->aStructTables)) {
			return $this->aStructTables[$table]['type_of_table'];
		}
		return FALSE;
	}

	function getDescrFields($table) {
		if (array_key_exists($table, $this->aStructTables)) {
			return $this->aStructTables[$table]['descr_fields'];
		}
		return FALSE;
	}

	function getFormatUploadCSV() {
		if (sizeof($this->aStructTables) > 0) {
			$i = 0;
			foreach ($this->aStructTables as $k => $v) {
				$arr[$i]['type_of_table']	= $this->getTypeOfTable($k);
				$arr[$i]['line_marker']		= $this->getLineMarker($k);
				$arr[$i]['descr_fields']	= $this->getDescrFields($k);
				$i++;
			}
			return $arr;
		}
		return FALSE;
	}
}

?>