<?php
class CCatalogData_DB extends TObject {
	var $id							=	NULL;
	var $category_id				=	NULL;
	var $product_title				=	NULL;
	var $product_description		=	NULL;
	var $image_middle				=	NULL;
	var $product_availability		=	NULL;
	var $leader						=	NULL;
	var $rank						=	NULL;
    var $alias						=	NULL;
	var $swf_file						=	NULL;
	function CCatalogData_DB ($table, $PK) {
		$this->TObject($table, $PK);
	}

	function getProduct ($id = null) {
		global $admin;
		$active_str = !$admin ? " AND active = 0" : "";
		if (!isset($id)) return FALSE;
		$sql = "
			SELECT
				a.id AS product_id,
				a.category_id AS product_category,
				a.product_title AS product_title,
				a.product_description AS product_description,
				a.image_middle AS image_middle,
				a.product_availability AS product_availability,
				a.rank AS product_rank,
				a.leader AS product_leader
			FROM
				$this->_tbl a
			WHERE
				a.id = '".My_Sql::escape($id)."'
				$active_str";
		$reqult = db_loadObject($sql, $this);
		return $result;
	}

	function getProducts ($id = null, $order_by = '', $rows = '', $start = 0, $admin = 0)
	{
		global $CONFIG;
		var_dump($this->getProductsWithRussianTitles());
		$ord_vals = array('id' => 'id',
		                  'title' => 'product_title',
		                  'availability' => 'product_availability',
		                  'rank' => 'rank',
		                  'active' => 'active',
		                  'leader' => 'leader',
		                  'hits' => 'hits');
		$order = 0 === strpos($order_by, "-") ? " DESC" : "";
		$order_by = $order ? substr($order_by, 1) : $order_by;
		if (in_array($order_by, array_keys($ord_vals))) {
			$order = $ord_vals[$order_by].$order;
		} else {
			$order = $CONFIG['catalog_prod_order_by'];
		}

		$active_str = !$admin ? " AND active = 1" : "";
		if (!isset($id)) return FALSE;
		$sql = "
			SELECT
				a.id AS product_id,
				a.category_id AS product_category,
				a.group_id AS group_id,
				a.ptype_id AS ptype_id,
				a.product_code AS product_code,
				a.product_title AS product_title,
				a.product_inf AS product_inf,
				a.product_description AS product_description,
				a.currency AS currency,
				a.price_1 AS price_1,
				a.price_2 AS price_2,
				a.price_3 AS price_3,
				a.price_4 AS price_4,
				a.price_5 AS price_5,
				a.product_description AS product_description,
				a.image_middle AS image_middle,
				a.image_big AS image_big,
				a.product_availability AS product_availability,
				a.active AS product_active,
				a.rank AS product_rank,
				a.leader AS product_leader,
				a.hits AS product_hits,
				a.owner_id,
				a.usr_group_id,
				a.rights
			FROM
				$this->_tbl a
			WHERE
				a.category_id = '".My_Sql::escape($id)."'
				{$active_str}
			ORDER BY ".$order;
		isset($start) ? $sql	.= ' LIMIT '.intval($start) : '';
		isset($start) && $rows > 0	? $sql	.= ', '.intval($rows): '';
		return db_loadList($sql);
	}
	
	function getProductsWithRussianTitles () {
		global $admin;
		if(!$admin) return FALSE; 
		$sql = "
			SELECT
				a.id AS product_id,
				a.category_id AS product_category,
				a.product_title AS product_title,
				a.product_description AS product_description,
				a.image_middle AS image_middle,
				a.product_availability AS product_availability,
				a.rank AS product_rank,
				a.leader AS product_leader
			FROM
				$this->_tbl a
			WHERE
				a.product_title REGEXP  '[а-я]'";
		$reqult = db_loadObject($sql, $this);
		return $result;
	}

	function getProperties ($id = null) {
		if (!isset($id)) return 0;
		$id = (int)$id;
		$sql = "
			SELECT
				b.id AS property_id,
				b.property_title AS property_title,
				b.property_prefix AS property_prefix,
				c.value AS property_value,
				b.property_suffix AS property_suffix
			FROM
				$this->_tbl a,
				rus_catalog_properties_table b,
				rus_catalog_property_values c,
				rus_catalog_references d,
				rus_catalog_groups e
			WHERE
				a.id = '".$id."'
			AND
				d.product_id = '".$id."'
			AND
				c.product_id = '".$id."'
			AND
				e.id = d.group_id
			AND
				c.property_id = b.id
			AND
				c.group_id = d.group_id
		";
		return db_loadList($sql);
	}

	function getGroup ($id = null) {
	}

}
class CCatalogCategories_DB extends TObject {
	var $id				= NULL;
	var $cleft			= NULL;
	var $cright			= NULL;
	var $clevel			= NULL;
	var $parent_id		= NULL;
	var $active			= NULL;
	var $title			= NULL;
	var $group_id		= 0;
	var $description	= NULL;
	var $img			= NULL;
	var $rank			= NULL;
	var $page_description =	NULL;
	var $keywords		= NULL;
	var $synonyms		= NULL;
    var $alias		    = NULL;
    var $commentable    = NULL;
	var $main    = NULL;
	var $products_synonyms    = NULL;

	function CCatalogCategories_DB ($table, $PK) {
		$this->TObject($table, $PK);
	}

	function listByNav( $parent_id, $limit=null, $order_by = null ) {
		$sql = "SELECT * FROM $this->_tbl WHERE clevel != 0 AND parent_id = '"
		    .intval($parent_id)."' ".My_Sql::escape($order_by)." LIMIT ".My_Sql::escape($limit);
		return db_loadList( $sql );
	}

	function listAll($parent_id = 1, $active_str = null, $limit=null) {
		global $CONFIG;

		$parent_id = (int)$parent_id;
		if ($parent_id > 0) $parent_id = " AND parent_id = $parent_id";
		$sql = "SELECT * FROM $this->_tbl WHERE clevel != 0 {$parent_id} "
		    .My_Sql::escape($active_str)." ORDER BY ".$CONFIG['catalog_ctg_order_by']." "
		    .My_Sql::escape($limit);
		$sql = db_loadList($sql);
		return $sql;
	}

	function insert() {
		if (db_insertObject($this->_tbl, $this)) return true;
		else return false;
	}

	function countObjs($id = 1) {
		$sql="SELECT count(1) FROM $this->_tbl WHERE clevel != 0 AND parent_id = ".intval($id);
		return db_loadResult($sql);
	}

	function getLevel ($id) {
		$sql = "SELECT clevel FROM $this->_tbl WHERE id = ".intval($id);
		return db_loadResult($sql);
	}
    function getTematik ($id) {
        $sql = "SELECT tematik FROM $this->_tbl WHERE id = ".intval($id);
        return db_loadResult($sql);
    }
}

?>
