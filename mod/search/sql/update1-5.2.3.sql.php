<?php
$sql = array(
    'ALTER TABLE `{site_prefix}_search_page_index` ADD `indx` TEXT NOT NULL AFTER `body`',
    'ALTER TABLE `{site_prefix}_search_tmp_page_index` ADD `indx` TEXT NOT NULL AFTER `body`',
    'ALTER TABLE `{site_prefix}_search_page_index` DROP INDEX `search_index`, ADD FULLTEXT `search_index` (`body`, `indx`)'
);
?>