<?php
$MOD_TABLES[] = "CREATE TABLE `".$prefix."_search_broken_links` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `url` varchar(255) NOT NULL default '',
  `referer` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_search_page_index` (
  `url` varchar(255) NOT NULL default '',
  `title` varchar(255) default NULL,
  `descr` text NOT NULL,
  `body` text,
  `indx` text,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `status` smallint(6) NOT NULL default '200',
  `title` varchar(255) default NULL,
  `categoryId` varchar(11) default NULL,
  `synonyms` varchar(255) default NULL,
  `categoryTitle` varchar(255) default NULL,
  `productId` varchar(11) default NULL,
  PRIMARY KEY (`url`),
  FULLTEXT KEY `search_index` (`body`, `indx`)
) ENGINE=MyISAM /*!40101 DEFAULT CHARSET=utf8 */";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_search_tmp_broken_links` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `url` varchar(255) NOT NULL default '',
  `referer` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_search_tmp_page_index` (
  `url` varchar(255) NOT NULL default '',
  `title` varchar(255) default NULL,
  `descr` text NOT NULL,
  `body` text,
  `indx` text,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `status` smallint(6) NOT NULL default '200', 
  `categoryId` varchar(11) default NULL,
  `synonyms` varchar(255) default NULL,
  `categoryTitle` varchar(255) default NULL,
  `productId` varchar(11) default NULL,
  PRIMARY KEY (`url`)
) ENGINE=MyISAM /*!40101 DEFAULT CHARSET=utf8 */";

?>