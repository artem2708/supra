<?php // Vers 5.8.2 27.06.2012

/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
			  'search_untitle'			=> array('type'		=> 'select',
												 'defval'	=> array('No title'	=> array(	'rus' => 'Выводить',
												 											'eng' => 'Display'),
												 					 'Title'	=> array(	'rus' => 'Не выводить',
												 											'eng' => 'Don\'t isplay'),
												 					 ),
												 'descr'	=> array(	'rus' => 'Выводить страницу в списке найденных страниц, если страница без &lt;title&gt;&lt;/title&gt',
												 						'eng' => 'To display page in the list of the found pages, if page without &lt;title&gt;&lt;/title&gt'),
												 'access'	=> 'editable',
												 ),

			   'search_max_rows'		=> array('type'		=> 'integer',
			   									 'defval'	=> '10',
			   									 'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу',
												 						'eng' => 'Maximum quantity of records on page'),
			   									 'access'	=> 'editable'
			   									 ),

			   'search_min_wd_len'		=> array('type'		=> 'integer',
			   									 'defval'	=> '2',
			   									 'descr'	=> array(	'rus' => 'Минимальная длина поискового слова',
												 						'eng' => 'Min search word length'),
			   									 'access'	=> 'editable'
			   									 ),

			   'search_max_wd_cnt'		=> array('type'		=> 'integer',
			   									 'defval'	=> '5',
			   									 'descr'	=> array(	'rus' => 'Максимальное количество поисковых слов',
												 						'eng' => 'Max search word count'),
			   									 'access'	=> 'editable'
			   									 ),

			   'search_specialbegin' 	=> array('type'		=> 'string',
			   									 'defval'	=> '<!--index:begin-->',
			   									 'descr'	=> array(	'rus' => 'Метка указатель начальной позиции индексации на странице',
												 						'eng' => 'Label pointer of index start position on page'),
			   									 'access'	=> 'editable'
			   									 ),

 			   'search_specialend' 		=> array('type'		=> 'string',
			   									 'defval'	=> '<!--index:end-->',
			   									 'descr'	=> array(	'rus' => 'Метка указатель конечной позиции индексации на странице',
												 						'eng' => 'Label pointer of index end position on page'),
			   									 'access'	=> 'editable'
			   									 ),

 			   'search_support_cookies' 		=> array('type'		=> 'select',
												 'defval'	=> array('1'	=> array('rus' => 'Да', 'eng' => 'Yes'),
												 					 '0'	=> array('rus' => 'Нет', 'eng' => 'No'),
												 					 ),
			   									 'descr'	=> array(	'rus' => 'Сохранять cookies при индексации',
												 						'eng' => 'Save cookies for the indexing'),
			   									 'access'	=> 'editable'
			   									 ),

 			   'search_indexed_cnt' 		=> array('type'		=> 'integer',
			   									 'defval'	=> '50',
			   									 'descr'	=> array(	'rus' => 'Кол-во страниц индексируемых за один раз',
												 						'eng' => 'Number of pages indexed at one time'),
			   									 'access'	=> 'editable'
			   									 ),
                'search_sleep_time' 		=> array('type'		=> 'integer',
                        'defval'	=> '0',
                        'descr'	=> array(	'rus' => 'Задержка в секундах, которая будет происходить после индексации каждой страницы',
                                'eng' => 'The delay in seconds, which will occur after indexing each page'),
                        'access'	=> 'editable'
                ),

			   );

$OPEN_NEW = array(
	"search_badurls" 						=> array(".gif",				// если в адресе страницы встречается слово из
													 ".jpg",				// вышеприведеннго массива, то страница не индексируется
													 ".swf",
													 ".zip",
													 ".psd",
													 ".csv",
													 ".pps",
													 ".pdf",
													 "search",
													 "forum",
													 "admin",
													 "404",
													 "map",
													 "nocache",
													 "shop"
													),
);

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'search_untitle' => 'No title',
'search_max_rows' => '12',
'search_min_wd_len' => '1',
'search_max_wd_cnt' => '7',
'search_specialbegin' => '<!--index:begin-->',
'search_specialend' => '<!--index:end-->',
'search_support_cookies' => '1',
'search_indexed_cnt' => '50',
'search_sleep_time' => '0',
/* ABOCMS:END */
);
?>