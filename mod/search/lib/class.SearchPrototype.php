<?php
require_once(RP."inc/rumor/class.RuMor.php");
require_once(RP . "mod/catalog/lib/class.CatalogPrototype.php");

class SearchPrototype extends Module {

	var $main_title;
	var $table_prefix;
	var $addition_to_path;
	var $module_name				= 'search';
	var $default_action				= 'search';
	var $admin_default_action		= 'index';
	var $tpl_path       			= 'mod/search/tpl/';
	var $block_module_actions		= array();
	var $block_main_module_actions	= array();
	
	public $search_query;

	function SearchPrototype($action = '', $transurl = '', $properties = array(), $prefix = NULL, $rewrite_mod_params = NULL) {
		global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $core, $server, $lang, $db, $permissions, $_msg;

		/* Get $action, $tpl_name, $permissions */
		extract( $this->init(array(
		        'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
		        'action' => $action, 'tpl_name' => $properties['tpl']
		)), EXTR_OVERWRITE);
		
		if ('only_create_object' == $action) {
		    return;
		}
		
		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}

		if (! self::is_admin()) {
		    $_msg = &$this->_msg;
//---------------------------------- обработка действий с сайта --------------------------------//
			switch ($action) {
// Вывод результатов поиска
			case 'search':
				global $request_query, $request_start, $request_categoryId, $request_sort,$request_category, $request_amountFrom, $request_amountTo;
				$start = (int)$request_start;
				if (!$start) $start = 1;
				
				if($_GET['type']){
					$search_query = $_GET['query'];  
					$search_query = strip_tags(trim($search_query));
					$search_query = str_replace(array("<", ">", "\"", "'"), "", $search_query);
					$search_query = str_replace('\\', '', $search_query);
					$search_query = my_sql::escape($search_query);
					$str_words = array();
					$words = preg_split("/\s+/", $search_query);
					if ($CONFIG['search_min_wd_len'] > 1 || $CONFIG['search_max_wd_cnt'] > 0) {
					    $a = true;
					    while ($a && (list(,$val) = each($words)) ) {
					    	if ( $CONFIG['search_max_wd_cnt'] > 0 && count($str_words) >= $CONFIG['search_max_wd_cnt']) {
					    	    $a = false;
					    	} else if ($CONFIG['search_min_wd_len'] <= 1 || 
					    	    ($CONFIG['search_min_wd_len'] > 1 && abo_strlen($val) >= $CONFIG['search_min_wd_len'])
					    	) {
					    	    $str_words[] = $val;
					    	}
					    }
					} else {
					    $str_words = $words;
					}
					$rumor = new RuMor(RP."inc/rumor");
					$rumor->prep_dict();
					$clause = array();

					for ($i=0; $i < sizeof($str_words); $i++) {
					    $base = $rumor->get_base_form(iconv("UTF-8", "WINDOWS-1251", $str_words[$i]));
					    $base = abo_strtolower(!empty($base) ? iconv("WINDOWS-1251", "UTF-8", $base[0]) : $str_words[$i]);
					    if (! in_array($base, $clause)) {
					        $clause[] = $base;
					    }
					}

					if (! empty($clause)) {
						$result=array();
						$rel = "MATCH (i.indx) AGAINST ('>\"{$search_query}\" +".implode(" +", $clause)."' IN BOOLEAN MODE)";
						$query = "SELECT SQL_CALC_FOUND_ROWS i.title,i.categoryTitle, {$rel} AS rel FROM "."{$this->table_prefix}_search_page_index as i WHERE {$rel} OR i.title LIKE '%{$search_query}%' OR i.synonyms LIKE '%{$search_query}%' OR i.categoryTitle LIKE '%{$search_query}%' ORDER BY rel LIMIT 10";	 	
						$db->query($query);
				    if ($db->nf()) {
			        while ($db->next_record()){
				         $result[] = array('value'=>$db->Record['title'], 'title' => $db->Record['title'].' - '.$db->Record['categoryTitle']);
			        }
			        echo json_encode($result);
						exit;
			        }
						
					}
					exit;
				}

				
				$main->include_main_blocks($this->module_name.'_results.html', 'main');
				$tpl->prepare();
				$search_query = strip_tags(trim($request_query));
				$search_query = str_replace(array("<", ">", "\"", "'"), "", $search_query);
				$search_query = str_replace('\\', '', $search_query);
				$search_query = my_sql::escape($search_query);
				$tpl->assign(
					array(
						'search_query'	=>	htmlspecialchars($search_query),
						'search_action'	=>	$transurl,
					)
				);
				$amount = '';
				$query.= $request_categoryId ? '&categoryId='.$request_categoryId : '';
				$query.= $request_category && !$request_categoryId ? '&category='.$request_category : '';
				$query.= $request_sort ? '&sort='.$request_sort : '';
				$query.= $request_amountFrom ? '&amountFrom='.$request_amountFrom : '';
				$query.= $request_amountTo ? '&amountTo='.$request_amountTo : '';
				//$query.= $request_start ? '&start='.$request_start : '';
				if (strlen($search_query)>0) {
				
					$this->show_search_results($search_query, $transurl, $start, $pages, $request_categoryId,$request_sort,$request_category,$request_amountFrom,$request_amountTo);
					$main->_show_nav_block( $pages,
											'nav',
											'query='.$search_query.$query,
											$start);

					$this->addition_to_path = $search_query;
					$this->main_title = 'Поиск "'.$search_query.'"';
				}
				break;

			case 'searchform':
				global $request_query,$request_category;
				$main->include_main_blocks($this->module_name.'_form.html', 'main');
				$tpl->prepare();
				$search_query = strip_tags(trim($request_query));
				$search_query = str_replace(array("<", ">", "\"", "'"), "", $search_query);
				$search_query = str_replace('\\', '', $search_query);
				$search_query = my_sql::escape($search_query);
				$catObject = new CatalogPrototype('only_create_object');
				$categorys = $catObject->getTopCategorys();
				if($categorys){
					$tpl->newBlock('block_categories');
					$tpl->assign(array(
							'title' => $request_category ? $categorys[$request_category] : 'Везде',
						));
					foreach($categorys as $key=>$val){
						$tpl->newBlock('block_category');
						$tpl->assign(array(
							'id' => $key,
							'title' => $val
						));
					}
				}
				$tpl->assign(array(
				    'query' => htmlspecialchars($search_query),
				    'search_link' => "{$transurl}&action=search",
				));

				break;

// Default...
			default:
				if (is_object($tpl)) $tpl->prepare();
				return;
			}
//------------------------------- конец обработки действий с сайта -----------------------------//
		} else {
//------------------------------ обработка действий из админ части -----------------------------//
            $this->block_module_actions = array(
                'searchform' => $this->_msg["Search_form"]
            );
            $this->block_main_module_actions = array(
                'search' => $this->_msg["Block_search_results"],
                'searchform' => $this->_msg["Search_form"]
            );

			switch ($action) {
		    case 'prp':
		    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step;
				$main->include_main_blocks('_properties.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Information"	=> $main->_msg["Information"],
					"MSG_Value"			=> $main->_msg["Value"],
					"MSG_Save"			=> $main->_msg["Save"],
					"MSG_Cancel"		=> $main->_msg["Cancel"],
				));
		    if (!$request_step || $request_step == 1) {
					$tpl->assign(array('form_action'	=> $baseurl,
									   'step'			=> 2,
									   'lang'			=> $lang,
									   'name'			=> $this->module_name,
									   'action'			=> 'prp'));
					$prp_html = $main->getModuleProperties($this->module_name);
		      $this->main_title = $this->_msg["Controls"];
				} elseif (2 == $request_step) {
		     	$main->setModuleProperties($this->module_name, $_POST);
					header('Location: '.$baseurl.'&action=prp&step=1');
				}
		    break;

			case "index":
				$SESSION["indexed_urls"] = Array();
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2($this->module_name."_index.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Begin_index" => $this->_msg["Begin_index"],
					"MSG_Confirm_begin_index" => $this->_msg["Confirm_begin_index"],
				));
				$tpl->assign(
					array(
						"_ROOT.indexer_begin"	=>	"$baseurl&action=beginindex",
						"_ROOT.indexer_launch"	=>	"$baseurl&action=makeindex",
					)
				);
				$this->main_title = $this->_msg["Site_indexation"];
				break;

// Приглашение начать индексацию сайта
			case "beginindex":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2($this->module_name."_index_begin.html", $this->tpl_path, "main");
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Begin_index_info" => $this->_msg["Begin_index_info"],
				));
				$host = $CONFIG["host"].($CONFIG['multilanguage'] && $CONFIG['basic_lang'] != $lang
				    ? "?lang=".$lang : '');
				$_SESSION["Spider"]["Tovisit"] = Array();
				$_SESSION["Spider"]["Tovisit"][$host] = 1;
				$_SESSION["Spider"]["Visited"] = Array();
				echo $this->_msg["Indexed"].' : ' . sizeof($_SESSION["Spider"]["Visited"]) . "<br>";
				echo $this->_msg["To_index"].' : ' . sizeof($_SESSION["Spider"]["Tovisit"]) . "<br>";
				$this->main_title = $this->_msg["Site_indexation"];
				break;

// Индексация
			case "makeindex":
			  if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				$tpl->prepare();
				// Массив содержащий проиндексированные url,
				// чтобы не индексировать их повторно
				$GLOBALS["indexed_urls"] = array();
				// Кол-во проиндексированных страниц
				$GLOBALS["number_of_pages"] = 0;
				// Массив содержащий битые ссылки,
				// чтобы не обрабатывать их повторно
				$GLOBALS["broken_urls"] = array();
				// проиндексирована ли стартовая страница
				$GLOBALS["start_page_indexed"] = 0;
				$GLOBALS["referer"] = "/";
				// Время начала индексации
//				$GLOBALS["begin_time"] = date("Y-m-d G:i:s");
				// очистить временную таблицу
				if(empty($_SESSION["Spider"]["Visited"])) {
					//echo 'Clear tmp_table.<br>';
					$this->recreate_tmp_tables();
				}
				set_time_limit(0);
				// запуск indexer'а
				$this->index_window_header();
				$time_start = $main->getmicrotime();
//				error_reporting(E_ALL);
				foreach($_SESSION["Spider"]["Visited"] as $k => $v){
					unset($_SESSION["Spider"]["Tovisit"][$k]);
				}
				echo $this->_msg["Indexed"]. ': ' . sizeof($_SESSION["Spider"]["Visited"]) . "<br>";
				echo $this->_msg["To_index"] . ': ' . sizeof($_SESSION["Spider"]["Tovisit"]) . "<br>";
				$stop = $CONFIG['search_indexed_cnt'];
				foreach($_SESSION["Spider"]["Tovisit"] as $k => $v) {
					$stop--;
					if($stop < 1)
                    break;

					$I = new cindexer($k, $this->_msg);
					if ($CONFIG['search_sleep_time'] > 0){
					    sleep($CONFIG['search_sleep_time']);
					}
					unset($_SESSION["Spider"]["Tovisit"][$k]);
					$_SESSION["Spider"]["Visited"][$k] = 1;
				}
				// поменять временную и текущую поисковые таблицы местами
				if(empty($_SESSION["Spider"]["Tovisit"]))
				{
					$time_end = $main->getmicrotime();
					print "<br><br>" . $this->_msg["Totally_indexed"] . ": " . sizeof($_SESSION["Spider"]["Visited"]) . "<br>";
					print $this->_msg["Indexation_time"] . ": " . number_format(($time_end - $time_start)/60,2) . " " . $this->_msg["min"];
					$this->index_window_footer();
					$this->rename_tables();
				}
				else
				{
					$this->index_window_footer();
					echo '<script>document.location.href = document.location.href;</script>';
				}
				exit;
				break;

// Вывод списка битых ссылок
			case 'blinks':
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_start;
				$start = ($request_start) ? (int)$request_start : 1;
				$order_by = 'id ASC';
				$main->include_main_blocks_2($this->module_name.'_blinks.html', $this->tpl_path);
				$tpl->prepare();

				$main->_show_nav_string($server.$lang.'_search_broken_links', '', '', 'action='.$action, $start, $CONFIG['search_max_rows'], $order_by);
				$tpl->assign(Array(
					"MSG_Pages" => $this->_msg["Pages"],
				));
				$this->show_broken_links($start, $CONFIG['search_max_rows'], $order_by);

				$tpl->assign(Array(
					"block_broken_links.MSG_Broken_link" => $this->_msg["Broken_link"],
					"block_broken_links.MSG_On_the_page" => $this->_msg["On_the_page"],
					"_ROOT.MSG_Broken_links_list_info" => $this->_msg["Broken_links_list_info"],
				));

				$this->main_title = $this->_msg["Broken_links"];
				break;

// Редактирование свойств (параметров) блока
			case "properties":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);

				global $module_id, $request_field, $title;
				$main->include_main_blocks("_block_properties.html", "main");
				$tpl->prepare();

				if ($main->show_actions($request_field, "", $this->block_main_module_actions, $this->block_module_actions)) {
					$main->show_linked_pages($module_id, $request_field);
				}

				$tpl->assign("_ROOT.title", $title);
				$this->main_title = $this->_msg["Block_properties"];
				break;

// Создание объекта для дальнейшего использования
			case 'only_create_object':
				break;

// Default...
			default:
				$main->include_main_blocks();
				$tpl->prepare();
			}
//--------------------------- конец обработки действий из админ части --------------------------//
		}
	}

	// вывод результатов поиска
	function show_search_results($search_query, $transurl, $start = "", &$pages, $category='', $sort='',$categoryIn='',$amountFrom='',$amountTo='',$labelSearch = true) {
		global $CONFIG, $db, $tpl, $server, $lang, $baseurl;
		
		$ret = false;
		$sortValue = $sort ? 'rel,'.htmlspecialchars($sort) : 'rel,hits DESC';
		if($sort=='p.price'){
			$sortValue .=' ASC';
		}
		elseif($sort=='p.hits'){
			$sortValue .=' DESC';
		}
		elseif($sort=='rel'){
			$sortValue .=' DESC';
		}
		elseif($sort=='product_title'){
			$sortValue .=' ASC';
		}
		$price = '';
		if($amountFrom || $amountTo){
			$price .=  $amountFrom ? ' AND p.price_1 >='.$amountFrom : '';
			$price .=  $amountTo ? ' AND p.price_1 <='.$amountTo : '';
				}
		$rows	= $CONFIG['search_max_rows'];
		$start_row	= ($start - 1) * $rows;
		$search_res = array();
		$search_url = array();
		$str_words = array();
		if(strlen($search_query)>7){
						$search_queryR = str_replace('supra ', '', $search_query);
						$search_queryR = str_replace('SUPRA ', '', $search_queryR);
						$search_queryR = str_replace('Supra ', '', $search_queryR);
					}
					else{
						$search_queryR = $search_query;
					}
		$words = preg_split("/\s+/", $search_queryR);
		if ($CONFIG['search_min_wd_len'] > 1 || $CONFIG['search_max_wd_cnt'] > 0) {
		    $a = true;
		    while ($a && (list(,$val) = each($words)) ) {
		    	if ( $CONFIG['search_max_wd_cnt'] > 0 && count($str_words) >= $CONFIG['search_max_wd_cnt']) {
		    	    $a = false;
		    	} else if ($CONFIG['search_min_wd_len'] <= 1 || 
		    	    ($CONFIG['search_min_wd_len'] > 1 && abo_strlen($val) >= $CONFIG['search_min_wd_len'])
		    	) {
		    	    $str_words[] = $val;
		    	}
		    }
		} else {
		    $str_words = $words;
		}
		$request_string = '/search/?query='.$search_query;
		//$request_string .= $start ? '&start='.$start : '';
		$rumor = new RuMor(RP."inc/rumor");
		$rumor->prep_dict();
		$clause = array();
		for ($i=0; $i < sizeof($str_words); $i++) {
		    $base = $rumor->get_base_form(iconv("UTF-8", "WINDOWS-1251", $str_words[$i]));
		    $base = abo_strtolower(!empty($base) ? iconv("WINDOWS-1251", "UTF-8", $base[0]) : $str_words[$i]);
		    if (! in_array($base, $clause)) {
		        $clause[] = $base;
		    }
		}

		if (! empty($clause)) {
			$wheres = '';
			if($category){
				$category != 'all' ? $wheres .= ' AND categoryId='.$category : $wheres .='';
			}else{
				$catObject = new CatalogPrototype('only_create_object');
				$child_ids = $catObject->getChildIds($categoryIn);
				$categoryIn ? $categorys = (count($child_ids) ? ', '.implode(', ', $child_ids).')' : ')').($active ? " AND active" : "") : $categorys=array();
				$categoryIn ? $wheres.= ' AND categoryId IN('.$categoryIn.$categorys : $wheres .='';
			}
			 $s_word ='';
			foreach($clause as $word){
				$s_word .= " OR i.synonyms LIKE '%{$word}%' OR i.categoryTitle LIKE '%{$word}%'";
			}
			
			$rel =  "(MATCH (i.body, i.indx, i.title) AGAINST ('>\"{$search_queryR}\" +".implode(" +", $clause)."' IN BOOLEAN MODE) OR i.title LIKE '%{$search_queryR}%' OR i.synonyms LIKE '%{$search_queryR}%' OR i.categoryTitle LIKE '%{$search_queryR}%' ".$s_word.")";
		    $query = "SELECT SQL_CALC_FOUND_ROWS categoryId, categoryTitle, {$rel} AS rel FROM "
		        ."{$this->table_prefix}_search_page_index as i 
		        LEFT JOIN ".$this->table_prefix."_catalog_products p ON i.productId = p.id
		        WHERE {$rel}{$wheres}{$price} Group BY categoryId ORDER BY categoryTitle";	
		        
		    $db->query($query);
		    if ($db->nf()) {
			    $tpl->newBlock('block_search_results');
			    $tpl->newBlock('block_search_categories');
			    $tpl->assign(array(
					 'link'	=> $request_string.'&categoryId=all'
					 )
				 );
				 
			 while ($db->next_record($query)){
				 if($i==0 && $i++){
					 if($category && $category!='all'){
					$search_query = $category != 'all' ? '«<b>'.htmlspecialchars($search_query).'</b>»'.' в категории <b>'.$db->Record['categoryTitle'].'</b>' : '«<b>'.htmlspecialchars($search_query).'</b>»';
				}
			}

				 $tpl->newBlock('block_search_category');
				 $tpl->assign(array(
					 'title' => $db->Record['categoryTitle'],
					 'link'	=> $request_string.'&categoryId='.$db->Record['categoryId']
					 )
				 );
			 }
			 
		    }
		    else{
			 if($labelSearch){
				$this->search_query='«<b>'.$search_query.'</b>»';
				if(preg_match("/^[a-zA-Z]+$/", $search_query) == 1) {
					$this->show_search_results($this->switcher($search_query),$transurl, $start, &$pages, $category, $sort,$categoryIn,$amountFrom,$amountTo,false);
				}
				else{
					$this->show_search_results($this->switcher($search_query,1),$transurl, $start, &$pages, $category, $sort,$categoryIn,$amountFrom,$amountTo,false);
				}
				

				return ;
				}
		    }
			
		    $query = "SELECT SQL_CALC_FOUND_ROWS p.image_middle, p.hits, p.price_1, i.url, i.title, i.descr, {$rel} AS rel FROM "
		        ."{$this->table_prefix}_search_page_index as i
		        LEFT JOIN ".$this->table_prefix."_catalog_products p ON i.productId = p.id
		        WHERE {$rel}{$wheres}{$price}  ORDER BY {$sortValue}"
		        ." LIMIT ".$start_row.",".$rows;	 	
	
		    $db->query($query);
		    $i = $start_row;
		    if ($db->nf()) {
		        $ret = true;
		        while ($db->next_record($query)){
			        
        			$tpl->newBlock("block_search_result_item");
    				$url = '/'.$db->Record['url'];
    				$markwords_url = (strstr($url, '?')) ? $url.'&mark='.$search_query : $url.'?mark='.$search_query;
    				$tpl->assign(array(
    					'search_url' =>	$url,
    					'domen' => $CONFIG['domen'],
    					'search_markwords_url' =>	$markwords_url,
    					'search_title' =>	strip_tags($db->Record['title']), 
    					'search_descr' =>	$db->Record['descr'],
    					'image' =>	$db->Record['image_middle'],
    					'price' =>	$db->Record['price_1']!=0 ? (int)$db->Record['price_1'].' руб.' : 'нет в наличии',
    					'hits' =>	$db->Record['hits'],
        			    'rel' => $db->Record["rel"],
        			    'i' => ++$i
    				));
		        }
    			$db->query("SELECT COUNT(*) FROM {$this->table_prefix}_search_page_index as i LEFT JOIN ".$this->table_prefix."_catalog_products p ON i.productId = p.id WHERE {$rel}{$wheres}{$price} ");
    			$db->next_record($query);
    			$pages = ceil($db->Record[0] / $rows);
		    }			
		}
		$search_query = strpos('/b',$search_query) ? $search_query : '«<b>'.$search_query.'</b>»';
		$request_string = $category ? $request_string.'&categoryId='.$category : $request_string;
		$request_string = $categoryIn && !$category ? $request_string.'&category='.$categoryIn : $request_string;
		$request_string = $start ? $request_string.'&start='.$start : $request_string;
		
		$request_string_sprice = $sort ? $request_string.'&sort='.$sort : $request_string;
		
		$request_string = $amountFrom ? $request_string.'&amountFrom='.$amountFrom : $request_string;
		$request_string = $amountTo ? $request_string.'&amountTo='.$amountTo : $request_string;
		
		$tpl->goToBlock('block_search_results');
		
		$params=array(
		'count'=>$db->Record[0],
		'search_query' => $search_query,
		'link_sort' =>$request_string,
		'link_price' =>$request_string_sprice,
		'amount_from' =>$amountFrom ? $amountFrom : 0,
		'amount_to' =>$amountTo ? $amountTo : 80000,
		);
		$params[str_replace('.','_',$sort)] = 'selected="selected"';
		$cat = !$category || $category=='all' ? ', <b>уточните категорию</b>' : '';
		$params['cat'] =  $cat;
		$tpl->assign($params);
		if (!$ret) {
			$tpl->newBlock('block_no_success');
		    $tpl->assign('search_query', $this->search_query);
		}		
		return $ret;
	}
	function switcher ($text,$reverse=false) {
	  $str[0] = array(
	    "й","ц","у","к","е","н","г","ш","щ","з","х","ъ",
	    "ф","ы","в","а","п","р","о","л","д","ж","э",
	    "я","ч","с","м","и","т","ь","б","ю"
	  );
	  $str[1]= array(
	    "q","w","e","r","t","y","u","i","o","p","[","]",
	    "a","s","d","f","g","h","j","k","l",";","'",
	    "z","x","c","v","b","n","m",",","."
	  );
	  $out = array();
	  foreach($str[0] as $i=>$key){
	    $out[0][$i] =  '#'.str_replace(array('.',']','['),array('\.','\]','\['),  $str[ $reverse ? 0:1][$i]).'#ui';
	    $out[1][$i] =  $str[$reverse ? 1:0][$i] ;
	  };
	  return preg_replace($out[0], $out[1], $text);
	}
	function index_window_header() {
		global $CONFIG, $lang;

		$date = date("d/m/Y G:i:s");
		$sitename = $CONFIG["sitename"];
		print "<html>
		<head>
		<LINK REL=STYLESHEET HREF=\"css/a.css\" TYPE=\"text/css\">
		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1251\">
		<META HTTP-EQUIV=\"pragma\" CONTENT=\"no-cache\">
		<meta http-equiv=\"expires\" content=\"-1\">
		</head>
		<body>
		<table><tr><td> <b>" . $this->_msg["Indexation_log_of"] . " $sitename ($date):</b><br><br>";
	}

	function index_window_footer() {
		print "</td>
		</tr></table>
		</body>
		</html>";
	}


	// функция для перестановки местами текущих и временных таблиц
	function rename_tables() {
		global $CONFIG, $db, $server, $lang;
		// удаляем временные таблицы если остались
		$db->query("DROP TABLE IF EXISTS ".$this->table_prefix."_search_old_page_index");
		$db->query("DROP TABLE IF EXISTS ".$this->table_prefix."_search_old_broken_links");
		// создадим индекс для поля body во временной таблице search_tmp_page_index с только что проиндексированными страницами
		$db->query("ALTER TABLE ".$server.$lang."_search_tmp_page_index ADD FULLTEXT INDEX search_index (body, indx)");
		// переименуем текущую поисковую таблицу search_page_index в search_old_page_index
		$db->query("ALTER TABLE ".$server.$lang."_search_page_index RENAME TO ".$this->table_prefix."_search_old_page_index");
		// переименуем текущую таблицу с битыми ссылками search_broken_links в search_old_broken_links
		$db->query("ALTER TABLE ".$server.$lang."_search_broken_links RENAME TO ".$this->table_prefix."_search_old_broken_links");
		//  сделаем временную таблицу search_tmp_page_index текущей поисковой таблицей search_page_index
		$db->query("ALTER TABLE ".$server.$lang."_search_tmp_page_index RENAME TO ".$server.$lang."_search_page_index");
		//  сделаем временную таблицу search_tmp_broken_links текущей таблицей с битыми ссылками search_broken_links
		$db->query("ALTER TABLE ".$server.$lang."_search_tmp_broken_links RENAME TO ".$server.$lang."_search_broken_links");
		// старую поисковую таблицу search_old_page_index переименуем во временную search_tmp_page_index
		$db->query("ALTER TABLE ".$this->table_prefix."_search_old_page_index RENAME TO ".$server.$lang."_search_tmp_page_index");
		$db->query("ALTER TABLE ".$server.$lang."_search_tmp_page_index DROP INDEX search_index");
		// старую таблицу с битыми ссылками search_old_broken_links переименуем во временную search_tmp_broken_links
		$db->query("ALTER TABLE ".$this->table_prefix."_search_old_broken_links RENAME TO ".$server.$lang."_search_tmp_broken_links");
		// удалим данные из временных таблиц
		$this->clear_tmp_table();
		return TRUE;
	}


	// функция для очистки временных таблиц
	function clear_tmp_table() {
		global $CONFIG, $db, $server, $lang;
		// удалим из search_tmp_page_index все данные
		$query = "DELETE FROM ".$server.$lang."_search_tmp_page_index";
		$db->query($query);
		// удалим из search_tmp_broken_links все данные
		$query = "DELETE FROM ".$server.$lang."_search_tmp_broken_links";
		$db->query($query);
		return TRUE;
	}

	function recreate_tmp_tables()
	{
		global $db;
		$db->query("DROP TABLE IF EXISTS {$this->table_prefix}_search_tmp_broken_links");
		$db->query("DROP TABLE IF EXISTS {$this->table_prefix}_search_tmp_page_index");
		$prefix = $this->table_prefix;
		require(RP."mod/search/sql/search_tables.php");
		foreach ($MOD_TABLES as $tbl) {
			if (false !== strpos($tbl, $this->table_prefix.'_search_tmp_broken_links')
			    || false !== strpos($tbl, $this->table_prefix.'_search_tmp_page_index')
			) {
			    $db->query($tbl);
			}
		}
	}

	// Функция для вывода списка битых ссылок
	function show_broken_links($start, $rows, $order_by) {
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;
		$start_row = ($start - 1) * $rows;
		$query = "SELECT *
					FROM ".$server.$lang."_search_broken_links
					ORDER BY $order_by
					LIMIT $start_row,$rows";
		$db->query($query);
		if ($db->nf() > 0) {
			$tpl->newBlock("block_broken_links");
			while($db->next_record()) {
				$url = (substr($db->f("url"),0,1) != "/") ? "/" . $db->f("url") : $db->f("url");
				$referer = (substr($db->f("referer"),0,1) != "/") ? "/" . $db->f("referer") : $db->f("referer");

				// вывод на экран списка
				$tpl->newBlock("block_broken_link");
				$tpl->assign(
					array(
						broken_link_id	=>	$db->f("id"),
						broken_link	=>	$url,
						broken_link_referer	=>	$referer,
					)
				);
			}
			return 1;
		}
		return 0;
	}
}

/* CLASSES OF INDEXER */
 class chttp
  { var $host_name="";
    var $host_port=80;
    var $proxy_host_name="";
    var $proxy_host_port=80;

    var $request_method="GET";
    var $user_agent="Manuel Lemos HTTP class test script";
    var $request_uri="";
    var $protocol_version="1.0";
    var $debug=0;
    var $support_cookies=1;
    var $cookies=array();

    /* private variables - DO NOT ACCESS */
    var $state="Disconnected";
    var $connection=0;
    var $content_length=0;
    var $read_length=0;
    var $request_host="";
    var $months=array("Jan"=>"01",
                      "Feb"=>"02",
                      "Mar"=>"03",
                      "Apr"=>"04",
                      "May"=>"05",
                      "Jun"=>"06",
                      "Jul"=>"07",
                      "Aug"=>"08",
                      "Sep"=>"09",
                      "Oct"=>"10",
                      "Nov"=>"11",
                      "Dec"=>"12");

	/* Private methods - DO NOT CALL */
	Function OutputDebug($message) {
		echo $message,"\n";
	}
	Function GetLine() {
		for($line="";;) {
			if(feof($this->connection)
			|| !($part=fgets($this->connection,100)))
				return(0);
			$line.=$part;
			$length=strlen($line);
			if($length>=2 && substr($line,$length-2,2)=="\r\n") {
				$line=substr($line,0,$length-2);
				if($this->debug)
					$this->OutputDebug("< $line");
				return($line);
			}
		}
	}

	Function PutLine($line) {
		if($this->debug) $this->OutputDebug("> $line");
		return(fputs($this->connection,"$line\r\n"));
	}

	Function PutData($data) {
		if($this->debug)
			$this->OutputDebug("> $data");
		return(fputs($this->connection,$data));
	}

	Function Readbytes($length)	{
		if($this->debug) {
			if(($bytes=fread($this->connection,$length))!="")
				$this->OutputDebug("< $bytes");
			return($bytes);
		}
		else
			return(fread($this->connection,$length));
	}

	Function EndOfInput()	{
		return(feof($this->connection));
	}

	Function Connect($host_name,$host_port)	{
		if($this->debug)
			$this->OutputDebug("Connecting to $host_name...");
		if(($this->connection=fsockopen($host_name,$host_port,&$error))==0)	{
			switch($error) {
				case -3:
					return("-3 socket could not be created");
				case -4:
					return("-4 dns lookup on hostname \"".$host_name."\" failed");
				case -5:
					return("-5 connection refused or timed out");
				case -6:
					return("-6 fdopen() call failed");
				case -7:
					return("-7 setvbuf() call failed");
				default:
					return($error." could not connect to the host \"".$host_name."\"");
			}
		}	else {
			if($this->debug)
				$this->OutputDebug("Connected to $host_name");
			$this->state="Connected";
			return("");
		}
	}

	Function Disconnect()	{
		if($this->debug)
			$this->OutputDebug("Disconnected from ".$this->host_name);
		fclose($this->connection);
		return("");
	}

	/* Public methods */

	Function Open($arguments) {
		if($this->state!="Disconnected")
			return("1 already connected");
		if(IsSet($arguments["HostName"]))
			$this->host_name=$arguments["HostName"];
		if(IsSet($arguments["HostPort"]))
			$this->host_port=$arguments["HostPort"];
		if(IsSet($arguments["ProxyHostName"]))
			$this->proxy_host_name=$arguments["ProxyHostName"];
		if(IsSet($arguments["ProxyHostPort"]))
			$this->proxy_host_port=$arguments["ProxyHostPort"];
		if(strlen($this->proxy_host_name)==0) {
			if(strlen($this->host_name)==0)
				return("2 it was not specified a valid hostname");
			$host_name=$this->host_name;
			$host_port=$this->host_port;
		} else {
			$host_name=$this->proxy_host_name;
			$host_port=$this->proxy_host_port;
		}
		$error=$this->Connect($host_name,$host_port);
		if(strlen($error)==0)
			$this->state="Connected";
		return($error);
	}

	Function Close() {
		if($this->state=="Disconnected")
			return("1 already disconnected");
		$error=$this->Disconnect();
		if(strlen($error)==0)
			$this->state="Disconnected";
		return($error);
	}

	Function SendRequest($arguments) {
		switch($this->state) {
			case "Disconnected":
				return("1 connection was not yet established");
			case "Connected":
				break;
			default:
				return("2 can not send request in the current connection state");
		}
		if(IsSet($arguments["RequestMethod"]))
			$this->request_method=$arguments["RequestMethod"];
		if(IsSet($arguments["User-Agent"]))
			$this->user_agent=$arguments["User-Agent"];
		if(strlen($this->request_method)==0)
			return("3 it was not specified a valid request method");
		if(IsSet($arguments["RequestURI"]))
			$this->request_uri=$arguments["RequestURI"];
		if(strlen($this->request_uri)==0
		|| substr($this->request_uri,0,1)!="/")
			return("4 it was not specified a valid request URI");
		$request_body="";
		$headers=(IsSet($arguments["Headers"]) ? $arguments["Headers"] : array());
		if($this->request_method=="POST") {
			if(IsSet($arguments["PostValues"])) {
				$values=$arguments["PostValues"];
				if(GetType($values)!="array")
					return("5 it was not specified a valid POST method values array");
				for($request_body="",Reset($values),$value=0;$value<count($values);Next($values),$value++) {
					if($value>0)
						$request_body.="&";
					$request_body.=Key($values)."=".UrlEncode($values[Key($values)]);
				}
				$headers["Content-type"]="application/x-www-form-urlencoded";
			}
		}
		if(strlen($this->proxy_host_name)==0)
			$request_uri=$this->request_uri;
		else
			$request_uri="http://".$this->host_name.($this->host_port==80 ? "" : ":".$this->host_port).$this->request_uri;
		if(($success=$this->PutLine($this->request_method." ".$request_uri." HTTP/".$this->protocol_version))) {
			if(($body_length=strlen($request_body)))
				$headers["Content-length"]=$body_length;
			for($host_set=0,Reset($headers),$header=0;$header<count($headers);Next($headers),$header++) {
				$header_name=Key($headers);
				$header_value=$headers[$header_name];
				if(GetType($header_value)=="array") {
					for(Reset($header_value),$value=0;$value<count($header_value);Next($header_value),$value++) {
						if(!$success=$this->PutLine("$header_name: ".$header_value[Key($header_value)]))
							break 2;
					}
				}	else {
					if(!$success=$this->PutLine("$header_name: $header_value"))
						break;
				}
				if(strtolower(Key($headers))=="host") {
					$this->request_host=strtolower($header_value);
					$host_set=1;
				}
			}
			if($success) {
				if(!$host_set) {
					$success=$this->PutLine("Host: ".$this->host_name);
					$this->request_host=strtolower($this->host_name);
				}
				if(count($this->cookies) && IsSet($this->cookies[0]))	{
					$now=gmdate("Y-m-d H-i-s");
					for($cookies=array(),$domain=0,Reset($this->cookies[0]);$domain<count($this->cookies[0]);Next($this->cookies[0]),$domain++)	{
						$domain_pattern=Key($this->cookies[0]);
						$match=strlen($this->request_host)-strlen($domain_pattern);
						if($match>=0 && !strcmp($domain_pattern,substr($this->request_host,$match)) && ($match==0 || $domain_pattern[0]=="." || $this->request_host[$match-1]==".")) {
							for(Reset($this->cookies[0][$domain_pattern]),$path_part=0;$path_part<count($this->cookies[0][$domain_pattern]);Next($this->cookies[0][$domain_pattern]),$path_part++) {
								$path=Key($this->cookies[0][$domain_pattern]);
								if(strlen($this->request_uri)>=strlen($path) && substr($this->request_uri,0,strlen($path))==$path) {
									for(Reset($this->cookies[0][$domain_pattern][$path]),$cookie=0;$cookie<count($this->cookies[0][$domain_pattern][$path]);Next($this->cookies[0][$domain_pattern][$path]),$cookie++) {
										$cookie_name=Key($this->cookies[0][$domain_pattern][$path]);
										$expires=$this->cookies[0][$domain_pattern][$path][$cookie_name]["expires"];
										if($expires==""
										|| strcmp($now,$expires)<0)
											$cookies[$cookie_name]=$this->cookies[0][$domain_pattern][$path][$cookie_name];
									}
								}
							}
						}
					}
					for(Reset($cookies),$cookie=0;$cookie<count($cookies);Next($cookies),$cookie++)
					{
						$cookie_name=Key($cookies);
						if(!($success=$this->PutLine("Cookie: ".UrlEncode($cookie_name)."=".$cookies[$cookie_name]["value"].";")))
							break;
					}
				}
				if($success)
				{
					if($success)
					{
						$success=$this->PutLine("");
						if($body_length
						&& $success)
							$success=$this->PutData($request_body);
					}
				}
			}
		}
		if(!$success)
			return("5 could not send the HTTP request");
		$this->state="RequestSent";
		return("");
	}

	Function ReadReplyHeaders(&$headers)
	{
		switch($this->state)
		{
			case "Disconnected":
				return("1 connection was not yet established");
			case "Connected":
				return("2 request was not sent");
			case "RequestSent":
				break;
			default:
				return("3 can not get request headers in the current connection state");
		}
		$headers=array();
		$this->content_length=$this->read_length=0;
		$this->content_length_set=0;
		for(;;)
		{
			$line=$this->GetLine();
			if(GetType($line)!="string")
				return("4 could not read request reply");
			if($line=="")
			{
				$this->state="GotReplyHeaders";
				return("");
			}
			$header_name=strtolower(strtok($line,":"));
			$header_value=Trim(Chop(strtok("\r\n")));
			if(IsSet($headers[$header_name]))
			{
				if(GetType($headers[$header_name])=="string")
					$headers[$header_name]=array($headers[$header_name]);
				$headers[$header_name][]=$header_value;
			}
			else
				$headers[$header_name]=$header_value;
			switch($header_name)
			{
				case "content-length":
					$this->content_length=intval($headers[$header_name]);
					$this->content_length_set=1;
					break;
				case "set-cookie":
					if($this->support_cookies)
					{
						$cookie_name=trim(strtok($headers[$header_name],"="));
						$cookie_value=strtok(";");
						$domain=$this->request_host;
						$path="/";
						$expires="";
						$secure=0;
						while(($name=strtolower(trim(strtok("="))))!="")
						{
							$value=UrlDecode(strtok(";"));
							switch($name)
							{
								case "domain":
									if($value==""
									|| !strpos($value,".",$value[0]=="."))
										break;
									$domain=strtolower($value);
									break;
								case "path":
									if($value!=""
									&& $value[0]=="/")
										$path=$value;
									break;
								case "expires":
									if(preg_match("/^((Mon|Monday|Tue|Tuesday|Wed|Wednesday|Thu|Thursday|Fri|Friday|Sat|Saturday|Sun|Sunday), )?([0-9]{2})\\-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\\-([0-9]{2,4}) ([0-9]{2})\\:([0-9]{2})\\:([0-9]{2}) GMT$/i",$value,$matches))
									{
										$year=intval($matches[5]);
										if($year<1900)
											$year+=($year<70 ? 2000 : 1900);
										$expires="$year-".$this->months[$matches[4]]."-".$matches[3]." ".$matches[6].":".$matches[7].":".$matches[8];
									}
									break;
								case "secure":
									$secure=1;
									break;
							}
						}
						$this->cookies[$secure][$domain][$path][$cookie_name]=array(
							"name"=>$cookie_name,
							"value"=>$cookie_value,
							"domain"=>$domain,
							"path"=>$path,
							"expires"=>$expires,
							"secure"=>$secure
						);
					}
			}
		}
	}

	Function ReadReplyBody(&$body,$length)
	{
		switch($this->state)
		{
			case "Disconnected":
				return("1 connection was not yet established");
			case "Connected":
				return("2 request was not sent");
			case "RequestSent":
				if(($error=$this->ReadReplyHeaders(&$headers))!="")
					return($error);
				break;
			case "GotReplyHeaders":
				break;
			default:
				return("3 can not get request headers in the current connection state");
		}
		$body="";
		if($this->content_length_set)
			$length=min($this->content_length-$this->read_length,$length);
		if($length>0
		&& !$this->EndOfInput()
		&& ($body=$this->ReadBytes($length))=="")
			return("4 could not get the request reply body");
		return("");
	}

	Function GetPersistentCookies(&$cookies)
	{
		$now=gmdate("Y-m-d H-i-s");
		$cookies=array();
		for($secure_cookies=0,Reset($this->cookies);$secure_cookies<count($this->cookies);Next($this->cookies),$secure_cookies++)
		{
			$secure=Key($this->cookies);
			for($domain=0,Reset($this->cookies[$secure]);$domain<count($this->cookies[$secure]);Next($this->cookies[$secure]),$domain++)
			{
				$domain_pattern=Key($this->cookies[$secure]);
				for(Reset($this->cookies[$secure][$domain_pattern]),$path_part=0;$path_part<count($this->cookies[$secure][$domain_pattern]);Next($this->cookies[$secure][$domain_pattern]),$path_part++)
				{
					$path=Key($this->cookies[$secure][$domain_pattern]);
					for(Reset($this->cookies[$secure][$domain_pattern][$path]),$cookie=0;$cookie<count($this->cookies[$secure][$domain_pattern][$path]);Next($this->cookies[$secure][$domain_pattern][$path]),$cookie++)
					{
						$cookie_name=Key($this->cookies[$secure][$domain_pattern][$path]);
						$expires=$this->cookies[$secure][$domain_pattern][$path][$cookie_name]["expires"];
						if($expires!=""
						&& strcmp($now,$expires)<0)
							$cookies[$secure][$domain_pattern][$path][$cookie_name]=$this->cookies[$secure][$domain_pattern][$path][$cookie_name];
					}
				}
			}
		}
	}

}



class ctags
  { var $text; // result of strip
    var $html; // html document
    var $search=array ("'<script[^>]*?>.*?</script>'si",
                       "'<style[^>]*?>.*?</style>'si",
                       "'<!--.*?-->'si",
                       "'<[\/\!]*?[^<>]*?>'si",
                       //"'([\r\n])[\s]+'",
                       "'\s+'",
                       "'&(quote|#34);'i",
                       "'&(quot|#34);'i",
                       "'&(amp|#38);'i",
                       "'&(lt|#60);'i",
                       "'&(gt|#62);'i",
                       "'&(nbsp|#160);'i",
                       "'&(iexcl|#161);'i",
                       "'&(cent|#162);'i",
                       "'&(pound|#163);'i",
                       "'&(copy|#169);'i",
                       "'&#(\d+);'e");
    var $replace = array (" ",
                          " ",
                          " ",
                          " ",
                          " ",
                          " ",
                          " ",
                          " ",
                          " ",
                          " ",
                          " ",
                          " ",
                          " ",
                          " ",
                          " ",
                          " ");

  function ctags($document="")
   { if (trim(chop($document))=="")
      return;
     $this->html=$document;
     $this->text=preg_replace($this->search,$this->replace,$this->html);
     return $this->text;
   }
}





class cindexer {
	var $host;

	// занесение в БД битых ссылок
	function process_broken_link($host, $url) {
		global $CONFIG, $db, $server, $lang;
		if ($_SESSION["Spider"]["Referer"][$host]) {
			$referer = mysql_real_escape_string($_SESSION["Spider"]["Referer"][$host]);
		}
		$query = "INSERT INTO ".$server.$lang."_search_tmp_broken_links (referer, url) VALUES ('{$referer}', '".mysql_real_escape_string($url)."')";
		$db->query($query);
		$GLOBALS["broken_urls"][] = $host;
		print "<a href=\"$url\" target=_blank>$url</a> <font color=\"red\">404 page not found</font><br>\n";
		return;
	}


  function cindexer($host, $_msg = false) {
		global $db, $main, $lc, $PHP_SELF, $CONFIG, $server, $lang, $_msg;
		static $indexed_urls = array();
		static $number_of_pages = 0;
        
		$_msg = $_msg ? $_msg : $GLOBALS['_msg'];
		$url = str_replace($CONFIG["host"],"",$host);
		
		$sess = strstr($url, 'PHPSESSID');
		if ($sess) {
			$url = str_replace("?" . $sess, "", $url);
			$url = str_replace("&" . $sess, "", $url);
		}

		// не индексировать повторно стартовую страницу
		if (("" == $url || "?lang=".$lang == $url) && $GLOBALS["start_page_indexed"]) {
			return;
		} else {
			$GLOBALS["start_page_indexed"] = 1;
		}

		
		$this->host = $host;
		// http class init
		$h=new chttp;
		$h->support_cookies = (int)$CONFIG['search_support_cookies'];
		// url parse
		$p = parse_url($host);
		if (!$p["path"]) {
			$p["path"] = "/";
		}
		// query
		if ($p["query"]) {
			$p["path"].="?".$p["query"];
		}
		$error=$h->Open(array("HostName"=>$p["host"]));
		// check if in black urls list
		for ($b=0;$b<count($CONFIG["search_badurls"]);$b++) {
			if (strstr($url, $CONFIG["search_badurls"][$b])) {
				$error="Host: $host is in bad urls list!";
				return;
			}
		}
		$rumor = new RuMor(RP."inc/rumor");
		$rumor->prep_dict();
		if ($error=="") {
			$error=$h->SendRequest(array( "RequestURI"=>$p["path"], "Headers"=>array("Host"=>$p["host"], "User-Agent"=>"Mozilla/4.0 (compatible; MSIE 5.0; Windows 98; DigExt)", "Pragma"=>"no-cache")));
			if ($error=="") {
				$fbody="";
				if ($error=="") {
				$headers=array();
				$error=$h->ReadReplyHeaders(&$headers);
				if (is_array($headers)) {
					reset($headers);
					$p=0;
					while(list($k,$v)=each($headers)) {
						if (strstr($k,"404") && $p == 0) {
							$this->process_broken_link($host, $url);
							return;
						}
						if ($k == 'content-type' && false === strpos($v, 'text/html')) {
						    return;
						}
    	            }
        	    }
				for(;;) {
					$error=$h->ReadReplyBody(&$body,1000);
					if ($error!="" || strlen($body)==0)
						break;
						$fbody .= $body;
					}

					// connection close
					$h->Close();
					$urls = $lc->get_internal_links($fbody);
					//title
					$obody=str_replace("\n"," ",$fbody);
					if (preg_match("/<title>(.*)<\/title>/usi",$obody,$arr)) {
						$title=$arr[1];
						// битая ссылка (изменения 15.04.2004):
						if ($title == "Error 404") {
							$this->process_broken_link($host, $url);
							return;
						}
					} else {
						$title=$CONFIG["search_untitle"];
					}
					$tags=new ctags();
					// descr
					
					if (preg_match("/".$CONFIG["search_specialbegin"]."(.*)/usi",$obody,$arr)) {
						
						if (preg_match("/(.*)".$CONFIG["search_specialend"]."/usi",$arr[1],$arr)) {
							$descr=substr($tags->ctags($arr[1]),0,250)."...";
							$fbody=$arr[1];
						} else {
							$descr=substr($tags->ctags($arr[1]),0,250)."...";
							$fbody=$arr[1];
						}
					} else {
					    $descr=substr($tags->ctags($obody),0,250)."...";
					}
					$body=$tags->ctags($fbody);
					$body=str_replace("&nbsp;"," ",$body);
					$body=str_replace("&copy;","",$body);
					$body=str_replace("&reg;","",$body);
					$url_lang = $this->get_lang_by_url($url);
					$url	= mysql_real_escape_string($url);

					$title	= mysql_real_escape_string($title);
					$descr	= mysql_real_escape_string(strip_tags($descr));
					$body	= mysql_real_escape_string($body);
/*
					$words = preg_split('#\s|[,.:;!?"\'()]#', $body, -1, PREG_SPLIT_NO_EMPTY);
					$base_words = array();
					foreach ( $words as $v ) {
					    if ( abo_strlen($v) > 3 ) {
					        $v = abo_strtolower($v);
					        $base = $rumor->get_base_form(iconv("UTF-8", "WINDOWS-1251", $v));
					        $base = empty($base) ? $v : iconv("WINDOWS-1251", "UTF-8", $base[0]);
					        if (! in_array($base, $base_words)) {
					            $base_words[] = $base;
					        }
					    }
					}
*/

					if (!$CONFIG['multilanguage']
					   || ($CONFIG['multilanguage']
					       && ($lang == $url_lang || (!$url_lang && $lang == $CONFIG['basic_lang']) )
					   )
					) {
    					$res = $db->query("SELECT * FROM {$server}{$lang}_search_tmp_page_index WHERE url = '{$url}' LIMIT 1");
    					if(!mysql_num_rows($res)) {
	    					if(count(explode('catalog/', $url))>1 && strpos($url, 'html')){
    					       
								$catObject = new CatalogPrototype('only_create_object');
		    					$arr = explode('/',$url);
		    					$categoryId = $arr[1];
		    					$productId = str_replace('.html','',$arr[2]);
		    					
		    					$product = $catObject->getProductSearch($productId);
		    					$categoryTitle = $product['category_title'];
		    					$descr = $product['product_description']? mysql_real_escape_string(substr($tags->ctags($product['product_description']),0,250)."...") : 'Описание по данному товару отсуствует.';
		    					$body = mysql_real_escape_string($tags->ctags($product['product_description']));
		    					$title = mysql_real_escape_string($product['product_title']);
								$synonyms = mysql_real_escape_string($product['synonyms']);
					
		    					$words = preg_split('#\s|[,.:;!?"\'()]#', $title.' '.$descr. ' '. $product['product_description_shop'], -1, PREG_SPLIT_NO_EMPTY);
								$base_words = array();
								foreach ( $words as $v ) {
								    if ( abo_strlen($v) > 3 ) {
								        $v = abo_strtolower($v);
								        $base = $rumor->get_base_form(iconv("UTF-8", "WINDOWS-1251", $v));
								        $base = empty($base) ? $v : iconv("WINDOWS-1251", "UTF-8", $base[0]);
								        if (! in_array($base, $base_words)) {
								            $base_words[] = $base;
								        }
								    }
								}
							$title = mysql_real_escape_string($product['product_code']);
    						$db->query( "INSERT INTO {$server}{$lang}_search_tmp_page_index (url, title, descr, body, indx, status, created, categoryId, categoryTitle, productId, synonyms)"
    						    ." VALUES ('{$url}', '{$title}', '{$descr}', '{$body}', '"
    						    .mysql_real_escape_string(implode(" ", $base_words )).' '.$synonyms."', 200, NOW(), '{$categoryId}', '{$categoryTitle}', '{$productId}','{$synonyms}')");
    						}
    					}
    					$GLOBALS["indexed_urls"][] = $host;
    					$GLOBALS["number_of_pages"]++;
    					// обход массива внутренних ссылок (изменения 15.04.2004):
    					if (is_array($urls)) {
    						reset($urls);
    						foreach ($urls as $href) {
    							$href = $CONFIG["host"] . $href;
    							$href=str_replace("http://","http:::",$href);
    							$href=str_replace("//","/",$href);
    							$href=str_replace("//","/",$href);
    							$href=str_replace("http:::","http://",$href);
    							if (!in_array($href, $GLOBALS["indexed_urls"]) && !array_key_exists($href, $_SESSION["Spider"]["Visited"])) {
    //								$GLOBAL["referer"] = $url;
    //								$I=new cindexer($href);
    								$_SESSION["Spider"]["Tovisit"][$href] = 1;
    								$_SESSION["Spider"]["Referer"][$href] = $url;
    							} else {
    //								print "<a href=\"" . $href . "\" target=_blank>" . $href . "</a>{$_msg['index_before']}<br>\n";
    							}
    						}
    					}
					}
					if(count(explode('catalog/', $url))>1 && strpos($url, 'html')){
					print $GLOBALS["number_of_pages"] . ": <a href=\"{$url}\" target=_blank>{$url}</a> <font color=\"green\">{$_msg['index_now']}</font><br>\n";
					}
					unset($fbody);
					unset($body);
				} else {
					$h->Close();
					return;
				}
			} else {
				$h->Close();
				print "[ERROR] Can't get \"".$p["host"].$p["path"]."\"!";
				return;
			}
		} else {
			$h->Close();
			print "[ERROR] Can't connect to \"".$p["host"]."\"!";
			return;
		}
	}
   function utf2ansii($var){
      static $table;
      if(!$table){
        $table = array(
                "\xD0\x81" => "\xA8", // Ё
                "\xD1\x91" => "\xB8", // ё
        );
      }
      if(is_array($var)){foreach($var as $_k => $_v){$ret[$_k] = $this->utf2ansii($_v);}return $ret;}
       elseif(is_string($var)){
        $ret = preg_replace(
                '#([\xD0-\xD1])([\x80-\xBF])#se',
                'isset($table["$0"]) ? $table["$0"] : chr(ord("$2")+("$1" == "\xD0" ? 0x30 : 0x70))',
                $var
        );
        return $ret;
      }
      return $var;
    }
	function ansii2utf($var){
		return iconv ('windows-1251', 'UTF-8', $var);
	}

	function get_lang_by_url($url)
	{   global $CONFIG;
	    static $langs = false;

        if (!$langs) {
    		foreach ($CONFIG['site_langs'] as $val) {
    			$langs[] = $val['value'];
    		}
        }
        if (preg_match("/[\?&]lang=(".implode("|", $langs).")(&.*)?$/i", $url, $ret)) {
        	$ret = $ret[1];
        } else if ($CONFIG['rewrite_mod']) {
			$p = explode("/", $url);
			$p = '' === $p[0] ? $p[1] : $p[0];
			$ret = in_array($p, $langs) ? $p : '';
		}

		return $ret;
	}
}
?>