<?php

require_once(RP.'mod/banners/lib/class.bannersDB.php');

class BannersPrototype extends Module {
	var $module_name = "banners";
	var $table_prefix;
	var $main_title;											// переопределяет title страницы, если модуль главный
	var $addition_to_path;										// добавляет к пути страницы строку, если модуль главный
	var $already_displayed = array();							// баннеры, уже показанные на этой странице
	var $_msg						= array();
	var $block_module_actions		= array();
	var $block_main_module_actions	= array();
	var $default_action 			= "showbanner";				// действие, выполняемое модулем по умолчанию
	var $admin_default_action		= 'showctg';
	var $tpl_path					= 'mod/banners/tpl/';
	var $banner_types				= array();
	var $action_tpl = array(
	    'banners' => 'banners_banners.html',
	    'showbanner' => 'banners_showbanner.html',
	);
    var $is_shop;
	function BannersPrototype($action = "", $transurl = "", $properties = array(), $prefix = "", $rewrite_mod_params = NULL, $adminurl = NULL, $noadmin = false) {
		global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang,	$permissions, $request_type, $_RESULT;
		
		/* Get $action, $tpl_name, $permissions */
		extract( $this->init(array(
		        'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
		        'action' => $action, 'tpl_name' => $properties['tpl']
		)), EXTR_OVERWRITE);
		$this->is_shop = isShop();
		// выставим свойства, которые пользователь имеет возможность задать для блока с данным модулем
		// индекс свойства в массиве $properties соответствует номеру поля в БД
		// например, первый элемент соотв. полю property1 в БД
		$category_id	= (int)$properties[1];										// показывать баннеры такой то категории
		$new_line		= (int)$properties[2];										// перенос строки после баннера
		
		if ('only_create_object' == $action) {
		    return;
		}
		
		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}
		
		if (! self::is_admin()) {

//---------------------------------- обработка действий с сайта --------------------------------//		
			switch ($action) {
			    
			case "banners":			    
                $main->include_main_blocks($tpl_name, 'main');
                $tpl->prepare();
                
				$this->show_banners($category_id, 1, 50, false, 0, 1);
				$tpl->gotoBlock('_ROOT');
				$tpl->assign(array(
				    'delay' => $properties[3] > 1 ? (int)$properties[3] : 10000,
				    'fade_delay' => $properties[4] > 1 ? (int)$properties[4] : 1500,
				));
				break;
				
// Вывод баннеров в поле в случайном порядке по их category_id
			case "showbanner":
				// сначала пустой шаблон
                $main->include_main_blocks($tpl_name, 'main');
                $tpl->prepare();
				$this->show_random_banner($category_id, $new_line);
				break;
// Default...
			default:
				if (is_object($tpl)) $tpl->prepare();
				return;
			}
//------------------------------- конец обработки действий с сайта -----------------------------//
		} else {
    		// действия, которые пользователь имеет возможность задать для блока с данным модулем
      		$this->block_module_actions	= array(
      		    'showbanner' => $this->_msg["Show_banner"],
      		    'banners' => 'Показать баннеры',
      		);
      		$this->block_main_module_actions	=  array(
      		    'showbanner' => $this->_msg["Show_banner"],
      		    'banners' => 'Показать баннеры',
      		);
      		$this->banner_types   = array(
									1	=>	$this->_msg["Animated_gif"],
									2	=>	$this->_msg["Flash_banner"],
									//3	=>	$this->_msg["Text_banner"],
									4	=>	$this->_msg["Promo_block"],
								);
      		
      		if (!isset($tpl)) {
      		    $main->message_die(iconv("WINDOWS-1251", "UTF-8", 'Не подключен класс для работы с шаблонами'));
      		}

			if (empty($action)) $action = $this->admin_default_action;
			switch ($action) {
		    case 'prp':
		    	if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_step;
				$main->include_main_blocks('_properties.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Information"	=> $main->_msg["Information"],
					"MSG_Value"			=> $main->_msg["Value"],
					"MSG_Save"			=> $main->_msg["Save"],
					"MSG_Cancel"		=> $main->_msg["Cancel"],
				));
		        if (!$request_step || $request_step == 1) {
					$tpl->assign(array('form_action'	=> $baseurl,
									   'step'			=> 2,
									   'lang'			=> $lang,
									   'name'			=> $this->module_name,
									   'action'			=> 'prp'));
					$prp_html = $main->getModuleProperties($this->module_name);
		            $this->main_title = $this->_msg["Controls"];
				} elseif (2 == $request_step) {
		        	$main->setModuleProperties($this->module_name, $_POST);
					header('Location: '.$baseurl.'&action=prp&step=1');
				}
		    	break;

		    case 'block_prp':
				global $request_sub_action, $request_block_id;
		    	$arr  = $this->getEditLink($request_sub_action, $request_block_id);
		    	$cont = 'var container = document.getElementById(\'target_span\');
		    		  	 container.innerHTML = "'.$this->getPropertyFields().'";
		    		  	 material_id		  = "'.$arr['material_id'].'";
		    		  	 material_url		  = "'.$arr['material_url'].'";
		    		  	 changeAction();
		    	      	 setURL("property1");';
				header('Content-Length: '.strlen($cont));
		    	echo $cont;
		    	exit;
		    	break;

// Редактирование свойств (параметров) блока
//-------------------------------------------------
			case "properties":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $module_id, $request_field, $request_block_id, $title;
				$main->include_main_blocks("_block_properties.html", "main");
				$tpl->prepare();
				$main->show_actions($request_field, "", $this->block_main_module_actions, $this->block_module_actions);
				//$tpl->assign("_ROOT.title", $title);
				$tpl->assign(array('_ROOT.title'		=> $title,
									'_ROOT.username'	=> $_SESSION['session_login'],
									'_ROOT.password'	=> $_SESSION['session_password'],
									'_ROOT.name'		=> $this->module_name,
									'_ROOT.lang'		=> $lang,
									'_ROOT.block_id'	=> $request_block_id,
								));
				$this->main_title = $this->_msg["Block_properties"];
				break;

			case 'showbanners':
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_start, $request_category;
				$start = ($request_start) ? $request_start : 1;
				$category = ($request_category) ? $request_category : 1;
				$main->include_main_blocks_2($this->module_name."_list.html", $this->tpl_path);
				$tpl->prepare();
				list($category_title, $ctg_owner_id) = $this->show_category($request_category);
				$pages_cnt = $this->show_banners($category, $start, null, null, $ctg_owner_id);
				// Функция для разбивки записей по страницам и вывода навигации по ним
				// Имена полей и их назначение :
				// ($table = таблица, $where = условие выборки, $blockname = имя блока,
				// $action = добавка к урл, $start = текущая страница/начало выборки,
				// $rows = кол-во строк, $order_by = порядок выборки, $separator = разделитель)
				if ($pages_cnt) $main->_show_nav_block($pages_cnt, null, 'action='.$action.'&category='.$category, $start);
				$tpl->assign(Array(
					"_ROOT.MSG_Activ" => $this->_msg["Activ"],
					"_ROOT.MSG_Ed" => $this->_msg["Ed"],
					"_ROOT.MSG_Rights" => $main->_msg["Rights"],
					"_ROOT.MSG_Name" => $this->_msg["Name"],
					"_ROOT.MSG_Type" => $this->_msg["Type"],
					"_ROOT.MSG_Shows" => $this->_msg["Shows"],
					"_ROOT.MSG_Max_show" => $this->_msg["Max_show"],
					"_ROOT.MSG_Clicks" => $this->_msg["Clicks"],
					"_ROOT.MSG_from" => $this->_msg["from"],
					"_ROOT.MSG_to" => $this->_msg["to"],
					"_ROOT.MSG_Del" => $this->_msg["Del"],
					"block_banners.MSG_Edit" => $this->_msg["Edit"],
					"block_banners.MSG_Confirm_delete_record" => $this->_msg["Confirm_delete_record"],
					"block_banners.MSG_Delete" => $this->_msg["Delete"],
					"_ROOT.MSG_Select_action" => $this->_msg["Select_action"],
					"_ROOT.MSG_Activate_publication" => $this->_msg["Activate_publication"],
					"_ROOT.MSG_Suspend_publication" => $this->_msg["Suspend_publication"],
					"_ROOT.MSG_Delete" => $this->_msg["Delete"],
					"_ROOT.MSG_Execute" => $this->_msg["Execute"],
					"nav.MSG_Pages" => $this->_msg["Pages"],
				));
				$this->main_title = ($category_title) ? $category_title : "";
				break;


// Добавление баннеров в базу данных
			case "add":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_step, $request_category, $request_type, $request_bname, $request_text,
						$request_url, $request_max_showing, $request_show_from, $request_show_to,
						$request_city, $request_block_id;

				#$fp = fopen('test.txt', 'w'); fputs($fp, var_export($_POST, true).var_export($_REQUEST, true)); fclose($fp);

				// Добавление новой записи, шаг первый
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name."_add.html", $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(array(
							"_ROOT.form_action"	=>	$baseurl.'&action=add&step=2',
					));
					$tpl->assign(Array(
						"_ROOT.MSG_Choose_category" => $this->_msg["Choose_category"],
						"_ROOT.MSG_Select_banner_type" => $this->_msg["Select_banner_type"],
						"_ROOT.MSG_Category" => $this->_msg["Category"],
						"_ROOT.MSG_Select_category" => $this->_msg["Select_category"],
						"_ROOT.MSG_Type" => $this->_msg["Type"],
						"_ROOT.MSG_required_fields" => $this->_msg["required_fields"],
						"_ROOT.MSG_Cancel" => $this->_msg["Cancel"],
						"_ROOT.MSG_Next" => $this->_msg["Next"],
					));
					$nf = $this->show_categories('block_categories', null, $true);
					if ($nf == 0) { // если нет ни одной категории то редирект
						header('Location: '.$baseurl.'&action=showctg&empty');
						exit;
					}
					$this->show_banner_types('block_types');
					$this->main_title = $this->_msg["Add_banner"];
				} else if ($request_step == 2) {
					list($allow, $ctg_rights) = $this->test_item_rights("e", "id", $this->table_prefix."_banners_categories",
																		$request_category, false, 0, true);
					if (!$allow) $main->message_access_denied($this->module_name, $action);

					$main->include_main_blocks_2($this->module_name . "_edit.html", $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(Array(
						"_ROOT.MSG_Enter_banner_name" => $this->_msg["Enter_banner_name"],
						"_ROOT.MSG_Shows_must_be_positive" => $this->_msg["Shows_must_be_positive"],
						"_ROOT.MSG_Enter_end_date" => $this->_msg["Enter_end_date"],
						"_ROOT.MSG_Enter_begin_date" => $this->_msg["Enter_begin_date"],
						"_ROOT.MSG_Enter_URL" => $this->_msg["Enter_URL"],
						"_ROOT.MSG_Enter_HTML_source" => $this->_msg["Enter_HTML_source"],
						"_ROOT.MSG_Enter_text" => $this->_msg["Enter_text"],
						"_ROOT.MSG_Choose_category" => $this->_msg["Choose_category"],
						"_ROOT.MSG_Category" => $this->_msg["Category"],
						"_ROOT.MSG_Select_category" => $this->_msg["Select_category"],
						"_ROOT.MSG_Banner_type" => $this->_msg["Banner_type"],
						"_ROOT.MSG_Name" => $this->_msg["Name"],
						"_ROOT.MSG_Show_no_more_than" => $this->_msg["Show_no_more_than"],
						"_ROOT.MSG_times" => $this->_msg["times"],
						"_ROOT.MSG_zero_or_empty" => $this->_msg["zero_or_empty"],
						"_ROOT.MSG_Show_from" => $this->_msg["Show_from"],
						"_ROOT.MSG_Select" => $this->_msg["Select"],
						"_ROOT.MSG_Clear" => $this->_msg["Clear"],
						"_ROOT.MSG_to" => $this->_msg["to"],
						"_ROOT.MSG_required_fields" => $this->_msg["required_fields"],
						"_ROOT.MSG_Save" => $this->_msg["Save"],
						"_ROOT.MSG_Cancel" => $this->_msg["Cancel"],
						"_ROOT.MSG_Show_for_cities" => $this->_msg["Show_for_cities"],
						"_ROOT.MSG_Cities" => $this->_msg["Cities"],
						"_ROOT.MSG_Add" => $this->_msg["Add"],
						"_ROOT.MSG_Delete" => $this->_msg["Delete"],
					));
					$tpl->assign(array(
							'_ROOT.form_action'			=>	$baseurl.'&action=add&step=3',
							'_ROOT.banner_type'			=>	$request_type,
							'_ROOT.banner_type_name'	=>	$this->banner_types[$request_type],
					));
					$this->show_categories('block_categories', $request_category, $true);
					$this->show_banner_types('block_types', $request_type);
					
					$this->show_select('block_cities', 0, 'name', BannersPrototype::getCityList(array('zone' => 'RU')));

					$tpl->newBlock("block_type".$request_type);
					$tpl->assign(Array(
						"MSG_URL" => $this->_msg["URL"],
						"MSG_Banner" => $this->_msg["Banner"],
						"MSG_HTML_source" => $this->_msg["HTML_source"],
						"MSG_Alternative_image" => $this->_msg["Alternative_image"],
						"MSG_Text" => $this->_msg["Text"],
					));
					$tpl->newBlock("block_check_type".$request_type);
					$this->main_title = $this->_msg["Add_banner"];
				// Добавление новой записи, шаг третий
				} else if ($request_step == 3) {
					list($allow, $ctg_rights) = $this->test_item_rights("e", "id", $this->table_prefix."_banners_categories",
																		$request_category, false, 0, true);
					if (!$allow) $main->message_access_denied($this->module_name, $action);

					if ($_FILES['filename']['name']) {
						$filename = $main->upload_file('filename', $CONFIG["banners_max_size"], $CONFIG["banners_path"]);
					}
					if ($_FILES['imagename']['name']) {
						$imagename = $main->upload_file('imagename', $CONFIG["banners_max_size"], $CONFIG["banners_path"]);
					}
                    $block_id = intval(substr($request_block_id, 10));
					$lid = $this->add_banner($request_type, $request_category, $request_bname,
					   $request_text, $request_url, $filename, $imagename, $request_max_showing,
					   $request_show_from , $request_show_to, $request_city, $ctg_rights, $block_id
					);

					if ($lid) {
						@$cache->delete_cache_files();
						$lc->add_internal_links($this->module_name, $this->table_prefix.'_banners', 'url', $lid, $this->_msg["link_in_banners"], '<a href="'.$request_url.'">banner url</a>');
						header('Location: '.$baseurl.'&action=showbanners&category='.$request_category);
					} else {
						header('Location: '.$baseurl.'&action=showctg');
					}
					exit;
				}
				break;


// Редактирование баннера
			case "edit":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_start, $request_type;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_banners", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name."_edit.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					'_ROOT.form_action'	=>	$baseurl.'&action=update&id='.$request_id.'&start='.$request_start,
					"_ROOT.MSG_Enter_banner_name" => $this->_msg["Enter_banner_name"],
					"_ROOT.MSG_Shows_must_be_positive" => $this->_msg["Shows_must_be_positive"],
					"_ROOT.MSG_Enter_end_date" => $this->_msg["Enter_end_date"],
					"_ROOT.MSG_Enter_begin_date" => $this->_msg["Enter_begin_date"],
					"_ROOT.MSG_Enter_URL" => $this->_msg["Enter_URL"],
					"_ROOT.MSG_Enter_HTML_source" => $this->_msg["Enter_HTML_source"],
					"_ROOT.MSG_Enter_text" => $this->_msg["Enter_text"],
					"_ROOT.MSG_Choose_category" => $this->_msg["Choose_category"],
					"_ROOT.MSG_Category" => $this->_msg["Category"],
					"_ROOT.MSG_Select_category" => $this->_msg["Select_category"],
					"_ROOT.MSG_Banner_type" => $this->_msg["Banner_type"],
					"_ROOT.MSG_Name" => $this->_msg["Name"],
					"_ROOT.MSG_Show_no_more_than" => $this->_msg["Show_no_more_than"],
					"_ROOT.MSG_times" => $this->_msg["times"],
					"_ROOT.MSG_zero_or_empty" => $this->_msg["zero_or_empty"],
					"_ROOT.MSG_Show_from" => $this->_msg["Show_from"],
					"_ROOT.MSG_Select" => $this->_msg["Select"],
					"_ROOT.MSG_Clear" => $this->_msg["Clear"],
					"_ROOT.MSG_to" => $this->_msg["to"],
					"_ROOT.MSG_required_fields" => $this->_msg["required_fields"],
					"_ROOT.MSG_Save" => $this->_msg["Save"],
					"_ROOT.MSG_Cancel" => $this->_msg["Cancel"],
					"_ROOT.MSG_Show_for_cities" => $this->_msg["Show_for_cities"],
					"_ROOT.MSG_Cities" => $this->_msg["Cities"],
					"_ROOT.MSG_Add" => $this->_msg["Add"],
					"_ROOT.MSG_Delete" => $this->_msg["Delete"],
				));
				$category = $this->show_banner($request_id, $request_start);
				if (!$category) { // если нет такого баннера то редирект
					header('Location: '.$baseurl.'&action=showctg');
					exit;
				}
				$tpl->assign(Array(
					"MSG_URL" => $this->_msg["URL"],
					"MSG_Banner" => $this->_msg["Banner"],
					"MSG_HTML_source" => $this->_msg["HTML_source"],
					"MSG_Alternative_image" => $this->_msg["Alternative_image"],
					"MSG_Text" => $this->_msg["Text"],
				));
				$this->show_categories('block_categories', $category, $true);
				
				$this->show_select('block_cities', 0, 'name', BannersPrototype::getCityList(array('zone' => 'RU')));

				$cities = $this->getBannerCityList($request_id);
				if (! empty($cities)) {
					$this->show_select('block_banner_cities', 0, 'name', $cities);
					$tpl->assign(array(
					   '_ROOT.cities'	=>	implode(",", array_keys($cities)),
					));
				}

				$tpl->newBlock("block_check_type$request_type");
				$this->main_title = $this->_msg["Edit_banner"];
				break;

// Обновление баннера
			case "update":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                global $request_id, $request_start, $request_category, $request_type, $request_bname, $request_text,
						$request_url, $request_max_showing, $request_show_from , $request_show_to, $request_city;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_banners", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($_FILES['filename']['name']) {
					$filename = $main->upload_file('filename', $CONFIG["banners_max_size"], $CONFIG["banners_path"]);
				}
				if ($_FILES['imagename']['name']) {
					$imagename = $main->upload_file('imagename', $CONFIG["banners_max_size"], $CONFIG["banners_path"]);
				}

				if ($this->update_banner($request_category, $request_bname, $request_text, $request_url, $request_id, $filename, $imagename, $request_max_showing, $request_show_from , $request_show_to, $request_city)) {
					@$cache->delete_cache_files();
					$lc->add_internal_links($this->module_name, $this->table_prefix."_banners", "url", $request_id, "ссылка в баннерах", "<a href=\"$request_url\">banner url</a>");
				}
				header("Location: $baseurl&action=showbanners&category=$request_category&start=$request_start");
				exit;
				break;

// Удаляем баннер
			case "del":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("d", "id", $this->table_prefix."_banners", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($this->delete_banner($request_id)) {
					@$cache->delete_cache_files();
					$lc->delete_internal_links($this->module_name, $this->table_prefix."_banners", $request_id);
				}
				header('Location: '.$baseurl.'&action=showbanners&category='.$request_category.'&start='.$request_start);
				exit;
				break;

// Отключение публикации
			case "suspend":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("p", "id", $this->table_prefix."_banners", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($main->suspend($this->table_prefix."_banners", $request_id)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showbanners&category=$request_category&start=$request_start");
				exit;
				break;

// Включение публикации
			case "activate":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("p", "id", $this->table_prefix."_banners", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if ($main->activate($this->table_prefix."_banners", $request_id)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showbanners&category=$request_category&start=$request_start");
				exit;
				break;


// Удалить выбранные баннеры
			case "delete_checked":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_category, $request_start;
				if(is_array($request_check)) {
					list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("d", "id", $this->table_prefix."_banners", $v, false, $ctg_owner_id)) {
							if ($this->delete_banner($v)) {
								@$cache->delete_cache_files();
								$lc->delete_internal_links($this->module_name, $this->table_prefix."_banners", $v);
							}
						}
					}
				}
				header('Location: '.$baseurl.'&action=showbanners&category='.$request_category.'&start='.$request_start);
				exit;
				break;

// Отключить публикацию выбранных баннеров
			case "suspend_checked":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_category, $request_start;
				if(is_array($request_check)) {
					list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $this->table_prefix."_banners", $v, false, $ctg_owner_id)) {
							if ($main->suspend($this->table_prefix."_banners", $v)) @$cache->delete_cache_files();
						}
					}
				}
				header("Location: $baseurl&action=showbanners&category=$request_category&start=$request_start");
				exit;
				break;

// Включить публикацию выбранных баннеров
			case "activate_checked":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_category, $request_start;
				if(is_array($request_check)) {
					list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $this->table_prefix."_banners", $v, false, $ctg_owner_id)) {
							if ($main->activate($this->table_prefix."_banners", $v)) @$cache->delete_cache_files();
						}
					}
				}
				header("Location: $baseurl&action=showbanners&category=$request_category&start=$request_start");
				exit;
				break;


// Редактирование прав баннера
			case 'editrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_banners", $request_id, true, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($this->table_prefix."_banners", "name", $request_id,
											"&id=".$request_id."&category=".$request_category."&start=".$request_start, $this->_msg["Banner"])) {
					header('Location: '.$baseurl.'&action=showbanners&category='.$request_category.'&start='.$request_start);
				}
				break;

// Сохранение прав баннера
			case 'saverights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start, $request_rights;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_banners", $request_id, true, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($this->table_prefix."_banners", $request_id, $request_rights);
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
    					"res"	=> 1,
    					"id"	=> $request_id,
					);
				} else {
					header('Location: '.$baseurl.'&action=showbanners&category='.$request_category.'&start='.$request_start);
				}
				exit;
				break;


// Показать категории баннеров
			case "showctg":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2($this->module_name."_categories.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"_ROOT.MSG_Empty_field" => $this->_msg["Empty_field"],
					"_ROOT.MSG_Ed" => $this->_msg["Ed"],
					"_ROOT.MSG_Rights" => $main->_msg["Rights"],
					"_ROOT.MSG_Category" => $this->_msg["Category"],
					"_ROOT.MSG_Del" => $this->_msg["Del"],
					"_ROOT.MSG_Create_new_category" => $this->_msg["Create_new_category"],
					"_ROOT.MSG_Create" => $this->_msg["Create"],
					'_ROOT.form_action' => $baseurl.'&action=addctg',
				));
				$res = $this->show_categories('block_categories');
				if (!$res && isset($_REQUEST['empty'])) {
				    $tpl->newBlock('warning');
				    $tpl->assign('txt', 'Список категорий пуст.');
				}
				$this->main_title = $this->_msg["Banners_categories"];
				break;

// Добавить категорию баннеров
			case "addctg":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_title, $request_block_id;
				if ($this->add_category($request_title, intval(substr($request_block_id, 10)))) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showctg");
				exit;
				break;

// Вывод категории на экран для редактирования
//---------------------------------------------------
			case "editctg":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_category;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_banners_categories", $request_category))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name."_edit_category.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
						"_ROOT.form_action"	=>	"$baseurl&action=updatectg",
						"_ROOT.cancel_link"	=>	"$baseurl&action=showctg",
				));
				$this->show_category($request_category);
				$tpl->assign(Array(
					"_ROOT.MSG_Empty_field" => $this->_msg["Empty_field"],
					"_ROOT.MSG_Save" => $this->_msg["Save"],
					"_ROOT.MSG_Cancel" => $this->_msg["Cancel"],
				));
				$this->main_title = $this->_msg["Edit_category"];
				break;

// Сделать update для категории
//------------------------------------
			case "updatectg":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_title;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_banners_categories", $request_category))
					$main->message_access_denied($this->module_name, $action);

				if ($this->update_category($request_category, $request_title)) @$cache->delete_cache_files();
				header("Location: $baseurl&action=showctg");
				exit;
				break;


// Удаляем категорию
//---------------------------------------------------------
			case "delctg":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_step, $request_category;
				if (!$this->test_item_rights("d", "id", $this->table_prefix."_banners_categories", $request_category))
					$main->message_access_denied($this->module_name, $action);

				// Удаление категории, первый шаг
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name."_del_category.html", $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(array(
							"_ROOT.form_action"	=>	"$baseurl&action=delctg&step=2&category=$request_category",
							"_ROOT.cancel_link"	=>	"$baseurl&action=showctg",
					));
					$this->show_category($request_category);
					$tpl->assign(Array(
						"MSG_Removing_category" => $this->_msg["Removing_category"],
						"MSG_deletes_its_banners" => $this->_msg["deletes_its_banners"],
						"MSG_Confirm_delete_category" => $this->_msg["Confirm_delete_category"],
						"MSG_Delete" => $this->_msg["Delete"],
						"MSG_Cancel" => $this->_msg["Cancel"],
					));
					$this->main_title = $this->_msg["Delete_category"];
				// Удаление категории, шаг второй
				} else if ($request_step == 2) {
					if ($this->delete_category($request_category)) @$cache->delete_cache_files();
					header("Location: ".$baseurl."&action=showctg");
					exit;
				}
				break;

// Редактирование прав категории
			case 'editctgrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_start;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_banners_categories", $request_category, true))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($this->table_prefix."_banners_categories", "title", $request_category,
											"&category=".$request_category, $this->_msg["Category"], "savectgrights")) {
					header('Location: '.$baseurl);
				}
				break;

// Сохранение прав категории
			case 'savectgrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_start, $request_rights;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_banners_categories", $request_category, true))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($this->table_prefix."_banners_categories", $request_category, $request_rights);
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
					    "res" => 1,
					    "category" => $request_category,
					);
				} else {
					header('Location: '.$baseurl);
				}
				exit;
				break;

// Создание объекта для дальнейшего использования
			case 'only_create_object':
				break;

// Default...
//-----------------------------------------------------------------------------
			default:
				$main->include_main_blocks();
				$tpl->prepare();
			}
//--------------------------- конец обработки действий из админ части --------------------------//
		}
	}


	// функция для вывода баннеров в случайном порядке по их category_id
	function show_random_banner($category_id = null, $new_line = null)
	{   global $CONFIG, $tpl, $main, $db, $lang, $baseurl;

    if(getenv("HTTP_CLIENT_IP")) {
        $ip = getenv("HTTP_CLIENT_IP");
    } elseif(getenv("HTTP_X_FORWARDED_FOR")) {
        $ip = getenv("HTTP_X_FORWARDED_FOR");
    } else {
        $ip = getenv("REMOTE_ADDR") ? getenv("REMOTE_ADDR") : $_SERVER['REMOTE_ADDR'];
    }
    
    $city_list = BannersPrototype::getCityList(array('ip' => $ip));
    $city_id = false;
    if (! empty($city_list)) {
      	list($city_id, $val) = each($city_list);
    }

    $date = date("Y-m-d");
		$category_id = (int)$category_id;
		$where = ($category_id) ? $where = "category_id = $category_id AND " : "";
  	$where .= "(";
  	$where .= " ( max_showing = 0 AND ISNULL(show_from) AND ISNULL(show_to) )";
  	$where .= " OR showing < max_showing";
  	$where .= " OR '".$date."' BETWEEN show_from AND show_to";
  	$where .= ") AND active";
  	$where .= $city_id ? " AND (bc.banner_id IS NULL OR bc.city_id=".$city_id.")" : "";
    if($this->is_shop) $where .= ' AND b.is_shop ';
	else $where .= ' AND b.is_shop=0 ';
		$banr_ids = array();
		$this->get_list_with_rights(
		    "b.id",
		    $this->table_prefix.'_banners b' .($city_id ? ' LEFT JOIN '.$this->table_prefix.'_banners_cities bc ON b.id=banner_id' : ''),
		    'b',
		    $where.' '.$not_in_str,
		    $city_id ? 'b.id' : ''
		);
		if ($db->nf()) {
			while ($db->next_record()) {
				$banr_ids[] = $db->f('id');
			}
		}

		if (count($banr_ids)) {
			$banr_id = $banr_ids[rand(0, count($banr_ids)-1)];
			$query = 'SELECT * FROM '.$this->table_prefix.'_banners WHERE id='.$banr_id;
			$db->query($query);
			$db->next_record();
			switch ($db->f('type')) {
				// анимированный гиф
				case 1:
					$tpl->newBlock("img");
					if ($db->f('filename')) {
						$img_path	= $CONFIG['banners_path'].$db->f('filename');
						if (file_exists($img_path)) {
							$url		= "/banner.php?id=" . $db->f('id').($CONFIG['basic_lang'] != $lang ? "&lang={$lang}" : "");
							$size		= getimagesize($img_path);
							$w			= $size[0];
							$h			= $size[1];
							$banner		= "<a href=\"".$url."\"><img src=\"/". $img_path."\" alt=\"\" width=\"".$w."\" height=\"".$h."\" border=\"0\"></a>";
						}
					}
					break;
				// флеш-баннер
				case 2:
					$tpl->newBlock("flash");
					$html_code			= $db->f('text');
					$url				= "/banner.php?id=" . $db->f('id').($CONFIG['basic_lang'] != $lang ? "&lang={$lang}" : "");
					$filename			= $db->f('filename');
					$imagename			= $db->f('imagename');
					$html_code			= preg_replace('/EMBED src="?[^"\s]*"?/i', "EMBED src=\"/".$CONFIG["banners_path"]."$filename?link=$url\"", $html_code);
					$html_code			= preg_replace('/PARAM NAME="?movie"? VALUE="?[^"\s]*"?/i', "PARAM NAME=movie VALUE=\"/" . $CONFIG["banners_path"] . "$filename?link=$url\"", $html_code);
					$banner				= $html_code;
					$banner				= str_replace("'", "\'", $banner);

					$banner       = '<a href="' . $url . '">' . $banner . '</a>';

					if ($imagename) $alternative_image = "<a href=\"".$url."\"><img src=\"/" . $CONFIG["banners_path"] . $imagename."\" border=0></a>";
					break;
				// текстовый баннер
				case 3:
					$tpl->newBlock("text");
					$url = "/banner.php?id=" . $db->f('id').($CONFIG['basic_lang'] != $lang ? "&lang={$lang}" : "");
					$banner = "&nbsp;<a href=\"".$url."\">".$db->f('text')."</a>";
					break;
				// промо-блок
				case 4:
					$tpl->newBlock("promo");
					$html_code			= $db->f('text');
					//$url				= str_replace('http://', '', $db->f('url'));
					$url				= "/banner.php?id=" . $db->f('id').($CONFIG['basic_lang'] != $lang ? "&lang={$lang}" : "");
					//$html_code			= preg_replace('/href="?[^"\s]*"?/i', "href=\"http://".$url."\"", $html_code);
					$html_code			= preg_replace('/href="?[^"\s]*"?/i', "href=\"".$url."\"", $html_code);
					$banner				= $html_code;
					$banner				= str_replace("'", "\'", $banner);
					break;
			}
			$nl = ($new_line) ? "<br>" : "";			
			if ($banner || $alternative_image) {
				$this->already_displayed[] = $banr_id;
				$this->countShowing($banr_id);
				//вывод на экран
				$tpl->assign(array(
					banner					=>	str_replace("\r\n", "", $banner),
					alternative_image		=>	$alternative_image,
					nl						=>	$nl, // есть ли перенос на новую строку
				));
				return TRUE;
			}
		}
		return FALSE;
	}
	function countShowing($banr_id)
	{   global $db;

	    $banr_id = (int)$banr_id;
	    if ($banr_id < 1) {
	    	return FALSE;
	    }
		$db->query("UPDATE ".$this->table_prefix."_banners SET showing = showing + 1 WHERE id = ".$banr_id);
	}

	function getPropertyFields() {
		GLOBAL $request_sub_action, $db;
		if ('' != $request_sub_action) {
		    $tpl = $this->getTmpProperties();
			switch ($request_sub_action) {
				case 'showbanner':
				    $tpl->newBlock('block_properties');				    
    		    	$tpl->assign(Array(
    					"MSG_Showing_banner_category" => $this->_msg["Showing_banner_category"],
    					"MSG_All_categories" => $this->_msg["All_categories"],
    					"MSG_Block_division" => $this->_msg["Block_division"],
    					"MSG_no" => $this->_msg["no"],
    					"MSG_yes" => $this->_msg["yes"],
    				));
    				$ctgr_list = $db->fetchAll("{$this->table_prefix}_banners_categories");
    				abo_str_array_crop($ctgr_list);
    				$tpl->assignList('category_', $ctgr_list);
    				//$this->show_categories('block_categories');
				    break;
				case 'banners':
				    $tpl->newBlock('block_properties');				    
    		    	$tpl->assign(Array(
    					"MSG_Showing_banner_category" => $this->_msg["Showing_banner_category"],
    					"MSG_All_categories" => $this->_msg["All_categories"],
    					"MSG_Block_division" => $this->_msg["Block_division"],
    					"MSG_no" => $this->_msg["no"],
    					"MSG_yes" => $this->_msg["yes"],
    				));
    				$ctgr_list = $db->fetchAll("{$this->table_prefix}_banners_categories");
    				abo_str_array_crop($ctgr_list);
    				$tpl->assignList('category_', $ctgr_list);
				    $tpl->newBlock('block_delay');				    
				    break;
				default:
				    $tpl->newBlock('block_properties_none');
			}
			return $this->getOutputContent($tpl->getOutputContent());
		}
		return FALSE;
	}

	function getEditLink($sub_action = NULL, $block_id = NULL) {
		return FALSE;
	}

	// функция для добавления баннеров в базу данных
	function add_banner($type, $category, $name, $text, $url, $filename = null, $imagename = null,
						$max_showing = 0, $show_from = 0, $show_to= 0, $city = '', $item_rights, $block_id = 0) {
    	global $CONFIG, $lang, $db;

    	$category	= (int)$category;
		$db->query("SELECT usr_group_id FROM ".$this->table_prefix."_banners_categories WHERE id=".$category);
		if (!$db->next_record()) return false;
		$owner_id = $_SESSION["siteuser"]["id"];
		list($group_id, $rights) = $this->get_group_and_rights('banners_default_rights', $block_id, $db->f("usr_group_id"));
		$rights = $item_rights ? $item_rights : $rights;

    	$type		= (int)$type;

    	$max_showing    = (int)$max_showing;
    	$max_showing = $max_showing > 1 ? $max_showing : 0;

    	$show_from = str_replace(".", "-", $show_from);
    	$show_to  = str_replace(".", "-", $show_to);
    	$show_from = "" == $show_from || "0000-00-00" == $show_from ? NULL : My_Sql::escape($show_from);
    	$show_to = "" == $show_to || "0000-00-00" == $show_to ? NULL : My_Sql::escape($show_to);
    	$show_from_unx = strtotime($show_from);
    	$show_to_unx = strtotime($show_to);

    	if ($show_from_unx < 1 || $show_to_unx < 1) {
    	   $show_from = $show_to = NULL;
    	}
    	if ($show_from_unx > $show_to_unx) {
    		$d = $show_from;
    		$show_from = $show_to;
    		$show_to = $d;
    	}
    	//echo "from-".$show_from."; to-".$show_to;
		if (!isset($category) || !isset($type)) return FALSE;
		$BD = new CBannersData_DB($this->table_prefix."_banners", "id");
		$BD->category_id	= $category;
		$BD->type			= $type;
		$BD->name			= $db->escape($name);
		$BD->text			= $db->escape($text);
		$BD->url			= $db->escape($url);
		$BD->filename		= ($filename) ? $filename : "NULL";
		$BD->imagename		= ($imagename) ? $imagename : "NULL";
		$BD->max_showing	= $max_showing;
		$BD->show_from		= $show_from;
		$BD->show_to		= $show_to;
    	$BD->owner_id		= $owner_id;
    	$BD->usr_group_id	= $group_id;
    	$BD->rights			= $rights;
		$BD->is_shop			= $_POST['is_shop'];
		if ($BD->store()) {
		    if ($city) {
		        $city_list = BannersPrototype::getCityList(array('ids' => $city));
		    	if (! empty($city_list)) {
		    		$db->query("INSERT INTO ".$this->table_prefix."_banners_cities (`banner_id`, `city_id`) VALUES ("
		    		    .$BD->id.",".implode("), (".$BD->id.",", array_keys($city_list)).")");
		    	}
		    }
			return TRUE;
		} else {
			return FALSE;
		}
	}


	// функция для редактирования баннеров в базе данных
	function update_banner($category, $name, $text, $url, $id, $filename = null, $imagename = null, $max_showing = 0, $show_from = 0, $show_to= 0, $city)
	{   global $lang, $db;
    $category	= (int)$category;
  	$id	= (int)$id;
    if (!$id || !$category) {
		    return FALSE;
		}

  	$max_showing    = (int)$max_showing;
  	$max_showing = $max_showing > 1 ? $max_showing : 0;
  	$show_from = str_replace(".", "-", $show_from);
  	$show_to  = str_replace(".", "-", $show_to);
  	$show_from = "" == $show_from || "0000-00-00" == $show_from ? "NULL" : My_Sql::escape($show_from);
  	$show_to = "" == $show_to || "0000-00-00" == $show_to ? "NULL" : My_Sql::escape($show_to);
  	$show_from_unx = strtotime($show_from);
  	$show_to_unx = strtotime($show_to);

  	if ($show_from_unx < 1 || $show_to_unx < 1) {
  	   $show_from = $show_to = "NULL";
  	}
  	if ($show_from_unx > $show_to_unx) {
  		$d = $show_from;
  		$show_from = $show_to;
  		$show_to = $d;
  	}
  	$show_from = ($show_from != 'NULL') ? "'".$show_from."'" : 'NULL';
  	$show_to = ($show_to != 'NULL') ? "'".$show_to."'" : 'NULL';
  	$db->query("UPDATE ".$this->table_prefix."_banners SET
						category_id = ".$category.",
						".($type ? "type = ".$type."," : "")."
						name = '".$db->escape($name)."',
						text = '".$db->escape($text)."',
						url = '".$db->escape($url)."',
						max_showing = ".$max_showing.",
						is_shop = ".(int)$_POST['is_shop'].",
						show_from = $show_from,
						show_to = $show_to"
						.($filename ? ", filename='".$filename."'" : "")
						.($imagename ? ", imagename='".$imagename."'" : "")."
					WHERE id=".$id);
		$res = $db->affected_rows();

		$db->query("DELETE FROM ".$this->table_prefix."_banners_cities WHERE `banner_id`=".$id);

  		$city_list = BannersPrototype::getCityList(array('ids' => $city));

		if (! empty($city_list)) {
		    $db->query("INSERT INTO ".$this->table_prefix."_banners_cities (`banner_id`, `city_id`) VALUES ("
		    	.$id.",".implode("), (".$id.",", array_keys($city_list)).")");
		}

		return $res;
	}


	// функция для удаления баннеров из базы данных
	function delete_banner($id) {
		global $CONFIG, $lang, $db;
		$id = (int)$id;
		$BD = new CBannersCategories_DB($this->table_prefix."_banners", "id");
		if ($BD->deleteByID($id)) {
		    $db->query("DELETE FROM ".$this->table_prefix."_banners_cities WHERE `banner_id`=".$id);
			return TRUE;
		} else {
			return FALSE;
		}
	}


	// функция для вывода баннера по id
	function show_banner($id = null, $start = null) {
		global $CONFIG, $tpl, $lang, $db;
		$id = (int)$id;
		if (!$id) return FALSE;
		$BD = new CBannersData_DB($this->table_prefix."_banners", "id");
		$BD->load($id);
		if ($BD->id) {
			$name	= htmlspecialchars($BD->name);
			$text	= ($BD->type==4)?$BD->text:htmlspecialchars($BD->text);
			$url	= htmlspecialchars($BD->url);
			$max_showing	= htmlspecialchars($BD->max_showing);
			$show_from   	= htmlspecialchars($BD->show_from);
			$show_to     	= htmlspecialchars($BD->show_to);
			if ($BD->filename) {
				$bannerfile_exists = "&nbsp;<a href=\"" . $CONFIG["banners_path"] . $BD->filename . "\" target=_blank>" . $this->_msg["already_upload"] . "</a>";
			} else {
				$bannerfile_exists = "";
			}
			if ($BD->imagename) {
				$image_exists = "&nbsp;<a href=\"" . $CONFIG["banners_path"] .$BD->imagename . "\" target=_blank>" . $this->_msg["already_upload"] . "</a>";
			} else {
				$image_exists = "";
			}
			$type = $BD->type;
			$tpl->assign(array(
					"_ROOT.banner_type"			=>	$type,
					"_ROOT.banner_name"			=>	$name,
					"_ROOT.is_shop_".$BD->is_shop			=>	'selected',
					"_ROOT.banner_type_name"	=>	$this->banner_types[$type],
					'_ROOT.banner_max_showing'	=>	$max_showing,
					'_ROOT.banner_show_from'    =>	$show_from,
					'_ROOT.banner_show_to'		=>	$show_to,
			));
			$tpl->newBlock('block_type'.$type);
			//вывод на экран
			$tpl->assign(
				array(
					'banner_name'		=>	$name,
					"is_shop_".$BD->is_shop			=>	'selected',
					'banner_text'		=>	$text,
					'banner_url'		=>	$url,
					'banner_type'		=>	$type,
					'banner_id'			=>	$BD->id,
					'start'				=>	$start,
					'bannerfile_exists'	=>	$bannerfile_exists,
					'image_exists'		=>	$image_exists,
				)
			);
			return $BD->category_id;
		}
		return FALSE;
	}


	// функция для вывода списка баннеров
	// -----------------------------------
	function show_banners($category, $start = null, $rows = null, $order_by = null, $owner_id = 0, $active = false) {
		global $CONFIG, $tpl, $baseurl, $db, $lang;
    	$category					= intval($category);
		if (!isset($category)) return 0;
		$start						= (intval($start)) ? $start : 1;
	    if (!$rows)			 $rows	= $CONFIG["banners_max_rows"];
    	if (!$order_by)  $order_by	= $CONFIG["banners_order_by"];
        if(!self::is_admin()){
			if($this->is_shop) $where = ' AND b.is_shop ';
			else $where = ' AND b.is_shop=0 ';
		}
		$total_cnt = $this->get_list_with_rights(
								"b.*",
								$this->table_prefix."_banners b", "b",
								"b.category_id = ".$category.($active ? " AND 1=active" : '').$where,
								"", $order_by,
								$start, $rows,
								true, $owner_id);

		$pages_cnt = ceil($total_cnt/$rows);
		if ($db->nf()) {
			$actions = array(	"edit"		=> "edit",
								"editrights"=> "editrights",
								"activate"	=>	array("activate","suspend"),
								"delete"	=> "del",
								"confirm_delete"	=> $this->_msg["Confirm_delete_record"]
							);
			while($db->next_record()) {
				// вывод на экран списка
				$id = $db->f("id");
				$adm_actions	= $this->get_actions_by_rights($baseurl."&id=".$id."&category=".$category."&start=".$start, $db->f("active"), $db->f("is_owner"), $db->f("user_rights"), $actions);
				$link = "/banner.php?id=" . $db->f('id').($CONFIG['basic_lang'] != $lang ? "&lang={$lang}" : "");
				$parse_text = preg_replace("/<a([^>]+)href=['\"]([^>]+)['\"]/i", "<a\\1href=\"{$link}\"", $db->f("text"));
				$assign = array(
					'banner_id'			    =>	$id,
					'banner_name'			=>	$db->f("name"),
					'banner_type'			=>	$this->banner_types[$db->f("type")],
					'banner_edit_action'	=>	$adm_actions["edit_action"],
					'banner_edit_rights_action'	=>	$adm_actions["edit_rights_action"],
					'banner_del_action'		=>	$adm_actions["delete_action"],
					'change_status_action'	=>	$adm_actions["change_status_action"],
					'text'	                =>	$db->f("text"),
					'filename'	            =>	$db->f("filename") && file_exists(RP.$CONFIG['banners_path'].$db->f("filename"))
					    ? "/".$CONFIG['banners_path'].$db->f("filename")
					    : $CONFIG['banners_no_img'],
					'banner_showing'	    =>	$db->f("showing"),
					'banner_max_showing'    =>	$db->f("max_showing") ? $db->f("max_showing") : "-/-",
					'banner_clicking'	    =>	$db->f("clicking"),
					'banner_show_from'	    =>	$db->f("show_from") ? $db->f("show_from") : "-/-",
					'banner_show_to'	    =>	$db->f("show_to") ? $db->f("show_to") : "-/-",
					'banner_ctr'			=>	round($db->f("clicking")/$db->f("showing"),  2),
					'start'				    =>	$start,
					'url' => $db->f('url'),
					'parse_text' => $parse_text,
					'link' => $link
				);

				$tpl->newBlock('block_banners');
				$tpl->assign($assign);
				if ($adm_actions["rights"]["d"] || $adm_actions["rights"]["p"]) {
					$tpl->newBlock('block_check');
					$tpl->assign("banner_id", $id);
				}
				switch ($db->f('type')) {
    				// анимированный гиф
    				case 1:
    				    $tpl->newBlock('block_banners_img');
    					break;
    				// флеш-баннер
    				case 2:
    					$tpl->newBlock('block_banners_flush');
    					break;
    				// текстовый баннер
    				case 3:
    				    $tpl->newBlock('block_banners_txt');
    					break;
    				// промо-блок
    				case 4:
    					$tpl->newBlock('block_banners_promo');
    					break;
    			}
				
				$tpl->assign($assign);
				
			}
			return $pages_cnt;
		}
		return 0;
	}

	// функция для вывода списка баннерных категорий
	// ----------------------------------------------
	function show_categories($blockname, $selected_category = null, $only_editable = false) {
		global $db, $tpl, $baseurl, $lang;
		$selected_category = intval($selected_category);

		$this->get_list_with_rights(
				"C.*",
				$this->table_prefix."_banners_categories AS C", "C",
				"", "", "C.id");
		$cnt = $db->nf();
		if ($cnt > 0) {
			$actions = array(	"edit"		=> "editctg",
								"editrights"=> "editctgrights",
								"delete"	=> "delctg",
								"confirm_delete"	=> ""
							);
			while ($db->next_record()) {
				if ($only_editable && !($db->f("user_rights") & 2)) {
					continue;
				}

				// вывод на экран списка
				$id = $db->f("id");
				$adm_actions = $this->get_actions_by_rights($baseurl."&category=".$id, 0, $db->f("is_owner"), $db->f("user_rights"), $actions);
				$selected = ($id == $selected_category) ? " selected" : "";
				$tpl->newBlock($blockname);
				$tpl->assign(
					array(
						category_title			=>	$db->f("title"),
						category_link			=>	"$baseurl&action=showbanners&category=".$id,
						category_edit_action	=>	$adm_actions["edit_action"],
						category_edit_rights_action	=>	$adm_actions["edit_rights_action"],
						category_del_action		=>	$adm_actions["delete_action"],
						category				=>	$id,
						selected				=>	$selected,
					)
				);
			}
			return $cnt;
		}
		return 0;
	}


	// функция для вывода инофрмации о категории по id
	// ------------------------------------------------
	function show_category($id = "", $need_output = true) {
		$id = intval($id);		if (!$id) return 0;
		global $CONFIG, $tpl, $baseurl, $lang;
		$BC = new CBannersCategories_DB($this->table_prefix."_banners_categories", "id");
		$BC->load($id);
		if ($BC->id) {
			$title = htmlspecialchars($BC->title);
			if ($need_output) {
				$tpl->assign(array(
					category			=>	$BC->id,
					category_title	=>	$title,
					category_link	=>	"$baseurl&action=showbanners&category=".$BC->id,
				));
			}
			return array($BC->title, $BC->owner_id);
		}
		return 0;
	}


	// функция добавления категории
	// ------------------------------------------------
	function add_category($title, $block_id = 0) {
		global $CONFIG, $db, $server, $lang;
		$title = $db->escape(trim($title));
		if (!$title) return FALSE;

		$owner_id = $_SESSION["siteuser"]["id"];
		list($group_id, $rights) = array_map('intval',$this->get_group_and_rights('banners_default_rights', $block_id));
		$query = "INSERT INTO ".$this->table_prefix."_banners_categories (title,owner_id,usr_group_id,rights)
					VALUES ('$title',$owner_id,$group_id,$rights)";
		$db->query($query);
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}


	// функция для update категории по id
	function update_category($id, $title) {
		global $lang, $db;
		$id = (int)$id;
		if (!isset($id) || !isset($title)) return FALSE;
		$title		= $db->escape($title);
		$BC			= new CBannersCategories_DB($this->table_prefix."_banners_categories", "id");
		$BC->id		= $id;
		$BC->title	= $title;
		if ($BC->store()) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


	// функция для удаления категории по id
	function delete_category($id = null) {
		global $lang, $db;
		$id = (int)$id;
		if (!isset($id)) return FALSE;
		$BC = new CBannersCategories_DB($this->table_prefix."_banners_categories", "id");
		if ($BC->DeleteByID($id)) {
			$BD = new CBannersData_DB($this->table_prefix."_banners", "category_id");
			$res = $BD->DeleteByCategory($id);
			if (! empty($res)) {
				$db->query("DELETE FROM ".$this->table_prefix."_banners_cities WHERE `banner_id` IN ("
				    .implode(",", $res).")");
			}
			return TRUE;
		} else {
			return FALSE;
		}
	}


	// функция для вывода типов баннера
	// ---------------------------------
	function show_banner_types($blockname, $selected_category = null) {
		global $tpl, $lang;
		$selected_category = intval($selected_category);
		if (count($this->banner_types) > 0) {
			foreach($this->banner_types as $type => $type_name) {
				$checked = ($type == $selected_category) ? " checked" : "";
				$tpl->newBlock($blockname);
				$tpl->assign(array(
					banner_type			=>	$type,
					banner_type_name	=>	$type_name,
					checked					=>	$checked,
				));
			}
			return 1;
		}
		return 0;
	}

	/**
	 * Получить список городов банера
	 *
	 * @param int $id
	 * @return mixed
	 */
	function getBannerCityList($id)
	{
		global $db;

		$db->query("SELECT city_id FROM ".$this->table_prefix."_banners_cities WHERE banner_id=".intval($id));
		if (! $db->nf()) {
			return false;
		}
		$city_ids = array();
		while ($db->next_record()) {
			$city_ids[] = $db->f('city_id');
		}
		
		return BannersPrototype::getCityList(array('ids' => implode(",", $city_ids)));
	}

    /**
     * Получить список городов из базы GEO IP
     *
     * @param string $zone
     * @return array
     */
    function getCityList($filter = false)
    {
    	global $db;

    	$where = false;
    	if (isset($filter['zone'])) {
    		$where[] = "zone='".$db->escape($filter['zone'])."'";
    	}
    	if (isset($filter['ids'])) {
    		$where[] = "city_id IN (".$db->escape($filter['ids'] ? $filter['ids'] : '-1').")";
    	}
    	if (isset($filter['id'])) {
    		$where[] = "city_id = ".intval($filter['id']);
    	}
    	if (isset($filter['ip'])) {
    	    $ip = sprintf("%u", ip2long($filter['ip']));
    		$where[] = $ip." BETWEEN first_long_ip AND last_long_ip";
    	}
    	$db->query("SELECT DISTINCT city_id, name, region, zone FROM counter_cities"
    	    .($where ? " WHERE ".implode(" AND ", $where) : "")
    	    ." ORDER BY name");
    	$ret = array();
    	while ($db->next_record()) {
    		$ret[$db->Record['city_id']] = array(
    		    'city_id' => $db->Record['city_id'],
    		    'name' => $db->Record['name'],
    		    'region' => $db->Record['region'],
    		    'zone' => $db->Record['zone'],
    		);
    	}

    	return $ret;
    }
}