<?php

class CBannersData_DB extends TObject {
	var $id					= NULL;
	var $category_id		= NULL;
	var $name				= NULL;
	var $text				= NULL;
	var $type				= NULL;
	var $filename			= NULL;
	var $imagename			= NULL;
	var $url				= NULL;
	var $showing			= NULL;
	var $max_showing		= NULL;
	var $show_from 			= NULL;
	var $show_to			= NULL;
	var $active				= NULL;
    var	$owner_id			= NULL;
    var	$usr_group_id		= NULL;
    var	$rights				= NULL;
	 var	$is_shop				= NULL;

	function CArticleData_DB ($table, $PK) {
		$this->TObject($table, $PK);
	}

	function load ($id) {
		global $CONFIG;
		$sql = "SELECT * FROM $this->_tbl WHERE $this->_tbl_key = '".My_Sql::escape($id)."'";
		return db_loadObject($sql, $this);
	}

	function DeleteByCategory($id) {
		global $CONFIG;
		if (!isset($id)) {
			return FALSE;
		} else {
			$sql = "SELECT filename, id FROM $this->_tbl WHERE category_id = ".intval($id);
			$data = db_loadList($sql);
			if ($data[0]['filename']) {
			    $ids = array();
				for($i = 0; $i<sizeof($data); $i++) {
					@unlink($CONFIG['banners_path'].$data[$i]['filename']);
					$ids[] = $data[$i]['id'];
				}
			}
			$sql = "DELETE FROM $this->_tbl WHERE category_id = ".intval($id);
			db_exec($sql);
			return $ids;
		}
	}
}

class CBannersCategories_DB extends TObject {
	var $id				= NULL;
	var $title			= NULL;
    var	$owner_id		= NULL;
    var	$usr_group_id	= NULL;
    var	$rights			= NULL;

	function CNewsCategories_DB ($table, $PK) {
		$this->TObject($table, $PK);
	}

	function getCategories () {
		$sql = "SELECT * FROM $this->_tbl ORDER BY id";
		return db_loadList($sql);
	}

	function load ($id) {
		$sql = "SELECT * FROM $this->_tbl WHERE $this->_tbl_key = '".My_Sql::escape($id)."'";
		return db_loadObject($sql, $this);
	}

	function DeleteByID($id) {
		global $lang;
//		$sql = "DELETE FROM $this->_tbl WHERE id = '$this->_tbl_key'";
		$sql = "DELETE FROM $this->_tbl WHERE $this->_tbl_key = '".My_Sql::escape($id)."'";
		db_exec($sql);
		if (db_affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}

?>
