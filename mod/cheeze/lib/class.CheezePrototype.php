<?php

///require_once(RP.'mod/banners/lib/class.cheezeDB.php');

class CheezePrototype extends Module {
    var $module_name = "cheeze";
    var $table_prefix;
    var $main_title;											// переопределяет title страницы, если модуль главный
    var $addition_to_path;										// добавляет к пути страницы строку, если модуль главный
    var $already_displayed = array();							// баннеры, уже показанные на этой странице
    var $_msg						= array();
    var $block_module_actions		= array();
    var $block_main_module_actions	= array();
    var $default_action 			= "showall";				// действие, выполняемое модулем по умолчанию
    var $admin_default_action		= 'showctg';
    var $tpl_path					= 'mod/cheeze/tpl/';
    var $banner_types				= array();
    var $action_tpl = array(
        'banners' => 'banners_banners.html',
    );
    var $is_shop;
    function CheezePrototype($action = "", $transurl = "", $properties = array(), $prefix = "", $rewrite_mod_params = NULL, $adminurl = NULL, $noadmin = false) {
        global $main, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang,	$permissions, $request_type, $_RESULT;

        /* Get $action, $tpl_name, $permissions */
        extract( $this->init(array(
            'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
            'action' => $action, 'tpl_name' => $properties['tpl']
        )), EXTR_OVERWRITE);
        $this->is_shop = isShop();
        // выставим свойства, которые пользователь имеет возможность задать для блока с данным модулем
        // индекс свойства в массиве $properties соответствует номеру поля в БД
        // например, первый элемент соотв. полю property1 в БД
        $category_id	= (int)$properties[1];										// показывать баннеры такой то категории
        $new_line		= (int)$properties[2];										// перенос строки после баннера

        if ('only_create_object' == $action) {
            return;
        }

        if ($this->actionExists($action)) {
            $this->doAction($action);
            return;
        }

        if (! self::is_admin()) {

//---------------------------------- обработка действий с сайта --------------------------------//		
            switch ($action) {
                case "showall":
                    global $tpl,$db,$db2;
                    $main->include_main_blocks($this->module_name."_showall.html", "main");
                    $tpl->prepare();
                    $db->query("SELECT * FROM ".$server.$lang."_cheeze WHERE type=0 AND active = 1");
                    if($db->nf()!=0) {
                        $tpl->newBlock("description");
                        while ($db->next_record()) {
                            preg_match_all('/<img[^>]+src=([\'"])?((?(1).+?|[^\s>]+))(?(1)\1)/',
                                $db->f('value'), $matches);
                            $img = $matches[2][0];
                                $tpl->newBlock("description_item");
                                $tpl->assign(Array(
                                    "title" => $db->f('title'),
                                    "link" => $db->f('link')   ?  $db->f('link') : $img,
                                    "img"    =>  $img,
                                    "img_preview" =>$img.'_preview.jpg'
                                ));
                        }
                    }
                    $db->query("SELECT * FROM ".$server.$lang."_cheeze WHERE type=1 AND active = 1");
                    if($db->nf()!=0) {
                        $tpl->newBlock("video");
                        while ($db->next_record()) {
                            preg_match_all('/<iframe[^>]+src=([\'"])?((?(1).+?|[^\s>]+))(?(1)\1)/',
                                $db->f('value'), $matches);
                            $video= $matches[2][0];
                            $code=  strripos($video,'/');
                            if($code){
                               $code= substr($video, $code)  ;
                               $img='http://img.youtube.com/vi'.$code.'/mqdefault.jpg';
                            }
                            else{
                                  $img=$db->f('value');
                            }
                            $tpl->newBlock("video_item");
                            $tpl->assign(Array(
                                "title" => $db->f('title'),
                                "video"    =>   $video,
                                "img"   =>$img
                            ));
                        }
                    }
                    break;


                default:
                    if (is_object($tpl)) $tpl->prepare();
                    return;
            }
//------------------------------- конец обработки действий с сайта -----------------------------//
        } else {

            // действия, которые пользователь имеет возможность задать для блока с данным модулем
            $this->block_module_actions	= array(
                'showall' => "Показать весь контент cheeze",
            );
            $this->block_main_module_actions	=  array(
                'showall' => "Показать весь контент cheeze",
            );

            if (!isset($tpl)) {
                $main->message_die(iconv("WINDOWS-1251", "UTF-8", 'Не подключен класс для работы с шаблонами'));
            }

            if (empty($action)) $action = $this->admin_default_action;
            switch ($action) {

                case 'showctg':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$server,$lang,$tpl;
                    $main->include_main_blocks_2($this->module_name."_cat.html", $this->tpl_path);
                    $tpl->prepare();
                        $db->query("SELECT * FROM " . $server . $lang . "_cheeze ");
                        $tpl->newBlock('cat_tov');
                        if($db->nf()!=0) {
                            while ($db->next_record()) {
                                $tpl->newBlock('cheeze_cat');
                                $tpl->assign(Array(
                                    "id" => $db->f('id'),
                                    "title" => $db->f('title'),
                                    "ico" => $db->f("active") == 0 ? "ico_swon" : "ico_swof",
                                    'type' =>   !$db->f('type') ? 'описание': 'видеорецепт',
                                ));
                            }
                        }
                    break;


                case 'deldescr':
                    global $db,$server,$lang,$request_id;
                    $db->query("DELETE FROM ".$server.$lang."_cheeze WHERE id=".$request_id);
                    Module::go_back();
                    break;


                case 'publ':
                    global $db,$db2,$server,$lang,$request_id;
                    $db->query("UPDATE ".$server.$lang."_cheeze SET active = if (active ,0,1)  WHERE id=".$request_id);
                    Module::go_back();
                    break;

                case 'editdescr':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$db2,$tpl,$server,$lang,$request_id;
                    $main->include_main_blocks_2($this->module_name."_editdescr.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assignGlobal("action","/admin.php?name=cheeze&action=updatedescr");
                    $arr=$db->getArrayOfResult("SELECT * FROM ".$server.$lang."_cheeze WHERE id = ".$request_id);
                    if(!empty($arr))foreach($arr as $v){
                           $tpl->assign(array('id'=>$v['id'],
                                              'title'=>$v['title'],
                                              'value'=>$v['value'],
                                              'type'=>$v['type']? 'Видео рецепта' : 'Описания',
                           ));
                        if(!$v['type']){
                            $tpl->newBlock('block_file');
                            $tpl->assign('link',$v['link']);
                            $tpl->newBlock('block_image');
                        }
                        else{
                            $tpl->newBlock('block_video');
                        }
                    }
                    break;



                case "adddescription":
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$tpl,$server,$lang,$request_title,$request_link,$request_type,$request_value;
                    //var_dump("INSERT INTO ".$server.$lang."_cheeze_faq (title,active,parent_id) VALUES('$request_name_cat',1,$request_category)");
                    $request_value=str_replace('<p>','',$request_value);
                    $request_value=str_replace('</p>','',$request_value);
                    if(!$request_type){
                        if (!function_exists('img_resize')) {
                            require_once(RP.'inc/img.php');
                        }
                        preg_match_all('/<img[^>]+src=([\'"])?((?(1).+?|[^\s>]+))(?(1)\1)/',
                            $request_value, $matches);
                        $img = $matches[2][0];
                        $IMG = array (
                            'old_path' => RP.$img,
                            'new_path' => RP.$img.'_preview.jpg',
                            'width'    => '300',
                            'height'   => '600',
                            #			'resize'   => "fixed",
                            'bg_color' => "#ffffff",
                            'extExists'	=> true,
                        );
                        $to = img_resize($IMG) ? $img : NULL;
                    }

                    $db->query("INSERT INTO ".$server.$lang."_cheeze (title,active,link,type,value) VALUES('{$request_title}',0,'{$request_link}','{$request_type}','{$request_value}')");
                    header("Location: /admin.php?name=cheeze"); exit;
                    break;

                case "updatedescr":
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$server,$lang,$request_value,$request_title,$request_id,$request_link;
                    if($request_link)$ink=",link=$request_link";
                    $request_value=str_replace('<p>','',$request_value);
                    $request_value=str_replace('</p>','',$request_value);
                    if(!$request_type){
                        if (!function_exists('img_resize')) {
                            require_once(RP.'inc/img.php');
                        }
                        preg_match_all('/<img[^>]+src=([\'"])?((?(1).+?|[^\s>]+))(?(1)\1)/',
                            $request_value, $matches);
                        $img = $matches[2][0];
                        $IMG = array (
                            'old_path' => RP.$img,
                            'new_path' => RP.$img.'_preview.jpg',
                            'width'    => '300',
                            'height'   => '600',
                            #			'resize'   => "fixed",
                            'bg_color' => "#ffffff",
                            'extExists'	=> true,
                        );
                        $to = img_resize($IMG) ? $img : NULL;
                    }
                    $db->query("UPDATE ".$server.$lang."_cheeze SET title='{$request_title}',value='{$request_value}'$ink WHERE id=".$request_id);
                    header("Location: /admin.php?name=cheeze");
                    break;


                case "adddescr":
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$tpl,$server,$lang;
                    $main->include_main_blocks_2($this->module_name."_add.html", $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assignGlobal("action","/admin.php?name=".$this->module_name."&action=adddescription");
                    break;

                case 'prp':
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $request_step;
                    $main->include_main_blocks('_properties.html', $this->tpl_path);
                    $tpl->prepare();
                    $tpl->assign(Array(
                        "MSG_Information"	=> $main->_msg["Information"],
                        "MSG_Value"			=> $main->_msg["Value"],
                        "MSG_Save"			=> $main->_msg["Save"],
                        "MSG_Cancel"		=> $main->_msg["Cancel"],
                    ));
                    if (!$request_step || $request_step == 1) {
                        $tpl->assign(array('form_action'	=> $baseurl,
                            'step'			=> 2,
                            'lang'			=> $lang,
                            'name'			=> $this->module_name,
                            'action'			=> 'prp'));
                        $prp_html = $main->getModuleProperties($this->module_name);
                        $this->main_title = $this->_msg["Controls"];
                    } elseif (2 == $request_step) {
                        $main->setModuleProperties($this->module_name, $_POST);
                        header('Location: '.$baseurl.'&action=prp&step=1');
                    }
                    break;

                case 'block_prp':
                    global $request_sub_action, $request_block_id;
                    $arr  = $this->getEditLink($request_sub_action, $request_block_id);
                    $cont = 'var container = document.getElementById(\'target_span\');
		    		  	 container.innerHTML = "'.$this->getPropertyFields().'";
		    		  	 material_id		  = "'.$arr['material_id'].'";
		    		  	 material_url		  = "'.$arr['material_url'].'";
		    		  	 changeAction();
		    	      	 setURL("property1");';
                    header('Content-Length: '.strlen($cont));
                    echo $cont;
                    exit;
                    break;

// Редактирование свойств (параметров) блока
//-------------------------------------------------
                case "properties":
                    if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
                    global $module_id, $request_field, $request_block_id, $title;
                    $main->include_main_blocks("_block_properties.html", "main");
                    $tpl->prepare();
                    $main->show_actions($request_field, "", $this->block_main_module_actions, $this->block_module_actions);
                    //$tpl->assign("_ROOT.title", $title);
                    $tpl->assign(array('_ROOT.title'		=> $title,
                        '_ROOT.username'	=> $_SESSION['session_login'],
                        '_ROOT.password'	=> $_SESSION['session_password'],
                        '_ROOT.name'		=> $this->module_name,
                        '_ROOT.lang'		=> $lang,
                        '_ROOT.block_id'	=> $request_block_id,
                    ));
                    $this->main_title = $this->_msg["Block_properties"];
                    break;






// Создание объекта для дальнейшего использования
                case 'only_create_object':
                    break;

// Default...
//-----------------------------------------------------------------------------
                default:
                    $main->include_main_blocks();
                    $tpl->prepare();
            }
//--------------------------- конец обработки действий из админ части --------------------------//
        }
    }



    function getPropertyFields() {
        GLOBAL $request_sub_action, $db;
        if ('' != $request_sub_action) {
            $tpl = $this->getTmpProperties();
            switch ($request_sub_action) {
                case 'showbanner':
                    $tpl->newBlock('block_properties');
                    $tpl->assign(Array(
                        "MSG_Showing_banner_category" => $this->_msg["Showing_banner_category"],
                        "MSG_All_categories" => $this->_msg["All_categories"],
                        "MSG_Block_division" => $this->_msg["Block_division"],
                        "MSG_no" => $this->_msg["no"],
                        "MSG_yes" => $this->_msg["yes"],
                    ));
                    $ctgr_list = $db->fetchAll("{$this->table_prefix}_banners_categories");
                    abo_str_array_crop($ctgr_list);
                    $tpl->assignList('category_', $ctgr_list);
                    //$this->show_categories('block_categories');
                    break;
                case 'banners':
                    $tpl->newBlock('block_properties');
                    $tpl->assign(Array(
                        "MSG_Showing_banner_category" => $this->_msg["Showing_banner_category"],
                        "MSG_All_categories" => $this->_msg["All_categories"],
                        "MSG_Block_division" => $this->_msg["Block_division"],
                        "MSG_no" => $this->_msg["no"],
                        "MSG_yes" => $this->_msg["yes"],
                    ));
                    $ctgr_list = $db->fetchAll("{$this->table_prefix}_banners_categories");
                    abo_str_array_crop($ctgr_list);
                    $tpl->assignList('category_', $ctgr_list);
                    $tpl->newBlock('block_delay');
                    break;
                default:
                    $tpl->newBlock('block_properties_none');
            }
            return $this->getOutputContent($tpl->getOutputContent());
        }
        return FALSE;
    }



}