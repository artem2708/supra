<?php

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_news` (
  `id` int(11) NOT NULL auto_increment,
  `category_id` smallint(5) unsigned NOT NULL default '0',
  `theme_id` smallint(5) unsigned NOT NULL default '0',
  `date` datetime default NULL,
  `source` varchar(30) default NULL,
  `announce` text,
  `announce_with_links` text,
  `body` text,
  `split_body` tinyint(4) NOT NULL default '0',
  `title` varchar(255) NOT NULL default '',
  `active` tinyint(4) NOT NULL default '0',
  `img` varchar(255) NOT NULL default '',
  `forum_theme` int(11) NOT NULL default '0',
  `seo_title` varchar(255) NOT NULL default '',
  `description` text,
  `keywords` text,
  `hits` bigint(20) unsigned NOT NULL default '0',
  `tags` varchar(255) NOT NULL default '',
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',
  `rss_id` varchar(50) NOT NULL default '',
  `rss_link` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_news_categories` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `owner_id` int(10) unsigned NOT NULL default '0',
  `usr_group_id` int(10) unsigned NOT NULL default '0',
  `rights` int(10) unsigned NOT NULL default '0',
  `rss_auto_update` tinyint(4) NOT NULL default '0',
  `rss_last_update` datetime NOT NULL default '0000-00-00 00:00:00',
  `rss_update_interval` int(11) NOT NULL default '0',
  `commentable` TINYINT( 1 ) UNSIGNED DEFAULT 0 NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_news_rss` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `category_id` int(10) unsigned NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `url` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_news_themes` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_news_ctgr` (
  `news_id` int(11) NOT NULL default 0,
  `ctgr_id` smallint(5) unsigned NOT NULL default 0,
   UNIQUE  (`news_id`, `ctgr_id`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_news_tag` (
  `id` int unsigned NOT NULL auto_increment,
  `name` varchar(31) NOT NULL default '',
  `cnt` int unsigned NOT NULL default 0,
  PRIMARY KEY  (`id`),
  KEY (`name`)
) ENGINE=MyISAM";

$MOD_TABLES[] = "CREATE TABLE `".$prefix."_news_tag_item` (
  `id` int unsigned NOT NULL auto_increment,
  `tag_id` int unsigned NOT NULL default 0,
  `item_id` int unsigned NOT NULL default 0,
  PRIMARY KEY  (`id`),
  KEY (`tag_id`, `item_id`)
) ENGINE=MyISAM";
?>