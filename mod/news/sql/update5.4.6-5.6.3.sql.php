<?php
$sql = array(
	"ALTER TABLE `{site_prefix}_news` CHANGE `img` `img` VARCHAR( 255 ) NOT NULL DEFAULT ''",
    "UPDATE `{site_prefix}_news` SET img=if(img, CONCAT('/files/news/{SITE_PREFIX}/', img), '')",
    "CREATE TABLE `{site_prefix}_news_tag` (
  `id` int unsigned NOT NULL auto_increment,
  `name` varchar(31) NOT NULL default '',
  `cnt` int unsigned NOT NULL default 0,
  PRIMARY KEY  (`id`),
  KEY (`name`)
) ENGINE=MyISAM",
    "CREATE TABLE `{site_prefix}_news_tag_item` (
  `id` int unsigned NOT NULL auto_increment,
  `tag_id` int unsigned NOT NULL default 0,
  `item_id` int unsigned NOT NULL default 0,
  PRIMARY KEY  (`id`),
  KEY (`tag_id`, `item_id`)
) ENGINE=MyISAM",
    "ALTER TABLE `{site_prefix}_news` ADD `tags` varchar(255) NOT NULL default '' AFTER `hits`",
);
?>