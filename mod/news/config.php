<?php // Vers 5.8.2 27.06.2012
/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double   - вещественное число
*	string   - строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select   - выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
			   'news_admin_order_by'	=> array('type'		=> 'select',
			   									 'defval'	=> array('id DESC'	=> array(	'rus' => 'В порядке обратном добавлению',
													 										'eng' => 'Reverce adding order'),
			   									 					 'id'		=> array(	'rus' => 'В порядке добавлению',
													 										'eng' => 'Adding order'),
			   									 					 ),
			   									 'descr'	=> array(	'rus' => 'Порядок вывода записей в адм. интерфейсе',
						 												'eng' => 'Order of record displaying on admin part'),
												 'access'	=> 'editable',
												 ),

			   'news_max_rows'			=> array('type'		=> 'integer',
			   									 'defval'	=> '10',
				   								 'descr'	=> array(	'rus' => 'Максимальное количество записей на страницу',
												 						'eng' => 'Maximum quantity of records on page'),
			   									 'access'	=> 'editable'
			   									 ),

			   'news_date_format'		=> array('type'		=> 'select',
			   									 'defval'	=> array('%d.%m.%Y %H:%i'	=> array(	'rus' => 'День.Месяц.Год Часы:Минуты',
												 										  			'eng' => 'Day.Month.Year Hours:Minutes'),
			   									 					 '%d.%m.%Y'			=> array(	'rus' => 'День.Месяц.Год',
												 													'eng' => 'Day.Month.Year'),
			   									 					 '%Y.%m.%d'			=> array(	'rus' => 'Год.Месяц.День',
												 													'eng' => 'Year.Month.Day'),
			   									 					 '%Y.%m.%d %H:%i'	=> array(	'rus' => 'Год.Месяц.День Часы:Минуты',
												 													'eng' => 'Year.Month.Day Hours:Minutes'),
			   									 					 ),
												 'descr'	=> array(	'rus' => 'Формат выводимой даты',
												 						'eng' => 'Format of deduced date'),
			   									 'access'	=> 'readonly'
			   									 ),
				'news_tag_separator' 	=> array('type'		=> 'string',
			   									 'defval'	=> 'h2',
			   								 	 'descr'	=> array(	'rus' => 'Тэг для постраничной разбивки',
												 						'eng' => 'tag for paginal breakdown'),
			   									 'access'	=> 'editable'
			   									 ),
				'news_form_rss' 		=> array('type'		=> 'checkbox',
			   									 'defval'	=> '1',
			   									 'descr'	=> array(	'rus' => 'Формировать RSS',
												 						'eng' => 'Form RSS'),
			   									 'access'	=> 'editable'
			   									 ),
			  'news_count_rss' => array('type'	=> 'string',
												 'defval'	=> '10',
												 'descr'	=> array(	'rus' => 'Количество новостей выводимых в RSS',
												 						'eng' => 'RSS count news'),
												 'access'	=> 'editable',
												 ),
			   'news_count_hits'		=> array('type'		=> 'checkbox',
			   									 'defval'	=> 'on',
			   									 'descr'	=> array(	'rus' => 'Учитывать статистику отображения контента',
												 						'eng' => 'To consider statistics of display of a content'),
			   									 'access'	=> 'editable'
			   									 ),
			   'news_default_rights'	=> array('type'		=> 'string',
			   									 'defval'	=> '111111110001',
			   									 'descr'	=> array(	'rus' => 'Права для новости по-умолчанию (формат <img src="/i/admin/rights_format.gif" width=72 height=18 alt="формат прав">, где x - 0 или 1, каждая группа представляет набор прав: удаление, публикация, редактирование, чтение)',
 												 						'eng' => 'News default rights (format <img src="/i/admin/rights_format.gif" width=72 height=18 alt="rights format">, where x - 0 or 1, every group is rights set: delete, publication, edit, read)'),
			   									 'access'	=> 'editable'
			   									 ),
			   'news_comment_cnt' => array('type'		=> 'integer',
            			   						'defval'	=> '20',
            			   						'descr'	=> array(	'rus' => 'Кол-во комментариев на страницу',
            												 		'eng' => 'Comments count on page'),
            			   						'access'	=> 'editable'
            			   						),
			   'news_max_tags_cnt' => array('type'		=> 'integer',
            			   						'defval'	=> '7',
            			   						'descr'	=> array(	'rus' => 'Максимальное кол-во тегов в новостях',
            												 		'eng' => 'Maximum number of tags in news'),
            			   						'access'	=> 'editable'
            			   						),
			   'news_show_tags'		=> array('type'		=> 'checkbox',
			   									 'defval'	=> 'on',
			   									 'descr'	=> array(	'rus' => 'Включить поддержку тегов в новостях',
												 						'eng' => 'Show tags'),
			   									 'access'	=> 'editable'
			   									 ),
               'news_key_mailgun'	=> array('type'		=> 'string',
                                                 'defval'	=> 'key-e2e53874f717a87b99cb2166aba83f18',
                                                 'descr'	=> array(	'rus' => 'Ключ для api Mailgun',
                                                                         'eng' => 'Api key Mailgun'),
                                                 'access'	=> 'editable'
               ),
               'news_url_mailgun' => array(
                   'type'   => 'string',
                   'defval' => 'https://api.mailgun.net/v3/suprashop.ru/messages',
                   'descr'  => array(
                       'rus' => 'Url для api Mailgun',
                       'eng' => 'Api url Mailgun'
                   ),
                   'access' => 'editable'
               ),
);

$OPEN_NEW = array(
    'news_img_path'	=> $CONFIG['files_upload_path'].'news/{SITE_PREFIX}/',	// Путь к картинкам новостей
    'news_order_by' => 'date DESC',
	'news_font_cnt' => 6,
 );

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'news_admin_order_by' => 'id DESC',
'news_max_rows' => '200',
'news_date_format' => '%d.%m.%Y',
'news_tag_separator' => 'H3',
'news_form_rss' => 'on',
'news_count_rss' => '20',
'news_count_hits' => 'on',
'news_default_rights' => '111111110001',
'news_comment_cnt' => '5',
'news_max_tags_cnt' => '6',
'news_show_tags' => 'on',
/* ABOCMS:END */
);
?>