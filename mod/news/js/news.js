var news_msgs = new Array();
function ajaxRead(baseurl) {
	var xmlObj = null;
	if (myForm.img.value) {
		var file = ''+baseurl+'&action=isupload&img_name='+get_file_name(myForm.img.value);

		if(window.XMLHttpRequest){
			xmlObj = new XMLHttpRequest();
		} else if(window.ActiveXObject){
			xmlObj = new ActiveXObject("Microsoft.XMLHTTP");
		} else {
			return;
		}

		xmlObj.onreadystatechange = function(){
			if (xmlObj.readyState == 4) {
				if (xmlObj.status == 200) {
					if (1 == xmlObj.responseXML.getElementsByTagName('data')[0].firstChild.data) {
						document.getElementById('img_attn').style.display = 'block';
						alert("Файл с таким именем уже присутствует на сервере\n");
					} else {
						document.getElementById('img_attn').style.display = 'none';
					}
				} else {
					alert("Ошибка получения данных XML:\n");
				}
			}
		}

		xmlObj.open ('GET', file, true);
		xmlObj.send ('');
	} else {
		document.getElementById('img_attn').style.display = 'none';
	}
}

function get_file_name(path) 
{
	path = path.replace(/\\/g, '/');
	return path.substr(path.lastIndexOf('/')+1);
}

function replaceSelectedText(obj) {
	obj.focus();

	if (document.selection) {
		var s = document.selection.createRange();
		if (s.text) {
			var st='<a href="'+String.fromCharCode(123)+'news_link'+String.fromCharCode(125)+'">'+s.text+'</a>';
			s.text=st;
			return true;
		}
	} else if (typeof(obj.selectionStart)=="number") {
		if (obj.selectionStart!=obj.selectionEnd) {
			var start = obj.selectionStart;
			var end = obj.selectionEnd;

			var st=obj.value.substr(0,start)+'<a href="'+String.fromCharCode(123)+'news_link'+String.fromCharCode(125)+'">'+obj.value.substr(start,end-start)+'</a>'+obj.value.substr(end);
			obj.value = st;
			obj.setSelectionRange(end,end);
		}
		return true;
	}
	return false;
}

function init_objects() {
	Calendar.setup(
		{
			inputField  : "date",      // ID of the input field
			ifFormat    : "dd.mm.y",    // the date format
			button      : "trigger",    // ID of the button
			mondayFirst : true
	//      daFormat    : "M d, y",    // the date format
	//			displayArea : displayDate

		}
	);
	if (myForm && myForm.date.value == "") {
		var date = new Date();
		var day = (date.getDate()<10 ? "0" : "")+date.getDate();
		var month = ((date.getMonth()+1)<10 ? "0" : "")+(date.getMonth()+1);
		myForm.date.value = day+"."+month+"."+date.getFullYear();
		if (shrs == "") shrs = date.getHours();
		if (smin == "") smin = date.getMinutes();
	}
	var sel = document.getElementById("hrs");
	for(var i=0;i<24;i++) {
		var oOption = document.createElement("OPTION");
		sel.options.add( oOption );
		oOption.innerHTML = (i<10?"0":"")+i;
		oOption.value = (i<10?"0":"")+i;
		if(i==shrs)oOption.selected = true;
	}
	sel = document.getElementById("min");
	for(var i=0;i<60;i++) {
		var oOption = document.createElement("OPTION");
		sel.options.add( oOption );
		oOption.innerHTML = (i<10?"0":"")+i;
		oOption.value = (i<10?"0":"")+i;
		if(i==smin)oOption.selected = true;
	}
}

function test_news_form(myForm) {
	//var selIdx = myForm.category.selectedIndex;
	//if (myForm.category.options[selIdx].value == '0') {
	if (myForm.category.value == '') {
		return 3;
	} else if (myForm.title.value == '') {
		return 0;
	} else if (myForm.date.value == '') {
		return 2;
	}

	return -1;
}