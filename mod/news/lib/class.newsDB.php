<?php

class CNewsData_DB extends TObject {

	// объявляем поля таблицы в виде переменных класса
	// внимание, если поле не указано - будут глюки при сохранении и апдейте!
	var $id				= NULL;
	var $category_id	= NULL;
	var $theme_id		= NULL;
	var $theme_title	= NULL;
	var $date			= NULL;
	var $source			= NULL;
	var $announce		= NULL;
	var $announce_with_links	= NULL;
	var $body			= NULL;
	var $split_body		= NULL;
	var $title			= NULL;
	var $title_add			= NULL;
	var $active			= NULL;
	var $calendar_date	= NULL;
	var $hrs			= NULL;
	var $min			= NULL;
	var $img			= NULL;
	var $forum_theme	= NULL;
	var $seo_title    = NULL;
	var $description	= NULL;
	var $keywords		= NULL;
	var $rss_id         = NULL;
	var $rss_link = null;
	var $owner_id		= NULL;
	var $usr_group_id	= NULL;
	var $rights			= NULL;
	var $tags			= NULL;
	
	// функция-конструктор
	// $table - таблица, где хранятся данные,
	// $PK - её Primary Key, уникальное поле, по которому TObject
	// будет ориентироваться в таблице и строить запросы WHERE
	// также $table после конструктора, внутри класса может быть вызван, как $this->_tbl
	// а первичный ключ, как $this->_tbl_key
	function CNewsData_DB ($table, $PK) {
		$this->TObject($table, $PK);
	}

	// функция возвращает все записи из таблицы в виде массива
	function ListAll () {
		$sql = "SELECT * FROM $this->_tbl";
		return db_loadList($sql);
	}

	function ListAllBy ($ctg_str, $order_by) {
		global $CONFIG;
		$query = 	"SELECT n.id, n.title, t.title as theme_title, n.announce, n.announce_with_links, NOW() as d, DATE_FORMAT(n.date,'" . $CONFIG['news_date_format'] . "') as news_date, n.img
					FROM $this->_tbl n
						LEFT JOIN ".$this->_tbl."_themes t ON n.theme_id = t.id
					WHERE ".My_Sql::escape($ctg_str)." date <= NOW() AND active = 1
					ORDER BY ".My_Sql::escape($order_by);
 		return db_loadList($query);
	}

	function ListBy ($ctg_str, $order_by, $start_row, $rows) {
		global $CONFIG;
		$query = 	"SELECT n.id, n.title, t.title as theme_title, n.announce, n.announce_with_links, NOW() as d, DATE_FORMAT(n.date,'" . $CONFIG['news_date_format'] . "') as news_date, n.img
					FROM $this->_tbl n
						LEFT JOIN ".$this->_tbl."_themes t ON n.theme_id = t.id
					WHERE ".My_Sql::escape($ctg_str)." date <= NOW() AND active = 1
					ORDER BY ".My_Sql::escape($order_by)."
					LIMIT ".intval($start_row).",".intval($rows);
 		return db_loadList($query);
	}

	// функция загрузки новостей (титулы)
	function load_titles($category, $start_row, $rows, $order_by) {
		global $CONFIG;
		$sql = "SELECT *, DATE_FORMAT(date,'" . $CONFIG["news_date_format"] . "') as news_date FROM $this->_tbl WHERE category_id = "
		    .intval($category)." ORDER BY ".My_Sql::escape($order_by)." LIMIT ".intval($start_row).",".intval($rows);
		return db_loadList($sql);
	}

	// функция загрузки новости по id
	function load_news($id) {
		global $CONFIG;

		//$sql = "SELECT *, DATE_FORMAT(date,'".$CONFIG["news_date_format"]."') as date, DATE_FORMAT(date,'".$CONFIG["calendar_date"]."') as calendar_date, DATE_FORMAT(date,'%H') as hrs, DATE_FORMAT(date,'%i') as min FROM $this->_tbl WHERE id = $id";
		$sql = "SELECT *, DATE_FORMAT(date,'".$CONFIG["news_date_format"]."') as date,";
		$sql .= " DATE_FORMAT(date,'".$CONFIG["calendar_date"]."') as calendar_date,";
		$sql .= " DATE_FORMAT(date,'%H') as hrs, DATE_FORMAT(date,'%i') as min";
		$sql .= " FROM $this->_tbl WHERE id = ".intval($id);
		return db_loadObject($sql, $this);
	}

	function show_news($id) {
		global $CONFIG;
		$sql = "SELECT n.*, t.title as theme_title, DATE_FORMAT(date,'" . $CONFIG["news_date_format"] . "') as date
				FROM $this->_tbl n
					LEFT JOIN ".$this->_tbl."_themes t ON n.theme_id = t.id
				WHERE n.id = ".intval($id)." AND active = 1";
		return db_loadObject($sql, $this);
	}

	// функция удаления записи из таблицы по id
	function DeleteByCategory($id) {
		$sql = "DELETE FROM $this->_tbl WHERE category_id = ".intval($id);
		db_exec($sql);
		if (!db_affected_rows() > 0) {
			return 0;
		} else {
			return 1;
		}
	}

	/**
	 * Получить список всех картинок новостей заданной какегории
	 * @param int $category
	 * @return array
	 */
	function LoadImgs($category) {
		$sql = "SELECT img FROM $this->_tbl WHERE category_id = ".intval($category);
		return db_loadList($sql);
	}
}

class CNewsCategories_DB extends TObject {
	var $id				= NULL;
	var $title			= NULL;
  var $owner_id		= NULL;
  var $usr_group_id	= NULL;
  var $rights			= NULL;
  var $rss_auto_update = null;
  var $rss_update_interval = null;
  var $commentable = null;


	// конструктор
	function CNewsCategories_DB ($table, $PK) {
		$this->TObject($table, $PK);
	}

	// показать всё
	function ListAll () {
		$sql = "SELECT * FROM $this->_tbl ORDER BY id";
		return db_loadList($sql);
	}

	// удалить категорию
	function DeleteByID($id) {
		global $main;
		$sql = "DELETE FROM $this->_tbl WHERE id = ".intval($id);
		db_exec($sql);
		if (!db_affected_rows() > 0) {
			return 0;
		} else {
			return 1;
		}
	}
}

/**
 * Класс для работы с RSS-каналами.
 *
 */
class CNewsRss_DB extends TObject
{
	/**
	 * Конструктор класса.
	 *
	 * @param string $table - таблица, где хранятся данные
	 * @param string $PK - Primary Key, уникальное поле, по которому TObject будет ориентироваться в таблице и строить запросы WHERE
	 * <br /> также $table после конструктора, внутри класса может быть вызван, как $this->_tbl, а первичный ключ, как $this->_tbl_key
	 * @return CNewsRss_DB
	 */
	function CNewsRss_DB ($table, $PK)
	{
		$this->TObject($table, $PK);
	}

	/**
	 * Получить список всех RSS-каналов.
	 *
	 * @param int $category_id - идентификатор категории, если 0 - получаем каналы всех категорий
	 * @return ассоциативный массив с данными о RSS-каналах
	 */
	function GetRssChannels($category_id)
	{
		$category_id = intval($category_id);

		$where = ' AND 1 ';
		if ($category_id)
		{
			$where .= $this->_tbl."category_id = '$category_id'";
		}

		$sql = "SELECT * FROM $this->_tbl WHERE $where ORDER BY id";
		return db_loadList($sql);
	}
}
?>