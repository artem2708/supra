<?php
require_once (RP.'mod/news/lib/class.newsDB.php');

class NewsPrototype extends Module {

	var $main_title;
	var $table_prefix;
	var $addition_to_path;									//     ,
	var $module_name				= 'news';
	var $default_action				= 'tlist';				// ,
	var $admin_default_action		= 'showctg';
	var $tpl_path					= 'mod/news/tpl/';
	var $block_module_actions		= array();
	var $block_main_module_actions	= array();
	var $allow_img_typs = array('image/gif', 'image/jpeg', 'image/png', 'image/pjpeg', 'image/x-png');
	var $action_tpl = array(
	   'archive' => 'news_archive.html',
	   'tlist' => 'news_titles.html',
	   'tlist_archive' => 'news_titles_archive.html',
	   'alist' => 'news_announces.html',
	   'alist_archive' => 'news_announces_archive.html',
	   'rss_list' => 'news_rss_titles.html',
	   'clouds' => 'news_clouds.html',
	   'tag' => 'news_tag.html',
	   'categories' => 'news_categories.html',
       'calendar' => 'news_calendar.html'
	);
	var $_cache = array();

	function NewsPrototype($action = '', $transurl = '', $properties = array(), $prefix = NULL, $rewrite_mod_params = NULL, $adminurl = NULL, $noadmin = false) {
		global $main, $core, $tpl, $tpl_path, $baseurl, $cache, $lc, $CONFIG, $PAGE, $server, $lang, $permissions, $request_type, $_RESULT;
		
		/* Get $action, $tpl_name, $permissions */
		extract( $this->init(array(
		        'server' => $server, 'lang' => $lang, 'prefix' => $prefix,
		        'action' => $action, 'tpl_name' => $properties['tpl']
		)), EXTR_OVERWRITE);
		
		$CONFIG['news_img_path'] = str_replace('{SITE_PREFIX}', $this->table_prefix, $CONFIG['news_img_path']);

		$category = $_REQUEST['id'] >=1 && $PAGE['field']==0 ? (int)$_REQUEST['id'] : (int)$properties[1];
		$rows = $properties[2] >= 1 ? (int)$properties[2] : $CONFIG['news_max_rows'];
		$start = 0 == $PAGE['field'] && $GLOBALS['request_start'] > 1 ? (int) $GLOBALS['request_start'] : 1;
		$filter = array(
			'ctgr' => $category,
			'start' => $start,
			'limit' => $rows
        );
		if($PAGE['field']==0 && $_REQUEST['date'] && preg_match('~(\d{2,4})\-(\d{2})\-(\d{2})~',$_REQUEST['date'],$M)){
		    $filter['date'] = sprintf('%04d%02d%02d',$M[1],$M[2],$M[3]);
		    $this->addition_to_path[sprintf('%02d-%02d-%04d',$M[3],$M[2],$M[1])] = '?date='.$_REQUEST['date'];
		}
		
		if ('only_create_object' == $action) {
		    return;
		}
		
		if ($this->actionExists($action)) {
		    $this->doAction($action);
		    return;
		}
		
		if (! self::is_admin()) {
		    //----------------------------------  Клиентаская часть (сайт)   --------------------------------//
		    if (isset($PAGE['dop_params']) && $PAGE['dop_params'][2] && $PAGE['field']==0) {
		        $action = 'show';
		        if (! $this->getNews($PAGE['dop_params'][2], $PAGE['dop_params'][1])) {
		            return;
		        }
		        $GLOBALS['request_id'] = $PAGE['dop_params'][2];
		        $GLOBALS['request_category'] = $PAGE['dop_params'][1];
		    }
			$adm_tpl = array (
				'news_edit_up'		=> $CONFIG['admin_tpl_path'].'_news_adm_up.html',
				'news_edit_middle'	=> $CONFIG['admin_tpl_path'].'_news_adm_middle.html',
				'news_edit_bottom'	=> $CONFIG['admin_tpl_path'].'_news_adm_bottom.html',
			);

			if ($this->actionExists($action)) {
			    $this->doAction($action);
			    return;
			}
            //echo $action;
            switch ($action) {




                case "subscr":
                    global $request_mail, $db, $server, $lang;
                    if ($request_mail) {
                        $mail = $db->getArrayOfResult('Select mail,`active` from ' . $this->table_prefix . '_news_users Where mail="' . $request_mail . '"');
                        if (!empty($mail)) {
                            if ($mail[0]['active'] == 0) {
                                $db->query("UPDATE {$this->table_prefix}_news_users SET `active`=1 WHERE mail='{$request_mail}'");
                                exit;
                            }
                            echo 1;
                            exit;
                        } else {
                            $db->query("INSERT INTO {$this->table_prefix}_news_users (`mail`, `news_id`, `active`) VALUES ( '{$request_mail}', '1', '1');");
                            exit;
                        }
                    }

                    break;

                case "unsubscr":
                    global $request_mail, $db, $server, $lang;
                    if ($request_mail) {
                        $mail = $db->getArrayOfResult('Select id,mail from ' . $this->table_prefix . '_news_users Where mail="' . $request_mail . '"');
                        if (!empty($mail)) {
                            $db->query("UPDATE {$this->table_prefix}_news_users SET `active`=0 WHERE mail='{$request_mail}'");
                            header('Location: /unsubscr');
                            exit;
                        }
                        header('Location: /');
                        exit;
                    }

                    break;
			case "tlist":
				global $request_start, $server, $lang;

				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main', $adm_tpl);
				$tpl->prepare();				
				$pages_cnt = $this->show_news_list('block_news_titles', $transurl, $filter, $adminurl);
				if ($pages_cnt) {
				    $_baseurl = $baseurl;
				    $baseurl = $transurl;
					$main->_show_nav_block($pages_cnt, null, 'action='.$action.'&category='.$category, $start);
					$baseurl = $_baseurl;
				}
				break;

			case 'tlist_archive':
				global $request_start, $lang;

				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main', $adm_tpl);
				$tpl->prepare();
				$this->show_news_list('block_news_titles', $transurl, $filter, $adminurl);
				break;


//
			case "archive_link":
				global $request_start, $server, $lang;
				$order_by = $CONFIG["news_order_by"];
				if ($category) $where = "AND category_id = $category";
				$main->include_main_blocks($this->module_name . "_archive_link.html", "main", $adm_tpl);
				$tpl->prepare();
				$main->_show_nav_string($this->table_prefix.'_news',
										$where, '',
										'action='.$action.'&category='.$category,
										$start,
										$rows,
										$order_by, '', 0);
				break;


//
			case 'archive':
				global $request_start, $server, $lang;
				$order_by = $CONFIG['news_order_by'];
				if ($category) $where = 'AND category_id = '.$category;
				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main', $adm_tpl);
				$tpl->prepare();
				$pages_cnt = $this->show_news_list('block_news_announces', $transurl, $filter, $adminurl);
				if ($pages_cnt) {
				    $_baseurl = $baseurl;
				    $baseurl = $transurl;
					$main->_show_nav_block($pages_cnt, null, 'action='.$action.'&category='.$category.'&ids='.htmlspecialchars($_GET['ids']), $start);
					$baseurl = $_baseurl;
				}
				break;

			case 'alist':
				global $request_start, $server, $lang;

				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main', $adm_tpl);
				$tpl->prepare();
				if($tpl_name == "news_announces_video.html") $filter['prod'] = true;
				$pages_cnt = $this->show_news_list('block_news_announces', $transurl, $filter, $adminurl);
				if ($pages_cnt) {
				    $_baseurl = $baseurl;
				    $baseurl = $transurl;
					$main->_show_nav_block($pages_cnt, null, 'action='.$action.'&category='.$category, $start);
					$baseurl = $_baseurl;
				}
				break;

//       +
			case 'alist_archive':
				global $request_start, $lang;
				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main', $adm_tpl);
				$tpl->prepare();
				$this->show_news_list('block_news_announces', $transurl, $filter, $adminurl);
				break;

			case 'rss_list':
				global $request_start, $server, $lang;

				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main', $adm_tpl);
				$tpl->prepare();
				$pages_cnt = $this->show_news_list('block_news_titles', $transurl, $filter, $adminurl);
				if ($pages_cnt) {
				    $_baseurl = $baseurl;
				    $baseurl = $transurl;
					$main->_show_nav_block($pages_cnt, null, 'action='.$action.'&category='.$category, $start);
					$baseurl = $_baseurl;
				}

				break;


//    id
			case 'show':
				global $request_id, $request_category;
				$request_id = (int)$request_id;
				$main->include_main_blocks($this->module_name.'.html', 'main', $adm_tpl);
				$tpl->prepare();

				if (file_exists(RP."mod/comments")) {
				    require_once(RP."mod/comments/lib/class.CommentsPrototype.php");
				    $comm = new CommentsPrototype('only_create_object');
				    $comm->showNewsComments($request_id, $CONFIG['news_comment_cnt']);
				}
				if ($ret = $this->show_news($transurl, $request_id, $request_category, $adminurl)) {
    				
					$filter = array(
						'ctgr' => $ret,
						'start' => 1,
						'nid' => $request_id,
						'limit' => 3
					);
					
					$pages_cnt = $this->show_news_list('block_news_titles', $baseurl, $filter, $adminurl);
					if ($CONFIG['news_count_hits']) {
    					Main::addHits($this->table_prefix."_news", $request_id);
    				}
    				$PAGE['process_params'] = true;
				}
				break;

			case 'tag':
			    $tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main');
				$tpl->prepare();
				
				global $request_tag;
				if ($request_tag && ($tag = $this->getTagsByNames(array($request_tag))) ) {
				    $tag = array_shift($tag);
				    $tpl->assign(array(
				        'tag_id' => $tag['id'],
				        'tag' => $tag['name'],
				        'baseurl' => $baseurl,
				        'transurl' => $transurl
				    ));
				    unset($filter['ctgr']);
				    $filter['tag'] = $tag['name'];
				    
					$pages_cnt = $this->show_news_list('block_news_announces', $transurl, $filter, $adminurl);
					if ($pages_cnt) {
					    $_baseurl = $baseurl;
					    $baseurl = $transurl;
						$main->_show_nav_block($pages_cnt, null, 'action='.$action.'&tag='.$tag['name'].($category ? '&category='.$category : ''), $start);
						$baseurl = $_baseurl;
					}
				}
				break;
				
			case 'clouds':
				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main');
                $tpl->prepare();
                
                $tpl->assignList('tag_', $this->getTagCloud(), array('transurl' => $transurl, 'divider' => ','));
                break;
				
			case 'categories':
				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main');
                $tpl->prepare();
                $filter = array(
                    'ctgr' => (array)$properties[1],
                    'only_editable' => false,
                    'with_news_cnt' => $properties[3]
                );
                $this->show_categories('list', $transurl, $filter);
                break;
                
            case 'calendar':
				global $request_start;
				$tpl_name = $tpl_name ? $tpl_name : $this->action_tpl[$action];
                $main->include_main_blocks($tpl_name, 'main', $adm_tpl);
				$tpl->prepare();
                $month = $_REQUEST['date'] ? $_REQUEST['date'] : date('Y-m');                
                preg_match('~^(\d{4})?-?(\d{1,2})(-(\d{1,2}))?$~',(string) $month,$M);
                $m = $M[2]?$M[2]:date('m');
                $y = $M[1]?$M[1]:date('Y');
                $days = date('t',mktime(0,0,0,$m,1,$y));
                $newsDay = $this->getNewsDates($month, $category);
                $weeks = array();
                for($i=1;$i<=$days;$i++){
                    $dayOfWeek = date('w',mktime(0,0,0,$m,$i,$y));
                    $numWeek = date('W',mktime(0,0,0,$m,$i,$y));
                    $ret = array('day'=>$i);
                    if(in_array($i,$newsDay)){
                        $ret['link'] = sprintf('%sdate=%s',$transurl,date('Y-m-d',mktime(0,0,0,$m,$i,$y)));
                        if ($category) {
                            $ret['link'] .= '&id='.$category;
                        }
                        $ret['date'] = date('d-m-Y',mktime(0,0,0,$m,$i,$y));
                    }
                    if(date('d-m-Y',mktime(0,0,0,$m,$i,$y)) == date('d-m-Y')){
                        $ret['curDate'] = true;
                    }
                    $weeks[$numWeek][$dayOfWeek] = $ret;
                }
                //выведем название месяца и ссылки вперед/назад
                $mI = array(1=>'Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентабрь','Октябрь','Ноябрь','Декабрь');
                $INF = array('monthName'=>$mI[(int) $m],'yearName'=>$y);
                
                $INF['nextMonth'] = sprintf('%sdate=%s',$transurl,date('Y-m',mktime(0,0,0,$m+1,1,$y)));
                $INF['prevMonth'] = sprintf('%sdate=%s',$transurl,date('Y-m',mktime(0,0,0,$m-1,1,$y)));
                $tpl->assign($INF);
                
                //выведем календарь
                foreach($weeks as $weekInfo){
                    $tpl->newBlock('block_week');
                    foreach(array(1,2,3,4,5,6,0) as $i){
                        $tpl->newBlock('block_day');
                        if(!$weekInfo[$i])
                            $tpl->newBlock('block_empty');
                        elseif(!$weekInfo[$i]['link'])
                            $tpl->newBlock('block_day_nolink');
                        else
                            $tpl->newBlock('block_day_link');
                        $tpl->assign($weekInfo[$i]);
                    }
                }
                break;

// Default...
			default:
				if (is_object($tpl)) $tpl->prepare();
				return;
			}
//-------------------------------      -----------------------------//
		}	else {
//------------------------------   Админка  -----------------------------//
			$this->block_module_actions	= array(
				'archive'		=> $this->_msg["Show_news_with_pager"],
				'tlist'			=> $this->_msg["Show_news_titles"],
				'tlist_archive' => $this->_msg["Show_news_titles_plus_link"],
				'alist'			=> $this->_msg["Show_news_announces"],
				'alist_archive'	=> $this->_msg["Show_news_announces_plus_link"],
				'rss_list'		=> $this->_msg["Show_news_rss_list"],
			    'categories'	=> $this->_msg["Show_categories"],
				'clouds'	=> $this->_msg["clouds"],
                'calendar'		=> 'Показать календарь',
			);
			$this->block_main_module_actions = array(
				'archive'	=> $this->_msg["Show_news_with_pager"],
				'tlist'			=> $this->_msg["Show_news_titles"],
				'tlist_archive' => $this->_msg["Show_news_titles_plus_link"],
				'alist'			=> $this->_msg["Show_news_announces"],
				'alist_archive'	=> $this->_msg["Show_news_announces_plus_link"],
				'rss_list'		=> $this->_msg["Show_news_rss_list"],
			    'categories'	=> $this->_msg["Show_categories"],
				'clouds'	=> $this->_msg["clouds"],
			    'calendar'		=> 'Показать календарь',
			);
			if ($this->actionExists($action)) {
			    $this->doAction($action);
			    return;
			}
			if (!isset($tpl)) {
			    $main->message_die('Шаблон не определен');
			}

			switch ($action){

                case "mailgun_check":
                    if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
                    global $db,$request_id;
                    $mails_date = $db->getArrayOfResult('Select id,category_id,active from ' . $this->table_prefix . '_news Where id='.$request_id);
                    if(!empty($mails_date))foreach($mails_date as $mail)
                    {

                        if($mail['active'] && $mail['category_id']==9) {
                            $news = $this->getNews($mail['id'], $mail['category_id']);
                            if($news)
                            {
                                //                        require_once(RP.'/inc/class.Mailer.php');
                                //                        $mail = new Mailer();
                                $tpl1 = new TemplatePower(RP.'_news_mail.html', T_BYFILE);
                                $tpl1->prepare();
                                $subject = "Новости от supra:".$news['title'];
                                //                        $mail->CharSet		= $CONFIG['email_charset'];
                                //                        $mail->ContentType	= "text/html"; //$CONFIG['email_type'];
                                //                        $mail->From			= "Supra-News";// $CONFIG['shop_order_recipient'];
                                //                        $mail->Mailer		= 'mail';

                                $adr=$db->getArrayOfResult('Select mail FROM `sup_rus_news_users` Where `active`=1');

                                //                        $mail->Subject		= $subject;

                                $date=$this->getNewsDate($news["date"]);
                                $img=substr($news['img'],0,-11)."_middle.jpg";
                                $tpl1->assign(array(
                                    'title'		=>	$news["title"],
                                    'date'		=>	$date,
                                    'text'		=>	$news["body"],
                                    'img'	    =>	$img,
                                ));
                                $body=$tpl1->getOutputContent();

                                $ch = $this->init_mailgun();
                                if($adr)foreach($adr as $mails){
                                    /*$mail->Body	*/$bodyn		= $body."<a href='http://suprashop.ru/news?action=unsubscr&mail=".$mails['mail']."'>Отписаться от рассылки</a><br>
                        <p align='center'>Официальный интернет-магазин <a href='http://suprashop.ru/'>suprashop.ru</a></p>
                        <p align='center'>Наш канал на youtube <a href='http://www.youtube.com/user/Supram3n'>www.youtube.com/user/Supram3n</a></p>
                        </div>";
                                    $this->send_mailgun($mails['mail'],$subject,$bodyn,$ch);//$mails['mail']
                                    //                        $mail->AddAddress();
                                    //                        $mail->Send();
                                    //                        $mail->ClearAddresses();
                                }
                                $this->close_mailgun($ch);


                            }

                            $db->query("UPDATE `sup_rus_news` SET `mail_check` = 1  Where id = ".$mail['id']);
                        }
                    }
                    header('Location: '.$_SERVER['HTTP_REFERER']);exit;
                    break;
    	    case 'prp':
    	    	if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
    				global $request_step;
    				$main->include_main_blocks('_properties.html', $this->tpl_path);
    				$tpl->prepare();
    				$tpl->assign(Array(
    					"MSG_Information"	=> $main->_msg["Information"],
    					"MSG_Value"			=> $main->_msg["Value"],
    					"MSG_Save"			=> $main->_msg["Save"],
    					"MSG_Cancel"		=> $main->_msg["Cancel"],
    				));
    		        if (!$request_step || $request_step == 1) {
    					$tpl->assign(array('form_action'	=> $baseurl,
    									   'step'			=> 2,
    									   'lang'			=> $lang,
    									   'name'			=> $this->module_name,
    									   'action'			=> 'prp'));
    					$prp_html = $main->getModuleProperties($this->module_name);
    		            $this->main_title = $this->_msg["Controls"];
    				} elseif (2 == $request_step) {
    		        	$main->setModuleProperties($this->module_name, $_POST);
    					header('Location: '.$baseurl.'&action=prp&step=1');
    				}
    		    break;
    
            case 'block_prp':
      				global $request_sub_action, $request_block_id;
      		    	$arr  = $this->getEditLink($request_sub_action, $request_block_id);
      		    	$cont = 'var container = document.getElementById(\'target_span\');
      		    		  	 container.innerHTML = "'.$this->getPropertyFields().'";
      		    		  	 material_id		  = "'.$arr['material_id'].'";
      		    		  	 material_url		  = "'.$arr['material_url'].'";
      		    		  	 changeAction();
      		    	      	 setURL("property1");';
      				header('Content-Length: '.strlen($cont));
    		    	echo $cont;
    		    	exit;
    		    	break;

			case "shownews":
				if (!$permissions["r"]) $main->message_access_denied($this->module_name, $action);
				global $request_start, $request_category;
				$category = ($request_category) ? (int)$request_category : 0;
				$main->include_main_blocks_2($this->module_name."_list.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Activ" => $this->_msg["Activ"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Rights" => $main->_msg["Rights"],
					"MSG_Date" => $this->_msg["Date"],
					"MSG_Title" => $this->_msg["Title"],
					"MSG_Shows" => $this->_msg["Shows"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Select_action" => $this->_msg["Select_action"],
					"MSG_Activate" => $this->_msg["Activate"],
					"MSG_Suspend" => $this->_msg["Suspend"],
					"MSG_Delete" => $this->_msg["Delete"],
					"MSG_Execute" => $this->_msg["Execute"],
					'category' => $category
				));
                if($category==9)
                {
                    $tpl->newBlock('block_mailgun');
                    $tpl->prepare();


                }
				list($category_title, $ctg_owner_id) = $this->show_category($request_category);
				$pages_cnt = $this->show_news_titles($category, $start, null, null, $ctg_owner_id);
				if (empty($pages_cnt)) {
					header("Location: $baseurl&action=add");
					exit;
				}
				//
				//      :
				// ($table = , $where =  , $blockname =  ,
				// $action =   , $start =  / ,
				// $rows = - , $order_by =  , $separator = )
				$main->_show_nav_block($pages_cnt, null, 'action='.$action.'&category='.$category, $start);
				$tpl->assign(Array(
					"MSG_Pages" => $this->_msg["Pages"],
				));
				$this->main_title = ($category_title) ? $category_title : "";
				break;


//
			case "add":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_step, $request_category, $request_theme, $request_title, $request_announce,
						$request_txt_content, $request_split_body, $request_date, $request_hrs, $request_min,
						$request_description, $request_keywords, $request_seo_title, $request_block_id, $request_reload_content,
						$request_tryon, $request_source, $request_img, $request_tags;
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path, $request_type == "JsHttpRequest" ? 'main' : '');
					$tpl->prepare();
					$tpl->assign(array(
						"MSG_File_already_exists" => $this->_msg["File_already_exists"],
						"MSG_Enter_title" => $this->_msg["Enter_title"],
						"MSG_Enter_announce" => $this->_msg["Enter_announce"],
						"MSG_Choose_date" => $this->_msg["Choose_date"],
						"MSG_Choose_category" => $this->_msg["Choose_category"],
						"MSG_Enter_title" => $this->_msg["Enter_title"],
						"MSG_Category" => $this->_msg["Category"],
						"MSG_Select_category" => $this->_msg["Select_category"],
						"MSG_News_topics" => $this->_msg["News_topics"],
						"MSG_Without_topic" => $this->_msg["Without_topic"],
						"MSG_Title" => $this->_msg["Title"],
						"MSG_Source" => $this->_msg["Source"],
						"MSG_Announce_short" => $this->_msg["Announce_short"],
						"MSG_Mark_link" => $this->_msg["Mark_link"],
						"MSG_News_content" => $this->_msg["News_content"],
						"MSG_Split_content_by_tag" => $this->_msg["Split_content_by_tag"],
						"MSG_Picture" => $this->_msg["Picture"],
						"MSG_Publish_date" => $this->_msg["Publish_date"],
						"MSG_Time" => $this->_msg["Time"],
						"MSG_Select" => $this->_msg["Select"],
						"MSG_Page_description" => $this->_msg["Page_description"],
						"MSG_Page_title" => $this->_msg["Page_title"],
						"MSG_Keywords" => $this->_msg["Keywords"],
						"MSG_required_fields" => $this->_msg["required_fields"],
						"MSG_Save" => $this->_msg["Save"],
						"MSG_Cancel" => $this->_msg["Cancel"],
						"MSG_Try_on" => $this->_msg["Try_on"],
						"MSG_rss_link" => $this->_msg["rss_link"],
						"MSG_Categories" => $this->_msg["Categories"],
						"MSG_Categories_of_news" => $this->_msg["Categories_of_news"],
						"MSG_Add_to_sel_ctgrs" => $this->_msg["Add_to_sel_ctgrs"],
						"MSG_Del_from_sel_ctgrs" => $this->_msg["Del_from_sel_ctgrs"],
						'_ROOT.form_action'	=> "http://".$_SERVER["HTTP_HOST"]."/".$baseurl.'&action=add&step=2'
											.($request_type == "JsHttpRequest" ? '&type=JsHttpRequest' : ''),
						'_ROOT.cancel_link'	=> "http://".$_SERVER["HTTP_HOST"]."/".$baseurl.'&action=showctg',
						'_ROOT.checked'		=> 'checked',
						'_ROOT.baseurl'	=> $baseurl,
						'max_tags' => $CONFIG['news_max_tags_cnt']
					));
					$filter = array();
					$nf = $this->show_categories('block_categories', $transurl, $filter);
					$this->show_themes('block_themes');
					if ($nf == 0) {
						header('Location: '.$baseurl.'&action=showctg&empty');
						exit;
					}
					$this->main_title	= $this->_msg["Add_news"];
					if ($request_type == "JsHttpRequest") {
						$_RESULT = array(
										"js_text"	=> "var shrs = 0;
var smin = 0;

news_msgs[0]	= '".$this->_msg["Enter_title"]."';
news_msgs[1]	= '".$this->_msg["Enter_announce"]."';
news_msgs[2]	= '".$this->_msg["Choose_date"]."';
news_msgs[3]	= '".$this->_msg["Choose_category"]."';

init_objects();",
									);
					}
				} else if ($request_step == 2) {
					list($allow, $ctg_rights) = $this->test_item_rights("e", "id", $this->table_prefix."_news_categories",
																		$request_category, false, 0, true);
					if (!$allow) $main->message_access_denied($this->module_name, $action);

					$block_id = intval(substr($request_block_id, 10));
					$lid = $this->add_news($request_category, $request_theme, $request_title, $request_announce,
										$request_txt_content, $request_split_body, $request_date, $request_hrs,
										$request_min, $request_seo_title, $request_description, $request_keywords, $request_img,
										$request_tags, $ctg_rights, $block_id, '', 0, '', $request_source);
					if ($lid) {
						@$cache->delete_cache_files();
						$lc->add_internal_links($this->module_name, $this->table_prefix.'_news', 'body', $lid, '  ', $request_txt_content);
						if ($CONFIG["news_form_rss"] == "on") {
							$this->form_rss();
						}
						$ctgr_list = explode(",", $request_category);
						if ($request_type == "JsHttpRequest") {
							if ($request_reload_content) {
								list($block_content, $next_block_id) = $core->get_block_content_with_menu($block_id);
								$_RESULT = array(
									"res" => $lid,
									"id" => $request_id,
									"content" => $block_content,
									"next_block" => $next_block_id,
									"replace_content" => true
								);
							} else {
								$_RESULT = array(
									"res" => $lid,
									"replace_content"	=> false,
								);
							}
						} else if ($request_tryon) {
						    header('Location: '.$baseurl.'&action=edit&category='.$ctgr_list[0]."&id=".$lid);
						} else {
							header('Location: '.$baseurl.'&action=shownews&category='.$ctgr_list[0]);
						}
					} else {
						header('Location: '.$baseurl.'&action=showctg');
					}
					exit;
				}
				break;

//
			case 'edit':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start,$request_tryon;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_news", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$main->include_main_blocks_2($this->module_name.'_edit.html', $this->tpl_path, $request_type == "JsHttpRequest" ? 'main' : '');
				$tpl->prepare();
				$tpl->assign(array(
					"MSG_File_already_exists" => $this->_msg["File_already_exists"],
					"MSG_XML_data_error" => $this->_msg["XML_data_error"],
					"MSG_Enter_title" => $this->_msg["Enter_title"],
					"MSG_Enter_announce" => $this->_msg["Enter_announce"],
					"MSG_Choose_date" => $this->_msg["Choose_date"],
					"MSG_Choose_category" => $this->_msg["Choose_category"],
					"MSG_Enter_title" => $this->_msg["Enter_title"],
					"MSG_Category" => $this->_msg["Category"],
					"MSG_Select_category" => $this->_msg["Select_category"],
					"MSG_News_topics" => $this->_msg["News_topics"],
					"MSG_Without_topic" => $this->_msg["Without_topic"],
					"MSG_Title" => $this->_msg["Title"],
					"MSG_Source" => $this->_msg["Source"],
					"MSG_Announce_short" => $this->_msg["Announce_short"],
					"MSG_Mark_link" => $this->_msg["Mark_link"],
					"MSG_News_content" => $this->_msg["News_content"],
					"MSG_Split_content_by_tag" => $this->_msg["Split_content_by_tag"],
					"MSG_Picture" => $this->_msg["Picture"],
					"MSG_Publish_date" => $this->_msg["Publish_date"],
					"MSG_Time" => $this->_msg["Time"],
					"MSG_Select" => $this->_msg["Select"],
					"MSG_Page_description" => $this->_msg["Page_description"],
					"MSG_Page_title" => $this->_msg["Page_title"],
					"MSG_Keywords" => $this->_msg["Keywords"],
					"MSG_required_fields" => $this->_msg["required_fields"],
					"MSG_Save" => $this->_msg["Save"],
					"MSG_Cancel" => $this->_msg["Cancel"],
					"MSG_Try_on" => $this->_msg["Try_on"],
					"MSG_rss_link" => $this->_msg["rss_link"],
					"MSG_Categories" => $this->_msg["Categories"],
					"MSG_Categories_of_news" => $this->_msg["Categories_of_news"],
					"MSG_Add_to_sel_ctgrs" => $this->_msg["Add_to_sel_ctgrs"],
					"MSG_Del_from_sel_ctgrs" => $this->_msg["Del_from_sel_ctgrs"],

					'_ROOT.form_action'	=> "http://".$_SERVER["HTTP_HOST"]."/".$baseurl.'&action=update&id='.$request_id
											.($request_type == "JsHttpRequest" ? '&type=JsHttpRequest' : ''),
					'_ROOT.cancel_link'	=> "http://".$_SERVER["HTTP_HOST"]."/".$baseurl.'&action=shownews&category='.$request_category.'&start='.$request_start,
					'_ROOT.baseurl'	=> "http://".$_SERVER["HTTP_HOST"]."/".$baseurl,
					'max_tags' => $CONFIG['news_max_tags_cnt']
				));
				list($category_id, $theme_id, $hrs, $min) = $this->show_news_adm($request_id, $request_start);
				if (!$category_id) {
					header('Location: '.$baseurl.'&action=showctg');
					exit;
				}
				$news_ctgr_list = $this->getNewsCtgrList($request_id);
				if ($news_ctgr_list) {
				    $tpl->assign('category_list', implode(",", array_keys($news_ctgr_list)));
				    foreach ($news_ctgr_list as $val) {
    					$tpl->newBlock('block_news_categories');
    					$tpl->assign($val);
    				}
				}
				$filter = array(
				    'selected' => $category_id
				);
				$this->show_categories('block_categories', $transurl, $filter);
				$this->show_themes('block_themes', $theme_id);
				$this->main_title	= $this->_msg["Edit_news"];
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
						"js_text"	=> "var shrs = ".$hrs.";
            var smin = ".$min.";
            news_msgs[0]	= '".$this->_msg["Enter_title"]."';
            news_msgs[1]	= '".$this->_msg["Enter_announce"]."';
            news_msgs[2]	= '".$this->_msg["Choose_date"]."';
            news_msgs[3]	= '".$this->_msg["Choose_category"]."';

            init_objects();",
					);
				}
				break;

//
			case 'update':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_theme, $request_title, $request_announce,
						$request_txt_content, $request_split_body, $request_date, $request_hrs, $request_min,
						$request_seo_title, $request_description, $request_keywords, $request_del_img, $request_start,
						$request_block_id, $request_rss_link, $request_rss_id, $request_tryon, $request_source,
						$request_img, $request_tags;


				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_news", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$res = $this->update_news($request_category, $request_theme, $request_title,
				$request_announce, $request_txt_content, $request_split_body, $request_date,
				$request_hrs, $request_min, $request_seo_title, $request_description, $request_keywords,
				$request_img, $request_tags, $request_id, $request_source);
				if ($res) {
					@$cache->delete_cache_files();
					$lc->add_internal_links($this->module_name, $this->table_prefix.'_news', 'body', $request_id, $this->_msg["Link_in_news"], $request_txt_content);
					if ($CONFIG["news_form_rss"] == "on") {
						$this->form_rss();
					}
    				if ($request_tryon) {
    					header('Location: '.$baseurl.'&action=edit&category='.$request_category
    					   ."&id=".$request_id.($request_start > 1 ? "&start=".$request_start : ''));
    					   exit;
    				}
				}
				if ($request_type == "JsHttpRequest") {
					list($block_content, $next_block_id) = $core->get_block_content_with_menu(intval(substr($request_block_id, 10)));
					$_RESULT = array(
									"res"		=> $res,
									"id"		=> $request_id,
									"content"	=> $block_content,
									"next_block"=> $next_block_id,
									"replace_content"	=> true,
								);
				} else {
					header('Location: '.$baseurl.'&action=shownews&category='.$request_category.'&start='.$request_start);
				}
				exit;
				break;

//
			case "del":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start, $request_block_id;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("d", "id", $this->table_prefix."_news", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$res = $this->delete_news($request_id, $request_category);
				if ($res) {
					@$cache->delete_cache_files();
					$lc->delete_internal_links($this->module_name, $this->table_prefix."_news", $request_id);

					if ($CONFIG["news_form_rss"] == "on") {
						$this->form_rss();
					}

				}
				if ($request_type == "JsHttpRequest" && $GLOBALS['JsHttpRequest']) {
					list($block_content, $next_block_id) = $core->get_block_content_with_menu(intval(substr($request_block_id, 10)));
					$_RESULT = array(
									"res"		=> $res,
									"content"	=> $block_content,
									"cur_block"	=> $request_block_id,
									"next_block"=> $next_block_id,
								);

				} else {
					header("Location: $baseurl&action=shownews&category=$request_category&start=$request_start");
				}
				exit;
				break;

		// /
			case "suspend":
			case "activate":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $_RESULT, $db;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("p", "id", $this->table_prefix."_news", $request_id, false, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$res = ($action == "suspend")
						? $main->suspend($this->table_prefix.'_news', $request_id)
						: $main->activate($this->table_prefix.'_news', $request_id);
				if ($res) {
					@$cache->delete_cache_files();
					if ($CONFIG["news_form_rss"] == "on") {
						$this->form_rss();
					}
				}

				if ($request_type == "JsHttpRequest") {
					$_RESULT = array("res"	=> $res);
				} else {
					header('Location: '.$baseurl.'&action=shownews&category='.$request_category.'&start='.$request_start);
				}
				exit;
				break;

//
			case "delete_checked":
				if (!$permissions["d"]) {
				    $main->message_access_denied($this->module_name, $action);
				}
				global $request_check, $request_category, $request_start;
				
				if (is_array($request_check)) {
					list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("d", "id", $this->table_prefix."_news", $v, false, $ctg_owner_id)) {
							if ($this->delete_news($v, $request_category)) {
								@$cache->delete_cache_files();
								$lc->delete_internal_links($this->module_name, $this->table_prefix."_news", $v);
							}
						}
					}
					if ($CONFIG["news_form_rss"] == "on") {
						$this->form_rss();
					}
				}
				header("Location: $baseurl&action=shownews&category=$request_category&start=$request_start");
				exit;
				break;

//
			case "suspend_checked":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_category, $request_start;
				if(is_array($request_check)) {
					list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $this->table_prefix."_news", $v, false, $ctg_owner_id)) {
							if ($main->suspend($this->table_prefix.'_news', $v)) {
								@$cache->delete_cache_files();
							}
						}
					}
					if ($CONFIG["news_form_rss"] == "on") {
						$this->form_rss();
					}
				}
				header("Location: $baseurl&action=shownews&category=$request_category&start=$request_start");
				exit;
				break;

//
			case "activate_checked":
				if (!$permissions["p"]) $main->message_access_denied($this->module_name, $action);
				global $request_check, $request_category, $request_start;
				if(is_array($request_check)) {
					list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
					foreach($request_check as $k => $v) {
						if ($this->test_item_rights("p", "id", $this->table_prefix."_news", $v, false, $ctg_owner_id)) {
							if ($main->activate($this->table_prefix.'_news', $v)) {
								@$cache->delete_cache_files();
							}
						}
					}
					if ($CONFIG["news_form_rss"] == "on") {
						$this->form_rss();
					}
				}
				header("Location: $baseurl&action=shownews&category=$request_category&start=$request_start");
				exit;
				break;


//
			case 'editrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_news", $request_id, true, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($this->table_prefix."_news", "title", $request_id,
											"&id=".$request_id."&category=".$request_category."&start=".$request_start, $this->_msg["The_news"])) {
					header('Location: '.$baseurl.'&action=shownews&category='.$request_category.'&start='.$request_start);
				}
				break;

//
			case 'saverights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_id, $request_category, $request_start, $request_rights;
				list($category_title, $ctg_owner_id) = $this->show_category($request_category, false);
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_news", $request_id, true, $ctg_owner_id))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($this->table_prefix."_news", $request_id, $request_rights);
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
									"res"	=> 1,
									"id"	=> $request_id,
								);
				} else {
					header('Location: '.$baseurl.'&action=shownews&category='.$request_category.'&start='.$request_start);
				}
				exit;
				break;

//
			case 'rss_list':
				if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2("{$this->module_name}_{$action}.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					"baseurl" => $baseurl,
					"action" => $action
				));
				Module::show_select('ctgr_list', $_REQUEST['category'], 'category_title', $this->getCategories());
				
				if ($_REQUEST['category'] && $ctgr = $this->get_category($_REQUEST['category'])) {
				    $tpl->newBlock('block_rss');
				    $tpl->assign(array(
				        'baseurl' => $baseurl,
				    ) + $ctgr);
				    $this->show_rss_channels($ctgr['id']);
				}
				$this->main_title	= $this->_msg["rss_list"];

				break;

//
			case 'showctg':
				if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2($this->module_name.'_categories.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					"MSG_Empty_field" => $this->_msg["Empty_field"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Rights" => $main->_msg["Rights"],
					"MSG_Category" => $this->_msg["Category"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Create_new_category" => $this->_msg["Create_new_category"],
					"MSG_Create" => $this->_msg["Create"],
					'_ROOT.form_action'	=> $baseurl.'&action=addctg',
				));
				$res = $this->show_categories('block_categories');
				if (!$res && isset($_REQUEST['empty'])) {
				    $tpl->newBlock('warning');
				    $tpl->assign('txt', 'Список категорий пуст.');
				}
				$this->main_title	= $this->_msg["News_categories"];

				break;

//
			case "addctg":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_title, $request_block_id;
				if ($this->add_category($request_title, intval(substr($request_block_id, 10)))) {
					@$cache->delete_cache_files();
				}
				header('Location: '.$baseurl.'&action=showctg');
				exit;
				break;


//
			case "editctg":
				//Права доступа.
				if (!$permissions["e"])
				{
					$main->message_access_denied($this->module_name, $action);
				}

				global $request_category;

				if (!$this->test_item_rights("e", "id", $this->table_prefix."_news_categories", $request_category))
				{
					$main->message_access_denied($this->module_name, $action);
				}

				// Отображаем данные о категории.
				$main->include_main_blocks_2($this->module_name."_edit_category.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					"MSG_Empty_field" => $this->_msg["Empty_field"],
					"MSG_Save" => $this->_msg["Save"],
					"MSG_rss_channels" => $this->_msg["rss_list"],
					"MSG_rss_update_interval" => $this->_msg["rss_update_interval"],
					"_ROOT.form_action"	=>	"$baseurl&action=updatectg",
					"_ROOT.cancel_link"	=>	"$baseurl&action=showctg",
					"_ROOT.show_rss_link"	=>	"$baseurl&category=$request_category&action=show_rss"
				));
				$ctgr = $this->show_category($request_category);
				$this->main_title = $this->_msg["Edit_category"];

				$category = intval($request_category);
				$tpl->assign(array(
					'MSG_rss_list_title' => $this->_msg['rss_list'],
					'MSG_Empty_field' => $this->_msg['Empty_field'],
					'MSG_Ed' => $this->_msg['Ed'],
					'MSG_rss_channel' => $this->_msg['rss_channel'],
					'MSG_Del' => $this->_msg['Del'],
					'MSG_rss_import_all' => $this->_msg['rss_import_all'],
					'MSG_create_new_rss' => $this->_msg['create_new_rss'],
					//'MSG_Create' => $this->_msg['Create'],
					'_ROOT.form_action_rss'	=> $baseurl."&category=$category&action=edit_rss",
					'_ROOT.import_rss_link'	=> $baseurl."&category=$category&action=import_rss",
					'category' => $category
				));

				$this->show_rss_channels($category);
				
				if (file_exists(RP."mod/comments")) {
    				require_once(RP."mod/comments/lib/class.CommentsPrototype.php");
    				$comm = new CommentsPrototype('only_create_object');
    				$comm->showCommentableForm($ctgr[2]);
    			}

				break;


//  update
			case "updatectg":
				if (!$permissions["e"]) {
					$main->message_access_denied($this->module_name, $action);
				}

				if (isset($_REQUEST['rss_update_interval'])) {
					$rss_update_interval = intval($_REQUEST['rss_update_interval']);
				} else {
					$rss_update_interval = false;
				}

				$rss_auto_update = $rss_update_interval > 0;

				global $request_category, $request_title;

				if (!$this->test_item_rights("e", "id", $this->table_prefix."_news_categories", $request_category)) {
				    $main->message_access_denied($this->module_name, $action);
				}
				$request_category = (int)$request_category;
				$set = array();
				    
				if ($_REQUEST['title']) {
				    $set['title'] = $_REQUEST['title'];
				}
				if (isset($_REQUEST['rss_update_interval'])) {
				    $set['rss_update_interval'] = $_REQUEST['rss_update_interval'];
				}
				if (isset($_REQUEST['commentable'])) {
				    $set['commentable'] = $_REQUEST['commentable'];
				}
				
				if ( $this->update_category($request_category, $set)) {
					@$cache->delete_cache_files();
				}
				if ($_REQUEST['title']) {
				    header("Location: {$baseurl}&action=showctg");
				} else {
				    header("Location: {$baseurl}&action=rss_list&category={$request_category}");
				}
				exit;
				break;


//
			case "delctg":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_step, $request_category;
				if (!$this->test_item_rights("d", "id", $this->table_prefix."_news_categories", $request_category))
					$main->message_access_denied($this->module_name, $action);

				//  ,
				if (!$request_step || $request_step == 1) {
					$main->include_main_blocks_2($this->module_name."_del_category.html", $this->tpl_path);
					$tpl->prepare();
					$tpl->assign(array(
						"MSG_Deleting_category" => $this->_msg["Deleting_category"],
						"MSG_deletes_all_its_news" => $this->_msg["deletes_all_its_news"],
						"MSG_Confirm_delete_this_category" => $this->_msg["Confirm_delete_this_category"],
						"MSG_Delete" => $this->_msg["Delete"],
						"MSG_Cancel" => $this->_msg["Cancel"],
						"_ROOT.form_action"	=>	"$baseurl&action=delctg&step=2&category=$request_category",
						"_ROOT.cancel_link"	=>	"$baseurl&action=showctg",
					));
					$this->show_category($request_category);
					$this->main_title = $this->_msg["Category_delete"];
				//  ,
				} else if ($request_step == 2) {
					if ($this->delete_category($request_category)) {
						@$cache->delete_cache_files();

						if ($CONFIG["news_form_rss"] == "on") {
							$this->form_rss();
						}

					}
					header("Location: $baseurl&action=showctg");
					exit;
				}
				break;

//
			case 'editctgrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_start;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_news_categories", $request_category, true))
					$main->message_access_denied($this->module_name, $action);

				if (!$this->show_rights_form($this->table_prefix."_news_categories", "title", $request_category,
											"&category=".$request_category, $this->_msg["Category"], "savectgrights")) {
					header('Location: '.$baseurl);
				}
				break;

//
			case 'savectgrights':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $request_category, $request_start, $request_rights;
				if (!$this->test_item_rights("e", "id", $this->table_prefix."_news_categories", $request_category, true))
					$main->message_access_denied($this->module_name, $action);

				$this->save_rights_form($this->table_prefix."_news_categories", $request_category, $request_rights);
				if ($request_type == "JsHttpRequest") {
					$_RESULT = array(
						"res"	=> 1,
						"category"	=> $request_category,
					);
				} else {
					header('Location: '.$baseurl);
				}
				exit;
				break;


//
			case 'showthemes':
				if (!$permissions['r']) $main->message_access_denied($this->module_name, $action);
				$main->include_main_blocks_2($this->module_name.'_themes.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Empty_field" => $this->_msg["Empty_field"],
					"MSG_Ed" => $this->_msg["Ed"],
					"MSG_Topic" => $this->_msg["Topic"],
					"MSG_Del" => $this->_msg["Del"],
					"MSG_Create_new_topic" => $this->_msg["Create_new_topic"],
					"MSG_Create" => $this->_msg["Create"],
				));
				$tpl->assign(array(
					'_ROOT.form_action'	=> $baseurl.'&action=addtheme',
				));
				$this->show_themes('block_themes');
				$this->main_title	= $this->_msg["News_topics"];
				break;


//
			case "addtheme":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_title;
				if ($this->add_theme($request_title)) {
					@$cache->delete_cache_files();
				}
				header('Location: '.$baseurl.'&action=showthemes');
				exit;
				break;


//
			case "edittheme":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_title, $request_theme;
				$main->include_main_blocks_2($this->module_name."_edit_theme.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(Array(
					"MSG_Empty_field" => $this->_msg["Empty_field"],
					"MSG_Save" => $this->_msg["Save"],
				));
				$tpl->assign(array(
					"_ROOT.form_action"	=>	"$baseurl&action=updatetheme",
					"_ROOT.cancel_link"	=>	"$baseurl&action=showthemes",
				));
				$this->show_theme($request_theme);
				$this->main_title = $this->_msg["Edit_topic"];
				break;


//  update
			case "updatetheme":
				if (!$permissions["e"]) $main->message_access_denied($this->module_name, $action);
				global $request_theme, $request_title;
				if ($this->update_theme($request_theme, $request_title)) {
					@$cache->delete_cache_files();
				}
				header("Location: $baseurl&action=showthemes");
				exit;
				break;


//
			case "deltheme":
				if (!$permissions["d"]) $main->message_access_denied($this->module_name, $action);
				global $request_theme;
				if ($this->delete_theme($request_theme)) {
					@$cache->delete_cache_files();
				}
				header("Location: $baseurl&action=showthemes");
				exit;
				break;

//
			case 'properties':
				if (!$permissions['e']) $main->message_access_denied($this->module_name, $action);
				global $module_id, $request_field, $request_block_id, $title;

				$main->include_main_blocks('_block_properties.html', 'main');
				$tpl->prepare();

				if ($main->show_actions($request_field, '', $this->block_main_module_actions, $this->block_module_actions))
				{
					$main->show_linked_pages($module_id, $request_field);
				}

				$tpl->assign(array( '_ROOT.title'		=> $title,
									'_ROOT.username'	=> $_SESSION['session_login'],
									'_ROOT.password'	=> $_SESSION['session_password'],
									'_ROOT.name'		=> $this->module_name,
									'_ROOT.lang'		=> $lang,
									'_ROOT.block_id'	=> $request_block_id,
								));
				$this->main_title	= $this->_msg["Block_properties"];
				break;

//
			case 'isupload':
				global $request_img_name;
			    header('Content-Type: text/xml');
			    echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
			    echo '<data>';
				if (!$request_img_name) {
					echo "0";
				} else {
				    $fl_name = RP.$CONFIG['news_img_path'].$request_img_name;
				    echo file_exists($fl_name) ? "1" : "0";
				}
				echo '</data>';
				exit;

				break;

			case 'show_rss': // Отобразить список RSS каналов.
				global $request_category;

				$category = intval($request_category);
				$main->include_main_blocks_2($this->module_name.'_rss_list.html', $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					'MSG_Empty_field' => $this->_msg['Empty_field'],
					'MSG_Ed' => $this->_msg['Ed'],
					'MSG_rss_channel' => $this->_msg['rss_channel'],
					'MSG_Del' => $this->_msg['Del'],
					'MSG_rss_import_all' => $this->_msg['rss_import_all'],
					'MSG_create_new_rss' => $this->_msg['create_new_rss'],
					'MSG_Create' => $this->_msg['Create'],
					'_ROOT.form_action_rss'	=> $baseurl."&category=$category&action=edit_rss",
					'_ROOT.import_rss_link'	=> $baseurl."&category=$category&action=import_rss",
					'category' => $category
				));

				$this->show_rss_channels($category);
				$this->main_title = $this->_msg['rss_list'];

				break;

			case 'edit_rss': // Добавление нового/редктирование RSS-канала.
				global $request_category;

				if (isset($_REQUEST['rss_id']))
				{
					$rss_id = intval($_REQUEST['rss_id']);
				} else {
					$rss_id = 0;
				}

				$category = intval($request_category);
				$main->include_main_blocks_2($this->module_name."_edit_rss.html", $this->tpl_path);
				$tpl->prepare();
				$tpl->assign(array(
					"MSG_Empty_field" => $this->_msg["Empty_field"],
					"MSG_Save" => $this->_msg["Save"],
					"_ROOT.form_action"	=> "$baseurl&category=$category&action=update_rss",
					"_ROOT.cancel_link"	=> "$baseurl&category=$category&action=editctg",
					"_ROOT.baseurl"	=> $baseurl,
					"_ROOT.category"	=> $category,
				));
				$this->show_rss_channel($rss_id, $category);

				// Если идентификатор RSS равен 0, то добавление, иначе - обновление.
				$page_title = $rss_id ? $this->_msg["edit_rss"] : $this->_msg["add_rss"];
				$this->main_title = $page_title;
				break;

			case 'update_rss': //Добавление/редактирование RSS-канала.
				global $request_category;

				$category = intval($request_category);
				if (isset($_REQUEST['rss_id'])) {
					$rss_id = intval($_REQUEST['rss_id']);
				} else {
					$rss_id = 0;
				}

				if (isset($_REQUEST['rss_name'])) {
					$rss_name = strval($_REQUEST['rss_name']);
				} else {
					$rss_name = '';
				}

				if (isset($_REQUEST['rss_url'])) {
					$rss_url = strval($_REQUEST['rss_url']);
				} else {
					$rss_url = '';
				}


				$result = $this->add_update_rss_channel($rss_id, $category, $rss_name, $rss_url);
				if ($result) {
					@$cache->delete_cache_files();
				}

				header("Location: {$baseurl}&category={$category}&action=rss_list");
				exit;
				break;

			case 'delete_rss': //Удаление RSS-канала.
				global $request_category;

				$category = intval($request_category);
				if (isset($_REQUEST['rss_id']))
				{
					$rss_id = intval($_REQUEST['rss_id']);
				} else {
					$rss_id = 0;
				}

				if ($rss_id)
				{
					$this->delete_rss_channel($rss_id);
				}
				header("Location: {$baseurl}&category={$category}&action=rss_list");
				exit;
				break;

			case 'import_rss': // Импорт всего списка RSS для данной категории.
				global $request_category;

				// Права доступа.
				if (!$permissions["e"])
				{
					$main->message_access_denied($this->module_name, $action);
				}

				// Подключаем модуль парсинга RSS-новостей.
				require_once(RP.'inc/simplepie.inc');
				$category = intval($request_category);
				$this->import_rss($category);

				header("Location: $baseurl&category=$category&action=rss_list");
				die();
				break;

// Default...
			default:
				$main->include_main_blocks();
				$tpl->prepare();
			}

			if (isset($_SESSION['adm_news_err'])) {
			    if ($tpl) {
			    	$tpl->gotoBlock('_ROOT');
			    	$tpl->newBlock('block_adm_news_error');
			    	$tpl->assign('err', $this->_msg[$_SESSION['adm_news_err']]);
			    }
				unset($_SESSION['adm_news_err']);
			}
//---------------------------       --------------------------//
		}
	}


	//
/*	function get_categories() {
		global $CONFIG, $server, $lang;
		$NC = new CNewsCategories_DB($this->table_prefix.'_news_categories', 'id');
		$arr = array();
		$temp = $NC->ListAll();
		if (count($temp) > 0) {
			foreach($temp as $key=>$cat) {
				$arr[$cat['id']] = $cat['title'];
			}
		}
		return $arr;
	}*/
    function getNewsDate($date){
	    $darr = explode('.', $date);
		$i = 1;
		$arr[$i++] = 'января';
		$arr[$i++] = 'февраля';
		$arr[$i++] = 'марта';
		$arr[$i++] = 'апреля';
		$arr[$i++] = 'мая';
		$arr[$i++] = 'июня';
		$arr[$i++] = 'июля';
		$arr[$i++] = 'августа';
		$arr[$i++] = 'сентября';
		$arr[$i++] = 'октября';
		$arr[$i++] = 'ноября';
		$arr[$i++] = 'декабря';
		$darr[1] = $arr[(int)$darr[1]];
		return implode(' ', $darr);
	}

	//
	function show_news_list($blockname, $transurl, $filter = array(), $adminurl = '')
	{   global $CONFIG, $tpl, $baseurl,$server,$lang, $db, $permissions, $PAGE;

		if ('' == $order_by) {
	        $order_by = $CONFIG['news_order_by'];
	    }
	    $start = $filter['start'] > 1 ? (int)$filter['start'] : 1;
		$rows = $filter['limit'] > 0 ? (int)$filter['limit'] : $CONFIG['news_max_rows'];
		$category = (int)$filter['ctgr'];
		$clause = array(
			'n.date <= NOW()'
		);
		if (! ($CONFIG['edit_on_site'] && $_SESSION["session_is_admin"] && $_COOKIE['edit_content'] )) {
			$clause[] = 'n.active = 1';
		}
		if ($filter['tag']) {
			$clause[] = "n.tags LIKE '%,".My_Sql::escape($filter['tag']).",%'";
		}
		if ($filter['date']) {
		    $clause[] = "DATE_FORMAT(n.date,'%Y%m%d')='".$filter['date']."'";
		}
		if ($filter['nid']) {
		    $clause[] = "n.id!=".(int)$filter['nid'];
		}
		
		$news_ids = array();
		
		if($_GET['ids']){
			$arr = explode('-', $_GET['ids']);
			$arr = explode('.', $arr[1]);
			foreach($arr as $k => $v)
			  if((int)$v) $news_ids[] = (int)$v;
		}
		
		if(sizeof($news_ids)){
		    $clause[] = "n.id IN (".implode(',', $news_ids).")";
		    $this->main_title = $ctgr['title'];
		    $total_cnt = $this->get_list_with_rights(
		        "n.id, n.title, n.body, t.title as theme_title,n.source, n.announce, tags, "
		            ."n.active, n.rss_link, n.announce_with_links, NOW() as d, "
		            ."DATE_FORMAT(date,'".$CONFIG["news_date_format"]."') as news_date, n.img",
				$this->table_prefix."_news_ctgr NC INNER JOIN {$this->table_prefix}_news n ON NC.news_id=n.id"
				    ." LEFT JOIN ".$this->table_prefix."_news_themes t ON n.theme_id = t.id",
				"n",
				implode(' AND ', $clause),
				"", 'date DESC', $start, $rows, true);
		}
		elseif ($category) {
		    $ctgr = $this->get_category($category);
		    if (! $ctgr) {
		        return;
		    }
			//var_dump($PAGE['id']);
		    if($PAGE['id'] == 278) $clause[] = "NC.ctgr_id IN (9,13)";//.$ctgr['id'];
			else $clause[] = "NC.ctgr_id=".$ctgr['id'];
		    $this->main_title = $ctgr['title'];
		    $total_cnt = $this->get_list_with_rights(
		        "n.id, n.title, n.body, t.title as theme_title,n.source, n.category_id,n.announce, tags, "
		            ."n.active, n.rss_link, n.announce_with_links, NOW() as d, "
		            ."DATE_FORMAT(date,'".$CONFIG["news_date_format"]."') as news_date, n.img",
				$this->table_prefix."_news_ctgr NC INNER JOIN {$this->table_prefix}_news n ON NC.news_id=n.id"
				    ." LEFT JOIN ".$this->table_prefix."_news_themes t ON n.theme_id = t.id",
				"n",
				implode(' AND ', $clause),
				"", 'date DESC', $start, $rows, true);
		} else {
		    $total_cnt = $this->get_list_with_rights(
		        "n.id, n.title, n.body, t.title as theme_title,n.source, n.announce, tags, "
		            ."n.active, n.rss_link, n.announce_with_links, NOW() as d, "
		            ."DATE_FORMAT(date,'".$CONFIG["news_date_format"]."') as news_date, n.img",
				"{$this->table_prefix}_news n LEFT JOIN {$this->table_prefix}_news_themes t ON n.theme_id = t.id",
				"n",
				implode(' AND ', $clause),
				"n.id", 'date DESC', $start, $rows, true);
		}
		//echo $db->sql;

		$pages_cnt = ceil($total_cnt/$rows);
		if ($CONFIG['edit_on_site'] && $_SESSION["session_is_admin"]  && $_COOKIE['edit_content'] && $permissions['e']) {
			//
			$tpl->newBlock('block_admin_js_set');
			$tpl->newBlock('block_create_news_menu');
			$tpl->assign("adminurl", $adminurl);
			$need_js_set = false;
		} else {
			$need_js_set = true;
		}
		if ($db->nf()) {
			$tpl->assign("news_archive_link", $transurl."&action=archive&start=".$start);
			if ($CONFIG["news_form_rss"] == "on") {
				$tpl->newBlock("block_news_rss");
				$tpl->assign("news_rss_link","/rss/".$this->table_prefix.".xml");
			}

			$categories = array();
			$ids = array();
			$path = substr(RP, 0, -1);
			while ($db->next_record()) {
			     //echo $db->f("title");
			    $ids[] = $db->f("id");
				$tpl->newBlock($blockname);
                $news_link = $this->get_news_link($transurl, $db->f("id"), $db->f('category_id'));
                $rss_link = trim($db->f('rss_link'));
      			if (empty($rss_link)) {
      				$rss_link = $news_link;
      			}
				$tpl->assign(array(
					'news_title'			=>	$db->f("title"),
					'news_body'			=>	$db->f("body"),
					'news_source'			=>	$db->f("source"),
					'news_date'			=>	$this->getNewsDate($db->f("news_date")),
					'news_announce'		=>	nl2br($db->f("announce")),
					'news_announce_with_links'	=>	nl2br(str_replace("{news_link}", $news_link, $db->f("announce_with_links"))),
					'news_link'			=>	$news_link,
					'news_id'				=>	$db->f("id"),
					'news_source_rss_link' => $rss_link
				));
				
				if($db->f("source") && $filter['prod']){
					$pcode = mysql_real_escape_string($db->f("source"));
					$query = 'SELECT p.product_title,p.id,p.alias, c.id as cid, c.alias as calias 
							  FROM '.$server.$lang.'_catalog_products as p
							  INNER JOIN '.$server.$lang."_catalog_categories as c ON (p.category_id=c.id)
							  WHERE p.product_code='$pcode' AND p.active=1 AND c.active=1";
					if(!$result = mysql_query($query)) die(mysql_error());
                    if($row = mysql_fetch_assoc($result)){
						$tpl->newBlock('prod');
						$link = '/catalog/'.($row['calias'] ? $rowp['calias'] : $row['cid']).'/'.($row['alias'] ? $rowp['alias'] : $row['id']).'.html';
						$tpl->assign('link', $link);
						$tpl->assign('title', $row['product_title']);
					}		
				}
				
				if ($db->f("theme_title")) {
					$tpl->newBlock('block_news_theme');
					$tpl->assign(array(
						news_theme		=>	$db->f("theme_title"),
					));
				}
				if ($db->f("img") && is_file($path.$db->f("img"))) {
					$img_prp = getimagesize($db->f("img"));				
					$tpl->newBlock('block_news_img'.($filter['nid'] ? '1' : ''));
					$tpl->assign(array(
						'img_path'			=>	$db->f("img"),
						'img_width_height'	=>	$img_prp[3],
						'news_link'  =>  $news_link
					));
				}
				$tags = trim($db->Record['tags'], ',');
				if ($CONFIG['news_show_tags'] && $tags) {
					
	    			$tags = preg_split("/,|\s/", $tags, -1, PREG_SPLIT_NO_EMPTY);
	    			foreach ($tags as $key => $val) {
	    				$tags[$key] = array('name' => $val);
	    			}
	    			$tpl->assignList($pref.'tag_', $tags, array(
	    			    'baseurl' => $transurl,
	    			    'transurl' => $transurl,
	    				'divider' => ','
	    			));
				}

				if ($CONFIG['edit_on_site'] && $_SESSION["session_is_admin"] && $_COOKIE['edit_content']) {
					//
					if ($need_js_set) {
						$tpl->newBlock('block_admin_js_set');
						$need_js_set = false;
					}
					$tmp_arr = array(
						"news_id"		=> $db->f("id"),
						"adminurl"		=> $adminurl,
					);
					$rights = intval($db->f("user_rights"));
					$tpl->newBlock('block_edit_news_menu_start');
					$tpl->assign($tmp_arr);
					if ($permissions['e'] && ($rights & 2)) {
						$tpl->newBlock('block_news_edit');
						$tpl->assign($tmp_arr);
					}
					if ($permissions['p'] && ($rights & 4)) {
						$tpl->newBlock('block_news_activate');
						$tpl->assign($tmp_arr); #var_dump($db);
						$tpl->assign(array(
							"activate_display"	=> $db->f("active") ? 'none' : 'inline',
							"suspend_display"	=> $db->f("active") ? 'inline' : 'none',
						));
					}
					if ($db->f("is_owner")) {
						$tpl->newBlock('block_news_rights');
						$tpl->assign($tmp_arr);
					}
					if ($permissions['d'] && ($rights & 8)) {
						$tpl->newBlock('block_news_delete');
						$tpl->assign($tmp_arr);
					}
					//if (empty($art_data)) $tpl->newBlock('block_empty_content');
					$tpl->newBlock('block_edit_news_menu_end');
				}
			}
			
			// Обновляем категории, для которых активно RSS автообновление.
			$db->query("SELECT C.id FROM {$this->table_prefix}_news_ctgr NC INNER JOIN "
			    ."{$this->table_prefix}_news_categories C ON C.id=NC.ctgr_id WHERE news_id IN (".implode(",", $ids)
			    .") AND rss_auto_update AND (DATE_ADD(`rss_last_update`, INTERVAL `rss_update_interval` MINUTE) < NOW() OR `rss_last_update` IS NULL)");			
			if ($db->nf()) {
				require_once(RP.'inc/simplepie.inc');
				while ($db->next_record()) {
					$this->import_rss($db->Record['id'], false);
				}
			}

			return $pages_cnt;
		}
		return FALSE;
	}


	//      id
	function show_news($transurl, $id = '', $category = 0, $adminurl = '')
	{   global $CONFIG, $main, $tpl, $db, $baseurl, $PAGE, $permissions, $request_start;
	
		$id = (int)$id;
		$category = (int)$category;
		if (!$id) return FALSE;
		$news = $this->getNews($id, $category);
		if ($news) {
			$tpl->newBlock('block_news_detail');
			$ctgr_id = $news['category_id'];
			$body = $news["body"];
	   		if ($news["split_body"] == 1) {
	   		    $start_patt = "~<".preg_quote($CONFIG["news_tag_separator"], '~')."[^>]*>~si";
	   		    $body_pages = preg_split($start_patt, $body);
				if (sizeof($body_pages) > 1) {
					if (empty($body_pages[0])) {
						unset($body_pages[0]);
						$start_idx = 1;
					} else {
						$start_idx = 0;
					}
					$start = ((int)$request_start) ? (int)$request_start : 1;
					$pages = sizeof($body_pages);
					$start = ($start > $pages) ? $pages : $start;
					$body = (($start == 1 && $start_idx == 0) ? "" : "<{$CONFIG["news_tag_separator"]}>")
					    .$body_pages[$start-1+$start_idx];
				}
			}
			$news_forum_link = $baseurl.(($news["forum_theme"])
										? "&action=addreply&theme=".$news["forum_theme"]
										: "&action=addtheme&news=".$news["id"]);
			$tpl->assign(array(
				'news_title'		=>	$news["title"],
				'news_source'		=>	$news["source"],
				'news_announce'	=>	nl2br($news["announce"]),
				'news_date'		=>	$this->getNewsDate($news["date"]),
				'news_link'		=>	$transurl."&action=show&id=".$news["id"],
				'news_id'		=>	$news["id"],
				'news_body'		=>	$body,
				'news_address'	=>	$main->permanent_address(),
				'news_forum_link'	=>	$news_forum_link,
				'news_archive'	=>	$baseurl.'&action=archive',
				'img_title'	=>	substr($news['img'],0,-11)."_middle.jpg",
				'img_title_big'	=>	substr($news['img'],0,-11),
			));
			if ($news["theme_title"]) {
				$tpl->newBlock('block_news_theme');
				$tpl->assign(array(
					'news_theme' =>	$news["theme_title"],
				));
			}
			if ($news["split_body"] == 1 && $pages > 1) {
				$nav_string = $main->_show_nav_block($pages, "nav", "action=show&id=".$news["id"], $start);
			}
			if ($news["img"] && is_file(substr(RP, 0, -1).$news["img"])) {
				$img_prp = getimagesize(RP.$CONFIG['news_img_path'].$news["img"]);
				$tpl->newBlock('block_news_img');
				$tpl->assign(array(
					'img_path'			=>	$news["img"],
					'img_width_height'	=>	$img_prp[3]
				));
			}
			$tags = trim($news['tags'], ',');
			if ($CONFIG['news_show_tags'] && $tags) {
		    	$tags = preg_split("/,|\s/", $tags, -1, PREG_SPLIT_NO_EMPTY);
		    	foreach ($tags as $key => $val) {
		    		$tags[$key] = array('name' => $val);
		    	}
		    	$tpl->assignList($pref.'tag_', $tags, array(
		    		'baseurl' => $transurl,
		    		'transurl' => $transurl,
		    		'divider' => ','
		    	));
			}
			$PAGE['alt_title'] = $news['seo_title'] ? $news['seo_title'] : $news["title"];
			$this->main_title = $news["title"];
			$PAGE['description'] = ($news["description"]) ? $news["description"] : strip_tags($news["announce"]);
			if ($news["keywords"]) {
				$PAGE['keywords'] = $news["keywords"];
			}

			if ($CONFIG['edit_on_site'] && $_SESSION["session_is_admin"] && $_COOKIE['edit_content']) {
				//
				$tmp_arr = array(
					"news_id"		=> $news["id"],
					"adminurl"		=> $adminurl,
				);
				$rights = intval($news["user_rights"]);
				$tpl->newBlock('block_edit_news_menu_start');
				$tpl->assign($tmp_arr);
				if ($permissions['e']) {
					$tpl->newBlock('block_news_add');
					$tpl->assign("adminurl", $adminurl);
				}
				if ($permissions['e'] && ($rights & 2)) {
					$tpl->newBlock('block_news_edit');
					$tpl->assign($tmp_arr);
				}
				if ($permissions['p'] && ($rights & 4)) {
					$tpl->newBlock('block_news_activate');
					$tpl->assign($tmp_arr);
					$tpl->assign(array(
						"activate_display"	=> $news["active"] ? 'none' : 'inline',
						"suspend_display"	=> $news["active"] ? 'inline' : 'none',
					));
				}
				if ($news["is_owner"]) {
					$tpl->newBlock('block_news_rights');
					$tpl->assign($tmp_arr);
				}
				if ($permissions['d'] && ($rights & 8)) {
					$tpl->newBlock('block_news_delete');
					$tpl->assign($tmp_arr);
				}
				//if (empty($art_data)) $tpl->newBlock('block_empty_content');
				$tpl->newBlock('block_edit_news_menu_end');
			}
			$original_date = $news['original_date'];
			$ctgr = $category ? $this->get_category($category) : false;
			$this->show_nav_path($id, $category, $transurl, $ctgr ? $ctgr['title'] : '');

			if ($category > 0) {
			    $db->query("SELECT N.id FROM {$this->table_prefix}_news N INNER JOIN "
			        ."{$this->table_prefix}_news_ctgr NC ON N.id=NC.news_id WHERE "
			        ."active=1 AND ctgr_id={$category} ORDER BY date DESC");
			} else {
			    $db->query("SELECT id FROM {$this->table_prefix}_news WHERE "
			        ."active=1 ORDER BY date DESC");
			}
    		$prev_news_id = $next_news_id = 0;
    		while ($db->next_record() && $id != $db->Record['id']) {
    			$prev_news_id = $db->Record['id'];
    		}
    		if ($db->next_record()) {
    		    $next_news_id = $db->Record['id'];
    		}
            if ($prev_news_id) {
    			$tpl->newBlock('block_prev_news_link');
    			$tpl->assign(array(
    			    'prev_news_link' => $this->get_news_link($transurl, $prev_news_id, $category),
    			));
            }
			if ($next_news_id) {
    			$tpl->newBlock('block_next_news_link');
    			$tpl->assign(array(
    			    'next_news_link' => $this->get_news_link($transurl, $next_news_id, $category)
    			));
			}
			return $ctgr_id;
		}
		return FALSE;
	}



	//   ,
	function show_nav_path($id, $category, $transurl, $category_title = false) {
	    if ($category_title) {
	        $GLOBALS['PAGE']['path_title'] = $category_title;
	    }
		$this->addition_to_path = $this->get_nav_path($id, $category, $transurl);
		return 1;
	}


	//   ,
	function get_nav_path($id, $category, $transurl) {
		global $CONFIG;
		$id			= (int)$id;
		$path = '';
		$ND = new CNewsData_DB($this->table_prefix."_news", "id");
		if ($id) {
			$ND->load($id);
			if ($ND->id) {
				$path = $ND->title;
			}
		}
		return $path ? array($path => $this->get_news_link($transurl, $id, $category)) : false;
	}

	//   RSS
	function form_rss() {
		global $CONFIG, $db, $tpl, $baseurl, $server, $lang;

		$link = Core::formPageLink($CONFIG['news_page_link'],$CONFIG['news_page_address'], $lang);
        $link = "/news/"; //ставило /about/news/
		if (!file_exists(RP."rss") || !is_dir(RP."rss")) {
			mkdir(RP."rss");
		}
		$rss_filename = RP."rss/".$this->table_prefix.".xml";
		if ($f = fopen($rss_filename, "w")) {

			fwrite($f, '<?xml version="1.0" encoding="utf-8"?>
');
			fwrite($f, '<!DOCTYPE rss [<!ENTITY % HTMLlat1 PUBLIC "-//W3C//ENTITIES Latin 1 for XHTML//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml-lat1.ent">]>
');
			fwrite($f, '<rss version="2.0" xml:base="'.$CONFIG["web_address"].'">
');
			fwrite($f, '<channel>
');
			fwrite($f, '<title>' . $this->_msg["News_of_site"] . ' '.$CONFIG["sitename"].'</title>
');
			fwrite($f, '<link>'.$CONFIG["web_address"].'</link>
');
			fwrite($f, '<description />
');
			fwrite($f, '<language>'.$lang.'</language>
');

		  $limit = $CONFIG["news_count_rss"] ? $CONFIG["news_count_rss"] : 10;
			$category	= (int)$category;
			$ctg_str	= ($category) ? 'category_id = '.$category.' AND' : '';
			$this->get_list_with_rights(
						"n.id, n.title, t.title as theme_title, n.announce, n.announce_with_links, NOW() as d, date, DATE_FORMAT(date,'".$CONFIG["news_date_format"]."') as news_date, n.img",
						$this->table_prefix."_news n LEFT JOIN ".$this->table_prefix."_news_themes t ON n.theme_id = t.id", "n",
						$ctg_str." n.date <= NOW() AND n.active = 1", "", 'date DESC', 1, $limit);
			if ($db->nf() > 0) {
				while($db->next_record()) {
					$date = explode(" ", $db->f("date"));
					$time = explode(":", $date[1]);
					$date = explode("-", $date[0]);
					//
					$news_link = htmlspecialchars(NewsPrototype::get_news_link("http://".$_SERVER["HTTP_HOST"].$link, $db->f("id"), 0));
					fwrite($f, '<item>
');
					fwrite($f, '	<title>'.htmlspecialchars($db->f("title")).'</title>
');
					fwrite($f, '	<link>'.$news_link.'</link>
');					
					fwrite($f, '	<description>'.nl2br(preg_replace("/&(?!amp;)/", "&amp;", $db->f("announce"))).'</description>
');
					fwrite($f, '	<pubDate>'.date("r", mktime($time[0], $time[1], 0, $date[1], $date[2], $date[0])).'</pubDate>
');
					fwrite($f, '</item>
');
				}
			}
			fwrite($f, '</channel>
');
			fwrite($f, '</rss>
');
			fclose($f);
			return TRUE;
		}
		return FALSE;
	}

	function getPropertyFields() {
		GLOBAL $request_site_target, $request_sub_action;
		if ('' != $request_sub_action) {

			$tpl = $this->getTmpProperties();

			switch ($request_sub_action) {
				case 'archive':
				case 'tlist':
				case 'alist':
				case 'alist_archive':
				case 'rss_list':
				case 'tlist_archive':
				case 'calendar':
					$tpl->newBlock('block_properties');
					$tpl->newBlock('block_news_list');
					$tpl->assign(Array(
						"MSG_News_category" => $this->_msg["News_category"],
						"MSG_All_categories" => $this->_msg["All_categories"],
						"MSG_News_per_page" => $this->_msg["News_per_page"],
					));
					$arr	= $this->getCategories();
					abo_str_array_crop($arr);
					$tpl->assign_array('block_categories', $arr);
					$tpl->newBlock('block_page_edit');
					$tpl->assign(Array(
						"MSG_Goto_edit" => $this->_msg["Goto_edit"],
						"site_target" => $request_site_target,
					));
					if($request_sub_action!='calendar') {
					    $tpl->newBlock('block_per_page');
					    $tpl->assign(Array(
					        "MSG_News_per_page" => $this->_msg["News_per_page"],
					    ));
					}
					break;
					
				case 'categories':
				    $tpl->newBlock('block_properties');
				    
				    $tpl->assignList('ctgr_', $this->getCategories());
				    
					$tpl->newBlock('block_news_cnt');
					$tpl->assign(Array(
						"MSG_Show_news_cnt" => $this->_msg["Show_news_cnt"],
					    "MSG_No" => $this->_msg["No"],
					    "MSG_Yes" => $this->_msg["Yes"],
					));
					
					break;

				default:
					$tpl->newBlock('block_properties_none');
					$tpl->assign(Array(
						"MSG_No" => $this->_msg["No"],
					));
					break;
			}
			return $this->getOutputContent($tpl->getOutputContent());
		}
		return FALSE;
	}

	function getEditLink($sub_action = NULL, $block_id = NULL) {
		GLOBAL $request_name, $db, $lang, $server;
		$block_id	= (int)$block_id;
		if ($sub_action && $block_id) {
			$db->query('SELECT	property1,
								property2,
								property3,
								property4,
								property5
							FROM '.$this->table_prefix.'_pages_blocks
							WHERE id = '.$block_id);
			if ($db->nf() > 0) {
				$db->next_record();
				$arr[]	= $db->f('property1');
				$arr[]	= $db->f('property2');
				$arr[]	= $db->f('property3');
				$arr[]	= $db->f('property4');
				$arr[]	= $db->f('property5');

				switch ($sub_action) {
					case 'archive':
						$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=shownews&menu=false&category=';
						$link['material_id']	= $arr[0];
						break;

					case 'tlist':
					case 'tlist_archive':
						$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=shownews&menu=false&category=';
						$link['material_id']	= $arr[0];
						break;

					case 'alist':
					case 'alist_archive':
						$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=shownews&menu=false&category=';
						$link['material_id']	= $arr[0];
						break;

					case 'rss_list':
						$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=shownews&menu=false&category=';
						$link['material_id']	= $arr[0];
						break;
				}
				return $link;
			}

			switch ($sub_action) {
				case 'archive':
					$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=shownews&menu=false&category=';
					$link['material_id']	= '';
					break;

				case 'tlist':
				case 'tlist_archive':
					$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=shownews&menu=false&category=';
					$link['material_id']	= '';
					break;

				case 'alist':
				case 'alist_archive':
					$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=shownews&menu=false&category=';
					$link['material_id']	= '';
					break;

				case 'rss_list':
					$link['material_url']	= '/admin.php?lang='.$lang.'&name='.$request_name.'&action=shownews&menu=false&category=';
					$link['material_id']	= '';
					break;
			}
			return $link;
		}
		return FALSE;
	}

//
	function add_news($category, $theme, $title, $announce, $body, $split_body,
	   $date, $hrs, $min, $seo_title, $description, $keywords, $img, $tags, $item_rights,
	   $block_id = 0, $rss_id = '', $active = 0, $rss_link = '', $source = '')
	{
		global $main, $server, $lang, $db, $CONFIG;
		$active = intval($active);

	    $ctrg_list = explode(",", $category);
	    $category = intval($ctrg_list[0]);
		if (!$category) {
			return false;
		}
        if($img)
        {
                $this->resizeimg(RP.$img,RP.$img."_1small.jpg",80,80);
                $this->resizeimg(RP.$img,RP.$img."_middle.jpg",600,0);
                $img=$img."_1small.jpg";
        }
		$db->query("SELECT usr_group_id FROM ".$this->table_prefix."_news_categories WHERE id=".$category);
		if (!$db->next_record())
		{
			return false;
		}
		$owner_id = $_SESSION["siteuser"]["id"];
		list($group_id, $rights) = $this->get_group_and_rights('news_default_rights', $block_id, $db->f("usr_group_id"));
		$rights = $item_rights ? $item_rights : $rights;
		
		$base_tags = $this->getTagsBaseForm($tags);
    	$tags = '';
    	if (! empty($base_tags)) {
    		$tags = ','.implode(',', $base_tags).',';
    	}
    			
		$ND = new CNewsData_DB($this->table_prefix."_news", "id");
		$hrs				= (int)$hrs;
		$min				= (int)$min;
		$date				= $db->escape($date);
		$ND->theme_id		= intval($theme);
		$ND->title			= $db->escape(strip_tags($title));
		$ND->title_add			= $db->escape(strip_tags($_POST['title_add']));
		$ND->source			= $db->escape($source);
		$ND->announce_with_links	= $db->escape($announce);
		$ND->announce		= $db->escape(strip_tags($announce));
		$ND->body			= $db->escape($body);
		$ND->split_body		= (int)$split_body;
		$ND->date			= $main->transform_date($date, $hrs, $min);
		$ND->category_id	= $category;
		$ND->img			= $img && file_exists(RP.$img) ? $img : '';
		$ND->seo_title	= $db->escape($seo_title);
		$ND->description	= $db->escape($description);
		$ND->keywords		= $db->escape($keywords);
		$ND->rss_id			= $db->escape($rss_id);
		$ND->rss_link		= $db->escape($rss_link);
		$ND->active			= $active;
      	$ND->owner_id		= $owner_id;
      	$ND->usr_group_id	= $group_id;
      	$ND->rights			= $rights;
      	$ND->tags 			= $tags;
		
		$ret = false;
		if ($ND->store()) {
		    $ret = db_insert_id();
		    $this->setNewsCtrgs($ret, $ctrg_list);
		    $this->addTags($ret, $base_tags);
		}

		return $ret;
	}

//
	function update_news($category, $theme, $title, $announce, $body, $split_body,
	   $date, $hrs, $min, $seo_title, $description, $keywords, $img, $tags, $id, $source = '')
	{
		global $main, $server, $lang, $CONFIG, $db;
		$ND = new CNewsData_DB($this->table_prefix."_news", "id");
		if (!isset($id))		return false;
		if (!isset($category))	return false;
    	$id = intval($id);
    	$ctrg_list = explode(",", $category);
	    $category = intval($ctrg_list[0]);
	    $category = intval($category);
	    if (!$category) {
			return false;
		}

	    if(!$ND->load_news($id)) {
	    	return FALSE;
	    }
	    
		$base_tags = $this->getTagsBaseForm($tags);
		$tags = '';
		if (! empty($base_tags)) {
			$tags = ','.implode(',', $base_tags).',';
		}
        if($img)
        {

            if(!file_exists(RP.$img."_middle.jpg") && !stripos($img,'_1small.jpg'))
            {
                $oldImg=$ND->img;
                $oldImg2=substr($oldImg,0,-11);
                if(stripos($img,'_1small.jpg'))unlink(RP.$oldImg);
                unlink(RP.$oldImg2."_middle.jpg");
                $this->resizeimg(RP.$img,RP.$img."_1small.jpg",80,80);
                $this->resizeimg(RP.$img,RP.$img."_middle.jpg",600,0);
                $img=$img."_1small.jpg";
            }
        }
		$ND->id					= $id;
		$ND->theme_id			= intval($theme);
		$ND->title				= $db->escape(strip_tags($title));
		$ND->title_add			= $db->escape(strip_tags($_POST['title_add']));
		$ND->source				= $db->escape($source);
		$ND->announce_with_links	= $db->escape($announce);
		$ND->announce			= $db->escape(strip_tags($announce));
		$ND->body				= $db->escape($body);
		$ND->split_body			= (int)$split_body;
		$ND->category_id		= $category;
		$ND->date				= $main->transform_date($date, $hrs, $min);
		$ND->img				= $img && file_exists(RP.$img) ? $img : '';
		$ND->seo_title    		= $db->escape($seo_title);
		$ND->description		= $db->escape($description);
		$ND->keywords			= $db->escape($keywords);
		$ND->rss_id				= $db->escape(rss_id);
		$ND->calendar_date	= NULL;
		$ND->hrs			= NULL;
		$ND->min			= NULL;
		$ND->tags			= $tags;
		
		$ret = false;
		if ($ND->store()) {
		    $ret = $id;
		    $this->setNewsCtrgs($ret, $ctrg_list);
		    
			$del_tags_ids = array();
        	$add_tags = $base_tags;
        	$tags = $this->getTags($id);
        	//echo "<hr>add_tags - "; print_r($add_tags); echo "<hr>";
        	//echo "<hr>tags - "; print_r($tags); echo "<hr>";
        	if (! empty($tags)) {
        		foreach ($tags as $tag) {
        			$k = array_search($tag['name'], $add_tags);
            		if (false === $k) {
            			$del_tags_ids[] = $tag['id'];
            		} else {
            			unset($add_tags[$k]);
            		}
            	}
        	}
        	//echo "<hr>del_tags_ids - "; print_r($del_tags_ids); echo "<hr>";
        	if (!empty($del_tags_ids)) {
        		$db->del("{$this->table_prefix}_news_tag_item",
        			array('item_id' => $id, new DbExp('tag_id IN ('.implode(',', $del_tags_ids).')'))
        		);
        	}
        	//echo "<hr>add_tags - "; print_r($add_tags); echo "<hr>";
        	//exit;
        	if (! empty($add_tags)) {
        		$this->addTags($id, $add_tags);			    
        	}
		}

		return $ret;
	}

//
	function delete_news($id, $ctgr_id)
	{   global $CONFIG, $db;
	
		$ND = new CNewsData_DB($this->table_prefix."_news", "id");
		$id = intval($id);   
	    $ctgr_list = $this->getNewsCtgrList($id);
	    if ($ctgr_list && array_key_exists($ctgr_id, $ctgr_list)) {
	        $db->query("DELETE FROM {$this->table_prefix}_news_ctgr WHERE news_id={$id} AND ctgr_id={$ctgr_id}");
	        unset($ctgr_list[$ctgr_id]);
	    }
	    $ret = 1;
	    if (empty($ctgr_list)) {
    		if (!$ND->delete($id)) {
    			$ret = 0;
    		}
    		$db->del("{$this->table_prefix}_news_tag_item", array('item_id' => $id));
	    }
	    
	    return $ret;   
	}
//
	function show_news_adm($id = null, $start = null) {
		global $tpl, $CONFIG;
		$id	= (int)$id;
		if ($id) {
			$ND = new CNewsData_DB($this->table_prefix.'_news', 'id');
			$ND->load_news($id);
			$tpl->assign(
				array(
					'news_title'		=> htmlspecialchars($ND->title),
					'news_title_add'		=> htmlspecialchars($ND->title_add),
					'news_source'		=> htmlspecialchars($ND->source),
					'text'				=> $ND->body,
					'news_display_date'	=> $ND->display_date,
					'date_hrs'			=> $ND->hrs,
					'date_min'			=> $ND->min,
					'date'				=> $ND->date,
					'calendar_date'		=> $ND->calendar_date,
					'news_announce'		=> htmlspecialchars($ND->announce),
					'news_announce_with_links'	=> $ND->announce_with_links,
					'news_body'			=> $body,
					'checked'			=> ($ND->split_body == 1) ? ' checked' : '',
					'news_address'		=> $_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'],
					'news_id'			=> $ND->id,
					'rss_id'			=> $ND->rss_id,
					'rss_link'			=> $ND->rss_link,
					'news_seo_title'        	=> $ND->seo_title,
					'news_description'	=> $ND->description,
					'news_keywords'		=> $ND->keywords,
					'start'				=> $start,
					'img_view_link'		=> $ND->img ? '<a target="new" href="'.$ND->img.'">' . $this->_msg["see"] . ' '.$ND->img.'</a>' : '',
					'img'				=> $ND->img,
					'tags'				=> trim($ND->tags, ',')
				)
			);
			if ($ND->img) {
				$tpl->newBlock('img');
				$tpl->assign(array(
					'img' => $ND->img
				));
				$tpl->gotoBlock('_ROOT');
			} 
			return ($ND->category_id) ? array($ND->category_id, $ND->theme_id, $ND->hrs, $ND->min) : FALSE;
		}
	}

//
	function show_news_titles($category, $start = null, $rows = null, $order_by = null, $owner_id = 0) {
		global $CONFIG, $tpl, $baseurl, $db;
    	$category	= intval($category);
		if (!isset($category)) return false;

		$pages_cnt	= -1;
		$start		= (intval($start)) ? $start : 1;
    	if (!isset($rows))	$rows = $CONFIG["news_max_rows"];
	    if (!isset($order_by)) $order_by = $CONFIG["news_admin_order_by"];
		$tpl->assign("form_link", $baseurl."&category=".$category."&start=".$start);
		$total_cnt = $this->get_list_with_rights(
								"n.*, DATE_FORMAT(date,'".$CONFIG["news_date_format"]."') as news_date",
								$this->table_prefix."_news n INNER JOIN {$this->table_prefix}_news_ctgr NC ON n.id=NC.news_id", "n",
								"NC.ctgr_id = ".$category, "", $order_by,
								$start, $rows,
								true, $owner_id);
		$pages_cnt = ceil($total_cnt/$rows);
		$actions = array(	"edit"		=> "edit",
							"editrights"=> "editrights",
							"activate"	=>	array("activate","suspend"),
							"delete"	=> "del",
							"confirm_delete"	=> $this->_msg["Confirm_delete_record"]
						);
		while ($db->next_record()) {
			$id				= $db->f('id');
			$adm_actions = $this->get_actions_by_rights($baseurl."&id=".$id."&category=".$category."&start=".$start, $db->f("active"), $db->f("is_owner"), $db->f("user_rights"), $actions);

			$title 					= htmlspecialchars($db->f("title"));
			$source 					= htmlspecialchars($db->f("source"));
			$change_status			= ($db->f("active")) ? "suspend" : "activate";
			$action_text			= ($db->f("active")) ? $this->_msg["Suspend"] : $this->_msg["Activate"];
			$action_img				= ($db->f("active")) ? "ico_swof.gif" : "ico_swon.gif";
			$tpl->newBlock('block_news_titles');
			$tpl->assign(array(
				"news_hits"			=>	$db->f("hits"),
				"news_title"		=>	$title,
				"news_source"		=>	$source,
				"news_date"			=>	$db->f("news_date"),
				'news_edit_action'	=>	$adm_actions["edit_action"],
				'news_edit_rights_action'	=>	$adm_actions["edit_rights_action"],
				'news_del_action'	=>	$adm_actions["delete_action"],
				'change_status_action'	=>	$adm_actions["change_status_action"],
				"news_id"			=>	$id,
			));
            if($category==9)
            {
                $tpl->newBlock('block_mailgun_st');
                $tpl->assign(array(
                    'id' => $id,
                    'mail_check' => $db->f("mail_check") ? "<img src='/i/admin/ico_mail.gif'>" : "",
                    'text' => $db->f("mail_check") ? "Повторить" : "Выполнить",
                ));
            }

			if ($adm_actions["rights"]["d"] || $adm_actions["rights"]["p"]) {
				$tpl->newBlock('block_check');
				$tpl->assign("news_id", $id);
			}
		}
		return $pages_cnt;
	}

//        id
	function show_category($id = null, $need_output = true) {
		global $tpl, $baseurl;
		if (!isset($id)) return false;
		$id = intval($id);
		$NC = new CNewsCategories_DB($this->table_prefix."_news_categories", "id");
		$NC->load($id);
		$title = htmlspecialchars($NC->title);
		if ($need_output) {
			$tpl->assign(array(
				'category' => $NC->id,
				'category_title' => $title,
				'category_link' => "$baseurl&action=shownews&category=".$NC->id,
				'rss_update_interval' => $NC->rss_update_interval,
				'rss_auto_update' => $NC->rss_auto_update ? 'checked' : ''
			));
		}
		return array($NC->title, $NC->owner_id, $NC->commentable);
	}

//   update   id
	function update_category($id, $data)
	{
		global $db;

		if (! $id) {
			return false;
		}
		
		$set = array();
		
		if ($data['title']) {
		    $set['title'] = strip_tags($data['title']);
		}
		if (isset($data['rss_update_interval'])) {
		    $set['rss_update_interval'] = (int)$data['rss_update_interval'];
		    $set['rss_auto_update'] = $data['rss_auto_update'] > 0 ? 1 : 0;
		}
		if (isset($data['commentable'])) {
		    $set['commentable'] = (int)$data['commentable'];
		}
		$ret = false;
		if (! empty($set)) {
		    $ret = $db->set("{$this->table_prefix}_news_categories", $set, array('id' => $id));
		}

		return $ret;
	}

//      id
	function delete_category($id = null)
	{   global $db, $CONFIG;

		$NC	= new CNewsCategories_DB($this->table_prefix."_news_categories", "id");
		if (!$id) {
			return false;
		}
		$id	= (int)$id;
		if (!$NC->DeleteByID($id)) {
			return FALSE;
		} else {
			$news_ids = $db->fetchColumn("{$this->table_prefix}_news_ctgr", 'news_id', array('clause' => array('ctgr_id' => $id)));
			if (! empty($news_ids)) {
				$db->query("SELECT N.id FROM {$this->table_prefix}_news N INNER JOIN {$this->table_prefix}_news_ctgr C ON N.id=C.news_id"
					." WHERE N.id IN (".implode(',', $news_ids).") GROUP BY N.id HAVING COUNT(C.ctgr_id)=1");
				$ids = array();
				while($db->next()) {
					$ids[] = $db->Record['id'];
				}
				if (! empty($ids)) {
					$ids = implode(',', $ids);
					$db->del("{$this->table_prefix}_news", array(new DbExp("id IN ({$ids})")) );
					$db->del("{$this->table_prefix}_news_tag_item", array(new DbExp("item_id IN ({$ids})")) );
				}
			}
			$db->del("{$this->table_prefix}_news_ctgr", array('ctgr_id' => $id) );
			
			return TRUE;
		}

		return false;
	}

//
	//function show_categories($blockname, $transurl, $selected_category = null, $only_editable = false, $only_active = false, $with_news_cnt = false)
	function show_categories($blockname, $transurl = '', $filter = array())
	{   global $tpl, $baseurl;

	    $NC = new CNewsCategories_DB($this->table_prefix.'_news_categories', 'id');
		$selected_category = intval($filter['selected']);
		$ctgs_list = $this->get_categories($filter);
		if ($ctgs_list) {
			$actions = array(	"edit"		=> "editctg",
								"editrights"=> "editctgrights",
								"delete"	=> "delctg",
								"confirm_delete"	=> ""
							);
			foreach($ctgs_list as $key=>$val) {
				$adm_actions = $this->get_actions_by_rights($baseurl."&category=".$val['id'], 0, $val["is_owner"], $val["user_rights"], $actions);
				$selected = ($val['id'] == $selected_category) ? ' selected' : '';
				$tpl->newBlock($blockname);
				$tpl->assign(array(
					"MSG_Category_ID" => $this->_msg["Category_ID"],
					"MSG_is" => $this->_msg["is"],
					'category_title'		=> $val['title'],
					'category_link'			=> $baseurl.'&action=shownews&category='.$val['id'],
					'category_edit_action'	=>	$adm_actions["edit_action"],
					'category_edit_rights_action'	=>	$adm_actions["edit_rights_action"],
					'category_del_action'	=>	$adm_actions["delete_action"],
					'category'				=> $val['id'],
				    'id'				=> $val['id'],
					'selected'				=> $selected,
				    'baseurl'				=> $baseurl,
				    'transurl'				=> $transurl,
				));
				if ($val['news_cnt']) {
				    $tpl->newBlock('news_cnt');
				    $tpl->assign('cnt', $val['news_cnt']);
				}
			}
			return count($NC);
		}
		return false;
	}

	//
	function getCategories($selected_category = NULL) {
		GLOBAL $tpl, $baseurl;
		$selected_category = (int)$selected_category;
		$ctgs_list = $this->get_categories();
		if ($ctgs_list) {
			foreach($ctgs_list as $key=>$val) {
				$selected	= ($val['id'] == $selected_category) ? ' selected' : '';
				$arr[$val['id']] = array(
				    'category_title'		=> $val['title'],
				    'category_link'			=> $baseurl.'&action=shownews&category='.$val['id'],
					'category_edit_link'	=> $baseurl.'&action=editctg&category='.$val['id'],
					'category_del_link'		=> $baseurl.'&action=delctg&category='.$val['id'],
					'category'				=> $val['id'],
					'id'       				=> $val['id'],
					'selected'				=> $selected,
				);
			}
			return $arr;
		}
		return FALSE;
	}

//
	function get_categories($filter = array())
	{   global $db;
		
	    $clause = array();
	    if ($filter['ctgr']) {
	        if (is_array($filter['ctgr'])) {
	            $clause[] = 'C.id IN ('.implode(',', array_map('intval', $filter['ctgr'])).')';
	        } else {
	            $clause[] = 'C.id='.intval($filter['ctgr']);
	        }
	    }
	    $clause = empty($clause) ? '' : implode(' AND ', $clause);
	    if ($filter['with_news_cnt']) {
	        $this->get_list_with_rights(
	            "C.*, COUNT(NC.news_id) as news_cnt",
	            "{$this->table_prefix}_news_categories AS C LEFT JOIN {$this->table_prefix}_news_ctgr AS NC ON C.id=NC.ctgr_id",
	            "C", $clause, 'C.id'
	        );
	    } else {
	        $this->get_list_with_rights("C.*", $this->table_prefix."_news_categories AS C", "C", $clause);
	    }
		$temp = array();
		while ($db->next_record()) {
			if (!$filter['only_editable'] || $db->f("user_rights") & 2) {
		        $temp[] = $db->Record;
		    }
		}
		return $temp;
	}

	//
	function add_category($title, $block_id = 0) {
		global $db;
		$title = $db->escape(trim(strip_tags($title)));
		if (!$title) return FALSE;

		$owner_id = $_SESSION["siteuser"]["id"];
		list($group_id, $rights) = $this->get_group_and_rights('news_default_rights', $block_id);

		$query = "INSERT INTO ".$this->table_prefix."_news_categories (title,owner_id,usr_group_id,rights,commentable)
					VALUES ('$title',$owner_id,$group_id,$rights,".intval($_REQUEST['commentable']).")";
		$db->query($query);
		return ($db->affected_rows() > 0) ? TRUE : FALSE;
	}


//
	function get_themes() {
		global $db;
		return $db->getArrayOfResult("SELECT * FROM ".$this->table_prefix."_news_themes ORDER BY title");
	}

//
	function show_themes($blockname, $selected_theme = null) {
		global $tpl, $baseurl;
		$selected_theme = intval($selected_theme);
		if ($themes = $this->get_themes()) {
			foreach($themes as $key=>$val) {
				$selected = ($val['id'] == $selected_theme) ? ' selected' : '';
				$tpl->newBlock($blockname);
				$tpl->assign(Array(
					"MSG_Edit" => $this->_msg["Edit"],
					"MSG_Topic_ID" => $this->_msg["Topic_ID"],
					"MSG_is" => $this->_msg["is"],
					"MSG_Confirm_delete_topic" => $this->_msg["Confirm_delete_topic"],
					"MSG_Delete" => $this->_msg["Delete"],
				));
				$tpl->assign(array(
					'theme_title'		=> $val['title'],
					'theme_link'		=> $baseurl.'&action=edittheme&theme='.$val['id'],
					'theme_edit_link'	=> $baseurl.'&action=edittheme&theme='.$val['id'],
					'theme_del_link'	=> $baseurl.'&action=deltheme&theme='.$val['id'],
					'theme'				=> $val['id'],
					'selected'			=> $selected,
				));
			}
			return count($themes);
		}
		return false;
	}

//       id
	function get_theme($id) {
		global $db;
		return $db->getArrayOfResult("SELECT * FROM ".$this->table_prefix."_news_themes WHERE id=".$id);
	}

//      id
	function show_theme($id = null) {
		global $tpl, $baseurl, $server, $lang;
		$id = intval($id);
		if ($theme = $this->get_theme($id)) {
			$tpl->assign(array(
				'theme'				=> $theme[0]['id'],
				'theme_title'		=> $theme[0]['title'],
			));
			return true;
		}
		return false;
	}

//
	function add_theme($title = NULL) {
		global $db;
		$title = $db->escape(trim($title));
		if (!$title) return false;
		return $db->query('INSERT INTO '.$this->table_prefix.'_news_themes SET title="'.$title.'"');
	}

//
	function update_theme($id, $title = NULL) {
		global $db;
		$id = (int)$id;
		$title = $db->escape(trim($title));
		if (!$id || !$title) return false;
		return $db->query('UPDATE '.$this->table_prefix.'_news_themes SET title="'.$title.'" WHERE id='.$id);
	}

//
	function delete_theme($id) {
		global $db;
		$id = (int)$id;
		if (!$id) return false;
		$db->query('UPDATE '.$this->table_prefix.'_news SET theme_id=0 WHERE theme_id='.$id);
		return $db->query('DELETE FROM '.$this->table_prefix.'_news_themes WHERE id='.$id);
	}

	/**
	 * Извлекает информацию о категории.
	 *
	 * @param int $category_id - идентификатор категории
	 * @return mixed - массив с данными о категории
	 */
	function get_category($category_id)
	{   global $db;

		$this->get_list_with_rights("C.*", $this->table_prefix."_news_categories AS C", "C",
		"`id` = ".intval($category_id));

		$result = array();
		while ($db->next_record()) {
			$temp[] = $db->Record;			
		}

		if (count($temp) > 0) {
			return $temp[0];
		} else {
			return false;
		}
	}

	/**
	 * Отображение списка RSS-каналов
	 *
	 * @param int $category_id - идентификатор категории
	 * @param int $start - начинать с какого
	 * @param int $rows - количество элементов
	 * @param string $order_by - сортировка
	 * @return mixed
	 */
	function show_rss_channels($category_id, $start = null, $rows = null, $order_by = null)
	{
		global $tpl, $baseurl;

		$category_id = intval($category_id);
		$rss_channels = $this->get_rss_channels($category_id);
		if (!$rss_channels)
		{
			return false;
		}

		// Получим данные о категории.
		$category_info = $this->get_category($category_id);

		//print_r($category_info);

		// Создаем алиасы для действий.
		$actions = array(
			"edit" => "edit_rss",
			"delete" => "delete_rss",
			"confirm_delete" => ""
		);

		if (!count($rss_channels))
		{
			return false;
		}

		// Отображаем спискок RSS-каналов.
		foreach ($rss_channels as $rss)
		{
			// Получаем действия по правам доступа категории.
			$adm_actions = $this->get_actions_by_rights($baseurl."&category=".$category_info['id']."&rss_id=".intval($rss['id']), 0,
			$category_info["is_owner"], $category_info["user_rights"], $actions);

			$tpl->newBlock('block_rss_channels');
			$tpl->assign(array(
				"MSG_rss_channel" => $this->_msg["rss_channel"],
				"MSG_is" => $this->_msg["is"],
				'rss_title' => htmlspecialchars($rss['name']),
				'edit_rss_action' => $adm_actions["edit_action"],
				'delete_rss_action' =>	$adm_actions["delete_action"]
			));
		}

		return true;
	}

	/**
	 * Отображение данных об RSS-канале
	 *
	 * @param int $rss_id - идентификатор канала, если 0 - данные не заполняются
	 * @param int $category_id - идентификатор категории
	 * @return mixed
	 */
	function show_rss_channel($rss_id, $category_id)
	{
		global $tpl;

		$rss_id = intval($rss_id);
		$category_id = intval($category_id);

		if (!$category_id)
		{
			return false;
		}

		if ($rss_id)
		{
			// Редактирование. Извлекам информацию о канале RSS.
			$rss_data = $this->get_rss_channel($rss_id);
			if (!$rss_data)
			{
				return false;
			}

			$channel_name = htmlspecialchars($rss_data['name']);
			$channel_url = htmlspecialchars($rss_data['url']);
		}
		else
		{
			// Добавление.
			$channel_name = '';
			$channel_url = '';
		}

		$tpl->newBlock('block_rss_data');
		$tpl->assign(array(
			"rss_name" => $channel_name,
			"rss_url" => $channel_url,
			"MSG_rss_name" => $this->_msg['rss_name'],
			"MSG_rss_url" => $this->_msg['rss_url'],
			'category' => $category_id
		));

		// В случае редактирования отображаем невидимое поле ввода с id канала.
		if ($rss_id)
		{
			$tpl->newBlock('block_hidden_rss_id');
			$tpl->assign(array(
				"rss_id" => $rss_id
			));
		}
	}

	/**
	 * Получить список всех RSS-каналов.
	 *
	 * @param int $category_id - идентификатор категории, если 0 - получаем каналы всех категорий
	 * @return ассоциативный массив с данными о RSS-каналах
	 */
	function get_rss_channels($category_id)
	{
		$category_id = intval($category_id);

		$where = '';
		if ($category_id)
		{
			$where .= " AND {$this->table_prefix}_news_rss.category_id = '$category_id'";
		}
		$sql = "SELECT * FROM {$this->table_prefix}_news_rss WHERE 1 $where ORDER BY id";

		global $db;
		$result = $db->getArrayOfResult($sql);

		return $result;
	}

	/**
	 * Получить информацию об RSS-канале.
	 *
	 * @param int $rss_id - идентификатор RSS-канала
	 * @return ассоциативный массив с данными об RSS-канале
	 */
	function get_rss_channel($rss_id)
	{
		$rss_id = intval($rss_id);

		$sql = "SELECT * FROM {$this->table_prefix}_news_rss WHERE `id` = '$rss_id'";

		global $db;
		$result = $db->getArrayOfResult($sql);

		if ($result)
		{
			return $result[0];
		}
		else
		{
			return false;
		}
	}

	/**
	 * Добавление/обновление данных об RSS-канале.
	 *
	 * @param int $rss_id - идентификатор RSS-канала, если 0, то будет создан новый канал
	 * @param int $category_id - идентификатор категории
	 * @param string $rss_name - название канала
	 * @param string $rss_url - URL-адрес канала
	 * @return mixed идентификатор вставленного или обновленного канала, в случае неудачи - false
	 */
	function add_update_rss_channel($rss_id, $category_id, $rss_name, $rss_url)
	{
		global $db;

		$rss_id = intval($rss_id);
		$category_id = intval($category_id);
		$rss_name = $db->escape(trim($rss_name));
		$rss_url = $db->escape(trim($rss_url));

		if (empty($rss_name) || empty($rss_url))
		{
			return false;
		}

		if ($rss_id)
		{
			// Обновление данных.
			$sql = "UPDATE ".$this->table_prefix."_news_rss SET
				`category_id` = '$category_id',
				`name` = '$rss_name',
				`url` = '$rss_url'
				WHERE `id` = '$rss_id'";
		} else {
			// Добавление данных.
			$sql = "INSERT INTO ".$this->table_prefix."_news_rss SET
				`category_id` = '$category_id',
				`name` = '$rss_name',
				`url` = '$rss_url'";
		}

		$result = $db->query($sql);
		if (!$result)
		{
			return false;
		}
		else
		{
			if ($rss_id)
			{
				return $rss_id;
			}
			else
			{
				return $db->lid();
			}
		}
	}

	/**
	 * Удаление RSS-канала
	 *
	 * @param int $rss_id - идентификатор канала
	 * @return mixed - рузельтат выполнения запроса
	 */
	function delete_rss_channel($rss_id)
	{
		global $db;
		$rss_id = intval($rss_id);
		if (!$rss_id)
		{
			return false;
		}

		$sql = "DELETE FROM {$this->table_prefix}_news_rss WHERE  `id` = '$rss_id'";

		global $db;
		return $db->query($sql);
	}

	/**
	 * Автоматический импорт новостей из RSS каналов, доступных указанной категории.
	 * Внимание! Для работы функции необходимо подключить файл require_once(RP.'inc/simplepie.inc');
	 *
	 * @param int $category_id - идентификатор категории
	 * @param bool $auto_update - проверять ли дату последнего обновления
	 * @return bool результат выполнения
	 */
	function import_rss($category_id, $auto_update = false)
	{
		global $db;
		$category_id = intval($category_id);

		if (!$category_id)
		{
			return false;
		}

		// Права доступа для директории.
		list($allow, $ctg_rights) = $this->test_item_rights("e", "id", $this->table_prefix."_news_categories",
			$category_id, false, 0, true);

		if (!$allow)
		{
			return false;
		}

		// Проверяем, итек ли срок обновления.
		if ($auto_update)
		{
			$sql = "SELECT * FROM ".$this->table_prefix."_news_categories
				WHERE `id` = '$category_id' AND (`rss_auto_update` = '1'
				AND (DATE_ADD(`rss_last_update`, INTERVAL `rss_update_interval` MINUTE) < NOW() OR `rss_last_update` IS NULL))";
			$ret = $db->getArrayOfResult($sql);
			if (!$ret) {
				return false;
			}
		}

		// Получаем список всех RSS-каналов для данной категории.
		$rss_channels = $this->get_rss_channels($category_id);
		if (!$rss_channels) {
			return false;
		}


		// Формируем список новостей для каждого канала.
		$data = array();
		$feed = new SimplePie();
		$feed->enable_cache(false);
		foreach ($rss_channels as $rss)
		{
			// Устанавливаем источник.
			$feed->set_feed_url($rss['url']);
			$feed->init();

			// Загружаем данные.
			$feed->handle_content_type();

			if (!$feed->data) {
				// Данных нет, пропускаем этот канал.
				continue;
			}

			$items = $feed->get_items();
			foreach ($items as $item)
			{
				// Формируем и добавляем новость.
				$news = array();
				$news['id'] = $item->get_id(false);
				$news['title'] = $item->get_title();
				$news['description'] = $item->get_description();
				$news['content'] = $item->get_content();
				$news['date'] = $item->get_date('d.m.Y');
				$news['hour'] = $item->get_date('H');
				$news['min'] = $item->get_date('m');
				$news['link'] = $item->get_link();

				if (empty($news['content']))
				{
					$news['content'] = $news['description'];
				}

				$data[] = $news;
			}
		}

		if (!count($data))
		{
			return false;
		}

		// Импортируем данные в БД.
		foreach ($data as $news)
		{
			$rss_id = trim(strval($news['id']));
			if (empty($rss_id)) {
				continue;
			}

			// Проверяем, есть ли уже в БД новость с таким же идентификатором RSS.
			$result = $db->getArrayOfResult("SELECT COUNT(*) AS cnt FROM {$this->table_prefix}_news WHERE `rss_id` = '{$rss_id}'");

			if (!$result) {
				continue;
			}

			if ($result[0]['cnt'] > 0) {
				// Такая новость уже существует, пропускаем.
				continue;
			}


			// Добавляем новость.
			$result = $this->add_news($category_id, 0, $news['title'], '', $news['content'], '',
				$news['date'], $news['hour'], $news['min'], '', '', '', '', '', $ctg_rights, 0, $news['id'], 1, $news['link']);

		}

		$date = date('Y-m-d H:i:s');

		// Указываем дату/время последнего обновления.
		$sql = "UPDATE ".$this->table_prefix."_news_categories SET
			`rss_last_update` = '$date'
			WHERE `id` = '$category_id'";
		$db->query($sql);

		return true;
	}

	function get_news_link($transurl, $id, $category) {
		global $CONFIG;
		return $CONFIG['rewrite_mod']
		    ? (false !== ( $pos = strpos($transurl, "?") ) ? substr($transurl, 0, $pos) : $transurl).($category ? "{$category}/" : "").$id.".html"
		    : $transurl."&action=show&id=".$id.($category ? "&category={$category}" : "");
	}

	function getCommentableItem($id)
	{   global $db;

		$db->query("SELECT N.id, N.title, C.commentable FROM "
		    .$this->table_prefix."_news N INNER JOIN "
		    .$this->table_prefix."_news_ctgr NC ON N.id=NC.news_id INNER JOIN "
		    .$this->table_prefix."_news_categories C ON NC.ctgr_id=C.id WHERE N.id="
		    .intval($id)." AND C.commentable>0");
		$ret = false;
		while (!$ret && $db->next_record()) {
			$ret = (1 == $db->Record['commentable'] || (2 == $db->Record['commentable'] && $_SESSION['siteuser']['id'] > 0) )
			 ? array(
    			'id' => $db->Record['id'],
    			'title' => $db->Record['title']
    		) : false;
		}

		return $ret;
	}
	
	function getCtrgIds($ids)
	{   global $db;
	
		$ids = array_map('intval', $ids);
		$ret = false;
		$db->query("SELECT id FROM {$this->table_prefix}_news_categories WHERE id IN (".implode(",", $ids).")");
		while ($db->next_record()) {
			$ret[] = $db->Record['id'];
		}
		
		return $ret;
	}
	
	function getNewsCtgrList($id)
	{   global $db;
	
		$ret = false;
		$db->query("SELECT C.id, C.title FROM {$this->table_prefix}_news_ctgr NC JOIN {$this->table_prefix}_news_categories C ON NC.ctgr_id=C.id WHERE news_id=".intval($id));
		while ($db->next_record()) {
			$ret[$db->Record['id']] = array('id' => $db->Record['id'], 'title' => $db->Record['title']);
		}
		
		return $ret;
	}
	
	function setNewsCtrgs($news_id, $ctgr_id_list)
	{   global $db;
	
	    $ret = false;
		$news_id = (int) $news_id;
		$db->query("DELETE FROM {$this->table_prefix}_news_ctgr WHERE news_id={$news_id}");
		$ctgr_id_list = array_map('intval', $ctgr_id_list);
		if (! empty($ctgr_id_list)) {
		    $db->query("INSERT INTO {$this->table_prefix}_news_ctgr (news_id, ctgr_id) VALUES ({$news_id}, "
		        .implode("), ({$news_id}, ", $ctgr_id_list).")");
		    $ret = true;
		}
		
		return $ret;
	}
	
	function getNews($id, $ctgr_id = 0)
	{   global $CONFIG, $db;
	    $news_id = (int) $id;
		$ctgr_id = (int) $ctgr_id;
	    if (! isset($this->_cache['news'.$id])) {
	        $this->get_list_with_rights(
    		    "n.*, t.title as theme_title, DATE_FORMAT(date,'{$CONFIG["news_date_format"]}') as date, date as original_date, n.category_id",
    		    ! $ctgr_id ? "{$this->table_prefix}_news n LEFT JOIN {$this->table_prefix}_news_themes t ON n.theme_id = t.id"
    		        : "{$this->table_prefix}_news n INNER JOIN {$this->table_prefix}_news_ctgr NC "
    		          ."ON n.id=NC.news_id LEFT JOIN {$this->table_prefix}_news_themes t ON n.theme_id = t.id",
    		    "n",
    		    "n.id = ".$id.($ctgr_id ? " AND NC.ctgr_id={$ctgr_id}" : "")
    		        .($CONFIG['edit_on_site'] && $_SESSION["session_is_admin"] && $_COOKIE['edit_content'] ? "" : " AND n.active = 1")
    		);

    		if ($db->next_record()) {
    		    $this->_cache['news'.$id] = $db->Record;
    		}
	    }
		return $this->_cache['news'.$id] ? $this->_cache['news'.$id] : false;
	}
	
	function getTagsBaseForm($str)
	{   global $CONFIG;
	
	    require_once(RP."inc/rumor/class.RuMor.php");
	    $ret = array();
	    $tags = preg_split("/,|\s/", $str, -1, PREG_SPLIT_NO_EMPTY);
	    
	    $rumor = new RuMor(RP."inc/rumor");
		$rumor->prep_dict();
		while ( count($ret) <= $CONFIG['gallery_max_tags_cnt'] && (list(,$v) = each($tags)) ) {
			$base = $rumor->get_base_form(iconv("UTF-8", "WINDOWS-1251", $v));			
			$val = empty($base) ? $v : iconv("WINDOWS-1251", "UTF-8", $base[0]);
			if (! in_array($val, $ret)) {
			    $ret[] = $val;
			}
		}
		
		return $ret;
	}
	
	function getTagsByNames($names)
	{   global $db;
	
	    $ret = false;	    
		
	    if (is_array($names) && !empty($names)) {
	        $names = array_map('abo_strtolower', $names);
    		$names = array_map(array('My_Sql', 'escape'), $names);
    		$ret = $db->fetchAll("{$this->table_prefix}_news_tag",
    		    array('clause' => array(new DbExp("name IN ('".implode("','", $names)."')"))),
    		    'name'
    		);
	    }
	    
	    return $ret;
	}
	
	function getTags($id)
	{   global $db;
	
		$db->query("SELECT T.* FROM {$this->table_prefix}_news_tag_item TI INNER JOIN {$this->table_prefix}_news_tag T"
		   ." ON TI.tag_id=T.id WHERE TI.item_id=".intval($id)
		);
		$ret = false;
		while ($db->next()) {
			$ret[$db->Record['id']] = $db->Record;
		}
		
		return $ret;
	}
	
	function addTags($id, $base_tags)
	{   global $db;

		if (! empty($base_tags)) {
			$tags = $this->getTagsByNames($base_tags);
			foreach ($base_tags as $val) {
			    if (isset($tags[$val])) {
			    	$tag_id = $tags[$val]['id'];
			    } else {
			    	$db->set("{$this->table_prefix}_news_tag", array('name' => $val));
			    	$tag_id = $db->lid();
			    }
			    $db->set("{$this->table_prefix}_news_tag_item", array('tag_id' => $tag_id, 'item_id' => $id));
			}
		}
	}
	
	function getTagCloud($param = array())
	{   global $db, $CONFIG;
	
		$limit = '';
		if ($param['limit'] > 0) {
		    $limit = " LIMIT ".intval($param['limit']);
		}
		$db->query("SELECT T.*, count(I.item_id) AS item_cnt FROM {$this->table_prefix}_news_tag T"
		    ." INNER JOIN {$this->table_prefix}_news_tag_item I ON T.id=I.tag_id GROUP BY T.id ORDER BY item_cnt DESC".$limit);

		$ret = $max = $min = false;
		while ($db->next()) {
			$ret[$db->Record['id']] = $db->Record;
			$ret[$db->Record['id']]['title'] = $db->Record['name'];
			if (false === $max || $db->Record['item_cnt'] > $max) {
			    $max = $db->Record['item_cnt'];
			}
			if (false === $min || $db->Record['item_cnt'] < $min) {
			    $min = $db->Record['item_cnt'];
			}
		}
		$step = ($max - $min)/$CONFIG['news_font_cnt'];
		foreach ($ret as $key => $val ) {
		    $ret[$key]['font_size'] = 1;
			for($a = $min, $i = 1; $a < $val['item_cnt']; $a += $step, $i++) {
			    $ret[$key]['font_size'] = $i;

			}
		}
		if ($ret) {
		    usort($ret, create_function('$a, $b', 'return strcasecmp($a["name"], $b["name"]);'));
		}

		return $ret;
	}
    
	function getNewsDates($month, $ctgr_id = 0){
        global $CONFIG, $db;
        if(strlen((string) $month)!=7){
            preg_match('~^(\d{4})\-(\d{1,2})~',(string) $month,$M);
            $month = sprintf('%4d-%02d',$M[1]?$M[1]:date('Y'),$M[2]?$M[2]:date('m'));
        }
        $this->get_list_with_rights(
            "distinct DAYOFMONTH(date) as day",
            ! $ctgr_id ? "{$this->table_prefix}_news n"
                : "{$this->table_prefix}_news n INNER JOIN {$this->table_prefix}_news_ctgr NC ON n.id=NC.news_id ",
            "n",
            "DATE_FORMAT(date, '%Y-%m')='$month' "
                .($CONFIG['edit_on_site'] && $_SESSION["session_is_admin"] && $_COOKIE['edit_content'] ? "" : " AND n.active = 1")
        );
        $ret = array();
		while ($db->next_record()) {
			$ret[] = $db->Record['day'];
		}
        return $ret;
    }

    function resizeimg($filename, $smallimage, $w, $h=false)
    {
        // получим размеры исходного изображения
        $size_img = getimagesize($filename);
        // Если размеры меньше, то масштабирования не нужно
        if (($size_img[0] < $w && $w == 600)) {
            $w=$size_img[0];
        }
        if(!$h)
        {
            $h = $size_img[1]*$w / $size_img[0];

        } //$h=$size_img[1];

        // создадим пустое изображение по заданным размерам
        $dest_img = imagecreatetruecolor($w, $h);
        $white = imagecolorallocate($dest_img, 255, 255, 255);
        if ($size_img[2]==2)  $src_img = imagecreatefromjpeg($filename);
        else if ($size_img[2]==1) $src_img = imagecreatefromgif($filename);
        else if ($size_img[2]==3) $src_img = imagecreatefrompng($filename);

        // масштабируем изображение     функцией imagecopyresampled()
        // $dest_img - уменьшенная копия
        // $src_img - исходной изображение
        // $w - ширина уменьшенной копии
        // $h - высота уменьшенной копии
        // $size_img[0] - ширина исходного изображения
        // $size_img[1] - высота исходного изображения

        imagecopyresampled($dest_img, $src_img, 0, 0, 0, 0, $w, $h, $size_img[0], $size_img[1]);
        // сохраняем уменьшенную копию в файл
        if ($size_img[2]==2)  imagejpeg($dest_img, $smallimage);
        else if ($size_img[2]==1) imagegif($dest_img, $smallimage);
        else if ($size_img[2]==3) imagepng($dest_img, $smallimage);
        // чистим память от созданных изображений
        imagedestroy($dest_img);
        imagedestroy($src_img);
        return true;
    }


    function send_mailgun($to,$subject,$body,$ch){

        $message = array();

        $message['from'] = "Supra-News <info@supra.ru>";

        $message['to'] = $to;

        $message['h:Reply-To'] = "Supra-News <info@supra.ru>";

        $message['subject'] = $subject;

        $message['html'] = $body;

        curl_setopt($ch, CURLOPT_POSTFIELDS,$message);

        /*$result = */ curl_exec($ch);

       // return $result;
//        require_once RP.'/inc/Mailgun/Mailgun.php';
//    use Mailgun\Mailgun;
//        $mg = new Mailgun("key-e2e53874f717a87b99cb2166aba83f18");
//        $domain = "example.com";
//
//        # Now, compose and send your message.
//        $mg->sendMessage($domain, array('from'    => 'Mailgun Sandbox <postmaster@sandbox641153bd76f645c48e4e7b1b42f3f387.mailgun.org>',
//                                        'to'      => 'Kseniya <info@supra.ru>',
//                                        'subject' => 'Hello Kseniya',
//                                        'text'    => 'Congratulations Kseniya, you just sent an email with Mailgun! '));
    }

    function init_mailgun(){

        $ch = curl_init();

        $config = array();

        $config['api_key'] = "key-e2e53874f717a87b99cb2166aba83f18";

        $config['api_url'] = "https://api.mailgun.net/v3/suprashop.ru/messages";

        curl_setopt($ch, CURLOPT_URL, $config['api_url']);

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($ch, CURLOPT_POST, true);

        return $ch;
    }

    function close_mailgun($ch){

        curl_close($ch);
    }
}

?>