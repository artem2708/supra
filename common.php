<?php
//error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_DEPRECATED );
error_reporting(E_ALL ^ E_NOTICE);
$_SERVER["DOCUMENT_ROOT"] = "/home/newsupra/www/";
// Если файл вызван напрямую, то редирект
if (strstr($_SERVER['PHP_SELF'], 'common.php')) {
    header('Location: index.php');
    die();
}

//ini_set('session.cookie_lifetime', 2678400);
//session_set_cookie_params(2678400);
session_start();
setlocale(LC_ALL, 'ru_RU');

define('RP', dirname(__FILE__).'/');									// Константа содержащая полный путь к текущему местоположению
require_once(RP.'config.php');											// Главный конфигурационный файл системы

error_reporting(isset($NEW['error_reporting']) ? $NEW['error_reporting'] : 0);
error_report(isset($NEW['error_report']) ? $NEW['error_report'] : 0);

require_once(RP.$CONFIG['inc_path'].'db_connect.php');					// Функции доступа к серверу БД (устарели, будут со временем удалены!!!)
require_once(RP.$CONFIG['inc_path'].'class.TObject.php');				// Класс устарел и будет со временем удалён!!!
require_once(RP.$CONFIG['inc_path'].'class.AboTemplate.php');
require_once(RP.$CONFIG['inc_path'].'class.Module.php');				// Класс Module реализация общих фунций для модулей системы
require_once(RP.$CONFIG['inc_path'].'class.Main.php');					// Класс Main, составная часть ядра системы
require_once(RP.$CONFIG['inc_path'].'class.LinksControl.php');			// Класс LinksControl, для контроля внутренних ссылок сайта.
require_once(RP.$CONFIG['inc_path'].'class.Cache.php');					// Класс Cache, для работы с кэшем страниц
require_once(RP.$CONFIG['inc_path'].'class.Typograph.php');				// Класс Typograph, для типографской обработки текста
require_once(RP.$CONFIG['inc_path'].'class.Pager.php');    				// Класс для формировани постраничной навигации
require_once(RP.$CONFIG['inc_path'].'func.str.php');    				// Функции для работы с текстом
require_once(RP.$CONFIG['inc_path'].'db_mysql.class.php');    				
require_once(RP.$CONFIG['inc_path'].'dbtree.class.php');    				



$new_db = new db($NEW['db_host'], $NEW['db_user'], $NEW['db_password'], $NEW['db_database']);
$tree = new dbtree('sup_rus_catalog_categories', '', $new_db);

$db 			= new My_Sql;
$db->Database 	= $NEW['db_database'];
$db->Host 		= $NEW['db_host'];
$db->User 		= $NEW['db_user'];
$db->Password 	= $NEW['db_password'];

$db1 			= new My_Sql;
$db1->Database 	= $NEW['db_database'];
$db1->Host 		= $NEW['db_host'];
$db1->User 		= $NEW['db_user'];
$db1->Password 	= $NEW['db_password'];

$db2 			= new My_Sql;
$db2->Database 	= $NEW['db_database'];
$db2->Host 		= $NEW['db_host'];
$db2->User 		= $NEW['db_user'];
$db2->Password 	= $NEW['db_password'];


$db3 			= new My_Sql;
$db3->Database 	= $NEW['db_database'];
$db3->Host 		= $NEW['db_host'];
$db3->User 		= $NEW['db_user'];
$db3->Password 	= $NEW['db_password'];

$db->query("SET NAMES utf8");

$site_config = Main::getSiteConfig();
$_SESSION['session_cur_site_id'] = $site_config['site_id'];
$_SESSION['session_cur_lang_id'] = $site_config['lang_id'];

$mult_lang   = @$site_config['multilanguage'] ? $site_config['multilanguage'] : $NEW['multilanguage'];
$def_lang    = @$site_config['language']      ? $site_config['language'] : $NEW['default_lang'];
$server      = @$site_config['pref']          ? $site_config['pref']."_" : Main::getServerName();

$server = !empty($_GET["site_target"]) ? Main::getAnotherServerName($_GET["site_target"]) : $server;

if (isset($_REQUEST['lang'])) {
	$_REQUEST['lang'] = substr($_REQUEST['lang'], 0, 4);
}
// Определение языковой версии сайта
if ($mult_lang && !empty($_REQUEST['lang'])) {
	$lang	= My_Sql::escape($_REQUEST['lang']);
} else {
	$lang	= $def_lang;
}
define('SITE_PREFIX', $server.$lang);
$main = new Main();
$CONFIG['basic_lang'] = $site_config['default_lang'] ? $site_config['default_lang'] : $CONFIG['default_lang'];
$CONFIG['multilanguage'] = $mult_lang;
$CONFIG['default_lang']  = $def_lang;
$CONFIG['domen']         = $site_config['host'];
$CONFIG['main_domen']    = Main::parseSiteName($CONFIG['web_address']);
$CONFIG['host']          = "http://".$site_config['host']."/";
$CONFIG['web_address']   = $CONFIG['host'];
$CONFIG['site_aliases']   = $site_config['site_aliases'];
$CONFIG['sitename']      = @$site_config['sitename']  ? $site_config['sitename'] : $CONFIG['sitename'];
$CONFIG['sitename_rus']  = @$site_config['sitename_rus']  ? $site_config['sitename_rus'] : $CONFIG['sitename_rus'];
$CONFIG['contact_email'] = @$site_config['contact_email'] ? $site_config['contact_email'] : $CONFIG['contact_email'];
$CONFIG['return_email']  = @$site_config['return_email']  ? $site_config['return_email'] : $CONFIG['return_email'];
$CONFIG['site_pref']         = substr($server, 0, -1);

$CONFIG['user_agent_ie'] = $CONFIG['user_agent_opera'] = $CONFIG['user_agent_ff'] = $CONFIG['user_agent_chrome'] = $CONFIG['user_agent_safari'] = false;
if ( stristr($_SERVER['HTTP_USER_AGENT'], 'Firefox') ) $CONFIG['user_agent_ff'] = true; 
elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Chrome') ) $CONFIG['user_agent_chrome'] = true; 
elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Safari') ) $CONFIG['user_agent_safari'] = true; 
elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Opera') ) $CONFIG['user_agent_opera'] = true; 
elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE') ) $CONFIG['user_agent_ie'] = true; 

// Проверка $_GET на спецсимволы, для предотвращения взлома
if (preg_match("/[(\;)(\|)(\')(\")(\!)(\<)(\>)]/", $_SERVER['REQUEST_URI'])) {
    $main->no_such_page();
    die();
}

$cache	= new Cache;
$lc		= new LinksControl;
$core	= new Core;

$lc->get_internal_links_from_file();
function error_report($var, $set=true)
{   static $x;

    if($set == true) {
        $x = $var;
    } else {
        return $x;
    }
    set_error_handler("ABO_ErrorHandler");
}
function ABO_ErrorHandler($errno, $errmsg, $filename, $linenum, $vars)
{
    $er = error_report(false,false);
    $er_msg = 2039;
    if (!$er) {
        return;
    }
    if ($er&1 && $er_msg&$errno) {//logged to file
        $msg = sprintf("date:%s\tfile:%s\tline:%s\tmsg:%s\n",date('d/m/Y H:i:s'), $filename, $linenum, $errmsg);
        error_log($msg, 3, RP.'admin/error_log.txt');
    }
    if ($er&2 && $er_msg&$errno){//send logg
        $msg = array('errno'=>$errno, 'errmsg'=>$errmsg, 'filename'=>$filename, 'linenum'=>$linenum, 'vars'=>$vars);
        if (function_exists('send_error')) {
            send_error($msg);
        }
    }
}

Module::init_json();

if (isset($_REQUEST['JsHttpRequest'])) {
  require_once RP."inc/class.JsHttpRequest.php";
  $GLOBALS['JsHttpRequest'] = true;
  $GLOBALS['JsHttpRequest'] = new JsHttpRequest();
}
// $_POST и $_GET переменные станут доступны через префикс "request_"
import_request_variables('GP', 'request_');

function isShop(){
	return $_SERVER['HTTP_HOST'] == 'supra.ru' ? false : true;
}

?>