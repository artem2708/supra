<?php
if($_SERVER['SERVER_NAME']=="supra.ru")
{
    header("Location: http://suprashop.ru/login.php?");exit;

}
require_once('common.php');

if("cgi" == php_sapi_name()){
	$_SERVER['SCRIPT_NAME'] = $_SERVER['SCRIPT_FILENAME'] = basename(__FILE__);
}
unset($msg);

$query_string = preg_replace('/logout/i', '', $_SERVER['QUERY_STRING']);

$_SESSION = array();

$msg = "";
if (isset($_REQUEST['logout'])) {
	$msg = 'Вы вышли из системы';
}
// else if ($CONFIG['siteusers_autologin']) {
//	$_SESSION['session_is_admin']	= TRUE;
//	$_SESSION["siteuser"]["siteusername"]	= 'Demo (autologin)';
//	$_SESSION["siteuser"]["id"]				= 0;
//	$_SESSION["siteuser"]["group_id"] = array(0);
//	$_SESSION['permissions']		= array();
//	$_SESSION['session_login']		= $_REQUEST['demo'];
//	$_SESSION['session_password']	= $_REQUEST['demo'];
//
//	$db->query('SELECT b.name as name, b.id as mod_id, e.title as site_domain, e.alias as site, f.language lang
//					FROM sites e,
//						 sites_languages f,
//						 modules b
//					WHERE e.id = f.category_id
//					ORDER BY e.title, f.language');
//
//
//		if ($db->nf() > 0) {
//			for ($i = 0; $i < $db->nf(); $i++) {
//				$db->next_record();
//
//				$server	= str_replace('.', '_', Main::parseSiteName($db->f('site')).'_');
//				$site	= str_replace('-', '_', $server).$db->f('lang');
//
//				$_SESSION['permissions'][$site][$db->f('name')]['r'] = 1;
//				$_SESSION['permissions'][$site][$db->f('name')]['e'] = 1;
//				$_SESSION['permissions'][$site][$db->f('name')]['p'] = 1;
//				$_SESSION['permissions'][$site][$db->f('name')]['d'] = 1;
//			}
//		}
//		header('Location: admin/?'.$query_string);
//}
else if (!empty($_REQUEST['email']) && !empty($_REQUEST['password'])) {

	$username = $db->escape((substr($_REQUEST['email'],0,63)));
	$password = $db->escape((substr($_REQUEST['password'],0,20)));

	if (Main::fillPermissions($username, $password)) {
	    parse_str($query_string, $param);
		header('Location: admin/?'
		    .($param['lang'] ? 'lang='.$param['lang'] : '')
		    .($param['name'] ? '&name='.$param['name'] : ''));
	} else {
		$msg = 'Неправильный логин или пароль';
	}
}

$tpl = new AboTemplate($CONFIG['admin_tpl_path'].'login.html');
$tpl->prepare();

$tpl->assign(
	array(
		'form_action'	=> $_SERVER['PHP_SELF'].'?'. $query_string,
		'msg_text'		=> $msg,
		'page_title'	=> ABOCMS_VERSION
	)
);

$tpl->printToScreen();

exit;
?>