<?php // Vers 5.8.2 7.06.2012

$CONFIG = array(
	'inc_path'				=> 'inc/',								// Путь к папке с дополнительными классами
	'class_path'			=> 'mod/',								// Путь к папке с классами-модулями
	'tpl_path'				=> 'tpl/',								// Путь к папке с русскоязычными шаблонами
	'admin_tpl_path'		=> 'tpl/admin/', 						// Путь к папке с шаблонами для backoffice
	'admin_main_template'	=> array(1 => '_index_1.html',			// Корневой шаблон для backoffice
									 2 => '_index_2.html',			// Корневой шаблон для backoffice, без левой колонки
									 ),
	'img_path'				=> 'i/',								// Путь к картинкам для основного сайта
	'admin_img_path'		=> 'i/admin/',							// Картинки для backoffice
	'tree_img_path'			=> 'i/tree/',							// Картинки для построения дерева (вложенных структур)
	'cache_path'			=> 'cache/', 							// Путь к папке с кэшированными страницами

	'files_upload_img_path'	=> 'files/images/',						// Путь к загруженным картинкам
	'files_upload_img_url' 	=> 'files/images/',						// Путь к закачанным картинкам
	'files_upload_path'		=> 'files/',							// Путь к загруженным файлам

	'admin_default_module'	=> 'pages', 							// Модуль по умолчанию в backoffice (при входе в систему)
	'script_filename'		=> 'index.php', 						// Скрипт, обслуживающий основной сайт

	'multibase'				=> 0, 									// Мультибазовость. 0 = нет, 1 = да. (НЕ РЕАЛИЗОВАННО)

	'no_cache_modules'		=> array(), 							// Список модулей, которые не следует кэшировать
	'no_cache_actions'		=> array('showbanner', 'showpoll'), 	// Список action, которые не следует кэшировать

	'email_headers'			=>	"Content-Type: text/html; charset=utf-8\n",			// Заголовки письма
	'email_type'			=> 'text/html',
	'email_charset'			=> 'utf-8',

	'pages_charset'			=> 'utf-8',

	'nav_separator' 		=> '&nbsp;&raquo;&nbsp;',				// Разделитель в навигационной строчке, показывающей вложенность
	'calendar_date' 		=> '%d.%m.%Y', 							// Дата, выводимая в поле календаря в адм. части. Не менять!
	'pages_in_block' 		=> 10, 									// Кол-во страниц в одном навигационном блоке (для функции разбиения записей по страницам 1 2 3 ... >>)

	// Языки сайтов
	'site_langs'			=> array(0	=> array('value'	=> 'rus',
												 'name'		=> 'Русский',
												 ),
									1	=> array('value'	=> 'ukr',
												 'name'		=> 'Украинский',
												 ),
									2	=> array('value'	=> 'blr',
												 'name'		=> 'Белорусский',
												 ),
									3	=> array('value'	=> 'eng',
												 'name'		=> 'Английский',
												 ),
									4	=> array('value'	=> 'de',
												 'name'		=> 'Немецкий',
												 ),
									5	=> array('value'	=> 'frn',
												 'name'		=> 'Французкий',
												 )
									),
		'css_path'				=> 'css/features/',
		'top_level_links'		=> array('1014', '1517', '1825'),
		'cookie_lt'           => 30, /* время жизни куки в днях */
		'geo_city_file_path' => RP.'admin/files/GeoIPCity.csv',
		'month' => array(
            1 => array('января', 'Январь'),
            2 => array('февраля', 'Февраль'),
            3 => array('марта', 'Март'),
            4 => array('апреля', 'Апрель'),
            5 => array('мая', 'Май'),
            6 => array('июня', 'Июнь'),
            7 => array('июля', 'Июль'),
            8 => array('августа', 'Август'),
            9 => array('сентября', 'Сентябрь'),
            10 => array('октября', 'Октябрь'),
            11 => array('ноября', 'Ноябрь'),
            12 => array('декабря', 'Декабрь')
        ),
        'cy' => array(
            'rur' => array('руб.', 'Рубли'),
            'usd' => array('долл.', 'Доллары США'),
            'eur' => array('евро', 'Евро'),
        ),
        'str_default_crop_len' => 45,
        'str_default_crop_suff' => '...',
);

$OPEN_NEW = array();

/**
* индекс type задёт тип значения, возможные значения:
*	integer  - целое число
*	double- вещественное число
*	string- строка
*	checkbox - элемент допускающий установку/снятие галочки (тег <input type="checkbox">)
*	select- выпадающий список
* индекс defval задаёт значение по умоляанию,
* индекс descr задаёт текстовое описание данного элемента, для вывода в качестве справки
* индекс access устанавливает уровень доступа, возможные значения
* 	public	- элемент доступен для чтения/изменения
*	final	- элемент доступен только для чтения
*	private - элемент не доступен
*/
$TYPES_NEW = array(
			'db_host'					=> array(
												 'type'		=> 'string',
												 'defval'	=> 'localhost',
												 'descr'	=> array(	'rus' => 'Хост сервера баз данных MySQL',
												 						'eng' => 'Host of MySQL database server'),
												 'access'	=> 'editable'
												 ),

			'db_database' 			=> array(
												 'type'		=> 'string',
												 'defval'	=> 'cms-test',
												 'descr'	=> array(	'rus' => 'Название БД',
												 						'eng' => 'DB name'),
												 'access'	=> 'editable'
												 ),

			'db_user'				=> array(
												 'type'	=> 'string',
												 'defval'	=> 'root',
												 'descr'	=> array(	'rus' => 'Пользователь БД',
												 						'eng' => 'DB user'),
												 'access'	=> 'editable'
												 ),

			'db_password'			=> array(
												 'type'		=> 'string',
												 'defval'	=> '',
												 'descr'	=> array(	'rus' => 'Пароль',
												 						'eng' => 'Password'),
												 'access'	=> 'editable'
												 ),

			'sitename'				=> array(
												 'type'	=> 'string',
												 'defval'	=> 'ABO.CMS DEMO',
												 'descr'	=> array(	'rus' => 'Название сайта',
												 						'eng' => 'Site name'),
												 'access'	=> 'editable'
												 ),

			'sitename_rus'				=> array(
												 'type'	=> 'string',
												 'defval'	=> 'ABO.CMS Демо',
												 'descr'	=> array(	'rus' => 'Название сайта на русском языке (используется для вставки в письма)',
												 						'eng' => 'Site name (it is used for an insert in e-mails)'),
												 'access'	=> 'editable'
												 ),

			'sids'				=> array(
												 'type'	=> 'string',
												 'defval'	=> 'ABO.CMS Демо',
												 'descr'	=> array(	'rus' => 'ID магазинов праз запетую',
												 						'eng' => 'Site name (it is used for an insert in e-mails)'),
												 'access'	=> 'editable'
												 ),
			
			
			'web_address'			=> array(
												 'type'		=> 'string',
												 'defval'	=> 'http://abocms/',
												 'descr'	=> array(	'rus' => 'Интернет-адрес веб-сайта',
												 						'eng' => 'Site WEB-address'),
												 'access'	=> 'editable'
												 ),

			'rewrite_mod'			=> array(
												 'type'		=>  'checkbox',
												 'defval'	=> 'checked',
												 'descr'	=> array(	'rus' => 'Использовать ЧПУ',
												 						'eng' => 'Use user friendly URL'),
												 'access'	=> 'editable'
												 ),

			'multilanguage'			=> array(
												 'type'		=> 'checkbox',
												 'defval'	=> 'checked',
												 'descr'	=> array(	'rus' => 'Многоязыковая поддержка',
												 						'eng' => 'Multilanguage support'),
												 'access'	=> 'editable'
												 ),

			'default_lang'			=> array(
												 'type'		=> 'select',
												 'defval'	=> array('rus' => array('rus' => 'Русский',
												 									'eng' => 'Russian'),
												 					 'eng' => array('rus' => 'Английский',
																	 				'eng' => 'English'),
																	 ),
												 'descr'	=> array(	'rus' => 'Язык сайта по умолчанию',
												 						'eng' => 'Language of a site by default'),
												 'access'	=> 'editable'
												 ),

			'admin_lang'			=> array(
												 'type'		=> 'select',
												 'defval'	=> array('rus' => array('rus' => 'Русский',
												 									'eng' => 'Russian'),
												 					 'eng' => array('rus' => 'Английский',
																	 				'eng' => 'English'),
																	 ),

												 'descr'	=> array(	'rus' => 'Язык в административной системе',
												 						'eng' => 'Language in a management system'),
												 'access'	=> 'editable'
												 ),
/*
			'use_cache'				=> array(
												 'type'		=> 'checkbox',
												 'defval'	=> '',
												 'descr'	=> array(	'rus' => 'Кешировать страницы',
												 						'eng' => 'Use page caching'),
												 'access'	=> 'editable'
												 ),

			'cache_expiration_time'	=> array(
												 'type'		=> 'integer',
												 'defval'	=> '0',
												 'descr'	=> array(	'rus' => 'Время в часах, после которого кэш-файл будет сгенерирован вновь (0 - файл не генерируется)',
												 						'eng' => 'Time in hours after which the cache-file will be generated again (0 - a file is not generated)'),
												 'access'	=> 'editable'
												 ),
*/
			'debug'					=> array(
												 'type'		=> 'checkbox',
												 'defval'	=> '0',
												 'descr'	=> array(	'rus' => 'Отображать отладочную информацию (внизу страницы)',
												 						'eng' => 'To display the debugging information (in the bottom of page)'),
												 'access'	=> 'editable'
												 ),

			'edit_on_site'					=> array(
												 'type'		=> 'checkbox',
												 'defval'	=> '0',
												 'descr'	=> array(	'rus' => 'Включить возможность редактирования с сайта',
												 						'eng' => 'Включить возможность редактирования с сайта'),
												 'access'	=> 'editable'
												 ),

			'contact_email'			=> array(
												 'type'		=> 'string',
												 'defval'	=> 'admin@cms.ru',
												 'descr'	=> array(	'rus' => 'Контактный email, отображаемый на сайте',
												 						'eng' => 'Contact email, displayed on a site'),
												 'access'	=> 'editable'
												 ),

			'return_email'			=> array(
												 'type'		=> 'string',
												 'defval'	=> 'admin@cms.ru',
												 'descr'	=> array(	'rus' => 'Обратный email, вставляемый в отсылаемое письмо',
												 						'eng' => 'Return email, inserted into the sent letter'),
												 'access'	=> 'editable'
												 ),

			'back_office_zip'		=> array(
												 'type'		=> 'select',
												 'defval'	=> array('0' => array(	'rus' => '0 (без сжатия)',
												 									'eng' => '0 (without compression)'),
												 					 '1' => array(	'rus' => '1 (минимальное сжатие)',
																	 				'eng' => '1 (the minimal compression)'),
												 					 '2' => array(	'rus' => '2',
																	 				'eng' => '2'),
												 					 '3' => array(	'rus' => '3',
																	 				'eng' => '3'),
												 					 '4' => array(	'rus' => '4',
																	 				'eng' => '4'),
												 					 '5' => array(	'rus' => '5',
																	 				'eng' => '5'),
												 					 '6' => array(	'rus' => '6',
																	 				'eng' => '6'),
												 					 '7' => array(	'rus' => '7',
																	 				'eng' => '7'),
												 					 '8' => array(	'rus' => '8',
																	 				'eng' => '8'),
												 					 '9' => array(	'rus' => '9 (максимальное сжатие)',
																	 				'eng' => '9 (the maximal compression)'),
																	 ),
												 'descr'	=> array(	'rus' => 'Степень сжатия страниц административной части',
												 						'eng' => 'Degree of compression of pages of an administrative part'),
												 'access'	=> 'editable'
												 ),

			'front_office_zip'		=> array(
												 'type'		=> 'select',
												 'defval'	=> array('0' => array(	'rus' => '0 (без сжатия)',
												 									'eng' => '0 (without compression)'),
												 					 '1' => array(	'rus' => '1 (минимальное сжатие)',
																	 				'eng' => '1 (the minimal compression)'),
												 					 '2' => array(	'rus' => '2',
																	 				'eng' => '2'),
												 					 '3' => array(	'rus' => '3',
																	 				'eng' => '3'),
												 					 '4' => array(	'rus' => '4',
																	 				'eng' => '4'),
												 					 '5' => array(	'rus' => '5',
																	 				'eng' => '5'),
												 					 '6' => array(	'rus' => '6',
																	 				'eng' => '6'),
												 					 '7' => array(	'rus' => '7',
																	 				'eng' => '7'),
												 					 '8' => array(	'rus' => '8',
																	 				'eng' => '8'),
												 					 '9' => array(	'rus' => '9 (максимальное сжатие)',
																	 				'eng' => '9 (the maximal compression)'),
																	 ),
												 'descr'	=> array(	'rus' => 'Степень сжатия страниц публичной части',
												 						'eng' => 'Degree of compression of pages of a public part'),
												 'access'	=> 'editable'
												 ),
        'server_updates' => array(
			'type'		=> 'string',
			'defval'	=> 'http://update51.abocms.ru',
			'descr'		=> array(
			'rus' => 'Сервер проверки обновлений',
			'eng' => 'Server check version '
			),
			'access'	=> 'final'
		),
        'L_KEY' => array(
			'type'		=> 'string',
			'defval'	=> 'demo',
			'descr'		=> array(
			'rus' => 'Лицензионный ключ',
			'eng' => 'Licence key '
			),
			'access'	=> 'final'
		),

			 'error_reporting'		=> array(
												 'type'		=> 'select',
												 'defval'	=> array('0'	=> array(	'rus' => 'Не выводить',
												 										'eng' => 'Not display'),
												 					 '85'	=> array(	'rus' => 'Только ошибки',
												 										'eng' => 'Only errors'),
												 					 '6135'	=> array(	'rus' => 'Ошибки и предупреждения',
												 										'eng' => 'Errors and warnings'),
																	 ),
												 'descr'	=> array(	'rus' => 'Режим вывода ошибок (error_reporting)',
												 						'eng' => 'Mode of a displaying of errors (error_reporting)'),
												 'access'	=> 'editable'
												 ),
				'error_report'		    => array(
												 'type'		=> 'select',
												 'defval'	=> array('0'	=> array(	'rus' => 'Не вести лог ошибок',
												 										'eng' => 'No logged'),
												 					 '1'	=> array(	'rus' => 'Вести лог ошибок',
												 										'eng' => 'Logged errors'),
												 					 '3'	=> array(	'rus' => 'Лог ошибок+отправка разработчику',
												 										'eng' => 'Logged errors+send report'),
												 					 '2'	=> array(	'rus' => 'Только отправка разработчику',
												 										'eng' => 'Send report'),
																	 ),
												 'descr'	=> array(
                                                                     'rus' => 'Логирование ошибок (error_log)',
												 					 'eng' => 'Logged errors (error_log)'
                                                ),
												 'access'	=> 'editable'
										 ),
				'repair_mode'			=> array(
												 'type'		=> 'checkbox',
												 'defval'	=> '0',
												 'descr'	=> array(	'rus' => 'Сайт на реконструкции',
												 						'eng' => 'Site under construction'),
												 'access'	=> 'editable',
												 'multisite' => true
												 ),
				'repair_msg'			=> array(
												 'type'		=> 'text',
												 'defval'	=> 'Сайт на реконструкции',
												 'descr'	=> array(	'rus' => 'Текст сообщения для закрытого сайта',
												 						'eng' => 'Message text for the closed site'),
												 'access'	=> 'editable',
												 'multisite' => true
												 ),
                'poll_popup'			=> array(
                    'type'		=> 'checkbox',
                    'defval'	=> 'checked',
                    'descr'	=> array(	'rus' => 'Использовать опрос вместо выбора города',
                        'eng' => 'Poll popup'),
                    'access'	=> 'editable'
                ),
                'poll_link'			=> array(
                    'type'		=> 'checkbox',
                    'defval'	=> 'checked',
                    'descr'	=> array(	'rus' => 'Вывод ссылки на опрос',
                        'eng' => 'Poll link'),
                    'access'	=> 'editable'
                ),
			);

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'db_host' => 'localhost',
'db_database' => 'newsupra',
'db_user' => 'newsupra',
'db_password' => '2KODJoJxeiTYo6g',
'sitename' => 'Suprashop',
'sitename_rus' => 'Suprashop',
'sids' => ' 178990, 211, 3678, 18433, 84657, 155, 12138, 17436, 81317, 107580, 205816, 255, 69730, 106143, 36975, 42000',
'web_address' => 'http://suprashop.ru',
'rewrite_mod' => 'on',
'multilanguage' => 'on',
'default_lang' => 'rus',
'admin_lang' => 'rus',
'debug' => '',
'edit_on_site' => '',
'contact_email' => 'support@supra.ru',
'return_email' => 'support@supra.ru',
'back_office_zip' => '0',
'front_office_zip' => '0',
'server_updates' => 'http://update51.abocms.ru',
'L_KEY' => '50c9e97d51',
'error_reporting' => '6135',
'error_report' => '0',
'repair_mode' => array(
	'sup' => '',	
	),
'repair_msg' => array(
	'sup' => 'Сайт на реконструкции',	
	),
'poll_popup' => '',
'poll_link' => '',
/* ABOCMS:END */
);
?>