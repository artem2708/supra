<?php
session_start();

// Unset all of the session variables.
$_SESSION = array();

// Finally, destroy the session.
session_destroy();
//if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] == '/admin.php') {
if(isset($_SERVER['HTTP_REFERER'])) {
    header("Location: login.php?logout");
} else {
    header("Location: ".(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'login.php'));
}

exit;

?>