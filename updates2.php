<?php
//version begining from 10.08.07
define ('RP', dirname(__FILE__).'/');
define ('SS', mktime());
define ('SL', ini_get('max_execution_time'));

session_start();
if(!$_SESSION['session_is_admin']){Updates::errMsg('Ошибка обращения к файлу обновлений');}
if (strstr($_SERVER['PHP_SELF'], basename(__FILE__))) {
	$n = new Updates;
	if($n->UploadFiles()){
		$n->getUpdate();
	}
}

class Updates {
	var $PARAMS = array();
	function init(){

		$this->PARAMS = $_SESSION['UPDATES'];
	}
	function Updates(){
		$this->init();
	}
	function checkUpdate(){
		$this->init();
		$this->Msg('Подключение к серверу обновлений');
		//отправляем запрос на разрешенные для обновления модули
		$data = $this->connect(null, 'check_update');
		if(!$data['DATA']) {
		    $this->errMsg('Ошибка разбора ответа сервера обновлений');
		}
		$local = $this->getLocalInfo();
		if(!is_array($local)) {
		    $this->errMsg('Ошибка получения данных локальной версии');
		}
		$step = 100/sizeof($data['DATA']);
		foreach($data['DATA'] as $_k=>$_v){
			$pr += $step;
			if(!$local[$_k]) continue;
			$date = (intval($_v['DATES'])>0) ? $_v['DATES_DMY'] : 'неизвестно';
			$this->Msg('Проверка обновлений модуля :'.iconv("WINDOWS-1251", "UTF-8", $_v['TITLE']), intval($pr));
			if ($_v['DATES'] > $local[$_k]['Date'] || version_compare($_v['MOD_VERSION'],$local[$_k]['Vers'])>0){
				$fl = 'TRUE';

				$txt = $this->getDescript($_k, $local[$_k]);
				if(!empty($txt)){
					$this->ScriptWrite('<font color="#0000ff">-----------</font>');
					$this->ScriptWrite('<font color="#0000ff"><pre>'.sprintf("v.%s\n%s",$_v['MOD_VERSION'],$txt).'</pre></font>');
					$this->ScriptWrite('<font color="#0000ff">-----------</font>');
					$this->ScriptWrite("$txt','$_k", 'window.opener.setDescript', false, false);
				}
			}
			 else {
			 	$fl = '';
			}
			$this->ScriptWrite("{$_k}','{$_v['MOD_VERSION']}','{$date}','$fl", 'window.opener.setVersion', false, false);
			#sleep(1);
		}
		$this->ScriptWrite('Проверка обновлений завершена','convert_ok');
	}
	function getUpdate()
	{
		$this->init();
		if(!is_array($this->PARAMS['modules']) || sizeof($this->PARAMS['modules'])<1) {
		    $this->errMsg('Не заданы модули для обновлений');
		}
		$this->Msg('Подключение к серверу обновлений');
		//отправляем запрос на разрешенные для обновления модули
		$data = $this->connect(null, 'check_update');
		if(!$data['DATA']){var_dump($data['BODY']);
		$this->errMsg('Ошибка разбора ответа сервера обновлений');}
		$locl_inf = $this->getLocalInfo();
		$step = 100/sizeof($this->PARAMS['modules']);
		foreach($this->PARAMS['modules'] as $k => $_v){
			$pr += $step;
			if(isset($this->PARAMS['modset'][$_v])){continue ;}
			if(!$data['DATA'][$_v]){$this->Msg('неизвесный модуль - '.$_v, $pr);continue;}
			$this->Msg('Получение изменений по модулю :'.iconv("WINDOWS-1251", "UTF-8", $data['DATA'][$_v]['TITLE'])." ".$data['DATA'][$_v]['MOD_VERSION'], intval($pr-($step/2)));
			$checkFiles = $this->connect(array('mod'=>$_v), 'check_mod');

			if($checkFiles['DATA']){
				$EdFiles = $this->checkEditFiles($checkFiles['DATA']);
				$ErrFile = array_filter($EdFiles, create_function('$a','return ($a["writable"])? false: true;'));
				if(sizeof($ErrFile)>0){
					$files = array_reduce($ErrFile, create_function('$a,$b', 'if($a){$a.="\n".$b["name"];}else{$a=$b["name"];} return $a;'));
					$this->ScriptWrite(
						'<font color="#ff0000"><pre>'."Ошибка: следущие файлы запрещены к перезаписи\n{$files}".'</pre></font>'
					);
					unset($files, $ErrFile);
					continue;
				}
				if(!$this->PARAMS['resset'][$_v]){
					//резервируем старые копии заменяемых файлов
					$reserveFiles = array_filter($EdFiles, create_function('$a','return ($a["replace"])?true:false;'));
					if(sizeof($reserveFiles)>0){if(!$this->reserve($reserveFiles, $_v)){
						$this->ScriptWrite(
							'<font color="#ff0000">Ошибка: невозможно создать резервную копию файлов</font>'
						);
						unset($reserveFiles);
						continue;
					}}
					$_SESSION['UPDATES']['resset'][$_v] = true;
				}
				if(!$this->PARAMS['updset'][$_v]){
					//копирование и установка файлов обновлений
					if(!$this->UploadFiles($EdFiles, $_v, $data['DATA'][$_v])){
						$this->ScriptWrite(
							'<font color="#ff0000">Ошибка загрузки файлов обновлений</font>'
						);
						continue;
					}
					 else {
					 	$this->setVersion($_v, $data['DATA'][$_v]);
					 	if ($this->updateSql($_v, $locl_inf[$_v]['Vers'])) {
					 		$this->Msg('Обновлены таблиц БД', $pr);
					 	} else if (! empty($this->Errors)) {
					 	    $this->ScriptWrite(
    							'<font color="#ff0000">Ошибки обновлени таблиц БД:</font>'
    						);
					 		foreach ($this->Errors as $err) {
					 			$this->ScriptWrite($err);
					 		}
					 		$this->Errors = array();
					 	}
					}
					$_SESSION['UPDATES']['updset'][$_v] = true;
				}
			}
			 else {var_dump($checkFiles['BODY']);}

			$_SESSION['UPDATES']['modset'][$_v] = true;
			$this->Msg('Установка обновлений по модулю :'.iconv("WINDOWS-1251", "UTF-8", $data['DATA'][$_v]['TITLE']),$pr);
			if(intval(SL*0.8+SS)<mktime()){$this->ScriptWrite('<script>location.reload();</script>'); exit;}
		}
		$this->ScriptWrite('Установка обновлений завершена','convert_ok');
		$this->ScriptWrite('admin.php?name=configuration&action=show_info','window.opener.location.reload');
	}
	/*
		@mod module_name
		@tbl - array name tables | mask
		@file - if exists -> baseencode serialize else return baseencode serialize
	*/
	function tableStruct($mod, $tbl=false, $file=false){
		if(!$mod || !$this->ConnecktMysql ()){return false;}
		if(!$tbl){
			$sql = "select concat_ws('_',s.alias,l.language) as prefix from sites s inner join sites_languages l ON (s.id=l.catagory_id)";
			$res = mysql_query($sql); if(!$res){return false;}
			while($tmp = mysql_fetch_assoc($res)){$pr[] = $tmp['prefix'];}
			@mysql_free_result($res);
			if(is_array($pr)){foreach($pr as $v){$tbl[] = $v.'_'.$mod.'*';}}
			$tbl = implode('|',$tbl);

		} elseif(is_array($tbl)){
			$tbl = implode('|',$tbl);
		}
		$tbl = str_replace('*','.*',$tbl);
		$STAT = array();
		$sql = "show table status";
		$res = mysql_query($sql); if(!$res){return false;}
		while($tmp = mysql_fetch_assoc($res)){
			if(preg_match('~^'.$tbl.'$~i', $tmp['Name'])){$STAT[($tmp['Name'])] = array();}
		}
		@mysql_free_result($res);
		if(is_array($STAT)){foreach($STAT as $k => $v){
			$sql = "show columns from `{$k}`";
			$res = mysql_query($sql); if(!$res){return false;}
			while($tmp = mysql_fetch_assoc($res)){$STAT[$k][($tmp['Field'])] = $tmp;}
			@mysql_free_result($res);
		}}
		if(!$file){return base64_encode(serialize($STAT));}
		$ARR = unserialize(base64_decode($file));
		$this->setStruct($STAT,$ARR);

		if($f == 'write'){
			$sql = "";
		}
	}
	function setStruct($NOW,$OLD){
		if(!is_array($NOW) || !is_array($OLD)){return false;}
	}
	function backuprestore(){
		$this->init();
		$dir = getcwd();
		@chdir(RP.'admin/backup/sysbackup');
        foreach (glob("*_*-*.tgz") as $filename) {
			preg_match('~(\w+)_(\d{1,2})(\d{1,2})(\d{2,4})-(\d{2})(\d{2})\.tgz~i', $filename, $m);
			$date = mktime($m[5],$m[6], 0, $m[3], $m[2], $m[4]);
			if(!isset($RET[($m[1])]) || $RET[($m[1])]< $date ){$RET[($m[1])]= $date;}

		}

		if(!isset($RET) && !is_array($RET) || !isset($RET[($this->PARAMS['mod'])])){
			$this->ScriptWrite(
				'<font color="#ff0000">Ошибка файл восстановления не найден</font>'
			);
			$this->ScriptWrite(false,'convert_ok');exit;
		}
		$d = $RET[($this->PARAMS['mod'])];
		$file = sprintf('%s_%08d-%04d.tgz', $this->PARAMS['mod'], date('dmY',$d), date('Hi',$d));

		if(!file_exists($file)){
			$this->ScriptWrite(
				'<font color="#ff0000">Ошибка файл восстановления не найден</font>'
			);
			$this->ScriptWrite(false,'convert_ok');exit;
		}
		@include_once(RP.'inc/class.archive.php');
		@chdir(RP);
		if(!class_exists('archive')){
			$this->ScriptWrite(
				'<font color="#ff0000">Ошибка подключения архиватора</font>'
			);
			$this->ScriptWrite(false,'convert_ok');exit;
		}
		$this->Msg('Открытие архива:'.$file,50);
		$tmp = new gzip_file(RP.'admin/backup/sysbackup/'.$file);
		$tmp->set_options(array('inmemory' => 0, 'basedir' => RP, 'overwrite' => 1 ));
		$tmp->getFiles();
		$tmp->extract_files();
		$this->Msg('Восстановление завершено',100);
		foreach($tmp->files as $v){if(substr($v['name'],-10) == 'config.php'){$path_to_config = $v['name']; break;}}
		if(!$path_to_config){$this->ScriptWrite(false,'convert_ok');exit;}
		$vers = $this->getFileVersion($path_to_config);
		if(!$vers){$this->setVersion($this->PARAMS['mod'], array('VERSION'=>'','DATES'=>'0'));}
		 else {$this->setVersion($this->PARAMS['mod'], array('VERSION'=>$vers['vr'], 'MOD_VERSION'=>$vers['vr'], 'DATES'=>$vers['Date']));}
		$this->ScriptWrite(false,'convert_ok');
		$this->ScriptWrite('admin.php?name=configuration&action=show_info','window.opener.location.reload');
		exit;
	}


	function UploadFiles($Files=false, $modName=false, $dopInfo = false){
		$uniq = &$_SESSION['UPDATES']['uniq'];
		if (!$uniq) {
		    $uniq = uniqid(rand(), true);
		}
		$tmp_file = RP.'admin/backup/sysbackup/'.$uniq.'.tmp';

		@include_once(RP.'inc/class.archive.php');

		if (!$Files) {
		    $Files = @$_SESSION['UPDATES']['Current']['Files'];
		}
		if (!$modName) {
		    $modName = @$_SESSION['UPDATES']['Current']['modName'];
		}
		if (!$dopInfo) {
		    $dopInfo = @$_SESSION['UPDATES']['Current']['dopInfo'];
		}
		$_SESSION['UPDATES']['Current'] = array (
			'Files'		=> $Files,
			'modName'	=> $modName,
			'dopInfo'	=> $dopInfo,
		);
		$file_load = false;
		$tmp_file = RP.'admin/backup/sysbackup/'.$uniq.'.tmp';

		$dop['noparse'] = true;
		if (@$_SESSION['UPDATES']['ft']) {
		    $dop['range'] = $_SESSION['UPDATES']['ft'];
        }
		$fp = $this->connect(
			array(
				'mod'	=>	$modName,
				'files'	=>	$Files
			),
			'get_file',
			$dop
		);

		if(!$fp && 'resource'!=gettype($fp)) {
		    return false;
		}
		
		$http_status = fgets($fp, 1024);

		list($h_t, $h_v, $h_c,$h_s) = sscanf(strtoupper(trim($http_status)), "%4s/%s %s %s");
		if($h_t!='HTTP' || ($h_c!='200' && $h_c!='206')){return false;}
		$headers = true;
		while ($headers && false!=($buff = fgets($fp, 1024))) {
			if (strpos($buff, ':')!=false) {
			    list($p, $v) = explode(':', trim($buff));
			    $HEAD[strtoupper($p)] = trim($v);
			}
			if ($buff == "\x0D\x0A") {
			    break;
			}
		}
		$tmp = fopen($tmp_file, 'ab');

		while (!feof($fp) && false!=($buff = fgets($fp, 4096))){
			fputs($tmp, $buff);
			$_SESSION['UPDATES']['ft'] += strlen($buff);
			if(mktime()>(SS+SL*0.8)){$this->ScriptWrite('<script>location.reload();</script>'); break;}
		}
		fclose($fp);
		fclose($tmp);

		$tmp = new gzip_file($tmp_file);
		$tmp->set_options(array('inmemory' => 1, 'basedir' => RP));
		$tmp->getFiles();
		if (is_array($tmp->files)) {
			foreach($tmp->files as $v) {
				if(substr($v['name2'],-10)=='config.php' && file_exists($v['name'])){
					list(,$m2) = each($tmp->extractList($v['name']));
					$this->saveConfig($m2, $v['name2']);
				} else {
				    $fls[] = $v['name2'];
				}
			}
		}
		if (isset($fls) && sizeof($fls)>0){
			$tmp->set_options(
				array(
					'inmemory' 	=> 0,
					'overwrite' => 1
				)
			);
			$tmp->extractList($fls);
		}
		unset($tmp, $fls);
		@unlink($tmp_file);
		unset($_SESSION['UPDATES']['ft'], $_SESSION['UPDATES']['Current']);
		return true;
	}
	function saveConfig($txt, $path){
		$dat = $_SESSION['UPDATES']['Current'];
		$versText = sprintf('%s %s', $dat['dopInfo']['MOD_VERSION'], $dat['dopInfo']['DATES_DMY']);
		//получение установок из старого файла
		$file_exists = file_exists($path);
		$old_config = $file_exists ? file_get_contents($path) : '';
		if (!$file_exists || !preg_match('~\/\* ABOCMS:START \*\/(.+)\/\* ABOCMS:END \*\/~ius',$old_config, $m) || $m[0]=='') {
		    $fp = @fopen(RP.$path, 'wb');
    		if(!$fp) return false;
    		fputs($fp, $txt);
    		fclose($fp);
    		return true;
		}

		$old_config = $m[0]; unset($m);
		$_from 	= array (
			'~\/\/\s*Vers\s+[X\d]{1,2}(\.[X\d]{1,2}){0,2}(\s[#\d]{1,2}\.[#\d]{1,2}\.[#\d]{4})?~is',
			'~\/\* ABOCMS:START \*\/(.+)\/\* ABOCMS:END \*\/~ius',
		);
		$_to	= array (
			'// Vers '.$versText,
			$old_config,
		);

		$n = preg_replace($_from, $_to, $txt);

		$fp = @fopen(RP.$path, 'wb');
		if(!$fp) return false;
		fputs($fp, $n);
		fclose($fp);
		return true;
	}

	function reserve($arr, $name){
		@include_once(RP.'inc/class.archive.php');
		if(!class_exists('archive')){return false;}
		if(!is_dir(RP.'admin/backup/sysbackup')){@mkdir(RP.'admin/backup/sysbackup');}
		if(!is_dir(RP.'admin/backup/sysbackup')){return false;}
		if(!is_array($arr) || sizeof($arr)<1){return true;}
		$box = sprintf('%s/%s_%s.tgz',	RP.'admin/backup/sysbackup',$name,date('dmY-hi'));
		$arch = new gzip_file($box);
    	$options = array(
    		'basedir' 		=> RP,
    		'comment' 		=> "backup modules {$name}; date: (".date('d/m/Y H:i').") ",
    		'storepaths' 	=> 1,
    		'overwrite' 	=> 1
    	);
    	$arch->set_options($options);
    	foreach($arr as $v){$arch->add_files($v['name']);}
    	$arch->create_archive();
    	if(file_exists($box)){return true;}
    	return false;
	}
	function getLocalInfo(){
		if(!$this->ConnecktMysql ()){$this->errMsg('Ошибка подключения к MySQL');}
		$SQL = "SELECT Vers,LU FROM sites ORDER BY id LIMIT 1";
		$res = mysql_query($SQL); if(!$res){$this->mysql_er();return false;}
		if(mysql_num_rows($res) == 1){$RET['_kernel'] = mysql_fetch_assoc($res); @mysql_free_result($res);$RET['_kernel']['FILE'] = '/config.php';}
		$SQL = "SELECT name, Vers, LU FROM modules order by name";
		$res = mysql_query($SQL);if(!$res){$this->mysql_er();return false;}
		if(mysql_num_rows($res)>0){
			while($RT = mysql_fetch_assoc($res)){
				$name = $RT['name']; unset($RT['name']);
				$RT['FILE'] = '/mod/'.$name.'/config.php';
				$RET[$name] = $RT;
			}
			@mysql_free_result($res);
		}
		if(!is_array($RET)){return false;}
		foreach($RET as $k => $v){
			$vr = $this->getFileVersion($v['FILE']);
			if($vr['vr'] && !$v['Vers']){$RET[$k]['Vers'] = $vr['vr'];}
			if($vr['vr'] && $v['Vers']){$RET[$k]['Vers'] = (version_compare($vr['vr'], $v['Vers'])>0) ? $v['Vers'] : $vr['vr'];}
			$RET[$k]['Date'] = (intval($vr['Date'])>intval($RET[$k]['LU'])) ? intval($vr['Date']) : intval($RET[$k]['LU']);
		}
		return $RET;
	}
	function getLocalVersion(){
		$local = $this->getLocalInfo();
		return $local;
	}
	function checkEditFiles($arr){
		$ret = array();
		if (!is_array($ret)) {
		    return $ret;
		}	
		foreach($arr as $k => $v){
			if (!file_exists(RP.$v['name'])){
			    $v['writable'] = false;
			    $path = $v['name'];
			    while ( !$v['writable'] && ($path = substr($path, 0, strrpos($path, '/'))) ) {
			        $v['writable'] = is_writable($path);
			    }			
			    $ret[] = $v;
			} elseif (md5_file(RP.$v['name'])!=$v['crc']) {
			    $v['replace'] = true;
			    $v['writable'] = is_writable(RP.$v["name"]);
			    $ret[] = $v;
			}
		}
		//@array_walk($ret, create_function('&$v,$k','$v["writable"] = (file_exists($v["name"])) ? is_writable($v["name"]) : is_writable(dirname($v["name"]));'));
		return $ret;
	}
	function getFileVersion($file){
		if(!file_exists(RP.$file)){return false;}
		$fp = fopen (RP.$file, 'r'); $line = fgets($fp , 1024); fclose($fp);
		if(!empty($line) && strpos($line, '//')){
			preg_match('~vers\s*((?:\d+)(?:\.\d+)?(?:\.\d+)?)\s+((?:\d{1,2})\.(?:\d{1,2})\.(?:\d{2,4}))?~i', $line, $m);
			if($m[1]) $ret['vr'] = $m[1];
			if($m[2]) {$d = explode('.', $m[2]); $ret['Date'] = mktime(0,0,0, $d[1],$d[0],$d[2]);}
			return $ret;
		}
		return false;
	}
	function setVersion($mod, $data){
		if(!$this->ConnecktMysql ()){$this->errMsg('Ошибка подключения к MySQL');}
		$tbl_query = ($mod == '_kernel') ? 'sites' : 'modules';
		$whr_query = ($mod == '_kernel') ? '1' : " name='{$mod}'";
		$SQL = "update {$tbl_query} set Vers='{$data['MOD_VERSION']}', LU='{$data['DATES']}' where $whr_query";
		$res = mysql_query($SQL);if(!$res){$this->mysql_er();return false;}
		return true;
	}
	function Msg($str=false, $proz = false){
		static $pr = 0; if($proz){$pr = intval($proz);}
		echo "<script>parent.baze_print('".str_replace(array("\n","\r"), array('\n',''), addslashes($str))."', $pr);</script>\n";
		echo $str."<br>\n";
		flush();
	}
	function ScriptWrite($str, $comm=false, $newLine = true, $addSlash = true){
		if($comm){
			if($comm == 'convert_ok'){$this->Msg(false,100);}
			echo "<script>parent.{$comm}('".str_replace(array("\n","\r"), array('\n',''), ($addSlash)? addslashes($str): $str)."');</script>";
		}
		 else {
		 	echo "$str";
		}
		if($newLine){echo "<br>\n";}
		flush();
	}
	function errMsg($str, $exit=true){
		echo "<script>parent.error_msg('".str_replace(array("\n","\r"), array('\n',''), addslashes($str))."');</script>";
		if($exit) exit;
		 else flush();
	}
	function getDescript($mdl, $info){
		$params = array ('modules' => $mdl, 'info' => $info);
		$Arr = $this->connect($params, 'getinfo');
		if(!empty($Arr['BODY'])) return $Arr['BODY'];
	}

	function connect($params, $type=null, $dop = null){
		static $HOST, $URL;

		//определяем хост подключения
		if(!$HOST){
			$uri = $this->PARAMS['server_updates'];
			$url = (strpos($uri,'http://')===false)? array('host' => $uri) : parse_url($uri);
			if(!$url['host']){$this->errMsg('Ошибка адреса сервера обновлений');}
			$HOST = $url['host']; unset($url);
		}

		//определяем страницу(URL) подключения
		if($type){
			$URL = sprintf('/version2/%s.html', $type, md5($this->PARAMS['L_KEY']));
		} elseif(!$URL) {
		 	$URL = '/';
		}

		//отправляем запрос на подключение
		while(++$i<5 && !$fp) {
		    if($i>1) {
		        $this->ScriptWrite('Попытка :'.($i+1));
		    }
		    $fp = @fsockopen($HOST, 80, $errno, $errstr, $i*5);
		}

		if(!$fp) {
		    $this->errMsg("Ошибка подключения к серверу обновлений {$errstr} ({$errno})");
		}

		//при наличии передаваемых данных формируем контейнер запроса
		if($params){
    		$msg_str = chunk_split(base64_encode(serialize($params)));
    		$msg_id = uniqid('');
    		$snd = <<<EOD
-----------------------------{$msg_id}
Content-Disposition: form-data; name="strcode"

{$msg_str}-----------------------------{$msg_id}--
EOD;
    		$out = "POST $URL HTTP/1.0\r\n";
    		$out .= "Content-Type: multipart/form-data; boundary=".str_repeat('-',27).$msg_id."\r\n";
		} else {
    		$out = "GET $URL HTTP/1.0\r\n";
		}
		$out .= "Host: $HOST\r\n";

		if(function_exists('gzinflate')){
			$out .= "Accept-Encoding:  deflate\r\n";
		}
    	if($params){
   			$out .= "Content-Length: ".(strlen($snd))."\r\n";
   		}

    		$out .= "Domain: {$_SERVER['HTTP_HOST']}\r\n";
    		$out .= "Key: ".md5($this->PARAMS['L_KEY'])."\r\n";
    	if($dop['alive']){
    		$out .= "Connection: Keep-Alive\r\n";
    	}
    	 else {
    	 	$out .= "Connection: Close\r\n";
    	}
    		$out .= "Cache-Control: no-cache\r\n";

    	if(isset($dop['range']) && intval($dop['range'])>0){
    		$out .= "Range: bytes={$dop['range']}-\r\n";
    	}
    	if($params){
    		$out .= "\r\n";
    		$out .= $snd;
		}
		$out .= "\r\n\r\n";

		fwrite($fp, $out); $header = true;
		stream_set_timeout($fp, 5);
		if($dop['noparse']) {
		    return $fp;
		}
		$ret = $this->parseSND($fp);
		fclose($fp);
		if($ret == false) {
		    $this->errMsg("Нулевой ответ сервера", false);
		}
		return $ret;
	}
	//функция разбора получаемых данных
	function parseSND(&$fp){
		$header = true;
		$proto = fgets($fp, 128);
		list($h_t, $h_v, $h_c,$h_s) = sscanf(strtoupper(trim($proto)), "%4s/%s %s %s");
		if($h_t!='HTTP' || $h_c!='200'){return false;}
		while(!feof($fp)){
			$buff .= fgets($fp, 128);
			if($header && $buff == "\x0D\x0A"){$header = false; $buff=''; continue;}
			if($header && strpos($buff, ':')!=false){list($p, $v) = explode(':', trim($buff)); $RET['HEAD'][strtoupper($p)] = trim($v); $buff='';}
			if(!$header){$RET['BODY'] .= $buff; $buff = '';}

		}

		//в случае сжатых данных декодируем
		if($RET['HEAD']['CONTENT-ENCODING']=='deflate'){
			if(false!==($tmp = @gzinflate($RET['BODY']))){$RET['BODY'] = $tmp;}
		}
		//в случае если данные являются serialize, разбираем их
		if(false!=($un = @unserialize($RET['BODY']))){$RET['DATA'] = $un;unset($RET['BODY']);}
		return $RET;
	}
	function ConnecktMysql ()
	{   static $sConnected = false;

 		if ($sConnected || (
 		    @mysql_connect($this->PARAMS['db_host'], $this->PARAMS['db_user'], $this->PARAMS['db_password'])
     		&& @mysql_select_db ($this->PARAMS['db_database'])
     	)) {
     	    $sConnected = TRUE;
     	} else {
     	    $this->mysql_er();
     	}
     	return $sConnected;
	}
	function mysql_er() {
    	$err = debug_backtrace();
        @array_shift($err);
        @array_shift($err);
        list(,$e) = each ($err);
        $errmsg = sprintf("<b>MySQL Error</b>: %s (%s)",$msg, $this->Error);
        $filename = basename($e['file']);
        $linenum  = $e['line'];

		if($this->PARAMS['error_report']&1){
        	$msg = sprintf("date:%s\tfile:%s\tline:%s\tmsg:%s\n",date('d/m/Y H:i:s'), $filename, $linenum, $errmsg);
        	error_log($msg, 3, RP2.'admin/error_log.txt');
    	}
    	if($er&2 && $er_msg&$errno){//send logg
        	$msg = array('errno'=>$errno, 'errmsg'=>$errmsg, 'filename'=>$filename, 'linenum'=>$linenum, 'vars'=>$vars);
        	if(function_exists('send_error')){send_error($msg);}
    	}
	}
	/**
	 * @return array['site_alias']['lang']
	 */
	function getSiteLangs()
	{
		if (!$this->ConnecktMysql ()) {
		    $this->errMsg('Ошибка подключения к MySQL');
		}
		$res = mysql_query("SELECT alias, language FROM sites AS S INNER JOIN sites_languages AS L ON S.id=L.category_id");
		if (!$res)  {
		    $this->mysql_er();
		    return false;
		}
		$ret = array();
		while ($data = mysql_fetch_row($res)) {
			$ret[$data[0]][] = $data[1];
		}

		return $ret;
	}
	function updateSql($mod_name, $from_ver)
	{
	    $this->Errors = array();
	    $inf = $this->getLocalInfo();
		if (!file_exists(RP."mod/".$mod_name) || !isset($inf[$mod_name])) {
			return false;
		}
		$curr_ver = $this->ver2Int($inf[$mod_name]['Vers']);
		$from_ver = $this->ver2Int($from_ver);
		if (!$curr_ver) {
			return false;
		}
		$from_ver = $from_ver > 1 ? intval($from_ver) : 1;
		$files = glob(RP."mod/".$mod_name."/sql/update*-*.sql.php");
		$upd_files = array();
		if ($files) {
			foreach ($files as $filename) {
				if (preg_match("/update(\d{1,2}(?:\.\d{1,2}){0,2})-(\d{1,2}(?:\.\d{1,2}){0,2}).sql.php$/i", $filename, $matches)) {
					$upd_from_ver = $this->ver2Int($matches[1]);
					$upd_to_ver = $this->ver2Int($matches[2]);
					if ($upd_to_ver<=$curr_ver && $upd_to_ver>$from_ver) {
						$upd_files[] = array('file' => $filename, 'from_ver' => $upd_from_ver, 'to_ver' => $upd_to_ver);
					}
				}
			}
		}
		$site_langs = $this->getSiteLangs();
		if (! empty($upd_files) && $site_langs) {
			usort($upd_files, array($this, '_cmpVers'));
			foreach ($upd_files as $file) {
			    $sql = array();
				include($file['file']);
				$this->Msg('Обновление - '.$file['file']);
				foreach ($sql as $query) {
					foreach ($site_langs as $alias => $langs) {
						foreach ($langs as $lang) {
							@mysql_query(str_replace('{site_prefix}', $alias."_".$lang, $query));
							if (mysql_error()) {
								$this->Errors[] = mysql_error();
							}
						}
					}
				}
			}
		}
		return empty($this->Errors) ? true : false;
	}
	function ver2Int($ver)
	{
		$a = explode(".", $ver);
		$n = 3;
		$ret = 0;
		for ($i = 0; $i < $n; $i++) {
		    $a[$i] = preg_replace("/^0*/", '', $a[$i]);
		    $ret += $a[$i] ? $a[$i]*pow(100, $n - $i - 1) : 0;
		}
		return $ret;
	}
	function _cmpVers($a, $b)
	{
		if ($a['from_ver'] > $b['from_ver']
		    || ($a['from_ver'] == $b['from_ver'] && $a['to_ver'] > $b['to_ver'] )
		) {
			 return -1;
		}
		if ($a['from_ver'] < $b['from_ver']
		    || ($a['from_ver'] == $b['from_ver'] && $a['to_ver'] < $b['to_ver'] )
		) {
			 return 1;
		}
		return 0;
	}
}

?>